###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines from reconstructed data.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_all_lines.py
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines import all_lines

options.set_input_and_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')

options.input_raw_format = 4.3
options.evt_max = 100

options.output_file = 'hlt2_all_lines.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "hlt2_all_lines.tck.json"


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=True):
    config = run_moore(options, make_lines, public_tools)
