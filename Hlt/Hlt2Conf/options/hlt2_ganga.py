###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 with Ganga-like input definition.

This demonstrates what an options file for use with Ganga looks like.

Requires a separate file that defines the input data using IOExtension.
"""
from Moore import options, run_moore

from Hlt2Conf.lines.charm.d0_to_hh import all_lines
from RecoConf.global_tools import stateProvider_with_simplified_geom
# TODO stateProvider_with_simplified_geom must go away from option files

options.evt_max = 10
options.simulation = True

# Must still define input data parameters
options.input_type = 'ROOT'
options.input_raw_format = 4.3
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'
options.geometry_version = 'trunk'
options.conditions_version = 'master'


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
