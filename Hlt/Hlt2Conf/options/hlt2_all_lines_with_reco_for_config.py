###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines with on-the-fly reconstruction.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_all_lines.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines import all_lines
import re

options.evt_max = 200
options.output_file = 'hlt2_all_lines_with_reco_for_config.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "hlt2_all_lines_with_reco_for_config.tck.json"
# temporarily use HiveWhiteBoard, see lhcb/LHCb!2878
options.use_iosvc = False
options.event_store = 'HiveWhiteBoard'


def remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.match(pattern_to_remove, name) is None
    }
    print("Removed lines: ", set(lines_dict) - set(filtered))
    return filtered


# Remove lines which either use gamma or pi0 or jets.
# gamma and pi0 can be added back when Moore!593 is merged.
pattern_to_remove = "(?i)(hlt2jets)"

hlt2_lines = remove_lines(all_lines, pattern_to_remove)

print("Number of HLT2 lines {}".format(len(hlt2_lines)))


def make_lines():
    return [builder() for builder in hlt2_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
