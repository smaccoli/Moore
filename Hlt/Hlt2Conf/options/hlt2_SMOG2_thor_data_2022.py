###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file to test running on data.
"""

from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
    get_UpgradeGhostId_tool_no_UT)
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClusterDecoder
from RecoConf.hlt1_tracking import (
    make_VeloClusterTrackingSIMD, make_RetinaClusters,
    get_global_measurement_provider, make_velo_full_clusters)
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
from RecoConf.hlt1_tracking import make_TrackBeamLineVertexFinderSoA_pvs
from GaudiKernel.SystemOfUnits import mm

from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.calorimeter_reconstruction import make_digits

from Hlt2Conf.lines.ift import all_lines  #Just test SMOG2 lines

options.input_files = [
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/hlt2/LHCb/0000251995/Run_0000251995_HLT20101_20221102-154523-256.mdf"
]

options.input_type = 'RAW'
options.input_process = 'Hlt2'
options.evt_max = 1000
options.output_file = "data_SMOG2_hlt2_2022_Run251995_addedSMOG2lines.dst"
options.output_type = "ROOT"
options.histo_file = "data_SMOG2_hlt2_2022_Run251995_addedSMOG2lines_histos.root"
options.output_manifest_file = "data_SMOG2_hlt2_2022_Run251995_addedSMOG2lines.tck.json"

#================================
# Data tags
#================================

options.geometry_version = 'trunk'
options.conditions_version = 'AlignmentV9_2023_03_16_VPSciFiRich'
options.simulation = False

#options.event_store = 'EvtStoreSvc'

#===============================
# DD4HEP settings
#===============================

dd4hepSvc = DD4hepSvc()
dd4hepSvc.DetectorList = [
    '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal', 'Muon'
]  #no UT
dd4hepSvc.ConditionsLocation = 'git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git'


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = []

options.scheduler_legacy_mode = False

with reconstruction.bind(from_file=False),\
     make_light_reco_pr_kf_without_UT.bind(skipRich=False, skipCalo=False, skipMuon=False),\
     make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=8.0),\
     make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=8.0),\
     make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.0),\
     make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=8.0),\
     make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD),\
     get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters),\
     make_muon_hits.bind(geometry_version=3),\
     make_digits.bind(calo_raw_bank=True),\
     make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClusterDecoder),\
     get_global_measurement_provider.bind(velo_hits=make_RetinaClusters),\
     hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),\
     make_TrackBeamLineVertexFinderSoA_pvs.bind(minz=-541 * mm):
    config = run_moore(options, make_lines, public_tools)
