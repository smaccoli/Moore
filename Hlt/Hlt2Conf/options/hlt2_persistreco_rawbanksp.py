###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using reco from file. Produces hlt2_testpersistreco.dst

Run like any other options file:

    ./Moore/run gaudirun.py tests/options/hlt_dst_output.py hlt2_persistreco.py
    or
    ./Moore/run gaudirun.py tests/options/hlt_mdf_output.py hlt2_persistreco.py
"""
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs as make_pvs
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_particles import make_has_rich_long_kaons
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, micrometer as um
import Functors as F
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Functors import require_all

from pprint import pprint

### Define input files and basic options ###

input_files = [
    # evttype 27163002
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000033_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000038_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000040_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000047_2.ldst'
]

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 4.3
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = 1000
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'
options.geometry_version = 'trunk'
options.conditions_version = 'master'

#following options are set in tests/options/hlt_dst_output.py or hlt_mdf_out.py
options.output_file = 'hlt2_test_persistreco_rawbanksp.{stream}.mdf'
options.output_type = 'MDF'

# This options file is used with different output types; subsequent tests
# require TCKs for each case
if options.output_type == "ROOT":
    options.output_manifest_file = "hlt2_persistreco_rawbanksp_root_output.tck.json"
elif options.output_type == "MDF":
    options.output_manifest_file = "hlt2_persistreco_rawbanksp_mdf_output.tck.json"

###Define 2 orthoganol lines (PIDK cut) with and without persistreco


def make_charm_kaons(pidk_cut):
    """Return kaons maker for D0 decay selection.
    """
    pvs = make_pvs()
    code = require_all(
        F.MINIPCUT(IPCut=60 * um, Vertices=pvs), F.PT > 800 * MeV,
        F.P > 5 * GeV, pidk_cut)
    return ParticleFilter(make_has_rich_long_kaons(), F.FILTER(code))


def make_dzeros(particle1, particle2, name, descriptor):
    """Return D0 maker with selection tailored for two-body hadronic final
    states.

    Args:
        particles (list of DataHandles): Input particles used in the
                                         combination.
        name (string): Combiner name for identification.
        descriptor (string): Decay descriptor to be reconstructed.
    """
    pvs = make_pvs()
    combination_cut = require_all(F.MASS > 1685 * MeV, F.MASS < 2045 * MeV,
                                  F.PT > 2 * GeV,
                                  F.MAX(F.PT) > 1000 * MeV,
                                  F.MAXDOCACUT(0.1 * mm))
    vertex_cut = require_all(F.MASS > 1715 * MeV, F.MASS < 2015 * MeV,
                             F.CHI2DOF < 10.,
                             F.BPVFDCHI2(pvs) > 25.,
                             F.BPVDIRA(pvs) > 0.99985)

    return ParticleCombiner([particle1, particle2],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cut)


def test_persistreco_raw_bank_sp_line(name='Hlt2_test_persistreco_raw_bank_sp',
                                      prescale=1,
                                      persistreco=False):
    kaons = make_charm_kaons(pidk_cut=F.PID_K < 15)
    dzeros = make_dzeros(
        particle1=kaons,
        particle2=kaons,
        name='Charm_D0ToHH_D0ToKmKp_persistreco',
        descriptor='D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dzeros],
        prescale=prescale,
        persistreco=persistreco,
        raw_banks=[
            'VP', 'UT', 'FTCluster', 'Rich', 'EcalPacked', 'HcalPacked',
            'Muon', 'VPRetinaCluster', 'Calo', 'Plume'
        ])


def test_raw_bank_sp_line(name='Hlt2_test_raw_bank_sp',
                          prescale=1,
                          persistreco=False):
    kaons = make_charm_kaons(pidk_cut=F.PID_K > 15)
    dzeros = make_dzeros(
        particle1=kaons,
        particle2=kaons,
        name='Charm_D0ToHH_D0ToKmKp_nopersistreco',
        descriptor='D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dzeros],
        prescale=prescale,
        persistreco=persistreco,
        raw_banks=['VPRetinaCluster'])


def make_streams():
    linedict = dict(
        test_stream_A=[test_persistreco_raw_bank_sp_line()],
        test_stream_B=[test_raw_bank_sp_line()])
    pprint(linedict)
    return linedict


# Modify stream dictionaries for testing purposes
import Moore.streams_hlt2
Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM = {
    "test_stream_A": [
        'ODIN', 'VP', 'UT', 'FTCluster', 'Rich', 'EcalPacked', 'HcalPacked',
        'Muon', 'VPRetinaCluster', 'Calo', 'Plume'
    ],
    "test_stream_B": [
        'VPRetinaCluster',
    ]
}

Moore.streams_hlt2.stream_bits = dict(test_stream_A=85, test_stream_B=90)

print("Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM")
pprint(Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM)

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_streams, public_tools)
