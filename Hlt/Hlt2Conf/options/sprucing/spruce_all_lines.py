###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration file of a Sprucing throughput test on all Sprucing lines.
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from PyConf.application import metainfo_repos

from Moore.streams_hlt2 import DETECTOR_RAW_BANK_TYPES, HLT1_REPORT_RAW_BANK_TYPES, HLT2_REPORT_RAW_BANK_TYPES
import Moore.streams_spruce

from Hlt2Conf.lines import sprucing_lines

import logging
log = logging.getLogger()

metainfo_repos.global_bind(extra_central_tags=['key-181687a7'])
options.input_raw_format = 0.3
options.input_process = 'Hlt2'
options.monitoring_file = "monitoring.json"
options.output_file = "spruce_all_lines.mdf"
options.output_type = "MDF"


def make_streams():
    "Makes each line its own stream"

    return {'AllStreams': [builder() for builder in sprucing_lines.values()]}


banks = set.union(DETECTOR_RAW_BANK_TYPES, HLT1_REPORT_RAW_BANK_TYPES,
                  HLT2_REPORT_RAW_BANK_TYPES)
Moore.streams_spruce.stream_banks = {'AllStreams': [i for i in banks]}

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom()
]

options.scheduler_legacy_mode = False

with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(options, make_streams, public_tools, analytics=True)
