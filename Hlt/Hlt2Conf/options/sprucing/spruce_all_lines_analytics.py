###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Run with
`./run gaudirun.py spruce_all_lines_analytics.py`

Performs event size analytics on Sprucing events by exploiting the fact that each
stream gets its own instance of `CombineRawBankViewsToRawEvent`.

-Make each Sprucing line its own stream.
-Report size of DstData RawBank (contains the physics and reconstruction objects) and
 total event size where the event contains the RawBanks defined in `banks` in this options file
-Will not produce output files through use of `stream_writer.bind(write=False)`.
-In log will see StatCounter with name of line

LineName...    INFO Number of counters : X
 |    Counter                                      |     #     |    sum | mean/eff^* | rms/err^*  |     min   |     max    |
 | "DstData bank size (bytes)"                     |         X |      Y |         YY |     YYY    |      YYYY |      YYYYY |
 | "Event size (bytes)"                            |         X |      Z |         ZZ |     ZZZ    |      ZZZZ |      ZZZZZ |


Note that by running `CombineRawBankViewsToRawEvent` in VERBOSE mode - achieved through
`CombineRawBankViewsToRawEvent.bind(OutputLevel=1)` one also has the log of the sizes of ALL RawBanks in `banks`.

Runs Sprucing over HLT1 filtered Min bias sample that has been processed by TOPO{2, 3} HLT2 lines - 478 events.
If you do not see this for your line then your line did not fire on any event in the input data.

Eventually we should have a test like this over each stream as that will define `banks` and hence the appropriate
event size. At the moment this script runs over ALL Sprucing lines and analyses ALL RawBanks so the event size contains
everything.

"""

import re
from Moore import options, run_moore
from PyConf.Algorithms import CombineRawBankViewsToRawEvent
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from PyConf.utilities import ConfigurationError
from PyConf.application import metainfo_repos

from Moore.streams_hlt2 import DETECTOR_RAW_BANK_TYPES, HLT1_REPORT_RAW_BANK_TYPES, HLT2_REPORT_RAW_BANK_TYPES
import Moore.streams_spruce

from Hlt2Conf.lines import sprucing_lines as all_lines
from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as b2oc_lines
from Hlt2Conf.lines.bandq import sprucing_lines as bandq_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as sl_lines
from Hlt2Conf.lines.rd import sprucing_lines as rd_lines
#from Hlt2Conf.lines.charm import sprucing_lines as charm_lines
from Hlt2Conf.lines.qee import sprucing_lines as qee_lines
import logging
log = logging.getLogger()

########################
WGs = {
    "b2oc": b2oc_lines,
    "bandq": bandq_lines,
    "b2cc": b2cc_lines,
    "sl": sl_lines,
    #"charm": charm_lines,
    "qee": qee_lines,
    "rd": rd_lines,
    "all": all_lines
}

##CHANGE wg to desired
wg = 'all'
########################
if wg not in WGs.keys():
    raise ConfigurationError(f'Working group ({wg}) not recognised.')

metainfo_repos.global_bind(extra_central_tags=['key-181687a7'])
options.input_raw_format = 0.3
options.input_process = 'Hlt2'
options.monitoring_file = "monitoring.json"

# Create new dictionary based on `sprucing_lines` with "readable" line names:
# SpruceRD_LbToKpTauE_TauTo3Pi_OS -> RD_LbToKpTauE_TauTo3Pi_OS
sprucing_lines = WGs[wg]
streamdict = {}
for k, v in sprucing_lines.items():
    wg, name = re.search('Spruce(.+?)_', k).group(1), re.search('_(.+?)$',
                                                                k).group(1)

    streamdict[wg + "_" + name] = [v]


def make_streams():
    "Makes each line its own stream"

    lines = {}
    for k, v in streamdict.items():
        lines[k] = [v[0]()]

    return lines


##Must declare "new" streams (one for each line) and the RawBanks you wish to analyse
banks = set.union(DETECTOR_RAW_BANK_TYPES, HLT1_REPORT_RAW_BANK_TYPES,
                  HLT2_REPORT_RAW_BANK_TYPES)
Moore.streams_spruce.stream_banks = {
    k: [i for i in banks]
    for k in streamdict.keys()
}

public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(
        from_file=True,
        spruce=True), CombineRawBankViewsToRawEvent.bind(OutputLevel=4):
    config = run_moore(options, make_streams, public_tools, analytics=True)
