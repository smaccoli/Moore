###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test pass-through Sprucing line. Produces spruce_passthrough.dst

Run like any other options file:

    ./Moore/run gaudirun.py spruce_passthrough.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Moore.lines import PassLine

input_files = ['hlt2_2or3bodytopo_realtime.mdf']

options.input_raw_format = 0.3
options.input_process = 'Hlt2'
options.input_files = input_files
options.input_type = 'MDF'
options.input_manifest_file = 'hlt2_2or3bodytopo_realtime.tck.json'

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'spruce_passthrough_gaudirun.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_passthrough_gaudirun.tck.json"


def pass_through_line(name="PassThrough"):
    """Return a Sprucing line that performs no selection
    """
    return PassLine(name=name)


def make_lines():
    return [pass_through_line()]


public_tools = [stateProvider_with_simplified_geom()]

config = run_moore(options, make_lines, public_tools)
