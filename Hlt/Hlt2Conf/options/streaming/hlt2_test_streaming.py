###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for testing the streaming of HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_streaming.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Moore.lines import Hlt2Line

from pprint import pprint
options.set_input_and_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')

options.input_raw_format = 4.3
options.evt_max = 1000
options.output_file = 'hlt2_test_persistreco_fromfile.{stream}.mdf'
options.output_type = 'MDF'
options.output_manifest_file = "hlt2_test_streaming.tck.json"

from RecoConf.decoders import default_ft_decoding_version
ft_decoding_version = 2  #4,6
default_ft_decoding_version.global_bind(value=ft_decoding_version)

from RecoConf.reconstruction_objects import reconstruction
from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_particles import make_has_rich_long_kaons
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, micrometer as um
import Functors as F
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Functors import require_all


def make_charm_kaons(pidk_cut):
    """Return kaons maker for D0 decay selection.
    """
    pvs = make_pvs()
    code = require_all(
        F.MINIPCUT(IPCut=60 * um, Vertices=pvs), F.PT > 800 * MeV,
        F.P > 5 * GeV, pidk_cut)

    return ParticleFilter(make_has_rich_long_kaons(), F.FILTER(code))


def make_dzeros(particle1, particle2, name, descriptor):
    """Return D0 maker with selection tailored for two-body hadronic final
    states.

    Args:
        particles (list of DataHandles): Input particles used in the
                                         combination.
        name (string): Combiner name for identification.
        descriptor (string): Decay descriptor to be reconstructed.
    """
    pvs = make_pvs()
    combination_cut = require_all(F.MASS > 1685 * MeV, F.MASS < 2045 * MeV,
                                  F.PT > 2 * GeV,
                                  F.MAX(F.PT) > 1000 * MeV,
                                  F.MAXDOCACUT(0.1 * mm))
    vertex_cut = require_all(F.MASS > 1715 * MeV, F.MASS < 2015 * MeV,
                             F.CHI2DOF < 10.,
                             F.BPVFDCHI2(pvs) > 25.,
                             F.BPVDIRA(pvs) > 0.99985)

    return ParticleCombiner([particle1, particle2],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cut)


def test_A_line(name='Hlt2_test_stream_A_line', prescale=1):
    kaons = make_charm_kaons(pidk_cut=F.PID_K > 15)
    dzeros = make_dzeros(
        particle1=kaons,
        particle2=kaons,
        name='Charm_D0ToHH_D0ToKmKp_stream_A',
        descriptor='D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dzeros],
        prescale=prescale)


def test_B_line(name='Hlt2_test_stream_B_line', prescale=1):
    # intentionally have some overlap with the other line in order to test
    # the routing bits in events going to both streams.
    kaons = make_charm_kaons(pidk_cut=F.PID_K > 0)
    dzeros = make_dzeros(
        particle1=kaons,
        particle2=kaons,
        name='Charm_D0ToHH_D0ToKmKp_stream_B',
        descriptor='D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dzeros],
        prescale=prescale)


def make_streams():
    linedict = dict(
        test_stream_A=[test_A_line()], test_stream_B=[test_B_line()])
    pprint(linedict)
    return linedict


# Modify stream dictionaries for testing purposes
import Moore.streams_hlt2
Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM = {
    "test_stream_A": [
        'ODIN', 'VP', 'UT', 'FTCluster', 'Rich', 'EcalPacked', 'HcalPacked',
        'Muon'
    ],
    "test_stream_B":
    ['ODIN', 'VP', 'UT', 'FTCluster', 'EcalPacked', 'HcalPacked',
     'Muon']  # Remove RICH raw bank
}
Moore.streams_hlt2.stream_bits = dict(test_stream_A=85, test_stream_B=90)

print("Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM")
pprint(Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM)

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_streams, public_tools)
