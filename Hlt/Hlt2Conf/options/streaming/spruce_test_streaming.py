###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test streaming of sprucing lines. Produces two spruce_streaming.dst files prepended by stream name

Run like any other options file:

    ./Moore/run gaudirun.py spruce_test_streaming.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.test.spruce_test import Test_sprucing_line, Test_extraoutputs_sprucing_line
from pprint import pprint

input_files = ['hlt2_2or3bodytopo_realtime.mdf']

options.input_raw_format = 0.5
options.input_files = input_files
options.input_manifest_file = "hlt2_2or3bodytopo_realtime.tck.json"
options.input_type = 'MDF'
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'spruce_streaming.{stream}.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_streaming.tck.json"


def make_streams():
    linedict = dict(
        test_stream_A=[Test_sprucing_line(name="Spruce_test_stream_A_line")],
        test_stream_B=[
            Test_extraoutputs_sprucing_line(name="Spruce_test_stream_B_line")
        ])
    pprint(linedict)
    return linedict


###Custom rawbanks not implemented for sprucing yet
# Modify stream dictionaries for testing purposes
import Moore.streams_spruce
Moore.streams_spruce.stream_banks = {
    "test_stream_A": [
        'ODIN', 'VP', 'UT', 'FTCluster', 'Rich', 'EcalPacked', 'HcalPacked',
        'Muon'
    ],
    "test_stream_B":
    ['ODIN', 'VP', 'UT', 'FTCluster', 'EcalPacked', 'HcalPacked',
     'Muon']  # Remove RICH raw bank
}

print("Moore.streams_spruce.stream_banks")
pprint(Moore.streams_spruce.stream_banks)

public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(options, make_streams, public_tools)
