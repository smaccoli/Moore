###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_trackeff_test.py
"""

from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_fastest_reconstruction
from Hlt2Conf.lines.trackeff.DiMuonTrackEfficiency import all_lines
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.decoders import default_ft_decoding_version

from Configurables import HiveDataBrokerSvc
HiveDataBrokerSvc().OutputLevel = 5

options.set_input_and_conds_from_testfiledb('upgrade_DC19_01_Bs2JPsiPhi_MD')
options.input_raw_format = 0.3
options.evt_max = 100
default_ft_decoding_version.global_bind(value=6)
options.output_file = 'hlt2_trackeff_alllines.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "hlt2_trackeff_withUT.tck.json"


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=False),\
    hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction),\
    make_fastest_reconstruction.bind(skipUT=False):
    config = run_moore(options, make_lines, public_tools)
