###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_opt.py
"""

from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction as reconstruction
from RecoConf.hlt2_global_reco import reconstruction as reconstruction_from_reco, make_fastest_reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.hlt1_tracking import default_ft_decoding_version
from RecoConf.hlt2_tracking import get_global_ut_hits_tool
from Hlt2Conf.lines.charmonium_to_dimuon import JpsiToMuMuDetached_line
from Hlt2Conf.lines.config_pid import nopid_hadrons, nopid_muons, nopid_electrons

from Configurables import HiveDataBrokerSvc
HiveDataBrokerSvc().OutputLevel = 5

input_files = [
    'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00157184/0000/00157184_00000001_1.xdigi',
]
default_ft_decoding_version.global_bind(value=6)
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'
options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 0.3
options.evt_max = 200

options.output_file = 'jpsi.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "jpsi.tck.json"


def make_lines():
    return [JpsiToMuMuDetached_line()]


public_tools = [stateProvider_with_simplified_geom()]

_nopid = True  # True for simulation, False for real data

with reconstruction.bind(from_file=False),\
     reconstruction_from_reco.bind(make_reconstruction=make_fastest_reconstruction),\
     make_fastest_reconstruction.bind(skipUT=True),\
     nopid_hadrons.bind(ignore_pid=_nopid),\
     nopid_muons.bind(ignore_pid=_nopid),\
     nopid_electrons.bind(ignore_pid=_nopid),\
     get_global_ut_hits_tool.bind(enable=False) :
    config = run_moore(options, make_lines, public_tools)
