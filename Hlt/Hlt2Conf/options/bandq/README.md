Update 2023/03/07: it works under `x86_64_v2-centos7-gcc11+detdesc-opt`. Build your stack by doing `utils/config.py binaryTag x86_64_v2-centos7-gcc11+detdesc-opt` before make 

For more up-to-date examples please visit: https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/BandQmigration#Test_your_line 

Examples in this directory are mainly for bookkeeping how to use `noPID` configs in Hlt2 and Sprucing scripts.

`hlt2_opt.py`: run the detached Jpsi2mumu hlt2 line on inclusive Jpsi2mumu sample and save the output into a dst file.

`spruce_opt.py`: run all bandq sprucing lines to analysis the output of `hlt2_opt.py`.

Run these scripts like:
```
PATH_OF_STACK/Moore/run gaudirun.py hlt2_opt.py
```
(Then in the same directory)
```
PATH_OF_STACK/Moore/run gaudirun.py spruce_opt.py
```

