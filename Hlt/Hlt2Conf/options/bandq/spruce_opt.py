###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test running Detached Jpsi2MuMu sprucing line on output of Detached Dimuon (JpsiToMuMuDetached_line) persistreco hlt2 lines (original reco from file). 

Run like any other options file:

    ./Moore/run gaudirun.py spruce_opt.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.reco_objects_for_spruce import reconstruction as reco_spruce, upfront_reconstruction as upfront_spruce

from Hlt2Conf.lines.bandq import sprucing_lines
from Hlt2Conf.lines.config_pid import nopid_muons

input_files = ['./jpsi.dst']

options.input_files = input_files
options.input_manifest_file = "jpsi.tck.json"
options.input_type = 'ROOT'
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'spruce_jpsi.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_jpsi.tck.json"


def make_lines():
    lines = [builder() for builder in sprucing_lines.values()]
    return lines


public_tools = [stateProvider_with_simplified_geom()]

_nopid = True  # True for simulation, False for real data

with nopid_muons.bind(ignore_pid=_nopid),\
     reconstruction.bind(from_file=True, spruce=True),\
     reco_spruce.bind(simulation=True),\
     upfront_spruce.bind(simulation=True) :
    config = run_moore(options, make_lines, public_tools)
