###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines from reconstructed data and the raw event.

Two instances of the each line are run: one that takes reconstructed objects
from the input file (produced by Brunel), and another that takes
reconstructed objects from an on-the-fly reconstruction.

This tests that this scheme is even possible, and that the application can
run successfully, but is used as proxy test for the output writing, which has
some complicated logic for disentangling outputs from the reconstruction and
from the file.
"""
import json

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.charm.d0_to_hh import all_lines

options.set_input_and_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')

options.input_raw_format = 4.3
options.evt_max = 200

options.output_file = 'hlt2_lines_reco_mix.dst'
options.output_type = 'ROOT'


def make_lines():
    lines = []
    # Instantiate lines taking the reconstruction from the file
    with reconstruction.bind(from_file=True):
        lines += [builder() for builder in all_lines.values()]
    # Instantiate the same lines, but running the reconstruction on demand
    with reconstruction.bind(from_file=False):
        for default_name, builder in all_lines.items():
            # Lines must have unique names
            line = builder(name=default_name + "RecoFromRaw")
            lines.append(line)

    # Dump the configuration so we can read it back in downstream tests
    with open("hlt2_lines_reco_mix.lines.json", "w") as outfile:
        d = [
            dict(
                name=l.name,
                extra_outputs=[eo_name for eo_name, _ in l.extra_outputs])
            for l in lines
        ]
        json.dump(d, outfile)
    return lines


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
