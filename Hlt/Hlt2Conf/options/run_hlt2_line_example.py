###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Moore configuration
from Moore import options, run_moore

# For production modules, we would use
# from Hlt2Conf.lines.<wg>.<module> import <line>
from Hlt2Conf.lines.hlt2_line_example import (all_lines, lb0tolcpmum_line)

from GaudiKernel.SystemOfUnits import MeV

# Temporary workaround for TrackStateProvider
from RecoConf.global_tools import stateProvider_with_simplified_geom
public_tools = [stateProvider_with_simplified_geom()]

options.set_input_and_conds_from_testfiledb('Upgrade_MinBias_LDST')
options.input_raw_format = 4.3
options.evt_max = 100
options.control_flow_file = 'control_flow.gv'
options.data_flow_file = 'data_flow.gv'
options.output_manifest_file = 'foo.tck.json'

# We have long names for algorithms and would like to see them in full glory
options.msg_svc_format = "% F%56W%S%7W%R%T %0W%M"


def make_lines():
    standard_lines = [line_builder() for line_builder in all_lines.values()]

    # This is to demonstrate how `configurable`/`bind` works. We could also pass the function arguments directly lb0tolcpmum_line()
    with lb0tolcpmum_line.bind(
            name="Hlt2Tutorial_Lb0ToLcpMumNu_LcpToPpKmPip_Pip_pt450MeV",
            pi_pt_min=450 * MeV):
        modified_line = lb0tolcpmum_line()

    return standard_lines + [modified_line]


run_moore(options, make_lines, public_tools)
