###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
import re

from pprint import pprint

# Select lines for testing purposes
from Hlt2Conf.lines import all_lines


def make_lines():
    return [builder() for builder in all_lines.values()]


def remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.match(pattern_to_remove, name) is None
    }
    print("Removed lines: ", set(lines_dict) - set(filtered))
    return filtered


def make_streams():
    turbo_lines, spruc_lines = [], []
    for name, builder in hlt2_lines.items():
        if name.startswith("Hlt2Topo"):
            spruc_lines.append(builder(persistreco=True))
        else:
            turbo_lines.append(builder())
    linedict = dict(turbo=turbo_lines, sprucing=spruc_lines)
    pprint(linedict)
    return linedict


# Modify stream dictionaries for testing purposes
import Moore.streams_hlt2
DETECTOR_RAW_BANK_TYPES = [
    'ODIN', 'VP', 'UT', 'FTCluster', 'Rich', 'EcalPacked', 'HcalPacked', 'Muon'
]
Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM = {
    "sprucing": DETECTOR_RAW_BANK_TYPES,
    "turbo": DETECTOR_RAW_BANK_TYPES,
}
Moore.streams_hlt2.stream_bits = dict(sprucing=87, turbo=88)

options.output_type = 'MDF'
options.output_file = '{stream}_hlt2_all_lines_with_reco_with_streams_mdf.mdf'
options.output_manifest_file = "hlt2_all_lines_with_reco_with_streams_mdf.tck.json"

options.use_iosvc = False
options.event_store = 'HiveWhiteBoard'

# Remove lines which contain jets
pattern_to_remove = "(?i)(hlt2jets)"
hlt2_lines = remove_lines(all_lines, pattern_to_remove)
print("Number of HLT2 lines {}".format(len(hlt2_lines)))

options.lines_maker = make_streams

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(
        options, public_tools=public_tools, exclude_incompatible=False)
