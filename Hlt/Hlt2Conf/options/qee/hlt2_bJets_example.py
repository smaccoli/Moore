###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Example options file for testing QEE Hlt2 DiBJet lines on signal MC.
    Run like any other options file:

    ./Moore/run gaudirun.py Moore/Hlt/Hlt2Conf/options/qee/hlt2_bJets_example.py
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Hlt2Conf.lines.qee.di_bjet import all_lines

input_files = [
    #some Evttype-49000054 b-jets from Murilo (see https://gitlab.cern.ch/lhcb/Moore/-/blob/dev_hlt2jets/Hlt/Hlt2Conf/options/testrun_jetsrun3_forntuple.py)
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000171_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000567_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000621_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000341_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000802_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000486_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000783_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000721_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000869_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000764_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000618_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000388_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000732_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000709_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000637_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000798_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000283_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000573_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000218_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000855_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000782_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000761_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070434/0000/00070434_00000803_2.ldst'
]

options.input_type = 'ROOT'
options.input_raw_format = 4.3
options.input_files = input_files
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'
options.simulation = True
options.evt_max = 1000
options.output_file = 'hlt2_qee_bjet.dst'
options.output_type = 'ROOT'


def make_lines():
    return [builder() for builder in all_lines.values()]


debug_mode = False
if debug_mode:
    from Gaudi.Configuration import DEBUG
    from Configurables import HLTControlFlowMgr, HiveDataBrokerSvc
    HLTControlFlowMgr().OutputLevel = DEBUG
    HiveDataBrokerSvc().OutputLevel = DEBUG
    options.output_level = DEBUG

public_tools = [stateProvider_with_simplified_geom()]
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=2)

real_time_reco = True
if real_time_reco:
    from RecoConf.reconstruction_objects import reconstruction
    with reconstruction.bind(from_file=False):
        config = run_moore(options, make_lines, public_tools)
else:
    run_moore(options, make_lines, public_tools)
