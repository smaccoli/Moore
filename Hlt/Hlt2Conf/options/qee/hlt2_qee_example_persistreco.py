###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Example options file for testing QEE Hlt2 lines on signal MC and persisting the reco, so we can run the sprucing.
    Run like any other options file:

    ./Moore/run gaudirun.py Moore/Hlt/Hlt2Conf/options/hlt2_qee_persistreco.py
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.qee.single_high_pt_muon import all_lines as single_muon_lines
from Hlt2Conf.lines.qee.electroweak_dimuon import all_lines as dimuon_lines

input_files = [
    # Signal Z -> Mu Mu not HLT1 filtered, 42112001, MagUp
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00127654/0000/00127654_00000062_1.xdigi'
]

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 0.3

options.evt_max = 100
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20201211'
options.conddb_tag = 'sim-20201218-vc-mu100'

options.output_file = 'hlt2_qee_zmumu.mdf'
options.output_type = 'MDF'
options.output_manifest_file = "hlt2_qee_zmumu.tck.json"

from RecoConf.decoders import default_ft_decoding_version
ft_decoding_version = 6
default_ft_decoding_version.global_bind(value=ft_decoding_version)


def make_lines():
    return [
        builder() for line_dict in [dimuon_lines, single_muon_lines]
        for builder in line_dict.values()
    ]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
