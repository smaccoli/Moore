###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test running second exclusive hlt2 line on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_b2oc_realtimereco.dst

Run like any other options file:

    ./Moore/run gaudirun.py spruce_b2oc_example_realtime.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from Hlt2Conf.lines.b_to_open_charm import sprucing_lines

input_files = ['hlt2_2or3bodytopo_realtime.mdf']

options.input_files = input_files
options.input_manifest_file = "hlt2_2or3bodytopo_realtime.tck.json"
options.input_type = 'MDF'

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'spruce_b2oc_realtimereco.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_b2oc_example_realtime.tck.json"


def make_lines():
    lines = [builder() for builder in sprucing_lines.values()]
    return lines


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(
        from_file=True, spruce=True,
        manifest_file=options.input_manifest_file):
    config = run_moore(options, make_lines, public_tools)
