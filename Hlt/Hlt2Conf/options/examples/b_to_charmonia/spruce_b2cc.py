###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test running Bs -> JpsiPhi sprucing line on output of Detached Dimuon (JpsiToMuMuDetached) persistreco hlt2 line (original reco from file or real time reco). Produces spruce_Bs2JpsiPhi.dst

1) run the Hlt2 Detached Dimuon line with your signal MC or minibias sample (modify the `outputdir`, `decay_descriptor`, `outputfile_name` and `output_manifest_file` to your use case, then run with:

    ./Moore/run gaudirun.py ./Moore/Hlt/Hlt2Conf/options/examples/Hlt2DiMuonLines_test.py

2) run the spruce options file with the output from 1):
    ./Moore/run gaudirun.py spruce_b2cc_example.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from Hlt2Conf.lines.b_to_charmonia import sprucing_lines

input_files = ['./dst/fromPersistReco/Bs2JpsiPhi.dst']

options.input_files = input_files
options.input_type = 'ROOT'
options.input_manifest_file = "Hlt2DiMuonLines_Bs2JpsiPhi.tck.json"
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'spruce_Bs2JpsiPhi.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_b2cc_example.tck.json"


def make_lines():
    lines = [builder() for builder in sprucing_lines.values()]
    return lines


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(
        from_file=True, spruce=True,
        manifest_file=options.input_manifest_file):
    config = run_moore(options, make_lines, public_tools)
