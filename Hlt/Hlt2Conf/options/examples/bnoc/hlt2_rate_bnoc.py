###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options

from Hlt2Conf.lines.bnoc import all_lines

#from Hlt2Conf.lines.bnoc.hlt2_bnoc import Bds_KstzKstzb_line


def make_lines():
    # to run all lines
    lines = [builder() for builder in all_lines.values()]
    # to run a specific line
    # lines = [ Bds_KstzKstzb_line() ]
    return lines


options.lines_maker = make_lines

from HltEfficiencyChecker.config import run_moore_with_tuples

options.set_input_and_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')
options.input_raw_format = 4.3
options.evt_max = 100000
#options.set_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')
#options.set_input_from_testfiledb('upgrade_minbias_hlt1_filtered')

options.ntuple_file = "rate_ntuple_bnoc_hlt2.root"

# needed to run over FTv2 data

from RecoConf.reconstruction_objects import reconstruction
#from RecoConf.protoparticles import make_charged_protoparticles
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(
        from_file=False):  #, make_charged_protoparticles.bind(
    #enable_muon_id=True):
    run_moore_with_tuples(
        options,
        hlt1=False,
        public_tools=[stateProvider_with_simplified_geom()])
