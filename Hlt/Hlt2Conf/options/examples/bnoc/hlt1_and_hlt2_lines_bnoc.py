###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options

from Hlt2Conf.lines.bnoc import all_lines


def make_lines():
    # to run all lines
    lines = [builder() for builder in all_lines.values()]
    # to run a specific line
    #lines = [ Bds_KstzKstzb_line() ]
    return lines


options.lines_maker = make_lines

decay = (
    '${Bs}[B_s0 -> ${Kstb}(K*(892)~0 -> ${Km}K- ${pip}pi+) ${Kst}(K*(892)0 -> ${Kp}K+ ${pim}pi-)]CC'
)

options.input_files = [
    # MagDown files
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000001_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000002_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000003_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000004_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000005_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000006_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000007_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000008_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000009_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000010_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000011_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000012_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000013_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000014_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000015_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000016_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000017_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000018_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000019_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000020_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000021_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000022_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000023_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000024_2.xdst',
    'root://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000025_2.xdst',
    'eoot://x509up_u15321@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102883/0000/00102883_00000026_2.xdst',
]
options.input_type = 'ROOT'
options.input_raw_format = 4.3
options.evt_max = 1000
options.print_freq = 100

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20190223'
options.conddb_tag = 'sim-20180530-vc-md100'
options.ntuple_file = "eff_ntuple_bnoc_hlt1_and_hlt2.root"

# needed to run over FTv6 data

from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.hlt1_allen import sequence as allen_sequence

with allen_sequence.bind(sequence="hlt1_pp_default"), reconstruction.bind(
        from_file=False):
    run_chained_hlt_with_tuples(
        options, decay, public_tools=[stateProvider_with_simplified_geom()])
