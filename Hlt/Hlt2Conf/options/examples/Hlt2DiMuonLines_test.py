###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py Moore/Hlt/Hlt2Conf/options/Hlt2DiMuonLines_test.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Hlt2Conf.lines.charmonium_to_dimuon import JpsiToMuMuDetached_line


def all_lines():
    return [JpsiToMuMuDetached_line()]


# When running from Upgrade MC, must use the post-juggling locations of the raw event
raw_event_format = 4.3
outputdir = "/eos/lhcb/wg/BandQ/trigger/Upgrade/persistency_studies/dst/fromPersistReco"
# a list of available samples for testing
#decay_descriptor = 30000000  # MinBias
#decay_descriptor = 18112001  # inclusive Upsilon
#decay_descriptor = 24142001  # inclusive Jpsi
#decay_descriptor = 28142001  # Psi2s --> mu mu
decay_descriptor = 12143001  # B --> Jpsi K
#decay_descriptor = 28144041  # Chic --> Jpsi mu mu
#decay_descriptor = 11114100  # B --> Ks mu mu
#decay_descriptor = 13144011  # Bs --> Jpsi phi
#decay_descriptor = 11144008  # B0->Jpsi rho, Jpsi->dimuon, rho->pipi
#decay_descriptor = 12143001  # B+ -> Jpsi K+, Jpsi->dimuon
#decay_descriptor = 12245021  # B -> Jpsi K pi pi, Jpsi->dimuon
#decay_descriptor = 15144001  # Lb -> Jpsi p K, Jpsi->dimuon

print("event type", decay_descriptor)
if decay_descriptor not in [
        30000000, 18112001, 24142001, 28142001, 12143001, 28144041, 11114100,
        13144011, 11144008, 12143001, 12245021, 15144001
]:
    print("Unknown evttype")
    exit()

# FT decoder version to use

# set the input files and the detector conditions
input_files = []
inputFileType = 'ROOT'
DDDBTag = ""
CONDDBTag = ""

# output file name
outputfile_name = "test.dst"

if decay_descriptor == 30000000:

    #input_files = [
    #    # MinBias 30000000
    #    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/30000000/LDST
    #    'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00069155/0000/00069155_00000878_2.ldst'
    #]
    #
    #DDDBTag = 'dddb-20171126'
    #CONDDBTag = 'sim-20171127-vc-md100'
    #inputFileType = 'ROOT'
    #outputfile_name = "MinBias_30000000_HLT2.dst"

    # HLT1-filtered
    input_files = [
        # MinBias 30000000
        # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/30000000/LDST
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000001_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000002_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000003_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000004_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000005_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000006_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000007_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000008_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000010_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000012_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000014_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000016_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000017_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000018_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000019_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000020_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000021_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000022_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000023_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000025_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000026_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000028_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000029_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000030_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000031_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000032_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000033_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000034_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000035_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000036_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000037_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000038_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000039_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000040_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000041_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000043_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000044_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000045_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000047_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000048_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000049_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000050_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000051_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000052_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000054_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000055_1.ldst',
    ]

    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = "MinBias_30000000_HLT1filtered_HLT2.dst"

elif decay_descriptor == 18112001:
    input_files = [
        # inclusive Upsilon, 18112001
        # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/18112001/LDST
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000016_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000017_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000020_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000024_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000026_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000027_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000028_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000029_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000030_2.ldst",
    ]

    # HLT1-filtered
    #input_files = [
    #    # inclusive Upsilon, 18112001
    #    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/18112001/LDST
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000001_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000002_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000003_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000004_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000005_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000006_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000007_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000008_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000043_1.ldst",
    #]

    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = "inclUpsilon_18112001_HLT2.dst"

elif decay_descriptor == 24142001:

    input_files = [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/inclJpsi/inclJpsi_24142001_BrunelReco.ldst"
    ]

    # HLT1-filtered
    #input_files = [
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/inclJpsi/inclJpsi_24142001_BrunelReco_HLT1filtered.dst"
    #]

    DDDBTag = 'dddb-20171122'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = "ROOT"
    outputfile_name = "inclJpsi_24142001_HLT2.dst"

elif decay_descriptor == 28142001:

    #input_files = [
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Psi2s/Psi2s_28142001_BrunelReco.ldst"
    #]

    # HLT1-filtered
    input_files = [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Psi2s/Psi2s_28142001_BrunelReco_HLT1filtered.dst"
    ]

    DDDBTag = 'dddb-20171122'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = "ROOT"
    outputfile_name = "Psi2s_28142001_HLT2.dst"

#elif decay_descriptor == 12143001:
#
#    #input_files = [
#    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2JpsiK/B2JpsiK_12143001_BrunelReco_1.ldst",
#    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2JpsiK/B2JpsiK_12143001_BrunelReco_2.ldst",
#    #]
#
#    # HL1-filtered
#    input_files = [
#        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2JpsiK/B2JpsiK_12143001_BrunelReco_HLT1filtered.dst",
#    ]
#
#    DDDBTag = 'dddb-20171122'
#    CONDDBTag = 'sim-20171127-vc-md100'
#    inputFileType = "ROOT"
#    outputfile_name = "B2JpsiK_12143001_HLT2.dst"

elif decay_descriptor == 28144041:

    input_files = [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Chic2JpsiMuMu/Chic2JpsiMuMu_28144041_BrunelReco_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Chic2JpsiMuMu/Chic2JpsiMuMu_28144041_BrunelReco_2.ldst",
    ]

    # HLT1-filtered
    #input_files = [
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Chic2JpsiMuMu/Chic2JpsiMuMu_28144041_BrunelReco_HLT1filtered.dst",
    #    ]

    DDDBTag = 'dddb-20171122'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = "ROOT"
    outputfile_name = "ChicJpsiDiMuMu_28144040_HLT2.dst"

elif decay_descriptor == 11114100:

    #input_files = [
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2KsMuMu/B2KsMuMu_11114100_BrunelReco_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2KsMuMu/B2KsMuMu_11114100_BrunelReco_2.ldst",
    #]

    # HLT1-filtered
    input_files = [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2KsMuMu/B2KsMuMu_11114100_BrunelReco_HLT1filtered.dst",
    ]

    DDDBTag = 'dddb-20171122'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = "ROOT"
    outputfile_name = "B2KsMuMu_28144040_HLT2.dst"

elif decay_descriptor == 13144011:

    #input_files = [
    #    # Bs2JpsiPhi, 13144011
    #    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/13144011/LDST
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000011_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000019_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000024_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000035_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000046_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000054_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000068_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000072_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000087_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000093_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000109_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000110_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000111_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000112_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000122_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000125_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000126_2.ldst",
    #    ]

    # HLT1-filtered
    input_files = [
        # Bs2JpsiPhi, 13144011
        # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/13144011/LDST
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000001_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000002_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000003_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000004_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000005_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000006_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000007_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000064_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000068_1.ldst",
    ]

    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = "Bs2JpsiPhi.dst"

elif decay_descriptor == 11144008:
    input_files = [
        # B0->Jpsi rho, Jpsi->dimuon, rho->pipi, 11144008
        # sim+std:/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/11144008/LDST
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000002_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000003_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000004_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000005_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000006_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000007_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000008_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000009_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000010_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000011_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000012_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000015_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000016_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000017_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000018_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000019_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000021_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000024_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000025_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000026_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000027_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000028_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000029_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000030_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000031_1.ldst",
        "root://door06.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000032_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000033_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000034_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000035_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000036_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000037_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076714/0000/00076714_00000038_1.ldst",
    ]

    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = "B2Jpsirho.dst"

elif decay_descriptor == 12143001:
    input_files = [
        # B+ -> Jpsi K+, Jpsi->dimuon, 12143001
        # sim+std:/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/12143001/LDST
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000001_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000002_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000003_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000004_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000006_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000007_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000009_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000010_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000011_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000014_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000015_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000016_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000017_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000018_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000019_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000020_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000021_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000022_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000023_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000024_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000025_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000026_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000027_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000029_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000030_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000031_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000033_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000034_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000036_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000037_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000038_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000039_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000040_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000041_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000042_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076711/0000/00076711_00000043_1.ldst",
    ]

    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = "B2JpsiK.dst"

elif decay_descriptor == 12245021:
    input_files = [
        # sim+std:/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/12143001/LDST
        # B -> Jpsi K pi pi, Jpsi->dimuon, 12245021
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000001_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000002_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000003_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000004_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000005_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000006_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000007_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000008_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000009_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000010_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000011_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000012_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000013_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000014_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000015_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000016_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000017_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000018_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000019_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000020_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000021_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000022_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000023_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000024_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000025_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000026_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000027_1.ldst",
        "root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000028_1.ldst",
        "root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000029_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000030_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000032_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000034_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076710/0000/00076710_00000035_1.ldst",
    ]

    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = "B2JpsiKpipi.dst"

elif decay_descriptor == 15144001:
    input_files = [
        # Lb -> Jpsi p K, Jpsi->dimuon, 15144001
        # sim+std:/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/15144001/LDST
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000001_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000002_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000003_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000006_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000007_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000008_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000009_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000010_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000011_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000012_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000013_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000014_1.ldst",
        "root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/LDST/00076738/0000/00076738_00000015_1.ldst",
        "root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/LDST/00076738/0000/00076738_00000017_1.ldst",
        "root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/LDST/00076738/0000/00076738_00000019_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000021_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000022_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000023_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000024_1.ldst",
        "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000025_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000026_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000027_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000028_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000029_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000030_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000031_1.ldst",
        "root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000032_1.ldst",
        "root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000034_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000035_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000037_1.ldst",
        "root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000038_1.ldst",
        "root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000039_1.ldst",
        "root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/LDST/00076738/0000/00076738_00000040_1.ldst",
    ]

    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = "Lb2JpsipK.dst"

# set the options
options.evt_max = 100
options.input_files = input_files
options.data_type = 'Upgrade'
options.dddb_tag = DDDBTag
options.conddb_tag = CONDDBTag
options.simulation = True
options.input_type = inputFileType
options.input_raw_format = raw_event_format

output_file_fullname = outputdir + "/" + outputfile_name
options.output_type = 'ROOT'
options.output_file = output_file_fullname
options.output_manifest_file = "Hlt2DiMuonLines_test.tck.json"

#def make_lines():
#    return [builder() for builder in all_lines.values()]

public_tools = [stateProvider_with_simplified_geom()]
config = run_moore(options, all_lines, public_tools)
