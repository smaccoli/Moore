###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define HLT2 line for ``Lambda_b0 -> Lambda_c+ pi+``.

With ``Lambda_c+ -> p+ K- pi+``.

An example input file and Moore configuration is also given at the bottom of
this file, so that it can be run as-is.
"""
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction,
                                             reconstruction)

# For code inside files under Hlt2Conf/python/lines you should reference these
# two modules using relative imports:
#
#     from ..standard_particles import make_has_rich_long_kaons
from Hlt2Conf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
)
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Functors import require_all

all_lines = {}


def filter_protons(particles, pvs, pt_min=0. * GeV, mipchi2_min=1, dllp_min=0):
    cut = require_all(
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.PID_P > dllp_min,
    )
    return ParticleFilter(particles, F.FILTER(cut))


def filter_kaons(particles, pvs, pt_min=0. * GeV, mipchi2_min=1, dllk_min=1):
    cut = require_all(
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.PID_K > dllk_min,
    )
    return ParticleFilter(particles, F.FILTER(cut))


def filter_pions(particles, pvs, pt_min=0.5 * GeV, mipchi2_min=9, dllk_max=5):
    cut = require_all(
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.PID_K < dllk_max,
    )
    return ParticleFilter(particles, F.FILTER(cut))


def make_lambdacs(protons,
                  kaons,
                  pions,
                  pvs,
                  two_body_comb_maxdocachi2=9.0,
                  comb_m_min=200 * MeV,
                  comb_m_max=3480 * MeV,
                  comb_pt_min=500 * MeV,
                  comb_maxdoca=1. * mm,
                  vchi2pdof_max=30,
                  bpvvdchi2_min=5):
    two_body_combination_code = F.MAXDOCACHI2CUT(two_body_comb_maxdocachi2)
    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > comb_pt_min,
        F.MAXDOCACUT(comb_maxdoca),
    )
    vertex_code = require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )
    return ParticleCombiner(
        [protons, kaons, pions],
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        Combination12Cut=two_body_combination_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_lambdabs(lcs,
                  pions,
                  pvs,
                  comb_m_min=500 * MeV,
                  comb_m_max=7000 * MeV,
                  comb_pt_min=400 * MeV,
                  comb_maxdoca=1 * mm,
                  vchi2pdof_max=20,
                  bpvvdchi2_min=5):
    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > comb_pt_min,
        F.MAXDOCACUT(comb_maxdoca),
    )
    vertex_code = require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )
    return ParticleCombiner(
        [lcs, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@register_line_builder(all_lines)
def lbtolcpi_lctopkpi_with_extras_line(
        name="Hlt2LbToLcpPim_LcToPpKmPip_with_extras", prescale=1):
    pvs = make_pvs()
    protons = filter_protons(make_has_rich_long_protons(), pvs)
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    lcs = make_lambdacs(protons, kaons, pions, pvs)
    lbs = make_lambdabs(lcs, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [lbs],
        prescale=prescale,
        tagging_particles=True,
        calo_digits=True,
        calo_clusters=True,
        pv_tracks=True,
        track_ancestors=True,
        persistreco=True,
    )


# Moore configuration
from Moore import options, run_moore

# In a normal options file, we would import the line from Hlt2Conf where it is
# defined
# from Hlt2Conf.lines.LbToLcPi import lbtolcpi_lctopkpi_line

# Temporary workaround for TrackStateProvider
from RecoConf.global_tools import stateProvider_with_simplified_geom
public_tools = [stateProvider_with_simplified_geom()]


def all_lines():
    return [lbtolcpi_lctopkpi_with_extras_line()]


options.set_input_and_conds_from_testfiledb('Upgrade_MinBias_LDST')
options.input_raw_format = 4.3
options.evt_max = 100
options.control_flow_file = 'control_flow.gv'
options.data_flow_file = 'data_flow.gv'
options.output_type = "ROOT"
options.output_file = "hlt2_line_test_with_extras.dst"
options.output_manifest_file = 'hlt2_line_test_with_extras.tck.json'

with reconstruction.bind(from_file=False):
    run_moore(options, all_lines, public_tools)
