###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file to test running on data.
"""

from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from Hlt2Conf.lines import all_lines
import re
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
    get_UpgradeGhostId_tool_no_UT)
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClusterDecoder
from RecoConf.hlt1_tracking import (
    make_VeloClusterTrackingSIMD, make_RetinaClusters,
    get_global_measurement_provider, make_velo_full_clusters)

from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.calorimeter_reconstruction import make_digits

options.histo_file = "histos_hlt2_pp_thor_data_2022.root"
options.output_file = "data_hlt2_pp_thor_data_2022.mdf"
options.output_type = "MDF"

options.event_store = 'EvtStoreSvc'


def remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.search(pattern_to_remove, name) is None
    }
    return filtered


# Explicitly remove tracking efficiency lines using the UT.
# Track eff lines use the TrackMasterFitter which used the DetailedMaterialLocator.
# The latter is not supported in DD4HEP and leads to crashes, see https://gitlab.cern.ch/lhcb/Rec/-/issues/429.
to_remove = [
    "Hlt2TrackEff_DiMuon_MuonUT.*", "Hlt2TrackEff_DiMuon_Downstream.*",
    "Hlt2TrackEff_DiMuon_VeloMuon.*", "Hlt2TrackEff_DiMuon_SeedMuon.*"
]


def make_lines():
    trunc_lines = all_lines
    for remove in to_remove:
        trunc_lines = remove_lines(trunc_lines, remove)
    print("Manually removed lines due to DD4HEP: ",
          all_lines.keys() - trunc_lines.keys())
    return [builder() for builder in trunc_lines.values()]


# Note the following lines are commented out as the DD4HEP build would automatically change to the detailed geometry (11/2022)
# from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
# public_tools = [
#     trackMasterExtrapolator_with_simplified_geom(),
#     stateProvider_with_simplified_geom()
# ]
public_tools = []

options.scheduler_legacy_mode = False

with reconstruction.bind(from_file=False),\
     make_light_reco_pr_kf_without_UT.bind(skipRich=False, skipCalo=False, skipMuon=False),\
     make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=8.0),\
     make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=8.0),\
     make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.0),\
     make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=8.0),\
     make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD),\
     get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters),\
     make_muon_hits.bind(geometry_version=3),\
     make_digits.bind(calo_raw_bank=True),\
     make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClusterDecoder),\
     get_global_measurement_provider.bind(velo_hits=make_RetinaClusters),\
     hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT):
    config = run_moore(options, make_lines, public_tools)
