###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running a test if a dst or mdf file contains particles for all fired trigger lines.

Runs over the output of the hlt2_all_lines test, manually running the unpackers
and DecReports decoder, and checks per event that each firing trigger line has
a non-zero number of Particle objects in the expected location.

Takes command-line arguments:

    python hlt2_check_output.py <DST or MDF file> <HLT2 JSON TCK>
"""
import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import (ApplicationMgr, LHCbApp, IODataManager)

from GaudiConf.reading import do_unpacking
from PyConf.application import configured_ann_svc

from GaudiConf.reading import load_manifest


def error(msg):
    print("Hlt2CheckOutput ERROR", msg)


LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)

# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
manifest = load_manifest(sys.argv[2])
algs = do_unpacking(manifest, input_process='Hlt2')

decdecoder = None
for alg in algs:
    if "HltDecReportsDecoder" in alg.getFullName():
        decdecoder = alg

appmgr = ApplicationMgr(TopAlg=algs)
appmgr.ExtSvc += [configured_ann_svc()]

input_file = sys.argv[1]
input_type = "ROOT" if input_file.find(".dst") != -1 else "RAW"
IOHelper(input_type).inputFiles([input_file], clear=True)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True
#this is not used anymore, so commented out for now

# Flag to record whether we saw any events
# The test can't check anything if no event fired
found_events = False
found_child_relations = False
while TES['/Event']:
    print('Checking next event.')

    #TES.dump()
    decRep = TES[str(decdecoder.OutputHltDecReportsLocation)].decReports()

    for name, report in decRep.items():
        if report.decision():
            print('Checking line {}'.format(name))
            prefix = '/Event/HLT2/{}'.format(name[:-len("Decision")])
            container = TES[prefix + '/Particles']
            if not container:
                error("no Particles container in " + prefix + "/Particles")
            if container.size() == 0:
                error("empty Particles container in " + prefix + "/Particles")

            relations = TES[prefix + '/Particle2VertexRelations']
            # The CopyParticles algorithm should ensure P2PV relations are saved for the
            # top-level object (the decay head)
            if not relations:
                error("no Relations container in " + prefix +
                      "/Particle2VertexRelations")
            # Special selection algorithm configuration is required to ensure
            # P2PV relations for the descendents are propagated (they are only
            # created if an object uses PVs in its selection). Here we check
            # that at least some lines have child relations, but don't require
            # it for every line (it's valid for a line not to require PVs to
            # perform its selection).

            if relations.size() > container.size():
                found_child_relations = True
            else:
                print("Not enough relations: relations = ", relations.size(),
                      " container =", container.size())

            found_events = True
    appMgr.run(1)

if not found_events:
    error('ERROR: no positive decisions found')
if not found_child_relations:
    error('ERROR: no child P2PV relations found')
