###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from Moore.lines import Hlt2Line

from GaudiKernel.SystemOfUnits import MeV, GeV

from RecoConf.decoders import default_ft_decoding_version
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction, reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom

from Hlt2Conf.algorithms_thor import ParticleFilter
import Functors as F
from Hlt2Conf.standard_particles import make_has_rich_long_kaons
from PyConf.Algorithms import AdvancedCloneKiller

from Hlt2Conf.standard_particles import make_phi2kk
from Hlt2Conf.lines.bandq.builders import b_hadrons
from Hlt2Conf.lines import charmonium_to_dimuon
# from Hlt2Conf.flavourTagging import make_sameside_tagging_kaons

from PyConf.Algorithms import FunctionalSSKaonTagger

default_ft_decoding_version.global_bind(value=6)


def prefilters():
    return [require_pvs(make_pvs())]


def _make_kaons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def noUT_sameside_tagging_kaons(pvs=make_pvs,
                                p_min=2 * GeV,
                                p_max=200 * GeV,
                                pt_max=10 * GeV,
                                eta_max=5.12,
                                ghostprob_max=0.5):
    selection_code = F.require_all(F.P > p_min, F.P < p_max, F.PT < pt_max,
                                   F.GHOSTPROB < ghostprob_max,
                                   F.ETA < eta_max)
    long_kaons = _make_kaons_with_selection(make_has_rich_long_kaons,
                                            selection_code)

    return AdvancedCloneKiller(InputParticles=[long_kaons])


def default_bs2jpsiphi_line_with_flavourtagging():
    jpsi = charmonium_to_dimuon.make_jpsi()
    phi = make_phi2kk()
    bs2jpsiphi = b_hadrons.make_b_hadron(
        particles=[jpsi, phi], descriptor='B_s0 -> J/psi(1S) phi(1020)')

    pvs = make_pvs()

    ssKaons = noUT_sameside_tagging_kaons()
    # ssKaons = make_sameside_tagging_kaons()
    ssKaonTagging = FunctionalSSKaonTagger(
        BCandidates=bs2jpsiphi, TaggingKaons=ssKaons, PrimaryVertices=pvs)

    return Hlt2Line(
        name="Hlt2Bs2JpsiPhi",
        algs=upfront_reconstruction() + prefilters() + [bs2jpsiphi],
        prescale=1,
        extra_outputs=[("SSKaonFlavourTags", ssKaonTagging),
                       ("SSTaggingKaons", ssKaons)])


def modified_bs2jpsiphi_line_with_flavourtagging():
    jpsi = charmonium_to_dimuon.make_jpsi()
    phi = make_phi2kk()
    bs2jpsiphi = b_hadrons.make_b_hadron(
        m_min=5350 * MeV,
        particles=[jpsi, phi],
        descriptor='B_s0 -> J/psi(1S) phi(1020)')

    pvs = make_pvs()

    ssKaons = noUT_sameside_tagging_kaons()
    # ssKaons = make_sameside_tagging_kaons()
    ssKaonTagging = FunctionalSSKaonTagger(
        BCandidates=bs2jpsiphi, TaggingKaons=ssKaons, PrimaryVertices=pvs)

    return Hlt2Line(
        name="Hlt2B2JpsiKModified",
        algs=upfront_reconstruction() + prefilters() + [bs2jpsiphi],
        prescale=1,
        extra_outputs=[("SSKaonFlavourTags", ssKaonTagging),
                       ("SSTaggingKaons", ssKaons)])


options.input_files = [
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00143551/0000/00143551_00000004_1.xdigi'
]

options.input_type = 'ROOT'
options.evt_max = 100
options.first_evt = 0
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'
options.geometry_version = 'trunk'
options.conditions_version = 'master'
options.input_raw_format = 0.3

options.output_file = 'hlt2_bs2jpsiphi_flavourtagging_sskaontagger.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "hlt2_bs2jpsiphi_flavourtagging_sskaontagger.tck.json"


def make_lines():
    return [
        default_bs2jpsiphi_line_with_flavourtagging(),
        modified_bs2jpsiphi_line_with_flavourtagging()
    ]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
