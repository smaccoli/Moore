###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from Moore.lines import Hlt2Line

from GaudiKernel.SystemOfUnits import MeV, GeV

from RecoConf.decoders import default_ft_decoding_version
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction, reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom

from Hlt2Conf.lines.bandq.builders import charged_hadrons, b_hadrons
from Hlt2Conf.lines import charmonium_to_dimuon

from Hlt2Conf.algorithms_thor import ParticleFilter
import Functors as F
from PyConf.Algorithms import AdvancedCloneKiller
from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions, make_has_rich_long_protons,
    make_long_electrons_with_brem, make_ismuon_long_muon)

from Hlt2Conf.flavourTagging import (
    # make_sameside_tagging_pions, make_sameside_tagging_protons,
    # make_oppositeside_vertex_charge_tagging_particles,
    make_oppositeside_tagging_kaons,
    make_oppositeside_tagging_electrons,
    make_oppositeside_tagging_muons)

from PyConf.Algorithms import (
    FunctionalSSPionTagger, FunctionalSSProtonTagger, FunctionalOSKaonTagger,
    FunctionalOSElectronTagger, FunctionalOSMuonTagger,
    FunctionalOSVertexChargeTagger, FlavourTagsMerger)

default_ft_decoding_version.global_bind(value=6)


def prefilters():
    return [require_pvs(make_pvs())]


def _make_pions_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_protons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_electrons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_muons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def noUT_sameside_tagging_pions(pvs=make_pvs,
                                p_min=2 * GeV,
                                p_max=200 * GeV,
                                pt_min=0.4 * GeV,
                                pt_max=10 * GeV,
                                eta_max=5.12,
                                pidp_max=5,
                                pidk_max=5,
                                ghostprob_max=0.5):
    selection_code = F.require_all(
        F.P > p_min, F.P < p_max, F.PT > pt_min, F.PT < pt_max,
        F.PID_P < pidp_max, F.PID_K < pidk_max, F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max)

    long_pions = _make_pions_with_selection(make_has_rich_long_pions,
                                            selection_code)

    return AdvancedCloneKiller(InputParticles=[long_pions])


def noUT_sameside_tagging_protons(pvs=make_pvs,
                                  p_min=2 * GeV,
                                  p_max=200 * GeV,
                                  pt_min=0.4 * GeV,
                                  pt_max=10 * GeV,
                                  eta_max=5.12,
                                  pidp_min=5,
                                  ghostprob_max=0.5):
    selection_code = F.require_all(
        F.P > p_min, F.P < p_max, F.PT > pt_min, F.PT < pt_max,
        F.PID_P > pidp_min, F.GHOSTPROB < ghostprob_max, F.ETA < eta_max)
    long_protons = _make_protons_with_selection(make_has_rich_long_protons,
                                                selection_code)

    return AdvancedCloneKiller(InputParticles=[long_protons])


def noUT_oppositeside_vertex_charge_tagging_particles(pvs=make_pvs,
                                                      p_min=2 * GeV,
                                                      p_max=200 * GeV,
                                                      pt_max=10 * GeV,
                                                      eta_max=5.12,
                                                      ghostprob_max=0.5):
    selection_code = F.require_all(F.P > p_min, F.P < p_max, F.PT < pt_max,
                                   F.GHOSTPROB < ghostprob_max,
                                   F.ETA < eta_max)
    long_pions = _make_pions_with_selection(make_has_rich_long_pions,
                                            selection_code)
    long_muons = _make_muons_with_selection(make_ismuon_long_muon,
                                            selection_code)
    long_electrons = _make_electrons_with_selection(
        make_long_electrons_with_brem, selection_code)

    return AdvancedCloneKiller(
        InputParticles=[long_pions, long_muons, long_electrons])


def b2jpsik_with_flavourtagging(b2jpsik, name):
    pvs = make_pvs()

    ssPions = noUT_sameside_tagging_pions()
    # ssPions = make_sameside_tagging_pions()
    ssPionTagging = FunctionalSSPionTagger(
        BCandidates=b2jpsik, TaggingPions=ssPions, PrimaryVertices=pvs)

    ssProtons = noUT_sameside_tagging_protons()
    # ssProtons = make_sameside_tagging_protons()
    ssProtonTagging = FunctionalSSProtonTagger(
        BCandidates=b2jpsik, TaggingProtons=ssProtons, PrimaryVertices=pvs)

    osKaons = make_oppositeside_tagging_kaons()
    osKaonTagging = FunctionalOSKaonTagger(
        BCandidates=b2jpsik, TaggingKaons=osKaons, PrimaryVertices=pvs)

    osElectrons = make_oppositeside_tagging_electrons()
    osElectronTagging = FunctionalOSElectronTagger(
        BCandidates=b2jpsik, TaggingElectrons=osElectrons, PrimaryVertices=pvs)

    osMuons = make_oppositeside_tagging_muons()
    osMuonTagging = FunctionalOSMuonTagger(
        BCandidates=b2jpsik, TaggingMuons=osMuons, PrimaryVertices=pvs)

    osVertexParticles = noUT_oppositeside_vertex_charge_tagging_particles()
    # osVertexParticles = make_oppositeside_vertex_charge_tagging_particles()
    osVertexChargeTagging = FunctionalOSVertexChargeTagger(
        BCandidates=b2jpsik,
        TaggingParticles=osVertexParticles,
        PrimaryVertices=pvs)

    flavourTags = FlavourTagsMerger(FlavourTagsIterable=[
        ssPionTagging, ssProtonTagging, osKaonTagging, osElectronTagging,
        osMuonTagging, osVertexChargeTagging
    ])

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [b2jpsik],
        prescale=1,
        extra_outputs=[("SSTaggingPions", ssPions),
                       ("SSTaggingProtons", ssProtons),
                       ("OSTaggingKaons", osKaons),
                       ("OSTaggingElectron", osElectrons),
                       ("OSTaggingMuon", osMuons),
                       ("OSVertexChargeParticles", osVertexParticles),
                       ("FlavourTags", flavourTags)])


def default_b2jpsik_line_with_flavourtagging():
    jpsi = charmonium_to_dimuon.make_jpsi()
    kaons = charged_hadrons.make_detached_kaons()
    b2jpsik = b_hadrons.make_b_hadron(
        particles=[jpsi, kaons], descriptor='[B+ -> J/psi(1S) K+]cc')

    return b2jpsik_with_flavourtagging(b2jpsik, "Hlt2B2JpsiK")


def modified_b2jpsik_line_with_flavourtagging():
    jpsi = charmonium_to_dimuon.make_jpsi()
    kaons = charged_hadrons.make_detached_kaons()
    b2jpsik = b_hadrons.make_b_hadron(
        m_min=5300 * MeV,
        particles=[jpsi, kaons],
        descriptor='[B+ -> J/psi(1S) K+]cc')

    return b2jpsik_with_flavourtagging(b2jpsik, "Hlt2B2JpsiKModified")


options.input_files = [
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00143567/0000/00143567_00000001_1.xdigi'
]

options.input_type = 'ROOT'
options.evt_max = 100
options.first_evt = 0
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'
options.input_raw_format = 0.3

options.output_file = 'hlt2_b2jpsik_flavourtagging.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "hlt2_b2jpsik_flavourtagging.tck.json"


def make_lines():
    return [
        default_b2jpsik_line_with_flavourtagging(),
        modified_b2jpsik_line_with_flavourtagging()
    ]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
