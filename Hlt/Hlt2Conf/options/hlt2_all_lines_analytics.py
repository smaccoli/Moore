###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Run with
`./run gaudirun.py hlt2_all_lines_analytics.py`

Performs event size analytics on HLT2 events by exploiting the fact that each
stream gets its own instance of `CombineRawBankViewsToRawEvent`.

-Make each HLT2 line its own stream.
-Report size of DstData RawBank (contains the physics and reconstruction objects) and
 total event size where the event contains the RawBanks defined in `banks` in this options file
-Will not produce output files.
-In log will see StatCounter with name of line

LineName...    INFO Number of counters : X
 |    Counter                                      |     #     |    sum | mean/eff^* | rms/err^*  |     min   |     max    |
 | "DstData bank size (bytes)"                     |         X |      Y |         YY |     YYY    |      YYYY |      YYYYY |
 | "Event size (bytes)"                            |         X |      Z |         ZZ |     ZZZ    |      ZZZZ |      ZZZZZ |


Note that by running `RawEventCombiner` in VERBOSE mode - achieved through
`RawEventCombiner.bind(OutputLevel=1)` one also has the log of the sizes of ALL RawBanks in `banks`.

Runs over MinBias data.
If you do not see this for your line then your line did not fire on any event in the input data.

Eventually we should have a test like this over each stream as that will define `banks` and hence the appropriate
event size. At the moment this script runs over ALL HLT2 lines and analyses ALL RawBanks so the event size contains
everything.

"""

from Moore import options, run_moore
from PyConf.Algorithms import RawEventSimpleCombiner
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from PyConf.utilities import ConfigurationError

from Hlt2Conf.lines import all_lines
from Hlt2Conf.lines.b_to_charmonia import all_lines as b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import all_lines as b2oc_lines
from Hlt2Conf.lines.bandq import all_lines as bandq_lines
from Hlt2Conf.lines.charm import all_lines as charm_lines
from Hlt2Conf.lines.qee import all_lines as qee_lines
from Hlt2Conf.lines.rd import all_lines as rd_lines
from Hlt2Conf.lines.semileptonic import all_lines as sl_lines

from Moore.streams_hlt2 import DETECTOR_RAW_BANK_TYPES, HLT1_REPORT_RAW_BANK_TYPES
import Moore.streams_hlt2

import re
import logging
log = logging.getLogger()

########################
WGs = {
    "b2oc": b2oc_lines,
    "bandq": bandq_lines,
    "b2cc": b2cc_lines,
    "sl": sl_lines,
    "charm": charm_lines,
    "qee": qee_lines,
    "rd": rd_lines,
    "all": all_lines
}

##CHANGE wg to desired
wg = 'all'
########################
if wg not in WGs.keys():
    raise ConfigurationError(f'Working group ({wg}) not recognised.')

options.monitoring_file = "monitoring.json"

# Create new dictionary based on `hlt2_lines` with "readable" line names:
# Hlt2Charm_DstpToD0Pip_D0ToKsKs_DDDD -> Charm_DstpToD0Pip_D0ToKsKs_DDDD
hlt2_lines = WGs[wg]
streamdict = {}
for k, v in hlt2_lines.items():
    name = re.search('Hlt2(.+?)$', k).group(1)
    streamdict[name] = [v]


def make_streams():
    "Makes each line its own stream"

    lines = {}
    for k, v in streamdict.items():
        lines[k] = [v[0]()]

    return lines


##Must declare "new" streams (one for each line) and the RawBanks you wish to analyse
banks = set.union(DETECTOR_RAW_BANK_TYPES, HLT1_REPORT_RAW_BANK_TYPES)
Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM = {
    k: [i for i in banks]
    for k in streamdict.keys()
}

public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=False), RawEventSimpleCombiner.bind(
        OutputLevel=4):
    config = run_moore(options, make_streams, public_tools, analytics=True)
