###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using reco from file. Produces hlt2_testpersistreco_fromfile.dst

Run like any other options file:

    ./Moore/run gaudirun.py tests/options/hlt_dst_output.py hlt2_persistreco_fromfile.py
    or
    ./Moore/run gaudirun.py tests/options/hlt_mdf_output.py hlt2_persistreco_fromfile.py
"""
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs as make_pvs
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_particles import make_has_rich_long_kaons
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, micrometer as um
import Functors as F
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Functors import require_all

###Define 2 orthoganol lines (PIDK cut) with and without persistreco


def make_charm_kaons(pidk_cut):
    """Return kaons maker for D0 decay selection.
    """
    pvs = make_pvs()
    code = require_all(
        F.MINIPCUT(IPCut=60 * um, Vertices=pvs), F.PT > 800 * MeV,
        F.P > 5 * GeV, pidk_cut)
    return ParticleFilter(make_has_rich_long_kaons(), F.FILTER(code))


def make_dzeros(particle1, particle2, name, descriptor):
    """Return D0 maker with selection tailored for two-body hadronic final
    states.

    Args:
        particles (list of DataHandles): Input particles used in the
                                         combination.
        name (string): Combiner name for identification.
        descriptor (string): Decay descriptor to be reconstructed.
    """
    pvs = make_pvs()
    combination_cut = require_all(F.MASS > 1685 * MeV, F.MASS < 2045 * MeV,
                                  F.PT > 2 * GeV,
                                  F.MAX(F.PT) > 1000 * MeV,
                                  F.MAXDOCACUT(0.1 * mm))
    vertex_cut = require_all(F.MASS > 1715 * MeV, F.MASS < 2015 * MeV,
                             F.CHI2DOF < 10.,
                             F.BPVFDCHI2(pvs) > 25.,
                             F.BPVDIRA(pvs) > 0.99985)

    return ParticleCombiner([particle1, particle2],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cut)


def test_persistreco_line(name='Hlt2_test_persistreco',
                          prescale=1,
                          persistreco=True):
    kaons = make_charm_kaons(pidk_cut=F.PID_K < 15)
    dzeros = make_dzeros(
        particle1=kaons,
        particle2=kaons,
        name='Charm_D0ToHH_D0ToKmKp_persistreco',
        descriptor='D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dzeros],
        prescale=prescale,
        persistreco=persistreco)


def test_nopersistreco_line(name='Hlt2_test_nopersistreco',
                            prescale=1,
                            persistreco=False):
    kaons = make_charm_kaons(pidk_cut=F.PID_K > 15)
    dzeros = make_dzeros(
        particle1=kaons,
        particle2=kaons,
        name='Charm_D0ToHH_D0ToKmKp_nopersistreco',
        descriptor='D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dzeros],
        prescale=prescale,
        persistreco=persistreco)


input_files = [
    # evttype 27163002
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000033_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000038_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000040_2.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000047_2.ldst'
]

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 4.3
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = 700
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'
options.geometry_version = 'trunk'
options.conditions_version = 'master'

#following options are set in tests/options/hlt_dst_output.py or hlt_mdf_out.py
# options.output_file = 'hlt2_test_persistreco_fromfile.mdf'
# options.output_type = 'ROOT'

# This options file is used with different output types; subsequent tests
# require TCKs for each case
if options.output_type == "ROOT":
    options.output_manifest_file = "hlt2_persistreco_fromfile_root_output.tck.json"
elif options.output_type == "MDF":
    options.output_manifest_file = "hlt2_persistreco_fromfile_mdf_output.tck.json"


def make_lines():
    return [test_persistreco_line(), test_nopersistreco_line()]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=True):
    config = run_moore(options, make_lines, public_tools)
