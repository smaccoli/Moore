###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Maker functions for probe tracks used for tracking efficiency studies.
"""

from Hlt2Conf.standard_particles import (
    _make_particles, standard_protoparticle_filter, get_all_track_selector,
    make_long_pions)
from PyConf import configurable
import Functors as F
from RecoConf.hlt2_probetracks import (make_charged_downstream,
                                       make_charged_seed, make_VeloMuon_tracks,
                                       make_muonut_particles)


@configurable
def make_ismuon_longmatch():
    """ creates LHCb::Particles to match long track to probe track  """
    with standard_protoparticle_filter.bind(Code=F.ISMUON):
        return make_long_pions()


@configurable
def make_muonut_muons():
    """ make LHCb::Particles from LHCb::ProtoParticles of muonut tracks  """
    return _make_particles(
        species="muon",
        make_protoparticles=make_muonut_particles,
        get_track_selector=get_all_track_selector)


@configurable
def make_downstream_muons():
    """ creates LHCb::Particles from LHCb::ProtoParticles of downstream tracks  """
    with standard_protoparticle_filter.bind(Code=F.ISMUON):
        return _make_particles(
            species="muon",
            make_protoparticles=make_charged_downstream,
            get_track_selector=get_all_track_selector)


@configurable
def make_velomuon_muons():
    """ creates LHCb::Particles from LHCb::ProtoParticles of velomuon tracks  """
    return _make_particles(
        species="muon",
        make_protoparticles=make_VeloMuon_tracks,
        get_track_selector=get_all_track_selector)


@configurable
def make_seed_muons():
    """ creates LHCb::Particles from LHCb::ProtoParticles of seed tracks  """
    return _make_particles(
        species="muon",
        make_protoparticles=make_charged_seed,
        get_track_selector=get_all_track_selector)
