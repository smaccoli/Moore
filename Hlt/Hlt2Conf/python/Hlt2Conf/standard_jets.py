###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Maker functions for jet definitions common across HLT2.
"""
from math import pi as M_PI

from GaudiKernel.SystemOfUnits import GeV

from PyConf.Algorithms import (ParticleMakerForParticleFlow, FastJetBuilder,
                               ParticleFlowMaker, JetTag, ParticleFlowFilter)

from .standard_particles import (get_long_track_selector,
                                 get_down_track_selector,
                                 standard_protoparticle_filter)

from PyConf import configurable

from .algorithms_thor import ParticleFilter
import Functors as F

from RecoConf.reconstruction_objects import (make_charged_protoparticles as
                                             _make_charged_protoparticles,
                                             make_pvs_v1 as _make_pvs)


@configurable
def make_pf_particles(make_protoparticles=_make_charged_protoparticles,
                      get_track_selector=get_long_track_selector,
                      make_protoparticle_filter=standard_protoparticle_filter,
                      c_over_e_cut=0):
    chargedProtos = make_protoparticles()
    return ParticleMakerForParticleFlow(
        InputProtoParticles=chargedProtos,
        TrackPredicate=get_track_selector(),
        c_over_e_cut=c_over_e_cut,
        ProtoParticlePredicate=make_protoparticle_filter()).Output


@configurable
def tag_jets(jets, tags, useflightdirection=False, name="TagJets_{hash}"):
    return JetTag(
        Jets=jets,
        Tags=tags,
        UseFlightDirection=useflightdirection,
        PVLocation=_make_pvs(),
        name=name).Output


@configurable
def build_jets(pflow,
               JetsByVtx=False,
               MCJets=False,
               applyJEC=False,
               name='JetBuilder_{hash}'):
    jetEcFilePath = ""  # Null jetEcFilePath: Don't apply JEC
    if applyJEC:
        jetEcFilePath = "paramfile://data/JetEnergyCorrections_R05_hlt_Run2.root"
    return FastJetBuilder(
        Input=pflow,
        PVLocation=_make_pvs(),
        PtMin=5000,  #  FastJet: min pT
        RParameter=0.5,  #  FastJet: cone size
        Sort='by_pt',  #  FastJet: sort by pt
        Strategy='Best',  #  FastJet: strategy:
        Type='antikt_algorithm',  #  FastJet: JetAlgortihm
        Recombination='E_scheme',  #  FastJet: RecombinationScheme
        JetID=98,  # LHCb: Jet PID number
        JetsByVtx=JetsByVtx,
        MCJets=MCJets,  # Reconstruct jets from particles from MCParticles
        jetEcFilePath=jetEcFilePath,
        jecLimNPvs=[0, 1],
        jecLimEta=[2.0, 2.2, 2.3, 2.4, 2.6, 2.8, 3.0, 3.2, 3.6, 4.2, 4.5],
        jecLimCpf=[0.06, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0001],
        jecLimPhi=[0, 1.0 / 6.0 * M_PI, 1.0 / 3.0 * M_PI, 0.5 * M_PI],
        jecLimPt=[5, 298],
        jetEcShift=0.,
        name=name).Output


@configurable
def make_trackjets(name='TrackJets',
                   pt_min=10 * GeV,
                   JetsByVtx=False,
                   tags=None,
                   useflightdirectionfortag=False):
    pflow = make_onlytrack_particleflow()
    jets = build_jets(pflow, JetsByVtx, name='JetBuilder' + name)

    if tags is not None:
        taggedjets = tag_jets(
            jets,
            tags,
            useflightdirection=useflightdirectionfortag,
            name="TagJet" + name)
        jets = taggedjets

    code = F.require_all(F.IS_ABS_ID("CELLjet"), F.PT > pt_min)
    return ParticleFilter(jets, F.FILTER(code), name=name)


@configurable
def particleflow_filter(pflow, toban, name="PFFilter"):
    return ParticleFlowFilter(
        Inputs=pflow, ParticlesToBan=toban, name=name).Output


def make_onlytrack_particleflow(name='PFTracksOnly_{hash}'):
    return ParticleFlowMaker(
        Inputs=[
            make_pf_particles(get_track_selector=get_long_track_selector),
            make_pf_particles(get_track_selector=get_down_track_selector)
        ],
        name=name).Output
