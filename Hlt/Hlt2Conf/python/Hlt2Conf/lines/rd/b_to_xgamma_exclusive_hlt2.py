###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of exclusive radiative  B -> X gamma Hlt2 lines:
- B_s0 -> ( phi(1020) -> K K ) gamma
- B0 -> (K*(892) -> K pi ) gamma

author: Izaac Sanderswood
date: 25.05.22

"""
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import GeV, MeV
import Functors as F
from math import cos

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_detached_kstar0s, make_rd_detached_phis, make_rd_photons)

from Hlt2Conf.lines.rd.builders.b_to_xgamma_exclusive_builders import (
    make_b2xgamma_excl)

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES

from Hlt2Conf.lines.rd.builders.flavour_tagging_particles import extra_outputs_for_FlavourTagging

all_lines = {}


@register_line_builder(all_lines)
def bs_to_phigamma_line(name="Hlt2RD_BsToPhiGamma",
                        persistreco=True,
                        prescale=1.):

    pvs = make_pvs()
    # remember to also look at the definition of make_rd_detached_phis as only some cuts are changed here
    phis = make_rd_detached_phis(
        name="rd_BsToPhiGamma_detached_phis",
        k_p_min=2. * GeV,
        k_pt_min=500. * MeV,
        k_ipchi2_min=11.,
        phi_pt_min=1500. * MeV,
        vchi2pdof_max=15.)

    photons = make_rd_photons(
        et_min=2. * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2)

    b_s0 = make_b2xgamma_excl(
        intermediate=phis,
        photons=photons,
        pvs=pvs,
        descriptor="B_s0 -> phi(1020) gamma",
        dira_min=cos(0.1),
        name="rd_BsToPhiGamma_Combiner")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [phis, b_s0],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        extra_outputs=extra_outputs_for_FlavourTagging(b_s0, persistreco),
    )


@register_line_builder(all_lines)
def bd_to_kstgamma_line(name="Hlt2RD_BdToKstGamma",
                        persistreco=True,
                        prescale=0.75):

    pvs = make_pvs()

    # remember to also look at the definition of make_rd_detached_kstar0s as only some cuts are changed here
    kst = make_rd_detached_kstar0s(
        name="rd_BdToKstGamma_detached_kstar0s",
        pi_p_min=2. * GeV,
        pi_pt_min=500. * MeV,
        k_p_min=3. * GeV,
        k_pt_min=500. * MeV,
        k_ipchi2_min=11.,
        pi_ipchi2_min=11.,
        k_pid=(F.PID_K > 0.),
        kstar0_pt_min=1500. * MeV,
        vchi2pdof_max=15.,
        am_min=705. * MeV,
        am_max=1085. * MeV,
    )

    photons = make_rd_photons(
        et_min=2. * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2)

    b0 = make_b2xgamma_excl(
        intermediate=kst,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        dira_min=cos(0.045),
        name="rd_BdToKstGamma_Combiner")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kst, b0],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        extra_outputs=extra_outputs_for_FlavourTagging(b0, persistreco),
    )
