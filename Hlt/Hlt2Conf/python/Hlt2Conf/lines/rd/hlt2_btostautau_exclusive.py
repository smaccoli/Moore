###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of b -> s tau tau HLT2 lines.
Final states built are (all taus decay to muons and neutrinos):
 1. Bs decays to phi
     - Bs -> phi(-> K+ K-) tau+ tau-
     - Bs -> phi(-> K+ K+) tau+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K-) tau+ tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) tau+ tau- with a fake kaon
     - Bs -> phi(-> K+ K-) tau+ tau- with a fake muon
 2. Lb decays to pK
     - Lb -> Lambda(-> p+ K-) tau+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K+) tau+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) tau+ tau+, Lambda(-> p+ K-) tau- tau- and their charge conjugates
     - Lb -> Lambda(-> p+ K-) tau+ tau- with a fake kaon
     - Lb -> Lambda(-> p+ K-) tau+ tau- with a fake proton
     - Lb -> Lambda(-> K+ K-) tau+ tau- with a fake muon
 3. Bd decays to K*
     - Bd -> K*(-> K+ pi-) tau+ tau-
     - Bd -> K*(-> K+ pi+) tau+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) tau+ tau+, K*(-> K+ pi-) tau- tau- and their charge conjugates
     - Bd -> K*(-> K+ pi-) tau+ tau- with a fake kaon
     - Bd -> K*(-> K+ pi-) tau+ tau- with a fake pion
     - Bd -> K*(-> K+ pi-) tau+ tau- with a fake muon
 4. Bs decays to K* K*
     - Bs -> K*(-> K+ pi-) K~*(-> K- pi+) tau+ tau-
     - Bs -> K*(-> K+ pi-) K*(-> K+ pi-) tau+ tau- and its charge conjugate
     - Bs -> K*(-> K+ pi-) K~*(-> K- pi+) tau+ tau+ and its charge conjugate
     - Bs -> K*(-> K+ pi-) K~*(-> K- pi+) tau+ tau- with a fake muon
 5. B+ decays to K+
     - B+ -> K+ tau+ tau-
     - B+ -> K+ tau- tau- and its charge conjugate
     - B+ -> K+ tau+ tau+ and its charge conjugate
     - B+ -> K+ tau+ tau- with a fake muon
     - B+ -> K+ tau+ tau- with a fake kaon
Note a: fake particles are obtained through reversing the PID requirements
Note b: muons from the taus are combined with a ParticleCombiner with DecayDescriptor [D0 -> mu+ mu-]cc
Note c: in general, the dihadron is mainly used as a way to add more cuts on the particle combination, and is not intended to represent a given particle (hence the loose mass requirements)
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import MeV, GeV

import Functors as F

from Hlt2Conf.lines.rd.builders import rdbuilder_thor
from Hlt2Conf.lines.rd.builders import btostautau_exclusive

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES

all_lines = {}

kwargs_kst_bs2kstkst = {
    "comb_pt_min": 600 * MeV,
    "vchi2pdof_max": 6,
    "bpvfdchi2_min": 0
}

kwargs_kaons = {
    "p_min": 3 * GeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_K > 4, ~F.ISMUON)
}
kwargs_kaons_reverse_pid = {
    "p_min": 3 * GeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_K < 4)
}

kwargs_kaons_for_bu = {
    "p_min": 10 * GeV,
    "pt_min": 1000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": F.require_all(F.PID_K > 5, ~F.ISMUON)
}
kwargs_kaons_for_bu_reverse_pid = {
    "p_min": 10 * GeV,
    "pt_min": 1000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": (F.PID_K < 5)
}

kwargs_kaons_for_kstar = {
    "p_min": 3 * GeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all((1 - F.PROBNN_PI) * F.PROBNN_K > 0.5, ~F.ISMUON)
}
kwargs_kaons_for_kstar_reverse_pid = {
    "p_min": 3 * GeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": ((1 - F.PROBNN_PI) * F.PROBNN_K < 0.5)
}

kwargs_pions = {
    "p_min": 0 * GeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PROBNN_PI > 0.5, ~F.ISMUON)
}
kwargs_pions_reverse_pid = {
    "p_min": 0 * GeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PROBNN_PI < 0.5)
}
kwargs_protons = {
    "p_min": 0 * GeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_P > 4, ~F.ISMUON)
}
kwargs_protons_reverse_pid = {
    "p_min": 0 * GeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_P < 4)
}
kwargs_muons = {
    "p_min": 0 * GeV,
    "pt_min": 0 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.ISMUON, F.PID_MU > 0.)
}
kwargs_muons_reverse_pid = {
    "p_min": 0 * GeV,
    "pt_min": 0 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_MU < 0.)
}


@register_line_builder(all_lines)
def bstophitautau_tautomu_line(name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToMu',
                               prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phis = btostautau_exclusive.make_phi(kaons, kaons, pvs)
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bs = btostautau_exclusive.make_bs(phis, dimuons, pvs)
    algs = rd_prefilter() + [dimuons, bs]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophitautau_tautomu_same_sign_kaons_line(
        name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToMu_SSK', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phis = btostautau_exclusive.make_phi(
        kaons,
        kaons,
        pvs,
        decay_descriptor="[phi(1020) -> K+ K+]cc",
        name='rd_same_sign_dikaons_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bs = btostautau_exclusive.make_bs(
        phis,
        dimuons,
        pvs,
        name='rd_make_bs_to_kktautau_same_sign_kaons_{hash}')
    algs = rd_prefilter() + [dimuons, bs]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophitautau_tautomu_same_sign_muons_line(
        name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToMu_SSmu', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phis = btostautau_exclusive.make_phi(kaons, kaons, pvs)
    dimuons = btostautau_exclusive.make_dimuon(
        muons,
        muons,
        pvs,
        decay_descriptor="[D0 -> mu+ mu+]cc",
        name='rd_same_sign_dimuons_for_btostautau_{hash}')
    bs = btostautau_exclusive.make_bs(
        phis,
        dimuons,
        pvs,
        name='rd_make_bs_to_kktautau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [dimuons, bs]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophitautau_tautomu_fakemuon_line(
        name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToMu_FakeMuon', prescale=0.04):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    phis = btostautau_exclusive.make_phi(kaons, kaons, pvs)
    dimuons = btostautau_exclusive.make_dimuon(
        muons, fake_muons, pvs, name='rd_fake_dimuons_for_btostautau_{hash}')
    bs = btostautau_exclusive.make_bs(
        phis, dimuons, pvs, name='rd_make_bs_to_kktautau_fake_muons_{hash}')
    algs = rd_prefilter() + [dimuons, phis, bs]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophitautau_tautomu_fakekaon_line(
        name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToMu_FakeKaon', prescale=0.2):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_reverse_pid)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phis = btostautau_exclusive.make_phi(
        kaons, fake_kaons, pvs, name='rd_fake_dikaons_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bs = btostautau_exclusive.make_bs(
        phis, dimuons, pvs, name='rd_make_bs_to_kktautau_fake_kaons_{hash}')
    algs = rd_prefilter() + [dimuons, phis, bs]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautomu_line(name='Hlt2RD_LbToPKTauTau_TauToMu', prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pks = btostautau_exclusive.make_lst(protons, kaons, pvs)
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    lb = btostautau_exclusive.make_lb(pks, dimuons, pvs)
    algs = rd_prefilter() + [dimuons, pks, lb]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautomu_same_sign_pK_line(
        name='Hlt2RD_LbToPKTauTau_TauToMu_SSpK', prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pks = btostautau_exclusive.make_lst(
        protons,
        kaons,
        pvs,
        decay_descriptor="[Lambda(1520)0 -> p+ K+]cc",
        name='rd_same_sign_pk_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    lb = btostautau_exclusive.make_lb(
        pks, dimuons, pvs, name='rd_make_lb_to_pktautau_same_sign_pk_{hash}')
    algs = rd_prefilter() + [dimuons, pks, lb]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautomu_same_sign_muons_sspmu_line(
        name='Hlt2RD_LbToPKTauTau_TauToMu_SSmu_SSpmu', prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pks = btostautau_exclusive.make_lst(protons, kaons, pvs)
    dimuons = btostautau_exclusive.make_dimuon(
        muons,
        muons,
        pvs,
        decay_descriptor="[D0 -> mu+ mu+]cc",
        name='rd_same_sign_dimuons_for_btostautau_{hash}')
    lb = btostautau_exclusive.make_lb(
        pks,
        dimuons,
        pvs,
        name='rd_make_lb_to_pktautau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [dimuons, pks, lb]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautomu_same_sign_muons_ospmu_line(
        name='Hlt2RD_LbToPKTauTau_TauToMu_SSmu_OSpmu', prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pks = btostautau_exclusive.make_lst(protons, kaons, pvs)
    dimuons = btostautau_exclusive.make_dimuon(
        muons,
        muons,
        pvs,
        decay_descriptor="[D0 -> mu- mu-]cc",
        name='rd_same_sign_dimuons_for_btostautau_{hash}')
    lb = btostautau_exclusive.make_lb(
        pks,
        dimuons,
        pvs,
        name='rd_make_lb_to_pktautau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [dimuons, pks, lb]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautomu_fakemuon_line(
        name='Hlt2RD_LbToPKTauTau_TauToMu_FakeMuon', prescale=0.08):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    pks = btostautau_exclusive.make_lst(protons, kaons, pvs)
    dimuons = btostautau_exclusive.make_dimuon(
        muons, fake_muons, pvs, name='rd_fake_dimuons_for_btostautau_{hash}')
    lb = btostautau_exclusive.make_lb(
        pks, dimuons, pvs, name='rd_make_lb_to_pktautau_fake_muons_{hash}')
    algs = rd_prefilter() + [dimuons, pks, lb]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautomu_fakeproton_line(
        name='Hlt2RD_LbToPKTauTau_TauToMu_FakeProton', prescale=0.2):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(
        **kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pks = btostautau_exclusive.make_lst(
        protons, kaons, pvs, name='rd_pk_fake_proton_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    lb = btostautau_exclusive.make_lb(
        pks, dimuons, pvs, name='rd_make_lb_to_pktautau_fake_protons_{hash}')
    algs = rd_prefilter() + [dimuons, pks, lb]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautomu_fakekaon_line(
        name='Hlt2RD_LbToPKTauTau_TauToMu_FakeKaon', prescale=0.4):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pks = btostautau_exclusive.make_lst(
        protons, kaons, pvs, name='rd_pk_fake_kaon_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    lb = btostautau_exclusive.make_lb(
        pks, dimuons, pvs, name='rd_make_lb_to_pktautau_fake_kaons_{hash}')
    algs = rd_prefilter() + [dimuons, pks, lb]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautomu_line(name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToMu',
                               prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    ksts = btostautau_exclusive.make_kst(kaons, pions, pvs)
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bd = btostautau_exclusive.make_bd(ksts, dimuons, pvs)
    algs = rd_prefilter() + [dimuons, ksts, bd]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautomu_same_sign_Kpi_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToMu_SSKpi', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        decay_descriptor="[K*(892)0 -> K+ pi+]cc",
        name='rd_same_sign_kpi_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bd = btostautau_exclusive.make_bd(
        ksts,
        dimuons,
        pvs,
        name='rd_make_bd_to_kpitautau_same_sign_kpi_{hash}')
    algs = rd_prefilter() + [dimuons, ksts, bd]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautomu_same_sign_muons_sskmu_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToMu_SSmu_SSKmu', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    ksts = btostautau_exclusive.make_kst(kaons, pions, pvs)
    dimuons = btostautau_exclusive.make_dimuon(
        muons,
        muons,
        pvs,
        decay_descriptor="[D0 -> mu+ mu+]cc",
        name='rd_same_sign_dimuons_for_btostautau_{hash}')
    bd = btostautau_exclusive.make_bd(
        ksts,
        dimuons,
        pvs,
        name='rd_make_bd_to_kpitautau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [dimuons, ksts, bd]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautomu_same_sign_muons_oskmu_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToMu_SSmu_OSKmu', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    ksts = btostautau_exclusive.make_kst(kaons, pions, pvs)
    dimuons = btostautau_exclusive.make_dimuon(
        muons,
        muons,
        pvs,
        decay_descriptor="[D0 -> mu- mu-]cc",
        name='rd_same_sign_dimuons_for_btostautau_{hash}')
    bd = btostautau_exclusive.make_bd(
        ksts,
        dimuons,
        pvs,
        name='rd_make_bd_to_kpitautau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [dimuons, ksts, bd]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautomu_fakemuon_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToMu_FakeMuon', prescale=0.05):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    ksts = btostautau_exclusive.make_kst(kaons, pions, pvs)
    dimuons = btostautau_exclusive.make_dimuon(
        muons, fake_muons, pvs, name='rd_fake_dimuons_for_btostautau_{hash}')
    bd = btostautau_exclusive.make_bd(
        ksts, dimuons, pvs, name='rd_make_bd_to_kpitautau_fake_muons_{hash}')
    algs = rd_prefilter() + [dimuons, ksts, bd]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautomu_fakekaon_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToMu_FakeKaon', prescale=0.4):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    ksts = btostautau_exclusive.make_kst(
        kaons, pions, pvs, name='rd_kpi_fake_kaon_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bd = btostautau_exclusive.make_bd(
        ksts, dimuons, pvs, name='rd_make_bd_to_kpitautau_fake_kaons_{hash}')
    algs = rd_prefilter() + [dimuons, ksts, bd]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautomu_fakepion_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToMu_FakePion', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    ksts = btostautau_exclusive.make_kst(
        kaons, pions, pvs, name='rd_kpi_fake_pion_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bd = btostautau_exclusive.make_bd(
        ksts, dimuons, pvs, name='rd_make_bd_to_kpitautau_fake_pions_{hash}')
    algs = rd_prefilter() + [dimuons, ksts, bd]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstokstksttautau_tautomu_line(
        name='Hlt2RD_BsToKstKstTauTau_KstToKPi_TauToMu', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        **kwargs_kst_bs2kstkst,
        name='rd_kpi_kstkst_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bs = btostautau_exclusive.make_bs_to_kstkst(ksts, dimuons, pvs)
    algs = rd_prefilter() + [dimuons, ksts, bs]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstokstksttautau_tautomu_same_sign_kstar_line(
        name='Hlt2RD_BsToKstKstTauTau_KstToKPi_TauToMu_SSKst', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        **kwargs_kst_bs2kstkst,
        name='rd_kpi_kstkst_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bs = btostautau_exclusive.make_bs_to_kstkst(
        ksts,
        dimuons,
        pvs,
        decay_descriptor="[B_s0 -> K*(892)0 K*(892)0 D0]cc",
        name='rd_make_bs_to_kstksttautau_same_sign_kst_{hash}')
    algs = rd_prefilter() + [dimuons, ksts, bs]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstokstksttautau_tautomu_same_sign_muons_line(
        name='Hlt2RD_BsToKstKstTauTau_KstToKPi_TauToMu_SSmu', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        **kwargs_kst_bs2kstkst,
        name='rd_kpi_kstkst_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(
        muons,
        muons,
        pvs,
        decay_descriptor="[D0 -> mu+ mu+]cc",
        name='rd_same_sign_dimuons_for_btostautau_{hash}')
    bs = btostautau_exclusive.make_bs_to_kstkst(
        ksts,
        dimuons,
        pvs,
        decay_descriptor="[B_s0 -> K*(892)0 K*(892)~0 D0]cc",
        name='rd_make_bs_to_kstksttautau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [dimuons, ksts, bs]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstokstksttautau_tautomu_fakemuon_line(
        name='Hlt2RD_BsToKstKstTauTau_KstToKPi_TauToMu_FakeMuon',
        prescale=0.7):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        **kwargs_kst_bs2kstkst,
        name='rd_kpi_kstkst_for_btostautau_{hash}')
    dimuons = btostautau_exclusive.make_dimuon(
        muons, fake_muons, pvs, name='rd_fake_dimuons_for_btostautau_{hash}')
    bs = btostautau_exclusive.make_bs_to_kstkst(
        ksts,
        dimuons,
        pvs,
        name='rd_make_bs_to_kstksttautau_fake_muons_{hash}')
    algs = rd_prefilter() + [dimuons, ksts, bs]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautomu_line(name='Hlt2RD_BuToKTauTau_TauToMu', prescale=0.5):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bu = btostautau_exclusive.make_bu(kaons, dimuons, pvs)
    algs = rd_prefilter() + [dimuons, bu]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautomu_same_sign_muons_oskmu_line(
        name='Hlt2RD_BuToKTauTau_TauToMu_SSmu_OSKmu', prescale=0.7):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    dimuons = btostautau_exclusive.make_dimuon(
        muons,
        muons,
        pvs,
        decay_descriptor="[D0 -> mu- mu-]cc",
        name='rd_same_sign_dimuons_for_btostautau_{hash}')
    bu = btostautau_exclusive.make_bu(
        kaons,
        dimuons,
        pvs,
        name='rd_make_bu_to_ktautau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [dimuons, bu]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautomu_same_sign_muons_sskmu_line(
        name='Hlt2RD_BuToKTauTau_TauToMu_SSmu_SSKmu', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    dimuons = btostautau_exclusive.make_dimuon(
        muons,
        muons,
        pvs,
        decay_descriptor="[D0 -> mu+ mu+]cc",
        name='rd_same_sign_dimuons_for_btostautau_{hash}')
    bu = btostautau_exclusive.make_bu(
        kaons,
        dimuons,
        pvs,
        name='rd_make_bu_to_ktautau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [dimuons, bu]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautomu_fakemuon_line(
        name='Hlt2RD_BuToKTauTau_TauToMu_FakeMuon', prescale=0.05):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    dimuons = btostautau_exclusive.make_dimuon(
        muons, fake_muons, pvs, name='rd_fake_dimuons_for_btostautau_{hash}')
    bu = btostautau_exclusive.make_bu(
        kaons, dimuons, pvs, name='rd_make_bu_to_ktautau_fake_muon_{hash}')
    algs = rd_prefilter() + [dimuons, bu]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautomu_fakekaon_line(
        name='Hlt2RD_BuToKTauTau_TauToMu_FakeKaon', prescale=0.2):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    dimuons = btostautau_exclusive.make_dimuon(muons, muons, pvs)
    bu = btostautau_exclusive.make_bu(
        fake_kaons,
        dimuons,
        pvs,
        name='rd_make_bu_to_ktautau_fake_kaon_{hash}')
    algs = rd_prefilter() + [dimuons, bu]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)
