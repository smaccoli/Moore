###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 lines for radiative b-decays
Author: Miguel Rebollo

- Xi_b- -> Xi- gamma
  Hlt2RD_XibmToXimGamma_XimToL0Pim_LLL
- Omega_b- -> Omega- gamma
  Hlt2RD_OmegabmToOmegamGamma_OmegamToLambda0Km_LLL
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Hlt2Conf.lines.rd.builders.baryonic_builders import (
    make_xim_to_lambda_pi_lll,
    make_omegam_to_lambda_k_lll,
)

from Hlt2Conf.lines.rd.builders.baryonic_radiative_builders import (
    make_rd_xibm,
    make_rd_obm,
)

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_photons

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _VRD_MONITORING_VARIABLES

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs

all_lines = {}


@register_line_builder(all_lines)
def xibtoxigamma_xitolpi_lll_line(name='Hlt2RD_XibmToXimGamma_XimToL0Pim_LLL',
                                  prescale=1):
    pvs = make_pvs()

    xim = make_xim_to_lambda_pi_lll(
        name='rd_xim_lll',
        pvs=pvs,
        comb_m_min=1277 * MeV,
        comb_m_max=1367 * MeV,
        m_min=1297 * MeV,
        m_max=1347 * MeV,
        comb_p_min=9. * GeV,
        p_min=20. * GeV,
        docachi2_max=25.,
        vchi2pdof_max=9.,
        bpvvdz_min=1 * mm,
        bpvdira_min=0.95,
        lambda_mass_window=30. * MeV,
        lambda_vchi2pdof_max=6.,
        lambda_bpvvdchi2_min=4.,
        lambda_pt_min=1. * GeV,
        pion_p_min=0.,
        pion_pt_min=200. * MeV,
        pion_mipchi2dvprimary_min=9.,
        pion_PID=None,
    )
    photons = make_rd_photons(
        e_min=5 * GeV,
        et_min=2 * GeV,
        IsNotH_min=0.6,
    )

    xibm = make_rd_xibm(
        xim,
        photons,
        pvs,
        name='xibm_lll',
        comb_mass_window=1000. * MeV,
        mass_window=800. * MeV,
        pt_min=1.5 * GeV,
        p_min=30 * GeV,
        sum_pt_min=5. * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xim, xibm],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def obtoomgamma_omtol0k_omtol0k_lll_line(
        name='Hlt2RD_OmegabmToOmegamGamma_OmegamToLambda0Km_LLL', prescale=1):
    pvs = make_pvs()

    omegam = make_omegam_to_lambda_k_lll(
        name='rd_om_lll',
        pvs=pvs,
        comb_m_min=1622 * MeV,
        comb_m_max=1722 * MeV,
        m_min=1642 * MeV,
        m_max=1702 * MeV,
        comb_p_min=9. * GeV,
        p_min=9.5 * GeV,
        docachi2_max=25.,
        vchi2pdof_max=9.,
        bpvvdz_min=0.5 * mm,
        bpvdira_min=0.95,
        lambda_mass_window=35. * MeV,
        lambda_vchi2pdof_max=9.,
        lambda_bpvvdchi2_min=4.,
        lambda_pt_min=1. * GeV,
        kaon_p_min=5. * GeV,
        kaon_pt_min=400. * MeV,
        kaon_mipchi2dvprimary_min=9.,
        kaon_PID=(F.PID_K > 6.),
    )
    photons = make_rd_photons(
        e_min=5 * GeV,
        et_min=2 * GeV,
    )

    omegabm = make_rd_obm(
        omegam,
        photons,
        pvs,
        name='obm_lll',
        comb_mass_window=1200. * MeV,
        mass_window=1000. * MeV,
        pt_min=1.5 * GeV,
        p_min=30 * GeV,
        sum_pt_min=5. * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omegam, omegabm],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)
