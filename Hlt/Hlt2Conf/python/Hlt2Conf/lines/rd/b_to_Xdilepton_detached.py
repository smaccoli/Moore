###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B0 -> K*0 DarkBoson (->ll) and B+ -> K+ Darkboson(->ll), ll = mumu, ee, mue for LongLong  leptons.


Authors: Martin Andersson, Eluned Smith

"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from .builders.rd_prefilters import rd_prefilter, _VRD_MONITORING_VARIABLES
from GaudiKernel.SystemOfUnits import MeV

from RecoConf.reconstruction_objects import make_pvs
from PyConf import configurable

from .builders import b_to_hemu_builders
from .builders import rdbuilder_thor

all_lines = {}


### mumu ###
@register_line_builder(all_lines)
@configurable
def bdtokstLLP_LLPtomumu_line(name="Hlt2RD_BdToKstLLP_LLPToMuMu",
                              prescale=1,
                              persistreco=False):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        parent_id='KS0', pidmu_muon_min=3.)

    kst = rdbuilder_thor.make_rd_detached_kstar0s()
    bd = b_to_hemu_builders.make_btohemu(
        dileptons=dimuons,
        hadrons=kst,
        pvs=pvs,
        decay_descriptor="[B0 -> KS0 K*(892)0]cc",
        dira_min=0.9999,
        fdchi2_min=121,
        m_min=4500 * MeV)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, kst, bd],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def butokpLLP_LLPtomumu_line(name="Hlt2RD_BuToKpLLP_LLPToMuMu",
                             prescale=1,
                             persistreco=False):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        parent_id='KS0', pidmu_muon_min=3.)

    kaons = rdbuilder_thor.make_rd_detached_kaons()
    bu = b_to_hemu_builders.make_btohemu(
        dileptons=dimuons,
        hadrons=kaons,
        pvs=pvs,
        decay_descriptor="[B+ -> KS0 K+]cc",
        dira_min=0.9999,
        fdchi2_min=121,
        m_min=4500 * MeV)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, bu],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


### ee ###
@register_line_builder(all_lines)
@configurable
def bdtokstLLP_LLPtoee_line(name="Hlt2RD_BdToKstLLP_LLPToEE",
                            prescale=1,
                            persistreco=False):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        parent_id='KS0', pid_e_min=3.)

    kst = rdbuilder_thor.make_rd_detached_kstar0s()
    bd = b_to_hemu_builders.make_btohemu(
        dileptons=dielectrons,
        hadrons=kst,
        pvs=pvs,
        decay_descriptor="[B0 -> KS0 K*(892)0]cc",
        dira_min=0.9999,
        fdchi2_min=121,
        m_min=4500 * MeV)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, kst, bd],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def butokpLLP_LLPtoee_line(name="Hlt2RD_BuToKpLLP_LLPToEE",
                           prescale=1,
                           persistreco=False):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        parent_id='KS0', pid_e_min=3.)

    kaons = rdbuilder_thor.make_rd_detached_kaons()
    bu = b_to_hemu_builders.make_btohemu(
        dileptons=dielectrons,
        hadrons=kaons,
        pvs=pvs,
        decay_descriptor="[B+ -> KS0 K+]cc",
        dira_min=0.9999,
        fdchi2_min=121,
        m_min=4500 * MeV)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, bu],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


### mu e ###
@register_line_builder(all_lines)
@configurable
def bdtokstLLP_LLPtomue_line(name="Hlt2RD_BdToKstLLP_LLPToMuE",
                             prescale=1,
                             persistreco=False):
    pvs = make_pvs()
    mues = rdbuilder_thor.make_rd_detached_mue(
        min_PIDe=3., min_PIDmu=3., parent_id='KS0')

    kst = rdbuilder_thor.make_rd_detached_kstar0s()
    bd = b_to_hemu_builders.make_btohemu(
        dileptons=mues,
        hadrons=kst,
        pvs=pvs,
        decay_descriptor="[B0 -> KS0 K*(892)0]cc",
        dira_min=0.9999,
        fdchi2_min=121,
        m_min=4500 * MeV)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [mues, kst, bd],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def butokpLLP_LLPtomue_line(name="Hlt2RD_BuToKpLLP_LLPToMuE",
                            prescale=1,
                            persistreco=False):
    pvs = make_pvs()
    mues = rdbuilder_thor.make_rd_detached_mue(
        min_PIDe=3., min_PIDmu=3., parent_id='KS0')

    kaons = rdbuilder_thor.make_rd_detached_kaons()
    bu = b_to_hemu_builders.make_btohemu(
        dileptons=mues,
        hadrons=kaons,
        pvs=pvs,
        decay_descriptor="[B+ -> KS0 K+]cc",
        dira_min=0.9999,
        fdchi2_min=121,
        m_min=4500 * MeV)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [mues, bu],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)
