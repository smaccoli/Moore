###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of Radiative inclusive Lines.

It contains the following lines (see descriptors below)
- Hlt2RD_BToHHGamma_Incl

The lines below are working in suboptimal mode and are thus not yet registered
to the all_lines dictionary. We feed a constant value to a BDT input since we
don't have the needed MINTREE functor available.

- Hlt2RD_BToHHHGamma_Incl
- Hlt2RD_BToHHGamma_GammaToEE_Incl
- Hlt2RD_BToHHHGamma_GammaToEE_Incl

Further work will be done in order to retrain the BDTs with available functors,
with samples including a wider variety of decays and with run3 data when
available.

author: Alejandro Alfonso, Aniol Lobo
date: 08.04.2022

This lines first combine 2 hadrons, where at least one of them must be charged.
In HHH lines the result is combined with a 3rd hadron, which is always neutral
except when the 2-hadron system is (K+ K-) in order to avoid repeating final
states. However, we allow repeating final states when two neutrals are present
to keep the list of combinations simpler.
The result is combined in hh(h)g lines with a high-pt photon to build a B
candidate and with a "converted" photon, which is built from an
electron-positron pair with small invariant mass, in hh(h)g_g_to_ee lines.
The b candidates are then given to a BDT to further reduce the output rate.
These radiative inclusive lines are inclusive in the sense that no PID is used
when selecting the hadrons, and no mass requirements are applied either to the
K*, D*, or B candidates
"""

from Moore.config import HltLine, register_line_builder
from Hlt2Conf.standard_particles import (
    make_long_electrons_no_brem,
    make_long_kaons,
    make_photons,
)
from RecoConf.reconstruction_objects import make_pvs

from .builders import (
    rd_prefilters,
    rdbuilder_thor,
    rad_incl_builder,
    b_tmva_builder,
)

all_lines = {}

from Hlt2Conf.lines.rd.builders.flavour_tagging_particles import extra_outputs_for_FlavourTagging

make_KsLL = rdbuilder_thor.make_rd_ks0_lls
make_LambdaLL = rdbuilder_thor.make_rd_lambda_lls
make_merged_pi0s = rdbuilder_thor.make_rd_merged_pi0s
make_resolved_pi0s = rdbuilder_thor.make_rd_resolved_pi0s


@register_line_builder(all_lines)
def BToHHGamma_Incl_line(name="Hlt2RD_BToHHGamma_Incl",
                         prescale=1,
                         persistreco=True):
    """Definition of B->HHgamma HLT2 line.

    Final states built are (omitting intermediate resonances):

    1. B0 -> K+ K- gamma
    2. [B+ -> K+ KS0 gamma]cc
    3. [B+ -> K+ Lambda0 gamma]cc
    4. [B+ -> K+ Lambda~0 gamma]cc

    This line first combines 2 hadrons, where at least one of them must be charged.
    The result is combined with a high-pt photon to build a B candidate.
    The b candidates are then given to a BDT to further reduce the output rate.
    These radiative inclusive lines are inclusive in the sense that no PID is used
    when selecting the hadrons, and no mass requirements are applied either to the
    K*, D*, or B candidates
    """
    pvs = make_pvs()

    charged_hadrons = rad_incl_builder.filter_basic_hadrons(
        make_long_kaons(), pvs)
    Ks0 = rad_incl_builder.filter_neutral_hadrons(make_KsLL(), pvs)
    Lambda = rad_incl_builder.filter_neutral_hadrons(make_LambdaLL(), pvs)

    photons = rad_incl_builder.filter_photons(make_photons(), pvs)

    kstars = rad_incl_builder.make_hh([
        [charged_hadrons, charged_hadrons],
        [charged_hadrons, Ks0],
        [Lambda, charged_hadrons],
        [Lambda, charged_hadrons],
    ], pvs, [
        "K*(892)0 -> K+ K-",
        "[K*(892)+ -> K+ KS0]cc",
        "[K*(892)+ -> Lambda0 K+]cc",
        "[K*(892)- -> Lambda0 K-]cc",
    ])
    presel_b = rad_incl_builder.make_presel_b(
        [kstars, photons], pvs,
        ["B0 -> K*(892)0 gamma", "[B+ -> K*(892)+ gamma]cc"])
    b = b_tmva_builder.make_b2(presel_b, pvs, "HHgamma", 0.22)

    return HltLine(
        name=name,
        algs=rd_prefilters.rd_prefilter() + [photons, kstars, b],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=rd_prefilters._RD_MONITORING_VARIABLES,
        extra_outputs=extra_outputs_for_FlavourTagging(b, persistreco),
    )


@register_line_builder(all_lines)
def BToHHHGamma_Incl_line(name="Hlt2RD_BToHHHGamma_Incl",
                          prescale=1,
                          persistreco=True):
    """Definition of B->HHHgamma HLT2 line.

    Final states built are (omitting intermediate resonances):

    1.  [B+ -> (K+ K-) K+ gamma]cc
    2.  B0 -> (K+ K-) KS0 gamma
    3.  [B0 -> (K+ K-) Lambda0 gamma]cc
    4.  [B+ -> (K+ KS0) KS0 gamma]cc
    5.  [B+ -> (K+ KS0) Lambda0 gamma]cc
    6.  [B+ -> (K+ KS0) Lambda~0 gamma]cc
    7.  [B+ -> (Lambda0 K+) KS0 gamma]cc       (same final state as 5.)
    8.  [B+ -> (Lambda0 K+) Lambda0 gamma]cc
    9.  [B+ -> (Lambda0 K+) Lambda~0 gamma]cc
    10. [B+ -> (Lambda~0 K+) KS0 gamma]cc      (same final state as 6.)
    11. [B+ -> (Lambda~0 K+) Lambda0 gamma]cc  (same final state as 9.)
    12. [B+ -> (Lambda~0 K+) Lambda~0 gamma]cc

    This line first combines 2 hadrons in the same manner as the HHgamma lines,
    where at least one of them must be charged.
    The result is combined with a 3rd hadron, which is always neutral except when
    the 2-hadron system is (K+ K-) in order to avoid repeating final states.
    However, we allow repeating final states when two neutrals are present to keep
    the list of combinations simpler.
    The result is combined with a high-pt photon to build a B candidate.
    The b candidates are then given to a BDT to further reduce the output rate.
    These radiative inclusive lines are inclusive in the sense that no PID is used
    when selecting the hadrons, and no mass requirements are applied either to the
    K*, D*, or B candidates
    """
    pvs = make_pvs()

    charged_hadrons = rad_incl_builder.filter_basic_hadrons(
        make_long_kaons(), pvs)
    Ks0 = rad_incl_builder.filter_neutral_hadrons(make_KsLL(), pvs)
    Lambda = rad_incl_builder.filter_neutral_hadrons(make_LambdaLL(), pvs)
    last_charged_hadrons = rad_incl_builder.filter_third_hadrons(
        make_long_kaons(), pvs)
    photons = rad_incl_builder.filter_photons(make_photons(), pvs)
    kstars = rad_incl_builder.make_hh([
        [charged_hadrons, charged_hadrons],
        [charged_hadrons, Ks0],
        [charged_hadrons, Lambda],
        [charged_hadrons, Lambda],
    ], pvs, [
        "K*(892)0 -> K+ K-",
        "[K*(892)+ -> K+ KS0]cc",
        "[K*(892)+ -> K+ Lambda0]cc",
        "[K*(892)- -> K- Lambda0]cc",
    ])

    dstars = rad_incl_builder.make_hhh([
        [kstars, last_charged_hadrons],
        [kstars, last_charged_hadrons],
        [kstars, Ks0],
        [kstars, Ks0],
        [kstars, Lambda],
        [kstars, Lambda],
        [kstars, Lambda],
        [kstars, Lambda],
    ], pvs, [
        'D*(2010)+ -> K*(892)0 K+',
        'D*(2010)- -> K*(892)0 K-',
        'D*(2007)0 -> K*(892)0 KS0',
        '[D*(2010)+ -> K*(892)+ KS0]cc',
        'D*(2007)0 -> K*(892)0 Lambda0',
        'D*(2007)~0 -> K*(892)0 Lambda~0',
        '[D*(2010)+ -> K*(892)+ Lambda0]cc',
        '[D*(2010)+ -> K*(892)+ Lambda~0]cc',
    ])
    presel_b = rad_incl_builder.make_presel_b(
        [dstars, photons], pvs,
        ["[B+ -> D*(2010)+ gamma]cc", "[B0 -> D*(2007)0 gamma]cc"])
    b = b_tmva_builder.make_b2(presel_b, pvs, "HHHgamma", 0.21)

    return HltLine(
        name=name,
        algs=rd_prefilters.rd_prefilter() + [photons, kstars, dstars, b],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=rd_prefilters._RD_MONITORING_VARIABLES,
        extra_outputs=extra_outputs_for_FlavourTagging(b, persistreco),
    )


@register_line_builder(all_lines)
def BToHHGamma_GammaToEE_Incl_line(name="Hlt2RD_BToHHGamma_GammaToEE_Incl",
                                   prescale=1,
                                   persistreco=True):
    """Definition of B->HHgammaEE HLT2 line.

    Final states built are (omitting intermediate resonances):

    1. B0 -> K+ K- (gamma -> e+ e-)
    2. [B+ -> K+ KS0 (gamma -> e+ e-)]cc
    3. [B+ -> K+ Lambda0 (gamma -> e+ e-)]cc
    4. [B+ -> K+ Lambda~0 (gamma -> e+ e-)]cc

    This line first combines 2 hadrons, where at least one of them must be charged.
    The result is combined with a "converted" photon, which is built from an
    electron-positron pair with small invariant mass, to build a B candidate.
    The b candidates are then given to a BDT to further reduce the output rate.
    These radiative inclusive lines are inclusive in the sense that no PID is used
    when selecting the hadrons, and no mass requirements are applied either to the
    K*, D*, or B candidates
    """
    pvs = make_pvs()

    electrons = rad_incl_builder.filter_electrons(
        make_long_electrons_no_brem(), pvs)
    charged_hadrons = rad_incl_builder.filter_basic_hadrons(
        make_long_kaons(), pvs)
    Ks0 = rad_incl_builder.filter_neutral_hadrons(make_KsLL(), pvs)
    Lambda = rad_incl_builder.filter_neutral_hadrons(make_LambdaLL(), pvs)

    photons = rad_incl_builder.make_gamma_ee(electrons, pvs)

    kstars = rad_incl_builder.make_hh([
        [charged_hadrons, charged_hadrons],
        [charged_hadrons, Ks0],
        [charged_hadrons, Lambda],
        [charged_hadrons, Lambda],
    ], pvs, [
        "K*(892)0 -> K+ K-",
        "[K*(892)+ -> K+ KS0]cc",
        "[K*(892)+ -> K+ Lambda0]cc",
        "[K*(892)- -> K- Lambda0]cc",
    ])
    presel_b = rad_incl_builder.make_presel_b(
        [kstars, photons], pvs,
        ["B0 -> K*(892)0 gamma", "[B+ -> K*(892)+ gamma]cc"])
    b = b_tmva_builder.make_b2(presel_b, pvs, "HHgammaEE", 0.14)

    return HltLine(
        name=name,
        algs=rd_prefilters.rd_prefilter() + [photons, kstars, b],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=rd_prefilters._RD_MONITORING_VARIABLES,
        extra_outputs=extra_outputs_for_FlavourTagging(b, persistreco),
    )


@register_line_builder(all_lines)
def BToHHHGamma_GammaToEE_Incl_line(name="Hlt2RD_BToHHHGamma_GammaToEE_Incl",
                                    prescale=1,
                                    persistreco=True):
    """Definition of B->HHHgammaEE HLT2 line.

    Final states built are (omitting intermediate resonances):

    1.  [B+ -> (K+ K-) K+ (gamma -> e+ e-)]cc
    2.  B0 -> (K+ K-) KS0 (gamma -> e+ e-)
    3.  [B0 -> (K+ K-) Lambda0 (gamma -> e+ e-)]cc
    4.  [B+ -> (K+ KS0) KS0 (gamma -> e+ e-)]cc
    5.  [B+ -> (K+ KS0) Lambda0 (gamma -> e+ e-)]cc
    6.  [B+ -> (K+ KS0) Lambda~0 (gamma -> e+ e-)]cc
    7.  [B+ -> (Lambda0 K+) KS0 (gamma -> e+ e-)]cc       (same final state as 5.)
    8.  [B+ -> (Lambda0 K+) Lambda0 (gamma -> e+ e-)]cc
    9.  [B+ -> (Lambda0 K+) Lambda~0 (gamma -> e+ e-)]cc
    10. [B+ -> (Lambda~0 K+) KS0 (gamma -> e+ e-)]cc      (same final state as 6.)
    11. [B+ -> (Lambda~0 K+) Lambda0 (gamma -> e+ e-)]cc  (same final state as 9.)
    12. [B+ -> (Lambda~0 K+) Lambda~0 (gamma -> e+ e-)]cc

    This line first combines 2 hadrons in the same manner as the HHgamma lines,
    where at least one of them must be charged.
    The result is combined with a 3rd hadron, which is always neutral except when
    the 2-hadron system is (K+ K-) in order to avoid repeating final states.
    However, we allow repeating final states when two neutrals are present to keep
    the list of combinations simpler.
    The result is combined with a "converted" photon, which is built from an
    electron-positron pair with small invariant mass, to build a B candidate.
    The b candidates are then given to a BDT to further reduce the output rate.
    These radiative inclusive lines are inclusive in the sense that no PID is used
    when selecting the hadrons, and no mass requirements are applied either to the
    K*, D*, or B candidates
    """
    pvs = make_pvs()

    electrons = rad_incl_builder.filter_electrons(
        make_long_electrons_no_brem(), pvs)

    charged_hadrons = rad_incl_builder.filter_basic_hadrons(
        make_long_kaons(), pvs)
    Ks0 = rad_incl_builder.filter_neutral_hadrons(make_KsLL(), pvs)
    Lambda = rad_incl_builder.filter_neutral_hadrons(make_LambdaLL(), pvs)
    last_charged_hadrons = rad_incl_builder.filter_third_hadrons(
        make_long_kaons(), pvs)
    photons = rad_incl_builder.make_gamma_ee(electrons, pvs)
    kstars = rad_incl_builder.make_hh([
        [charged_hadrons, charged_hadrons],
        [charged_hadrons, Ks0],
        [charged_hadrons, Lambda],
        [charged_hadrons, Lambda],
    ], pvs, [
        "K*(892)0 -> K+ K-",
        "[K*(892)+ -> K+ KS0]cc",
        "[K*(892)+ -> K+ Lambda0]cc",
        "[K*(892)- -> K- Lambda0]cc",
    ])

    dstars = rad_incl_builder.make_hhh([
        [kstars, last_charged_hadrons],
        [kstars, last_charged_hadrons],
        [kstars, Ks0],
        [kstars, Ks0],
        [kstars, Lambda],
        [kstars, Lambda],
        [kstars, Lambda],
        [kstars, Lambda],
    ], pvs, [
        'D*(2010)+ -> K*(892)0 K+',
        'D*(2010)- -> K*(892)0 K-',
        'D*(2007)0 -> K*(892)0 KS0',
        '[D*(2010)+ -> K*(892)+ KS0]cc',
        'D*(2007)0 -> K*(892)0 Lambda0',
        'D*(2007)~0 -> K*(892)0 Lambda~0',
        '[D*(2010)+ -> K*(892)+ Lambda0]cc',
        '[D*(2010)+ -> K*(892)+ Lambda~0]cc',
    ])

    presel_b = rad_incl_builder.make_presel_b(
        [dstars, photons], pvs,
        ["[B+ -> D*(2010)+ gamma]cc", "[B0 -> D*(2007)0 gamma]cc"])
    b = b_tmva_builder.make_b2(presel_b, pvs, "HHHgammaEE", 0.14)

    return HltLine(
        name=name,
        algs=rd_prefilters.rd_prefilter() + [photons, kstars, dstars, b],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=rd_prefilters._RD_MONITORING_VARIABLES,
        extra_outputs=extra_outputs_for_FlavourTagging(b, persistreco),
    )
