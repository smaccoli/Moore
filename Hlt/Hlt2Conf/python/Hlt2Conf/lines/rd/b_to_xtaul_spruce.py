###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''                                                                                       
Author: Tommaso Fulghesu
Contact: tommaso.fulghesu@cern.ch
Date: 07/12/2021                                                        
'''

######################################################################################
####                                                                              ####
#### The following exlusive lines Yb -> X tau (-> 3pi nu_tau) l cover the decays: ####
####                                                                              ####
#### B0 -> K* (-> K pi) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                   ####
#### B0 -> K* (-> K pi) tau (-> 3pi nu_tau) mu                                    ####
#### B0 -> K* (-> K pi) tau (-> 3pi nu_tau) e                                     ####
#### B0 -> phi (-> K K) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                   ####
#### B0 -> phi (-> K K) tau (-> 3pi nu_tau) mu                                    ####
#### B0 -> phi (-> K K) tau (-> 3pi nu_tau) e                                     ####
#### B0 -> rho (-> pi pi) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                 ####
#### B0 -> rho (-> pi pi) tau (-> 3pi nu_tau) mu                                  ####
#### B0 -> rho (-> pi pi) tau (-> 3pi nu_tau) e                                   ####
#### B0 -> eta' (-> pi pi gamma) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)          ####
#### B0 -> eta' (-> pi pi gamma) tau (-> 3pi nu_tau) mu                           ####
#### B0 -> eta' (-> pi pi gamma) tau (-> 3pi nu_tau) e                            ####
#### B0 -> Ks (-> pi pi) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                  ####
#### B0 -> Ks (-> pi pi) tau (-> 3pi nu_tau) mu                                   ####
#### B0 -> Ks (-> pi pi) tau (-> 3pi nu_tau) e                                    ####
#### Bu -> K+ tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                             ####
#### Bu -> K+ tau (-> 3pi nu_tau) mu                                              ####
#### Bu -> K+ tau (-> 3pi nu_tau) e                                               ####
#### Bu -> K1(1270)+ (-> K pi pi) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)         ####
#### Bu -> K1(1270)+ (-> K pi pi) tau (-> 3pi nu_tau) mu                          ####
#### Bu -> K1(1270)+ (-> K pi pi) tau (-> 3pi nu_tau) e                           ####
#### Lb -> K p tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                            ####
#### Lb -> K p tau (-> 3pi nu_tau) mu                                             ####
#### Lb -> K p tau (-> 3pi nu_tau) e                                              ####
#### Lb -> Lambda (-> pi p) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)               ####
#### Lb -> Lambda (-> pi p) tau (-> 3pi nu_tau) mu                                ####
#### Lb -> Lambda (-> pi p) tau (-> 3pi nu_tau) e                                 ####
####                                                                              ####
######################################################################################

from Moore.config import register_line_builder
from Moore.lines import SpruceLine
from PyConf import configurable
from GaudiKernel.SystemOfUnits import MeV, GeV
from Hlt2Conf.lines.rd.builders import b_to_xtaul_rd_builder as builder
from Hlt2Conf.lines.rd.builders import rdbuilder_thor as common_builder
from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter
import Functors as F

sprucing_lines = {}


## B0 -> K* tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KstTauTau_OS_ExclusiveLine(
        name='SpruceRD_BdToKstTauTau_KstToKPi_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    kst = common_builder.make_rd_detached_kstar0s(
        pi_p_min=2 * GeV,
        pi_pt_min=250 * MeV,
        k_p_min=2 * GeV,
        k_pt_min=250 * MeV,
        pi_ipchi2_min=9.,
        k_ipchi2_min=9.,
        pi_pid=F.PID_K < 0,
        k_pid=F.PID_K > 0,
        am_min=700 * MeV,
        am_max=1100 * MeV,
        kstar0_pt_min=0.5 * GeV,
        adocachi2cut=20.,
        vchi2pdof_max=15.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[B0 -> J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKstTauTau_KstToKPi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KstTauTau_SS_ExclusiveLine(
        name='SpruceRD_BdToKstTauTau_KstToKPi_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) (tau+->pi-pi-pi+) + CC
    
              and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    kst = common_builder.make_rd_detached_kstar0s(
        pi_p_min=2 * GeV,
        pi_pt_min=250 * MeV,
        k_p_min=2 * GeV,
        k_pt_min=250 * MeV,
        pi_ipchi2_min=9.,
        k_ipchi2_min=9.,
        pi_pid=F.PID_K < 0,
        k_pid=F.PID_K > 0,
        am_min=700 * MeV,
        am_max=1100 * MeV,
        kstar0_pt_min=0.5 * GeV,
        adocachi2cut=20.,
        vchi2pdof_max=15.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[B0 ->  J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKstTauTau_KstToKPi_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KstTauMu_OS_ExclusiveLine(
        name='SpruceRD_BdToKstTauMu_KstToKPi_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) mu- + CC
    
              and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    kst = common_builder.make_rd_detached_kstar0s(
        pi_p_min=2 * GeV,
        pi_pt_min=250 * MeV,
        k_p_min=2 * GeV,
        k_pt_min=250 * MeV,
        pi_ipchi2_min=9.,
        k_ipchi2_min=9.,
        pi_pid=F.PID_K < 0,
        k_pid=F.PID_K > 0,
        am_min=700 * MeV,
        am_max=1100 * MeV,
        kstar0_pt_min=0.5 * GeV,
        adocachi2cut=20.,
        vchi2pdof_max=15.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[B0 -> J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKstTauMu_KstToKPi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KstTauMu_SS_ExclusiveLine(
        name='SpruceRD_BdToKstTauMu_KstToKPi_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) mu+ + CC
    
              and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    kst = common_builder.make_rd_detached_kstar0s(
        pi_p_min=2 * GeV,
        pi_pt_min=250 * MeV,
        k_p_min=2 * GeV,
        k_pt_min=250 * MeV,
        pi_ipchi2_min=9.,
        k_ipchi2_min=9.,
        pi_pid=F.PID_K < 0,
        k_pid=F.PID_K > 0,
        am_min=700 * MeV,
        am_max=1100 * MeV,
        kstar0_pt_min=0.5 * GeV,
        adocachi2cut=20.,
        vchi2pdof_max=15.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[B0 ->  J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKstTauMu_KstToKPi_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KstTauE_OS_ExclusiveLine(
        name='SpruceRD_BdToKstTauE_KstToKPi_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) e- + CC
    
              and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    kst = common_builder.make_rd_detached_kstar0s(
        pi_p_min=2 * GeV,
        pi_pt_min=250 * MeV,
        k_p_min=2 * GeV,
        k_pt_min=250 * MeV,
        pi_ipchi2_min=9.,
        k_ipchi2_min=9.,
        pi_pid=F.PID_K < 0,
        k_pid=F.PID_K > 0,
        am_min=700 * MeV,
        am_max=1100 * MeV,
        kstar0_pt_min=0.5 * GeV,
        adocachi2cut=20.,
        vchi2pdof_max=15.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKstTauE_KstToKPi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KstTauE_SS_ExclusiveLine(
        name='SpruceRD_BdToKstTauE_KstToKPi_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) e+ + CC
    
              and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) e- + CC
    """
    kst = common_builder.make_rd_detached_kstar0s(
        pi_p_min=2 * GeV,
        pi_pt_min=250 * MeV,
        k_p_min=2 * GeV,
        k_pt_min=250 * MeV,
        pi_ipchi2_min=9.,
        k_ipchi2_min=9.,
        pi_pid=F.PID_K < 0,
        k_pid=F.PID_K > 0,
        am_min=700 * MeV,
        am_max=1100 * MeV,
        kstar0_pt_min=0.5 * GeV,
        adocachi2cut=20.,
        vchi2pdof_max=15.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKstTauE_KstToKPi_TauTo3Pi_SS_Decision")


## B0 -> phi tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2PhiTauTau_OS_ExclusiveLine(
        name='SpruceRD_BdToPhiTauTau_PhiToKK_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToPhiTauTau_PhiToKK_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2PhiTauTau_SS_ExclusiveLine(
        name='SpruceRD_BdToPhiTauTau_PhiToKK_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToPhiTauTau_PhiToKK_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2PhiTauMu_OS_ExclusiveLine(
        name='SpruceRD_BdToPhiTauMu_PhiToKK_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) mu- + CC
    
              and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) mu+ + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToPhiTauMu_PhiToKK_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2PhiTauMu_SS_ExclusiveLine(
        name='SpruceRD_BdToPhiTauMu_PhiToKK_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) mu+ + CC
    
              and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) mu- + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToPhiTauMu_PhiToKK_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2PhiTauE_OS_ExclusiveLine(
        name='SpruceRD_BdToPhiTauE_PhiToKK_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) e- + CC
    
              and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) e+ + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToPhiTauE_PhiToKK_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2PhiTauE_SS_ExclusiveLine(
        name='SpruceRD_BdToPhiTauE_PhiToKK_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) e+ + CC
    
              and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) e- + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToPhiTauE_PhiToKK_TauTo3Pi_SS_Decision")


## B0 -> rho tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2RhoTauTau_OS_ExclusiveLine(
        name='SpruceRD_BdToRhoTauTau_RhoToPiPi_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    rho = builder.make_rd_detached_rhos()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToRhoTauTau_RhoToPiPi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2RhoTauTau_SS_ExclusiveLine(
        name='SpruceRD_BdToRhoTauTau_RhoToPiPi_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    rho = builder.make_rd_detached_rhos()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToRhoTauTau_RhoToPiPi_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2RhoTauMu_OS_ExclusiveLine(
        name='SpruceRD_BdToRhoTauMu_RhoToPiPi_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) mu- + CC
    
              and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    rho = builder.make_rd_detached_rhos()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToRhoTauMu_RhoToPiPi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2RhoTauMu_SS_ExclusiveLine(
        name='SpruceRD_BdToRhoTauMu_RhoToPiPi_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) mu+ + CC
    
              and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    rho = builder.make_rd_detached_rhos()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToRhoTauMu_RhoToPiPi_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2RhoTauE_OS_ExclusiveLine(
        name='SpruceRD_BdToRhoTauE_RhoToPiPi_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) e- + CC
    
              and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    rho = builder.make_rd_detached_rhos()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToRhoTauE_RhoToPiPi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2RhoTauE_SS_ExclusiveLine(
        name='SpruceRD_BdToRhoTauE_RhoToPiPi_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) e+ + CC
    
              and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) e- + CC
    """
    rho = builder.make_rd_detached_rhos()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToRhoTauE_RhoToPiPi_TauTo3Pi_SS_Decision")


## B0 -> eta' tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2EtapTauTau_OS_ExclusiveLine(
        name='SpruceRD_BdToEtapTauTau_EtapToPiPiGamma_TauTo3Pi_OS',
        prescale=1):
    """
    Sprucing line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[B0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        hlt2_filter_code=
        "Hlt2RD_BdToEtapTauTau_EtapToPiPiGamma_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2EtapTauTau_SS_ExclusiveLine(
        name='SpruceRD_BdToEtapTauTau_EtapToPiPiGamma_TauTo3Pi_SS',
        prescale=1):
    """
    Sprucing line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[B0 -> J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        hlt2_filter_code=
        "Hlt2RD_BdToEtapTauTau_EtapToPiPiGamma_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2EtapTauMu_OS_ExclusiveLine(
        name='SpruceRD_BdToEtapTauMu_EtapToPiPiGamma_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) mu- + CC
    
              and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) mu+ + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[B0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        hlt2_filter_code=
        "Hlt2RD_BdToEtapTauMu_EtapToPiPiGamma_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2EtapTauMu_SS_ExclusiveLine(
        name='SpruceRD_BdToEtapTauMu_EtapToPiPiGamma_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) mu+ + CC
    
              and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) mu- + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[B0 -> J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        hlt2_filter_code=
        "Hlt2RD_BdToEtapTauMu_EtapToPiPiGamma_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2EtapTauE_OS_ExclusiveLine(
        name='SpruceRD_BdToEtapTauE_EtapToPiPiGamma_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) e- + CC
    
              and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) e+ + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        hlt2_filter_code=
        "Hlt2RD_BdToEtapTauE_EtapToPiPiGamma_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2EtapTauE_SS_ExclusiveLine(
        name='SpruceRD_BdToEtapTauE_EtapToPiPiGamma_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) e+ + CC
    
              and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) e- + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        hlt2_filter_code=
        "Hlt2RD_BdToEtapTauE_EtapToPiPiGamma_TauTo3Pi_SS_Decision")


## B0 -> Ks0 (LL) tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KsTauTau_LL_OS_ExclusiveLine(
        name='SpruceRD_BdToKsTauTau_KsLLToPiPi_TauTo3Pi_OS_',
        prescale=1,
        persistreco=True):
    """
    Sprucing line for Bd -> (Kshort->pi+ pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
               and for Bd -> (Kshort->pi+ pi-) (tau-->pi-pi-pi+) (tau+->pi+pi+pi-)+ CC
    """
    ks = common_builder.make_rd_ks0_lls(adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKsTauTau_KsLLToPiPi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KsTauTau_LL_SS_ExclusiveLine(
        name='SpruceRD_BdToKsTauTau_KsLLToPiPi_TauTo3Pi_SS_',
        prescale=1,
        persistreco=True):
    """
    Sprucing line for Bd -> (Kshort->pi+ pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for Bd -> (Kshort->pi+ pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    ks = common_builder.make_rd_ks0_lls(adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKsTauTau_KsLLToPiPi_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KsTauMu_LL_OS_ExclusiveLine(
        name='SpruceRD_BdToKsTauMu_KsLLToPiPi_TauTo3Pi_OS_',
        prescale=1,
        persistreco=True):
    """
    Sprucing line for Bd -> (Kshort->pi+ pi-) (tau+->pi+pi+pi-) mu- + CC
    
              and for Bd -> (Kshort->pi+ pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    ks = common_builder.make_rd_ks0_lls(adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKsTauMu_KsLLToPiPi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KsTauMu_LL_SS_ExclusiveLine(
        name='SpruceRD_BdToKsTauMu_KsLLToPiPi_TauTo3Pi_SS_',
        prescale=1,
        persistreco=True):
    """
    Sprucing line for Bd -> (Kshort->pi+ pi-) (tau+->pi+pi+pi-) mu+ + CC
    
              and for Bd -> (Kshort->pi+ pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    ks = common_builder.make_rd_ks0_lls(adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKsTauMu_KsLLToPiPi_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KsTauE_LL_OS_ExclusiveLine(
        name='SpruceRD_BdToKsTauE_KsLLToPiPi_TauTo3Pi_OS_',
        prescale=1,
        persistreco=True):
    """
    Sprucing line for Bd -> (Kshort->pi+ pi-) (tau+->pi+pi+pi-) e- + CC
    
              and for Bd -> (Kshort->pi+ pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    ks = common_builder.make_rd_ks0_lls(adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKsTauE_KsLLToPiPi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bd2KsTauE_LL_SS_ExclusiveLine(
        name='SpruceRD_BdToKsLLTauE_KsLLToPiPi_TauTo3Pi_SS_',
        prescale=1,
        persistreco=True):
    """
    Sprucing line for Bd -> (Kshort->pi+ pi-) (tau+->pi+pi+pi-) e+ + CC
    
              and for Bd -> (Kshort->pi+ pi-) (tau-->pi-pi-pi+) e- + CC
    """
    ks = common_builder.make_rd_ks0_lls(adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        hlt2_filter_code="Hlt2RD_BdToKsLLTauE_KsToPiPi_TauTo3Pi_SS_Decision")


## Bu -> K+ tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2KplusTauTau_OS_ExclusiveLine(
        name='SpruceRD_BuToKplTauTau_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for B+ -> K+ (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for B+ -> K+ (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=F.PID_K > 2)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kaon, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToKplTauTau_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2KplusTauTau_SS_ExclusiveLine(
        name='SpruceRD_BuToKplTauTau_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for B+ -> K+ (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for B+ -> K+ (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=F.PID_K > 2)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kaon, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToKplTauTau_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2KplusTauMu_OS_ExclusiveLine(
        name='SpruceRD_BuToKplTauMu_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for B+ -> K+ (tau+->pi+pi+pi-) mu- + CC
    
              and for B+ -> K+ (tau-->pi-pi-pi+) mu+ + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=F.PID_K > 2)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kaon, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToKplTauMu_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2KplusTauMu_SS_ExclusiveLine(
        name='SpruceRD_BuToKplTauMu_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for B+ -> K+ (tau+->pi+pi+pi-) mu+ + CC
    
              and for B+ -> K+ (tau-->pi-pi-pi+) mu- + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=F.PID_K > 2)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kaon, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToKplTauMu_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2KplusTauE_OS_ExclusiveLine(
        name='SpruceRD_BuToKplTauE_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for B+ -> K+ (tau+->pi+pi+pi-) e- + CC
    
              and for B+ -> K+ (tau-->pi-pi-pi+) e+ + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=F.PID_K > 2)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kaon, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToKplTauE_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2KplusTauE_SS_ExclusiveLine(
        name='SpruceRD_BuToKplTauE_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for B+ -> K+ (tau+->pi+pi+pi-) e+ + CC
    
              and for B+ -> K+ (tau-->pi-pi-pi+) e- + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=F.PID_K > 2)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, kaon, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToKplTauE_TauTo3Pi_SS_Decision")


## Bu -> K1(1270)+ tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2K1plusTauTau_OS_ExclusiveLine(
        name='SpruceRD_BuToK1plTauTau_K1plTo3Pi_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for B+ -> (K1+->pi+ pi+ pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for B+ -> (K1+->pi+ pi+ pi-) (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    k1 = builder.make_rd_detached_k1s()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToK1plTauTau_K1plTo3Pi_TauTo3Pi_OS_Decision"
    )


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2K1plusTauTau_SS_ExclusiveLine(
        name='SpruceRD_BuToK1plTauTau_K1plTo3Pi_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for B+ -> (K1+->pi+ pi+ pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for B+ -> (K1+->pi+ pi+ pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    k1 = builder.make_rd_detached_k1s()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToK1plTauTau_K1plTo3Pi_TauTo3Pi_SS_Decision"
    )


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2K1plusTauMu_OS_ExclusiveLine(
        name='SpruceRD_BuToK1plTauMu_K1plTo3Pi_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for B+ -> (K1+->pi+ pi+ pi-) (tau+->pi+pi+pi-) mu- + CC
    
              and for B+ -> (K1+->pi+ pi+ pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    k1 = builder.make_rd_detached_k1s()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToK1plTauMu_K1plTo3Pi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2K1plusTauMu_SS_ExclusiveLine(
        name='SpruceRD_BuToK1plTauMu_K1plTo3Pi_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for B+ -> (K1+->pi+ pi+ pi-) (tau+->pi+pi+pi-) mu+ + CC
    
              and for B+ -> (K1+->pi+ pi+ pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    k1 = builder.make_rd_detached_k1s()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToK1plTauMu_K1plTo3Pi_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2K1plusTauE_OS_ExclusiveLine(
        name='SpruceRD_BuToK1plTauE_K1plTo3Pi_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for B+ -> (K1+->pi+ pi+ pi-) (tau+->pi+pi+pi-) e- + CC
    
              and for B+ -> (K1+->pi+ pi+ pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    k1 = builder.make_rd_detached_k1s()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToK1plTauE_K1plTo3Pi_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bu2K1plusTauE_SS_ExclusiveLine(
        name='SpruceRD_BuToK1plTauE_K1plTo3Pi_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for B+ -> (K1+->pi+ pi+ pi-) (tau+->pi+pi+pi-) e+ + CC
    
              and for B+ -> (K1+->pi+ pi+ pi-) (tau-->pi-pi-pi+) e- + CC
    """
    k1 = builder.make_rd_detached_k1s()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul])


## Bs -> phi tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2PhiTauTau_OS_ExclusiveLine(
        name='SpruceRD_BsToPhiTauTau_PhiToKK_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bs -> (phi->K+ K-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for Bs -> (phi->K+ K-) (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[B_s0 -> J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2PhiTauTau_SS_ExclusiveLine(
        name='SpruceRD_BsToPhiTauTau_PhiToKK_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bs -> (phi->K+ K-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for Bs -> (phi->K+ K-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[B_s0 -> J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2PhiTauMu_OS_ExclusiveLine(
        name='SpruceRD_BsToPhiTauMu_PhiToKK_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bs -> (phi->K+ K-) (tau+->pi+pi+pi-) mu- + CC
    
              and for Bs -> (phi->K+ K-) (tau-->pi-pi-pi+) mu+ + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[B_s0 -> J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2PhiTauMu_SS_ExclusiveLine(
        name='SpruceRD_BsToPhiTauMu_PhiToKK_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bs -> (phi->K+ K-) (tau+->pi+pi+pi-) mu+ + CC
    
              and for Bs -> (phi->K+ K-) (tau-->pi-pi-pi+) mu- + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[B_s0 -> J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2PhiTauE_OS_ExclusiveLine(
        name='SpruceRD_BsToPhiTauE_PhiToKK_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bs -> (phi->K+ K-) (tau+->pi+pi+pi-) e- + CC
    
              and for Bs -> (phi->K+ K-) (tau-->pi-pi-pi+) e+ + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[B_s0 -> J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2PhiTauE_SS_ExclusiveLine(
        name='SpruceRD_BsToPhiTauE_PhiToKK_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bs -> (phi->K+ K-) (tau+->pi+pi+pi-) e+ + CC
    
              and for Bs -> (phi->K+ K-) (tau-->pi-pi-pi+) e- + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[B_s0 -> J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul])


## Bs -> eta' tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2EtapTauTau_OS_ExclusiveLine(
        name='SpruceRD_BsToEtapTauTau_EtapToPiPiGamma_TauTo3Pi_OS',
        prescale=1):
    """
    Sprucing line for Bs -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for Bs -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[B_s0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2EtapTauTau_SS_ExclusiveLine(
        name='SpruceRD_BsToEtapTauTau_EtapToPiPiGamma_TauTo3Pi_SS',
        prescale=1):
    """
    Sprucing line for Bs -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for Bs -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[B_s0 -> J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2EtapTauMu_OS_ExclusiveLine(
        name='SpruceRD_BsToEtapTauMu_EtapToPiPiGamma_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bs -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) mu- + CC
    
              and for Bs -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) mu+ + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[B_s0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2EtapTauMu_SS_ExclusiveLine(
        name='SpruceRD_BsToEtapTauMu_EtapToPiPiGamma_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bs -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) mu+ + CC
    
              and for Bs -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) mu- + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[B_s0 -> J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2EtapTauE_OS_ExclusiveLine(
        name='SpruceRD_BsToEtapTauE_EtapToPiPiGamma_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Bs -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) e- + CC
    
              and for Bs -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) e+ + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[B_s0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Bs2EtapTauE_SS_ExclusiveLine(
        name='SpruceRD_BsToEtapTauE_EtapToPiPiGamma_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Bs -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) e+ + CC
    
              and for Bs -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) e- + CC
    """
    etap = builder.make_rd_detached_etaps()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[B_s0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        hlt2_filter_code="Hlt2RD_BuToK1plTauE_K1plTo3Pi_TauTo3Pi_SS_Decision")


## Lb -> K p tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2KpTauTau_OS_ExclusiveLine(
        name='SpruceRD_LbToKpTauTau_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Lb -> p K- (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for Lb -> p K- (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        hlt2_filter_code="Hlt2RD_LbToKpTauTau_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2KpTauTau_SS_ExclusiveLine(
        name='SpruceRD_LbToKpTauTau_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Lb -> p K- (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for Lb -> p K- (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        hlt2_filter_code="Hlt2RD_LbToKpTauTau_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2KpTauMu_OS_ExclusiveLine(
        name='SpruceRD_LbToKpTauMu_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Lb -> p K- (tau+->pi+pi+pi-) mu- + CC
    
              and for Lb -> p K- (tau-->pi-pi-pi+) mu+ + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        hlt2_filter_code="Hlt2RD_LbToKpTauMu_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2KpTauMu_SS_ExclusiveLine(
        name='SpruceRD_LbToKpTauMu_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Lb -> p K- (tau+->pi+pi+pi-) mu+ + CC
    
              and for Lb -> p K- (tau-->pi-pi-pi+) mu- + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        hlt2_filter_code="Hlt2RD_LbToKpTauMu_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2KpTauE_OS_ExclusiveLine(
        name='SpruceRD_LbToKpTauE_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Lb -> p K- (tau+->pi+pi+pi-) e- + CC
    
              and for Lb -> p K- (tau-->pi-pi-pi+) e+ + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        hlt2_filter_code="Hlt2RD_LbToKpTauE_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2KpTauE_SS_ExclusiveLine(
        name='SpruceRD_LbToKpTauE_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Lb -> p K- (tau+->pi+pi+pi-) e+ + CC
    
              and for Lb -> p K- (tau-->pi-pi-pi+) e- + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        hlt2_filter_code="Hlt2RD_LbToKpTauE_TauTo3Pi_SS_Decision")


## Lambda_b -> Lambda (LL) tau l
@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2LambdaTauTau_LL_OS_ExclusiveLine(
        name='SpruceRD_LbToLambdaTauTau_LambdaToPiP_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    
              and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) (tau+->pi+pi+pi-) + CC
    """
    lambda0 = common_builder.make_rd_lambda_lls(
        pi_ipchi2_min=9., adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau+")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        hlt2_filter_code=
        "Hlt2RD_LbToLambdaTauTau_LambdaLLToPiP_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2LambdaTauTau_LL_SS_ExclusiveLine(
        name='SpruceRD_LbToLambdaTauTau_LambdaLLToPiP_TauTo3Pi_SS',
        prescale=1):
    """
    Sprucing line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC
    
              and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    lambda0 = common_builder.make_rd_lambda_lls(
        pi_ipchi2_min=9., adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="tau-")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        hlt2_filter_code=
        "Hlt2RD_LbToLambdaTauTau_LambdaLLToPiP_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2LambdaTauMu_LL_OS_ExclusiveLine(
        name='SpruceRD_LbToLambdaTauMu_LambdaLLToPiP_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) mu- + CC
    
              and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    lambda0 = common_builder.make_rd_lambda_lls(
        pi_ipchi2_min=9., adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu+")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        hlt2_filter_code=
        "Hlt2RD_LbToLambdaTauMu_LambdaLLToPiP_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2LambdaTauMu_LL_SS_ExclusiveLine(
        name='SpruceRD_LbToLambdaTauMu_LambdaToPiP_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) mu+ + CC
    
              and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    lambda0 = common_builder.make_rd_lambda_lls(
        pi_ipchi2_min=9., adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="mu-")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        hlt2_filter_code=
        "Hlt2RD_LbToLambdaTauMu_LambdaLLToPiP_TauTo3Pi_SS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2LambdaTauE_LL_OS_ExclusiveLine(
        name='SpruceRD_LbToLambdaTauE_LambdaLLToPiP_TauTo3Pi_OS', prescale=1):
    """
    Sprucing line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) e- + CC
    
              and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    lambda0 = common_builder.make_rd_lambda_lls(
        pi_ipchi2_min=9., adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e+")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        hlt2_filter_code=
        "Hlt2RD_LbToLambdaTauE_LambdaLLToPiP_TauTo3Pi_OS_Decision")


@register_line_builder(sprucing_lines)
@configurable
def Spruce_Lambdab2LambdaTauE_LL_SS_ExclusiveLine(
        name='SpruceRD_LbToLambdaTauE_LambdaToPiP_TauTo3Pi_SS', prescale=1):
    """
    Sprucing line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) e+ + CC
    
              and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) e- + CC
    """
    lambda0 = common_builder.make_rd_lambda_lls(
        pi_ipchi2_min=9., adocachi2cut=25., vchi2pdof_max=20.)
    dilepton = builder.make_dilepton_from_tauls(daughter_id="e-")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        hlt2_filter_code=
        "Hlt2RD_LbToLambdaTauE_LambdaLLToPiP_TauTo3Pi_SS_Decision")
