###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builders and filters for the multilepton lines for the RD working group.

TODO: revise `_best` naming scheme
TODO: make more visible that OS and SS cuts are same

author: Vitalii Lisovskyi
date: 21.01.2022

"""

from RecoConf.reconstruction_objects import make_pvs
import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import MeV, GeV

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.standard_particles import make_long_pions
from .rdbuilder_thor import make_rd_prompt_muons, make_rd_prompt_electrons, make_rd_prompt_dielectrons, make_rd_prompt_dimuons, make_rd_prompt_mue

from PyConf import configurable

###########################
# Four-lepton builders    #
###########################


@configurable
def make_prompt_four_muon(
        name,
        #pvs,
        parent_id='J/psi(1S)',
        pid_cut_mu=F.require_all(F.ISMUON, F.PID_MU > 2.),
        pid_best=6,
        comb_m_min=400 * MeV,
        comb_m_max=13000 * MeV,
        comb12_m_min=0 * MeV,
        comb12_m_max=13000 * MeV,
        comb123_m_min=0 * MeV,
        comb123_m_max=13000 * MeV,
        m_min=400 * MeV,
        m_max=12000 * MeV,
        pt_best=0.4 * GeV,
        p_best=4.5 * GeV,
        comb_pt_min=1.5 * GeV,  #2. * GeV,
        pt_min=2.5 * GeV,  #3.5 * GeV,
        docachi2_max=16.,
        vchi2pdof_max=6.,
):
    """
    Make J/psi(1S) -> mu+ mu+ mu- mu-
    """
    muons = make_rd_prompt_muons(pid=pid_cut_mu)
    comb_cut_12 = F.require_all(
        F.MAXDOCACHI2CUT(docachi2_max),
        F.MAX(F.PID_MU) > pid_best, in_range(comb12_m_min, F.MASS,
                                             comb12_m_max))
    comb_cut_123 = F.require_all(
        F.MAXDOCACHI2CUT(docachi2_max),
        in_range(comb123_m_min, F.MASS, comb123_m_max))
    comb_cut = F.require_all(
        F.MAXDOCACHI2CUT(docachi2_max), in_range(comb_m_min, F.MASS,
                                                 comb_m_max),
        F.MAX(F.PT) > pt_best,
        F.MAX(F.P) > p_best, F.PT > comb_pt_min)
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
    )

    return ParticleCombiner([muons, muons, muons, muons],
                            name=name + "_combiner",
                            DecayDescriptor=f"{parent_id} -> mu+ mu+ mu- mu-",
                            Combination12Cut=comb_cut_12,
                            Combination123Cut=comb_cut_123,
                            CombinationCut=comb_cut,
                            CompositeCut=vertex_cut)


@configurable
def make_prompt_3mue(
        name,
        #pvs,
        pid_cut_mu=F.require_all(F.ISMUON, F.PID_MU > 2.),
        pid_cut_e=(F.PID_E > 5.5),
        pid_best=5.5,
        parent_id='J/psi(1S)',
        comb_m_min=0 * MeV,
        comb_m_max=13000 * MeV,
        m_min=300 * MeV,
        m_max=12000 * MeV,
        pt_best=0.35 * GeV,
        p_best=7 * GeV,
        comb_pt_min=1.7 * GeV,  #2. * GeV,
        pt_min=2.5 * GeV,  #3.5 * GeV,
        docachi2_max=10.,
        vchi2pdof_max=6.,
):
    """
    Make [J/psi(1S) -> mu+ mu+ mu- e-]cc
    """
    muons = make_rd_prompt_muons(pid=pid_cut_mu, pt_min=350. * MeV)
    electrons = make_rd_prompt_electrons(pt_min=400. * MeV, pid=pid_cut_e)
    comb_cut_12 = F.require_all(F.MASS < comb_m_max,
                                F.MAXDOCACHI2CUT(docachi2_max),
                                F.MAX(F.PID_MU) > pid_best)
    comb_cut_123 = F.require_all(F.MASS < comb_m_max,
                                 F.MAXDOCACHI2CUT(docachi2_max))
    comb_cut = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACHI2CUT(docachi2_max),
        F.MAX(F.PT) > pt_best,
        F.MAX(F.P) > p_best, F.PT > comb_pt_min)
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
    )

    return ParticleCombiner(
        [muons, muons, muons, electrons],
        name=name + "_combiner",
        DecayDescriptor=f"[{parent_id} -> mu+ mu+ mu- e-]cc",
        Combination12Cut=comb_cut_12,
        Combination123Cut=comb_cut_123,
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut)


@configurable
def make_prompt_2mu2e(
        name,
        #pvs,
        parent_id='J/psi(1S)',
        comb_m_min=0 * MeV,
        comb_m_max=13000 * MeV,
        m_min=200 * MeV,
        m_max=12000 * MeV,
        pt_best=0.4 * GeV,
        p_best=5. * GeV,
        pid_best=5.,
        comb_pt_min=1. * GeV,  #2. * GeV,
        pt_min=2.5 * GeV,  #3.5 * GeV,
        max_pt_min=0.75 * GeV,
        docachi2_max=10.,
        vchi2pdof_max=6.,
):
    """
    Make J/psi(1S) -> rho(770)0 omega(782); which essetinally should be mu+ mu- e+ e-.
    """
    dimuons = make_rd_prompt_dimuons(
        pid=F.require_all(F.ISMUON, F.PID_MU > 3.5),
        pt_muon_min=300. * MeV,
        pid_best=pid_best,
        pt_best=pt_best,
        p_best=p_best,
        adocachi2cut_max=10.,
        vchi2pdof_max=8.,
        am_max=comb_m_max)
    dielectrons = make_rd_prompt_dielectrons(
        pt_e_min=0.3 * GeV,
        max_vchi2ndof=8.,
        max_dilepton_mass=comb_m_max,
        PIDe_min=5,
    )
    comb_cut = F.require_all(
        F.MAXDOCACHI2CUT(docachi2_max), in_range(comb_m_min, F.MASS,
                                                 comb_m_max),
        F.MAX(F.PT) > max_pt_min, F.PT > comb_pt_min)
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
    )

    return ParticleCombiner(
        [dimuons, dielectrons],
        name=name + "_combiner",
        DecayDescriptor=f"{parent_id} -> rho(770)0 omega(782)",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut)


@configurable
def make_prompt_2muSS2eSS(
        name,
        parent_id='J/psi(1S)',
        comb_m_min=0 * MeV,
        comb_m_max=13000 * MeV,
        m_min=200 * MeV,
        m_max=12000 * MeV,
        pt_best=0.5 * GeV,
        p_best=5. * GeV,
        pid_best=5.,
        comb_pt_min=1. * GeV,  #2. * GeV,
        pt_min=2.5 * GeV,  #3.5 * GeV,
        max_pt_min=0.75 * GeV,
        docachi2_max=16.,
        vchi2pdof_max=6.,
):
    """
    Make J/psi(1S) -> rho(770)0 omega(782); which essetinally should be mu+ mu+ e- e-.
    """
    dimuons = make_rd_prompt_dimuons(
        same_sign=True,
        pid=F.require_all(F.ISMUON, F.PID_MU > pid_best),
        pt_muon_min=350. * MeV,
        pt_best=pt_best,
        p_best=p_best,
        adocachi2cut_max=10.,
        vchi2pdof_max=8.,
        am_max=comb_m_max)
    dielectrons = make_rd_prompt_dielectrons(
        same_sign=True,
        pt_e_min=0.3 * GeV,
        max_vchi2ndof=8.,
        max_dilepton_mass=comb_m_max)  #opposite_sign=False)
    comb_cut = F.require_all(
        F.MAXDOCACHI2CUT(docachi2_max), in_range(comb_m_min, F.MASS,
                                                 comb_m_max),
        F.MAX(F.PT) > max_pt_min, F.PT > comb_pt_min)
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
    )

    return ParticleCombiner(
        [dimuons, dielectrons],
        name=name + "_combiner",
        DecayDescriptor=f"{parent_id} -> rho(770)0 omega(782)",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut)


@configurable
def make_prompt_mue2e(
        name,
        #pvs,
        parent_id='J/psi(1S)',
        comb_m_min=0 * MeV,
        comb_m_max=13000 * MeV,
        m_min=100 * MeV,
        m_max=12000 * MeV,
        comb_pt_min=1.7 * GeV,  #2. * GeV,
        pt_min=2.9 * GeV,  #3.5 * GeV,
        pid_best=None,  #kept for consistency with the other builders
        max_pt_min=0.8 * GeV,
        pt_best=0.55 * GeV,
        p_best=5. * GeV,
        docachi2_max=12.,
        vchi2pdof_max=6.,
        min_dielectron_mass=0 * MeV,
):
    """
    Make [J/psi(1S) -> f_0(980) omega(782)]cc; which essetinally should be mu+ e- e+ e-.
    """
    dileptons = make_rd_prompt_mue(
        pid_muon=F.require_all(F.ISMUON, F.PID_MU > 5.5),
        pid_electron=(F.PID_E > 6.),
        pt_muon_min=300. * MeV,
        pt_electron_min=300. * MeV,
        pt_best=pt_best,
        p_best=p_best,
        am_max=comb_m_max)
    dielectrons = make_rd_prompt_dielectrons(
        pt_e_min=0.3 * GeV,
        max_vchi2ndof=6.,
        min_dilepton_pt=0.5 * GeV,
        min_dilepton_mass=min_dielectron_mass,
        max_dilepton_mass=comb_m_max)
    comb_cut = F.require_all(
        F.MAXDOCACHI2CUT(docachi2_max), in_range(comb_m_min, F.MASS,
                                                 comb_m_max),
        F.MAX(F.PT) > max_pt_min, F.PT > comb_pt_min)
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
    )

    return ParticleCombiner(
        [dileptons, dielectrons],
        name=name + "_combiner",
        DecayDescriptor=f"[{parent_id} -> f_0(980) omega(782)]cc",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut)


@configurable
def make_prompt_2e2e(
        name,
        parent_id='J/psi(1S)',
        comb_m_min=0 * MeV,
        comb_m_max=13000 * MeV,
        m_min=0 * MeV,
        m_max=12000 * MeV,
        pid_best=6.5,
        pt_best=None,  #kept for consistency with the other builders
        p_best=None,  #kept for consistency with the other builders
        comb_pt_min=1.5 * GeV,  #2. * GeV,
        pt_min=2.5 * GeV,  #3.5 * GeV,
        max_pt_min=0.8 * GeV,
        docachi2_max=9.,
        vchi2pdof_max=6.,
        min_dielectron_mass=0 * MeV,
        bpvipchi2_max=None,
):
    """
    Make J/psi(1S) -> omega(782) omega(782); which essetinally should be e+ e- e+ e-.
    Note that this selection can (must?) create 2 candidates per event, due to available permutations for each event.
    The reason this can be "better" than the plain e+ e+ e- e- 4-body combination, is due to smaller amount of duplicate brem.
    """
    dielectrons = make_rd_prompt_dielectrons(
        PIDe_min=pid_best,
        pt_e_min=0.3 * GeV,
        max_vchi2ndof=4.,
        min_dilepton_mass=min_dielectron_mass,
        max_dilepton_mass=comb_m_max)
    comb_cut = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAX(F.PT) > max_pt_min, F.MAXDOCACHI2CUT(docachi2_max),
        F.PT > comb_pt_min)
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max), F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max)
    if bpvipchi2_max is not None:
        pvs = make_pvs()
        #vertex_cut &= F.BPVIPCHI2(pvs) < bpvipchi2_max,
        vertex_cut = F.require_all(
            in_range(m_min, F.MASS, m_max), F.PT > pt_min,
            F.CHI2DOF < vchi2pdof_max,
            F.BPVIPCHI2(pvs) < bpvipchi2_max)

    return ParticleCombiner(
        [dielectrons, dielectrons],
        name=name + "_combiner",
        DecayDescriptor=f"{parent_id} -> omega(782) omega(782)",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut)


### Tagging with the excited state ###


@configurable
def make_psi2S_tag(
        name,
        jpsis,
        electronchannel=True,
        comb_m_min=2900 * MeV,
        comb_m_max=4600 * MeV,
        m_min=3000 * MeV,
        m_max=4500 * MeV,
        diff_m_min=400 * MeV,
        diff_m_max=800 * MeV,
        pid_best=None,
        pt_best=None,  #0.4 * GeV, #lepton
        p_best=None,  #4. * GeV, #lepton
        Jpsi_comb_pt_min=1. * GeV,  #1. * GeV,
        Jpsi_pt_min=1.2,  #1.2 * GeV,
        pi_pt_min=0.4 * GeV,
        comb_pt_min=2.0 * GeV,  #2. * GeV,
        pt_min=3.0 * GeV,  #3.5 * GeV,
        docachi2_max=9.,
        vchi2pdof_max=6.):
    """
    Tag the Jpsi with psi(2S)->Jpsi pi+pi- decay. This allows to use the mass constraint and improve the mass resolution.
    """
    pions = ParticleFilter(
        make_long_pions(), Cut=F.FILTER(F.require_all(F.PT > pi_pt_min)))

    comb_cut_12 = F.require_all(F.MASS < comb_m_max,
                                F.MAXDOCACHI2CUT(docachi2_max))
    comb_cut = F.require_all(
        F.MAXDOCACHI2CUT(docachi2_max), in_range(comb_m_min, F.MASS,
                                                 comb_m_max),
        in_range(diff_m_min, (F.MASS - F.CHILD(1, F.MASS)), diff_m_max),
        F.PT > comb_pt_min)
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
    )

    return ParticleCombiner([jpsis, pions, pions],
                            name=name + "_psi2S_combiner",
                            DecayDescriptor="psi(2S) -> J/psi(1S) pi+ pi-",
                            Combination12Cut=comb_cut_12,
                            CombinationCut=comb_cut,
                            CompositeCut=vertex_cut)
