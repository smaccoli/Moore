###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Author: Tommaso Fulghesu
Contact: tommaso.fulghesu@cern.ch
Date: 07/12/2021
'''

######################################################################################
####                                                                              ####
#### Builders for exlusive lines Yb -> X tau (-> 3pi nu_tau)                      ####
####                                                                              ####
######################################################################################

from PyConf import configurable
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner

import Functors as F
from Functors.math import in_range

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_detached_muons, make_rd_detached_electrons, make_rd_photons,
    make_rd_detached_phis, make_rd_tauons_hadronic_decay,
    make_rd_has_rich_detached_pions, make_rd_has_rich_detached_kaons,
    make_rd_has_rich_detached_protons)


@configurable
def make_rd_detached_rhos(name='rd_detached_rhos_{hash}',
                          make_pions=make_rd_has_rich_detached_pions,
                          pi_p_min=2 * GeV,
                          pi_pt_min=250 * MeV,
                          pi_ipchi2_min=6.,
                          am_min=250 * MeV,
                          am_max=1000 * MeV,
                          rho_pt_min=800 * MeV,
                          ipchi2_rho_min=9.,
                          adocachi2cut_max=30.,
                          bpvipchi2_max=30,
                          vchi2pdof_max=15):
    """
    Build rho(770)->pipi candidates
    """
    pvs = make_pvs()
    pions = make_pions(
        p_min=pi_p_min, pt_min=pi_pt_min, mipchi2dvprimary_min=pi_ipchi2_min)
    descriptor = 'rho(770)0 -> pi+ pi-'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2cut_max))
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_rho_min, Vertices=pvs),
        F.BPVIPCHI2(pvs) < bpvipchi2_max)
    return ParticleCombiner(
        Inputs=[pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_rd_detached_etaps(name='rd_detached_etaps_{hash}',
                           make_pions=make_rd_has_rich_detached_pions,
                           pi_p_min=2 * GeV,
                           pi_pt_min=250 * MeV,
                           pi_ipchi2_min=6.,
                           e_gamma_min=1 * GeV,
                           et_gamma_min=1 * GeV,
                           pi_docachi2_cut=30.,
                           am_min=700 * MeV,
                           am_max=1300 * MeV,
                           adocachi2cut_max=20.,
                           ipchi2_etap_min=6.,
                           bpvipchi2_max=30,
                           vchi2pdof_max=15):
    """
    Build eta'->pipigamma candidates
    """
    pvs = make_pvs()
    pions = make_pions(
        p_min=pi_p_min, pt_min=pi_pt_min, mipchi2dvprimary_min=pi_ipchi2_min)
    photons = make_rd_photons(et_min=et_gamma_min, e_min=e_gamma_min)
    descriptor = '[eta_prime -> pi+ pi- gamma]cc'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2cut_max))
    combination12_code = F.MAXDOCACHI2CUT(pi_docachi2_cut)
    vertex_code = F.require_all(
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_etap_min, Vertices=pvs),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max)
    return ParticleCombiner(
        Inputs=[pions, pions, photons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        Combination12Cut=combination12_code,
        CompositeCut=vertex_code)


@configurable
def make_rd_detached_k1s(name='rd_detached_k1s_{hash}',
                         make_pions=make_rd_has_rich_detached_pions,
                         make_kaons=make_rd_has_rich_detached_kaons,
                         pi_ipchi2_min=6.,
                         pi_p_min=2 * GeV,
                         pi_pt_min=250 * MeV,
                         k_ipchi2_min=6.,
                         k_p_min=2 * GeV,
                         k_pt_min=250 * MeV,
                         am_min=1000 * MeV,
                         am_max=1600 * MeV,
                         am_2pi_max=1450 * MeV,
                         adocachi2cut_max=25.,
                         ipchi2_k1_min=9.,
                         bpvfdchi2_min=16,
                         vchi2pdof_max=15):
    """
    Build K1(1270)->Kpipi candidates
    """
    pvs = make_pvs()
    pions = make_pions(
        p_min=pi_p_min, pt_min=pi_pt_min, mipchi2dvprimary_min=pi_ipchi2_min)
    kaons = make_kaons(
        p_min=k_p_min, pt_min=k_pt_min, mipchi2dvprimary_min=k_ipchi2_min)
    descriptor = '[K_1(1270)+ -> K+ pi+ pi-]cc'
    combination12_code = F.MASS < am_2pi_max

    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2cut_max))
    vertex_code = F.require_all(
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_k1_min, Vertices=pvs),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    return ParticleCombiner(
        Inputs=[kaons, pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_dihadron_from_pK(name='dihadron_from_pk_{hash}',
                          parent_id='Lambda(1520)0',
                          make_protons=make_rd_has_rich_detached_protons,
                          make_kaons=make_rd_has_rich_detached_kaons,
                          p_ipchi2_min=6.,
                          k_ipchi2_min=6.,
                          p_pid=F.PID_P > 2,
                          k_pid=F.PID_K > 2,
                          pt_min=400 * MeV,
                          am_min=1300 * MeV,
                          am_max=5620 * MeV,
                          adocachi2cut_max=25.,
                          vtxchi2_max=15):
    """
    Make pK combination
    """
    protons = make_protons(mipchi2dvprimary_min=p_ipchi2_min, pid=p_pid)
    kaons = make_kaons(mipchi2dvprimary_min=k_ipchi2_min, pid=k_pid)
    descriptor = f'[{parent_id} -> p+ K-]cc'
    combination_code = F.require_all(F.PT > pt_min,
                                     in_range(am_min, F.MASS, am_max),
                                     F.MAXDOCACHI2CUT(adocachi2cut_max))

    vertex_code = F.require_all(F.CHI2 < vtxchi2_max)
    return ParticleCombiner(
        Inputs=[protons, kaons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def filter_rd_detached_phis(name='rd_detached_phis_{hash}',
                            k_pid=F.PID_K > 0,
                            am_max=1100 * MeV,
                            adocachi2cut_max=25.,
                            vchi2pdof_max=20,
                            bpvipchi2_max=30):
    """
    Filter with IPCHI2 cut the phi(1020) built in rdbuilder_thor
    """
    pvs = make_pvs()
    phis = make_rd_detached_phis(
        k_pid=k_pid,
        adocachi2cut=adocachi2cut_max,
        vchi2pdof_max=vchi2pdof_max)
    code = F.BPVIPCHI2(pvs) < bpvipchi2_max

    return ParticleFilter(phis, name=name, Cut=F.FILTER(code))


@configurable
def make_dilepton_from_tauls(parent_id="J/psi(1S)",
                             daughter_id="tau-",
                             name='dilepton_from_tauls_{hash}_{hash}',
                             pi_pt_min=250 * MeV,
                             pi_ipchi2_min=6.,
                             best_pi_pt_min=800 * MeV,
                             best_pi_ipchi2_min=9.,
                             pi_pid=F.PID_K < 0.,
                             tau_am_min=800 * MeV,
                             tau_am_max=2100 * MeV,
                             tau_m_min=1000 * MeV,
                             tau_m_max=2000 * MeV,
                             tau_chi2_max=16,
                             tau_maxdocacut=0.2 * mm,
                             tau_bpvfdchi2_min=16,
                             tau_pt_min=500 * MeV,
                             make_muons=make_rd_detached_muons,
                             make_electrons=make_rd_detached_electrons,
                             mu_pt=500 * MeV,
                             e_pt=500 * MeV,
                             mu_p=3 * GeV,
                             e_p=3 * GeV,
                             mu_pid=F.require_all(F.ISMUON, F.PID_MU > 2.),
                             e_pid=F.PID_E > 3.,
                             mu_mipchi2dvprimary=9.,
                             e_mipchi2dvprimary=9.,
                             chi2dof_max=75,
                             comb_m_min=100 * MeV):
    """
    Make tau-l combination.
    The tau is filtered from rdbuilder_thor with requirements on IPCHI2, VDRHO, VDZ and M
    """
    decay_descriptor = f'[{parent_id} -> tau- {daughter_id}]cc'
    pvs = make_pvs()
    combination_code = F.MASS > comb_m_min
    vertex_code = F.CHI2DOF < chi2dof_max
    code = F.require_all(
        in_range(tau_m_min, F.MASS, tau_m_max),
        F.CHI2() < tau_chi2_max, F.MAXDOCACUT(tau_maxdocacut),
        F.BPVFDCHI2(pvs) > tau_bpvfdchi2_min,
        F.PT() > tau_pt_min)
    taus = ParticleFilter(
        make_rd_tauons_hadronic_decay(
            pi_pt_min=pi_pt_min,
            pi_ipchi2_min=pi_ipchi2_min,
            best_pi_pt_min=best_pi_pt_min,
            best_pi_ipchi2_min=best_pi_ipchi2_min,
            pi_pid=pi_pid,
            am_min=tau_am_min,
            am_max=tau_am_max),
        name=name,
        Cut=F.FILTER(code))
    leptons = taus
    if daughter_id == "mu+" or daughter_id == "mu-":
        leptons = make_muons(
            pt_min=mu_pt,
            p_min=mu_p,
            mipchi2dvprimary_min=mu_mipchi2dvprimary,
            pid=mu_pid)
    if daughter_id == "e+" or daughter_id == "e-":
        leptons = make_electrons(
            pt_min=e_pt,
            p_min=e_p,
            mipchi2dvprimary_min=e_mipchi2dvprimary,
            pid=e_pid)
    return ParticleCombiner(
        Inputs=[taus, leptons],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_beauty2xtaul(particles,
                      descriptors,
                      name='beauty_to_xtaul_{hash}_{hash}',
                      comb_m_min=1 * GeV,
                      comb_m_max=12 * GeV,
                      m_min=2 * GeV,
                      m_max=10 * GeV,
                      p_min=3 * GeV,
                      pt_min=1 * GeV,
                      vchi2pdof_max=100,
                      ipchi2_max=50.,
                      fdchi2_min=120,
                      dira_min=0.995):
    """
    Builder for B0, Bu, Bs and Lambdab
    """
    pvs = make_pvs()
    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vchi2pdof_max, F.P > p_min,
        F.PT > pt_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > fdchi2_min,
        F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def filter_BToTauTau(
        ditaus,
        name='filter_RD_B2tautau_{hash}',
        m_min=2 * GeV,
        m_max=7 * GeV,
        p_min=3 * GeV,
        pt_min=1 * GeV,
        bpvdira_min=0.995,
        bpvfdchi2_min=225,
        bpvipchi2_max=50,
        chi2_max=90,
        bpvfd_max=90 * mm,
):
    pvs = make_pvs()
    code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.P > p_min, F.PT > pt_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.CHI2() < chi2_max,
        F.BPVFD(pvs) < bpvfd_max)
    return ParticleFilter(ditaus, F.FILTER(code), name=name)
