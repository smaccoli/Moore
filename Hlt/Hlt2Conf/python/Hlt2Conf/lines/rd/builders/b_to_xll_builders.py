###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the b -> X ll builders

Includes:

    B+ / B0 -> H J/psi(1S) builder for a hadron H
    - A general 2 body combiner for making B+ and B0

    X -> hhh builder combining 3 hadrons

    K*+ -> K+ pi0 builder

    X -> ppbar builder
    X -> Lambda0pbar builder
    B(s)0 / B- -> Xpbarll builder
    


Contact: Richard Williams (rmw68@cam.ac.uk)
"""

### Builders ###
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
import Functors as F
from Hlt2Conf.algorithms_thor import ParticleCombiner
from PyConf import configurable


@configurable
def make_rd_BToXll(
        dileptons,
        hadrons,
        pvs,
        Descriptor,
        name='rd_BToXll_{hash}',
        am_min=4_500. * MeV,
        am_max=7_000. * MeV,
        B_pt_min=0. * MeV,
        bpvipchi2_max=25.,
        min_cosine=0.9995,
        FDchi2_min=100.,
        vchi2pdof_max=9.,
        low_factor=0.5,
        high_factor=1.5,
):

    ### Make the Bu or Bs -> hadron l+ l- candidate ###

    combination_code = F.require_all(
        in_range(low_factor * am_min, F.MASS, high_factor * am_max),
        F.SUM(F.PT) > B_pt_min,
    )

    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVDIRA(pvs) > min_cosine,
        F.BPVFDCHI2(pvs) > FDchi2_min,
        F.CHI2DOF < vchi2pdof_max,
    )

    return ParticleCombiner(
        [dileptons, hadrons],
        DecayDescriptor=Descriptor,
        CombinationCut=combination_code,
        name=name,
        CompositeCut=vertex_code,
    )


def make_rd_XTohhh(
        hadron_1,
        hadron_2,
        hadron_3,
        Descriptor,
        name='rd_XTohhh_{hash}',
        am_min=500. * MeV,
        am_max=4200. * MeV,
        adocachi2cut=25.,
        vchi2pdof_max=9.,
        X_pt_min=500. * MeV,
):

    two_body_combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2cut))

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(adocachi2cut), Indices=[2, 3]),
        F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(adocachi2cut), Indices=[1, 3]),
    )

    vertex_code = F.require_all(
        F.PT > X_pt_min,
        F.CHI2DOF < vchi2pdof_max,
    )

    return ParticleCombiner([hadron_1, hadron_2, hadron_3],
                            name=name,
                            DecayDescriptor=Descriptor,
                            Combination12Cut=two_body_combination_code,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


def make_rd_KstToKpPi(
        kaons,
        pions,
        Decay_Descriptor="[K*(892)+ -> K+ pi0]cc",
        pt_min=400. * MeV,
        am_min=600. * MeV,
        am_max=1200. * MeV,
        adocachi2cut=30.,
        vchi2pdof_max=9.,
):

    ### Make K* -> K+ pi0 candidate ###

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > pt_min,
        F.MAXDOCACHI2CUT(adocachi2cut),
    )

    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, )

    return ParticleCombiner([kaons, pions],
                            DecayDescriptor=Decay_Descriptor,
                            CombinationCut=combination_code,
                            name='My_K_star_plus Maker_{hash}',
                            ParticleCombiner="ParticleAdder",
                            CompositeCut=vertex_code)


from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import GeV
from Hlt2Conf.algorithms_thor import ParticleFilter
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_detached_muons, make_detached_dielectron_with_brem)


@configurable
def make_dimuon(name='rd_detached_dimuon_{hash}',
                pt_dimuon_min=0. * MeV,
                pt_muon_min=300. * MeV,
                p_muon_min=3000. * MeV,
                ipchi2_muon_min=9.,
                pidmu_muon_min=3.,
                bpvvdchi2_min=30.,
                vchi2pdof_max=30.,
                am_min=0. * MeV,
                am_max=6000. * MeV,
                same_sign=False):

    muons = make_rd_detached_muons(
        name='rd_detached_muons_{hash}',
        pt_min=pt_muon_min,
        p_min=p_muon_min,
        mipchi2dvprimary_min=ipchi2_muon_min,
        #        pid=F.require_all(F.ISMUON, F.PID_MU > pidmu_muon_min))
        pid=(F.PID_MU > pidmu_muon_min))  # keep the IsMuon out for the moment

    DecayDescriptor = 'J/psi(1S) -> mu+ mu-'
    if same_sign: DecayDescriptor = '[J/psi(1S) -> mu+ mu+]cc'

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.PT > pt_dimuon_min,
    )

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    return ParticleCombiner([muons, muons],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_dielectron(name='rd_detached_dielectron_{hash}',
                    pid_e_min=2.,
                    pt_e_min=0.35 * GeV,
                    ipchi2_e_min=9.,
                    parent_id="J/psi(1S)",
                    opposite_sign=True,
                    pt_diE_min=0. * MeV,
                    adocachi2cut_max=169.,
                    bpvvdchi2_min=30.,
                    vfaspfchi2ndof_max=9.,
                    am_min=0. * MeV,
                    am_max=6000. * MeV):

    dielectrons = make_detached_dielectron_with_brem(
        opposite_sign=opposite_sign,
        PIDe_min=pid_e_min,
        pt_e=pt_e_min,
        minipchi2=ipchi2_e_min,
        dielectron_ID=parent_id,
        pt_diE=pt_diE_min,
        m_diE_min=am_min,
        m_diE_max=am_max,
        adocachi2cut=adocachi2cut_max,
        bpvvdchi2=bpvvdchi2_min,
        vfaspfchi2ndof=vfaspfchi2ndof_max)
    code = F.require_all(F.MASS > am_min, F.MASS < am_max)
    return ParticleFilter(dielectrons, F.FILTER(code), name=name)


def make_b_to_Xpbar_dilepton(
        dileptons,
        Xs,
        pbars,
        pvs,
        name="rd_b_to_Xpbar_dilepton",
        descriptor="B_s0 -> J/psi(1S) p+ p~-",
        # Xpbar intermediate cuts
        am_Xpbar_min=1800. * MeV,
        am_Xpbar_max=6000. * MeV,
        am_Xpbar_scale_min=0.8,
        am_Xpbar_scale_max=1.2,
        min_Xpbar_pt=0 * MeV,
        adocachi2cut_Xpbar_max=30.,
        # b cuts
        am_b_min=4000 * MeV,
        am_b_max=6500 * MeV,
        am_b_scale_min=0.8,
        am_b_scale_max=1.2,
        min_b_pt=0 * GeV,
        vchi2pdof_b_max=25,
        bpvipchi2_b_max=25,
        FDchi2_b_min=100,
        dira_b_min=0.999):
    '''builder for resonant and non-resonant B(s)ppbarmumu and B(s)ppbaree'''

    combination_code = F.require_all(
        in_range(am_Xpbar_min * am_Xpbar_scale_min,
                 F.SUBCOMB(Functor=F.MASS, Indices=[2, 3]),
                 am_Xpbar_max * am_Xpbar_scale_max),
        F.SUBCOMB(Functor=F.PT, Indices=[2, 3]) > min_Xpbar_pt,
        F.SUBCOMB(
            Functor=F.MAXDOCACHI2CUT(adocachi2cut_Xpbar_max), Indices=[2, 3]),
        in_range(am_b_min * am_b_scale_min, F.MASS, am_b_max * am_b_scale_max),
        F.PT > min_b_pt)

    vertex_code = F.require_all(
        in_range(am_Xpbar_min, F.SUBCOMB(Functor=F.MASS, Indices=[2, 3]),
                 am_Xpbar_max),
        in_range(am_b_min, F.MASS, am_b_max),
        F.BPVIPCHI2(pvs) < bpvipchi2_b_max,
        F.BPVDIRA(pvs) > dira_b_min,
        F.BPVFDCHI2(pvs) > FDchi2_b_min,
        F.CHI2DOF < vchi2pdof_b_max,
    )

    return ParticleCombiner([dileptons, Xs, pbars],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            name=name,
                            CompositeCut=vertex_code)
