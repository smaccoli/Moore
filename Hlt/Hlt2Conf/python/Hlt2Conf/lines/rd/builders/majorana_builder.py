###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of exotic majorana neutrino search builders for the B+/Bc+ -> lepton Majorana, where Majorana -> lepton + pion. 
Builders description:
+ make_majorana_lepton : a default filter for the majorana lepton. TODO: When F.CHILD ready for the DD daughters the pt cut should be loosen and a tighter cut should be applied after vertex fix in make_majorana builder. Leads to a minor correction.
+ make_majorana : a default builder of a majorana candidate. TODO: see comment above
+ make_bhadron_majorana : a default builder of a charged b. TODO: add the IPCHI2 cut, when functor works. 

TO DO:
    - replace F.BPVDIRA(pvs) by ipchi2

Contacts: Lera Lukashenko valeriia.lukashenk0@cern.ch
"""

from GaudiKernel.SystemOfUnits import MeV
import Functors as F
from Functors.math import in_range

from PyConf import configurable

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_filter_tracks
from Hlt2Conf.algorithms_thor import ParticleCombiner


@configurable
def make_majorana_lepton(leptons,
                         pvs,
                         name="standard_lepton_for_majorana",
                         pt_min=500 * MeV,
                         p_min=0 * MeV,
                         mipchi2dvprimary_min=25.0,
                         pid=None):
    """
    Filter default leptons for HNL
    """
    return make_filter_tracks(
        make_particles=leptons,
        make_pvs=pvs,
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_majorana(pions,
                  leptons,
                  name='Generic_Majorana',
                  descriptor='',
                  am_min=200 * MeV,
                  am_max=7000 * MeV,
                  adocachi2=16.,
                  pt_min=700 * MeV,
                  vtxchi2_max=9.):
    """
    Make HNL -> lep +  pi. 
    """
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2))
    majorana_code = F.require_all(F.PT > pt_min, F.CHI2 < vtxchi2_max)

    return ParticleCombiner([leptons, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=majorana_code)


@configurable
def make_bhadron_majorana(majoranas,
                          leptons,
                          pvs,
                          name='Generic_B_2_Majorana',
                          descriptor='',
                          am_min=4300 * MeV,
                          am_max=7200 * MeV,
                          m_min=4500 * MeV,
                          m_max=6800 * MeV,
                          adocachi2=25.,
                          vtxchi2_max=9.,
                          mipchi2_max=16.,
                          bpvdls_min=4.,
                          dira_min=0.):
    """
    Make B-> lep + HNL
    """
    #majoranas = make_majorana()
    #leptons = make_majorana()
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2))
    b_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.BPVDLS(pvs()) > bpvdls_min, F.CHI2 < vtxchi2_max,
        F.BPVDIRA(pvs()) > dira_min,
        F.MINIPCHI2(pvs()) < mipchi2_max)
    return ParticleCombiner([majoranas, leptons],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=b_code)
