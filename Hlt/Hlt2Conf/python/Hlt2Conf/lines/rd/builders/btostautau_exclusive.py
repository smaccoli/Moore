###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiKernel.SystemOfUnits import MeV
import Functors as F
from Functors.math import in_range

from Hlt2Conf.algorithms_thor import ParticleCombiner


def make_phi(kaons1,
             kaons2,
             pvs,
             comb_m_max=3650 * MeV,
             comb_pt_min=400 * MeV,
             vchi2pdof_max=4,
             bpvfdchi2_min=16,
             dira_min=0.96,
             decay_descriptor="[phi(1020) -> K+ K-]cc",
             name="rd_dikaons_for_btostautau"):
    """Builder for X -> K K decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.PT > comb_pt_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner([kaons1, kaons2],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)


def make_kst(kaons,
             pions,
             pvs,
             comb_m_min=750 * MeV,
             comb_m_max=3550 * MeV,
             comb_pt_min=800 * MeV,
             vchi2pdof_max=3,
             bpvfdchi2_min=75,
             dira_min=0.96,
             decay_descriptor="[K*(892)0 -> K+ pi-]cc",
             name="rd_kpi_for_btostautau"):
    """Builder for X -> K pi decays"""
    combination_code = in_range(comb_m_min, F.MASS, comb_m_max)
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.PT > comb_pt_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner([kaons, pions],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)


def make_lst(protons,
             kaons,
             pvs,
             comb_m_max=5000 * MeV,
             comb_pt_min=600 * MeV,
             vchi2pdof_max=4,
             bpvfdchi2_min=16,
             dira_min=0.96,
             decay_descriptor="[Lambda(1520)0 -> p+ K-]cc",
             name="rd_pk_for_btostautau"):
    """Builder for X -> p K decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.PT > comb_pt_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner([protons, kaons],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)


def make_dimuon(muons1,
                muons2,
                pvs,
                comb_m_max=4500 * MeV,
                vchi2pdof_max=25,
                dira_min=0,
                decay_descriptor="[D0 -> mu+ mu-]cc",
                name="rd_dimuons_for_btostautau"):
    """Builder for detached dimuons"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner([muons1, muons2],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)


def make_bs(phis,
            dimuons,
            pvs,
            comb_m_max=7500 * MeV,
            vchi2pdof_max=150,
            dira_min=0.999,
            name="rd_make_bs_to_kktautau"):
    """Builder for Bs -> K K tau (-> mu nu nu) tau (-> mu nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner([dimuons, phis],
                            DecayDescriptor="[B_s0 -> D0 phi(1020)]cc",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)


def make_bu(kaons,
            dimuons,
            pvs,
            comb_m_max=7000 * MeV,
            vchi2pdof_max=75,
            dira_min=0.999,
            name="rd_make_bu_to_ktautau"):
    """Builder for Bu -> K tau (-> mu nu nu) tau (-> mu nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner([dimuons, kaons],
                            DecayDescriptor="[B+ -> D0 K+]cc",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)


def make_lb(pks,
            dimuons,
            pvs,
            comb_m_max=7750 * MeV,
            vchi2pdof_max=150,
            dira_min=0.999,
            name="rd_make_lb_to_pktautau"):
    """Builder for Lb -> p K tau (-> mu nu nu) tau (-> mu nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        [dimuons, pks],
        DecayDescriptor="[Lambda_b0 -> D0 Lambda(1520)0]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name)


def make_bd(ksts,
            dimuons,
            pvs,
            comb_m_max=7250 * MeV,
            vchi2pdof_max=100,
            dira_min=0.999,
            name="rd_make_bd_to_kpitautau"):
    """Builder for Bd -> K pi tau (-> mu nu nu) tau (-> mu nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner([dimuons, ksts],
                            DecayDescriptor="[B0 -> D0 K*(892)0]cc",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)


def make_bs_to_kstkst(ksts,
                      dimuons,
                      pvs,
                      comb_m_max=7000 * MeV,
                      kstkst_m_max=3500 * MeV,
                      vchi2pdof_max=150,
                      dira_min=0.995,
                      docachi2_max=25.0,
                      decay_descriptor="[B_s0 -> K*(892)0 K*(892)~0 D0]cc",
                      name="rd_make_bs_to_kstksttautau"):
    """Builder for Bs -> Kst Kst tau (-> mu nu nu) tau (-> mu nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    two_body_combination_code = F.require_all(F.MASS < kstkst_m_max,
                                              F.MAXDOCACHI2CUT(docachi2_max))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner([ksts, ksts, dimuons],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code,
                            Combination12Cut=two_body_combination_code,
                            CompositeCut=vertex_code,
                            name=name)
