###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Hlt2 particle filters/combiners for the following:
exclusive B -> hh gamma builder

author: Izaac Sanderswood
date: 25.05.22
"""
from GaudiKernel.SystemOfUnits import MeV
import Functors as F
from Functors.math import in_range

from PyConf import configurable

from Hlt2Conf.algorithms_thor import ParticleCombiner


@configurable
def make_b2xgamma_excl(intermediate,
                       photons,
                       pvs,
                       descriptor,
                       dira_min,
                       name,
                       comb_m_min=4200 * MeV,
                       comb_m_max=6400 * MeV,
                       pt_min=2000 * MeV,
                       bpv_ipchi2_max=12):
    combination_code = F.require_all(
        F.PT > pt_min,
        in_range(comb_m_min, F.MASS, comb_m_max),
    )
    vertex_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.PT > pt_min,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < bpv_ipchi2_max)
    # return particle combiner
    return ParticleCombiner(
        name=name,
        Inputs=[intermediate, photons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
