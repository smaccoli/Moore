###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of radiative inclusive builders
"""

import math
import Functors as F
from Functors.math import in_range, log

from Hlt2Conf.algorithms import ParticleContainersMerger
from Hlt2Conf.algorithms_thor import (
    ParticleFilter,
    ParticleCombiner,
)
from Hlt2Conf.standard_particles import make_long_kaons, make_photons
from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import configurable

####################################
# basic builders                   #
####################################
# Definition of inclusive_radiative_b basic particles: hadrons, photons, electrons


@configurable
def filter_electrons(particles,
                     pvs,
                     pt_min=0.1 * GeV,
                     name='rd_rad_incl_electrons_{hash}'):
    """Returns electrons for inclusive_radiative_b selection """
    code = F.require_all(F.PT > pt_min)
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_photons(particles,
                   pvs,
                   pt_min=2.0 * GeV,
                   p_min=5.0 * GeV,
                   name='rd_rad_incl_photons_{hash}'):
    """Returns photons for inclusive_radiative_b selection"""
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_basic_hadrons(particles,
                         pvs,
                         pt_min=1.0 * GeV,
                         pt_max=25.0 * GeV,
                         ipchi2_min=7.4,
                         trchi2_max=2.5,
                         trgp_max=0.2,
                         param1=1.0,
                         param2=1.0,
                         param3=1.1,
                         name='rd_rad_incl_charged_hadrons_{hash}'):
    """Returns basic hadrons with the inclusive_radiative_b selections"""
    difficult_cut = F.require_all(
        in_range(pt_min, F.PT, pt_max),
        log(F.BPVIPCHI2(pvs)) >
        (param1 / ((F.PT / GeV - param2)**2) +
         (param3 / pt_max) * (pt_max - F.PT) + math.log(ipchi2_min)),
    )  # mimicking track mva HLT1 cut, IP chi2 over a hyperbola-like function of PT

    code = F.require_all(
        # F.CHI2DOF < trchi2_max, # <- removed for the early data taking in 2022
        # F.GHOSTPROB < trgp_max, # <- removed for the early data taking in 2022
        ((F.PT > pt_max) & (F.BPVIPCHI2(pvs) > ipchi2_min)) |
        (difficult_cut), )

    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_third_hadrons(particles,
                         pvs,
                         pt_min=0.2 * GeV,
                         p_min=3.0 * GeV,
                         trchi2_max=3.0,
                         ipchi2_min=4.0,
                         name='rd_rad_incl_third_hadrons_{hash}'):
    """Returns third hadron used in HHH inclusive_radiative_b selections"""
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        # F.CHI2DOF < trchi2_max, # <- removed for the early data taking in 2022
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_extra_charged_hadrons(particles,
                                 pvs,
                                 pt_min=0.8 * GeV,
                                 trchi2_max=2.5,
                                 trgp_max=0.2,
                                 ipchi2_min=8.0,
                                 name='rd_rad_incl_extra_ch_hadons_{hash}'):
    """Returns extra charged hadrons with the inclusive_radiative_b selections"""
    code = F.require_all(
        F.PT > pt_min,
        # F.GHOSTPROB < trgp_max, # <- removed for the early data taking in 2022
        # F.CHI2DOF < trchi2_max, # <- removed for the early data taking in 2022
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_neutral_hadrons(particles,
                           pvs,
                           pt_min=0.5 * GeV,
                           ipchi2_min=0.0,
                           name='rd_rad_incl_neutral_hadrons_{hash}_{hash}'):
    """Returns extra neutral hadrons with the inclusive_radiative_b selections"""
    code = F.require_all(
        F.PT > pt_min,
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_pi0(particles,
               pvs,
               pt_min=1.6 * GeV,
               name='rd_rad_incl_extra_pi0s_{hash}'):
    """Returns extra pi0s for inclusive_radiative_b selection"""
    code = F.require_all(F.PT > pt_min)
    return ParticleFilter(particles, F.FILTER(code), name=name)


####################################
# hh builder                       #
####################################
# Definition of HH builder for inclusive_radiative_b selections
@configurable
def make_hh(inputs,
            pvs,
            descriptors,
            pt_min=2.0 * GeV,
            doca_chi2_max=1000.,
            m_max=10.0 * GeV,
            vtx_chi2_max=1000.,
            bpvfdchi2_min=16.,
            eta_min=2.,
            eta_max=5.,
            name='rd_rad_incl_hh_{hash}'):
    """Builds two-hadron particle for inclusive_radiative_b"""
    combination_code = F.require_all(
        F.PT > pt_min,
        F.MAXDOCACHI2CUT(doca_chi2_max),
        F.MASS < m_max,
    )

    vertex_code = F.require_all(
        F.CHI2 < vtx_chi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        in_range(eta_min, F.BPVETA(pvs), eta_max),
    )

    hh = []
    for input, descriptor in zip(
            inputs,
            descriptors,
    ):
        subname = "rd_rad_incl_K*_{hash}"
        if ("K+ K-" in descriptor):
            subname = "rd_rad_incl_K*0ToK+K-_{hash}"
        if ("K+ KS0" in descriptor):
            subname = "rd_rad_incl_K*+ToK+KS0_{hash}"
        if ("K+ Lambda0" in descriptor):
            subname = "rd_rad_incl_K*+ToK+Lambda0_{hash}"
        if ("K- Lambda0" in descriptor):
            subname = "rd_rad_incl_K*-ToK-Lambda0_{hash}"
        hh.append(
            ParticleCombiner(
                input,
                DecayDescriptor=descriptor,
                CombinationCut=combination_code,
                CompositeCut=vertex_code,
                name=subname))

    return ParticleContainersMerger(hh, name=name)


####################################
# hhh builder                      #
####################################
# Definition of (HH)H builder for inclusive_radiative_b selections.
@configurable
def make_hhh(inputs,
             pvs,
             descriptors,
             pt_min=2.0 * GeV,
             doca_chi2_max=1000.,
             m_max=10.0 * GeV,
             vtx_chi2_max=1000.,
             bpvfdchi2_min=16.,
             eta_min=2.,
             eta_max=5.,
             dira_min=0.,
             name='rd_rad_incl_hhh_{hash}'):
    """Builds HHH particle as HH + H for inclusive_radiative_b"""
    combination_code = F.require_all(
        F.PT > pt_min,
        F.MAXDOCACHI2CUT(doca_chi2_max),
        F.MASS < m_max,
    )

    vertex_code = F.require_all(
        F.CHI2 < vtx_chi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        in_range(eta_min, F.BPVETA(pvs), eta_max),
        F.BPVDIRA(pvs) > dira_min,
    )

    hhh = []
    for input, descriptor in zip(inputs, descriptors):
        subname = 'rd_rad_incl_D*_{hash}'
        if ("K*(892)0 K+" in descriptor):
            subname = 'rd_rad_incl_D*ToK*0K+_{hash}'
        if ("K*(892)0 K-" in descriptor):
            subname = 'rd_rad_incl_D*ToK*0K-_{hash}'
        if ("K*(892)0 KS0" in descriptor):
            subname = 'rd_rad_incl_D*ToK*0KS0_{hash}'
        if ("K*(892)+ KS0" in descriptor):
            subname = 'rd_rad_incl_D*ToK*+KS0_{hash}'
        if ("K*(892)0 Lambda0" in descriptor):
            subname = 'rd_rad_incl_D*ToK*0Lambda0_{hash}'
        if ("K*(892)0 Lambda~0" in descriptor):
            subname = 'rd_rad_incl_D*ToK*0Lambda~0_{hash}'
        if ("K*(892)+ Lambda0" in descriptor):
            subname = 'rd_rad_incl_D*ToK*+Lambda0_{hash}'
        if ("K*(892)+ Lambda~0" in descriptor):
            subname = 'rd_rad_incl_D*ToK*+Lambda~0_{hash}'
        hhh.append(
            ParticleCombiner(
                input,
                DecayDescriptor=descriptor,
                CombinationCut=combination_code,
                CompositeCut=vertex_code,
                name=subname))
    return ParticleContainersMerger(hhh, name=name)


####################################
# Converted photons                #
####################################
# Definition of inclusive_radiative_b gamma->ee builder


@configurable
def make_gamma_ee(electrons,
                  pvs,
                  pt_min=1.5 * GeV,
                  p_min=0.0 * GeV,
                  m_max=50.0 * MeV,
                  name='rd_rad_incl_converted_photons_{hash}'):

    combination_code = F.require_all(
        F.PT > pt_min,
        F.MASS < m_max,
        F.P > p_min,
    )

    vertex_code = F.require_all(F.ALL)

    return ParticleCombiner(
        [electrons, electrons],
        DecayDescriptor="gamma -> e+ e-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


####################################
# B builder                        #
####################################
# Definition of HH builder for inclusive_radiative_b selections


@configurable
def make_presel_b(inputs,
                  pvs,
                  descriptors,
                  pt_min=2.0 * GeV,
                  vtx_chi2_max=1000.,
                  eta_min=2.,
                  eta_max=5.,
                  corrm_min=1.0 * GeV,
                  corrm_max=11.0 * GeV,
                  dira_min=0.,
                  bpvvdchi2_min=0.,
                  name='rd_rad_incl_presel_B_{hash}'):
    """Builds B->X gamma for inclusive_radiative_b selection"""
    combination_code = F.require_all(
        F.PT > pt_min,
        F.MASS < corrm_max,
    )

    vertex_code = F.require_all(
        F.CHI2DOF < vtx_chi2_max,
        in_range(eta_min, F.BPVETA(pvs), eta_max),
        in_range(corrm_min, F.BPVCORRM(pvs), corrm_max),
        F.BPVDIRA(pvs) > dira_min,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )

    presel_b = []
    for descriptor in descriptors:
        subname = 'rd_rad_incl_preselB_{hash}'
        if ("K*(892)0 gamma" in descriptor):
            subname = 'rd_rad_incl_preselB0ToK*0Gamma_{hash}'
        if ("K*(892)+ gamma" in descriptor):
            subname = 'rd_rad_incl_preselB+ToK*+Gamma_{hash}'
        if ("D*(2007)0 gamma" in descriptor):
            subname = 'rd_rad_incl_preselB0ToD*0Gamma_{hash}'
        if ("D*(2010)+ gamma" in descriptor):
            subname = 'rd_rad_incl_preselB+ToD*+Gamma_{hash}'
        presel_b.append(
            ParticleCombiner(
                inputs,
                DecayDescriptor=descriptor,
                CombinationCut=combination_code,
                CompositeCut=vertex_code,
                name=subname))

    return ParticleContainersMerger(presel_b, name=name)


####################################
# extra outputs                    #
####################################
# Definition of HH builder for inclusive_radiative_b selections


def extra_charged_hadrons(particles=make_long_kaons,
                          pvs=make_pvs,
                          pt_min=1.2 * GeV,
                          ipchi2_min=150.,
                          name='rd_rad_incl_extra_ch_hadrons_{hash}'):
    return filter_extra_charged_hadrons(
        particles(),
        pvs(),
        pt_min=pt_min,
        ipchi2_min=ipchi2_min,
        name=name,
    )


def extra_neutral_hadrons(particles,
                          pvs=make_pvs,
                          pt_min=1.2 * GeV,
                          ipchi2_min=5.0,
                          name='rd_rad_incl_extra_neutral_hadrons_{hash}'):
    return filter_neutral_hadrons(
        particles,
        pvs(),
        pt_min=pt_min,
        ipchi2_min=ipchi2_min,
        name=name,
    )


def extra_gamma(particles=make_photons,
                pvs=make_pvs,
                pt_min=3.3 * GeV,
                name='rd_rad_incl_extra_photons_{hash}'):
    return filter_photons(
        particles(),
        pvs(),
        pt_min=pt_min,
        p_min=0.,
        name=name,
    )


def extra_pi0(particles,
              pvs=make_pvs,
              pt_min=3.3 * GeV,
              name='rd_rad_incl_extra_pi0s_{hash}'):
    return filter_pi0(
        particles,
        pvs(),
        pt_min=pt_min,
        name=name,
    )
