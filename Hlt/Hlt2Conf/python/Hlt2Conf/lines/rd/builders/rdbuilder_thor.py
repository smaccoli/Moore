###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the common RD builders in the ThOr framework.

** The list of currently available filters and builders: **

-- Charged tracks --
make_filter_tracks : generic builder of long (pion) tracks
make_rd_prompt_{muons,electrons,pions,kaons,protons} - generic filters for each charged particle (long tracks)
make_rd_prompt_pions_no_rich - generic filter for pions without requiring RICH
make_rd_detached_{muons,electrons,pions,kaons,protons} - generic filters for each charged particle (long tracks)
make_rd_has_rich_detached_{pions,kaons,protons} – long-track hadron filters with RICH info
make_rd_detached_down_{pions,kaons,protons} - downstream-track hadron filters for V0 and hyperon candidates

-- Neutrals -- only dummy selections for the moment!
make_rd_photons
make_rd_resolved_pi0s
make_rd_merged_pi0s

-- V0 particles --
make_rd_ks0_lls - KS0 builder from long-track pions
make_rd_ks0_dds - KS0 builder from downstream-track pions
make_rd_lambda_lls - Lambda0 builder from long tracks
make_rd_lambda_dds - Lambda0 builder from downstream tracks

-- Popular intermediate hadrons --
make_rd_detached_kstar0s - K*->K+pi- builder
make_rd_detached_phis - phi->K+K- builder
### make_rd_detached_rho0 - rho->pi+pi- builder #removed for the moment

-- Dileptons --
make_rd_{prompt,detached}_dimuon - mu+mu- combination
make_rd_{prompt,detached}_dielectron - e+e- combination with the proper brem recovery
make_rd_{prompt,detached}_mue - mue (same- or opposite-sign) combination

-- Other --
make_rd_tauons_hadronic_decay - tau->3pi builder
make_rd_detached_dihadron - hh builder (same- or opposite-sign)
"""
from GaudiKernel.SystemOfUnits import GeV, MeV, ps
from RecoConf.reconstruction_objects import make_pvs

import Functors as F
from Functors.math import in_range

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

from PyConf import configurable

from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions,
    make_has_rich_long_kaons,
    make_has_rich_long_protons,
    make_long_pions,
    make_long_kaons,
    make_long_protons,
    #make_has_rich_down_pions, make_has_rich_down_kaons, make_has_rich_down_protons, # we don't usually need RICH info in fact for D tracks
    make_down_pions,
    make_down_kaons,
    make_down_protons,
    #make_KsLL, make_KsDD, make_LambdaLL, make_LambdaDD, # we build our own versions below
    make_long_muons,
    make_long_electrons_with_brem,
    make_long_electrons_no_brem,
    make_photons,
    make_resolved_pi0s,
    make_merged_pi0s,
    # make_detached_mumu,
    _make_dielectron_with_brem,
    make_detached_dielectron_with_brem,
    make_detached_mue_with_brem)

#define the masses of composite particles.
_K0_M = 497.611 * MeV
_Lambda0_M = 1115.683 * MeV

####################################
# Charged hadron selections        #
####################################


@configurable
def make_filter_tracks(
        make_particles=make_has_rich_long_pions,
        make_pvs=make_pvs,
        name="rd_has_rich_long_pions",
        pt_min=250. * MeV,
        p_min=2.0 * GeV,
        trchi2dof_max=3,  #TBC with Reco
        trghostprob_max=0.4,  #TBC with Reco
        mipchi2dvprimary_min=None,
        pid=None):
    """
    Build generic long tracks.
    """
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        #F.CHI2DOF < trchi2dof_max, # <- removed for the early data taking in 2022
        #F.GHOSTPROB < trghostprob_max # <- removed for the early data taking in 2022
    )

    if pid is not None:
        code &= pid

    if mipchi2dvprimary_min is not None:
        pvs = make_pvs()
        code &= F.MINIPCHI2(pvs) > mipchi2dvprimary_min

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


####################################
# Detached                         #
####################################


@configurable
def make_rd_detached_muons(
        name="rd_detached_muons_{hash}",
        pt_min=250. * MeV,
        p_min=3000. * MeV,
        mipchi2dvprimary_min=3.,  #TBC
        pid=F.require_all(F.ISMUON, F.PID_MU > 0.)):
    """
    Return RD detached muons.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_muons,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_rd_detached_electrons(
        name="rd_detached_electrons_{hash}",
        pt_min=250. * MeV,
        p_min=3000. * MeV,
        mipchi2dvprimary_min=3.,  #TBC
        pid=(F.PID_E > 0.)):
    """
    Return RD detached electrons with brem recovery.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_electrons_with_brem,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_rd_detached_pions(
        name="rd_detached_pions_{hash}",
        p_min=2. * GeV,
        pt_min=250. * MeV,
        mipchi2dvprimary_min=4.,  #TBC
        pid=(F.PID_K <= 2.)):
    """
    Return RD detached pions.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_rd_detached_kaons(
        name="rd_detached_kaons_{hash}",
        p_min=2. * GeV,
        pt_min=250. * MeV,
        mipchi2dvprimary_min=4.,  #TBC
        pid=(F.PID_K > -2.)):
    """
    Return RD detached kaons.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_rd_detached_protons(
        name="rd_detached_protons_{hash}",
        p_min=2. * GeV,
        pt_min=250. * MeV,
        mipchi2dvprimary_min=4.,  #TBC
        pid=(F.PID_P > -2.)):
    """
    Return RD detached protons.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_protons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


# has_rich versions


@configurable
def make_rd_has_rich_detached_pions(
        name="rd_has_rich_detached_pions_{hash}",
        p_min=2. * GeV,
        pt_min=250. * MeV,
        mipchi2dvprimary_min=4.,  #TBC
        pid=(F.PID_K <= 0.)):
    """
    Return RD detached pions with hasRich.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_rd_has_rich_detached_kaons(
        name="rd_has_rich_detached_kaons_{hash}",
        p_min=2. * GeV,
        pt_min=250. * MeV,
        mipchi2dvprimary_min=4.,  #TBC
        pid=(F.PID_K > 0.)):
    """
    Return RD detached kaons with hasRich.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_rd_has_rich_detached_protons(
        name="rd_has_rich_detached_protons_{hash}",
        p_min=8.5 *
        GeV,  #kaon RICH threshold below which we have no p-K separation
        pt_min=250. * MeV,
        mipchi2dvprimary_min=4.,  #TBC
        pid=F.require_all(F.PID_P > 0., F.PID_P - F.PID_K > -2.)):
    """
    Return RD detached protons with hasRich.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_protons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


####################################
# Prompt                           #
####################################


@configurable
def make_rd_prompt_muons(
        name="rd_prompt_muons_{hash}",
        pt_min=250. * MeV,
        p_min=3000. * MeV,
        pid=F.require_all(F.PID_MU > 3.)):  #F.ISMUON, F.PID_MU > 3.
    """
    Return RD prompt muons.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_muons,
        pt_min=pt_min,
        p_min=p_min,
        pid=pid)


@configurable
def make_rd_prompt_electrons(name="rd_prompt_electrons_{hash}",
                             pt_min=250. * MeV,
                             p_min=3000. * MeV,
                             pid=(F.PID_E > 6.)):
    """
    Return RD prompt electrons with brem recovery.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_electrons_with_brem,
        pt_min=pt_min,
        p_min=p_min,
        pid=pid)


@configurable
def make_rd_prompt_pions(name="rd_prompt_pions_{hash}", pid=(F.PID_K < 0.)):
    """
     Return RD prompt pions.
     """
    return make_filter_tracks(
        make_particles=make_has_rich_long_pions, name=name, pid=pid)


@configurable
def make_rd_prompt_pions_no_rich(name="rd_prompt_pions_{hash}", pid=None):
    """
     Return RD prompt pions without RICH requirement.
     """
    return make_filter_tracks(
        make_particles=make_long_pions, name=name, pid=pid)


@configurable
def make_rd_prompt_kaons(name="rd_prompt_kaons_{hash}", pid=(F.PID_K > 0.)):
    """
     Return RD prompt kaons.
     """
    return make_filter_tracks(
        make_particles=make_has_rich_long_kaons, name=name, pid=pid)


@configurable
def make_rd_prompt_protons(name="rd_prompt_protons_{hash}",
                           p_min=10. * GeV,
                           pid=(F.PID_P > 0.)):
    """
     Return RD prompt protons.
     """
    return make_filter_tracks(
        make_particles=make_has_rich_long_protons,
        name=name,
        p_min=p_min,
        pid=pid)


####################################
# Downstream tracks                #
####################################


def make_rd_detached_down_pions(
        name="rd_detached_down_pions_{hash}",
        mipchi2dvprimary_min=4.,  #this does not work
        pt_min=0. * MeV,
        p_min=0. * GeV,
        pid=None):
    """
    Return RD downstream hadrons with pion mass hypothesis.
    """
    return make_filter_tracks(
        make_particles=make_down_pions,
        name=name,
        #mipchi2dvprimary_min=mipchi2dvprimary_min,
        pt_min=pt_min,
        p_min=pt_min,
        pid=pid)


def make_rd_detached_down_kaons(
        name="rd_detached_down_kaons_{hash}",
        mipchi2dvprimary_min=4.,  #TBC
        pt_min=0. * MeV,
        p_min=0. * GeV,
        pid=None):
    """
    Return RD downstream hadrons with kaon mass hypothesis.
    """
    return make_filter_tracks(
        make_particles=make_down_kaons,
        name=name,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pt_min=pt_min,
        p_min=pt_min,
        pid=pid)


def make_rd_detached_down_protons(
        name="rd_detached_down_protons_{hash}",
        mipchi2dvprimary_min=4.,  #TBC
        pt_min=0. * MeV,
        p_min=0. * GeV,
        pid=None):
    """
    Return RD downstream hadrons with proton mass hypothesis.
    """
    return make_filter_tracks(
        make_particles=make_down_protons,
        name=name,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pt_min=pt_min,
        p_min=pt_min,
        pid=pid)


###################################
# Neutral particles                #
###################################


@configurable
def make_rd_photons(name="rd_photons_{hash}",
                    make_particles=make_photons,
                    IsPhoton_min=0.5,
                    IsNotH_min=0.3,
                    E19_min=0.2,
                    e_min=50. * MeV,
                    et_min=250. * MeV):
    """For the time being just a preliminary selection while the neutrals calibration in progress"""

    code = F.require_all(F.PT > et_min, F.P > e_min,
                         F.IS_PHOTON > IsPhoton_min, F.IS_NOT_H > IsNotH_min,
                         F.CALO_NEUTRAL_1TO9_ENERGY_RATIO > E19_min)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_rd_resolved_pi0s(
        name="rd_resolved_pi0s_{hash}",
        make_particles=make_resolved_pi0s,
        pt_min=250. * MeV,
        cl_min=0.,
        # photon_args={
        #     'ConfLevelCut': 0.1,
        #     'PtCut': 250. * MeV
        # },
):
    """For the time being just a dummy selection"""

    # code =F.require_all('PT > {pt_min}', 'P > {p_min}').format(
    #     pt_min=pt_min, p_min=p_min)
    #
    # return ParticleFilter(
    #     make_particles(photon_args=photon_args), Code=code, name=name)
    code = F.PT > pt_min
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_rd_merged_pi0s(name="rd_merged_pi0s_{hash}",
                        make_particles=make_merged_pi0s,
                        pt_min=250. * MeV,
                        cl_min=0.):
    """For the time being just a dummy selection"""

    code = F.PT > pt_min
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


####################################
# V0 particles and alike           #
####################################
# they are in principle defined in the Hlt2 standard_particles, however there is no way to tune IPCHI2 cuts on their children with the current ThOr framework.


@configurable
def make_rd_ks0_lls(
        name="rd_ks0_lls_{hash}",
        make_pions=make_rd_detached_pions,
        make_pvs=make_pvs,
        mass_window_comb=50. * MeV,
        mass_window=35. * MeV,
        pi_p_min=2. * GeV,
        pi_pt_min=0. * GeV,  # has to be 0 !!!
        pi_ipchi2_min=9.,
        pt_min=0 * MeV,
        adocachi2cut=30.,
        bpvvdchi2_min=4.,
        bpvltime_min=1. * ps,
        vchi2pdof_max=25.):
    '''
    Build Long-Long KS0 candidates. Approximately corresponding to the Run2
    "StdVeryLooseKsLL" cuts.
    '''

    pions = make_pions(
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        mipchi2dvprimary_min=pi_ipchi2_min,
        pid=None)  # important to get rid of any PID cuts!
    descriptor = 'KS0 -> pi+ pi-'
    combination_code = F.require_all(
        in_range(_K0_M - mass_window_comb, F.MASS, _K0_M + mass_window_comb),
        F.MAXDOCACHI2CUT(adocachi2cut))
    pvs = make_pvs()
    vertex_code = F.require_all(
        in_range(_K0_M - mass_window, F.MASS, _K0_M + mass_window),
        F.PT > pt_min, F.CHI2DOF < vchi2pdof_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    return ParticleCombiner([pions, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_ks0_dds(
        name="rd_ks0_dds_{hash}",
        make_pions=make_rd_detached_down_pions,
        make_pvs=make_pvs,
        mass_window_comb=80. * MeV,
        mass_window=64. * MeV,
        pi_p_min=2. * GeV,
        pi_pt_min=0.,  # has to be 0 !!!
        #pi_ipchi2_min=4,
        adocachi2cut=30.,
        bpvvdchi2_min=4.,
        bpvltime_min=1. * ps,
        vchi2pdof_max=25.):
    '''
    Build Down-Down KS0 candidates. Approximately corresponding to the Run2
    "StdLooseKsDD" cuts.
    '''

    pions = make_pions(
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        #mipchi2dvprimary_min=pi_ipchi2_min,
        pid=None)
    descriptor = 'KS0 -> pi+ pi-'
    combination_code = F.require_all(
        in_range(_K0_M - mass_window_comb, F.MASS, _K0_M + mass_window_comb),
        F.MAXDOCACHI2CUT(adocachi2cut))
    pvs = make_pvs()
    vertex_code = F.require_all(
        in_range(_K0_M - mass_window, F.MASS, _K0_M + mass_window),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    return ParticleCombiner([pions, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_lambda_lls(
        name="rd_lambda_lls_{hash}",
        make_protons=make_rd_detached_protons,
        make_pions=make_rd_detached_pions,
        make_pvs=make_pvs,
        mass_window_comb=50. * MeV,
        mass_window=35. * MeV,
        lambda_pt_min=0. * MeV,
        pi_p_min=2. * GeV,
        p_p_min=2. * GeV,
        p_pt_min=0.,  # recommended to be small
        pi_pt_min=0.,  # has to be 0 !!!
        pi_ipchi2_min=6.,
        p_ipchi2_min=9.,
        adocachi2cut=30.,
        bpvvdchi2_min=4.,
        bpvltime_min=1. * ps,
        vchi2pdof_max=25.):
    '''
    Build Long-Long Lambda candidates. Approximately corresponding to the Run2
    "StdVeryLooseLambdaLL" cuts.
    '''

    protons = make_protons(
        p_min=p_p_min,
        pt_min=p_pt_min,
        mipchi2dvprimary_min=p_ipchi2_min,
        pid=None)  # important to get rid of any PID cuts!
    pions = make_pions(
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        mipchi2dvprimary_min=pi_ipchi2_min,
        pid=None)
    descriptor = '[Lambda0 -> p+ pi-]cc'
    combination_code = F.require_all(
        in_range(_Lambda0_M - mass_window_comb,
                 F.MASS, _Lambda0_M + mass_window_comb),
        F.MAXDOCACHI2CUT(adocachi2cut))
    pvs = make_pvs()
    vertex_code = F.require_all(
        in_range(_Lambda0_M - mass_window, F.MASS, _Lambda0_M + mass_window),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min, F.PT > lambda_pt_min)
    return ParticleCombiner([protons, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_lambda_dds(
        name="rd_lambda_dds_{hash}",
        make_protons=make_rd_detached_down_protons,
        make_pions=make_rd_detached_down_pions,
        make_pvs=make_pvs,
        mass_window_comb=80. * MeV,
        mass_window=64. * MeV,
        lambda_pt_min=0. * MeV,
        pi_p_min=2. * GeV,
        p_p_min=2. * GeV,
        p_pt_min=0.,  # recommended to be small
        pi_pt_min=0.,  # has to be 0 !!!
        pi_ipchi2_min=6,
        p_ipchi2_min=9,
        adocachi2cut=30.,
        bpvvdchi2_min=4.,
        bpvltime_min=1. * ps,
        vchi2pdof_max=25.,
):
    '''
    Build Down-Down Lambda candidates. Approximately corresponding to the Run2
    "StdLooseLambdaDD" cuts.
    '''

    protons = make_protons(
        p_min=p_p_min,
        pt_min=p_pt_min,
        mipchi2dvprimary_min=p_ipchi2_min,
        pid=None)  # important to get rid of any PID cuts!
    pions = make_pions(
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        mipchi2dvprimary_min=pi_ipchi2_min,
        pid=None)
    descriptor = '[Lambda0 -> p+ pi-]cc'
    combination_code = F.require_all(
        in_range(_Lambda0_M - mass_window_comb,
                 F.MASS, _Lambda0_M + mass_window_comb),
        F.MAXDOCACHI2CUT(adocachi2cut))
    pvs = make_pvs()
    vertex_code = F.require_all(
        in_range(_Lambda0_M - mass_window, F.MASS, _Lambda0_M + mass_window),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min, F.PT > lambda_pt_min)
    return ParticleCombiner([protons, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


####################################
# 2-body hadron combinations       #
####################################


@configurable
def make_Kstps_with_pi0s(
        name="Kstp_Maker_resolved_Pi0s_{hash}",
        min_ipchi2_k=4.,
        Kstp_PT=0.,
        Kstp_max_vtxchi2=36.,
        PIDK_K_min=-4.,
        pt_K_min=250. * MeV,
        p_K_min=1_000. * MeV,
        pt_pi_min=250. * MeV,
        am_min=600. * MeV,
        am_max=1_200. * MeV,
        low_factor=0.5,
        high_factor=1.5,
        pi0_type="resolved",  # either "resolved" or "merged".
):

    ### make K*+ -> K+ pi0 candidate ###

    kaons = make_rd_detached_kaons(
        mipchi2dvprimary_min=min_ipchi2_k,
        pid=(F.PID_K > PIDK_K_min),
        pt_min=pt_K_min,
        p_min=p_K_min)

    if pi0_type == "resolved":
        pions = make_rd_resolved_pi0s(pt_min=pt_pi_min)

    elif pi0_type == "merged":
        pions = make_rd_merged_pi0s(pt_min=pt_pi_min)

    else:
        raise Exception(
            f"Error: argument pi0_type was {pi0_type}, pi0_type argument can be either: 'resolved' or 'merged'"
        )

    combination_code = F.require_all(
        in_range(low_factor * am_min, F.MASS, high_factor * am_max),
        F.SUM(F.PT) > Kstp_PT * MeV,
    )

    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.CHI2DOF < Kstp_max_vtxchi2,
    )

    return ParticleCombiner([kaons, pions],
                            DecayDescriptor="[K*(892)+ -> K+ pi0]cc",
                            CombinationCut=combination_code,
                            name=name,
                            ParticleCombiner="ParticleAdder",
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_kstar0s(
        name="rd_detached_kstar0s_{hash}",
        make_rd_detached_pions=make_rd_has_rich_detached_pions,
        make_rd_detached_kaons=make_rd_has_rich_detached_kaons,
        make_pvs=make_pvs,
        am_min=592. * MeV,
        am_max=1192. * MeV,
        pi_p_min=2. * GeV,
        pi_pt_min=100. * MeV,
        pi_ipchi2_min=4.,
        pi_pid=(F.PID_K < 8.),
        k_p_min=2. * GeV,
        k_pt_min=100. * MeV,
        k_ipchi2_min=4.,
        k_pid=(F.PID_K > -2.),
        kstar0_pt_min=400. * MeV,
        adocachi2cut=30.,
        vchi2pdof_max=25.,
        same_sign=False):
    '''
    Build Kstar0 candidates. Approximately corresponding to the Run2
    "StdVeryLooseDetachedKstar" cuts.
    '''

    pions = make_rd_detached_pions(
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        mipchi2dvprimary_min=pi_ipchi2_min,
        pid=pi_pid)
    kaons = make_rd_detached_kaons(
        p_min=k_p_min,
        pt_min=k_pt_min,
        mipchi2dvprimary_min=k_ipchi2_min,
        pid=k_pid)
    descriptor = '[K*(892)0 -> K+ pi-]cc'
    if same_sign: descriptor = '[K*(892)0 -> K+ pi+]cc'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2cut))
    #pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.PT > kstar0_pt_min)
    return ParticleCombiner([kaons, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_phis(
        name="rd_detached_phis_{hash}",
        make_rd_detached_kaons=make_rd_has_rich_detached_kaons,
        make_pvs=make_pvs,
        am_min=950. * MeV,  #the real min is the di-kaon threshold
        am_max=1100. * MeV,
        k_p_min=2. * GeV,
        k_pt_min=100. * MeV,
        k_ipchi2_min=4.,
        k_pid=(F.PID_K > 0.),
        phi_pt_min=400. * MeV,
        adocachi2cut=30.,
        vchi2pdof_max=25.):
    '''
    Build phi(1020) candidates. Approximately corresponding to the Run2
    "StdLooseDetachedPhi2KK" cuts.
    '''

    kaons = make_rd_detached_kaons(
        p_min=k_p_min,
        pt_min=k_pt_min,
        mipchi2dvprimary_min=k_ipchi2_min,
        pid=k_pid)
    descriptor = 'phi(1020) -> K+ K-'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2cut))
    #pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.PT > phi_pt_min)
    return ParticleCombiner([kaons, kaons],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_rho0(
        name="rd_detached_rho0_{hash}",
        make_rd_detached_pions=make_rd_has_rich_detached_pions,
        pi_pt_min=150. * MeV,
        pi_ipchi2_min=4.0,
        pi_pid=(F.PID_K < 0.),
        pt_min=600. * MeV,
        am_min=500. * MeV,
        am_max=1100. * MeV,
        adocachi2cut=10.,
        vchi2pdof_max=9.,
):
    '''
    Build pi+pi- candidates. Approximately corresponding to the Run2
    "StdLooseRho0" cuts but more flexible.
    '''
    pions = make_rd_detached_pions(
        pt_min=pi_pt_min,
        mipchi2dvprimary_min=pi_ipchi2_min,
        pid=pi_pid,
    )
    DecayDescriptor = 'rho(770)0 -> pi+ pi-'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_min,
        F.MAXDOCACHI2CUT(adocachi2cut))

    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max)
    return ParticleCombiner([pions, pions],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


####################################
# Dileptons                        #
####################################


@configurable
def make_prompt_dielectron_with_brem(
        name="prompt_dielectron_with_brem_builder_{hash}",
        electron_maker=make_long_electrons_no_brem,
        opposite_sign=True,
        PIDe_min=5.,
        pt_e=0.25 * GeV,
        trghostprob=0.3,
        dielectron_ID="J/psi(1S)",
        pt_diE=0. * MeV,
        m_diE_min=0. * MeV,
        m_diE_max=6000. * MeV,
        vchi2pdof_max=10.):
    """Make prompt Jpsi -> e+ e- or [Jpsi -> e+ e+ ]cc candidates adding bremsstrahlung correction to the
    electrons. The selection follows make_detached_dielectron_with_brem but without displacement cuts.
    """
    #pvs = make_pvs()
    particles = electron_maker()

    code_e = F.require_all(
        F.PID_E > PIDe_min,
        F.PT > pt_e,
        #F.GHOSTPROB < trghostprob # <- removed for the early data taking in 2022
    )

    prompt_e = ParticleFilter(
        particles, F.FILTER(code_e), name=name + "_electron_{hash}")
    prompt_dielectron_with_brem = _make_dielectron_with_brem(
        prompt_e,
        pt_diE=pt_diE,
        m_diE_min=m_diE_min,
        m_diE_max=m_diE_max,
        m_diE_ID=dielectron_ID,
        opposite_sign=opposite_sign)

    code_dielectron = F.require_all(F.CHI2DOF < vchi2pdof_max)
    return ParticleFilter(
        prompt_dielectron_with_brem,
        F.FILTER(code_dielectron),
        name=name + "_{hash}")


@configurable
def make_rd_prompt_dielectrons(
        name="rd_prompt_dielectrons_{hash}",
        #        opposite_sign=True,
        same_sign=False,
        PIDe_min=5.,
        pt_e_min=0.25 * GeV,
        trghostprob=0.25,
        parent_id="omega(782)",
        min_dilepton_pt=0 * MeV,
        min_dilepton_mass=0 * MeV,
        max_dilepton_mass=12000 * MeV,
        max_vchi2ndof=9.):
    """
    Make the prompt dielectron pair, opposite-sign or same-sign.
    """
    #pvs = make_pvs()
    dileptons = make_prompt_dielectron_with_brem(
        opposite_sign=not same_sign,
        PIDe_min=PIDe_min,
        pt_e=pt_e_min,
        dielectron_ID=parent_id,
        pt_diE=min_dilepton_pt,
        m_diE_min=min_dilepton_mass,
        m_diE_max=max_dilepton_mass,
        vchi2pdof_max=max_vchi2ndof)
    code = F.require_all(
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.PT > min_dilepton_pt)
    return ParticleFilter(dileptons, F.FILTER(code), name=name)


@configurable
def make_rd_prompt_dimuons(
        name="rd_prompt_dimuons_{hash}",
        parent_id='rho(770)0',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=250. * MeV,
        p_muon_min=3000. * MeV,
        pid=F.require_all(F.PID_MU > 3.),  #F.ISMUON, F.PID_MU > 3.
        pid_best=6,
        pt_best=0.5 * GeV,
        p_best=5. * GeV,
        adocachi2cut_max=30.,
        vchi2pdof_max=16.,
        am_min=0. * MeV,
        am_max=12000. * MeV,
        same_sign=False):
    """
    Make the prompt dimuon, opposite-sign or same-sign.
    """
    muons = make_rd_prompt_muons(pt_min=pt_muon_min, p_min=p_muon_min, pid=pid)

    DecayDescriptor = f'{parent_id} -> mu+ mu-'
    if same_sign: DecayDescriptor = f'[{parent_id} -> mu+ mu+]cc'

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_dimuon_min,
        F.MAX(F.PT) > pt_best,
        F.MAX(F.P) > p_best,
        F.MAX(F.PID_MU) > pid_best, F.MAXDOCACHI2CUT(adocachi2cut_max))
    #pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max)
    return ParticleCombiner([muons, muons],
                            name=name + "_combiner_{hash}",
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_prompt_mue(
        name="rd_prompt_mue_{hash}",
        parent_id='f_0(980)',
        pt_dilepton_min=0. * MeV,
        pt_muon_min=300. * MeV,
        p_muon_min=3000. * MeV,
        pt_electron_min=300. * MeV,
        p_electron_min=3000. * MeV,
        pid_muon=F.require_all(F.PID_MU > 3.),  #F.ISMUON, F.PID_MU > 3.
        pid_electron=(F.PID_E > 5.),
        pt_best=0.5 * GeV,
        p_best=5. * GeV,
        adocachi2cut_max=30.,
        vchi2pdof_max=16.,
        am_min=0. * MeV,
        am_max=12000. * MeV,
        same_sign=False):
    """
    Make the prompt dimuon, opposite-sign or same-sign.
    """
    muons = make_rd_prompt_muons(
        pt_min=pt_muon_min, p_min=p_muon_min, pid=pid_muon)

    electrons = make_rd_prompt_electrons(
        name="rd_prompt_electrons_for" + name,
        pt_min=pt_electron_min,
        p_min=p_electron_min,
        pid=pid_electron)

    DecayDescriptor = f'{parent_id} -> mu- e+'
    if same_sign: DecayDescriptor = f'[{parent_id} -> mu+ e+]cc'

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_dilepton_min,
        F.MAX(F.PT) > pt_best,
        F.MAX(F.P) > p_best, F.MAXDOCACHI2CUT(adocachi2cut_max))
    #pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max)
    return ParticleCombiner([muons, electrons],
                            name=name + "_combiner_{hash}",
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_dimuon(name="rd_detached_dimuon_{hash}",
                            parent_id='J/psi(1S)',
                            pt_dimuon_min=0. * MeV,
                            pt_muon_min=300. * MeV,
                            p_muon_min=3000. * MeV,
                            ipchi2_muon_min=9.,
                            pidmu_muon_min=3.,
                            adocachi2cut_max=30.,
                            bpvvdchi2_min=30.,
                            vchi2pdof_max=30.,
                            am_min=0. * MeV,
                            am_max=6000. * MeV,
                            same_sign=False):
    """
    Make the detached dimuon, opposite-sign or same-sign.
    """
    muons = make_rd_detached_muons(
        name="rd_detached_muons_{hash}",
        pt_min=pt_muon_min,
        p_min=p_muon_min,
        mipchi2dvprimary_min=ipchi2_muon_min,
        pid=F.require_all(F.ISMUON, F.PID_MU > pidmu_muon_min))

    DecayDescriptor = f'{parent_id} -> mu+ mu-'
    if same_sign: DecayDescriptor = f'[{parent_id} -> mu+ mu+]cc'

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_dimuon_min,
        F.MAXDOCACHI2CUT(adocachi2cut_max))
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    return ParticleCombiner([muons, muons],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_dielectron(
        name="rd_detached_dielectron_{hash}",
        pid_e_min=2.,
        pt_e_min=0.25 * GeV,
        p_e_min=0. * GeV,
        ipchi2_e_min=9.,
        parent_id="J/psi(1S)",
        opposite_sign=True,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,  #should be max
        bpvvdchi2_min=30.,
        vfaspfchi2ndof_max=10.,
        am_min=0. * MeV,
        am_max=6000. * MeV):
    """
    Make the detached e+e- pair with the proper bremsstrahlung correction handling.
    """
    dielectrons = make_detached_dielectron_with_brem(
        opposite_sign=opposite_sign,
        PIDe_min=pid_e_min,
        pt_e=pt_e_min,
        p_e=p_e_min,
        minipchi2=ipchi2_e_min,
        dielectron_ID=parent_id,
        pt_diE=pt_diE_min,
        m_diE_min=am_min,
        m_diE_max=am_max,
        adocachi2cut=adocachi2cut_min,
        bpvvdchi2=bpvvdchi2_min,
        vfaspfchi2ndof=vfaspfchi2ndof_max)
    code = F.require_all(F.MASS > am_min, F.MASS < am_max)
    return ParticleFilter(dielectrons, F.FILTER(code), name=name)


@configurable
def make_rd_detached_mue(name="rd_detached_mue_{hash}",
                         min_dilepton_mass=0. * MeV,
                         max_dilepton_mass=6000. * MeV,
                         parent_id='J/psi(1S)',
                         min_probnn_mu=0.,
                         min_PIDmu=0.,
                         IsMuon=False,
                         min_PIDe=2.,
                         min_pt_e=0.25 * GeV,
                         min_pt_mu=0.25 * GeV,
                         min_bpvvdchi2=30.,
                         max_vchi2ndof=10.,
                         same_sign=False):
    """
    Make the detached muon-electron pair, opposite-sign or same-sign.
    """
    dileptons = make_detached_mue_with_brem(
        dilepton_ID=parent_id,
        min_probnn_mu=min_probnn_mu,
        min_PIDmu=min_PIDmu,
        IsMuon=False,
        min_PIDe=min_PIDe,
        opposite_sign=not same_sign,
        min_pt_e=min_pt_e,
        min_pt_mu=min_pt_mu,
        min_bpvvdchi2=min_bpvvdchi2,
        max_vchi2ndof=max_vchi2ndof)
    code = F.require_all(
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass))
    return ParticleFilter(dileptons, F.FILTER(code), name=name)


####################################
# Tau builders                     #
####################################


@configurable
def make_rd_tauons_hadronic_decay(
        name="rd_tauons_hadronic_decay_{hash}",
        make_pions=make_rd_detached_pions,
        descriptor="[tau+ -> pi- pi+ pi+]cc",
        pi_pt_min=150. * MeV,
        pi_ipchi2_min=4.0,
        pi_pid=(F.PID_K < 8.),
        best_pi_pt_min=300. * MeV,
        best_pi_ipchi2_min=5.,
        pt_max=300. * MeV,
        am_min=400. * MeV,
        am_max=3500. * MeV,
        make_pvs=make_pvs,
        #        adoca_max=0.15 * mm,
        adocachi2_max=6.,
        am_2pi_max=1670. * MeV,
        vchi2pdof_max=25.,
        bpvdira_min=0.99,
):
    '''
    Build tau->3pi candidates. Approximately corresponding to the Run2
    "StdLooseDetachedTau" cuts.
    '''

    pions = make_pions(
        pt_min=pi_pt_min, mipchi2dvprimary_min=pi_ipchi2_min, pid=pi_pid)

    combination12_code = F.require_all(
        F.MASS < am_2pi_max,
        F.MAXDOCACHI2CUT(adocachi2_max),
        #F.DOCA(1, 2) < adoca_max,
    )

    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUBCOMB(Functor=(F.MASS < am_2pi_max), Indices=(1, 3)),
        # F.DOCA(1, 3) < adoca_max,
        # F.DOCA(2, 3) < adoca_max,
        F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(adocachi2_max), Indices=(2, 3)),
        F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(adocachi2_max), Indices=(1, 3)),
        F.SUM(F.PT > best_pi_pt_min) > 1,
        F.SUM(F.MINIPCHI2(pvs) > best_pi_ipchi2_min) > 1,
    )

    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > bpvdira_min,
    )
    return ParticleCombiner([pions, pions, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            Combination12Cut=combination12_code,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


####################################
# Dihadron                         #
####################################


@configurable
def make_rd_detached_dihadron(name="make_rd_detached_dihadrons_{hash}",
                              parent_id='B_s0',
                              same_sign=False,
                              pid=None,
                              min_pt_track=500 * MeV,
                              min_p_track=3000 * MeV,
                              min_ipchi2_track=20.,
                              max_adocachi2=30.,
                              max_vchi2ndof=9.0,
                              am_min=4500 * MeV,
                              am_max=6000 * MeV,
                              min_bpvvdchi2=196.,
                              max_ipchi2_dihadron=25.,
                              min_pt_dihadron=1000 * MeV,
                              min_DIRA_dihadron=0.999):
    """
    Make the detached dihadron, opposite-sign or same-sign.
    """
    descriptor = f'{parent_id} -> pi+ pi-'
    if same_sign: descriptor = f'[{parent_id} -> pi+ pi+]cc'
    pvs = make_pvs()
    pions = make_rd_detached_pions(
        mipchi2dvprimary_min=min_ipchi2_track,
        pt_min=min_pt_track,
        p_min=min_p_track,
        pid=pid)
    combination_code = F.MAXDOCACHI2CUT(max_adocachi2)
    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.CHI2DOF < max_vchi2ndof,
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
        F.BPVIPCHI2(pvs) < max_ipchi2_dihadron,
        F.BPVDIRA(pvs) > min_DIRA_dihadron, F.PT > min_pt_dihadron)
    return ParticleCombiner(
        Inputs=[pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


####################################
# Trihadron                        #
####################################


@configurable
def make_rd_detached_K1(
        name="make_rd_detached_K_1_{hash}",
        Descriptor="[K_1(1270)+ -> K+ pi- pi+]cc",
        adocachi2_max=25.,
        am_min=500. * MeV,
        am_max=4200. * MeV,
        low_factor=0.5,
        high_factor=1.5,
        pi_p_min=1_000. * MeV,
        pi_pt_min=0. * MeV,
        pi_ipchi2_min=9.,
        pi_PIDK_max=-2.,
        K_p_min=1_000. * MeV,
        K_pt_min=0. * MeV,
        K_ipchi2_min=9.,
        K_PIDK_min=-2.,
        K_1_min_bpvfdchi2=0.,
        K_1_pt_min=0. * MeV,
        vchi2pdof_max=25.,
        K_1_min_DIRA=None,
):
    """
    make K_1(1270) -> K+ pi+ pi-
    """

    kaons = make_rd_detached_kaons(
        p_min=K_p_min,
        pt_min=K_pt_min,
        mipchi2dvprimary_min=K_ipchi2_min,
        pid=(F.PID_K > K_PIDK_min),
    )
    pions = make_rd_detached_pions(
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        mipchi2dvprimary_min=pi_ipchi2_min,
        pid=(F.PID_K <= pi_PIDK_max),
    )

    two_body_combination_code = F.require_all(
        in_range(low_factor * am_min, F.MASS, high_factor * am_max),
        F.MAXDOCACHI2CUT(adocachi2_max),
    )

    combination_code = F.require_all(
        in_range(low_factor * am_min, F.MASS, high_factor * am_max),
        F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(adocachi2_max), Indices=[2, 3]),
        F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(adocachi2_max), Indices=[1, 3]),
    )

    pvs = make_pvs()
    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.PT > K_1_pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > K_1_min_bpvfdchi2,
    )

    if K_1_min_DIRA is not None:
        vertex_code &= (F.BPVDIRA(pvs) > K_1_min_DIRA)

    return ParticleCombiner([kaons, pions, pions],
                            name=name,
                            DecayDescriptor=Descriptor,
                            Combination12Cut=two_body_combination_code,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_K2(
        name="make_rd_detached_K_2_{hash}",
        Descriptor="[K_2(1770)+ -> K+ K+ K-]cc",
        adocachi2_max=25.,
        am_min=500. * MeV,
        am_max=4200. * MeV,
        low_factor=0.5,
        high_factor=1.5,
        K_p_min=1_000. * MeV,
        K_pt_min=0. * MeV,
        K_ipchi2_min=9.,
        K_PIDK_min=-2.,
        K_2_min_bpvfdchi2=0.,
        K_2_pt_min=0. * MeV,
        vchi2pdof_max=25.,
        K_2_min_DIRA=None,
):
    """
    make K_2(1770) -> K+ K+ K-
    """

    kaons = make_rd_detached_kaons(
        p_min=K_p_min,
        pt_min=K_pt_min,
        mipchi2dvprimary_min=K_ipchi2_min,
        pid=(F.PID_K > K_PIDK_min),
    )

    two_body_combination_code = F.require_all(
        in_range(low_factor * am_min, F.MASS, high_factor * am_max),
        F.MAXDOCACHI2CUT(adocachi2_max),
    )

    combination_code = F.require_all(
        in_range(low_factor * am_min, F.MASS, high_factor * am_max),
        F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(adocachi2_max), Indices=[2, 3]),
        F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(adocachi2_max), Indices=[1, 3]),
    )

    pvs = make_pvs()
    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.PT > K_2_pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > K_2_min_bpvfdchi2,
    )

    if K_2_min_DIRA is not None:
        vertex_code &= (F.BPVDIRA(pvs) > K_2_min_DIRA)

    return ParticleCombiner([kaons, kaons, kaons],
                            name=name,
                            DecayDescriptor=Descriptor,
                            Combination12Cut=two_body_combination_code,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)
