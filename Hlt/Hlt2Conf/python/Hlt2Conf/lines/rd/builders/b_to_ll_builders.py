###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builders and filters of B2ll(')(gamma) lines for the RD working group.
- B0(s) -> Mu Mu
- B0(s) -> Mu E
- B0(s) -> E E

- B0(s) -> Mu Mu Gamma
- B0(s) -> E E Gamma
- B0(s) -> Mu E Gamma


author: Titus Mombächer
date: 08.12.2021

"""

from RecoConf.reconstruction_objects import make_pvs

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

from .rdbuilder_thor import make_rd_detached_dimuon, make_rd_detached_dielectron, make_rd_detached_mue, make_rd_photons

from PyConf import configurable

###########################
# Dilepton lines          #
###########################


@configurable
def filter_B2MuMu(name="filter_RD_BToMuMu_{hash}",
                  pt_muon_min=500 * MeV,
                  ipchi2_muon_min=20.,
                  pidmu_muon_min=0.,
                  vchi2pdof_max=9.0,
                  am_min=4000 * MeV,
                  am_max=6000 * MeV,
                  bpvvdchi2_min=196.,
                  bipchi2_max=25.,
                  pt_dimuon_min=1000 * MeV,
                  DIRA_dimuon_min=0.999,
                  same_sign=False):
    """
    Filter Dimuons for the B2mumu line (valid for B0 and B_s0)
    """
    pvs = make_pvs()
    dimuons = make_rd_detached_dimuon(
        parent_id='B_s0',
        name="rd_detached_dimuon_for_Bmumu_{hash}",
        pt_muon_min=pt_muon_min,
        ipchi2_muon_min=ipchi2_muon_min,
        pidmu_muon_min=pidmu_muon_min,
        vchi2pdof_max=vchi2pdof_max,
        am_min=am_min,
        am_max=am_max,
        bpvvdchi2_min=bpvvdchi2_min,
        same_sign=same_sign)
    code = F.require_all(
        F.BPVIPCHI2(pvs) < bipchi2_max, F.PT > pt_dimuon_min,
        F.BPVDIRA(pvs) > DIRA_dimuon_min)
    return ParticleFilter(dimuons, F.FILTER(code), name=name)


@configurable
def filter_B2EE(name="filter_RD_BToEE_{hash}",
                pt_e_min=500 * MeV,
                ipchi2_electron_min=20.,
                pide_electron_min=None,
                vchi2pdof_max=9.0,
                am_min=4000 * MeV,
                am_max=6000 * MeV,
                bpvvdchi2_min=196.,
                bipchi2_max=25.,
                pt_dielectron_min=1000 * MeV,
                DIRA_dielectron_min=0.999,
                same_sign=False):
    """
    Filter Dielectrons for the B2ee line (valid for B0 and B_s0)
    """
    pvs = make_pvs()
    dielectrons = make_rd_detached_dielectron(
        parent_id='B_s0',
        name="rd_detached_dielectron_for_B_e_{hash}",
        pt_e_min=pt_e_min,
        ipchi2_e_min=ipchi2_electron_min,
        pid_e_min=pide_electron_min,
        vfaspfchi2ndof_max=vchi2pdof_max,
        pt_diE_min=pt_dielectron_min,
        am_min=am_min,
        am_max=am_max,
        bpvvdchi2_min=bpvvdchi2_min,
        opposite_sign=~same_sign)
    code = F.require_all(
        F.BPVIPCHI2(pvs) < bipchi2_max,
        F.BPVDIRA(pvs) > DIRA_dielectron_min)
    return ParticleFilter(dielectrons, F.FILTER(code), name=name)


def filter_B2MuE(name="filter_RD_BToMuE",
                 parent_id="B_s0",
                 min_probnn_mu=None,
                 min_PIDmu=None,
                 min_PIDe=None,
                 min_pt_track=500. * MeV,
                 min_ipchi2_track=20.,
                 max_ipchi2_emu=25.,
                 max_adocachi2=30.0,
                 min_DIRA_emu=0.999,
                 min_pt_emu=1000. * MeV,
                 min_bpvvdchi2=196.0,
                 max_vchi2ndof=9.0,
                 min_mass=4500 * MeV,
                 max_mass=6000 * MeV,
                 same_sign=False):
    """
    Filter MuE for the B2MuE line (valid for B0 and B_s0)
    """
    pvs = make_pvs()
    emu = make_rd_detached_mue(
        name="rd_detached_mue_for_Bmue",
        max_dilepton_mass=max_mass,
        min_probnn_mu=min_probnn_mu,
        min_PIDe=min_PIDe,
        min_pt_e=min_pt_track,
        min_pt_mu=min_pt_track,
        min_bpvvdchi2=min_bpvvdchi2,
        max_vchi2ndof=max_vchi2ndof,
        same_sign=False)
    code = F.require_all(
        F.BPVIPCHI2(pvs) < max_ipchi2_emu, F.PT > min_pt_emu,
        F.BPVDIRA(pvs) > min_DIRA_emu, F.MASS > min_mass)
    return ParticleFilter(emu, F.FILTER(code), name=name)


###########################
# Radiative lines         #
###########################


@configurable
def make_B2MuMuGamma(name="make_B2MuMuGamma",
                     pt_muon_min=800 * MeV,
                     ipchi2_muon_min=10.,
                     pidmu_muon_min=1.,
                     dimuon_vchi2pdof_max=10.0,
                     min_E_gamma=5000 * MeV,
                     min_ET_gamma=1000 * MeV,
                     min_IsPhoton_gamma=0.6,
                     IsNotH_min=0.4,
                     E19_min=0.2,
                     am_min=4000 * MeV,
                     am_max=7000 * MeV,
                     dimuon_bpvvdchi2_min=100.,
                     pt_B_min=1500 * MeV,
                     ipchi2_B_max=18.,
                     DIRA_B_min=0.995):
    """
    make B2mumugamma candidates (valid for B0 and B_s0)
    """
    pvs = make_pvs()

    dimuons = make_rd_detached_dimuon(
        parent_id='J/psi(1S)',
        name="rd_detached_dimuon_for_Bmumugamma",
        pt_muon_min=pt_muon_min,
        ipchi2_muon_min=ipchi2_muon_min,
        pidmu_muon_min=pidmu_muon_min,
        vchi2pdof_max=dimuon_vchi2pdof_max,
        am_min=0.,
        am_max=am_max,
        bpvvdchi2_min=dimuon_bpvvdchi2_min)
    photons = make_rd_photons(
        IsPhoton_min=min_IsPhoton_gamma,
        IsNotH_min=IsNotH_min,
        E19_min=E19_min,
        et_min=min_ET_gamma,
        e_min=min_E_gamma)

    descriptor = "B_s0 -> J/psi(1S) gamma"

    combination_code = (F.PT > pt_B_min)
    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.BPVIPCHI2(pvs) < ipchi2_B_max,
        F.BPVDIRA(pvs) > DIRA_B_min)
    return ParticleCombiner(
        Inputs=[dimuons, photons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_B2EEGamma(name="make_B2EEGamma",
                   pt_e_min=800 * MeV,
                   ipchi2_electron_min=10.,
                   pid_e_min=3.,
                   dielectron_vchi2pdof_max=8.0,
                   min_E_gamma=5000 * MeV,
                   min_ET_gamma=1000 * MeV,
                   min_IsPhoton_gamma=0.6,
                   IsNotH_min=0.4,
                   E19_min=0.2,
                   am_min=4000 * MeV,
                   am_max=7000 * MeV,
                   dielectron_bpvvdchi2_min=100.,
                   pt_B_min=1500 * MeV,
                   ipchi2_B_max=16.,
                   DIRA_B_min=0.995):
    """
    make B2eegamma candidates (valid for B0 and B_s0)
    """

    pvs = make_pvs()

    dimuons = make_rd_detached_dielectron(
        parent_id='J/psi(1S)',
        name="rd_detached_dielectron_for_Beegamma",
        pt_e_min=pt_e_min,
        ipchi2_e_min=ipchi2_electron_min,
        pid_e_min=pid_e_min,
        vfaspfchi2ndof_max=dielectron_vchi2pdof_max,
        am_min=0.,
        am_max=am_max,
        bpvvdchi2_min=dielectron_bpvvdchi2_min)
    photons = make_rd_photons(
        IsPhoton_min=min_IsPhoton_gamma,
        IsNotH_min=IsNotH_min,
        E19_min=E19_min,
        et_min=min_ET_gamma,
        e_min=min_E_gamma)

    descriptor = "B_s0 -> J/psi(1S) gamma"
    combination_code = (F.PT > pt_B_min)
    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.BPVIPCHI2(pvs) < ipchi2_B_max,
        F.BPVDIRA(pvs) > DIRA_B_min)

    return ParticleCombiner(
        Inputs=[dimuons, photons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_B2MuEGamma(name="make_B2MuEGamma",
                    pt_muon_min=800 * MeV,
                    pt_electron_min=800 * MeV,
                    pidmu_muon_min=1.,
                    pide_electron_min=2.,
                    dilepton_vchi2pdof_max=10.0,
                    min_E_gamma=5000 * MeV,
                    min_ET_gamma=1000 * MeV,
                    min_IsPhoton_gamma=0.6,
                    IsNotH_min=0.4,
                    E19_min=0.2,
                    am_min=4000 * MeV,
                    am_max=7000 * MeV,
                    dilepton_bpvvdchi2_min=100.,
                    pt_B_min=1500 * MeV,
                    ipchi2_B_max=18.,
                    DIRA_B_min=0.995):
    """
    make B2muegamma candidates (valid for B0 and B_s0)
    """

    pvs = make_pvs()

    dimuons = make_rd_detached_mue(
        parent_id='J/psi(1S)',
        name="rd_detached_mue_for_Bmuegamma",
        min_pt_mu=pt_muon_min,
        min_pt_e=pt_electron_min,
        min_PIDmu=pidmu_muon_min,
        min_PIDe=pide_electron_min,
        max_vchi2ndof=dilepton_vchi2pdof_max,
        max_dilepton_mass=am_max,
        min_bpvvdchi2=dilepton_bpvvdchi2_min)
    photons = make_rd_photons(
        IsPhoton_min=min_IsPhoton_gamma,
        IsNotH_min=IsNotH_min,
        E19_min=E19_min,
        et_min=min_ET_gamma,
        e_min=min_E_gamma)

    descriptor = "B_s0 -> J/psi(1S) gamma"
    combination_code = (F.PT > pt_B_min)
    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.BPVIPCHI2(pvs) < ipchi2_B_max,
        F.BPVDIRA(pvs) > DIRA_B_min)

    return ParticleCombiner(
        Inputs=[dimuons, photons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
