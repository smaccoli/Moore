###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the builders for the Hb->V0ll(h) exclusive lines, with LL and DD KS and L0
Includes builders for:
    B0 -> J/psi(1S) KS0
    [B+ -> J/psi(1S) KS0 pi+]cc
    [Lambda_b0 -> J/psi(1S) Lambda0]cc

Contact: Harry Cliff (harry.victor.cliff@cern.ch)
"""
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

from PyConf import configurable

_max_vertexchi2 = 9.0
_min_B_mass = 4000. * MeV


@configurable
def filter_hadrons(hadrons, min_pt=400. * MeV):
    """Filter input hadrons to all candidates"""

    # Minimum pT cut
    cut = (F.PT > min_pt)

    return ParticleFilter(hadrons, F.FILTER(cut))


@configurable
def make_b2ksll(dileptons,
                Kshorts,
                pvs,
                max_mass_B=7000 * MeV,
                max_vertexchi2=_max_vertexchi2,
                min_B_mass=_min_B_mass,
                max_B_mass=7000 * MeV,
                min_dira=0.9995,
                max_ipchi2=30.,
                min_fdchi2=100.,
                max_docachi2=30.):
    """Make the B0 -> KS0 l+ l-  (l = e or mu) candidate"""

    # Filter the Kshorts
    kshorts = filter_hadrons(hadrons=Kshorts)

    # upper B mass and DOCAChi2 cut
    combination_code = F.require_all(F.MASS < max_mass_B,
                                     F.MAXDOCACHI2CUT(max_docachi2))

    # some additional cuts
    vertex_code = F.require_all(F.MASS > min_B_mass, F.MASS < max_B_mass,
                                F.CHI2DOF < max_vertexchi2,
                                F.BPVDIRA(pvs) > min_dira,
                                F.BPVIPCHI2(pvs) < max_ipchi2,
                                F.BPVFDCHI2(pvs) > min_fdchi2)

    return ParticleCombiner(
        [dileptons, kshorts],
        DecayDescriptor="B0 -> J/psi(1S) KS0",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_lambdab2lambdall(dileptons,
                          Lambdas,
                          pvs,
                          max_mass_B=7000 * MeV,
                          max_vertexchi2=_max_vertexchi2,
                          min_B_mass=_min_B_mass,
                          max_B_mass=7000 * MeV,
                          min_dira=0.9995,
                          max_ipchi2=30.,
                          min_fdchi2=100.,
                          max_docachi2=30.):
    """Make the Lambda_b0 -> Lambda0 l+ l-  (l = e or mu) candidate"""

    # Filter the Kshorts
    lambdas = filter_hadrons(hadrons=Lambdas)

    # upper B mass and DOCAChi2 cut
    combination_code = F.require_all(F.MASS < max_mass_B,
                                     F.MAXDOCACHI2CUT(max_docachi2))

    # some additional cuts
    vertex_code = F.require_all(F.MASS > min_B_mass, F.MASS < max_B_mass,
                                F.CHI2DOF < max_vertexchi2,
                                F.BPVDIRA(pvs) > min_dira,
                                F.BPVIPCHI2(pvs) < max_ipchi2,
                                F.BPVFDCHI2(pvs) > min_fdchi2)

    return ParticleCombiner(
        [dileptons, lambdas],
        DecayDescriptor="[Lambda_b0 -> J/psi(1S) Lambda0]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_ksttokspi(Kshorts,
                   pions,
                   pvs,
                   min_Kst_mass=0. * MeV,
                   max_Kst_mass=5400. * MeV,
                   max_docachi2=30.,
                   min_pt=400. * MeV,
                   max_vertexchi2=25.):
    """Make K*(892)+ -> KS0 pi+ candidate"""

    # Filter the Kshorts
    kshorts = filter_hadrons(hadrons=Kshorts)

    # upper K*+ mass and DOCAChi2 cut
    combination_code = F.require_all(
        F.MASS > min_Kst_mass, F.MASS < max_Kst_mass,
        F.MAXDOCACHI2CUT(max_docachi2), F.PT > min_pt)

    # some additional cuts
    vertex_code = F.require_all(F.MASS > min_Kst_mass, F.MASS < max_Kst_mass,
                                F.CHI2DOF < max_vertexchi2)

    return ParticleCombiner([kshorts, pions],
                            DecayDescriptor="[K*(892)+ -> KS0 pi+]cc",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_b2kspill(dileptons,
                  Kstars,
                  pvs,
                  max_mass_B=7000 * MeV,
                  max_vertexchi2=_max_vertexchi2,
                  min_B_mass=_min_B_mass,
                  max_B_mass=7000 * MeV,
                  min_dira=0.9995,
                  max_ipchi2=30.,
                  min_fdchi2=100.,
                  max_docachi2=30.):
    """Make the B+ -> K*(892)+ l+ l-  (l = e or mu) candidate"""

    # upper B mass and DOCAChi2 cut
    combination_code = F.require_all(F.MASS < max_mass_B,
                                     F.MAXDOCACHI2CUT(max_docachi2))

    # some additional cuts
    vertex_code = F.require_all(F.MASS > min_B_mass, F.MASS < max_B_mass,
                                F.CHI2DOF < max_vertexchi2,
                                F.BPVDIRA(pvs) > min_dira,
                                F.BPVIPCHI2(pvs) < max_ipchi2,
                                F.BPVFDCHI2(pvs) > min_fdchi2)

    return ParticleCombiner([dileptons, Kstars],
                            DecayDescriptor="[B+ -> J/psi(1S) K*(892)+]cc",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)
