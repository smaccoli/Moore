###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''

Definition of some builders useful for Baryon Number Violation and Lepton Flavor Violation decays:
    1) B -> h mu   (h = pi, K or p)
    2) L -> h mu    (h = pi, K or p)
    3) Lb -> h mu   (h = pi, K or p)
    4) Lc -> p K pi
    5) Ds -> K K pi
    6) Bs -> Lc mu
    7) Lb -> Ds mu

Notes:
    1) This is the HLT2 porting of B2PMu Stripping line:
          https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingLFVLines.py#L2415
    2) This is the HLT2 porting of Lz2KMu Stripping line:
          https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBLVLines.py#L234
    3) This is the HLT2 porting of Lb2KMu Stripping Line:
          https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBLVLines.py#L250
    4) This is taken from B2OC common builders:
          https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/lines/b_to_open_charm/builders/cbaryon_builder.py#L30
    5) This is taken from B2OC common builders:
          https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/lines/b_to_open_charm/builders/d_builder.py#L583
    6) This is the HLT2 porting of Bs2LcMu Stripping line:
          https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBLVLines.py#L280
    7) This is the HLT2 porting of Lb2DsMu Stripping line:
          https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBLVLines.py#L275

    * Warning: For F.BPVDIRA(pvs) and F.BOVVDCHI2 documentation reports LoKi/ThOr differences unknown


Author: Fabio De Vellis
Contact: fabio.de.vellis@cern.ch

'''

import Functors as F

from Hlt2Conf.algorithms_thor import ParticleCombiner

from GaudiKernel.SystemOfUnits import MeV, mm, ps

from PyConf import configurable


# 1) B -> h mu
@configurable
def make_BToHMu(pvs,
                hadron,
                muon,
                descriptor,
                B_LEFTMASS_COMB=4500 * MeV,
                B_RIGHTMASS_COMB=6200 * MeV,
                B_LEFTMASS_VTX=4700 * MeV,
                B_RIGHTMASS_VTX=6000 * MeV,
                DOCA_CUT_COMB=0.1 * mm,
                B_CHI2=9,
                B_BPVDIRA=0.9995,
                B_BPVVDCHI2=225,
                B_BPVIPCHI2=25):

    combination_cut = F.require_all(
        F.math.in_range(B_LEFTMASS_COMB, F.MASS, B_RIGHTMASS_COMB),
        F.MAXDOCACUT(DOCA_CUT_COMB))

    vertex_cut = F.require_all(
        F.CHI2DOF < B_CHI2,
        F.math.in_range(B_LEFTMASS_VTX, F.MASS, B_RIGHTMASS_VTX),
        F.BPVDIRA(pvs) > B_BPVDIRA,
        F.BPVFDCHI2(pvs) > B_BPVVDCHI2,
        F.BPVIPCHI2(pvs) < B_BPVIPCHI2)

    return ParticleCombiner([hadron, muon],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cut)


# 2) L -> h mu
@configurable
def make_LzToHMu(pvs,
                 hadron,
                 muon,
                 descriptor,
                 Lz_LEFTMASS_COMB=500 * MeV,
                 Lz_RIGHTMASS_COMB=1700 * MeV,
                 Lz_LEFTMASS_VTX=700 * MeV,
                 Lz_RIGHTMASS_VTX=1500 * MeV,
                 DOCA_CUT_COMB=0.3 * mm,
                 Lz_CHI2=9,
                 Lz_BPVDIRA=0.9995,
                 Lz_BPVVDCHI2=225,
                 Lz_BPVIPCHI2=25,
                 Lz_TAU=10 * ps):

    combination_cut = F.require_all(
        F.math.in_range(Lz_LEFTMASS_COMB, F.MASS, Lz_RIGHTMASS_COMB),
        F.MAXDOCACUT(DOCA_CUT_COMB))

    vertex_cut = F.require_all(
        F.CHI2DOF < Lz_CHI2,
        F.math.in_range(Lz_LEFTMASS_VTX, F.MASS, Lz_RIGHTMASS_VTX),
        F.BPVDIRA(pvs) > Lz_BPVDIRA,
        F.BPVFDCHI2(pvs) > Lz_BPVVDCHI2,
        F.BPVIPCHI2(pvs) < Lz_BPVIPCHI2,
        F.BPVLTIME(pvs) > Lz_TAU)

    return ParticleCombiner([hadron, muon],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cut)


# 3) Lb -> h mu
@configurable
def make_LbToHMu(pvs,
                 hadron,
                 muon,
                 descriptor,
                 Lb_LEFTMASS_COMB=4800 * MeV,
                 Lb_RIGHTMASS_COMB=6500 * MeV,
                 Lb_LEFTMASS_VTX=5000 * MeV,
                 Lb_RIGHTMASS_VTX=6200 * MeV,
                 DOCA_CUT_COMB=0.3 * mm,
                 Lb_CHI2=9,
                 Lb_BPVDIRA=0,
                 Lb_BPVVDCHI2=225,
                 Lb_BPVIPCHI2=25,
                 Lb_TAU=1 * ps):

    combination_cut = F.require_all(
        F.math.in_range(Lb_LEFTMASS_COMB, F.MASS, Lb_RIGHTMASS_COMB),
        F.MAXDOCACUT(DOCA_CUT_COMB))

    vertex_cut = F.require_all(
        F.CHI2DOF < Lb_CHI2,
        F.math.in_range(Lb_LEFTMASS_VTX, F.MASS, Lb_RIGHTMASS_VTX),
        F.BPVDIRA(pvs) > Lb_BPVDIRA,
        F.BPVFDCHI2(pvs) > Lb_BPVVDCHI2,
        F.BPVIPCHI2(pvs) < Lb_BPVIPCHI2,
        F.BPVLTIME(pvs) > Lb_TAU)

    return ParticleCombiner([hadron, muon],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cut)


# 4) Lc -> p K pi
@configurable
def make_LcToPKPi(pvs,
                  proton,
                  kaon,
                  pion,
                  descriptor,
                  Lc_LEFTMASS_COMB=2000 * MeV,
                  Lc_RIGHTMASS_COMB=2600 * MeV,
                  Lc_LEFTMASS_VTX=2200 * MeV,
                  Lc_RIGHTMASS_VTX=2400 * MeV,
                  DAUGHTERS_SUMPT=1800 * MeV,
                  DOCA_CUT_COMB=0.5 * mm,
                  Lc_CHI2=10,
                  Lc_DIRA=0,
                  Lc_BPVVDCHI2=36):

    combination_cut = F.require_all(
        F.math.in_range(Lc_LEFTMASS_COMB, F.MASS, Lc_RIGHTMASS_COMB),
        F.SUM(F.PT) > DAUGHTERS_SUMPT)

    combination12_cut = F.MAXDOCACUT(DOCA_CUT_COMB)

    vertex_cut = F.require_all(
        F.math.in_range(Lc_LEFTMASS_VTX, F.MASS, Lc_RIGHTMASS_VTX),
        F.CHI2DOF < Lc_CHI2,
        F.BPVFDCHI2(pvs) > Lc_BPVVDCHI2,
        F.BPVDIRA(pvs) > Lc_DIRA)

    return ParticleCombiner([proton, kaon, pion],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            Combination12Cut=combination12_cut,
                            CompositeCut=vertex_cut)


# 5) Ds -> K+ K- pi+
@configurable
def make_DsToKKPi(pvs,
                  kaons,
                  pion,
                  descriptor,
                  Ds_LEFTMASS_COMB=1700 * MeV,
                  Ds_RIGHTMASS_COMB=2200 * MeV,
                  Ds_LEFTMASS_VTX=1900 * MeV,
                  Ds_RIGHTMASS_VTX=2000 * MeV,
                  DAUGHTERS_SUMPT=1800 * MeV,
                  DOCA_CUT_COMB=0.5 * mm,
                  Ds_CHI2=10,
                  Ds_DIRA=0,
                  Ds_BPVVDCHI2=36):

    combination_cut = F.require_all(
        F.math.in_range(Ds_LEFTMASS_COMB, F.MASS, Ds_RIGHTMASS_COMB),
        F.SUM(F.PT) > DAUGHTERS_SUMPT)

    combination12_cut = F.MAXDOCACUT(DOCA_CUT_COMB)

    vertex_cut = F.require_all(
        F.math.in_range(Ds_LEFTMASS_VTX, F.MASS, Ds_RIGHTMASS_VTX),
        F.CHI2DOF < Ds_CHI2,
        F.BPVFDCHI2(pvs) > Ds_BPVVDCHI2,
        F.BPVDIRA(pvs) > Ds_DIRA)

    return ParticleCombiner([kaons, kaons, pion],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            Combination12Cut=combination12_cut,
                            CompositeCut=vertex_cut)


# 6) B -> Lc mu
def make_BToLcMu(pvs,
                 Lambda_c,
                 muon,
                 descriptor,
                 B_LEFTMASS_COMB=4500 * MeV,
                 B_RIGHTMASS_COMB=6200 * MeV,
                 B_LEFTMASS_VTX=4700 * MeV,
                 B_RIGHTMASS_VTX=6000 * MeV,
                 DOCA_CUT_COMB=0.3 * mm,
                 B_CHI2=9,
                 B_DIRA=0.995,
                 B_BPVIPCHI2=25,
                 B_BPVVDCHI2=225,
                 B_TAU=1 * ps):

    combination_cut = F.require_all(
        F.math.in_range(B_LEFTMASS_COMB, F.MASS, B_RIGHTMASS_COMB),
        F.MAXDOCACUT(DOCA_CUT_COMB))

    vertex_cut = F.require_all(
        F.math.in_range(B_LEFTMASS_VTX, F.MASS, B_RIGHTMASS_VTX),
        F.CHI2DOF < B_CHI2,
        F.BPVDIRA(pvs) > B_DIRA,
        F.BPVFDCHI2(pvs) > B_BPVVDCHI2,
        F.BPVIPCHI2(pvs) < B_BPVIPCHI2,
        F.BPVLTIME(pvs) > B_TAU)

    return ParticleCombiner([Lambda_c, muon],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cut)


# 7) Lb -> Ds mu
def make_LbToDsMu(pvs,
                  Ds,
                  muon,
                  descriptor,
                  Lb_LEFTMASS_COMB=4800 * MeV,
                  Lb_RIGHTMASS_COMB=6500 * MeV,
                  Lb_LEFTMASS_VTX=5000 * MeV,
                  Lb_RIGHTMASS_VTX=6200 * MeV,
                  DOCA_CUT_COMB=0.3 * mm,
                  Lb_CHI2=9,
                  Lb_DIRA=0,
                  Lb_BPVIPCHI2=25,
                  Lb_BPVVDCHI2=225,
                  Lb_TAU=1 * ps):

    combination_cut = F.require_all(
        F.math.in_range(Lb_LEFTMASS_COMB, F.MASS, Lb_RIGHTMASS_COMB),
        F.MAXDOCACUT(DOCA_CUT_COMB))

    vertex_cut = F.require_all(
        F.math.in_range(Lb_LEFTMASS_VTX, F.MASS, Lb_RIGHTMASS_VTX),
        F.CHI2DOF < Lb_CHI2,
        F.BPVDIRA(pvs) > Lb_DIRA,
        F.BPVFDCHI2(pvs) > Lb_BPVVDCHI2,
        F.BPVIPCHI2(pvs) < Lb_BPVIPCHI2,
        F.BPVLTIME(pvs) > Lb_TAU)

    return ParticleCombiner([Ds, muon],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cut)
