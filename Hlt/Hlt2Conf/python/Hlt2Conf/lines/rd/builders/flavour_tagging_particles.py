###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#adding for flavour tagging
from Hlt2Conf.standard_particles import make_has_rich_long_pions, make_has_rich_up_pions


def extra_outputs_for_FlavourTagging(BCandidates, persistreco=False):
    #from PyConf.Algorithms import GetFlavourTaggingParticles
    #TODO: waiting for the event size check result from FT group
    #pvs = make_pvs()
    #longTaggingParticles = GetFlavourTaggingParticles(
    #    BCandidates=BCandidates,
    #    TaggingParticles=make_has_rich_long_pions(),
    #    PrimaryVertices=pvs
    #)
    #upstreamTaggingParticles = GetFlavourTaggingParticles(
    #    BCandidates=BCandidates,
    #    TaggingParticles=make_has_rich_up_pions(),
    #    PrimaryVertices=pvs
    #)
    longTaggingParticles = make_has_rich_long_pions()
    upstreamTaggingParticles = make_has_rich_up_pions()
    if persistreco:
        extra_outputs = [('UpstreamTaggingParticles',
                          upstreamTaggingParticles)]
    else:
        extra_outputs = [('LongTaggingParticles', longTaggingParticles),
                         ('UpstreamTaggingParticles',
                          upstreamTaggingParticles)]
    return extra_outputs
