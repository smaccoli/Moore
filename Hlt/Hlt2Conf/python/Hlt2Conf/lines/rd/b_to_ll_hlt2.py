###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of B2ll(')(gamma) lines for the RD working group.
It contains also B2HH as this is an important normalisation.
- B -> Mu Mu
- B -> Mu E
- B -> E E
- B -> h h

- B -> Mu Mu Gamma
- B -> E E Gamma
- B -> Mu E Gamma


author: Titus Mombächer
date: 25.10.2021
"""

from Moore.config import HltLine, register_line_builder

from .builders.rdbuilder_thor import make_rd_detached_dihadron
from .builders.b_to_ll_builders import filter_B2MuMu, filter_B2MuE, filter_B2EE, make_B2MuMuGamma, make_B2EEGamma, make_B2MuEGamma

from .builders.rd_prefilters import rd_prefilter, _VRD_MONITORING_VARIABLES

all_lines = {}


@register_line_builder(all_lines)
def Hlt2RD_BToMuMu(name="Hlt2RD_BToMuMu", prescale=1, persistreco=True):
    """
    Register B2mumu (B0 and B_s0) line (opposite sign), persist reco to allow isolation developments
    """

    dimuons = filter_B2MuMu()

    return HltLine(
        name=name,
        algs=rd_prefilter() + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_BToMuMu_SameSign(name="Hlt2RD_BToMuMu_SameSign",
                            prescale=1,
                            persistreco=True):
    """
    Register B2mumu (B0 and B_s0) same sign line, persist reco to allow isolation developments
    """

    dimuons = filter_B2MuMu(same_sign=True)

    return HltLine(
        name=name,
        algs=rd_prefilter() + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_BToEE(name="Hlt2RD_BToEE", prescale=1, persistreco=True):
    """
    Register B2EE (B0 and B_s0) line (opposite sign), persist reco to develop isolation
    """

    dielectrons = filter_B2EE()

    return HltLine(
        name=name,
        algs=rd_prefilter() + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_BToEE_SameSign(name="Hlt2RD_BToEE_SameSign",
                          prescale=1,
                          persistreco=True):
    """
    Register B2EE (B0 and B_s0) same sign line, persist reco to develop isolation
    """

    dielectrons = filter_B2EE(same_sign=True)

    return HltLine(
        name=name,
        algs=rd_prefilter() + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_BToMuE(name="Hlt2RD_BToMuE", prescale=1, persistreco=True):
    """
    Register B2MuE (B0 and B_s0) line (opposite sign), persist reco to develop isolation
    """

    emu = filter_B2MuE()

    return HltLine(
        name=name,
        algs=rd_prefilter() + [emu],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_BToMuE_SameSign(name="Hlt2RD_BToMuE_SameSign",
                           prescale=1,
                           persistreco=True):
    """
    Register B2MuE (B0 and B_s0) same sign line, persist reco to develop isolation
    """

    emu = filter_B2MuE(same_sign=True)

    return HltLine(
        name=name,
        algs=rd_prefilter() + [emu],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_BTohh(name="Hlt2RD_BTohh", prescale=1, persistreco=True):
    """
    Register B2HH (B0 and B_s0) line (opposite sign), persist reco to allow isolation developments
    """

    dihadrons = make_rd_detached_dihadron()

    return HltLine(
        name=name,
        algs=rd_prefilter() + [dihadrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_BToMuMuGamma(name="Hlt2RD_BToMuMuGamma",
                        prescale=1,
                        persistreco=False):
    """
    Register B2MuMuGamma (B0 and B_s0) line (opposite sign dilepton)
    """
    bs = make_B2MuMuGamma()

    return HltLine(
        name=name,
        algs=rd_prefilter() + [bs],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_BToEEGamma(name="Hlt2RD_BToEEGamma", prescale=1, persistreco=False):
    """
    Register B2EEGamma (B0 and B_s0) line (opposite sign dilepton)
    """
    bs = make_B2EEGamma()

    return HltLine(
        name=name,
        algs=rd_prefilter() + [bs],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_BToMuEGamma(name="Hlt2RD_BToMuEGamma",
                       prescale=1,
                       persistreco=False):
    """
    Register B2MuEGamma (B0 and B_s0) line (opposite sign dilepton)
    """
    bs = make_B2MuEGamma()

    return HltLine(
        name=name,
        algs=rd_prefilter() + [bs],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )
