###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Sprucing line definitions of B -> Xll lines, especially
 - B+ -> K+ e+ e- (CC)
 - B+ -> K+ mu+ mu- (CC)
 - B+ -> K+ mu+ e- (CC)
 - B0 -> K*0 (-> K+ pi-) e+ e- (CC)
 - B0 -> K*0 (-> K+ pi-) mu+ mu- (CC)
 - B0 -> K*0 (-> K+ pi-) mu+ e- (CC)

Lepton same sign lines : 
 - B+ -> K+ e- e- (CC)
 - B+ -> K+ mu- mu- (CC)
 - B+ -> K+ mu- e- (CC)
 - B0 -> K*0 (-> K+ pi-) e- e- (CC)
 - B0 -> K*0 (-> K+ pi-) mu- mu- (CC)
 - B0 -> K*0 (-> K+ pi-) mu- e- (CC)

Hadron same sign lines :
 - B0 -> K*++ (-> K+ pi+) e+ e- (CC)
 - B0 -> K*++ (-> K+ pi+) mu+ mu- (CC)
 - B0 -> K*++ (-> K+ pi+) e- e- (CC)
 - B0 -> K*++ (-> K+ pi+) mu- mu- (CC)

Protonic final state lines :
- B(s)0 -> (J/psi(1S) -> mu+ mu-) p+ p~- (cc)
- B(s)0 -> (J/psi(1S) -> mu+ mu+) p+ p~- (cc)
- B+ -> (J/psi(1S) -> mu+ mu-) Lambda0 (-> p+ pi-) p+ (cc) 
- B+ -> (J/psi(1S) -> mu+ mu+) Lambda0 (-> p+ pi-) p+ (cc) 

Contact authors : Richard Morgan Williams (richard.morgan.williams@cern.ch), Felicia Carolin Volle (felicia.carolin.volle@cern.ch)

Last update : 13/05/2022

'''
from Moore.lines import SpruceLine
from Moore.config import register_line_builder
from PyConf import configurable

from RecoConf.reconstruction_objects import make_pvs
from .builders import rdbuilder_thor
from .builders import b_to_xll_builders
from .builders.rd_prefilters import rd_prefilter
from GaudiKernel.SystemOfUnits import MeV
import Functors as F

sprucing_lines = {}
########################################
#      B+ -> K+ ll sprucing lines      #
########################################

#### Selections #####
BtoKee = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 16.,
        "bpvipchi2_max": 9.,
    },
    "dielectrons": {
        "adocachi2cut_min": 9.,
        "ipchi2_e_min": 9.,
        "pid_e_min": 1.,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 250. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "kaons": {
        "pt_min": 250. * MeV,
        "mipchi2dvprimary_min": 9.,
        "p_min": 1_000. * MeV,
        "pid": (F.PID_K > -2.),
    }
}

BtoKmumu = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 16.,
        "bpvipchi2_max": 16.,
    },
    "dimuons": {
        "adocachi2cut_max": 25.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 250. * MeV,
        "vchi2pdof_max": 25.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "kaons": {
        "pt_min": 250. * MeV,
        "mipchi2dvprimary_min": 9.,
        "p_min": 1_000. * MeV,
        "pid": (F.PID_K > -2.),
    }
}


@register_line_builder(sprucing_lines)
def Spruce_BuToKpEE_line(name="SpruceRD_BuToKpEE", prescale=1):
    '''
    Sprucing line for B+ -> K+ e+ e- (cc)
    '''
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKee["dielectrons"], opposite_sign=True)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKee["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectronDecision',
            'Hlt2_InclDetDiElectron_3BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpEE_SameSign_line(name="SpruceRD_BuToKpEE_SameSign",
                                  prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKee["dielectrons"], opposite_sign=False)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKee["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectron_SSDecision',
            'Hlt2_InclDetDiElectron_3Body_SSDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpMuMu_line(name="SpruceRD_BuToKpMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKmumu["dimuons"], same_sign=False)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKmumu["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKmumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuonDecision',
            'Hlt2_InclDetDiMuon_3BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpMuMu_SameSign_line(name="SpruceRD_BuToKpMuMu_SameSign",
                                    prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKmumu["dimuons"], same_sign=True)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKmumu["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKmumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuon_SSDecision',
            'Hlt2_InclDetDiMuon_3Body_SSDecision',
        ])


#########################################
#      B+ -> pi+ ll sprucing lines      #
#########################################

#### Selections #####
Btopiee = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 9.,
    },
    "dielectrons": {
        "adocachi2cut_min": 9.,
        "ipchi2_e_min": 9.,
        "pid_e_min": 1.,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 250. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "pions": {
        "pt_min": 250. * MeV,
        "mipchi2dvprimary_min": 9.,
        "p_min": 1_000. * MeV,
        "pid": (F.PID_K <= 4.),
    }
}

Btopimumu = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 16.,
        "bpvipchi2_max": 16.,
    },
    "dimuons": {
        "adocachi2cut_max": 25.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 250. * MeV,
        "vchi2pdof_max": 25.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "pions": {
        "pt_min": 250. * MeV,
        "mipchi2dvprimary_min": 9.,
        "p_min": 1_000. * MeV,
        "pid": (F.PID_K <= 4.),
    }
}


@register_line_builder(sprucing_lines)
def Spruce_BuToPipEE_line(name="SpruceRD_BuToPipEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopiee["dielectrons"], opposite_sign=True)
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopiee["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopiee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectronDecision',
            'Hlt2_InclDetDiElectron_3BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToPipEE_SameSign_line(name="SpruceRD_BuToPipEE_SameSign",
                                   prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopiee["dielectrons"], opposite_sign=False)
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopiee["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopiee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectron_SSDecision',
            'Hlt2_InclDetDiElectron_3Body_SSDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToPipMuMu_line(name="SpruceRD_BuToPipMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopimumu["dimuons"], same_sign=False)
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopimumu["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopimumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuonDecision',
            'Hlt2_InclDetDiMuon_3BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToPipMuMu_SameSign_line(name="SpruceRD_BuToPipMuMu_SameSign",
                                     prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopimumu["dimuons"], same_sign=True)
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopimumu["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopimumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuon_SSDecision',
            'Hlt2_InclDetDiMuon_3Body_SSDecision',
        ])


############################################
#      B+ -> K+ pi0 ll sprucing lines      #
############################################

#### Selections #####
BtoKpResolvedPi0ee = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 9.,
    },
    "dielectrons": {
        "adocachi2cut_min": 9.,
        "ipchi2_e_min": 9.,
        "pid_e_min": 1.,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 250. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "Kstps": {
        "PIDK_K_min": 1.,
        "p_K_min": 1_000. * MeV,
        "am_min": 600. * MeV,
        "am_max": 2_500. * MeV,
        "pi0_type": "resolved",
        "Kstp_PT": 500. * MeV,
        "min_ipchi2_k": 9.,
        "Kstp_max_vtxchi2": 36.,
        "pt_K_min": 2_000. * MeV,
        "pt_pi_min": 500. * MeV,
    }
}
BtoKpResolvedPi0mumu = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 9.,
    },
    "dimuons": {
        "adocachi2cut_max": 25.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -2.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 250. * MeV,
        "vchi2pdof_max": 25.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "Kstps": {
        "PIDK_K_min": 1.,
        "p_K_min": 1_000. * MeV,
        "am_min": 600. * MeV,
        "am_max": 2_500. * MeV,
        "pi0_type": "resolved",
        "Kstp_PT": 500. * MeV,
        "min_ipchi2_k": 9.,
        "Kstp_max_vtxchi2": 36.,
        "pt_K_min": 1_000. * MeV,
        "pt_pi_min": 500. * MeV,
    }
}


@register_line_builder(sprucing_lines)
def Spruce_BuToKpResolvedPi0EE_line(name="SpruceRD_BuToKpResolvedPi0EE",
                                    prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKpResolvedPi0ee["dielectrons"], opposite_sign=True)
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(**BtoKpResolvedPi0ee["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0ee["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectronDecision',
            'Hlt2_InclDetDiElectron_3BodyDecision',
            'Hlt2_InclDetDiElectron_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpResolvedPi0EE_SameSign_line(
        name="SpruceRD_BuToKpResolvedPi0EE_SameSign", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKpResolvedPi0ee["dielectrons"], opposite_sign=False)
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(**BtoKpResolvedPi0ee["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0ee["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectron_SSDecision',
            'Hlt2_InclDetDiElectron_3Body_SSDecision',
            'Hlt2_InclDetDiElectron_4Body_SSDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpResolvedPi0MuMu_line(name="SpruceRD_BuToKpResolvedPi0MuMu",
                                      prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKpResolvedPi0mumu["dimuons"], same_sign=False)
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(
        **BtoKpResolvedPi0mumu["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0mumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuonDecision',
            'Hlt2_InclDetDiMuon_3BodyDecision',
            'Hlt2_InclDetDiMuon_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpResolvedPi0MuMu_SameSign_line(
        name="SpruceRD_BuToKpResolvedPi0MuMu_SameSign", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKpResolvedPi0mumu["dimuons"], same_sign=True)
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(
        **BtoKpResolvedPi0mumu["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0mumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuon_SSDecision',
            'Hlt2_InclDetDiMuon_3Body_SSDecision',
            'Hlt2_InclDetDiMuon_4Body_SSDecision',
        ])


#############################################
#      B0 -> pi+ pi- ll sprucing lines      #
#############################################

#### Selections #####
Btopipiee = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 16.,
        "bpvipchi2_max": 16.,
    },
    "dielectrons": {
        "adocachi2cut_min": 9.,
        "ipchi2_e_min": 9.,
        "pid_e_min": 1.,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 250. * MeV,
        "vfaspfchi2ndof_max": 25.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "rhos": {
        "pi_pid": (F.PID_K <= 4.),
        "am_min": 500. * MeV,
        "am_max": 1_500. * MeV,
        "pi_pt_min": 250. * MeV,
        "pt_min": 250. * MeV,
        "pi_ipchi2_min": 9.,
        "adocachi2cut": 25.,
        "vchi2pdof_max": 16.,
    }
}

Btopipimumu = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 16.,
        "bpvipchi2_max": 16.,
    },
    "dimuons": {
        "adocachi2cut_max": 25.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 250. * MeV,
        "vchi2pdof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "rhos": {
        "pi_pid": (F.PID_K <= 4.),
        "am_min": 500. * MeV,
        "am_max": 1_500. * MeV,
        "pi_pt_min": 250. * MeV,
        "pt_min": 250. * MeV,
        "pi_ipchi2_min": 9.,
        "adocachi2cut": 25.,
        "vchi2pdof_max": 25.,
    }
}


@register_line_builder(sprucing_lines)
def Spruce_B0ToPipPimEE_line(name="SpruceRD_B0ToPipPimEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopipiee["dielectrons"], opposite_sign=True)
    rhos = rdbuilder_thor.make_rd_detached_rho0(**Btopipiee["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        rhos,
        pvs,
        Descriptor="[B0 -> J/psi(1S) rho(770)0]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopipiee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectronDecision',
            'Hlt2_InclDetDiElectron_3BodyDecision',
            'Hlt2_InclDetDiElectron_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_B0ToPipPimEE_SameSign_line(name="SpruceRD_B0ToPipPimEE_SameSign",
                                      prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopipiee["dielectrons"], opposite_sign=False)
    rhos = rdbuilder_thor.make_rd_detached_rho0(**Btopipiee["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        rhos,
        pvs,
        Descriptor="[B0 -> J/psi(1S) rho(770)0]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopipiee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectron_SSDecision',
            'Hlt2_InclDetDiElectron_3Body_SSDecision',
            'Hlt2_InclDetDiElectron_4Body_SSDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_B0ToPipPimMuMu_line(name="SpruceRD_B0ToPipPimMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopipimumu["dimuons"], same_sign=False)
    rhos = rdbuilder_thor.make_rd_detached_rho0(**Btopipimumu["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        rhos,
        pvs,
        Descriptor="[B0 -> J/psi(1S) rho(770)0]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopipimumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuonDecision',
            'Hlt2_InclDetDiMuon_3BodyDecision',
            'Hlt2_InclDetDiMuon_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_B0ToPipPimMuMu_SameSign_line(
        name="SpruceRD_B0ToPipPimMuMu_SameSign", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopipimumu["dimuons"], same_sign=True)
    rhos = rdbuilder_thor.make_rd_detached_rho0(**Btopipimumu["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        rhos,
        pvs,
        Descriptor="[B0 -> J/psi(1S) rho(770)0]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopipimumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuon_SSDecision',
            'Hlt2_InclDetDiMuon_3Body_SSDecision',
            'Hlt2_InclDetDiMuon_4Body_SSDecision',
        ])


###########################################
#      B0 -> K+ K- ll sprucing lines      #
###########################################

#### Selections #####
BtoKKee = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 25.,
        "bpvipchi2_max": 25.,
    },
    "dielectrons": {
        "adocachi2cut_min": 25.,
        "ipchi2_e_min": 9.,
        "pid_e_min": 1.,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 250. * MeV,
        "vfaspfchi2ndof_max": 25.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "phis": {
        "k_pid": (F.PID_K > -4.),
        "am_min": 950. * MeV,
        "am_max": 2_000. * MeV,
        "k_p_min": 1_000. * MeV,
        "k_pt_min": 250. * MeV,
        "k_ipchi2_min": 9.,
        "phi_pt_min": 0. * MeV,
        "adocachi2cut": 25.,
        "vchi2pdof_max": 25.,
    }
}

BtoKKmumu = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 25.,
        "bpvipchi2_max": 25.,
    },
    "dimuons": {
        "adocachi2cut_max": 25.,
        "ipchi2_muon_min": 4.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 250. * MeV,
        "vchi2pdof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "phis": {
        "k_pid": (F.PID_K > -4.),
        "am_min": 950. * MeV,
        "am_max": 1_100. * MeV,
        "k_p_min": 2_000. * MeV,
        "k_pt_min": 0. * MeV,
        "k_ipchi2_min": 9.,
        "phi_pt_min": 250. * MeV,
        "adocachi2cut": 25.,
        "vchi2pdof_max": 25.,
    }
}


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpKmEE_line(name="SpruceRD_B0ToKpKmEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKKee["dielectrons"], opposite_sign=True)
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKee["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        phis,
        pvs,
        Descriptor="[B0 -> J/psi(1S) phi(1020)]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKKee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectronDecision',
            'Hlt2_InclDetDiElectron_3BodyDecision',
            'Hlt2_InclDetDiElectron_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpKmEE_SameSign_line(name="SpruceRD_B0ToKpKmEE_SameSign",
                                    prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKKee["dielectrons"], opposite_sign=False)
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKee["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        phis,
        pvs,
        Descriptor="[B0 -> J/psi(1S) phi(1020)]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKKee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectron_SSDecision',
            'Hlt2_InclDetDiElectron_3Body_SSDecision',
            'Hlt2_InclDetDiElectron_4Body_SSDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpKmMuMu_line(name="SpruceRD_B0ToKpKmMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKKmumu["dimuons"], same_sign=False)
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKmumu["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        phis,
        pvs,
        Descriptor="[B0 -> J/psi(1S) phi(1020)]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKKmumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuonDecision',
            'Hlt2_InclDetDiMuon_3BodyDecision',
            'Hlt2_InclDetDiMuon_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpKmMuMu_SameSign_line(name="SpruceRD_B0ToKpKmMuMu_SameSign",
                                      prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKKmumu["dimuons"], same_sign=True)
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKmumu["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        phis,
        pvs,
        Descriptor="[B0 -> J/psi(1S) phi(1020)]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKKmumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuon_SSDecision',
            'Hlt2_InclDetDiMuon_3Body_SSDecision',
            'Hlt2_InclDetDiMuon_4Body_SSDecision',
        ])


############################################
#      B0 -> K+ pi- ll sprucing lines      #
############################################

#### Selections #####
BtoKPiee = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 16.,
    },
    "dielectrons": {
        "adocachi2cut_min": 9.,
        "ipchi2_e_min": 9.,
        "pid_e_min": 1.,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 250. * MeV,
        "vfaspfchi2ndof_max": 25.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "Kst0s": {
        "am_min": 600. * MeV,
        "am_max": 2_500. * MeV,
        "pi_pid": (F.PID_K < 4.),
        "k_pid": (F.PID_K > -4.),
        "pi_p_min": 1_000. * MeV,
        "pi_pt_min": 250. * MeV,
        "pi_ipchi2_min": 16.,
        "k_p_min": 1_000. * MeV,
        "k_pt_min": 250. * MeV,
        "k_ipchi2_min": 16.,
        "kstar0_pt_min": 250. * MeV,
        "adocachi2cut": 16.,
        "vchi2pdof_max": 25.,
    }
}

BtoKPimumu = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 25.,
        "bpvipchi2_max": 25.,
    },
    "dimuons": {
        "adocachi2cut_max": 25.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 250. * MeV,
        "vchi2pdof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "Kst0s": {
        "am_min": 600. * MeV,
        "am_max": 2_500. * MeV,
        "pi_pid": (F.PID_K < 4.),
        "k_pid": (F.PID_K > -4.),
        "pi_p_min": 1_000. * MeV,
        "pi_pt_min": 250. * MeV,
        "pi_ipchi2_min": 9.,
        "k_p_min": 1_000. * MeV,
        "k_pt_min": 250. * MeV,
        "k_ipchi2_min": 9.,
        "kstar0_pt_min": 250. * MeV,
        "adocachi2cut": 25.,
        "vchi2pdof_max": 25.,
    }
}


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpPimEE_line(name="SpruceRD_B0ToKpPimEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiee["dielectrons"], opposite_sign=True)
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**BtoKPiee["Kst0s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiee["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectronDecision',
            'Hlt2_InclDetDiElectron_3BodyDecision',
            'Hlt2_InclDetDiElectron_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpPimEE_SameSign_line(name="SpruceRD_B0ToKpPimEE_SameSign",
                                     prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiee["dielectrons"], opposite_sign=False)
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**BtoKPiee["Kst0s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiee["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectron_SSDecision',
            'Hlt2_InclDetDiElectron_3Body_SSDecision',
            'Hlt2_InclDetDiElectron_4Body_SSDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpPimMuMu_line(name="SpruceRD_B0ToKpPimMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPimumu["dimuons"], same_sign=False)
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**BtoKPimumu["Kst0s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPimumu["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuonDecision',
            'Hlt2_InclDetDiMuon_3BodyDecision',
            'Hlt2_InclDetDiMuon_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def B0ToKpPimMuMu_SameSign_line(name="SpruceRD_B0ToKpPimMuMu_SameSign",
                                prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPimumu["dimuons"], same_sign=True)
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**BtoKPimumu["Kst0s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPimumu["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuon_SSDecision',
            'Hlt2_InclDetDiMuon_3Body_SSDecision',
            'Hlt2_InclDetDiMuon_4Body_SSDecision',
        ])


################################################
#      B+ -> K+ pi+ pi- ll sprucing lines      #
################################################

#### Selections #####
BtoKPiPiee = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 9.,
    },
    "dielectrons": {
        "adocachi2cut_min": 16.,
        "ipchi2_e_min": 16.,
        "pid_e_min": 1.,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 250. * MeV,
        "vfaspfchi2ndof_max": 16.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "K1s": {
        "pi_PIDK_max": 4,
        "K_PIDK_min": -4,
        "K_1_pt_min": 250. * MeV,
        "pi_p_min": 1_000. * MeV,
        "pi_pt_min": 250. * MeV,
        "pi_ipchi2_min": 9.,
        "K_p_min": 1_000. * MeV,
        "K_pt_min": 250. * MeV,
        "K_ipchi2_min": 25.,
        "vchi2pdof_max": 25.,
    },
}

BtoKPiPimumu = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 16.,
        "bpvipchi2_max": 16.,
    },
    "dimuons": {
        "adocachi2cut_max": 25.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 250. * MeV,
        "vchi2pdof_max": 25.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "K1s": {
        "pi_PIDK_max": 4,
        "K_PIDK_min": -4,
        "K_1_pt_min": 250. * MeV,
        "pi_p_min": 1_000. * MeV,
        "pi_pt_min": 250. * MeV,
        "pi_ipchi2_min": 9.,
        "K_p_min": 1_000. * MeV,
        "K_pt_min": 250. * MeV,
        "K_ipchi2_min": 9.,
        "vchi2pdof_max": 9.,
    },
}


@register_line_builder(sprucing_lines)
def Spruce_BuToKpPipPimEE_line(name="SpruceRD_BuToKpPipPimEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiPiee["dielectrons"], opposite_sign=True)
    K1s = rdbuilder_thor.make_rd_detached_K1(**BtoKPiPiee["K1s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+ ]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPiee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectronDecision',
            'Hlt2_InclDetDiElectron_3BodyDecision',
            'Hlt2_InclDetDiElectron_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpPipPimEE_SameSign_line(
        name="SpruceRD_BuToKpPipPimEE_SameSign", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiPiee["dielectrons"], opposite_sign=False)
    K1s = rdbuilder_thor.make_rd_detached_K1(**BtoKPiPiee["K1s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+ ]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPiee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectron_SSDecision',
            'Hlt2_InclDetDiElectron_3Body_SSDecision',
            'Hlt2_InclDetDiElectron_4Body_SSDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpPipPimMuMu_line(name="SpruceRD_BuToKpPipPimMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPiPimumu["dimuons"], same_sign=False)
    K1s = rdbuilder_thor.make_rd_detached_K1(**BtoKPiPimumu["K1s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPimumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuonDecision',
            'Hlt2_InclDetDiMuon_3BodyDecision',
            'Hlt2_InclDetDiMuon_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpPipPimMuMu_SameSign_line(
        name="SpruceRD_BuToKpPipPimMuMu_SameSign", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPiPimumu["dimuons"], same_sign=True)
    K1s = rdbuilder_thor.make_rd_detached_K1(**BtoKPiPimumu["K1s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPimumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuon_SSDecision',
            'Hlt2_InclDetDiMuon_3Body_SSDecision',
            'Hlt2_InclDetDiMuon_4Body_SSDecision',
        ])


##############################################
#      B+ -> K+ K+ K- ll sprucing lines      #
##############################################

#### Selections #####
Bto3Kee = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 16.,
        "bpvipchi2_max": 16.,
    },
    "dielectrons": {
        "adocachi2cut_min": 9.,
        "ipchi2_e_min": 16.,
        "pid_e_min": 1.,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 250. * MeV,
        "vfaspfchi2ndof_max": 16.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "K2s": {
        "K_PIDK_min": -2,
        "K_p_min": 1_000. * MeV,
        "K_pt_min": 250. * MeV,
        "K_ipchi2_min": 9.,
        "K_2_pt_min": 0. * MeV,
        "vchi2pdof_max": 16.0,
    },
}

Bto3Kmumu = {
    "B": {
        "FDchi2_min": 100.,
        "vchi2pdof_max": 16.,
        "bpvipchi2_max": 16.,
    },
    "dimuons": {
        "adocachi2cut_max": 25.,
        "ipchi2_muon_min": 4.,
        "pidmu_muon_min": -2.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 250. * MeV,
        "vchi2pdof_max": 25.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "K2s": {
        "K_PIDK_min": -2,
        "K_p_min": 1_000. * MeV,
        "K_pt_min": 250. * MeV,
        "K_ipchi2_min": 9.,
        "K_2_pt_min": 0. * MeV,
        "vchi2pdof_max": 36.,
    },
}


@register_line_builder(sprucing_lines)
def Spruce_BuToKpKpKmEE_line(name="SpruceRD_BuToKpKpKmEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Bto3Kee["dielectrons"], opposite_sign=True)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3Kee["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kee["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectronDecision',
            'Hlt2_InclDetDiElectron_3BodyDecision',
            'Hlt2_InclDetDiElectron_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpKpKmEE_SameSign_line(name="SpruceRD_BuToKpKpKmEE_SameSign",
                                      prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Bto3Kee["dielectrons"], opposite_sign=False)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3Kee["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kee["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiElectron_SSDecision',
            'Hlt2_InclDetDiElectron_3Body_SSDecision',
            'Hlt2_InclDetDiElectron_4Body_SSDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpKpKmMuMu_line(name="SpruceRD_BuToKpKpKmMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Bto3Kmumu["dimuons"], same_sign=False)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3Kmumu["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kmumu["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuonDecision',
            'Hlt2_InclDetDiMuon_3BodyDecision',
            'Hlt2_InclDetDiMuon_4BodyDecision',
        ])


@register_line_builder(sprucing_lines)
def Spruce_BuToKpKpKmMuMu_SameSign_line(
        name="SpruceRD_BuToKpKpKmMuMu_SameSign", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Bto3Kmumu["dimuons"], same_sign=True)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3Kmumu["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kmumu["B"])

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuon_SSDecision',
            'Hlt2_InclDetDiMuon_3Body_SSDecision',
            'Hlt2_InclDetDiMuon_4Body_SSDecision',
        ])


##################
### MuE lines ####
##################


@register_line_builder(sprucing_lines)
@configurable
def Spruce_BuToKpMuE_line(name="SpruceRD_BuToKpMuE", prescale=1):
    '''
    Sprucing line for B+ -> K+ mu+ e- (cc)
    '''
    pvs = make_pvs()
    electronmuon = rdbuilder_thor.make_rd_detached_mue(
        same_sign=False, max_vchi2ndof=9.)
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9., pid=(F.PID_K > 0.), pt_min=400. * MeV)

    Bu = b_to_xll_builders.make_rd_BToXll(
        electronmuon,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [electronmuon, Bu],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuEDecision', 'Hlt2_InclDetDiMuE_3BodyDecision'
        ])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_B0ToKpPimMuE_line(name="SpruceRD_B0ToKpPimMuE", prescale=1):
    '''
    Sprucing line for B0 -> (K*0 -> K+ pi-) mu+ e-
    '''
    pvs = make_pvs()
    electronmuon = rdbuilder_thor.make_rd_detached_mue(
        same_sign=False, max_vchi2ndof=9.)
    hadron = rdbuilder_thor.make_rd_detached_kstar0s(
        am_min=500 * MeV,
        am_max=2600 * MeV,
        pi_pid=(F.PID_K < -0.),  ##new
        k_pid=(F.PID_K > 0.),
        pi_ipchi2_min=9.,
        k_ipchi2_min=9.,
        pi_pt_min=250 * MeV,
        k_pt_min=250. * MeV,
        adocachi2cut=30.,
        vchi2pdof_max=9.0,
    )

    B0 = b_to_xll_builders.make_rd_BToXll(
        electronmuon,
        hadron,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0 ]cc",
        name="make_rd_BToXll_for_" + name,
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [electronmuon, hadron, B0],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuEDecision', 'Hlt2_InclDetDiMuE_3BodyDecision',
            'Hlt2_InclDetDiMuE_4BodyDecision'
        ])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_BuToKpMuE_SameSignMuE_line(name="SpruceRD_BuToKpMuE_SameSignMuE",
                                      prescale=1):
    '''
    Sprucing line for B+ -> K+ mu- e- (cc)
    '''
    pvs = make_pvs()
    electronmuon = rdbuilder_thor.make_rd_detached_mue(
        same_sign=True, max_vchi2ndof=9.)
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9., pid=(F.PID_K > 0.), pt_min=400. * MeV)

    Bu = b_to_xll_builders.make_rd_BToXll(
        electronmuon,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [electronmuon, Bu],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_SSDecision',
            'Hlt2_InclDetDiMuE_3Body_SSDecision'
        ])


@register_line_builder(sprucing_lines)
@configurable
def Spruce_B0ToKpPimMuE_SameSignMuE_line(
        name="SpruceRD_B0ToKpPimMuE_SameSignMuE", prescale=1):
    '''
    Sprucing line for B0 -> (K*0 -> K+ pi-) mu- e-
    '''
    pvs = make_pvs()
    electronmuon = rdbuilder_thor.make_rd_detached_mue(
        same_sign=True, max_vchi2ndof=9.)
    hadron = rdbuilder_thor.make_rd_detached_kstar0s(
        am_min=500 * MeV,
        am_max=2600 * MeV,
        pi_pid=(F.PID_K < -0.),  ##new
        k_pid=(F.PID_K > 0.),
        pi_ipchi2_min=9.,
        k_ipchi2_min=9.,
        pi_pt_min=250 * MeV,
        k_pt_min=250. * MeV,
        adocachi2cut=30.,
        vchi2pdof_max=9.0,
    )

    B0 = b_to_xll_builders.make_rd_BToXll(
        electronmuon,
        hadron,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0 ]cc",
        name="make_rd_BToXll_for_" + name,
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [electronmuon, hadron, B0],
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_SSDecision',
            'Hlt2_InclDetDiMuE_3Body_SSDecision',
            'Hlt2_InclDetDiMuE_4Body_SSDecision'
        ])


###########################
# BToXpbarll Spruce lines #
###########################


@register_line_builder(sprucing_lines)
def BToPpPmEE_line(name="SpruceRD_BToPpPmEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9.)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, protons, Bs],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_Decision', 'Hlt2_InclDetDiMuE_3Body_Decision',
            'Hlt2_InclDetDiMuE_4Body_Decision'
        ])


@register_line_builder(sprucing_lines)
def BToPpPmEE_SameSign_line(name="SpruceRD_BToPpPmEESS", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9., opposite_sign=False)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, protons, Bs],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_SSDecision',
            'Hlt2_InclDetDiMuE_3Body_SSDecision',
            'Hlt2_InclDetDiMuE_4Body_SSDecision'
        ])


@register_line_builder(sprucing_lines)
def BuToL0PmEE_LL_line(name="SpruceRD_BuToL0PmEE_LL", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        am_Xpbar_min=2000. * MeV,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_Decision', 'Hlt2_InclDetDiMuE_3Body_Decision',
            'Hlt2_InclDetDiMuE_4Body_Decision'
        ])


@register_line_builder(sprucing_lines)
def BuToL0PmEE_LL_SameSign_line(name="SpruceRD_BuToL0PmEESS_LL", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9., opposite_sign=False)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_SSDecision',
            'Hlt2_InclDetDiMuE_3Body_SSDecision',
            'Hlt2_InclDetDiMuE_4Body_SSDecision'
        ])


#@register_line_builder(sprucing_lines)
def BuToL0PmEE_DD_line(name="SpruceRD_BuToL0PmEE_DD", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_Decision', 'Hlt2_InclDetDiMuE_3Body_Decision',
            'Hlt2_InclDetDiMuE_4Body_Decision'
        ])


#@register_line_builder(sprucing_lines)
def BuToL0PmEE_DD_SameSign_line(name="SpruceRD_BuToL0PmEESS_DD", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9., opposite_sign=False)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_SSDecision',
            'Hlt2_InclDetDiMuE_3Body_SSDecision',
            'Hlt2_InclDetDiMuE_4Body_SSDecision'
        ])


@register_line_builder(sprucing_lines)
def BToPpPmMuMu_line(name="SpruceRD_BToPpPmMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(vchi2pdof_max=9.)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, protons, Bs],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_Decision', 'Hlt2_InclDetDiMuE_3Body_Decision',
            'Hlt2_InclDetDiMuE_4Body_Decision'
        ])


@register_line_builder(sprucing_lines)
def BToPpPmMuMu_SameSign_line(name="SpruceRD_BToPpPmMuMuSS", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        same_sign=True, vchi2pdof_max=9.)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, protons, Bs],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_SSDecision',
            'Hlt2_InclDetDiMuE_3Body_SSDecision',
            'Hlt2_InclDetDiMuE_4Body_SSDecision'
        ])


@register_line_builder(sprucing_lines)
def BuToL0PmMuMu_LL_line(name="SpruceRD_BuToL0PmMuMu_LL", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(vchi2pdof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_Decision', 'Hlt2_InclDetDiMuE_3Body_Decision',
            'Hlt2_InclDetDiMuE_4Body_Decision'
        ])


@register_line_builder(sprucing_lines)
def BuToL0PmMuMu_LL_SameSign_line(name="SpruceRD_BuToL0PmMuMuSS_LL",
                                  prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        same_sign=True, vchi2pdof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_SSDecision',
            'Hlt2_InclDetDiMuE_3Body_SSDecision',
            'Hlt2_InclDetDiMuE_4Body_SSDecision'
        ])


#@register_line_builder(sprucing_lines)
def BuToL0PmMuMu_DD_line(name="SpruceRD_BuToL0PmMuMu_DD", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(vchi2pdof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_Decision', 'Hlt2_InclDetDiMuE_3Body_Decision',
            'Hlt2_InclDetDiMuE_4Body_Decision'
        ])


#@register_line_builder(sprucing_lines)
def BuToL0PmMuMu_DD_SameSign_line(name="SpruceRD_BuToL0PmMuMuSS_DD",
                                  prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        same_sign=True, vchi2pdof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=[
            'Hlt2_InclDetDiMuE_SSDecision',
            'Hlt2_InclDetDiMuE_3Body_SSDecision',
            'Hlt2_InclDetDiMuE_4Body_SSDecision'
        ])
