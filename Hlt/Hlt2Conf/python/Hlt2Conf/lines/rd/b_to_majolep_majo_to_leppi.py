###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This defines HLT2 lines for B+(-)/B_c+(-) -> mu/e(+/-) N, N-> e/mu pi, where N is a neutral lepton
And a normalisation line for B+ -> pi+ KS0, where KS0 -> pi+pi-
Both same sign lepton and opposite sign lepton are included due to the KS0 descriptor being independent of the lepton signs.
N has no lifetime hypothesis applied. Combination of the neutral lepton daugthers is limited between 200(to cut conversion)-7000 MeV
All leptons from B are Long. Daughters of the neutral lepton can be both Long and Downstream.
Loose and Tight lines have a different PID cut looseness on pions and electrons. Note, PID_E is applied to pions too, so there is a need for a no_brem added calibration.
Tight lines will hopefully have raw event information saved, specifically tracking infor.

List of lines (LL - Long, DD - downstream):

Hlt2RD_BpToMajoMu_MajoToEPi_LL_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> epi, LL)
Hlt2RD_BpToMajoE_MajoToMuPi_LL_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mupi, LL)
Hlt2RD_BpToMajoMu_MajoToEPi_DD_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> epi, DD)
Hlt2RD_BpToMajoE_MajoToMuPi_DD_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mupi, DD)

Hlt2RD_BpToMajoMu_MajoToEPi_LL_Loose: loose PID cut line for the B(_c)+ -> mu HNL(-> epi, LL)
Hlt2RD_BpToMajoE_MajoToMuPi_LL_Loose: loose PID cut line for the B(_c)+ -> e HNL(-> mupi, LL)
Hlt2RD_BpToMajoMu_MajoToEPi_DD_Loose: loose PID cut line for the B(_c)+ -> mu HNL(-> epi, DD)
Hlt2RD_BpToMajoE_MajoToMuPi_DD_Loose: loose PID cut line for the B(_c)+ -> e HNL(-> mupi, DD)

Hlt2RD_BuToKs0Pi_Ks0ToPiPi_LL: B+ -> pi KS0 (->pipi) LL line
Hlt2RD_BuToKs0Pi_Ks0ToPiPi_DD: B+ -> pi KS0 (->pipi) DD line


Contact: Lera Lukashenko, valeriia.lukashenko@cern.ch

"""
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.standard_particles import (
    make_long_pions, make_long_muons, make_long_electrons_with_brem,
    make_down_pions, make_down_muons, make_down_electrons_no_brem,
    make_KsLL_fromSV, make_KsDD_fromSV)

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders import rad_incl_builder
from Hlt2Conf.lines.rd.builders.majorana_builder import make_majorana_lepton, make_majorana, make_bhadron_majorana
from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _VRD_MONITORING_VARIABLES
import Functors as F

all_lines = {}


@register_line_builder(all_lines)
def BpToMajoMu_MajoToEPi_LL_Tight_line(
        name="Hlt2RD_BpToMajoMu_MajoToEPi_LL_Tight", prescale=1,
        presistreco=True):
    pvs = make_pvs

    muons = make_majorana_lepton(
        leptons=make_long_muons,
        name='majo_long_muons_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=F.ISMUON)

    electrons_with_brem = make_majorana_lepton(
        leptons=make_long_electrons_with_brem,
        name='majo_long_electrons_with_brem_{hash}',
        pvs=pvs,
        pid=(F.PID_E > 1.))

    pions = make_majorana_lepton(
        leptons=make_long_pions,
        name='majo_long_pions_{hash}',
        pvs=pvs,
        pid=(F.PID_E <= 0.))

    majoranas2piE = make_majorana(
        name='Majo2epi_{hash}',
        pions=pions,
        leptons=electrons_with_brem,
        descriptor='[KS0 -> e- pi+]cc')

    b2MuN = make_bhadron_majorana(
        name='B2MajoMu_{hash}',
        majoranas=majoranas2piE,
        leptons=muons,
        pvs=pvs,
        descriptor='[B+ -> KS0 mu+]cc')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [majoranas2piE, b2MuN],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BpToMajoE_MajoToMuPi_LL_Tight_line(
        name="Hlt2RD_BpToMajoE_MajoToMuPi_LL_Tight", prescale=1,
        presistreco=True):
    pvs = make_pvs

    muons = make_majorana_lepton(
        leptons=make_long_muons,
        name='majo_long_muons_{hash}',
        pvs=pvs,
        pid=F.ISMUON)

    electrons_with_brem = make_majorana_lepton(
        leptons=make_long_electrons_with_brem,
        name='majo_long_electrons_with_brem_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=(F.PID_E > 1.))

    pions = make_majorana_lepton(
        leptons=make_long_pions,
        name='majo_long_pions_{hash}',
        pvs=pvs,
        pid=(F.PID_E <= 0.))

    majoranas2piMu = make_majorana(
        name='Majo2mupi_{hash}',
        pions=pions,
        leptons=muons,
        descriptor='[KS0 -> mu- pi+]cc')

    b2EN = make_bhadron_majorana(
        name='B2MajoE_{hash}',
        majoranas=majoranas2piMu,
        leptons=electrons_with_brem,
        pvs=pvs,
        descriptor='[B+ -> KS0 e+]cc')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [majoranas2piMu, b2EN],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BpToMajoMu_MajoToEPi_DD_Tight_line(
        name="Hlt2RD_BpToMajoMu_MajoToEPi_DD_Tight", prescale=1,
        presistreco=True):
    pvs = make_pvs

    muons = make_majorana_lepton(
        leptons=make_long_muons,
        name='majo_long_muons_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=F.ISMUON)

    down_electrons_without_brem = make_majorana_lepton(
        leptons=make_down_electrons_no_brem,
        name='majo_down_electrons_without_brem_{hash}',
        pvs=pvs,
        mipchi2dvprimary_min=None,
        pt_min=600 * MeV,
        pid=(F.PID_E > 1.))

    down_pions = make_majorana_lepton(
        leptons=make_down_pions,
        name='majo_down_pions_{hash}',
        pvs=pvs,
        pt_min=600 * MeV,
        mipchi2dvprimary_min=None,
        pid=(F.PID_E <= 0.))

    majoranas2piE = make_majorana(
        name='Majo2epi_{hash}',
        pions=down_pions,
        leptons=down_electrons_without_brem,
        descriptor='[KS0 -> e- pi+]cc')

    b2MuN = make_bhadron_majorana(
        name='B2MajoMu_{hash}',
        majoranas=majoranas2piE,
        leptons=muons,
        pvs=pvs,
        descriptor='[B+ -> KS0 mu+]cc')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [majoranas2piE, b2MuN],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BpToMajoE_MajoToMuPi_DD_Tight_line(
        name="Hlt2RD_BpToMajoE_MajoToMuPi_DD_Tight", prescale=1,
        presistreco=True):
    pvs = make_pvs

    electrons_with_brem = make_majorana_lepton(
        leptons=make_long_electrons_with_brem,
        name='majo_long_electrons_with_brem_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=(F.PID_E > 1.))

    down_muons = make_majorana_lepton(
        leptons=make_down_muons,
        name='majo_down_muons_{hash}',
        pvs=pvs,
        mipchi2dvprimary_min=None,
        pt_min=600 * MeV,
        pid=((F.ISMUON) & (F.PID_MU > 0.)))

    down_pions = make_majorana_lepton(
        leptons=make_down_pions,
        name='majo_down_pions_{hash}',
        pvs=pvs,
        mipchi2dvprimary_min=None,
        pt_min=600 * MeV,
        pid=(F.PID_E <= 0.))

    majoranas2piMu = make_majorana(
        name='Majo2mupi_{hash}',
        pions=down_pions,
        leptons=down_muons,
        descriptor='[KS0 -> mu- pi+]cc',
    )

    b2EN = make_bhadron_majorana(
        name='B2MajoE_{hash}',
        majoranas=majoranas2piMu,
        leptons=electrons_with_brem,
        pvs=pvs,
        descriptor='[B+ -> KS0 e+]cc')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [majoranas2piMu, b2EN],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BpToMajoMu_MajoToEPi_LL_Loose_line(
        name="Hlt2RD_BpToMajoMu_MajoToEPi_LL_Loose", prescale=1,
        presistreco=False):
    pvs = make_pvs

    muons = make_majorana_lepton(
        leptons=make_long_muons,
        name='majo_long_muons_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=F.ISMUON)

    electrons_with_brem = make_majorana_lepton(
        leptons=make_long_electrons_with_brem,
        name='majo_long_electrons_with_brem_{hash}',
        pvs=pvs,
        pid=(F.PID_E > 0.))

    pions = make_majorana_lepton(
        leptons=make_long_pions,
        name='majo_long_pions_{hash}',
        pvs=pvs,
        pid=(F.PID_E <= 0))

    majoranas2piE = make_majorana(
        name='Majo2epi_{hash}',
        pions=pions,
        leptons=electrons_with_brem,
        descriptor='[KS0 -> e- pi+]cc')

    b2MuN = make_bhadron_majorana(
        name='B2MajoMu_{hash}',
        majoranas=majoranas2piE,
        leptons=muons,
        pvs=pvs,
        descriptor='[B+ -> KS0 mu+]cc')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [majoranas2piE, b2MuN],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BpToMajoE_MajoToMuPi_LL_Loose_line(
        name="Hlt2RD_BpToMajoE_MajoToMuPi_LL_Loose", prescale=1,
        presistreco=False):
    pvs = make_pvs

    muons = make_majorana_lepton(
        leptons=make_long_muons,
        name='majo_long_muons_{hash}',
        pvs=pvs,
        pid=F.ISMUON)

    electrons_with_brem = make_majorana_lepton(
        leptons=make_long_electrons_with_brem,
        name='majo_long_electrons_with_brem_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=(F.PID_E > 0.))

    pions = make_majorana_lepton(
        leptons=make_long_pions,
        name='majo_long_pions_{hash}',
        pvs=pvs,
        pid=(F.PID_E <= 0))

    majoranas2piMu = make_majorana(
        name='Majo2mupi_{hash}',
        pions=pions,
        leptons=muons,
        descriptor='[KS0 -> mu- pi+]cc')

    b2EN = make_bhadron_majorana(
        name='B2MajoE_{hash}',
        majoranas=majoranas2piMu,
        leptons=electrons_with_brem,
        pvs=pvs,
        descriptor='[B+ -> KS0 e+]cc')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [majoranas2piMu, b2EN],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BpToMajoMu_MajoToEPi_DD_Loose_line(
        name="Hlt2RD_BpToMajoMu_MajoToEPi_DD_Loose", prescale=1,
        presistreco=False):
    pvs = make_pvs

    muons = make_majorana_lepton(
        leptons=make_long_muons,
        name='majo_long_muons_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=F.ISMUON)

    down_electrons_without_brem = make_majorana_lepton(
        leptons=make_down_electrons_no_brem,
        name='majo_down_electrons_without_brem_{hash}',
        pvs=pvs,
        pt_min=600 * MeV,
        mipchi2dvprimary_min=None,
        pid=(F.PID_E > 0.))

    down_pions = make_majorana_lepton(
        leptons=make_down_pions,
        name='majo_down_pions_{hash}',
        pvs=pvs,
        pt_min=600 * MeV,
        mipchi2dvprimary_min=None,
        pid=(F.PID_E <= 0.))

    majoranas2piE = make_majorana(
        name='Majo2epi_{hash}',
        pions=down_pions,
        leptons=down_electrons_without_brem,
        descriptor='[KS0 -> e- pi+]cc')

    b2MuN = make_bhadron_majorana(
        name='B2MajoMu_{hash}',
        majoranas=majoranas2piE,
        leptons=muons,
        pvs=pvs,
        descriptor='[B+ -> KS0 mu+]cc')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [majoranas2piE, b2MuN],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BpToMajoE_MajoToMuPi_DD_Loose_line(
        name="Hlt2RD_BpToMajoE_MajoToMuPi_DD_Loose", prescale=1,
        presistreco=False):
    pvs = make_pvs

    electrons_with_brem = make_majorana_lepton(
        leptons=make_long_electrons_with_brem,
        name='majo_long_electrons_with_brem_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=(F.PID_E > 0.))

    down_muons = make_majorana_lepton(
        leptons=make_down_muons,
        name='majo_down_muons_{hash}',
        pvs=pvs,
        mipchi2dvprimary_min=None,
        pt_min=600 * MeV,
        pid=((F.ISMUON) & (F.PID_MU > 0.)))

    down_pions = make_majorana_lepton(
        leptons=make_down_pions,
        name='majo_down_pions_{hash}',
        pvs=pvs,
        pt_min=600 * MeV,
        mipchi2dvprimary_min=None,
        pid=(F.PID_E <= 0.))

    majoranas2piMu = make_majorana(
        name='Majo2mupi_{hash}',
        pions=down_pions,
        leptons=down_muons,
        descriptor='[KS0 -> mu- pi+]cc',
    )

    b2EN = make_bhadron_majorana(
        name='B2MajoE_{hash}',
        majoranas=majoranas2piMu,
        leptons=electrons_with_brem,
        pvs=pvs,
        descriptor='[B+ -> KS0 e+]cc')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [majoranas2piMu, b2EN],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BuToKs0Pi_Ks0ToPiPi_LL_line(name="Hlt2RD_BuToKs0Pi_Ks0ToPiPi_LL",
                                prescale=1,
                                presistreco=False):
    pvs = make_pvs

    b_pions = make_majorana_lepton(
        leptons=make_long_pions,
        name='majo_long_pions_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=(F.PID_E <= 0.))
    #Here change to make_pvs_v2 when rad_incl_builders are in thor
    Ks2pipi = rad_incl_builder.filter_neutral_hadrons(make_KsLL_fromSV(),
                                                      make_pvs())

    b2Ks0Pi = make_bhadron_majorana(
        name='B2KsPi_LL_{hash}',
        majoranas=Ks2pipi,
        leptons=b_pions,
        pvs=pvs,
        am_min=5000.,
        am_max=5600.,
        m_min=5100.,
        m_max=5500.,
        descriptor='[B+ -> KS0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Ks2pipi, b2Ks0Pi],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BuToKs0Pi_Ks0ToPiPi_DD_line(name="Hlt2RD_BuToKs0Pi_Ks0ToPiPi_DD",
                                prescale=1,
                                presistreco=False):
    pvs = make_pvs

    long_pions = make_majorana_lepton(
        leptons=make_long_pions,
        name='majo_long_pions_{hash}',
        pvs=pvs,
        pt_min=700 * MeV,
        pid=(F.PID_E <= 0.))
    Ks2pipi = rad_incl_builder.filter_neutral_hadrons(make_KsDD_fromSV(),
                                                      make_pvs())

    b2Ks0Pi = make_bhadron_majorana(
        name='B2KsPi_DD_{hash}',
        majoranas=Ks2pipi,
        leptons=long_pions,
        pvs=pvs,
        am_min=5000.,
        am_max=5600.,
        m_min=5100.,
        m_max=5500.,
        descriptor='[B+ -> KS0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Ks2pipi, b2Ks0Pi],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)
