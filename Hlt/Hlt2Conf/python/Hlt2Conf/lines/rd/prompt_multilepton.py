###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of prompt multilepton lines, which live at the edge of RD and B&Q groups.
- {head} -> mu+ mu- mu+ mu-
- {head} -> mu+ mu- e+ mu-
- {head} -> mu+ mu- e+ e-
- {head} -> mu+ e- mu+ e-
- {head} -> mu+ e- e+ e-
- {head} -> e+ e- e+ e-

where the {head} can be the a charmonium, bottomonium or a light meson.
In addition, the Jpsi decays are attempted to be tagged with the psi(2S)->Jpsi pi+pi-.

TODO: perhaps add more same-sign combinations, and Upsilon(2S) tagging for the Upsilon(1S).
TODO: perhaps, basic decays to 6 leptons.

author: Vitalii Lisovskyi
date: 21.01.2022

WHAT TO DO IF THE RATE IS TOO HIGH?
> go to the builders, and tighten the lepton PID cuts.
> tighten the parent PT requirement.
> tighten the vertex chi2 to 4 everywhere.
"""
from GaudiKernel.SystemOfUnits import MeV, GeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
import Functors as F
from .builders.prompt_multilepton_builders import make_prompt_four_muon, make_prompt_3mue, make_prompt_2mu2e, make_prompt_2muSS2eSS, make_prompt_mue2e, make_prompt_2e2e, make_psi2S_tag
from .builders.rd_prefilters import rd_prefilter_noPV, rd_prefilter, _VRD_MONITORING_VARIABLES, _VRD_NOPV_MONITORING_VARIABLES

all_lines = {}

### Four-muon lines ###


@register_line_builder(all_lines)
def Hlt2RD_JpsiTo4Mu(name="Hlt2RD_JpsiTo4Mu", prescale=1, persistreco=False):
    """
    Register the Jpsi->mu+mu+mu-mu- line.
    """

    fourlepton = make_prompt_four_muon(
        name + "_builder",
        comb_m_min=2500 * MeV,
        comb_m_max=4100 * MeV,
        m_min=2600 * MeV,
        m_max=4000 * MeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_UpsilonTo4Mu(name="Hlt2RD_UpsilonTo4Mu",
                        prescale=1,
                        persistreco=False):
    """
    Register the Upsilon->mu+mu+mu-mu- line.
    """

    fourlepton = make_prompt_four_muon(
        name + "_builder",
        parent_id='Upsilon(1S)',
        comb_m_min=8900 * MeV,
        comb_m_max=10600 * MeV,
        m_min=9000 * MeV,
        m_max=10500 * MeV,
        pid_best=5.,
        pt_best=0.8 * GeV,
        pt_min=0.9 * GeV,
        docachi2_max=10.,
        vchi2pdof_max=8.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_LowMassTo4Mu(name="Hlt2RD_LowMassTo4Mu",
                        prescale=1,
                        persistreco=False):
    """
    Register the phi->mu+mu+mu-mu- line.
    """

    fourlepton = make_prompt_four_muon(
        name + "_builder",
        parent_id='phi(1020)',
        comb_m_min=350 * MeV,
        comb_m_max=1300 * MeV,
        m_min=400 * MeV,
        m_max=1200 * MeV,
        pid_cut_mu=F.require_all(F.ISMUON, F.PID_MU > 1.),
        pid_best=5.,
        pt_best=0.45 * GeV,
        p_best=4.5 * GeV,
        comb_pt_min=0.45 * GeV,
        pt_min=0.65 * GeV,
        docachi2_max=9.,
        vchi2pdof_max=7.,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


### 3mue lines ###


@register_line_builder(all_lines)
def Hlt2RD_JpsiTo3MuE(name="Hlt2RD_JpsiTo3MuE", prescale=1, persistreco=False):
    """
    Register the Jpsi->mu+mu+mu-e- line.
    """

    fourlepton = make_prompt_3mue(
        name + "_builder",
        comb_m_min=2400 * MeV,
        comb_m_max=4100 * MeV,
        m_min=2500 * MeV,
        m_max=4000 * MeV,
        pt_best=0.8 * GeV,
        docachi2_max=9.,
        vchi2pdof_max=5.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_UpsilonTo3MuE(name="Hlt2RD_UpsilonTo3MuE",
                         prescale=1,
                         persistreco=False):
    """
    Register the Upsilon->mu+mu+mu-e- line.
    """

    fourlepton = make_prompt_3mue(
        name + "_builder",
        parent_id='Upsilon(1S)',
        comb_m_min=8200 * MeV,
        comb_m_max=12600 * MeV,
        m_min=8500 * MeV,
        m_max=12000 * MeV,
        pid_best=5.5,
        pt_best=0.9 * GeV,
        comb_pt_min=2. * GeV,
        pt_min=2.9 * GeV,
        docachi2_max=7.,
        vchi2pdof_max=4.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_LowMassTo3MuE(name="Hlt2RD_LowMassTo3MuE",
                         prescale=1,
                         persistreco=False):
    """
    Register the phi->mu+mu+mu-e- line.
    """

    fourlepton = make_prompt_3mue(
        name + "_builder",
        parent_id='phi(1020)',
        comb_m_min=0 * MeV,
        comb_m_max=1500 * MeV,
        m_min=400 * MeV,
        m_max=1400 * MeV,
        comb_pt_min=0.8 * GeV,
        pt_min=1. * GeV,
        pid_best=5.5,
        pt_best=0.5 * GeV)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


### 2mu2e lines ###


@register_line_builder(all_lines)
def Hlt2RD_JpsiTo2Mu2E(name="Hlt2RD_JpsiTo2Mu2E",
                       prescale=1,
                       persistreco=False):
    """
    Register the Jpsi->mu+mu-e+e- line.
    """

    fourlepton = make_prompt_2mu2e(
        name + "_builder",
        comb_m_min=2400 * MeV,
        comb_m_max=4100 * MeV,
        m_min=2500 * MeV,
        m_max=4000 * MeV,
        pt_best=0.8 * GeV,
        p_best=7. * GeV,
        pid_best=6.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_UpsilonTo2Mu2E(name="Hlt2RD_UpsilonTo2Mu2E",
                          prescale=1,
                          persistreco=False):
    """
    Register the Upsilon->mu+mu-e+e- line.
    """

    fourlepton = make_prompt_2mu2e(
        name + "_builder",
        parent_id='Upsilon(1S)',
        comb_m_min=8000 * MeV,
        comb_m_max=12600 * MeV,
        m_min=8200 * MeV,
        m_max=12500 * MeV,
        pt_best=0.7 * GeV,
        p_best=5. * GeV,
        vchi2pdof_max=5.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_LowMassTo2Mu2E(name="Hlt2RD_LowMassTo2Mu2E",
                          prescale=1,
                          persistreco=False):
    """
    Register the phi->mu+mu-e+e- line.
    """

    fourlepton = make_prompt_2mu2e(
        name + "_builder",
        parent_id='phi(1020)',
        comb_m_min=0 * MeV,
        comb_m_max=1500 * MeV,
        m_min=200 * MeV,
        m_max=1400 * MeV,
        pt_best=0.35 * GeV,
        p_best=4. * GeV,
        comb_pt_min=0.4 * GeV,
        pt_min=0.75 * GeV)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


### 2mu2e same-sign lines ###


@register_line_builder(all_lines)
def Hlt2RD_JpsiTo2Mup2Em(name="Hlt2RD_JpsiTo2Mup2Em",
                         prescale=1,
                         persistreco=False):
    """
    Register the Jpsi->mu+mu+e-e- line.
    """

    fourlepton = make_prompt_2muSS2eSS(
        name + "_builder",
        comb_m_min=2400 * MeV,
        comb_m_max=4100 * MeV,
        m_min=2500 * MeV,
        m_max=4000 * MeV,
        pt_best=0.65 * GeV,
        p_best=7. * GeV,
        pid_best=6.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_UpsilonTo2Mup2Em(name="Hlt2RD_UpsilonTo2Mup2Em",
                            prescale=1,
                            persistreco=False):
    """
    Register the Upsilon->mu+mu+e-e- line.
    """

    fourlepton = make_prompt_2muSS2eSS(
        name + "_builder",
        parent_id='Upsilon(1S)',
        comb_m_min=8000 * MeV,
        comb_m_max=12600 * MeV,
        m_min=8500 * MeV,
        m_max=12500 * MeV,
        pt_best=0.75 * GeV,
        p_best=5. * GeV,
        pid_best=5.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_LowMassTo2Mup2Em(name="Hlt2RD_LowMassTo2Mup2Em",
                            prescale=1,
                            persistreco=False):
    """
    Register the phi->mu+mu+e-e- line.
    """

    fourlepton = make_prompt_2muSS2eSS(
        name + "_builder",
        parent_id='phi(1020)',
        comb_m_min=0 * MeV,
        comb_m_max=1500 * MeV,
        m_min=200 * MeV,
        m_max=1400 * MeV,
        pt_best=0.35 * GeV,
        p_best=4. * GeV,
        comb_pt_min=0.4 * GeV,
        pt_min=0.8 * GeV,
        pid_best=6.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


### mu3e lines ###


@register_line_builder(all_lines)
def Hlt2RD_JpsiToMu3E(name="Hlt2RD_JpsiToMu3E", prescale=1, persistreco=False):
    """
    Register the Jpsi->mu+e+e-e- line.
    """

    fourlepton = make_prompt_mue2e(
        name + "_builder",
        comb_m_min=2400 * MeV,
        comb_m_max=4100 * MeV,
        m_min=2500 * MeV,
        m_max=3900 * MeV,
        pt_best=0.75 * GeV,
        p_best=7. * GeV,
        pt_min=2.9 * GeV,
        max_pt_min=0.9 * GeV,
        vchi2pdof_max=5.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_UpsilonToMu3E(name="Hlt2RD_UpsilonToMu3E",
                         prescale=1,
                         persistreco=False):
    """
    Register the Upsilon->mu+e+e-e- line.
    """

    fourlepton = make_prompt_mue2e(
        name + "_builder",
        parent_id='Upsilon(1S)',
        comb_m_min=8000 * MeV,
        comb_m_max=12900 * MeV,
        m_min=8100 * MeV,
        m_max=12800 * MeV,
        pt_best=0.75 * GeV,
        p_best=5. * GeV,
        pt_min=2.9 * GeV,
        max_pt_min=0.9 * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_LowMassToMu3E(name="Hlt2RD_LowMassToMu3E",
                         prescale=1,
                         persistreco=False):
    """
    Register the phi->mu+e+e-e- line.
    """

    fourlepton = make_prompt_mue2e(
        name + "_builder",
        parent_id='phi(1020)',
        comb_m_min=0 * MeV,
        comb_m_max=1500 * MeV,
        m_min=200 * MeV,
        m_max=1400 * MeV,
        pt_best=0.5 * GeV,
        p_best=6. * GeV,
        comb_pt_min=0.7 * GeV,
        pt_min=0.8 * GeV,
        min_dielectron_mass=20 *
        MeV  # require that each dielectron pair is not from photon conversions
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


### 4e lines


@register_line_builder(all_lines)
def Hlt2RD_JpsiTo4E(name="Hlt2RD_JpsiTo4E", prescale=1, persistreco=False):
    """
    Register the Jpsi->e+e+e-e- line.
    """

    fourlepton = make_prompt_2e2e(
        name + "_builder",
        comb_m_min=2400 * MeV,
        comb_m_max=4100 * MeV,
        m_min=2500 * MeV,
        m_max=4000 * MeV,
        pid_best=6.5,
        comb_pt_min=1.9 * GeV,  #2. * GeV,
        pt_min=2.8 * GeV,  #3.5 * GeV,
        max_pt_min=0.85 * GeV,
        docachi2_max=9.,
        vchi2pdof_max=4.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_UpsilonTo4E(name="Hlt2RD_UpsilonTo4E",
                       prescale=1,
                       persistreco=False):
    """
    Register the Upsilon->e+e+e-e- line.
    """

    fourlepton = make_prompt_2e2e(
        name + "_builder",
        parent_id='Upsilon(1S)',
        comb_m_min=7500 * MeV,
        comb_m_max=12900 * MeV,
        m_min=8000 * MeV,
        m_max=12800 * MeV,
        pid_best=5.,
        bpvipchi2_max=25.,
        docachi2_max=9.,
        vchi2pdof_max=5.,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_LowMassTo4E(name="Hlt2RD_LowMassTo4E",
                       prescale=1,
                       persistreco=False):
    """
    Register the phi->e+e+e-e- line.
    """

    fourlepton = make_prompt_2e2e(
        name + "_builder",
        parent_id='phi(1020)',
        comb_m_min=10 * MeV,  # to reduce clones / photon conversions
        comb_m_max=1600 * MeV,
        m_min=100 * MeV,  # to reduce clones / photon conversions
        m_max=1500 * MeV,
        comb_pt_min=1.0 * GeV,
        pt_min=1.2 * GeV,
        pid_best=6.5,
        docachi2_max=6.,
        vchi2pdof_max=4.,
        bpvipchi2_max=16.,
        min_dielectron_mass=20 *
        MeV  # require that each dielectron pair is not from photon conversions
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [fourlepton],  #here we do require a PV
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


### builders with the psi(2S) tag ###


@register_line_builder(all_lines)
def Hlt2RD_Psi2SToJpsiPiPi_JpsiTo4Mu(name="Hlt2RD_Psi2SToJpsiPiPi_JpsiTo4Mu",
                                     prescale=1,
                                     persistreco=False):
    """
    Register the psi(2S) -> pi+pi- Jpsi(->mu+mu+mu-mu-) line.
    """

    jpsis = make_prompt_four_muon(
        name=name + "_jpsi_builder",
        pid_best=4,
        pt_best=0.4 * GeV,
        p_best=4. * GeV,
        m_min=2800 * MeV,
        m_max=3850 * MeV,
    )
    fourlepton = make_psi2S_tag(
        name=name + "_builder",
        jpsis=jpsis,
        electronchannel=False,
        pid_best=4,
        pt_best=0.4 * GeV,
        p_best=4. * GeV)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [jpsis, fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_Psi2SToJpsiPiPi_JpsiTo2Mu2E(
        name="Hlt2RD_Psi2SToJpsiPiPi_JpsiTo2Mu2E", prescale=1,
        persistreco=False):
    """
    Register the psi(2S) -> pi+pi- Jpsi(->mu+mu-e+e-) line.
    """

    jpsis = make_prompt_2mu2e(
        name=name + "_jpsis_builder",
        pid_best=4,
        pt_best=0.4 * GeV,
        p_best=4. * GeV,
        m_min=2500 * MeV,
        m_max=4000 * MeV)  #(name=name, pid_best=3, pt_best=0.4 * GeV,
    #p_best=4. * GeV)
    fourlepton = make_psi2S_tag(
        name=name + "_builder",
        jpsis=jpsis,
        pid_best=4.,
        pt_best=0.4 * GeV,
        p_best=4. * GeV)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [jpsis, fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_Psi2SToJpsiPiPi_JpsiTo3MuE(name="Hlt2RD_Psi2SToJpsiPiPi_JpsiTo3MuE",
                                      prescale=1,
                                      persistreco=False):
    """
    Register the psi(2S) -> pi+pi- Jpsi(->mu+mu-mu+e-) line.
    """

    jpsis = make_prompt_3mue(
        name=name + "_jpsis_builder",
        pid_best=4,
        pt_best=0.4 * GeV,
        p_best=4. * GeV,
        m_min=2500 * MeV,
        m_max=4000 * MeV)  #(name=name, pid_best=3., pt_best=0.4 * GeV,
    #p_best=4. * GeV)
    fourlepton = make_psi2S_tag(name=name + "_builder", jpsis=jpsis)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [jpsis, fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_Psi2SToJpsiPiPi_JpsiTo2Mup2Em(
        name="Hlt2RD_Psi2SToJpsiPiPi_JpsiTo2Mup2Em", prescale=1,
        persistreco=False):
    """
    Register the psi(2S) -> pi+pi- Jpsi(->mu+mu+e-e-) line.
    """

    jpsis = make_prompt_2muSS2eSS(
        name=name + "_jpsis_builder",
        pid_best=4,
        pt_best=0.4 * GeV,
        p_best=4. * GeV,
        m_min=2500 * MeV,
        m_max=4000 * MeV)
    fourlepton = make_psi2S_tag(name=name + "_builder", jpsis=jpsis)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [jpsis, fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_Psi2SToJpsiPiPi_JpsiToMu3E(name="Hlt2RD_Psi2SToJpsiPiPi_JpsiToMu3E",
                                      prescale=1,
                                      persistreco=False):
    """
    Register the psi(2S) -> pi+pi- Jpsi(->mu+e-e+e-) line.
    """
    jpsis = make_prompt_mue2e(
        name=name + "_jpsis_builder",
        pid_best=4,
        pt_best=0.4 * GeV,
        p_best=4. * GeV,
        m_min=2500 * MeV,
        m_max=4000 * MeV)
    fourlepton = make_psi2S_tag(name=name + "_builder", jpsis=jpsis)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [jpsis, fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2RD_Psi2SToJpsiPiPi_JpsiTo4E(name="Hlt2RD_Psi2SToJpsiPiPi_JpsiTo4E",
                                    prescale=0.5,
                                    persistreco=False):
    """
    Register the psi(2S) -> pi+pi- Jpsi(->e+e-e+e-) line.
    """
    jpsis = make_prompt_2e2e(
        name=name + "_jpsi_builder",
        pid_best=4,
        pt_best=0.4 * GeV,
        p_best=4. * GeV,
        m_min=2500 * MeV,
        m_max=4000 * MeV)
    fourlepton = make_psi2S_tag(name=name + "_builder", jpsis=jpsis)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter_noPV() + [jpsis, fourlepton],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_VRD_NOPV_MONITORING_VARIABLES,
    )
