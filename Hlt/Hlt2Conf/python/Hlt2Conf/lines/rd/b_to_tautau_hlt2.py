###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Author: Ya Zhao, Tommaso Fulghesu  
Contact: ya.zhao@cern.ch, tommaso.fulghesu@cern.ch
Date: 01/04/2022
'''
#######################################################################################
####                                                                               ####
#### Hlt2 lines for selecting OS and SS events of B0 and B_s0 decaying into a pair ####
#### of taus, each one decays hadronically into three pions                        ####
####                                                                               ####
#######################################################################################

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from Hlt2Conf.lines.rd.builders import b_to_xtaul_rd_builder as builder
from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES
from GaudiKernel.SystemOfUnits import GeV

hlt2_lines = {}


@register_line_builder(hlt2_lines)
@configurable
def Hlt2RD_BdToTauTau_OS_Line(name="Hlt2RD_BdToTauTau_TauTo3Pi_OS",
                              prescale=1,
                              persistreco=True):
    """
    Register B -> tautau (B0 and B_s0) line
    """
    ditaus = builder.filter_BToTauTau(
        ditaus=builder.make_dilepton_from_tauls(
            parent_id="B_s0", daughter_id="tau+", tau_pt_min=1 * GeV))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ditaus],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(hlt2_lines)
@configurable
def Hlt2RD_BdToTauTau_SS_Line(name="Hlt2RD_BdToTauTau_TauTo3Pi_SS",
                              prescale=1,
                              persistreco=True):
    """
    Register B -> tautau (B0 and B_s0) line
    """
    ditaus = builder.filter_BToTauTau(
        ditaus=builder.make_dilepton_from_tauls(
            parent_id="B_s0", daughter_id="tau-", tau_pt_min=1 * GeV))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ditaus],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)
