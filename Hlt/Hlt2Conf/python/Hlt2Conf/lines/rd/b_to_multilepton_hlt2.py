###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of Nmu(h) HLT2 lines for the RD working group.
- Single-vertex Detached -> 4mu with persist-reco
- Single-vertex Detached -> 4e with persist-reco
- Single-vertex Detached -> 2e2mu with persist-reco
- B -> X(mumu) X(mumu) with long (and in the future downstream) dimuons
- B -> X(ee) X(ee) with long dielectrons
- B -> X(mumu) X(mumu) K with long (and in the future downstream) dimuons
- B -> X(ee) X(ee) K with long dielectrons
- B -> X(mumu) X(mumu) X(mumu) with long (and in the future downstream) dimuons
- B -> X(ee) X(ee) X(ee) with long dielectrons
- B -> X(mumu) X(mumu) X(mumu) K with long (and in the future downstream) dimuons
- B -> X(ee) X(ee) X(ee) K with long dielectrons

author: Titus Mombächer
date: 03.08.2022
"""

import Functors as F

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter

from .builders.rdbuilder_thor import (
    make_rd_detached_muons, make_rd_detached_kaons, make_rd_detached_dimuon,
    make_rd_detached_dielectron, make_rd_detached_electrons)

from .builders.Nmu_builders import (make_b_4l, make_b_2mu2e,
                                    make_generic_4lepton_LLP, make_B24lK_LLP,
                                    make_B26l_LLP, make_B26lK_LLP)

from Moore.config import HltLine, register_line_builder

all_lines = {}

## 4 lepton combinations


@register_line_builder(all_lines)
def Displaced4Mu_incl_line(name="Hlt2RD_Displaced4Mu_Incl",
                           prescale=1,
                           persistreco=True):
    """Displaced->4mu inclusive selection"""
    lepton = make_rd_detached_muons(pid=(F.PID_MU > 1), mipchi2dvprimary_min=9)
    b = make_b_4l(
        lepton, descriptor='B0 -> mu+ mu+ mu- mu-', name="make_rd_b24mu")

    return HltLine(
        name=name,
        algs=rd_prefilter() + [lepton, b],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Displaced4E_incl_line(name="Hlt2RD_Displaced4E_Incl",
                          prescale=1,
                          persistreco=True):
    """Displaced->4e inclusive selection, bremsstrahlung check needs to be done offline"""
    lepton = make_rd_detached_electrons(
        pid=(F.PID_E > 2), mipchi2dvprimary_min=12)
    b = make_b_4l(lepton, descriptor='B0 -> e+ e+ e- e-', name="make_rd_b24e")

    return HltLine(
        name=name,
        algs=rd_prefilter() + [lepton, b],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
def DisplacedMuMuEE_incl_line(name="Hlt2RD_DisplacedMuMuEE_Incl",
                              prescale=1,
                              persistreco=True):
    """Displaced->mumuee inclusive selection"""
    dilepton1 = make_rd_detached_dimuon(
        pidmu_muon_min=0, parent_id="J/psi(1S)")
    dilepton2 = make_rd_detached_dielectron(pid_e_min=2, parent_id="phi(1020)")
    b = make_b_2mu2e(
        dilepton1, dilepton2, descriptor='B0 -> J/psi(1S) phi(1020)')

    return HltLine(
        name=name,
        algs=rd_prefilter() + [dilepton1, dilepton2, b],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
def BTo2LLP_LLPToMuMu_LL_line(name="Hlt2RD_BTo2LLP_LLPToMuMu_LL",
                              prescale=1,
                              persistreco=False):
    """Bs/Bd -> X (mumu) X (mumu) with long muon combinations"""
    dimuon = make_rd_detached_dimuon(parent_id="KS0", pidmu_muon_min=1)
    b = make_generic_4lepton_LLP(
        dimuon,
        dimuon,
        descriptor='B0 -> KS0 KS0',
        name="make_rd_generic_4mu_LL")

    return HltLine(
        name=name, algs=rd_prefilter() + [dimuon, b], prescale=prescale)


@register_line_builder(all_lines)
def BTo2LLP_LLPToEE_LL_line(name="Hlt2RD_BTo2LLP_LLPToEE_LL",
                            prescale=1,
                            persistreco=False):
    """Bs/Bd -> X (ee) X (ee) with long muon combinations"""
    dielectron = make_rd_detached_dielectron(parent_id="KS0", pid_e_min=1)
    b = make_generic_4lepton_LLP(
        dielectron,
        dielectron,
        descriptor='B0 -> KS0 KS0',
        name="make_rd_generic_4e_LL")

    return HltLine(
        name=name, algs=rd_prefilter() + [dielectron, b], prescale=prescale)


@register_line_builder(all_lines)
def BuTo2LLPKp_LLPToMuMu_LL_line(name="Hlt2RD_BuTo2LLPKp_LLPToMuMu_LL",
                                 prescale=1,
                                 persistreco=False):
    """B+ -> X (mumu) X (mumu) K+ with long muon combinations"""
    dimuon = make_rd_detached_dimuon(parent_id="KS0", pidmu_muon_min=1)
    kaon = make_rd_detached_kaons(pid=(F.PID_K > 0.), mipchi2dvprimary_min=25.)
    b = make_B24lK_LLP(
        dimuon,
        dimuon,
        kaon,
        descriptor="[B+ -> KS0 KS0 K+]cc",
        name="make_rd_B24muK_LL")

    return HltLine(
        name=name, algs=rd_prefilter() + [dimuon, kaon, b], prescale=prescale)


@register_line_builder(all_lines)
def BuTo2LLPKp_LLPToEE_LL_line(name="Hlt2RD_BuTo2LLPKp_LLPToEE_LL",
                               prescale=1,
                               persistreco=False):
    """B+ -> X (ee) X (ee) K+ with long electron combinations"""
    dielectron = make_rd_detached_dielectron(parent_id="KS0", pid_e_min=1)
    kaon = make_rd_detached_kaons(pid=(F.PID_K > 0.), mipchi2dvprimary_min=25.)
    b = make_B24lK_LLP(
        dielectron,
        dielectron,
        kaon,
        descriptor="[B+ -> KS0 KS0 K+]cc",
        name="make_rd_B24eK_LL")

    return HltLine(
        name=name,
        algs=rd_prefilter() + [dielectron, kaon, b],
        prescale=prescale)


## 6 lepton combinations


@register_line_builder(all_lines)
def BTo3LLP_LLPToMuMu_LLL_line(name="Hlt2RD_BTo3LLP_LLPToMuMu_LLL",
                               prescale=1,
                               persistreco=False):
    """Bs/Bd -> X (mumu) X (mumu) X (mumu) with long muon combinations"""
    dimuons_long = make_rd_detached_dimuon(parent_id="KS0", pidmu_muon_min=0)
    b = make_B26l_LLP(
        dimuons_long,
        dimuons_long,
        dimuons_long,
        descriptor='B0 -> KS0 KS0 KS0',
        name="make_rd_B26mu_LLL")

    return HltLine(
        name=name, algs=rd_prefilter() + [dimuons_long, b], prescale=prescale)


@register_line_builder(all_lines)
def BTo3LLP_LLPToEE_LLL_line(name="Hlt2RD_BTo3LLP_LLPToEE_LLL",
                             prescale=1,
                             persistreco=False):
    """Bs/Bd -> X (ee) X (ee) X (ee) with long electron combinations"""
    dielectron = make_rd_detached_dielectron(parent_id="KS0", pid_e_min=None)
    b = make_B26l_LLP(
        dielectron,
        dielectron,
        dielectron,
        descriptor='B0 -> KS0 KS0 KS0',
        name="make_rd_B26e_LLL")

    return HltLine(
        name=name, algs=rd_prefilter() + [dielectron, b], prescale=prescale)


@register_line_builder(all_lines)
def BuTo3LLPKp_LLPToMuMu_LLL_line(name="Hlt2RD_BuTo3LLPKp_LLPToMuMu_LLL",
                                  prescale=1,
                                  persistreco=False):
    """B+ -> X (mumu) X (mumu) X (mumu) K+ with long muon combinations"""
    dimuons_long = make_rd_detached_dimuon(parent_id="KS0", pidmu_muon_min=0)
    kaon = make_rd_detached_kaons(pid=(F.PID_K > 0.), mipchi2dvprimary_min=25.)
    b = make_B26lK_LLP(
        dimuons_long,
        dimuons_long,
        dimuons_long,
        kaon,
        descriptor="[B+ -> KS0 KS0 KS0 K+]cc",
        name="make_rd_B26muK_LLL")

    return HltLine(
        name=name,
        algs=rd_prefilter() + [dimuons_long, kaon, b],
        prescale=prescale)


@register_line_builder(all_lines)
def BuTo3LLPKp_LLPToEE_LLL_line(name="Hlt2RD_BuTo3LLPKp_LLPToEE_LLL",
                                prescale=1,
                                persistreco=False):
    """B+ -> X (ee) X (ee) X (ee) K+ with long electron combinations"""
    dielectron = make_rd_detached_dielectron(parent_id="KS0", pid_e_min=None)
    kaon = make_rd_detached_kaons(pid=(F.PID_K > 0.), mipchi2dvprimary_min=25.)
    b = make_B26lK_LLP(
        dielectron,
        dielectron,
        dielectron,
        kaon,
        descriptor="[B+ -> KS0 KS0 KS0 K+]cc",
        name="make_rd_B26eK_LLL")

    return HltLine(
        name=name,
        algs=rd_prefilter() + [dielectron, kaon, b],
        prescale=prescale)
