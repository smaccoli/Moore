###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Builder for charmed hadrons in semileptonic decays.
   Based on B2OC upgrade branch.
"""
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from Hlt2Conf.algorithms_thor import ParticleCombiner
from RecoConf.reconstruction_objects import make_pvs
from PyConf import configurable
from . import base_builder

_PION_M = 139.57061 * MeV
_PIZ_M = 134.9768 * MeV
_GAMMA_M = 0.0 * MeV

############################################################
# D decay builders, nBody base and mode-specific builders. #
############################################################


# === Generic Hc builder ===
@configurable
def make_Hc_to_nbody(particles,
                     descriptor,
                     comb_m_min,
                     comb_m_max,
                     mother_m_min=None,
                     mother_m_max=None,
                     mother_pt_min=None,
                     name='HcToNBodyBuilder_{hash}',
                     make_pvs=make_pvs,
                     comb_pt_min=2000 * MeV,
                     comb_pt_any_min=None,
                     comb_pt_sum_min=None,
                     comb_doca_max=0.1 * mm,
                     comb_docachi2_max=None,
                     vchi2pdof_max=4,
                     bpvdira_min=0.999,
                     bpvfdchi2_min=25,
                     bpvvdz_min=None):
    """
    Base builder for a generic Hc hadron decaying to an arbitrary number of particles.

    Parameters
    ----------
    particles :
        Maker algorithm instances for input particles.
    descriptor : a single decay descriptor (+ cc), not a list!
        Decay descriptor to be reconstructed.
        Example: descriptor = "[D0 -> pi+ K-]cc"
    comb_m_min :
        Lower invariant mass limit for the particle combination.
    comb_m_max :
        Upper invariant mass limit for the particle combination.
    make_pvs : callable
        Primary vertex maker function.
    comb_pt_any_min :
        Minimum pt that at least one of the particles in the combination needs to have.
    comb_pt_sum_min :
        Minimum value of the direct sum of the pt of the particles in the combination (pt_1 + pt_2 + ...).
    The rest of configurables refer to standard variables for the selection.
    """

    pvs = make_pvs()
    combination_code = (in_range(comb_m_min, F.MASS, comb_m_max))
    if comb_pt_min is not None:
        combination_code = F.require_all(combination_code, F.PT > comb_pt_min)
    if comb_pt_any_min is not None:
        combination_code = F.require_all(combination_code,
                                         F.SUM(F.PT > comb_pt_any_min) > 0)
    if comb_pt_sum_min is not None:
        combination_code = F.require_all(combination_code,
                                         F.SUM(F.PT) > comb_pt_sum_min)
    if comb_doca_max is not None:
        combination_code = F.require_all(combination_code,
                                         F.MAXDOCACUT(comb_doca_max))
    if comb_docachi2_max is not None:
        combination_code = F.require_all(combination_code,
                                         F.MAXDOCACHI2CUT(comb_docachi2_max))

    mother_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    if (mother_m_min is not None) and (mother_m_max is not None):
        mother_code = F.require_all(
            mother_code, in_range(mother_m_min, F.MASS, mother_m_max))
    if mother_pt_min is not None:
        mother_code = F.require_all(mother_code, F.PT > mother_pt_min)
    if bpvvdz_min is not None:
        mother_code = F.require_all(mother_code, F.BPVVDZ(pvs) > bpvvdz_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=mother_code)


@configurable
def make_d0_tokpi(name='D0ToKPiBuilder_{hash}',
                  comb_m_min=1784.84 * MeV,
                  comb_m_max=1944.84 * MeV,
                  comb_docachi2_max=20.,
                  vchi2pdof_max=6,
                  bpvfdchi2_min=25,
                  bpvdira_min=0.99,
                  comb_pt_min=None,
                  comb_pt_any_min=None,
                  comb_pt_sum_min=None,
                  comb_doca_max=None,
                  daughter_pt_min=150.0 * MeV,
                  daughter_mipchi2_min=10.0,
                  kaon_pid=(F.PID_K > 3.),
                  pion_pid=(F.PID_K < 20.)):
    """
    Builder for the decay D0->K+pi-.
    """

    with base_builder.make_candidate.bind(
            pt_min=daughter_pt_min, mipchi2_min=daughter_mipchi2_min):
        particles = [
            base_builder.make_kaons(pid=kaon_pid),
            base_builder.make_pions(pid=pion_pid)
        ]

    return make_Hc_to_nbody(
        particles,
        "[D0 -> K- pi+]cc",
        comb_m_min,
        comb_m_max,
        name=name,
        comb_docachi2_max=comb_docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        bpvdira_min=bpvdira_min,
        comb_pt_min=comb_pt_min,
        comb_pt_any_min=comb_pt_any_min,
        comb_pt_sum_min=comb_pt_sum_min,
        comb_doca_max=comb_doca_max)


@configurable
def make_dplus_tokpipi(name='DpToKPiPiBuilder_{hash}',
                       comb_m_min=1830 * MeV,
                       comb_m_max=1910 * MeV,
                       comb_pt_any_min=800 * MeV,
                       comb_pt_sum_min=2500 * MeV,
                       daughter_p_min=5 * GeV,
                       daughter_pt_min=300 * MeV,
                       daughter_mipchi2_min=9.,
                       kaon_pid=(F.PID_K > 4.),
                       pion_pid=(F.PID_K < 2.)):
    """
    Builder for the decay D+->K-pi+pi+.
    """

    with base_builder.make_candidate.bind(
            p_min=daughter_p_min,
            pt_min=daughter_pt_min,
            mipchi2_min=daughter_mipchi2_min):
        particles = [
            base_builder.make_kaons(pid=kaon_pid),
            base_builder.make_pions(pid=pion_pid),
            base_builder.make_pions(pid=pion_pid)
        ]

    descriptor = "[D+ -> K- pi+ pi+]cc"

    return make_Hc_to_nbody(
        particles,
        descriptor,
        comb_m_min,
        comb_m_max,
        name=name,
        comb_pt_any_min=comb_pt_any_min,
        comb_pt_sum_min=comb_pt_sum_min)


@configurable
def make_d0_tok3pi(name='D0ToK3PiBuilder_{hash}',
                   comb_m_min=1784.84 * MeV,
                   comb_m_max=1944.84 * MeV,
                   comb_docachi2_max=20.,
                   vchi2pdof_max=6,
                   bpvfdchi2_min=25,
                   bpvdira_min=0.99,
                   comb_pt_min=None,
                   comb_pt_any_min=None,
                   comb_pt_sum_min=None,
                   comb_doca_max=0.2,
                   daughter_p_min=2 * GeV,
                   daughter_pt_min=400.0 * MeV,
                   daughter_mipchi2_min=9.0,
                   kaon_pid=(F.PID_K > 1.),
                   pion_pid=(F.PID_K < 8.)):
    """
    Builder for the decay D0->K-pi-pi+pi+.
    """

    with base_builder.make_candidate.bind(
            pt_min=daughter_pt_min, mipchi2_min=daughter_mipchi2_min):
        particles = [
            base_builder.make_kaons(pid=kaon_pid),
            base_builder.make_pions(pid=pion_pid),
            base_builder.make_pions(pid=pion_pid),
            base_builder.make_pions(pid=pion_pid)
        ]

    return make_Hc_to_nbody(
        particles,
        "[D0 -> K- pi- pi+ pi+]cc",
        comb_m_min,
        comb_m_max,
        name=name,
        comb_docachi2_max=comb_docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        bpvdira_min=bpvdira_min,
        comb_pt_min=comb_pt_min,
        comb_pt_any_min=comb_pt_any_min,
        comb_pt_sum_min=comb_pt_sum_min,
        comb_doca_max=comb_doca_max)


@configurable
def make_ds_tokkpi(name='DsToKKPiBuilder_{hash}',
                   comb_m_min=1920 * MeV,
                   comb_m_max=2010 * MeV,
                   comb_pt_any_min=800 * MeV,
                   comb_pt_sum_min=2500 * MeV,
                   daughter_p_min=5 * GeV,
                   daughter_pt_min=300 * MeV,
                   daughter_mipchi2_min=9.,
                   kaon_pid=(F.PID_K > 4.),
                   pion_pid=(F.PID_K < 2.)):
    """
    Builder for the decay Ds+->K+K-pi+.
    """

    with base_builder.make_candidate.bind(
            p_min=daughter_p_min,
            pt_min=daughter_pt_min,
            mipchi2_min=daughter_mipchi2_min):
        particles = [
            base_builder.make_kaons(pid=kaon_pid),
            base_builder.make_kaons(pid=kaon_pid),
            base_builder.make_pions(pid=pion_pid)
        ]

    descriptor = "[D_s+ -> K+ K- pi+]cc"

    return make_Hc_to_nbody(
        particles,
        descriptor,
        comb_m_min,
        comb_m_max,
        name=name,
        comb_pt_any_min=comb_pt_any_min,
        comb_pt_sum_min=comb_pt_sum_min)


@configurable
def make_jpsi_tomumu(name='JpsiToMuMuBuilder_{hash}',
                     comb_m_min=3047 * MeV,
                     comb_m_max=3147 * MeV,
                     comb_pt_any_min=1200 * MeV,
                     comb_pt_sum_min=3500 * MeV,
                     comb_docachi2_max=20.,
                     vchi2pdof_max=10,
                     bpvfdchi2_min=25,
                     bpvdira_min=0.999):
    """
    Builder for the decay J/psi -> mu+ mu-.
    """

    muons = base_builder.make_ismuon_long_muon()
    particles = [muons, muons]

    descriptor = "J/psi(1S) -> mu+ mu-"

    return make_Hc_to_nbody(
        particles,
        descriptor,
        comb_m_min,
        comb_m_max,
        name=name,
        comb_docachi2_max=comb_docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        bpvdira_min=bpvdira_min,
        comb_pt_min=None,
        comb_pt_any_min=comb_pt_any_min,
        comb_pt_sum_min=comb_pt_sum_min,
        comb_doca_max=None)


@configurable
def make_lambdac_topkpi(name='LcTopKPiBuilder_{hash}',
                        mother_m_min=None,
                        mother_m_max=None,
                        mother_pt_min=None,
                        comb_m_min=2256 * MeV,
                        comb_m_max=2317 * MeV,
                        comb_pt_min=2500 * MeV,
                        comb_pt_any_min=800 * MeV,
                        comb_pt_sum_min=1500 * MeV,
                        comb_doca_max=None,
                        comb_docachi2_max=None,
                        daughter_p_min=150 * MeV,
                        daughter_pt_min=150 * MeV,
                        daughter_mipchi2_min=10.,
                        kaon_pid=(F.PID_K > 5.),
                        pion_pid=(F.PID_K < 50.),
                        proton_pid=(F.PID_P > 5.)):
    """
     Builder for the decay Lambda_c+ -> p+ K- pi+.
    """

    with base_builder.make_candidate.bind(
            p_min=daughter_p_min,
            pt_min=daughter_pt_min,
            mipchi2_min=daughter_mipchi2_min):
        particles = [
            base_builder.make_protons(pid=proton_pid),
            base_builder.make_kaons(pid=kaon_pid),
            base_builder.make_pions(pid=pion_pid)
        ]

    descriptor = "[Lambda_c+ -> p+ K- pi+]cc"

    return make_Hc_to_nbody(
        particles=particles,
        descriptor=descriptor,
        mother_m_min=mother_m_min,
        mother_m_max=mother_m_max,
        mother_pt_min=mother_pt_min,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_docachi2_max=comb_docachi2_max,
        name=name,
        comb_pt_min=comb_pt_min,
        comb_pt_any_min=comb_pt_any_min,
        comb_pt_sum_min=comb_pt_sum_min,
        comb_doca_max=comb_doca_max)


@configurable
def make_xicplus_topkpi(
        name='XicplusTopKPiBuilder_{hash}',
        comb_m_min=2377.71 * MeV,  #xicplus - 90
        comb_m_max=2557.71 * MeV,  #xicplus + 90
        mother_m_min=2387.71 * MeV,  #xicplus - 80
        mother_m_max=2547.71 * MeV,  #xicplus + 80
        mother_pt_min=None,
        comb_pt_min=None,
        comb_pt_any_min=None,
        comb_pt_sum_min=None,
        comb_doca_max=None,
        comb_docachi2_max=20.,
        vchi2pdof_max=6.,
        bpvdira_min=0.99,
        bpvfdchi2_min=25,
        bpvvdz_min=None,
        proton_p_min=8000 * MeV,
        common_daug_p_min=2000 * MeV,
        common_daug_pt_min=250 * MeV,
        common_daug_mipchi2_min=4.,
        kaon_pid=(F.PID_K > -2.),
        pion_pid=(F.PID_K < 10.),
        proton_pid=F.require_all(F.PID_P > 0., (F.PID_P - F.PID_K) > 0.)):
    """
     Builder for the decay Xi_c+ -> p+ K- pi+.
    """
    #set the decay descriptor
    descriptor = "[Xi_c+ -> p+ K- pi+]cc"

    #make protons
    with base_builder.make_candidate.bind(
            p_min=proton_p_min,
            pt_min=common_daug_pt_min,
            mipchi2_min=common_daug_mipchi2_min):
        particles = [base_builder.make_protons(pid=proton_pid)]

    #make kaons and pions
    with base_builder.make_candidate.bind(
            p_min=common_daug_p_min,
            pt_min=common_daug_pt_min,
            mipchi2_min=common_daug_mipchi2_min):
        particles.append(base_builder.make_kaons(pid=kaon_pid))
        particles.append(base_builder.make_pions(pid=pion_pid))

    return make_Hc_to_nbody(
        particles,
        descriptor,
        comb_m_min,
        comb_m_max,
        mother_m_min=mother_m_min,
        mother_m_max=mother_m_max,
        mother_pt_min=mother_pt_min,
        name=name,
        comb_pt_min=comb_pt_min,
        comb_pt_any_min=comb_pt_any_min,
        comb_pt_sum_min=comb_pt_sum_min,
        comb_doca_max=comb_doca_max,
        comb_docachi2_max=comb_docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
        bpvvdz_min=bpvvdz_min)


@configurable
def make_xic0_topkkpi(
        name='Xic0TopKKPiBuilder_{hash}',
        comb_m_min=2380.44 * MeV,  #xic0 - 90
        comb_m_max=2560.44 * MeV,  #xic0 + 90
        mother_m_min=2390.44 * MeV,  #xicplus - 80
        mother_m_max=2550.44 * MeV,  #xicplus + 80
        mother_pt_min=None,
        comb_pt_min=None,
        comb_pt_any_min=None,
        comb_pt_sum_min=None,
        comb_doca_max=None,
        comb_docachi2_max=20.,
        vchi2pdof_max=6.,
        bpvdira_min=0.99,
        bpvfdchi2_min=25,
        bpvvdz_min=None,
        proton_p_min=8000 * MeV,
        common_daug_p_min=2000 * MeV,
        common_daug_pt_min=250 * MeV,
        common_daug_mipchi2_min=4.,
        kaon_pid=(F.PID_K > -2.),
        pion_pid=(F.PID_K < 10.),
        proton_pid=F.require_all(F.PID_P > 0., (F.PID_P - F.PID_K) > 0.)):
    """
     Builder for the decay Xi_c0 -> p+ K- K- pi+.
    """
    #set the decay descriptor
    descriptor = "[Xi_c0 -> p+ K- K- pi+]cc"

    #make protons
    with base_builder.make_candidate.bind(
            p_min=proton_p_min,
            pt_min=common_daug_pt_min,
            mipchi2_min=common_daug_mipchi2_min):
        particles = [base_builder.make_protons(pid=proton_pid)]

    #make kaons and pions
    with base_builder.make_candidate.bind(
            p_min=common_daug_p_min,
            pt_min=common_daug_pt_min,
            mipchi2_min=common_daug_mipchi2_min):
        particles.append(base_builder.make_kaons(pid=kaon_pid))
        particles.append(base_builder.make_kaons(pid=kaon_pid))
        particles.append(base_builder.make_pions(pid=pion_pid))

    return make_Hc_to_nbody(
        particles,
        descriptor,
        comb_m_min,
        comb_m_max,
        mother_m_min=mother_m_min,
        mother_m_max=mother_m_max,
        mother_pt_min=mother_pt_min,
        name=name,
        comb_pt_min=comb_pt_min,
        comb_pt_any_min=comb_pt_any_min,
        comb_pt_sum_min=comb_pt_sum_min,
        comb_doca_max=comb_doca_max,
        comb_docachi2_max=comb_docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
        bpvvdz_min=bpvvdz_min)


@configurable
def make_omegac_topkkpi(
        name='OmegacTopKKPiBuilder_{hash}',
        mother_m_min=None,
        mother_m_max=None,
        mother_vchi2pdof_max=6.,
        mother_bpvdira_min=0.99,
        mother_bpvfdchi2_min=25.,
        comb_m_min=2605.2 * MeV,
        comb_m_max=2785.2 * MeV,
        comb_pt_min=None,
        comb_doca_max=None,
        comb_docachi2_max=20.,
        daughter_p_min=2000. * MeV,  # (P>8000.0) & (P>2000.0) proton ?
        daughter_pt_min=250. * MeV,
        daughter_mipchi2_min=4.,
        kaon_pid=(F.PID_K > -2.),
        pion_pid=(F.PID_K < 10.),
        proton_pid=F.require_all(F.PID_P > 0., (F.PID_P - F.PID_K) > 0.)):
    """
     Builder for the decay Omega_c0 -> p+ K- K- pi+.
    """

    with base_builder.make_candidate.bind(
            p_min=daughter_p_min,
            pt_min=daughter_pt_min,
            mipchi2_min=daughter_mipchi2_min):
        particles = [
            base_builder.make_protons(pid=proton_pid),
            base_builder.make_kaons(pid=kaon_pid),
            base_builder.make_kaons(pid=kaon_pid),
            base_builder.make_pions(pid=pion_pid)
        ]

    descriptor = "[Omega_c0 -> p+ K- K- pi+]cc"

    return make_Hc_to_nbody(
        particles=particles,
        descriptor=descriptor,
        mother_m_min=mother_m_min,
        mother_m_max=mother_m_max,
        vchi2pdof_max=mother_vchi2pdof_max,
        bpvdira_min=mother_bpvdira_min,
        bpvfdchi2_min=mother_bpvfdchi2_min,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        name=name,
        comb_pt_min=comb_pt_min,
        comb_doca_max=comb_doca_max,
        comb_docachi2_max=comb_docachi2_max)
