###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Builder for base objects in semileptonic decays.
"""

# format imports according to conventions
from PyConf import configurable
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.algorithms import ParticleContainersMerger
from Hlt2Conf.standard_particles import (
    standard_protoparticle_filter, make_has_rich_long_pions,
    make_has_rich_long_kaons, make_has_rich_long_protons,
    make_ismuon_long_muon, make_long_muons, make_long_pions,
    make_long_electrons_with_brem)


@configurable
def make_candidate(name='SLB_MakeCandidate_{hash}',
                   make_particles=make_has_rich_long_pions,
                   make_pvs=make_pvs,
                   p_min=2. * GeV,
                   pt_min=250. * MeV,
                   mipchi2_min=4.,
                   pid=None,
                   **kwargs):
    """
    Base for track selection.
    """
    pvs = make_pvs()
    code = F.require_all(F.P > p_min, F.MINIPCHI2(pvs) > mipchi2_min)

    if pt_min is not None:
        code = F.require_all(code, F.PT > pt_min)
    if pid is not None:
        code = F.require_all(code, pid)

    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_pions(name='SLB_Pion_{hash}', pid=(F.PID_K < 20.)):
    """
    Generic pions.
    """
    return make_candidate(
        make_particles=make_has_rich_long_pions, pid=pid, name=name)


@configurable
def make_prong_pions(name='SLB_ProngPion_{hash}',
                     pt_min=350. * MeV,
                     mipchi2_min=10.):
    """
    Return pions from taus. Cuts from StdLooseDetachedTau3pi
    DOUBLE CHECK THIS
    """
    return make_candidate(
        # pid=pid,
        mipchi2_min=mipchi2_min,
        pt_min=pt_min,
        name=name)


@configurable
def make_kaons(name='SLB_Kaon_{hash}', pid=(F.PID_K > -2.)):
    """
    Generic kaons.
    """
    return make_candidate(
        make_particles=make_has_rich_long_kaons, pid=pid, name=name)


@configurable
def make_kaons_from_b(name='SLB_KaonsFromB_{hash}',
                      pid=(F.PID_K > 5.),
                      p_min=10000 * MeV,
                      pt_min=1000 * MeV,
                      mipchi2_min=36.):
    """
    Kaons directly produced in the decay of a b-hadron.
    """
    return make_candidate(
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        pid=pid,
        name=name)


def make_pions_from_b(name='SLB_PionsFromB_{hash}',
                      pid=F.require_all(F.PID_K < -5., F.PID_MU < -5.,
                                        F.PID_P < -5.),
                      p_min=10000 * MeV,
                      pt_min=800 * MeV,
                      mipchi2_min=36.):
    """
    Pions directly produced in the decay of a b-hadron.
    """
    return make_candidate(
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        pid=pid,
        name=name)


@configurable
def make_protons(name='SLB_Proton_{hash}',
                 pid=F.require_all(F.PID_P > 0., (F.PID_P - F.PID_K) > 0.)):
    """
    Generic protons.
    """
    return make_candidate(
        make_particles=make_has_rich_long_protons, pid=pid, name=name)


@configurable
def make_protons_from_b(name='SLB_ProtonsFromB_{hash}',
                        pid=F.require_all(F.PID_P > 10.,
                                          (F.PID_P - F.PID_K) > 10.),
                        p_min=15000 * MeV,
                        pt_min=1000 * MeV,
                        mipchi2_min=16.):
    """
    Protons directly produced in the decay of a b-hadron.
    """
    return make_candidate(
        make_particles=make_has_rich_long_protons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        pid=pid,
        name=name)


@configurable
def make_fake_protons_from_b(name='SLB_FakeProtonsFromB_{hash}',
                             pid=F.require_all(F.PID_P < 10.,
                                               (F.PID_P - F.PID_K) < 10.),
                             p_min=15000 * MeV,
                             pt_min=1000 * MeV,
                             mipchi2_min=16.):
    """
    Fake protons directly produced in the decay of a b-hadron. Inverted standard PID requirements.
    """
    return make_protons_from_b(
        pid=pid,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        name=name)


@configurable
def make_inmuon_long_muon():
    """
    Muon required to be in the geometric acceptance of the muon chambers but not required to have ismuon==1.
    """
    with standard_protoparticle_filter.bind(Code=F.PPHASMUONINFO):
        return make_long_muons()


@configurable
def make_muons_from_b(require_ismuon=1,
                      p_min=6. * GeV,
                      pt_min=1. * GeV,
                      mipchi2_min=9.,
                      pid=(F.PID_MU > 0.0)):
    """
    Muons directly produced in the decay of a b-hadron.
    """
    if require_ismuon: make_particles = make_ismuon_long_muon
    else: make_particles = make_inmuon_long_muon
    return make_candidate(
        make_particles=make_particles,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        pid=pid)


@configurable
def make_muons_for_btomunu(
        pid=(F.PID_MU > 3.0), p_min=6000 * MeV, pt_min=1500 * MeV,
        mipchi2_min=25):
    """
    Muons produced in the decay b -> Xu mu nu.
    """
    return make_muons_from_b(
        p_min=p_min, pt_min=pt_min, mipchi2_min=mipchi2_min, pid=pid)


@configurable
def make_fake_muons_for_btomunu(p_min=6000 * MeV,
                                pt_min=1500 * MeV,
                                mipchi2_min=25):
    """
    Same builder as above, but removing the PIDmu requirement.
    """
    return make_fake_muons_from_b_noPID(
        p_min=p_min, pt_min=pt_min, mipchi2_min=mipchi2_min)


@configurable
def make_incalo_long_electrons_with_brem():
    """
    Electron required to be in the geometric acceptance of the Ecal.
    """
    with standard_protoparticle_filter.bind(Code=F.PPHASECALINFO):
        return make_long_electrons_with_brem()


@configurable
def make_electrons_from_b(name='SLB_ElectronsFromB_{hash}',
                          p_min=6. * GeV,
                          pt_min=1. * GeV,
                          mipchi2_min=9.,
                          pid=(F.PID_E > 0.0),
                          require_in_calo=0):
    """
    Electrons directly produced in the decay of a b-hadron.
    """
    if require_in_calo: make_particles = make_incalo_long_electrons_with_brem
    else: make_particles = make_long_electrons_with_brem

    return make_candidate(
        make_particles=make_particles,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        pid=pid,
        name=name)


@configurable
def make_fake_electrons_from_b_reversedPID(p_min=6. * GeV,
                                           pt_min=1. * GeV,
                                           mipchi2_min=9.,
                                           pid=(F.PID_E < 0.0)):
    """
    'Fake electrons' directly produced in the decay of a b-hadron.
    Fake electrons definition: PIDe<0.0, in Ecal acceptance
    """
    return make_electrons_from_b(
        pid=pid,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        require_in_calo=1)


@configurable
def make_tauons_electronic_decay(maker=make_electrons_from_b,
                                 p_min=3. * GeV,
                                 pt_min=None,
                                 mipchi2_min=16.):
    """
    Tauons reconstructed through the electronic decay.
    """
    return maker(p_min=p_min, pt_min=pt_min, mipchi2_min=mipchi2_min)


@configurable
def make_fake_tauons_electronic_decay():
    """
    Fake tauons reconstructed through the electronic decay (with a fake electron).
    """
    return make_tauons_electronic_decay(
        maker=make_fake_electrons_from_b_reversedPID)


@configurable
def make_fake_muons_from_b_reversedPID(
        pid=(F.PID_MU < 0.0), p_min=6. * GeV, pt_min=1. * GeV, mipchi2_min=9.):
    """
    'Fake muons' directly produced in the decay of a b-hadron.
    Fake muon definition: ismuon==1 and PIDmu<0.0.
    """
    return make_muons_from_b(
        pid=pid, p_min=p_min, pt_min=pt_min, mipchi2_min=mipchi2_min)


@configurable
def make_fake_muons_from_b_notIsMuon(p_min=6. * GeV,
                                     pt_min=1. * GeV,
                                     mipchi2_min=9.):
    """
    'Fake muons' directly produced in the decay of a b-hadron.
    Fake muon definition: ismuon==0.
    """
    return make_muons_from_b(
        require_ismuon=0,
        #TODO: check if this worked
        pid=(~F.ISMUON),
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min)


@configurable
def make_fake_muons_from_b_noPID(pid=None,
                                 p_min=6. * GeV,
                                 pt_min=1. * GeV,
                                 mipchi2_min=9.):
    """
    'Fake muons' directly produced in the decay of a b-hadron.
    Fake muon definition: ismuon==1 and no requirement on PIDmu (inclusive).
    """
    return make_muons_from_b(
        pid=pid, p_min=p_min, pt_min=pt_min, mipchi2_min=mipchi2_min)


@configurable
def make_tauons_muonic_decay(maker=make_muons_from_b,
                             p_min=3. * GeV,
                             pt_min=None,
                             mipchi2_min=16.):
    """
    Tauons reconstructed through the muonic decay.
    """
    return maker(p_min=p_min, pt_min=pt_min, mipchi2_min=mipchi2_min)


@configurable
def make_fake_tauons_muonic_decay():
    """
    Fake tauons reconstructed through the muonic decay (with a fake muon).
    """
    return make_tauons_muonic_decay(maker=make_fake_muons_from_b_notIsMuon)


@configurable
def make_tauons_hadronic_decay(make_pvs=make_pvs,
                               name='SLB_HadronicTauons_{hash}',
                               comb_m_min=400. * MeV,
                               comb_m_max=3500. * MeV,
                               comb_pt_any_max=300 * MeV,
                               comb_doca_max=0.15 * mm,
                               mipchi2_min=15,
                               bpvdira_min=0.99,
                               vchi2_max=16,
                               m_min=400. * MeV,
                               m_max=3500. * MeV,
                               twobody_m_max=1670 * MeV):
    """
    Tauons reconstructed through the hadronic decay.
    """
    pions = make_prong_pions()
    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.MAXDOCACUT(comb_doca_max),
        F.SUBCOMB(Functor=F.MASS < twobody_m_max, Indices=(1, 3)))

    if mipchi2_min:
        combination_code = F.require_all(
            combination_code, 1 < F.SUM(F.MINIPCHI2(pvs) > mipchi2_min))
    if comb_pt_any_max:
        combination_code = F.require_all(combination_code,
                                         F.SUM(F.PT < comb_pt_any_max) <= 1)

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.BPVDIRA(pvs) > bpvdira_min, F.CHI2 < vchi2_max)

    twobody_code = (F.MASS < twobody_m_max)

    tau = ParticleCombiner([pions, pions, pions],
                           DecayDescriptor="[tau+ -> pi- pi+ pi+]cc",
                           Combination12Cut=twobody_code,
                           CombinationCut=combination_code,
                           CompositeCut=vertex_code)

    tau_chargethree = ParticleCombiner(
        [pions, pions, pions],
        DecayDescriptor="[tau+ -> pi+ pi+ pi+]cc",
        Combination12Cut=twobody_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)

    return ParticleContainersMerger([tau, tau_chargethree], name=name)


@configurable
def isolation_particles(maker=make_long_pions):
    """
    Particles to be persisted for isolation purposes.
    """
    return maker()
