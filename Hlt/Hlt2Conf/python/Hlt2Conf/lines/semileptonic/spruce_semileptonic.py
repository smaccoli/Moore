###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of semileptonic hlt2 lines, notice PROCESS = 'spruce'

Output:
updated dictionary of sprucing_lines

To be noted:
line builders have PROCESS as argument to allow ad hoc settings

"""
from Moore.config import SpruceLine, register_line_builder

from .builders import sl_line_prefilter
from .HbToHcTauNu_TauToLNuNu import make_b0todptaunu_dptokpipi_tautolnunu, make_b0todptaunu_dptokpipi_tautolnunu_fakelepton

PROCESS = 'spruce'
sprucing_lines = {}


@register_line_builder(sprucing_lines)
def spruce_b0todptaunu_dptokpipi_tautomununu_line(
        name="SpruceSLB_B0ToDpTauNu_DpToKPiPi_TauToMuNuNu", prescale=1):
    """
    SL sprucing line for the decay B0 -> D+(-> K pi pi) tau(-> mu nu nu) nu.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu(
        process=PROCESS, lepton="mu")
    return SpruceLine(
        name=name, prescale=prescale, algs=sl_line_prefilter() + [line_alg])


@register_line_builder(sprucing_lines)
def spruce_b0todptaunu_dptokpipi_fakemuon_line(
        name="SpruceSLB_B0ToDpTauNu_DpToKPiPi_FakeMuon", prescale=0.1):
    """
    SL sprucing line for the decay B0 -> D+(-> K pi pi) tau(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu")
    return SpruceLine(
        name=name, prescale=prescale, algs=sl_line_prefilter() + [line_alg])
