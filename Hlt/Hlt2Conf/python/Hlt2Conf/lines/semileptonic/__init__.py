###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the SL selection lines.
"""

from . import hlt2_semileptonic
from . import spruce_semileptonic

all_lines = {}
all_lines.update(hlt2_semileptonic.hlt2_lines)

sprucing_lines = {}
sprucing_lines.update(spruce_semileptonic.sprucing_lines)
