###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Conf.algorithms import ParticleContainersMerger

from .builders.base_builder import make_tauons_hadronic_decay, make_kaons_from_b, make_pions_from_b
from .builders.b_builder import make_b2xulnu


def make_bstoktaunu_hadronic(process):
    """
    Selection for the decay Bs0 -> K tau (->pi+pi+pi-)
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    kaons = make_kaons_from_b()
    taus = make_tauons_hadronic_decay()
    bs_rs = make_b2xulnu(
        particles=[kaons, taus], descriptor="[B_s~0 -> K+ tau-]cc")
    bs_ws = make_b2xulnu(
        particles=[kaons, taus], descriptor="[B_s~0 -> K+ tau+]cc")
    line_alg = ParticleContainersMerger([bs_rs, bs_ws])

    return line_alg


def make_b0topitaunu_hadronic(process):
    """
    Selection for the decay B0 -> pi tau (->pi+pi+pi-)
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    pions = make_pions_from_b()
    taus = make_tauons_hadronic_decay()
    b0_rs = make_b2xulnu(
        particles=[pions, taus], descriptor="[B~0 -> pi+ tau-]cc")
    b0_ws = make_b2xulnu(
        particles=[pions, taus], descriptor="[B~0 -> pi+ tau+]cc")
    line_alg = ParticleContainersMerger([b0_rs, b0_ws])

    return line_alg
