###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Conf.algorithms import ParticleContainersMerger

from .builders.base_builder import (
    make_muons_from_b,
    make_fake_muons_from_b_reversedPID,
    make_fake_muons_from_b_notIsMuon,
    make_electrons_from_b,
    make_fake_electrons_from_b_reversedPID,
)
from .builders.charm_hadron_builder import (
    make_d0_tokpi,
    make_dplus_tokpipi,
    make_d0_tok3pi,
    make_ds_tokkpi,
    make_Hc_to_nbody,
    make_lambdac_topkpi,
    make_xicplus_topkpi,
    make_xic0_topkkpi,
    make_omegac_topkkpi,
)
from .builders.b_builder import make_b2xclnu

from GaudiKernel.SystemOfUnits import MeV
import Functors as F

from PyConf import ConfigurationError


def make_butod0lnu_d0tokpi(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B- -> D0 mu-]cc"
        descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[B- -> D0 e-]cc"
        descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dzs = make_d0_tokpi()
    bus_dzmum = make_b2xclnu([dzs, leptons],
                             descriptor=descriptor_rs,
                             name=f"Bu2D0{lepton}Nu_D02KPi_combiner")
    bus_dzmup = make_b2xclnu([dzs, leptons],
                             descriptor=descriptor_ws,
                             name=f"Bu2D0{lepton}Nu_D02KPi_WS_combiner")
    line_alg = ParticleContainersMerger([bus_dzmum, bus_dzmup])

    return line_alg


def make_butod0lnu_d0tokpi_fakelepton(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_reversedPID()
        descriptor_rs = "[B- -> D0 mu-]cc"
        descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[B- -> D0 e-]cc"
        descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dzs = make_d0_tokpi()
    bus_dzmum = make_b2xclnu([dzs, fake_leptons],
                             descriptor=descriptor_rs,
                             name=f"Bu2D0{lepton}Nu_D02KPi_fakeL_combiner")
    bus_dzmup = make_b2xclnu([dzs, fake_leptons],
                             descriptor=descriptor_ws,
                             name=f"Bu2D0{lepton}Nu_D02KPi_fakeL_WS_combiner")
    line_alg = ParticleContainersMerger([bus_dzmum, bus_dzmup])

    return line_alg


def make_b0todplnu_dptokpipi(process, lepton):
    """
    Selction for the decay B0 -> D+(-> K pi pi) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dps = make_dplus_tokpipi()
    b0s_rightsign = make_b2xclnu([dps, leptons],
                                 descriptor=descriptor_rs,
                                 name=f"B02Dp{lepton}Nu_Dp2KPiPi_combiner")
    b0s_wrongsign = make_b2xclnu([dps, leptons],
                                 descriptor=descriptor_ws,
                                 name=f"B02Dp{lepton}Nu_Dp2KPiPi_WS_combiner")
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_b0todplnu_dptokpipi_fakelepton(process, lepton):
    """
    Selection for the decay B0 -> D+(-> K pi pi) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_reversedPID()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dps = make_dplus_tokpipi()
    b0s_rightsign = make_b2xclnu(
        [dps, fake_leptons],
        descriptor=descriptor_rs,
        name=f"B02Dp{lepton}Nu_Dp2KPiPi_fakeL_combiner")
    b0s_wrongsign = make_b2xclnu(
        [dps, fake_leptons],
        descriptor=descriptor_ws,
        name=f"B02Dp{lepton}Nu_Dp2KPiPi_fakeL_WS_combiner")
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_butod0lnu_d0tok3pi(process, lepton):
    """
    Selction for the decay B+ -> D0(-> K pi pi pi) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B+ -> D0 mu+]cc"
        descriptor_ws = "[B+ -> D0 mu-]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[B+ -> D0 e+]cc"
        descriptor_ws = "[B+ -> D0 e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dzs = make_d0_tok3pi()
    bps_rightsign = make_b2xclnu([dzs, leptons],
                                 descriptor=descriptor_rs,
                                 name=f"B2D0{lepton}Nu_D02K3pi_combiner")
    bps_wrongsign = make_b2xclnu([dzs, leptons],
                                 descriptor=descriptor_ws,
                                 name=f"B2D0{lepton}Nu_D02K3pi_WS_combiner")
    line_alg = ParticleContainersMerger([bps_rightsign, bps_wrongsign])

    return line_alg


def make_butod0lnu_d0tok3pi_fakelepton(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi pi pi) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_reversedPID()
        descriptor_rs = "[B+ -> D0 mu+]cc"
        descriptor_ws = "[B+ -> D0 mu-]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[B+ -> D0 e+]cc"
        descriptor_ws = "[B+ -> D0 e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dzs = make_d0_tok3pi()
    bps_rightsign = make_b2xclnu([dzs, fake_leptons],
                                 descriptor=descriptor_rs,
                                 name=f"B2D0{lepton}Nu_D02K3pi_fakeL_combiner")
    bps_wrongsign = make_b2xclnu(
        [dzs, fake_leptons],
        descriptor=descriptor_ws,
        name=f"B2D0{lepton}Nu_D02K3pi_fakeL_WS_combiner")
    line_alg = ParticleContainersMerger([bps_rightsign, bps_wrongsign])

    return line_alg


def make_bstodslnu_dstokkpi(process, lepton):
    """
    Selection for the decay B_s0 -> Ds(-> K K pi) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B_s0 -> D_s- mu+]cc"
        descriptor_ws = "[B_s0 -> D_s- mu-]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[B_s0 -> D_s- e+]cc"
        descriptor_ws = "[B_s0 -> D_s- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dss = make_ds_tokkpi()
    bss_dsmup = make_b2xclnu([dss, leptons],
                             descriptor=descriptor_rs,
                             name=f"Bs2Ds{lepton}Nu_Ds2KKPi_combiner")
    bss_dsmum = make_b2xclnu([dss, leptons],
                             descriptor=descriptor_ws,
                             name=f"Bs2Ds{lepton}Nu_Ds2KKPi_WS_combiner")
    line_alg = ParticleContainersMerger([bss_dsmum, bss_dsmup])

    return line_alg


def make_bstodslnu_dstokkpi_fakelepton(process, lepton):
    """
    Selection for the decay B_s0 -> Ds(-> K K pi) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_reversedPID()
        descriptor_rs = "[B_s0 -> D_s- mu-]cc"
        descriptor_ws = "[B_s0 -> D_s- mu+]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[B_s0 -> D_s- e-]cc"
        descriptor_ws = "[B_s0 -> D_s- e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dss = make_ds_tokkpi()
    bss_dsmum = make_b2xclnu([dss, fake_leptons],
                             descriptor=descriptor_rs,
                             name=f"Bs2Ds{lepton}Nu_Ds2KKPi_fakeL_combiner")
    bss_dsmup = make_b2xclnu([dss, fake_leptons],
                             descriptor=descriptor_ws,
                             name=f"Bs2Ds{lepton}Nu_Ds2KKPi_fakeL_WS_combiner")
    line_alg = ParticleContainersMerger([bss_dsmum, bss_dsmup])

    return line_alg


def make_lbtolclnu_lctopkpi(process, lepton):
    """
    Selection for the decay [Lb0 -> Lc+(-> p K pi) l nu]cc.
    """

    # this stripping line used to have proton P>8.0GeV
    # do we still need this, and why? for next MR

    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.):
        lcs = make_lambdac_topkpi(
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            mother_pt_min=2500 * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
            comb_pt_min=None,
            comb_pt_any_min=None,
            comb_pt_sum_min=2500 * MeV,
            daughter_p_min=2000 * MeV,
            daughter_pt_min=250 * MeV,
            daughter_mipchi2_min=4.,
            kaon_pid=(F.PID_K > -2.),
            pion_pid=(F.PID_K < 10.),
            proton_pid=F.require_all(F.PID_P > 0., (F.PID_P - F.PID_K) > 0.),
            comb_docachi2_max=20.)

    lb_rightsign = make_b2xclnu(
        particles=[lcs, leptons],
        descriptor=descriptor_rs,
        name=f"Lb2Lc{lepton}Nu_Lc2pKPi_combiner")

    lb_wrongsign = make_b2xclnu(
        particles=[lcs, leptons],
        descriptor=descriptor_ws,
        name=f"Lb2Lc{lepton}Nu_Lc2pKPi_WS_combiner")

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolclnu_lctopkpi_fakelepton(process, lepton):
    """
    Selection for the decay [Lb0 -> Lc+(-> p K pi) l nu]cc, with a fake lepton.
    """

    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if lepton == "mu":
        leptons = make_fake_muons_from_b_notIsMuon()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton == "e":
        leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.):
        lcs = make_lambdac_topkpi(
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            mother_pt_min=2500 * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
            comb_pt_min=None,
            comb_pt_any_min=None,
            comb_pt_sum_min=2500 * MeV,
            daughter_p_min=2000 * MeV,
            daughter_pt_min=250 * MeV,
            daughter_mipchi2_min=4.,
            kaon_pid=(F.PID_K > -2.),
            pion_pid=(F.PID_K < 10.),
            proton_pid=F.require_all(F.PID_P > 0., (F.PID_P - F.PID_K) > 0.),
            comb_docachi2_max=20.)

    lb_rightsign = make_b2xclnu(
        particles=[lcs, leptons],
        descriptor=descriptor_rs,
        name=f"Lb2Lc{lepton}Nu_Lc2pKPi_fakeL_combiner")

    lb_wrongsign = make_b2xclnu(
        particles=[lcs, leptons],
        descriptor=descriptor_ws,
        name=f"Lb2Lc{lepton}Nu_Lc2pKPi_fakeL_WS_combiner")

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_bctod0lnu_d0tokpi(process, lepton):
    """
    Selection for the decay Bc+ -> D0(-> K pi) l nu.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_lp = "[B_c+ -> D0 mu+]cc"
        descriptor_lm = "[B_c- -> D0 mu-]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_lp = "[B_c+ -> D0 e+]cc"
        descriptor_lm = "[B_c- -> D0 e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    dzs = make_d0_tokpi()

    bcs_dzmup = make_b2xclnu(
        particles=[dzs, leptons],
        descriptor=descriptor_lp,
        name=f"Bc2D0{lepton}pNu_D02KPi_combiner")
    bcs_dzmum = make_b2xclnu(
        particles=[dzs, leptons],
        descriptor=descriptor_lm,
        name=f"Bc2D0{lepton}nNu_D02KPi_combiner")
    line_alg = ParticleContainersMerger([bcs_dzmup, bcs_dzmum])

    return line_alg


def make_bctod0lnu_d0tokpi_fakelepton(process, lepton):
    """
    Selection for the decay Bc+ -> D0(-> K pi) l nu, with a fake lepton.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    if lepton == "mu":
        leptons = make_fake_muons_from_b_reversedPID()
        descriptor_lp = "[B_c+ -> D0 mu+]cc"
        descriptor_lm = "[B_c- -> D0 mu-]cc"
    elif lepton == "e":
        leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_lp = "[B_c+ -> D0 e+]cc"
        descriptor_lm = "[B_c- -> D0 e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    dzs = make_d0_tokpi()
    bcs_dzmup = make_b2xclnu(
        particles=[dzs, leptons],
        descriptor=descriptor_lp,
        name=f"Bc2D0{lepton}pNu_D02KPi_fakeMu_combiner")
    bcs_dzmum = make_b2xclnu(
        particles=[dzs, leptons],
        descriptor=descriptor_lm,
        name=f"Bc2D0{lepton}nNu_D02KPi_fakeMu_combiner")
    line_alg = ParticleContainersMerger([bcs_dzmup, bcs_dzmum])

    return line_alg


def make_xib0toxicpluslnu_xicplustopkpi(process, lepton):
    """
    SL line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) l- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) l+ nu (Wrong sign)
    """
    #make light lepton candidates
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[Xi_b0 -> Xi_c+ mu-]cc"
        descriptor_ws = "[Xi_b0 -> Xi_c+ mu+]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[Xi_b0 -> Xi_c+ e-]cc"
        descriptor_ws = "[Xi_b0 -> Xi_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    #make Xi_c+ candidates
    xicplus_mass = 2467.71  # units MeV (PDG 2020)
    delta_xicplus_comb = 90.0  # units MeV
    comb_m_min = (xicplus_mass - delta_xicplus_comb)
    comb_m_max = (xicplus_mass + delta_xicplus_comb)
    delta_xicplus_mother = 80.0  # units MeV
    mother_m_min = (xicplus_mass - delta_xicplus_mother)
    mother_m_max = (xicplus_mass + delta_xicplus_mother)
    xicplus = make_xicplus_topkpi(
        comb_m_min=comb_m_min * MeV,  #default is hardcoded
        comb_m_max=comb_m_max * MeV,  #default is hardcoded
        mother_m_min=mother_m_min * MeV,  #default is hardcoded
        mother_m_max=mother_m_max * MeV)  #default is hardcoded

    #make Xib0 candidates (RS and WS)
    xibs_rs = make_b2xclnu(
        name=f"Xib0ToXicplus{lepton}Builder_RS",
        descriptor=descriptor_rs,
        particles=[xicplus, leptons])
    xibs_ws = make_b2xclnu(
        name=f"Xib0ToXicplus{lepton}Builder_WS",
        descriptor=descriptor_ws,
        particles=[xicplus, leptons])
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xib0toxicpluslnu_xicplustopkpi_fakelepton(process, lepton):
    """
    SL line for the decays with fake lepton:
        Xi_b0 -> Xi_c+ (-> p K- pi+) l- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) l+ nu (Wrong sign)
    """
    #make light lepton candidates
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_notIsMuon(
        )  #IsMuon==0 and no pid
        descriptor_rs = "[Xi_b0 -> Xi_c+ mu-]cc"
        descriptor_ws = "[Xi_b0 -> Xi_c+ mu+]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[Xi_b0 -> Xi_c+ e-]cc"
        descriptor_ws = "[Xi_b0 -> Xi_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    #make Xi_c+ candidates
    xicplus_mass = 2467.71  # units MeV (PDG 2020)
    delta_xicplus_comb = 90.0  # units MeV
    comb_m_min = (xicplus_mass - delta_xicplus_comb)
    comb_m_max = (xicplus_mass + delta_xicplus_comb)
    delta_xicplus_mother = 80.0  # units MeV
    mother_m_min = (xicplus_mass - delta_xicplus_mother)
    mother_m_max = (xicplus_mass + delta_xicplus_mother)
    xicplus = make_xicplus_topkpi(
        comb_m_min=comb_m_min * MeV,
        comb_m_max=comb_m_max * MeV,
        mother_m_min=mother_m_min * MeV,
        mother_m_max=mother_m_max * MeV)

    #make Xib0 candidates (RS and WS)
    xibs_rs = make_b2xclnu(
        name=f"Xib0ToXicplus{lepton}Builder_FakeLepton_RS",
        descriptor=descriptor_rs,
        particles=[xicplus, fake_leptons])
    xibs_ws = make_b2xclnu(
        name=f"Xib0ToXicplus{lepton}Builder_FakeLepton_WS",
        descriptor=descriptor_ws,
        particles=[xicplus, fake_leptons])
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xibminustoxic0lnu_xic0topkkpi(process, lepton):
    """
    SL line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) l- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) l+ nu (Wrong sign)
    """
    #make light lepton candidates
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[Xi_b- -> Xi_c0 mu-]cc"
        descriptor_ws = "[Xi_b- -> Xi_c0 mu+]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[Xi_b- -> Xi_c0 e-]cc"
        descriptor_ws = "[Xi_b- -> Xi_c0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    #make Xi_c0 candidates
    xic0_mass = 2470.44  # units MeV (PDG 2020)
    delta_xic0_comb = 90.0  # units MeV
    comb_m_min = (xic0_mass - delta_xic0_comb)
    comb_m_max = (xic0_mass + delta_xic0_comb)
    delta_xic0_mother = 80.0  # units MeV
    mother_m_min = (xic0_mass - delta_xic0_mother)
    mother_m_max = (xic0_mass + delta_xic0_mother)
    xic0 = make_xic0_topkkpi(
        comb_m_min=comb_m_min * MeV,
        comb_m_max=comb_m_max * MeV,
        mother_m_min=mother_m_min * MeV,
        mother_m_max=mother_m_max * MeV)

    #make Xib- candidates
    xibs_rs = make_b2xclnu(
        name=f"XibminusToXic0{lepton}Builder_RS",
        descriptor=descriptor_rs,
        particles=[xic0, leptons])
    xibs_ws = make_b2xclnu(
        name=f"XibminusToXic0{lepton}Builder_WS",
        descriptor=descriptor_ws,
        particles=[xic0, leptons])
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xibminustoxic0lnu_xic0topkkpi_fakelepton(process, lepton):
    """
    SL line for the decays with a fake lepton:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) l- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) l+ nu (Wrong sign)
    """
    #make light lepton candidates
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_notIsMuon(
        )  #IsMuon==0 and no pid
        descriptor_rs = "[Xi_b- -> Xi_c0 mu-]cc"
        descriptor_ws = "[Xi_b- -> Xi_c0 mu+]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[Xi_b- -> Xi_c0 e-]cc"
        descriptor_ws = "[Xi_b- -> Xi_c0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    #make Xi_c0 candidates
    xic0_mass = 2470.44  # units MeV (PDG 2020)
    delta_xic0_comb = 90.0  # units MeV
    comb_m_min = (xic0_mass - delta_xic0_comb)
    comb_m_max = (xic0_mass + delta_xic0_comb)
    delta_xic0_mother = 80.0  # units MeV
    mother_m_min = (xic0_mass - delta_xic0_mother)
    mother_m_max = (xic0_mass + delta_xic0_mother)
    xic0 = make_xic0_topkkpi(
        comb_m_min=comb_m_min * MeV,
        comb_m_max=comb_m_max * MeV,
        mother_m_min=mother_m_min * MeV,
        mother_m_max=mother_m_max * MeV)

    #make Xib- candidates
    xibs_rs = make_b2xclnu(
        name=f"XibminusToXic0{lepton}Builder_fakeL_RS_combiner",
        descriptor=descriptor_rs,
        particles=[xic0, fake_leptons])
    xibs_ws = make_b2xclnu(
        name=f"XibminusToXic0{lepton}Builder_fakeL_WS_combiner",
        descriptor=descriptor_ws,
        particles=[xic0, fake_leptons])
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_omegabtoomegaclnu_omegactopkkpi(process, lepton):
    """
    Selection for the decay [Omegab- -> Omegac0(-> p K- K- pi+) l nu]cc.
    """

    Omegac_mass = 2695.2  # units MeV
    delta_Omegac_comb = 90.0  # units MeV, Delta combination
    delta_Omegac_mother = 60.0  # units MeV, Delta composite

    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[Omega_b- -> Omega_c0 mu-]cc"
        descriptor_ws = "[Omega_b- -> Omega_c0 mu+]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[Omega_b- -> Omega_c0 e-]cc"
        descriptor_ws = "[Omega_b- -> Omega_c0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    omegacs = make_omegac_topkkpi(
        mother_m_min=(Omegac_mass - delta_Omegac_mother) * MeV,
        mother_m_max=(Omegac_mass + delta_Omegac_mother) * MeV,
        comb_m_min=(Omegac_mass - delta_Omegac_comb) * MeV,
        comb_m_max=(Omegac_mass + delta_Omegac_comb) * MeV)

    omegab_rightsign = make_b2xclnu(
        particles=[omegacs, leptons],
        descriptor=descriptor_rs,
        name=f"Omegab2Omegac{lepton}Nu_Omegac2pKKPi_combiner")
    omegab_wrongsign = make_b2xclnu(
        particles=[omegacs, leptons],
        descriptor=descriptor_ws,
        name=f"Omegab2Omegac{lepton}Nu_Omegac2pKKPi_WS_combiner")
    linealg = ParticleContainersMerger([omegab_rightsign, omegab_wrongsign])

    return linealg


def make_omegabtoomegaclnu_omegactopkkpi_fakelepton(process, lepton):
    """
    Selection for the decay [Omegab- -> Omegac0(-> p K- K- pi+) l nu]cc, with a fake lepton. 
    """

    Omegac_mass = 2695.2  # units MeV
    delta_Omegac_comb = 90.0  # units MeV, Delta combination
    delta_Omegac_mother = 60.0  # units MeV, Delta composite

    # Fake muons
    if lepton == "mu":
        leptons = make_fake_muons_from_b_notIsMuon()
        descriptor_rs = "[Omega_b- -> Omega_c0 mu-]cc"
        descriptor_ws = "[Omega_b- -> Omega_c0 mu+]cc"
    elif lepton == "e":
        leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[Omega_b- -> Omega_c0 e-]cc"
        descriptor_ws = "[Omega_b- -> Omega_c0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    omegacs = make_omegac_topkkpi(
        mother_m_min=(Omegac_mass - delta_Omegac_mother) * MeV,
        mother_m_max=(Omegac_mass + delta_Omegac_mother) * MeV,
        comb_m_min=(Omegac_mass - delta_Omegac_comb) * MeV,
        comb_m_max=(Omegac_mass + delta_Omegac_comb) * MeV)

    omegab_rightsign = make_b2xclnu(
        particles=[omegacs, leptons],
        descriptor=descriptor_rs,
        name=f"Omegab2Omegac{lepton}Nu_Omegac2pKKPi_fakeL_combiner")
    omegab_wrongsign = make_b2xclnu(
        particles=[omegacs, leptons],
        descriptor=descriptor_ws,
        name=f"Omegab2Omegac{lepton}Nu_Omegac2pKKPi_fakeL_WS_combiner")
    linealg = ParticleContainersMerger([omegab_rightsign, omegab_wrongsign])

    return linealg
