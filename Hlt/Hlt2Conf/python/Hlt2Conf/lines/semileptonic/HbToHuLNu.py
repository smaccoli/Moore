###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

from .builders.base_builder import make_muons_from_b, make_fake_muons_from_b_notIsMuon, make_protons_from_b, make_muons_for_btomunu, make_fake_muons_for_btomunu, make_pions_from_b, make_kaons_from_b
from .builders.b_builder import make_b2xulnu


def make_muons_for_lbtopmunu(pid=None):
    """
    Muons produced in the decay Lb0 -> p mu nu.
    """
    return make_muons_from_b(
        pid=pid, p_min=3000 * MeV, pt_min=1500 * MeV, mipchi2_min=16)


def make_fake_muons_for_lbtopmunu():
    """
    Same builder as above, but requiring the particles to have ismuon==0.
    """
    return make_fake_muons_from_b_notIsMuon(
        p_min=3000 * MeV, pt_min=1500 * MeV, mipchi2_min=16)


def make_lbtopmunu(process):
    """
    Selection for the decay Lb0 -> p mu nu.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    protons = make_protons_from_b()
    muons = make_muons_for_lbtopmunu()
    line_alg = make_b2xulnu([protons, muons],
                            "[Lambda_b0 -> p+ mu-]cc",
                            vchi2pdof_max=9.0,
                            bpvdira_min=0.9994,
                            bpvfdchi2_min=150)

    return line_alg


def make_lbtopmunu_fakep(process):
    """
    Selection for the decay Lb0 -> p mu nu, with a fake proton.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    protons = make_protons_from_b(
        pid=F.require_all(F.PID_P < 10., F.PID_P - F.PID_K < 10.))
    muons = make_muons_for_lbtopmunu()
    line_alg = make_b2xulnu([protons, muons],
                            "[Lambda_b0 -> p+ mu-]cc",
                            vchi2pdof_max=9.0,
                            bpvdira_min=0.9994,
                            bpvfdchi2_min=150)

    return line_alg


def make_lbtopmunu_fakemu(process):
    """
    Selection for the decay Lb0 -> p mu nu, with a fake muon.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    protons = make_protons_from_b()
    muons = make_fake_muons_for_lbtopmunu()
    line_alg = make_b2xulnu([protons, muons],
                            "[Lambda_b0 -> p+ mu-]cc",
                            vchi2pdof_max=9.0,
                            bpvdira_min=0.9994,
                            bpvfdchi2_min=150)

    return line_alg


def make_lbtopmunu_ss(process):
    """
    Selection for the decay Lb0 -> p mu nu (same-sign).
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    protons = make_protons_from_b()
    muons = make_muons_for_lbtopmunu()
    line_alg = make_b2xulnu([protons, muons],
                            "[Lambda_b0 -> p+ mu+]cc",
                            vchi2pdof_max=9.0,
                            bpvdira_min=0.9994,
                            bpvfdchi2_min=150)

    return line_alg


def make_lbtopmunu_ss_fakep(process):
    """
    Selection for the decay Lb0 -> p mu nu (same-sign), with a fake proton.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    protons = make_protons_from_b(
        pid=F.require_all(F.PID_P < 10., F.PID_P - F.PID_K < 10.))
    muons = make_muons_for_lbtopmunu()
    line_alg = make_b2xulnu([protons, muons],
                            "[Lambda_b0 -> p+ mu+]cc",
                            vchi2pdof_max=9.0,
                            bpvdira_min=0.9994,
                            bpvfdchi2_min=150)

    return line_alg


def make_lbtopmunu_ss_fakemu(process):
    """
    Selection for the decay Lb0 -> p mu nu (same-sign), with a fake muon.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    protons = make_protons_from_b()
    muons = make_fake_muons_for_lbtopmunu()
    line_alg = make_b2xulnu([protons, muons],
                            "[Lambda_b0 -> p+ mu+]cc",
                            vchi2pdof_max=9.0,
                            bpvdira_min=0.9994,
                            bpvfdchi2_min=150)

    return line_alg


def make_b0topimunu(process):
    """
    Selection for the decay B0 -> pi mu nu.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    pions = make_pions_from_b()
    muons = make_muons_for_btomunu()
    line_alg = make_b2xulnu(
        particles=[pions, muons], descriptor="[B~0 -> pi+ mu-]cc")

    return line_alg


def make_b0topimunu_fakemu(process):
    """
    Selection for the decay B0 -> pi mu nu, with a fake muon.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    pions = make_pions_from_b()
    muons_nopid = make_fake_muons_for_btomunu()
    line_alg = make_b2xulnu(
        particles=[pions, muons_nopid], descriptor="[B~0 -> pi+ mu-]cc")

    return line_alg


def make_b0topimunu_fakek(process):
    """
    Selection for the decay B0 -> pi mu, with a fake pions.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    pions_nopid = make_pions_from_b(pid=None)
    muons = make_muons_for_btomunu()
    line_alg = make_b2xulnu(
        particles=[pions_nopid, muons], descriptor="[B~0 -> pi+ mu-]cc")

    return line_alg


def make_bstokmunu(process):
    """
    Selection for the decay Bs0 -> K mu nu.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    kaons = make_kaons_from_b()
    muons = make_muons_for_btomunu()
    line_alg = make_b2xulnu(
        particles=[kaons, muons], descriptor="[B_s~0 -> K+ mu-]cc")

    return line_alg


def make_bstokmunu_fakemu(process):
    """
    Selection for the decay Bs0 -> K mu nu, with a fake muon.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    kaons = make_kaons_from_b()
    muons_nopid = make_fake_muons_for_btomunu()
    line_alg = make_b2xulnu(
        particles=[kaons, muons_nopid], descriptor="[B_s~0 -> K+ mu-]cc")

    return line_alg


def make_bstokmunu_fakek(process):
    """
    Selection for the decay Bs0 -> K mu, with a fake kaon.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    kaons_nopid = make_kaons_from_b(pid=None)
    muons = make_muons_for_btomunu()
    line_alg = make_b2xulnu(
        particles=[kaons_nopid, muons], descriptor="[B_s~0 -> K+ mu-]cc")

    return line_alg
