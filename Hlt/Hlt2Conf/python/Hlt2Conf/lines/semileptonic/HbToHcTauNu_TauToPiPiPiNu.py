###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Conf.algorithms import ParticleContainersMerger
from GaudiKernel.SystemOfUnits import MeV, mm

from .builders.charm_hadron_builder import make_dplus_tokpipi, make_jpsi_tomumu, make_d0_tokpi, make_ds_tokkpi, make_lambdac_topkpi, make_d0_tok3pi
from .builders.b_builder import make_b2xtaunu, make_b2xulnu
from .builders.base_builder import make_tauons_hadronic_decay, make_protons_from_b

_B0_M = 5279.65 * MeV
_Bp_M = 5279.34 * MeV
_Bc_M = 6274.47 * MeV
_Bs_M = 5366.88 * MeV
_Lb_M = 5619.60 * MeV


def make_bptod0taunu_d0tok3pi_tautopipipinu(process):
    """
    Selection for the decay B+ -> D~0 tau+, hadronic tau decay, D~0 -> K- pi- pi+ pi+.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    d0s = make_d0_tok3pi()

    m_min = _Bp_M - 3000 * MeV
    m_max = _Bp_M + 2000 * MeV
    comb_m_min = _Bp_M - 3000 * MeV
    comb_m_max = _Bp_M + 2000 * MeV
    bpvdira_min = 0.995
    vchi2pdof_max = 100.
    bpvfdchi2_min = 0.

    line_alg = make_b2xtaunu(
        particles=[d0s, tauons],
        descriptor="[B+ -> D~0 tau+]cc",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min)

    return line_alg


def make_b0todptaunu_dptokpipi_tautopipipinu(process):
    """
    Selection for the decay B0 -> D- tau+, hadronic tau decay.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    dps = make_dplus_tokpipi()
    # IMPORTANT NOTE: we need to check if the cut trpchi2 > 0.01 is needed for the Dp children.
    b0s_rightsign = make_b2xtaunu(
        particles=[dps, tauons], descriptor="[B0 -> D- tau+]cc")
    b0s_wrongsign = make_b2xtaunu(
        particles=[dps, tauons], descriptor="[B0 -> D- tau-]cc")
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_bctojpsitaunu_jpsitomumu_tautopipipinu(process):
    """
    Selection for the decay B_c+ -> J/psi(1S) tau+, hadronic tau decay.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    jpsis = make_jpsi_tomumu()

    m_min = _Bc_M - 3000 * MeV
    m_max = _Bc_M + 2000 * MeV
    comb_m_min = _Bc_M - 3000 * MeV
    comb_m_max = _Bc_M + 2000 * MeV
    comb_doca_max = 0.2 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.
    bpvfdchi2_min = 0.

    line_alg = make_b2xtaunu(
        particles=[jpsis, tauons],
        descriptor="[B_c+ -> J/psi(1S) tau+]cc",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min)

    return line_alg


def make_bptod0taunu_d0tokpi_tautopipipinu(process):
    """
    Selection for the decay B+ -> D~0 tau+, hadronic tau decay.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    dzeros = make_d0_tokpi()

    m_min = _Bp_M - 3000 * MeV
    m_max = _Bp_M + 2000 * MeV
    comb_m_min = _Bp_M - 3000 * MeV
    comb_m_max = _Bp_M + 2000 * MeV
    comb_doca_max = 0.2 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.
    bpvfdchi2_min = 0.

    line_alg = make_b2xtaunu(
        particles=[dzeros, tauons],
        descriptor="[B+ -> D~0 tau+]cc",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min)

    return line_alg


def make_bstodstaunu_dstokkpi_tautopipipinu(process):
    """
    Selection for the decay B_s0 -> D_s- tau+, hadronic tau decay.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    dss = make_ds_tokkpi(
        # comb_m_min=1760 * MeV,
        # comb_m_max=2080 * MeV,
    )

    m_min = _Bs_M - 3000 * MeV
    m_max = _Bs_M + 2000 * MeV
    comb_mb_min = _Bs_M - 3000 * MeV
    comb_mb_max = _Bs_M + 2000 * MeV
    comb_doca_max = 0.2 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.
    bpvfdchi2_min = 0.

    bss_rightsign = make_b2xtaunu(
        particles=[dss, tauons],
        descriptor="[B_s0 -> D_s- tau+]cc",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_mb_min,
        comb_m_max=comb_mb_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min)

    bss_wrongsign = make_b2xtaunu(
        particles=[dss, tauons],
        descriptor="[B_s0 -> D_s- tau-]cc",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_mb_min,
        comb_m_max=comb_mb_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min)

    line_alg = ParticleContainersMerger([bss_rightsign, bss_wrongsign])

    return line_alg


def make_lbtolctaunu_lctopkpi_tautopipipinu(process):
    """
    Selection for the decay Lambda_b0 -> Lambda_c+ tau-, hadronic tau decay.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    lcs = make_lambdac_topkpi()

    m_min = _Lb_M - 3000 * MeV
    m_max = _Lb_M + 2000 * MeV
    comb_m_min = _Lb_M - 3000 * MeV
    comb_m_max = _Lb_M + 2000 * MeV
    comb_doca_max = 0.2 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.
    bpvfdchi2_min = 0.
    #AMAXDOCA<0.15

    Lbs_rightsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor="[Lambda_b0 -> Lambda_c+ tau-]cc",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min)

    Lbs_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor="[Lambda_b0 -> Lambda_c+ tau+]cc",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min)

    line_alg = ParticleContainersMerger([Lbs_rightsign, Lbs_wrongsign])

    return line_alg


def make_lbtoptaunu_tautopipipinu(process):
    """
    Selection for the decay Lambda_b0 -> p+ tau-, hadronic tau decay.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    #preliminary, add CHILD() cuts
    protons = make_protons_from_b(
        pt_min=1200 * MeV, mipchi2_min=25.0)  #pid = (F.PID_P > 5.))
    tauons = make_tauons_hadronic_decay()

    m_min = _Lb_M - 3000 * MeV
    m_max = _Lb_M + 2000 * MeV
    comb_m_min = _Lb_M - 3000 * MeV
    comb_m_max = _Lb_M + 2000 * MeV
    comb_doca_max = 0.2 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.
    bpvfdchi2_min = 0.
    #AMAXDOCA<0.15

    Lbs_rightsign = make_b2xulnu(
        particles=[protons, tauons],
        descriptor="[Lambda_b0 -> p+ tau-]cc",
        mcorr_min=m_min,
        mcorr_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        comb_doca_max=comb_doca_max)

    Lbs_wrongsign = make_b2xulnu(
        particles=[protons, tauons],
        descriptor="[Lambda_b0 -> p+ tau+]cc",
        mcorr_min=m_min,
        mcorr_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        comb_doca_max=comb_doca_max)

    line_alg = ParticleContainersMerger([Lbs_rightsign, Lbs_wrongsign])

    return line_alg
