###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Generic lines used for testing purposes. Some are snapshots of production lines, but are not used for production.
Follow https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hlt2_line.html#code-design-guidelines
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, micrometer as um, ps
from PyConf.Algorithms import Monitor__ParticleRange
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs

from ...algorithms_thor import ParticleFilter, ParticleCombiner
from ...standard_particles import (
    make_long_electrons_with_brem, make_long_muons, make_has_rich_long_pions,
    make_long_pions, make_photons, make_has_rich_down_protons, make_down_pions,
    make_has_rich_long_kaons, make_long_kaons, make_has_rich_long_protons)

all_lines = {}


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


#Dummy line, included to trigger neutral reconstruction.
@register_line_builder(all_lines)
def photons_line(name="Hlt2Generic_Photons", prescale=1):
    photons = make_photons()
    line_alg = ParticleFilter(Input=photons, Cut=F.FILTER(F.P > 40 * GeV))
    # test monitoring of neutral basics
    photon_mon = Monitor__ParticleRange(
        name="Monitor_Generic_Photons_IsPhoton",
        Input=photons,
        Variable=F.IS_PHOTON,
        HistogramName="/Generic_Photons/IsPhoton",
        Bins=100,
        Range=(0, 1))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg, photon_mon])


#Dummy line, testing on top of neutrals the AdditionalInfo functors
@register_line_builder(all_lines)
def filtered_photons_line(name="Hlt2Generic_FilteredPhotons", prescale=1):
    photons = make_photons()
    code = F.require_all(F.P > 40 * GeV, F.IS_PHOTON > 0.5, F.IS_NOT_H > 0.3,
                         F.CALO_NEUTRAL_SHOWER_SHAPE < 1500)
    line_alg = ParticleFilter(Input=photons, Cut=F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


# Dummy line, testing the algorithms in Rec creating and cutting on multiple relation tables
from PyConf.Algorithms import (
    LHCb__Phys__RelationTables__FilterRelTables as FilterRelTables,
    LHCb__Phys__RelationTables__MakeRelTables as MakeRelTables,
)


@register_line_builder(all_lines)
def mRT_photons_line(name="Hlt2Generic_mRTPhotons", prescale=1):
    def tr(**outputs):
        return {"OutputLocation": [v for (k, v) in outputs.items()]}

    ai = ["IsPhoton", "ShowerShape", "IsNotH"]
    tables = MakeRelTables(
        InputParticles=make_photons(),
        AdditionalInfo=ai,
        outputs={prop: None
                 for prop in ai},
        output_transform=tr).outputs
    predicateCut = (  (F.COLUMN(ColumnLabel=tables["IsPhoton"].location) > 0.5) \
                    & (F.COLUMN(ColumnLabel=tables["ShowerShape"].location) <  1500) \
                    & (F.COLUMN(ColumnLabel=tables["IsNotH"].location) >0.3) \
                    )
    photons = FilterRelTables(InputTables = [ dh for dh in tables.values()], \
                                RelCuts = predicateCut).OutputLocation
    code = F.require_all(F.P > 40 * GeV)
    line_alg = ParticleFilter(photons, F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


# Dummy line, testing the algorithms in Rec creating and cutting on single relation tables
from PyConf.Algorithms import (
    LHCb__Phys__RelationTables__FilterRelTable as FilterRelTable,
    LHCb__Phys__RelationTables__AiToRelTable as AiToRelTable,
)


@register_line_builder(all_lines)
def sRT_photons_line(name="Hlt2Generic_sRTPhotons", prescale=1):
    relTab = AiToRelTable(
        InputParticles=make_photons(),
        AdditionalInfo="ShowerShape").OutputLocation
    predicateCut = (F.IDENTITY < 1500)
    photons = FilterRelTable(
        InputTable=relTab, Predicate=predicateCut).OutputLocation
    code = F.require_all(
        F.P > 40 * GeV,
        F.IS_PHOTON > 0.5,
        F.IS_NOT_H > 0.3,
    )
    line_alg = ParticleFilter(photons, F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


'''
Dummy line, testing the neutral functors from relation tables.
For now, the functions that create the functor must be imported inside
the line, as the relation tables are only produced once the
neutral reconstruction has run.
'''


@register_line_builder(all_lines)
def fRT_photons_line(name="Hlt2Generic_fRTPhotons", prescale=1):
    from RecoConf.reconstruction_objects import (get_IsPhoton_table,
                                                 get_IsNotH_table)
    IS_PHOTON = F.TO_VALUE_RELATION_TABLE(get_IsPhoton_table(), 1)
    IS_NOT_H = F.TO_VALUE_RELATION_TABLE(get_IsNotH_table())
    photons = make_photons()
    code = F.require_all(F.P > 40 * GeV, IS_PHOTON > 0.5, IS_NOT_H > 0.3)
    line_alg = ParticleFilter(Input=photons, Cut=F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


#From Hlt2Conf/lines/monitoring/pi0_line.py
@register_line_builder(all_lines)
def pi0_to_gammagamma_line(name="Hlt2Generic_Pi0ToGammaGamma", prescale=1):
    code = F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, F.IS_NOT_H > 0.2)
    photons = ParticleFilter(Input=make_photons(), Cut=F.FILTER(code))

    combination_code = F.require_all(
        F.PT > 1400 * MeV,
        F.math.in_range(100 * MeV, F.MASS, 200 * MeV),
    )
    pi0s = ParticleCombiner(
        # mstahl: note on naming -- this module names all combiners and their names begin with "Generic_" to be able to easily group them in performace tests and spot them when inspecting log files.
        # Filters are not named, see https://gitlab.cern.ch/lhcb/Moore/-/issues/378 and https://gitlab.cern.ch/lhcb/Moore/-/issues/380
        name="Generic_Pi0ToGammaGamma_Combiner",
        Inputs=[photons, photons],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="pi0 -> gamma gamma",
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
    )
    return Hlt2Line(name=name, algs=[pi0s], prescale=prescale)


#From Hlt2Conf/lines/charm/hyperons.py
@register_line_builder(all_lines)
def xicz_to_lpipi_dd_line(name="Hlt2Generic_Xic0ToL0PimPip_DD"):
    pvs = make_pvs()
    down_pions = ParticleFilter(make_down_pions(), F.FILTER(F.PT > 180 * MeV))
    down_protons = ParticleFilter(
        make_has_rich_down_protons(),
        F.FILTER(
            F.require_all(F.PT > 700 * MeV, F.P > 10 * GeV, F.PID_P > -5.)))
    dd_lambdas = ParticleCombiner(
        [down_protons, down_pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Generic_Hyperons_Lambda_DD",
        CombinationCut=F.require_all(
            F.MASS < 1190 * MeV,
            F.MAXDOCACUT(2 * mm),
            F.MAXDOCACHI2CUT(12.),
            F.PT > 0.9 * GeV,
            F.P > 13 * GeV,
            F.SUM(F.PT) > 1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1165 * MeV,
            F.PT > 1 * GeV,
            F.P > 14 * GeV,
            F.CHI2DOF < 9.,
        ),
    )
    long_xicz_pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_K < 5.), ),
    )
    xic0s = ParticleCombiner(
        [dd_lambdas, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- pi+]cc",
        name="Generic_Hyperons_Xic0ToL0PimPip_DD",
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(800 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 800 * um,
            F.DOCA(2, 3) < 150 * um,
            F.math.in_range(2250 * MeV, F.MASS, 2590 * MeV),
            F.MAX(F.MINIPCHI2(pvs)) > 9.,
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 6.,
            F.BPVDIRA(pvs) > 0.996,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dd_lambdas, xic0s])


#From Hlt2Conf/lines/charm/hyperons.py
@register_line_builder(all_lines)
def oc_to_ximkpipi_lll_line(name="Hlt2Generic_Oc0ToXimKmPipPip_LLL"):
    pvs = make_pvs()
    long_lambda_pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(32.))))
    long_lambda_protons = ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 9 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_P > -5.)))
    ll_lambdas = ParticleCombiner(
        [long_lambda_protons, long_lambda_pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Generic_Hyperons_LambdaFromHyperon_LL",
        CombinationCut=F.require_all(
            F.MASS < 1190 * MeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 450 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1165 * MeV,
            F.PT > 550 * MeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(make_pvs()) > 8 * mm,
            F.BPVFDCHI2(make_pvs()) > 480.,
        ),
    )
    long_xi_pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(8.))))
    lll_xis = ParticleCombiner(
        [ll_lambdas, long_xi_pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Generic_Hyperons_Xim_LLL",
        CombinationCut=F.require_all(
            F.MASS < 1400 * MeV,
            F.MAXDOCACUT(150 * um),
            F.PT > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.PT > 600 * MeV,
            F.CHI2DOF < 9.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 4 * mm,
            F.BPVFDCHI2(pvs) > 16.,
        ),
    )
    long_oc_kaons = ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 5 * GeV, _MIPCHI2_MIN(3.),
                          F.PID_K > 0.), ),
    )
    long_oc_pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, _MIPCHI2_MIN(3.),
                          F.PID_K < 5.), ),
    )
    oc0s = ParticleCombiner(
        [lll_xis, long_oc_kaons, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Xi- K- pi+ pi+]cc",
        name="Generic_Hyperons_Oc0ToXimKmPipPip_LLL",
        Combination12Cut=F.require_all(
            F.MASS < 2535 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 275 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), ll_lambdas, lll_xis, oc0s])


#From former LoKi example line
def _make_phi():
    kaons = make_long_kaons()
    filteredKaons = ParticleFilter(
        name="FilteredPIDkaons", Input=kaons, Cut=F.FILTER(F.PID_K > 0.))
    combination_code = F.require_all(
        F.math.in_range(980. * MeV, F.MASS, 1060. * MeV),
        F.MAXDOCACHI2CUT(30.),
        F.SUM(F.CHI2DOF < 5.) > 0)
    vertex_code = F.require_all(F.CHI2 < 10, F.PT > 1 * GeV)
    return ParticleCombiner(
        name="Generic_Phi2KKMaker",
        Inputs=[kaons, filteredKaons],
        DecayDescriptor=
        "[phi(1020) -> K+ K-]cc",  # mstahl: how does the combiner handle the different inputs? is cc needed because of this?
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def _make_bs2jpsiphi(jpsi, phi, pvs):
    combination_code = F.require_all(
        F.math.in_range(5100 * MeV, F.MASS, 5600 * MeV))
    vertex_code = F.require_all(
        F.math.in_range(5100 * MeV, F.MASS, 5600 * MeV), F.CHI2DOF < 20,
        F.BPVLTIME(pvs) > 0.01 * ps)
    return ParticleCombiner(
        name="Generic_Bs2JPsiPhiCombiner_{hash}",
        Inputs=[jpsi, phi],
        DecayDescriptor="B_s0 -> J/psi(1S) phi(1020)",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


# test monitoring
def my_mass_mon(datahandle, name, min, max):
    return


@register_line_builder(all_lines)
def bs2jpsiphi_jpsi2mumu_phi2kk_line(
        name='Hlt2Generic_Bs0ToJpsiPhi_JPsiToMupMum', prescale=1):
    pvs = make_pvs()
    # mstahl: (almost) all production lines should use make_ismuon_long_muon instead of make_long_muons
    muons = make_long_muons()
    filteredMuons = ParticleFilter(
        name="FilteredPIDMuons", Input=muons, Cut=F.FILTER(F.PID_MU > 0.))
    mother_code = F.CHI2 < 16.
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV),
        F.MAXDOCACHI2 <
        20,  # mstahl: mixture of MAXDOCACHI2 and MAXDOCACHI2CUT. the latter should be preferred
        F.SUM(F.PT > 0.5 * GeV) > 0)
    jpsi = ParticleCombiner(
        name="Generic_MassConstrJpsi2MuMuMaker",
        Inputs=[muons, filteredMuons],
        DecayDescriptor=
        "[J/psi(1S) -> mu+ mu-]cc",  # mstahl: how does the combiner handle the different inputs? is cc needed because of this?
        CombinationCut=combination_code,
        CompositeCut=mother_code)

    phi = _make_phi()
    phi_mon = Monitor__ParticleRange(
        name="Monitor_Phi_for_Generic_Bs0ToJpsiPhi_m",
        Input=phi,
        Variable=F.MASS,
        HistogramName="/Phi_for_Generic_Bs0ToJpsiPhi/m",
        Bins=100,
        Range=(980 * MeV, 1060 * MeV))

    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), phi_mon, jpsi, bs],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(all_lines)
def bs2jpsiphi_jpsi2ee_phi2kk_line(name='Hlt2BsToJpsiPhi_JPsi2ee_PhiToKK',
                                   prescale=1):
    pvs = make_pvs()
    electrons = make_long_electrons_with_brem()
    filteredElectrons = ParticleFilter(
        name="FilteredPIDElectrons",
        Input=electrons,
        Cut=F.FILTER(F.PID_E > 0.))
    mother_code = F.CHI2 < 16.
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV), F.MAXDOCACHI2 < 20,
        F.SUM(F.PT > 0.5 * GeV) > 0)
    jpsi = ParticleCombiner(
        name=name,
        Inputs=[electrons, filteredElectrons],
        DecayDescriptor="[J/psi(1S) -> e+ e-]cc",
        CombinationCut=combination_code,
        CompositeCut=mother_code)

    phi = _make_phi()
    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), jpsi, bs],
        prescale=prescale,
    )
