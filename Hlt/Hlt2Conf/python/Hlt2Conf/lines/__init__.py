###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Package containing modules that define HLT2 lines.

Modules defined within this package should register the lines they want
to export in a registry called `all_lines`.

"""
import logging
from Moore.config import add_line_to_registry

from . import (
    b_to_open_charm,
    rd,
    bandq,
    topological_b,
    pid,
    qee,
    ift,
    charm,
    b_to_charmonia,
    semileptonic,
    monitoring,
    charmonium_to_dimuon,
    bnoc,
    DiMuonNoIP,
    trackeff,
)

from Hlt2Conf.lines.inclusive_detached_dilepton import inclusive_detached_dilepton_trigger

__all__ = [
    'all_lines',
    'sprucing_lines',
    'modules',
]

log = logging.getLogger(__name__)


def _get_all_lines(modules):
    all_lines = {}
    for module in modules:
        try:
            lines = module.all_lines
        except AttributeError:
            raise AttributeError(
                'line module {} does not define mandatory `all_lines`'.format(
                    module.__name__))
        for name, maker in lines.items():
            add_line_to_registry(all_lines, name, maker)
    return all_lines


def _get_sprucing_lines(modules):
    sprucing_lines = {}
    for module in modules:
        try:
            lines = module.sprucing_lines
        except AttributeError:
            log.info('line module {} does not define `sprucing_lines`'.format(
                module.__name__))
            continue
        for name, maker in lines.items():
            add_line_to_registry(sprucing_lines, name, maker)
    return sprucing_lines


modules = [
    b_to_open_charm,
    inclusive_detached_dilepton_trigger,
    rd,
    bandq,
    topological_b,
    pid,
    qee,
    ift,
    charm,
    b_to_charmonia,
    semileptonic,
    monitoring,
    charmonium_to_dimuon,
    bnoc,
    DiMuonNoIP,
    trackeff,
]

all_lines = _get_all_lines(modules)
sprucing_lines = _get_sprucing_lines(modules)
