###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 lines for
  - ``B_s0 -> J/psi phi``       Hlt2Starterkit_Bs0ToJpsiPhi_PR
  - ``B_s0 -> K+ K- mu+ mu-``   Hlt2Starterkit_Bs0ToKmKpMumMup_SP
Both lines select essentially the same events; the former does so with an "traditional"
approach of three 2-body combinations, while the latter uses a 4-body combiner directly.
Which one is faster?
Another difference is that the first line uses perist reco,
and the second latter selective persistence.

TODO: With this selection we measured rates above 1 kHz on an Hlt1-filtered MinBias sample.
The signal rate should be around 
(2000*10^30 cm^-2 s^-1)*(87*0.254*10^-30 cm^2)*(0.001*0.06*0.5) = 1.32 Hz
(instantaneous luminosity)*(B+ production cross section @13 TeV * f_s/f_u)*(Br(Bs->J/psi phi)*Br(J/psi->mu+mu-)*Br(phi->K+K-))
See https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/hlt2_guidelines.html#true-signal-rate ,
the PDG, and https://lhcbproject.web.cern.ch/lhcbproject/Publications/l/LHCb-PAPER-2020-046.html.

Can you help out by improving the selection?
"""
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import (GeV, MeV, mm)
from PyConf.Algorithms import VoidFilter

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import (
    make_pvs,
    upfront_reconstruction,
)
from RecoConf.event_filters import require_pvs
from ...standard_particles import (make_long_kaons, make_ismuon_long_muon,
                                   make_long_pions)
from ...algorithms_thor import (ParticleCombiner, ParticleFilter)

all_lines = {}


def _muons_for_jpsi():
    pvs = make_pvs()
    cut = F.require_all(
        F.PT > 300 * MeV,
        F.MINIPCHI2(pvs) > 3.,
    )
    return ParticleFilter(make_ismuon_long_muon(), F.FILTER(cut))


def _kaons_for_phi():
    pvs = make_pvs()
    cut = F.require_all(
        F.PT > 120 * MeV,
        F.MINIPCHI2(pvs) > 4.,
    )
    return ParticleFilter(make_long_kaons(), F.FILTER(cut))


@register_line_builder(all_lines)
def bs0_to_jpsiphi_line(name="Hlt2Starterkit_Bs0ToJpsiPhi_PR"):

    pvs = make_pvs()
    muons = _muons_for_jpsi()
    kaons = _kaons_for_phi()

    jpsi_combination_code = F.require_all(
        in_range(2.7 * GeV, F.MASS, 3.4 * GeV),
        F.SUM(F.PT) > 0.8 * GeV,
        F.MAXDOCACUT(0.2 * mm),
    )
    jpsi_vertex_code = F.require_all(
        in_range(2.8 * GeV, F.MASS, 3.3 * GeV),
        F.CHI2DOF < 12.,
    )
    jpsis = ParticleCombiner(
        [muons, muons],
        name="Starterkit_BsToJpsiPhi_JpsiToMumMup_Combiner",
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=jpsi_combination_code,
        CompositeCut=jpsi_vertex_code,
    )

    phi_combination_code = F.require_all(
        F.MASS < 1100 * MeV,
        F.MAXDOCACUT(0.2 * mm),
    )
    phi_vertex_code = F.require_all(
        F.MASS < 1080 * MeV,
        F.CHI2DOF < 12.,
    )
    phis = ParticleCombiner(
        [kaons, kaons],
        name="Starterkit_BsToJpsiPhi_PhiToKmKp_Combiner",
        DecayDescriptor="phi(1020) -> K+ K-",
        CombinationCut=phi_combination_code,
        CompositeCut=phi_vertex_code,
    )

    bs_combination_code = F.require_all(
        in_range(4.9 * GeV, F.MASS, 6.3 * GeV),
        F.SUM(F.PT) > 3 * GeV,
        F.MAXDOCACUT(0.2 * mm),
    )
    bs_vertex_code = F.require_all(
        F.CHI2DOF < 12.,
        in_range(5 * GeV, F.MASS, 6.2 * GeV),
        F.BPVFDCHI2(pvs) > 6.,
    )
    bss = ParticleCombiner(
        [jpsis, phis],
        name="Starterkit_BsToJpsiPhi_BsToJpsiPhi_Combiner",
        DecayDescriptor="B_s0 -> J/psi(1S) phi(1020)",
        CombinationCut=bs_combination_code,
        CompositeCut=bs_vertex_code,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), jpsis, bss],
        persistreco=True,
    )


@register_line_builder(all_lines)
def bs0_to_kkmumu_line(name="Hlt2Starterkit_Bs0ToKmKpMumMup_SP"):

    pvs = make_pvs()
    muons = _muons_for_jpsi()
    require_at_least_two_muons = VoidFilter(
        name='Starterkit_BsToJpsiPhi_require_two_muons',
        Cut=F.SIZE(muons) > 1,
    )
    kaons = _kaons_for_phi()

    bs_two_body_combination_code = F.require_all(
        in_range(2.7 * GeV, F.MASS, 3.4 * GeV),
        F.SUM(F.PT) > 0.8 * GeV,
        F.MAXDOCACUT(0.2 * mm),
    )
    bs_three_body_combination_code = F.MAXDOCACUT(0.2 * mm)
    bs_combination_code = F.require_all(
        in_range(4.9 * GeV, F.MASS, 6.3 * GeV),
        F.SUM(F.PT) > 3 * GeV,
        F.MAXDOCACUT(0.2 * mm),
        F.SUBCOMB(Functor=F.MASS < 1100 * MeV, Indices=[3, 4]),
    )
    bs_vertex_code = F.require_all(
        F.CHI2DOF < 12.,
        in_range(5 * GeV, F.MASS, 6.2 * GeV),
        F.BPVFDCHI2(pvs) > 6.,
        F.SUBCOMB(
            Functor=in_range(2.8 * GeV, F.MASS, 3.3 * GeV), Indices=[1, 2]),
        F.SUBCOMB(Functor=F.MASS < 1100 * MeV, Indices=[3, 4]),
    )
    bss = ParticleCombiner(
        [muons, muons, kaons, kaons],
        name="Starterkit_BsToJpsiPhi_BsToKKMuMu_Combiner",
        DecayDescriptor="B_s0 -> mu+ mu- K+ K-",
        Combination12Cut=bs_two_body_combination_code,
        Combination123Cut=bs_three_body_combination_code,
        CombinationCut=bs_combination_code,
        CompositeCut=bs_vertex_code,
    )

    pion_cut = F.require_all(
        F.PT > 200 * MeV,
        F.MINIPCHI2(pvs) > 4.,
    )
    pions_for_bc = ParticleFilter(make_long_pions(), F.FILTER(pion_cut))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), require_at_least_two_muons, bss],
        extra_outputs=[("PiForBc", pions_for_bc)],
    )
