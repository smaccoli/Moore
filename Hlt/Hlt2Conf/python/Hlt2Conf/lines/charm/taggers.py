###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Taggers employed by charm trigger lines:

 1. D*(2010)+ -> D0 pi+
 2. B -> D0 mu- X
 3. B -> D*(2010)+ (-> D0 pi+) mu- X

The following unphysical combinations are included as well to model
combinatorial background:

 4. B -> D*(2010)+ (-> D0 pi+) mu+ X

All taggers should preferably be called with a name in camel style, e.g.
`CharmDstpToD0Pi_D0ToKmPip`. Otherwise a default (but longer) name will be
assigned.

N.B.: For D0 self-conjugate final states the charge conjugate D~0 objects are
      not created because they would have identical properties and hence would
      be a waste of CPU (duplicating the selection and vertexing).
      This is annoying when D* tagging is added, however, and one cannot create
      the 'physical' charge combinations of `D*+ -> D0 pi+` and
      `D*- -> D~0 pi-`. It can be considered a flaw in the selection framework
      that we cannot currently express these sorts of combinations cleanly.
      For the moment the 'unphisical' `D*- -> D0 pi-` decay is reconstructed
      instead. See https://gitlab.cern.ch/lhcb/Moore/issues/64
"""

import inspect
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from Hlt2Conf.algorithms import ParticleContainersMerger
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from Hlt2Conf.standard_particles import make_long_pions, make_ismuon_long_muon
from RecoConf.reconstruction_objects import make_pvs


def _get_caller_function_name():
    """
    Gets the function name (expects to be called from a function with local 'name' object with value 'Hlt2<function_name>').
    """
    calling_function_name = inspect.getargvalues(
        inspect.currentframe().f_back.f_back).locals.get("name", "")
    if calling_function_name.startswith("Hlt2"):
        return calling_function_name.replace("Hlt2", "")
    else:
        return ""


def make_tagging_pions(for_b=False):
    """Return maker for tagging pions for building D*(2010)+ -> D0 pi+ decays.

    Args:
        for_b (bool, optional): Set whether cuts should be tuned for prompt
                                or doubly tagged lines.
    """
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.PT > (150 * MeV if for_b else 200 * MeV),
                          F.P > (2 * GeV if for_b else 1 * GeV))),
    )


def make_tagging_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=9., Vertices=make_pvs()),
                F.PT > 1 * GeV,
                F.P > 2 * GeV,
                F.PID_MU > 0.,
            ), ),
    )


def _make_dstar_from_descriptor(dzeros, decay_descriptor, name, for_b=False):
    """Make D*(2010)+ -> D0 pi+ candidates starting from D0/anti-D0 candidates
    and a decay descriptor. This is a helper function and should not be used
    in lines definitions. The function `make_dstars` should be used instead.

    Args:
        dzeros (DataHandle): Input D0 and anti-D0 candidates.
        decay_descriptor (string): Decay descriptor for the combination, e.g.
                                   `[D*(2010)+ -> D0 pi+]cc`
        name (string): Name for the combiner algorithm.
        for_b (bool, optional): Set whether the D*(2010)+ cuts should be tuned
                                for the singly or doubly tagged lines.

    Returns:
        Output of particle filtering algorithm as particle container
        DataHandle.
    """
    pions = make_tagging_pions(for_b)
    return ParticleCombiner(
        [dzeros, pions],
        DecayDescriptor=decay_descriptor,
        name=name,
        CombinationCut=F.MASS - F.CHILD(1, F.MASS) < 165 * MeV,
        CompositeCut=F.require_all(F.MASS - F.CHILD(1, F.MASS) < 160 * MeV,
                                   F.CHI2DOF < (16. if for_b else 25.)),
    )


def make_dstars(dzeros, self_conjugate_d0_decay, d0_name=None, for_b=False):
    """Make a D*(2010)+ -> D0 pi+ combination. Both self-conjugate and non
    self-conjugate D0 decays are supported. Slow pions are added within
    this function and don't have to be passed as an argument.

    Args:
        dzeros (DataHandle): Input D0 and anti-D0 candidates.
        self_conjugate_d0_decay (bool): True if the D0 final state is
                                        self-conjugate and anti-D0 are not
                                        reconstructed.
        d0_name (string, optional): D0 nickname to be used in the name of the
                                    combiner algorithm. If the D*+ is used in
                                    both singly and doubly tagged lines with
                                    the same requirements, the name should be
                                    the same as that passed to the B tagger.
        for_b (bool, optional): Set whether the D*(2010)+ cuts should be tuned
                                for the singly or doubly tagged lines.
                                Defaults to False.

    Returns:
        Output of particle filtering algorithm as particle container
        DataHandle.
    """
    if d0_name is None:
        d0_name = _get_caller_function_name()
    if self_conjugate_d0_decay:
        dstarp = _make_dstar_from_descriptor(
            dzeros, "D*(2010)+ -> D0 pi+",
            f"Charm_Taggers_DstpBuilder{'ForB' if for_b else ''}For{d0_name}",
            for_b)
        dstarm = _make_dstar_from_descriptor(
            dzeros, "D*(2010)- -> D0 pi-",
            f"Charm_Taggers_DstmBuilder{'ForB' if for_b else ''}For{d0_name}",
            for_b)
        return ParticleContainersMerger([dstarp, dstarm])
    else:
        return _make_dstar_from_descriptor(
            dzeros, "[D*(2010)+ -> D0 pi+]cc",
            f"Charm_Taggers_DstBuilder{'ForB' if for_b else ''}For{d0_name}",
            for_b)


def _make_sl_bs_from_descriptor(ds, muons, decay_descriptor, name):
    """Make B- -> D mu- candidates starting from D/anti-D candidates
    and a decay descriptor. This is just a helper function and should not be
    used in lines definitions. The function `make_sl_bs` should be used
    instead.

    Args:
        ds (DataHandle): Input D and anti-D candidates.
        decay_descriptor (string): Decay descriptor for the combination.
        name (string): Name for the combiner algorithm.

    Returns:
        Output of particle filtering algorithm as particle container
        DataHandle.
    """
    pvs = make_pvs()
    return ParticleCombiner(
        [ds, muons],
        DecayDescriptor=decay_descriptor,
        name=name,
        CombinationCut=F.require_all(
            in_range(2.3 * GeV, F.MASS, 10 * GeV), F.MAXDOCACHI2CUT(10.)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 9., in_range(2.3 * GeV, F.MASS, 10 * GeV),
            in_range(2.8 * GeV, F.BPVCORRM(pvs), 8.5 * GeV),
            F.BPVDIRA(pvs) > 0.999,
            F.CHILD(1, F.END_VZ) - F.END_VZ > -3 * mm),
    )


def make_sl_bs(dzeros, muons, self_conjugate_d0_decay, d0_name=None):
    """Make a B- -> D0 mu- combination. Both self-conjugate and non
    self-conjugate D0 decays are supported.

    Args:
        dzeros (DataHandle): Input D0 and anti-D0 candidates.
        muons (DataHandle): Input muon candidates. It is recommended to use
                            `make_tagging_muons`.
        self_conjugate_d0_decay (bool): True if the D0 final state is
                                        self-conjugate and anti-D0 are not
                                        reconstructed.
        d0_name (string, optional): D0 nickname to be used in the name of the
                                    combiner algorithm.

    Returns:
        Output of particle filtering algorithm as particle container
        DataHandle.
    """
    if d0_name is None:
        d0_name = _get_caller_function_name()
    if self_conjugate_d0_decay:
        bms = _make_sl_bs_from_descriptor(
            dzeros, muons, "B- -> D0 mu-",
            f"Charm_Taggers_BpBuilderFor{d0_name}")
        bps = _make_sl_bs_from_descriptor(
            dzeros, muons, "B+ -> D0 mu+",
            f"Charm_Taggers_BmBuilderFor{d0_name}")
        return ParticleContainersMerger([bms, bps])
    else:
        return _make_sl_bs_from_descriptor(
            dzeros, muons, "[B- -> D0 mu-]cc",
            f"Charm_Taggers_BBuilderFor{d0_name}")


def make_dt_bs(dzeros,
               muons,
               self_conjugate_d0_decay,
               d0_name=None,
               same_sign=False):
    """Make a B0 -> D*(2010)- mu combination. Both self-conjugate and non
    self-conjugate D0 decays and opposite-sign (physical) and
    same-sign (unphysical) D*(2010)-mu combinations are supported.

    Args:
        dzeros (DataHandle): Input D0 and anti-D0 candidates.
        muons (DataHandle): Input muon candidates. It is recommended to use
                            `make_tagging_muons`.
        self_conjugate_d0_decay (bool, optional): True if the D0 final state is
                                                  self-conjugate and anti-D0
                                                  are not reconstructed.
                                                  Defaults to False.
        d0_name (string, optional): D0 nickname to be used in the name of the
                                    combiner algorithm. If the D*+ is used in
                                    both singly and doubly tagged lines with
                                    the same requirements, the name should be
                                    the same as that passed to the D*+ tagger.
        same_sign (bol, optional): True to produce wrong-sign combinations
                                   to model combinatorial background.

    Returns:
        Output of particle filtering algorithm as particle container
        DataHandle.
    """
    if d0_name is None:
        d0_name = _get_caller_function_name()
    dstars = make_dstars(
        dzeros, self_conjugate_d0_decay, d0_name=d0_name, for_b=True)
    if same_sign:
        return _make_sl_bs_from_descriptor(
            dstars, muons, "[B0 -> D*(2010)- mu-]cc",
            f"Charm_Taggers_BToDstpMup_BuilderFor{d0_name}")
    else:
        return _make_sl_bs_from_descriptor(
            dstars, muons, "[B0 -> D*(2010)- mu+]cc",
            f"Charm_Taggers_BToDstpMum_BuilderFor{d0_name}")
