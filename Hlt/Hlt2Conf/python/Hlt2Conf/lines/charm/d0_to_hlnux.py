###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of some D0 -> h l nu and D0 -> h h h l nuHLT2 lines.
All lines are tagged with D*(2010)+ -> D0 pi+ decays.

3-body lines:
---------------------
    D0 -> K- e+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmEpNu_OS)
    D0 -> K- e- nu (Hlt2Charm_DstpToD0Pip_D0ToKmEmNu_SS)
    D0 -> K- mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmMupNu_OS)
    D0 -> K- mu- nu (Hlt2Charm_DstpToD0Pip_D0ToKmMumNu_SS)
    D0 -> pi- e+ nu (Hlt2Charm_DstpToD0Pip_D0ToPimEpNu_OS)
    D0 -> pi- e- nu (Hlt2Charm_DstpToD0Pip_D0ToPimEmNu_SS)
    D0 -> pi- mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToPimMupNu_OS)
    D0 -> pi- mu- nu (Hlt2Charm_DstpToD0Pip_D0ToPimMumNu_SS)

    D0 -> K+ e- nu (Hlt2Charm_DstpToD0Pip_D0ToKpEmNu_WS)
    D0 -> K+ mu- nu (Hlt2Charm_DstpToD0Pip_D0ToKpMumNu_WS)

5-body lines:
---------------------
    D0 -> K- Pi- Pi+ e+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmPimPipEpNu)
    D0 -> K- Pi+ Pi+ e- nu (Hlt2Charm_DstpToD0Pip_D0ToKmPipPipEmNu_KLepSS)
    D0 -> K- Pi- Pi+ e- nu / D0 -> K- Pi+ Pi+ e+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmPimPipEmNu_SS)
    D0 -> K- Pi- Pi+ mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmPimPipMupNu)
    D0 -> K- Pi+ Pi+ mu- nu (Hlt2Charm_DstpToD0Pip_D0ToKmPipPipMumNu_KLepSS)
    D0 -> K- Pi- Pi+ mu- nu / D0 -> K- Pi+ Pi+ mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmPimPipMumNu_SS)

    D0 -> K- K+ Pi- e+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmKpPimEpNu)
    D0 -> K- K+ Pi+ e- nu (Hlt2Charm_DstpToD0Pip_D0ToKmKpPipEmNu_KLepSS)
    D0 -> K- K+ Pi- e- nu / D0 -> K- K+ Pi+ e+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmKpPimEmNu_SS)
    D0 -> K- K+ Pi- mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmKpPimMupNu)
    D0 -> K- K+ Pi+ mu- nu (Hlt2Charm_DstpToD0Pip_D0ToKmKpPipMumNu_KLepSS)
    D0 -> K- K+ Pi- mu- nu / D0 -> K- Pi+ Pi+ mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmKpPimMumNu_SS)

    D0 -> K- K- K+ e+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmKmKpEpNu_KLepRS)
    D0 -> K- K+ K+ e- nu (Hlt2Charm_DstpToD0Pip_D0ToKmKpKpEmNu_KLepWS)
    D0 -> K- K- K+ e- nu / D0 -> K- K+ K+ e+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmKmKpEmNu_KLepSS)
    D0 -> K- K- K+ mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmKmKpMupNu_KLepRS)
    D0 -> K- K+ K+ mu- nu (Hlt2Charm_DstpToD0Pip_D0ToKmKpKpMumNu_KLepWS)
    D0 -> K- K- K+ mu- nu / D0 -> K- K+ K+ mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToKmKmKpMumNu_KLepSS)

    D0 -> pi- pi- pi+ e+ nu (Hlt2Charm_DstpToD0Pip_D0ToPimPimPipEpNu_PiLepRS)
    D0 -> pi- pi+ pi+ e- nu (Hlt2Charm_DstpToD0Pip_D0ToPimPiPimEmNu_PiLepWS)
    D0 -> pi- pi- pi+ e- nu / D0 -> pi- pi+ pi+ e+ nu (Hlt2Charm_DstpToD0Pip_D0ToPimPimPipEmNu_PiLepSS)
    D0 -> pi- pi- pi+ mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToPimPimPipMupNu_PiLepRS)
    D0 -> pi- pi+ pi+ mu- nu (Hlt2Charm_DstpToD0Pip_D0ToPimPipPiPMumNu_PiLepWS), background
    D0 -> pi- pi- pi+ mu- nu / D0 -> pi- pi+ pi+ mu+ nu (Hlt2Charm_DstpToD0Pip_D0ToPimPimPipMumNu_PiLepSS)

TODO: Incorporation of Partial Reco BDT from Run2
"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV, micrometer as um
from Hlt2Conf.algorithms import ParticleContainersMerger
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from Hlt2Conf.standard_particles import (make_long_pions, make_long_kaons,
                                         make_long_electrons_with_brem,
                                         make_ismuon_long_muon)
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from .prefilters import charm_prefilters
from .taggers import make_tagging_pions

all_lines = {}

###############################################################################
# Shortcuts for (combined) particles used throughout the module
###############################################################################


def _make_long_kaons_for_sld():
    """Kaons for D0 -> K l nu decays.
    Requires poor pointing to the PV and has tighter cuts on the P, PT.
    """
    return ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 800. * MeV,
                F.P > 3 * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=9., Vertices=make_pvs()),
                F.CHI2DOF < 3.,
                F.GHOSTPROB < 0.35,
                F.PID_K > 5,
                # F.PID_MU > 5,
                # F.PID_P > 5,
                F.PID_K - F.PID_MU > 5,
            )))


def _make_long_pions_for_sld():
    """Pions for D0 -> pi l nu decays
    Requires poor pointing to the PV and has tighter cuts on the P, PT.
    """
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 800. * MeV, F.P > 3 * GeV,
                          F.MINIPCHI2CUT(IPChi2Cut=9., Vertices=make_pvs()),
                          F.CHI2DOF < 3., F.GHOSTPROB < 0.35, F.PID_K < -5)))


def _make_long_muons_for_sld():
    """Muons for D0 -> h mu nu decays
    Does not have pointing requirement for the PV and loose cuts on PT for
    attempt to not bias q2.
    """

    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 500. * MeV,
                F.PID_MU > 3,
                F.MINIPCHI2CUT(IPChi2Cut=1., Vertices=make_pvs()),
                # F.MINIPCUT(IPCut=50.*um, Vertices=make_pvs()),
            )))


def _make_long_electrons_with_brem_for_sld():
    """Electrons for D0 -> h e nu decays.
    Does not have pointing requirement for the PV and loose cuts on PT for
    attempt to not bias q2.
    As brem is subset of nobrem, nobrem is the most general container.
    Rate should be studied, as run the risk of uncontrolled kpi.
    """

    return ParticleFilter(
        make_long_electrons_with_brem(),
        F.FILTER(
            F.require_all(
                F.PT > 500. * MeV,
                F.CHI2DOF < 3.,
                F.GHOSTPROB < 0.35,
                F.PID_E > 1.,
                F.MINIPCHI2CUT(IPChi2Cut=1., Vertices=make_pvs()),
                # F.MINIPCUT(IPCut=50.*um, Vertices=make_pvs()),
            )))


###############################################################################
# D0 -> K l combinations
###############################################################################


def _make_d0_part_reco_ke_os():
    """Combine to form D0 -> K- e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_electrons_with_brem_for_sld(),
            _make_long_kaons_for_sld()
        ],
        name="Charm_SLD_D0_Ke_OS",
        DecayDescriptor="[D0 -> e+ K-]cc",
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV), ),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_ke_ss():
    """Combine to form D0 -> K- e- background decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_electrons_with_brem_for_sld(),
            _make_long_kaons_for_sld()
        ],
        name="Charm_SLD_D0_Ke_SS",
        DecayDescriptor="[D0 -> e- K-]cc",
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV), ),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kmu_os():
    """Combine to form D0 -> K- mu+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[_make_long_muons_for_sld(),
                _make_long_kaons_for_sld()],
        name="Charm_SLD_D0_Kmu_OS",
        DecayDescriptor="[D0 -> mu+ K-]cc",
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV), ),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kmu_ss():
    """Combine to form D0 -> K- mu- background decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[_make_long_muons_for_sld(),
                _make_long_kaons_for_sld()],
        name="Charm_SLD_D0_Kmu_SS",
        DecayDescriptor="[D0 -> mu- K-]cc",
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV), ),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


###############################################################################
# D0 -> pi l combinations
###############################################################################


def _make_d0_part_reco_pie_os():
    """Combine to form D0 -> pi- e+ nu decays.
    Slightly looser min combination mass for background.
    """
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_electrons_with_brem_for_sld(),
            _make_long_pions_for_sld()
        ],
        name="Charm_SLD_D0_pie_OS",
        DecayDescriptor="[D0 -> e+ pi-]cc",
        CombinationCut=F.require_all(
            in_range(300. * MeV, F.MASS, 2000. * MeV), ),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_pie_ss():
    """Combine to form D0 -> pi- e- background decays.
    Slightly looser min combination mass for background.
    """
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_electrons_with_brem_for_sld(),
            _make_long_pions_for_sld()
        ],
        name="Charm_SLD_D0_pie_SS",
        DecayDescriptor="[D0 -> e- pi-]cc",
        CombinationCut=F.require_all(
            in_range(300. * MeV, F.MASS, 2000. * MeV), ),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_pimu_os():
    """Combine to form D0 -> pi- mu+ nu decays.
    Slightly looser min combination mass for background.
    """
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[_make_long_muons_for_sld(),
                _make_long_pions_for_sld()],
        name="Charm_SLD_D0_pimu_OS",
        DecayDescriptor="[D0 -> mu+ pi-]cc",
        CombinationCut=F.require_all(
            in_range(300. * MeV, F.MASS, 2000. * MeV), ),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_pimu_ss():
    """Combine to form D0 -> pi- mu- background decays.
    Slightly looser min combination mass for background.
    """
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[_make_long_muons_for_sld(),
                _make_long_pions_for_sld()],
        name="Charm_SLD_D0_pimu_SS",
        DecayDescriptor="[D0 -> mu- pi-]cc",
        CombinationCut=F.require_all(
            in_range(300. * MeV, F.MASS, 2000. * MeV), ),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


###############################################################################
# D0 -> h h h l combinations
###############################################################################

####################
# K pi pi
####################


def _make_d0_part_reco_kpipie_os():
    """Combine to form D0 -> K- pi- pi+ e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_Kpipie_OS",
        DecayDescriptor="[D0 -> K- pi- pi+ e+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kpipie_KLepSS():
    """Combine to form D0 -> K- pi+ pi+ e- nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_Kpipie_KLepSS",
        DecayDescriptor="[D0 -> K- pi+ pi+ e-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kpipie_SS1():
    """Combine to form D0 -> K- pi- pi+ e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_Kpipie_SS1",
        DecayDescriptor="[D0 -> K- pi- pi+ e-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kpipie_SS2():
    """Combine to form D0 -> K- pi+ pi+ e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_Kpipie_SS2",
        DecayDescriptor="[D0 -> K- pi+ pi+ e+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kpipimu_os():
    """Combine to form D0 -> K- pi- pi+ e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_Kpipimu_OS",
        DecayDescriptor="[D0 -> K- pi- pi+ mu+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kpipimu_KLepSS():
    """Combine to form D0 -> K- pi+ pi+ mu- nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_Kpipimu_KLepSS",
        DecayDescriptor="[D0 -> K- pi+ pi+ mu-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kpipimu_SS1():
    """Combine to form D0 -> K- pi- pi+ mu- nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_Kpipimu_SS1",
        DecayDescriptor="[D0 -> K- pi- pi+ mu-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kpipimu_SS2():
    """Combine to form D0 -> K- pi+ pi+ mu+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_Kpipimu_SS2",
        DecayDescriptor="[D0 -> K- pi+ pi+ mu+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


####################
# K K pi
####################


def _make_d0_part_reco_kkpie_os():
    """Combine to form D0 -> K- K+ Pi- e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_KKpie_OS",
        DecayDescriptor="[D0 -> K- K+ pi- e+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkpie_KLepSS():
    """Combine to form D0 -> K- K+ pi+ e- nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_KKpie_KLepSS",
        DecayDescriptor="[D0 -> K- K+ pi+ e-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkpie_SS1():
    """Combine to form D0 -> K- K+ pi- e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_KKpie_SS1",
        DecayDescriptor="[D0 -> K- K+ pi- e-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkpie_SS2():
    """Combine to form D0 -> K- K+ pi+ e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_KKpie_SS2",
        DecayDescriptor="[D0 -> K- K+ pi+ e+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkpimu_os():
    """Combine to form D0 -> K- K+ pi- mu+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_KKpimu_OS",
        DecayDescriptor="[D0 -> K- K+ pi- mu+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkpimu_KLepSS():
    """Combine to form D0 -> K- K+ pi+ mu- nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_KKpimu_KLepSS",
        DecayDescriptor="[D0 -> K- K+ pi+ mu-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkpimu_SS1():
    """Combine to form D0 -> K- K+ pi- mu- nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_KKpimu_SS1",
        DecayDescriptor="[D0 -> K- K+ pi- mu-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkpimu_SS2():
    """Combine to form D0 -> K- K+ pi+ mu+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_KKpimu_SS2",
        DecayDescriptor="[D0 -> K- K+ pi+ mu+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


####################
# K K K
####################


def _make_d0_part_reco_kkke_neutral():
    """Combine to form D0 -> K- K- K+ e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_KKKe_neutral",
        DecayDescriptor="[D0 -> K- K- K+ e+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkke_charged():
    """Combine to form D0 -> K- K+ K+ e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_KKKe_charged",
        DecayDescriptor="[D0 -> K- K+ K+ e+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkkmu_neutral():
    """Combine to form D0 -> K- K- K+ mu+."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_KKKmu_neutral",
        DecayDescriptor="[D0 -> K- K- K+ mu+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_kkkmu_charged():
    """Combine to form D0 -> K- K+ K+ mu+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_kaons_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_KKKmu_charged",
        DecayDescriptor="[D0 -> K- K+ K+ mu+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


####################
# pi pi pi
####################


def _make_d0_part_reco_3pie_neutral():
    """Combine to form D0 -> pi- pi- pi+ e+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_3pie_neutral",
        DecayDescriptor="[D0 -> pi- pi- pi+ e+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_3pie_charged():
    """Combine to form D0 -> pi- pi+ pi+ e- nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_electrons_with_brem_for_sld()
        ],
        name="Charm_SLD_D0_3pie_charged",
        DecayDescriptor="[D0 -> pi- pi+ pi+ e-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1999.5 * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_3pimu_neutral():
    """Combine to form D0 -> K- K+ K- mu+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_3pimu_neutral",
        DecayDescriptor="[D0 -> pi- pi- pi+ mu+]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


def _make_d0_part_reco_3pimu_charged():
    """Combine to form D0 -> pi- pi+ pi+ mu+ nu decays."""
    pvs = make_pvs()

    return ParticleCombiner(
        Inputs=[
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_pions_for_sld(),
            _make_long_muons_for_sld()
        ],
        name="Charm_SLD_D0_3pimu_charged",
        DecayDescriptor="[D0 -> pi- pi+ pi+ mu-]cc",
        # FIXME adding a mass cut in Combination12Cut would
        # likely improve performance.
        # Combination12Cut=F.require_all(F.MASS < XYZ * MeV),
        Combination123Cut=F.require_all(F.MASS < 1895. * MeV),
        CombinationCut=F.require_all(
            in_range(500. * MeV, F.MASS, 2000. * MeV),
            F.MAXSDOCACUT(100 * um)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 20., in_range(500. * MeV, F.BPVCORRM(pvs),
                                      3000. * MeV),
            F.BPVDIRA(pvs) > 0.999,
            F.BPVVDZ(pvs) > 0.,
            F.BPVFDCHI2(pvs) > 100.))


###############################################################################
# D*(2010)+ -> D0 pi+ combiners
###############################################################################


def _build_my_dstar(dzs, decay):
    """Build the D* from any D0 defined and passed decay descriptor."""
    return ParticleCombiner(
        Inputs=[dzs, make_tagging_pions()],
        DecayDescriptor=decay,
        CombinationCut=F.require_all(
            F.MAXDOCACHI2CUT(20.),
            F.MASS - F.CHILD(1, F.MASS) < 405. * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS - F.CHILD(1, F.MASS) < 400. * MeV,
            F.CHI2DOF < 9.,
        ))


###############################################################################
# Lines definition
###############################################################################

####################
# K l nu
####################


@register_line_builder(all_lines)
def dst_to_d0pi_d02kenu_os(name="Hlt2Charm_DstpToD0Pip_D0ToKmEpNu_OS"):
    """Opposite-sign D -> K e nu."""
    dz = _make_d0_part_reco_ke_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kenu_ss(name="Hlt2Charm_DstpToD0Pip_D0ToKmEmNu_SS"):
    """Same-sign D -> K e background."""
    dz = _make_d0_part_reco_ke_ss()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kenu_ws(name="Hlt2Charm_DstpToD0Pip_D0ToKpEmNu_WS"):
    """Wrong-sign D -> K e background."""
    dz = _make_d0_part_reco_ke_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kmunu_os(name="Hlt2Charm_DstpToD0Pip_D0ToKmMupNu_OS"):
    """Opposite-sign D -> K mu nu."""
    dz = _make_d0_part_reco_kmu_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kmunu_ss(name="Hlt2Charm_DstpToD0Pip_D0ToKmMumNu_SS"):
    """Same-sign D -> K mu background."""
    dz = _make_d0_part_reco_kmu_ss()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kmunu_ws(name="Hlt2Charm_DstpToD0Pip_D0ToKpMumNu_WS"):
    """Wrong-sign D -> K mu background."""
    dz = _make_d0_part_reco_kmu_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


####################
# pi l nu
####################


@register_line_builder(all_lines)
def dst_to_d0pi_d02pienu_os(name="Hlt2Charm_DstpToD0Pip_D0ToPimEpNu_OS"):
    """Opposite-sign D -> pi e nu."""
    dz = _make_d0_part_reco_pie_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02pienu_ss(name="Hlt2Charm_DstpToD0Pip_D0ToPimEmNu_SS"):
    """Same-sign D -> pi e background."""
    dz = _make_d0_part_reco_pie_ss()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02pimunu_os(name="Hlt2Charm_DstpToD0Pip_D0ToPimMupNu_OS"):
    """Opposite-sign D -> pi mu nu."""
    dz = _make_d0_part_reco_pimu_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02pimunu_ss(name="Hlt2Charm_DstpToD0Pip_D0ToPimMumNu_SS"):
    """Same-sign D -> pi mu background."""
    dz = _make_d0_part_reco_pimu_ss()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


####################
# K pi pi l nu
####################


@register_line_builder(all_lines)
def dst_to_d0pi_d02kpipienu_os(name="Hlt2Charm_DstpToD0Pip_D0ToKmPimPipEpNu"):
    """D0 -> K- Pi+ Pi- e+ nu."""
    dz = _make_d0_part_reco_kpipie_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kpipienu_klep_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmPipPipEmNu_KLepSS"):
    """D0 -> K- Pi+ Pi+ e- nu SS channel."""
    dz = _make_d0_part_reco_kpipie_KLepSS()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kpipienu_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmPimPipEmNu_SS"):
    """D0 -> K- Pi+ Pi- e- nu / D0 -> K- Pi+ Pi+ e+ nu SS channel."""
    dz1 = _make_d0_part_reco_kpipie_SS1()
    dz2 = _make_d0_part_reco_kpipie_SS2()
    dz = ParticleContainersMerger([dz1, dz2])
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kpipimunu_os(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmPimPipMupNu"):
    """D0 -> K- Pi+ Pi- Mu+ nu."""
    dz = _make_d0_part_reco_kpipimu_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kpipimunu_klep_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmPipPipMumNu_KLepSS"):
    """D0 -> K- Pi+ Pi+ mu- nu SS channel."""
    dz = _make_d0_part_reco_kpipimu_KLepSS()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kpipimunu_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmPimPipMumNu_SS"):
    """D0 -> K- Pi+ Pi- mu- nu / D0 -> K- Pi+ Pi+ mu+ nu SS channel."""
    dz1 = _make_d0_part_reco_kpipimu_SS1()
    dz2 = _make_d0_part_reco_kpipimu_SS2()
    dz = ParticleContainersMerger([dz1, dz2])
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


####################
# K K pi l nu
####################


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkpienu_os(name="Hlt2Charm_DstpToD0Pip_D0ToKmKpPimEpNu"):
    """D0 -> K- K+ Pi- e+ nu."""
    dz = _make_d0_part_reco_kkpie_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkpienu_klep_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmKpPipEmNu_KLepSS"):
    """D0 -> K- K+ Pi+ e- nu SS channel."""
    dz = _make_d0_part_reco_kkpie_KLepSS()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkpienu_ss(name="Hlt2Charm_DstpToD0Pip_D0ToKmKpPimEmNu_SS"):
    """D0 -> K- K+ Pi- mu- nu / D0 -> K- K+ Pi+ mu+ nu SS channel."""
    dz1 = _make_d0_part_reco_kkpie_SS1()
    dz2 = _make_d0_part_reco_kkpie_SS2()
    dz = ParticleContainersMerger([dz1, dz2])
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkpimunu_os(name="Hlt2Charm_DstpToD0Pip_D0ToKmKpPimMupNu"):
    """D0 -> K- K+ Pi- e+ nu."""
    dz = _make_d0_part_reco_kkpimu_os()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkpimunu_klep_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmKpPipMumNu_KLepSS"):
    """D0 -> K- K+ Pi+ mu- nu SS channel."""
    dz = _make_d0_part_reco_kkpimu_KLepSS()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkpimunu_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmKpPimMumNu_SS"):
    """D0 -> K- K+ Pi- mu- nu / D0 -> K- K+ Pi+ mu+ nu SS channel."""
    dz1 = _make_d0_part_reco_kkpimu_SS1()
    dz2 = _make_d0_part_reco_kkpimu_SS2()
    dz = ParticleContainersMerger([dz1, dz2])
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


####################
# K K K l nu
####################


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkkenu_rs(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmKmKpEpNu_KLepRS"):
    """D0 -> K- K+ K- e+ nu."""
    dz = _make_d0_part_reco_kkke_neutral()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkkenu_ws(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmKpKpEmNu_KLepWS"):
    """D0 -> K- K+ K+ e- nu."""
    dz = _make_d0_part_reco_kkke_neutral()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkkenu_klep_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmKmKpEmNu_KLepSS"):
    """D0 -> K- K+ K+/- e+/- nu SS channel
    allows explicit reconstruction of
    D*+ -> D0 Pi with
    D0 -> K- K+ K- e- nu
    D0 -> K- K+ K+ e+ nu
    and their charge conjugates
    - 2 negative kaons with an electron, D*+,
    - 2 positive kaons with a positron, D*+,
    - 2 positive kaons with a positron, D*-,
    - 2 negative kaons with an electron, D*-.
    """
    dz = _make_d0_part_reco_kkke_charged()
    # Merger takes into account possible background, which is just oppositely tagged signal
    dstp_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    dstm_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    dst_to_d0pi = ParticleContainersMerger([dstp_to_d0pi, dstm_to_d0pi])
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkkmunu_rs(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmKmKpMupNu_KLepRS"):
    """D0 -> K- K+ K- mu+ nu."""
    dz = _make_d0_part_reco_kkkmu_neutral()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkkmunu_ws(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmKpKpMumNu_KLepWS"):
    """D0 -> K- K+ K+ mu- nu."""
    dz = _make_d0_part_reco_kkkmu_neutral()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d02kkkmunu_klep_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmKmKpMumNu_KLepSS"):
    """D0 -> K- K+ K+/- mu+/- nu SS channel."""
    dz = _make_d0_part_reco_kkkmu_charged()
    # Merger takes into account possible background, which is just oppositely tagged signal
    dstp_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    dstm_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    dst_to_d0pi = ParticleContainersMerger([dstp_to_d0pi, dstm_to_d0pi])
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


####################
# pi pi pi l nu
####################


@register_line_builder(all_lines)
def dst_to_d0pi_d023pienu_rs(
        name="Hlt2Charm_DstpToD0Pip_D0ToPimPimPipEpNu_PiLepRS"):
    """D0 -> Pi- Pi+ Pi- e+ nu, i.e. the only physical charge combo."""
    dz = _make_d0_part_reco_3pie_neutral()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d023pienu_ws(
        name="Hlt2Charm_DstpToD0Pip_D0ToPimPiPimEmNu_PiLepWS"):
    """D0 -> Pi- Pi+ Pi- e+ nu, i.e. the only physical charge combo."""
    dz = _make_d0_part_reco_3pie_neutral()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d023pienu_klep_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToPimPimPipEmNu_PiLepSS"):
    """D0 -> Pi- Pi+ Pi+ e+ nu SS channel."""
    dz = _make_d0_part_reco_3pie_charged()
    # Merger takes into account possible background, which is just oppositely tagged signal
    dstp_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    dstm_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    dst_to_d0pi = ParticleContainersMerger([dstp_to_d0pi, dstm_to_d0pi])
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d023pimunu_rs(
        name="Hlt2Charm_DstpToD0Pip_D0ToPimPimPipMupNu_PiLepRS"):
    """D0 -> pi- pi+ pi- mu+ nu."""
    dz = _make_d0_part_reco_3pimu_neutral()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d023pimunu_klep_ws(
        name="Hlt2Charm_DstpToD0Pip_D0ToPimPipPiPMumNu_PiLepWS"):
    """D0 -> K- K+ K+ mu- nu SS channel."""
    dz = _make_d0_part_reco_3pimu_neutral()
    dst_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])


@register_line_builder(all_lines)
def dst_to_d0pi_d023pimunu_klep_ss(
        name="Hlt2Charm_DstpToD0Pip_D0ToPimPimPipMumNu_PiLepSS"):
    """D0 -> K- K+ K+ mu- nu SS channel."""
    dz = _make_d0_part_reco_3pimu_charged()
    # Merger takes into account possible background, which is just oppositely tagged signal
    dstp_to_d0pi = _build_my_dstar(dz, "[D*(2010)+ -> D0 pi+]cc")
    dstm_to_d0pi = _build_my_dstar(dz, "[D*(2010)- -> D0 pi-]cc")
    dst_to_d0pi = ParticleContainersMerger([dstp_to_d0pi, dstm_to_d0pi])
    return Hlt2Line(name=name, algs=charm_prefilters() + [dz, dst_to_d0pi])
