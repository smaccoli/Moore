###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of Rare Charm HLT2 lines.


Final states that are built are:

--------------------
two-body lines:
--------------------

D*+ -> D0(-> mu+ mu-) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToMumMup]
D*+ -> D0(-> mu+ e-) pi+ cc and D*+ -> (D0 -> mu- e+) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToMumpEpm]
D*+ -> D0(-> e+ e-) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToEmEp]
D*+ -> D0(-> p+ e-) pi+ cc and D*+ -> D0(-> p- e+) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToPmpEpm]
D*+ -> D0(-> p+ mu-) pi+ cc and D*+ -> D0(-> p- mu+) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToPmpMupm]
D*+ -> D0(-> tau- e+ ) pi+ cc and D*+ -> D0(-> tau+ e-) pi+ cc, [Hlt2Charm_DstpToD0Pip_D0ToTaupmEmp_TaumToPimPimPipX]

control lines

D*+ -> D0(-> pi- pi+) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToPimPip_RareCharmControl]
D*+ -> D0(-> K- pi+ ) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToKmPip_RareCharmControl]
D*+ -> D0(-> K- mu+ ) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToKmpMupmX_RareCharmControl]

--------------------
three-body lines:
--------------------

D(s)+ -> pi+ mu+ mu- cc (OS) [Hlt2Charm_DpDspToPipMumMup]
D(s)+ -> pi- mu+ mu+ cc (SS) [Hlt2Charm_DpDspToPimMupMup]

D(s)+ -> pi+ e+ e- cc (OS) [Hlt2Charm_DpDspToPipEmEp]
D(s)+ -> pi- e+ e+ cc (SS) [Hlt2Charm_DpDspToPimEpEp]

D(s)+ -> K+ mu+ mu- cc (OS) [Hlt2Charm_DpDspToKpMumMup]
D(s)+ -> K- mu+ mu+ cc (SS) [Hlt2Charm_DpDspToKmMupMup]

D(s)+ -> K+ e+ e- cc (OS) [Hlt2Charm_DpDspToKpEmEp]
D(s)+ -> K- e+ e+ cc (SS) [Hlt2Charm_DpDspToKmEpEp]

D(s)+ -> pi+ mu+ e- cc (OS) [Hlt2Charm_DpDspToPipMupEm]
D(s)+ -> pi+ e+ mu- cc (OS) [Hlt2Charm_DpDspToPipMumEp]
D(s)+ -> pi- mu+ e+ cc (SS) [Hlt2Charm_DpDspToPimMupEp]

D(s)+ -> K+ mu+ e- cc (OS) [Hlt2Charm_DpDspToKpMupEm]
D(s)+ -> K+ e+ mu- cc (OS) [Hlt2Charm_DpDspToKpMumEp]
D(s)+ -> K- mu+ e+ cc (SS) [Hlt2Charm_DpDspToKmMupEp]

control lines

D(s)+ -> pi+ mu+ mu+ cc (WS) [Hlt2Charm_DpDspToPipMupMup_RareCharmControl]
D(s)+ -> pi+ e+ e+ cc (WS) [Hlt2Charm_DpDspToPipEpEp_RareCharmControl]
D(s)+ -> K+ mu+ mu+ cc (WS) [Hlt2Charm_DpDspToKpMupMup_RareCharmControl]
D(s)+ -> K+ e+ e+ cc (WS) [Hlt2Charm_DpDspToKpEpEp_RareCharmControl]
D(s)+ -> pi+ mu+ e+ cc (WS) [Hlt2Charm_DpDspToPipMupEp_RareCharmControl]
D(s)+ -> K+ mu+ e+ cc (WS) [Hlt2Charm_DpDspToKpMupEp_RareCharmControl

D(s)+ -> K+ pi+ pi- cc  [Hlt2Charm_DpDspToKpPimPip_RareCharmControl]
D(s)+ -> pi+ pi+ pi- cc [Hlt2Charm_DpDspToPipPimPip_RareCharmControl]
D(s)+ -> Ks K+ cc       [Hlt2Charm_DpDspToKsKp_LL_RareCharmControl]
D(s)+ -> Ks pi+ cc      [Hlt2Charm_DpDspToKsPip_LL_RareCharmControl]

--------------------
four-body lines:
--------------------

purely leptonic

D*+ ->D0 (-> mu+ mu- mu+ mu-) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToMumMupMumMup]
D*+ ->D0 (-> e+ e- e+ e-) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToEmEpEmEp]
D*+ ->D0 (-> mu+ mu- e+ e-) pi+ and D*+ ->D0 (-> mu+ mu+ e- e-) pi+ and D*+ ->D0 (-> mu- mu- e+ e+) pi+] cc  [Hlt2Charm_DstpToD0Pip_D0ToMuMuEE]

Muon lines

D*+ -> D0 (-> pi+ pi- mu+ mu-) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToPimPipMumMup]
D*+ -> D0 (-> K+ K- mu+ mu-) pi+ cc   [Hlt2Charm_DstpToD0Pip_D0ToKmKpMumMup]
D*+ -> D0 (-> K- pi+ mu+ mu-) pi+ cc (RS)  [Hlt2Charm_DstpToD0Pip_D0ToKmPipMumMup]
D*+ -> D0 (-> K+ pi- mu+ mu-) pi+ cc (WS)  [Hlt2Charm_DstpToD0Pip_D0ToKpPimMumMup]
D0 -> K- pi+ mu+ mu- cc [Hlt2Charm_D0ToKmPipMumMup_Untag]

D(s)+ -> K+ Ks mu+ mu- [Hlt2Charm_DpDspToKsKpMumMup_LL, Hlt2Charm_DpDspToKsKpMumMup_DD]
D(s)+ -> K+ pi0 mu+ mu- [Hlt2Charm_DpDspToPi0KpMumMup_M, Hlt2Charm_DpDspToPi0KpMumMup_R]
D(s)+ -> pi+ Ks mu+ mu- [Hlt2Charm_DpDspToKsPipMumMup_LL, Hlt2Charm_DpDspToKsPipMumMup_DD]

Electron lines

D*+ -> D0( -> pi+ pi- e+ e-) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToPimPipEmEp]
D*+ -> D0( -> K+ K- e+ e-) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToKmKpEmEp]
D*+ -> D0( -> K- pi+ e+ e-) pi+ cc (RS) [Hlt2Charm_DstpToD0Pip_D0ToKmPipEmEp]
D*+ -> (D0 -> K+ pi- e+ e-) pi+ cc (WS) [Hlt2Charm_DstpToD0Pip_D0ToKpPimEmEp]
D0 -> K- pi+ e+ e- cc [Hlt2Charm_D0ToKmPipEmEp_Untag]

D(s)+ -> K+ Ks e+ e- [Hlt2Charm_DpDspToKsKpEmEp_LL, Hlt2Charm_DpDspToKsKpEmEp_DD]
D(s)+ -> K+ pi0 e+ e- [Hlt2Charm_DpDspToPi0KpEmEp_M, Hlt2Charm_DpDspToPi0KpEmEp_R]
D(s)+ -> pi+ Ks e+ e- [Hlt2Charm_DpDspToKsPipEmEp_LL, Hlt2Charm_DpDspToKsPipEmEp_DD]

LFV decays

D*+ -> D0 (-> pi- pi+ mu+ e-) pi+ cc, D*+ -> D0 (-> pi- pi+ mu- e+) pi+ cc (OS) [Hlt2Charm_DstpToD0Pip_D0ToPimPipMumpEpm]
D*+ -> D0 (-> K+ K- mu+ e-) pi+ cc, D*+ -> D0 (-> K+ K- mu- e+) pi+ cc (OS) [Hlt2Charm_DstpToD0Pip_D0ToKmKpMumpEpm]
D*+ -> D0 (-> K- pi+ mu+ e-) pi+ cc and D*+ -> D0 (-> K- pi+ mu- e+) pi+ cc (OS) [Hlt2Charm_DstpToD0Pip_D0ToKmPipMumpEpm]
D*+ -> D0 (-> K+ pi- mu+ e-) pi+ cc and  D*+ -> D0 (-> K+ pi- mu- e+) pi+ cc (OS) [Hlt2Charm_DstpToD0Pip_D0ToKpPimMumpEpm]

LNV decays

D*+ -> D0 (-> pi+ pi+ mu- mu-) pi+ cc and D*+ -> D0 (-> pi- pi- mu+ mu+) pi+ cc (SS) [Hlt2Charm_DstpToD0Pip_D0ToPimpPimpMupmMupm]
D*+ -> D0 (-> K+ K+ mu- mu-) pi+ cc and D*+ -> D0 (-> K- K- mu+ mu+) pi+ cc (SS) [Hlt2Charm_DstpToD0Pip_D0ToKmpKmpMupmMupm]
D*+ -> D0 (-> K+ pi+ mu- mu-) pi+ cc and D*+ -> D0 (-> K- pi- mu+ mu+) pi+ cc (SS) [Hlt2Charm_DstpToD0Pip_D0ToKmpPimpMupmMupm]

D*+ -> D0 (-> pi+ pi+ e- e-) pi+ cc and D*+ -> D0 (-> pi- pi- e+ e+) pi+ cc (SS) [Hlt2Charm_DstpToD0Pip_D0ToPimpPimpEpmEpm]
D*+ -> D0 (-> K+ K+ e- e-) pi+ cc and D*+ -> D0 (-> K- K- e+ e+) pi+ cc (SS) [Hlt2Charm_DstpToD0Pip_D0ToKmpKmpEpmEpm]
D*+ -> D0 (-> K+ pi+ e- e-) pi+ cc and D*+ -> D0 (-> K- pi- e+ e+) pi+ cc (SS) [Hlt2Charm_DstpToD0Pip_D0ToKmpPimpEpmEpm]

LFV+ LNV decays

D*+ -> D0 (-> pi+ pi+ mu- e-) pi+ cc and D*+ -> D0 (-> pi- pi- mu+ e+) pi+ cc (SS) [Hlt2Charm_DstpToD0Pip_D0ToPimpPimpMupmEpm]
D*+ -> D0 (-> K+ K+ mu- e-) pi+ cc and D*+ -> D0 (-> K- K- mu+ e+) pi+ cc (SS) [Hlt2Charm_DstpToD0Pip_D0ToKmpKmpMupmEpm]
D*+ -> D0 (-> K+ pi+ mu- e-) pi+ cc and D*+ -> D0 (-> K- pi- mu+ e+) pi+ cc (SS) [Hlt2Charm_DstpToD0Pip_D0ToKmpPimpMupmEpm]

control lines

D*+ -> D0 (-> pi- pi- mu- mu-) pi+ cc and D*+ -> D0 (-> pi+ pi+ mu+ mu+) pi+  [Hlt2Charm_DstpToD0Pip_D0ToPimpPimpMumpMump_RareCharmControl]
D*+ -> D0 (-> K- K- mu- mu-) pi+ cc and D*+ -> D0 (-> K+ K+ mu+ mu+) pi+ cc  [Hlt2Charm_DstpToD0Pip_D0ToKmpKmpMumpMump_RareCharmControl]
D*+ -> D0 (-> K- pi- mu- mu-) pi+ cc and  D*+ -> D0 (-> K+ pi+ mu+ mu+) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToKmpPimpMumpMump_RareCharmControl]

D*+ -> D0 (-> pi- pi- e- e-) pi+ cc and D*+ -> D0 (-> pi+ pi+ e+ e+) pi+  [Hlt2Charm_DstpToD0Pip_D0ToPimpPimpEmpEmp_RareCharmControl]
D*+ -> D0 (-> K- K- e- e-) pi+ cc and D*+ -> D0 (-> K+ K+ e+ e+) pi+ cc  [Hlt2Charm_DstpToD0Pip_D0ToKmpKmpEmpEmp_RareCharmControl]
D*+ -> D0 (-> K- pi- e- e-) pi+ cc and  D*+ -> D0 (-> K+ pi+ e+ e+) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToKmpPimpEmpEmp_Control]

D*+ -> D0 (-> pi+ pi- pi+ pi-) pi+ cc [Hlt2Charm_DstpToD0Pip_D0ToPimPimPipPip_RareCharmControl]
D*+ -> D0 (-> K+ K- pi+ pi-) pi+ cc   [Hlt2Charm_DstpToD0Pip_D0ToKmKpPimPip_RareCharmControl]
D*+ -> D0 (-> K- pi+ pi+ pi-) pi+ cc (RS) [Hlt2Charm_DstpToD0Pip_D0ToKmPimPipPip_RareCharmControl]
D*+ -> D0 (-> K+ pi- pi+ pi-) pi+ cc (WS) [Hlt2Charm_DstpToD0Pip_D0ToKpPimPimPip_RareCharmControl]

--------------------
baryonic lines:
--------------------

Lambda_c+ -> p+ mu+ mu- cc [Hlt2Charm_LcpToPpMumMup]
Lambda_c+ -> p~- mu+ mu+ cc  [Hlt2Charm_LcpToPmMupMup]
Lambda_c+ -> mu+ e- p+ , Lambda_c+ ->p+ e+ mu- [Hlt2Charm_LcpToPpMumpEpm]
Lambda_c+ -> p+ e+ e- cc [Hlt2Charm_LcpToPpEmEp]
Lambda_c+ -> p~- e+ e+ cc [Hlt2Charm_LcpToPmEpEp]

Xi_c -> Lambda(pi p)mu+ mu- [Hlt2Charm_Xic0ToL0MumMup_LL, Hlt2Charm_Xic0ToL0MumMup_DD]
Xi_c -> Lambda(pi p)e+ e- [Hlt2Charm_Xic0ToL0EmEp_LL, Hlt2Charm_Xic0ToL0EmEp_DD]

control lines

Lambda_c+ -> p+ pi+ pi- cc [Hlt2Charm_LcpToPpPimPip_RareCharmControl]
Lambda_c+ -> Ks0 p+ cc [Hlt2Charm_LcpToKsPp_LL_RareCharmControl, Hlt2Charm_LcpToKsPp_DD_RareCharmControl]

--------------------
SL tagged lines lines:
--------------------
B- > D*+(->D0(->K- pi+ mu+ mu-) pi+) mu- cc (RS) [Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKmPipMumMup]
B- > D*+(->D0(->K+ pi- mu+ mu-) pi+) mu- cc (WS) [Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKpPimMumMup]
B- > D*+(->D0(->K- pi+ e+ e-) pi+) mu- cc (RS) [Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKmPipEmEp]
B- > D*+(->D0(->K+ pi- e+ e-) pi+) mu- cc (WS) [Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKpPimEmEp]


B- -> D+(->K+ mu+ mu-) mu- cc [Hlt2Charm_B2DpDspMumX_DpDspToKpMumMup]
B- -> D+(->K+ e+ e-) mu- cc [Hlt2Charm_B2DpDspMumX_DpDspToKpEmEp]

B- -> D+(->pi+ mu+ mu-) mu- cc [Hlt2Charm_B2DpDspMumX_DpDspToPipMumMup]
B- -> D+(->pi+ e+ e-) mu- cc [Hlt2Charm_B2DpDspMumX_DpDspToPipEmEp]

Lambda_b0 -> Lambda_c+( -> p mu+ mu-) mu-] cc [Hlt2Charm_B2LcpMum_LcpToPpMumMup]
Lambda_b0 -> Lambda_c+( ->p e+ e-) mu-] cc [Hlt2Charm_B2LcpMum_LcpToPpEmEp]

control lines

Lambda_b0 -> Lambda_c+ (->p+ Ks) mu-]cc [Hlt2Charm_B2LcpMumX_LcpToKsPp_LL_RareCharmControl, Hlt2Charm_B2LcpMumX_LcpToKsPp_DD_RareCharmControl]

B- -> D(s)+(->K+ Ks) mu- cc [Hlt2Charm_B2DpDspMumX_DpDspToKsKp_LL_RareCharmControl]
B- -> D(s)+(->pi+ Ks) mu- cc [Hlt2Charm_B2DpDspMumX_DpDspToKsPip_LL_RareCharmControl]

--------------------
"""
from GaudiKernel.SystemOfUnits import MeV

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from Hlt2Conf.algorithms import ParticleContainersMerger

from .prefilters import charm_prefilters
from . import rare_charm_makers as m
""" reduced mass ranges for control modes"""
_MASS_M_MIN_CONTROL_HHLL = 1745.0 * MeV
_MASS_M_MAX_CONTROL_HHLL = 1985.0 * MeV

_MASS_M_MIN_CONTROL_HHL = 1750.0 * MeV
_MASS_M_MAX_CONTROL_HHL = 2050.0 * MeV

_MASS_M_MIN_CONTROL_LL = 1795 * MeV
_MASS_M_MAX_CONTROL_LL = 1935 * MeV

_MASS_M_MIN_CONTROL_LAMBDAC = 2000 * MeV
_MASS_M_MAX_CONTROL_LAMBDAC = 2600 * MeV

###############################################################################
# Definition of the lines
###############################################################################

all_lines = {}
""" Define the tagged purely leptonic D02LL and D02PL lines """


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2mumu_line(name='Hlt2Charm_DstpToD0Pip_D0ToMumMup',
                                  prescale=1):

    muons = m.make_rarecharm_muons_forTwoBodyDecays()
    dzeros = m.make_rarecharm_d02ll_dzeros(muons, muons, 'D0 -> mu- mu+')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2mue_line(name='Hlt2Charm_DstpToD0Pip_D0ToMumpEpm',
                                 prescale=1):

    electrons = m.make_rarecharm_electrons_with_brem_forTwoBodyDecays()
    muons = m.make_rarecharm_muons_forTwoBodyDecays()
    dzeros_muminus_eplus = m.make_rarecharm_d02ll_dzeros(
        muons, electrons, 'D0 -> mu- e+')
    dzeros_muplus_eminus = m.make_rarecharm_d02ll_dzeros(
        muons, electrons, 'D0 -> mu+ e-')
    dzeros = ParticleContainersMerger(
        [dzeros_muminus_eplus, dzeros_muplus_eminus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2ee_line(name='Hlt2Charm_DstpToD0Pip_D0ToEmEp',
                                prescale=1):

    electrons = m.make_rarecharm_electrons_no_brem_forTwoBodyDecays()
    """m.make_rarecharm_d02ee_dzeros adds brems internally avoiding dublication of gammas """
    dzeros = m.make_rarecharm_d02ee_dzeros(
        particles=electrons, diElectonID="D0")
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2taue_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToTaupmEmp_TaumToPimPimPipX',
        prescale=1):

    electrons = m.make_rarecharm_electrons_with_brem_tightIPCut()
    pions = m.make_rarecharm_pions_from_tau()
    taus_plus = m.make_rarecharm_tau(pions, 'tau+ -> pi+ pi+ pi-')
    taus_minus = m.make_rarecharm_tau(pions, 'tau- -> pi- pi- pi+')
    dzeros_taup_em = m.make_rarecharm_d02taue_dzeros(taus_plus, electrons,
                                                     'D0 -> tau+ e-')
    dzeros_taum_ep = m.make_rarecharm_d02taue_dzeros(taus_minus, electrons,
                                                     'D0 -> tau- e+')
    dzeros = ParticleContainersMerger([dzeros_taup_em, dzeros_taum_ep])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pe_line(name='Hlt2Charm_DstpToD0Pip_D0ToPmpEpm',
                                prescale=1):

    electrons = m.make_rarecharm_electrons_with_brem_forD2pl()
    protons = m.make_rarecharm_protons_forD2pl()
    dzeros_eminus_pplus = m.make_rarecharm_d02ll_dzeros(
        electrons, protons, 'D0 -> e- p+')
    dzeros_eplus_pminus = m.make_rarecharm_d02ll_dzeros(
        electrons, protons, 'D0 -> e+ p~-')
    dzeros = ParticleContainersMerger(
        [dzeros_eminus_pplus, dzeros_eplus_pminus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, protons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pmu_line(name='Hlt2Charm_DstpToD0Pip_D0ToPmpMupm',
                                 prescale=1):

    muons = m.make_rarecharm_muons_forD2pl()
    protons = m.make_rarecharm_protons_forD2pl()
    dzeros_muminus_pplus = m.make_rarecharm_d02ll_dzeros(
        muons, protons, 'D0 -> mu- p+')
    dzeros_muplus_pminus = m.make_rarecharm_d02ll_dzeros(
        muons, protons, 'D0 -> mu+ p~-')
    dzeros = ParticleContainersMerger(
        [dzeros_muminus_pplus, dzeros_muplus_pminus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


""" Define the tagged hadronic D02HH control lines """


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipi_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPip_RareCharmControl',
        prescale=0.02):

    pions = m.make_rarecharm_pions_forTwoBodyDecays()
    dzeros = m.make_rarecharm_d02ll_dzeros(
        pions,
        pions,
        'D0 -> pi- pi+',
        m_min=_MASS_M_MIN_CONTROL_LL,
        m_max=_MASS_M_MAX_CONTROL_LL)
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pions, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpi_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPip_RareCharmControl',
        prescale=0.01):

    kaons = m.make_rarecharm_kaons_forTwoBodyDecays()
    pions = m.make_rarecharm_pions_forTwoBodyDecays()
    dzeros = m.make_rarecharm_d02ll_dzeros(
        kaons,
        pions,
        '[D0 -> K- pi+]cc',
        m_min=_MASS_M_MIN_CONTROL_LL,
        m_max=_MASS_M_MAX_CONTROL_LL)
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros, soft_pions, '[D*(2010)+ -> D0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kaons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2Kmu_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpMupmX_RareCharmControl',
        prescale=1):

    kaons = m.make_rarecharm_kaons_forTwoBodyDecays()
    muons = m.make_rarecharm_muons_forTwoBodyDecays()
    dzeros_kminus_muplus = m.make_rarecharm_d02ll_dzeros(
        muons,
        kaons,
        'D0 -> mu+ K-',
        m_min=_MASS_M_MIN_CONTROL_LL,
        m_max=_MASS_M_MAX_CONTROL_LL)
    dzeros_kplus_muminus = m.make_rarecharm_d02ll_dzeros(
        muons,
        kaons,
        'D0 -> mu- K+',
        m_min=_MASS_M_MIN_CONTROL_LL,
        m_max=_MASS_M_MAX_CONTROL_LL)
    dzeros = ParticleContainersMerger(
        [dzeros_kminus_muplus, dzeros_kplus_muminus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


"""tagged D->4l"""


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2mumumumu_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToMumMupMumMup', prescale=1):

    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, "J/psi(1S) -> mu+ mu-")
    dzeros = m.make_rarecharm_d02ll_dzeros(dimuons, dimuons,
                                           'D0 -> J/psi(1S) J/psi(1S)')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2eeee_line(name='Hlt2Charm_DstpToD0Pip_D0ToEmEpEmEp',
                                  prescale=1):

    electrons = m.make_rarecharm_electrons_no_brem()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    dzeros = m.make_rarecharm_d02ll_dzeros(dielectrons, dielectrons,
                                           'D0 -> J/psi(1S) J/psi(1S)')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2mumuee_line(name='Hlt2Charm_DstpToD0Pip_D0ToMuMuEE',
                                    prescale=1):

    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'phi(1020) -> mu+ mu-')
    dimuons_plus = m.make_rarecharm_TwoMuons(muons, 'phi(1020) -> mu+ mu+')
    dimuons_minus = m.make_rarecharm_TwoMuons(muons, 'phi(1020) -> mu- mu-')
    electrons = m.make_rarecharm_electrons_no_brem()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    electrons_plus = m.get_positive_particles(electrons)
    dielectrons_plus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_plus, diElectonID="J/psi(1S)", isOS=False)
    electrons_minus = m.get_negative_particles(electrons)
    dielectrons_minus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_minus, diElectonID="J/psi(1S)", isOS=False)
    dzeros_pm = m.make_rarecharm_d02ll_dzeros(dimuons, dielectrons,
                                              'D0 -> phi(1020) J/psi(1S)')
    dzeros_epp_mumm = m.make_rarecharm_d02ll_dzeros(
        dimuons_minus, dielectrons_plus, 'D0 -> phi(1020) J/psi(1S)')
    dzeros_emm_mupp = m.make_rarecharm_d02ll_dzeros(
        dimuons_plus, dielectrons_minus, 'D0 -> phi(1020) J/psi(1S)')
    dzeros = ParticleContainersMerger(
        [dzeros_pm, dzeros_epp_mumm, dzeros_emm_mupp])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


"""three body lines """
""" Instantiations, D->pimumu"""


@register_line_builder(all_lines)
@configurable
def d2pimumuos_line(name='Hlt2Charm_DpDspToPipMumMup', prescale=1):
    pions = m.make_rarecharm_pions_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(dimuons, pions, 'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds(dimuons, pions, 'D- -> J/psi(1S) pi-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dimuons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def d2pimumuss_line(name='Hlt2Charm_DpDspToPimMupMup', prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons_plus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu+')
    dimuons_minus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu- mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(dimuons_plus, pions,
                                        'D+ -> J/psi(1S) pi-')
    ds_minus = m.make_rarecharm_d2hll_ds(dimuons_minus, pions,
                                         'D- -> J/psi(1S) pi+')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2pimumuwscontrol_line(name='Hlt2Charm_DpDspToPipMupMup_RareCharmControl',
                           prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons_plus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu+')
    dimuons_minus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu- mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(dimuons_plus, pions,
                                        'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds(dimuons_minus, pions,
                                         'D- -> J/psi(1S) pi-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" Instantiations, D->Kmumu """


@register_line_builder(all_lines)
@configurable
def d2kmumuos_line(name='Hlt2Charm_DpDspToKpMumMup', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(dimuons, kaons, 'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds(dimuons, kaons, 'D- -> J/psi(1S) K-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def d2kmumuss_line(name='Hlt2Charm_DpDspToKmMupMup', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons_plus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu+')
    dimuons_minus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu- mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(dimuons_plus, kaons,
                                        "D+ -> J/psi(1S) K-")
    ds_minus = m.make_rarecharm_d2hll_ds(dimuons_minus, kaons,
                                         "D- -> J/psi(1S) K+")
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kmumuwscontrol_line(name='Hlt2Charm_DpDspToKpMupMup_RareCharmControl',
                          prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons_plus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu+')
    dimuons_minus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu- mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(dimuons_plus, kaons,
                                        'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds(dimuons_minus, kaons,
                                         'D- -> J/psi(1S) K-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" Instantiations, D->PiEE """


@register_line_builder(all_lines)
@configurable
def d2pieeos_line(name='Hlt2Charm_DpDspToPipEmEp', prescale=1):
    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds_plus = m.make_rarecharm_d2hll_ds(dielectrons, pions,
                                        'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds(dielectrons, pions,
                                         'D- -> J/psi(1S) pi-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dielectrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def d2pieess_line(name='Hlt2Charm_DpDspToPimEpEp', prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    electrons_plus = m.get_positive_particles(electrons)
    dielectrons_plus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_plus, diElectonID="J/psi(1S)", isOS=False)
    electrons_minus = m.get_negative_particles(electrons)
    dielectrons_minus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_minus, diElectonID="J/psi(1S)", isOS=False)
    ds_plus = m.make_rarecharm_d2hll_ds(dielectrons_plus, pions,
                                        'D+ -> J/psi(1S) pi-')
    ds_minus = m.make_rarecharm_d2hll_ds(dielectrons_minus, pions,
                                         'D- -> J/psi(1S) pi+')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2pieewscontrol_line(name='Hlt2Charm_DpDspToPipEpEp_RareCharmControl',
                         prescale=1):
    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    electrons_plus = m.get_positive_particles(electrons)
    dielectrons_plus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_plus, diElectonID="J/psi(1S)", isOS=False)
    electrons_minus = m.get_negative_particles(electrons)
    dielectrons_minus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_minus, diElectonID="J/psi(1S)", isOS=False)
    ds_plus = m.make_rarecharm_d2hll_ds(dielectrons_plus, pions,
                                        'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds(dielectrons_minus, pions,
                                         'D- -> J/psi(1S) pi-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" Instantiations, D->KEE """


@register_line_builder(all_lines)
@configurable
def d2keeos_line(name='Hlt2Charm_DpDspToKpEmEp', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds_plus = m.make_rarecharm_d2hll_ds(dielectrons, kaons,
                                        'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds(dielectrons, kaons,
                                         'D- -> J/psi(1S) K-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dielectrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def d2keess_line(name='Hlt2Charm_DpDspToKmEpEp', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    electrons_plus = m.get_positive_particles(electrons)
    dielectrons_plus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_plus, diElectonID="J/psi(1S)", isOS=False)
    electrons_minus = m.get_negative_particles(electrons)
    dielectrons_minus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_minus, diElectonID="J/psi(1S)", isOS=False)
    ds_plus = m.make_rarecharm_d2hll_ds(dielectrons_plus, kaons,
                                        'D+ -> J/psi(1S) K-')
    ds_minus = m.make_rarecharm_d2hll_ds(dielectrons_minus, kaons,
                                         'D- -> J/psi(1S) K+')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2keewscontrol_line(name='Hlt2Charm_DpDspToKpEpEp_RareCharmControl',
                        prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    electrons_plus = m.get_positive_particles(electrons)
    dielectrons_plus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_plus, diElectonID="J/psi(1S)", isOS=False)
    electrons_minus = m.get_negative_particles(electrons)
    dielectrons_minus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_minus, diElectonID="J/psi(1S)", isOS=False)
    ds_plus = m.make_rarecharm_d2hll_ds(dielectrons_plus, kaons,
                                        'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds(dielectrons_minus, kaons,
                                         'D- -> J/psi(1S) K-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" Instantiations, D->PiMuE """


@register_line_builder(all_lines)
@configurable
def d2pimueos_line(name='Hlt2Charm_DpDspToPipMupEm', prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_with_brem_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    muplus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e- mu+')
    muminus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e+ mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(muplus_eminus, pions,
                                        'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds(muminus_eplus, pions,
                                         'D- -> J/psi(1S) pi-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2piemuos_line(name='Hlt2Charm_DpDspToPipMumEp', prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_with_brem_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    eplus_muminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e+ mu-')
    eminus_muplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e- mu+')
    ds_plus = m.make_rarecharm_d2hll_ds(eplus_muminus, pions,
                                        'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds(eminus_muplus, pions,
                                         'D- -> J/psi(1S) pi-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2pimuess_line(name='Hlt2Charm_DpDspToPimMupEp', prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_with_brem_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    muplus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                 'J/psi(1S) -> e+ mu+')
    muminus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                   'J/psi(1S) -> e- mu+')
    ds_plus = m.make_rarecharm_d2hll_ds(muplus_eplus, pions,
                                        'D+ -> J/psi(1S) pi-')
    ds_minus = m.make_rarecharm_d2hll_ds(muminus_eminus, pions,
                                         'D- -> J/psi(1S) pi+')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2pimuewscontrol_line(name='Hlt2Charm_DpDspToPipMupEp_RareCharmControl',
                          prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_with_brem_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    muplus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                 'J/psi(1S) -> e+ mu+')
    muminus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                   'J/psi(1S) -> e- mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(muplus_eplus, pions,
                                        'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds(muminus_eminus, pions,
                                         'D- -> J/psi(1S) pi-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" Instantiations, D->KMuE"""


@register_line_builder(all_lines)
@configurable
def d2kmueos_line(name='Hlt2Charm_DpDspToKpMupEm', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    electrons = m.make_rarecharm_electrons_with_brem_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    muplus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e- mu+')
    muminus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e+ mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(muplus_eminus, kaons,
                                        'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds(muminus_eplus, kaons,
                                         'D- -> J/psi(1S) K-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kemuos_line(name='Hlt2Charm_DpDspToKpMumEp', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    electrons = m.make_rarecharm_electrons_with_brem_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    eplus_muminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e+ mu-')
    eminus_muplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e- mu+')
    ds_plus = m.make_rarecharm_d2hll_ds(eplus_muminus, kaons,
                                        'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds(eminus_muplus, kaons,
                                         'D- -> J/psi(1S) K-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kmuess_line(name='Hlt2Charm_DpDspToKmMupEp', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    electrons = m.make_rarecharm_electrons_with_brem_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    muplus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                 'J/psi(1S) -> e+ mu+')
    muminus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                   'J/psi(1S) -> e- mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(muplus_eplus, kaons,
                                        'D+ -> J/psi(1S) K-')
    ds_minus = m.make_rarecharm_d2hll_ds(muminus_eminus, kaons,
                                         'D- -> J/psi(1S) K+')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kmuewscontrol_line(name='Hlt2Charm_DpDspToKpMupEp_RareCharmControl',
                         prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    electrons = m.make_rarecharm_electrons_with_brem_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    muplus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                 'J/psi(1S) -> e+ mu+')
    muminus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                   'J/psi(1S) -> e- mu-')
    ds_plus = m.make_rarecharm_d2hll_ds(muplus_eplus, kaons,
                                        'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds(muminus_eminus, kaons,
                                         'D- -> J/psi(1S) K-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" hadronic D->hpipi control lines """


@register_line_builder(all_lines)
@configurable
def d2kpipioscontrol_line(name='Hlt2Charm_DpDspToKpPimPip_RareCharmControl',
                          prescale=0.005):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    noPID_pions = m.make_rarecharm_noPID_pions_tightIPCut()
    dipions = m.make_rarecharm_TwoPions(noPID_pions, noPID_pions,
                                        'J/psi(1S) -> pi+ pi-')
    ds_plus = m.make_rarecharm_d2hll_ds(dipions, kaons, 'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds(dipions, kaons, 'D- -> J/psi(1S) K-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dipions, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def d2pipipioscontrol_line(name='Hlt2Charm_DpDspToPipPimPip_RareCharmControl',
                           prescale=0.01):

    pions = m.make_rarecharm_pions_tightIPCut()
    noPID_pions = m.make_rarecharm_noPID_pions_tightIPCut()
    dipions = m.make_rarecharm_TwoPions(noPID_pions, noPID_pions,
                                        'J/psi(1S) -> pi+ pi-')
    ds_plus = m.make_rarecharm_d2hll_ds(dipions, pions, 'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds(dipions, pions, 'D- -> J/psi(1S) pi-')
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dipions, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def d2kskcontrol_line(name='Hlt2Charm_DpDspToKsKp_LL_RareCharmControl',
                      prescale=0.10):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    ds_plus = m.make_rarecharm_d2hll_ds(
        ksll,
        kaons,
        'D+ -> KS0 K+',
        m_min=_MASS_M_MIN_CONTROL_HHL,
        m_max=_MASS_M_MAX_CONTROL_HHL)
    ds_minus = m.make_rarecharm_d2hll_ds(
        ksll,
        kaons,
        'D- -> KS0 K-',
        m_min=_MASS_M_MIN_CONTROL_HHL,
        m_max=_MASS_M_MAX_CONTROL_HHL)
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kaons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def d2kspicontrol_line(name='Hlt2Charm_DpDspToKsPip_LL_RareCharmControl',
                       prescale=0.10):

    pions = m.make_rarecharm_pions_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    ds_plus = m.make_rarecharm_d2hll_ds(
        ksll,
        pions,
        'D+ -> KS0 pi+',
        m_min=_MASS_M_MIN_CONTROL_HHL,
        m_max=_MASS_M_MAX_CONTROL_HHL)
    ds_minus = m.make_rarecharm_d2hll_ds(
        ksll,
        pions,
        'D- -> KS0 pi-',
        m_min=_MASS_M_MIN_CONTROL_HHL,
        m_max=_MASS_M_MAX_CONTROL_HHL)
    ds = ParticleContainersMerger([ds_plus, ds_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pions, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" Define the tagged D02HHMUMU lines """
""" D0 -> pipill lines"""


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipimumuos_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPipMumMup', prescale=1):

    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    dzeros = m.make_rarecharm_d02hhmumu_dzeros(dimuons, pions, pions,
                                               'D0 -> J/psi(1S) pi- pi+')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dimuons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipieeos_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPipEmEp', prescale=1):

    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_no_brem()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    dzeros = m.make_rarecharm_d02hhel_dzeros(dielectrons, pions, pions,
                                             'D0 -> J/psi(1S) pi- pi+')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipimumuss_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimpPimpMupmMupm', prescale=1):

    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    piplus_mumimus = m.make_rarecharm_displaced_HadronLepton(
        muons, pions, 'KS0 -> mu- pi+')
    dzeros_pip_mum = m.make_rarecharm_d02hhll_sslepton_dzeros(
        piplus_mumimus, piplus_mumimus, 'D0 -> KS0 KS0')
    #D->pi+pi+mu-mu-
    piminus_mumplus = m.make_rarecharm_displaced_HadronLepton(
        muons, pions, 'KS0 -> mu+ pi-')
    dzeros_pim_mup = m.make_rarecharm_d02hhll_sslepton_dzeros(
        piminus_mumplus, piminus_mumplus, 'D0 -> KS0 KS0')
    #D->pi-pi-mu+mu+
    dzeros = ParticleContainersMerger([dzeros_pip_mum, dzeros_pim_mup])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipieess_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimpPimpEpmEpm', prescale=1):

    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_with_brem()
    piplus_eminus = m.make_rarecharm_displaced_HadronLepton(
        electrons, pions, 'KS0 -> e- pi+')
    dzeros_pip_em = m.make_rarecharm_d02hhll_sslepton_dzeros(
        piplus_eminus, piplus_eminus, 'D0 -> KS0 KS0')
    #D->pi+pi+e-e-
    piminus_eplus = m.make_rarecharm_displaced_HadronLepton(
        electrons, pions, 'KS0 -> e+ pi-')
    dzeros_pim_ep = m.make_rarecharm_d02hhll_sslepton_dzeros(
        piminus_eplus, piminus_eplus, 'D0 -> KS0 KS0')
    #D->pi-pi-e+e+
    dzeros = ParticleContainersMerger([dzeros_pip_em, dzeros_pim_ep])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipimueos_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPipMumpEpm', prescale=1):

    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_with_brem()
    muons = m.make_rarecharm_muons()
    muplus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e- mu+')
    dzeros_mup_em = m.make_rarecharm_d02hhel_dzeros(
        muplus_eminus, pions, pions, 'D0 -> J/psi(1S) pi+ pi-')
    muminus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e+ mu-')
    dzeros_mum_ep = m.make_rarecharm_d02hhel_dzeros(
        muminus_eplus, pions, pions, 'D0 -> J/psi(1S) pi+ pi-')
    dzeros = ParticleContainersMerger([dzeros_mup_em, dzeros_mum_ep])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipimuess_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimpPimpMupmEpm', prescale=1):

    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_with_brem()
    muons = m.make_rarecharm_muons()
    piplus_emimus = m.make_rarecharm_displaced_HadronLepton(
        electrons, pions, 'KS0 -> e- pi+')
    piplus_mumimus = m.make_rarecharm_displaced_HadronLepton(
        muons, pions, 'KL0 -> mu- pi+')
    dzeros_pip_em_pip_mum = m.make_rarecharm_d02hhll_sslepton_dzeros(
        piplus_emimus, piplus_mumimus, 'D0 -> KS0 KL0')
    piminus_eplus = m.make_rarecharm_displaced_HadronLepton(
        electrons, pions, 'KS0 -> e+ pi-')
    piminus_muplus = m.make_rarecharm_displaced_HadronLepton(
        muons, pions, 'KL0 -> mu+ pi-')
    dzeros_pim_ep_pim_mup = m.make_rarecharm_d02hhll_sslepton_dzeros(
        piminus_eplus, piminus_muplus, 'D0 -> KS0 KL0')
    dzeros = ParticleContainersMerger(
        [dzeros_pip_em_pip_mum, dzeros_pim_ep_pim_mup])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipimumusscontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimpPimpMumpMump_RareCharmControl',
        prescale=1):

    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    dimuons_plus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu+')
    dzeros_plus = m.make_rarecharm_d02hhmumu_dzeros(dimuons_plus, pions, pions,
                                                    'D0 -> J/psi(1S) pi+ pi+')
    dimuons_minus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu- mu-')
    dzeros_minus = m.make_rarecharm_d02hhmumu_dzeros(
        dimuons_minus, pions, pions, 'D0 -> J/psi(1S) pi- pi-')
    dzeros = ParticleContainersMerger([dzeros_plus, dzeros_minus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipieesscontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimpPimpEmpEmp_RareCharmControl',
        prescale=1):

    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_no_brem()
    electrons_plus = m.get_positive_particles(electrons)
    dielectrons_plus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_plus, diElectonID="J/psi(1S)", isOS=False)
    dzeros_plus = m.make_rarecharm_d02hhel_dzeros(
        dielectrons_plus, pions, pions, 'D0 -> J/psi(1S) pi+ pi+')
    electrons_minus = m.get_negative_particles(electrons)
    dielectrons_minus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_minus, diElectonID="J/psi(1S)", isOS=False)
    dzeros_minus = m.make_rarecharm_d02hhel_dzeros(
        dielectrons_minus, pions, pions, 'D0 -> J/psi(1S) pi- pi-')
    dzeros = ParticleContainersMerger([dzeros_plus, dzeros_minus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" D0 -> KKll lines"""


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkmumuos_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmKpMumMup', prescale=1):
    kaons = m.make_rarecharm_kaons()
    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    dzeros = m.make_rarecharm_d02hhmumu_dzeros(dimuons, kaons, kaons,
                                               'D0 -> J/psi(1S) K- K+')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dimuons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkeeos_line(name='Hlt2Charm_DstpToD0Pip_D0ToKmKpEmEp',
                                    prescale=1):

    kaons = m.make_rarecharm_kaons()
    electrons = m.make_rarecharm_electrons_no_brem()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    dzeros = m.make_rarecharm_d02hhel_dzeros(dielectrons, kaons, kaons,
                                             "D0 -> J/psi(1S) K- K+")
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dielectrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkmumuss_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpKmpMupmMupm', prescale=1):

    kaons = m.make_rarecharm_kaons()
    muons = m.make_rarecharm_muons()
    kplus_mumimus = m.make_rarecharm_displaced_HadronLepton(
        muons, kaons, 'KS0 -> mu- K+')
    dzeros_kp_mum = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kplus_mumimus, kplus_mumimus, 'D0 -> KS0 KS0')
    #D->K+K+mu-mu-
    kminus_mumplus = m.make_rarecharm_displaced_HadronLepton(
        muons, kaons, 'KS0 -> mu+ K-')
    dzeros_km_mup = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kminus_mumplus, kminus_mumplus, 'D0 -> KS0 KS0')
    #D->K-K-mu+mu+
    dzeros = ParticleContainersMerger([dzeros_kp_mum, dzeros_km_mup])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkeess_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpKmpEpmEpm', prescale=1):

    kaons = m.make_rarecharm_kaons()
    electrons = m.make_rarecharm_electrons_with_brem()
    kplus_eminus = m.make_rarecharm_displaced_HadronLepton(
        electrons, kaons, 'KS0 -> e- K+')
    dzeros_kp_em = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kplus_eminus, kplus_eminus, 'D0 -> KS0 KS0')
    #D->K+K+e-e-
    kminus_eplus = m.make_rarecharm_displaced_HadronLepton(
        electrons, kaons, 'KS0 -> e+ K-')
    dzeros_km_ep = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kminus_eplus, kminus_eplus, 'D0 -> KS0 KS0')
    #D->K-K-e+e+
    dzeros = ParticleContainersMerger([dzeros_kp_em, dzeros_km_ep])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkmueos_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmKpMumpEpm', prescale=1):

    kaons = m.make_rarecharm_kaons()
    electrons = m.make_rarecharm_electrons_with_brem()
    muons = m.make_rarecharm_muons()
    muplus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e- mu+')
    dzeros_mup_em = m.make_rarecharm_d02hhel_dzeros(
        muplus_eminus, kaons, kaons, 'D0 -> J/psi(1S) K+ K-')
    muminus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e+ mu-')
    dzeros_mum_ep = m.make_rarecharm_d02hhel_dzeros(
        muminus_eplus, kaons, kaons, 'D0 -> J/psi(1S) K+ K-')
    dzeros = ParticleContainersMerger([dzeros_mup_em, dzeros_mum_ep])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, kaons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkmumusscontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpKmpMumpMump_RareCharmControl',
        prescale=1):

    kaons = m.make_rarecharm_kaons()
    muons = m.make_rarecharm_muons()
    dimuons_plus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu+')
    dzeros_plus = m.make_rarecharm_d02hhmumu_dzeros(dimuons_plus, kaons, kaons,
                                                    'D0 -> J/psi(1S) K+ K+')
    dimuons_minus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu- mu-')
    dzeros_minus = m.make_rarecharm_d02hhmumu_dzeros(
        dimuons_minus, kaons, kaons, 'D0 -> J/psi(1S) K- K-')
    dzeros = ParticleContainersMerger([dzeros_plus, dzeros_minus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkeesscontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpKmpEmpEmp_RareCharmControl',
        prescale=1):

    kaons = m.make_rarecharm_kaons()
    electrons = m.make_rarecharm_electrons_no_brem()
    electrons_plus = m.get_positive_particles(electrons)
    dielectrons_plus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_plus, diElectonID="J/psi(1S)", isOS=False)
    dzeros_plus = m.make_rarecharm_d02hhel_dzeros(
        dielectrons_plus, kaons, kaons, 'D0 -> J/psi(1S) K+ K+')
    electrons_minus = m.get_negative_particles(electrons)
    dielectrons_minus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_minus, diElectonID="J/psi(1S)", isOS=False)
    dzeros_minus = m.make_rarecharm_d02hhel_dzeros(
        dielectrons_minus, kaons, kaons, 'D0 -> J/psi(1S) K- K-')
    dzeros = ParticleContainersMerger([dzeros_plus, dzeros_minus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkmuess_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpKmpMupmEpm', prescale=1):

    kaons = m.make_rarecharm_kaons()
    electrons = m.make_rarecharm_electrons_with_brem()
    muons = m.make_rarecharm_muons()
    kplus_emimus = m.make_rarecharm_displaced_HadronLepton(
        electrons, kaons, 'KS0 -> e- K+')
    kplus_mumimus = m.make_rarecharm_displaced_HadronLepton(
        muons, kaons, 'KL0 -> mu- K+')
    dzeros_kp_em_kp_mum = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kplus_emimus, kplus_mumimus, 'D0 -> KS0 KL0')
    kminus_eplus = m.make_rarecharm_displaced_HadronLepton(
        electrons, kaons, 'KS0 -> e+ K-')
    kminus_muplus = m.make_rarecharm_displaced_HadronLepton(
        muons, kaons, 'KL0 -> mu+ K-')
    dzeros_km_ep_km_mup = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kminus_eplus, kminus_muplus, 'D0 -> KS0 KL0')
    dzeros = ParticleContainersMerger(
        [dzeros_kp_em_kp_mum, dzeros_km_ep_km_mup])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, muons, kaons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


""" D0 -> Kpill lines"""


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimumurs_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPipMumMup', prescale=1):
    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    dzeros_rs = m.make_rarecharm_d02hhmumu_dzeros(
        dimuons, kaons, pions, '[D0 -> J/psi(1S) K- pi+]cc')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_rs, soft_pions, '[D*(2010)+ -> D0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dimuons, dzeros_rs, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimumuws_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKpPimMumMup', prescale=1):
    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    dzeros_ws = m.make_rarecharm_d02hhmumu_dzeros(
        dimuons, kaons, pions, '[D0 -> J/psi(1S) K+ pi-]cc')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_ws, soft_pions, '[D*(2010)+ -> D0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dimuons, dzeros_ws, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dzero2kpimumuosuntagged_line(name='Hlt2Charm_D0ToKmPipMumMup_Untag',
                                 prescale=1):
    kaons = m.make_rarecharm_kaons_tightIPCut()
    pions = m.make_rarecharm_pions_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    dzeros = m.make_rarecharm_d02hhmumu_dzeros(dimuons, kaons, pions,
                                               '[D0 -> J/psi(1S) K- pi+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dimuons, dzeros],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpieers_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPipEmEp', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_no_brem()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    dzeros_rs = m.make_rarecharm_d02hhel_dzeros(dielectrons, kaons, pions,
                                                '[D0 -> J/psi(1S) K- pi+]cc')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_rs, soft_pions, '[D*(2010)+ -> D0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dielectrons, dzeros_rs, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpieews_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKpPimEmEp', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_no_brem()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    dzeros_ws = m.make_rarecharm_d02hhel_dzeros(dielectrons, kaons, pions,
                                                '[D0 -> J/psi(1S) K+ pi-]cc')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_ws, soft_pions, '[D*(2010)+ -> D0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dielectrons, dzeros_ws, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dzero2kpieeosuntagged_line(name='Hlt2Charm_D0ToKmPipEmEp_Untag',
                               prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    dzeros = m.make_rarecharm_d02hhel_dzeros(dielectrons, kaons, pions,
                                             '[D0 -> J/psi(1S) K- pi+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dielectrons, dzeros],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimumuss_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpPimpMupmMupm', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    kplus_mumimus = m.make_rarecharm_displaced_HadronLepton(
        muons, kaons, 'KS0 -> mu- K+')
    piplus_mumimus = m.make_rarecharm_displaced_HadronLepton(
        muons, pions, 'KL0 -> mu- pi+')
    dzeros_hp_mum = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kplus_mumimus, piplus_mumimus, 'D0 -> KS0 KL0')
    #D->K+pi+mu-mu-
    kminus_muplus = m.make_rarecharm_displaced_HadronLepton(
        muons, kaons, 'KS0 -> mu+ K-')
    piminus_muplus = m.make_rarecharm_displaced_HadronLepton(
        muons, pions, 'KL0 -> mu+ pi-')
    dzeros_hm_mup = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kminus_muplus, piminus_muplus, 'D0 -> KS0 KL0')
    #D->K-pi-mu+mu+
    dzeros = ParticleContainersMerger([dzeros_hp_mum, dzeros_hm_mup])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpieess_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpPimpEpmEpm', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_with_brem()
    kplus_eminus = m.make_rarecharm_displaced_HadronLepton(
        electrons, kaons, 'KS0 -> e- K+')
    piplus_eminus = m.make_rarecharm_displaced_HadronLepton(
        electrons, pions, 'KL0 -> e- pi+')
    dzeros_hp_em = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kplus_eminus, piplus_eminus, "D0 -> KS0 KL0")
    #D->K+pi+e-e-
    kminus_eplus = m.make_rarecharm_displaced_HadronLepton(
        electrons, kaons, 'KS0 -> e+ K-')
    piminus_eplus = m.make_rarecharm_displaced_HadronLepton(
        electrons, pions, 'KL0 -> e+ pi-')
    dzeros_hm_ep = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kminus_eplus, piminus_eplus, "D0 -> KS0 KL0")
    #D->K-pi-e+e+
    dzeros = ParticleContainersMerger([dzeros_hp_em, dzeros_hm_ep])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimuers_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPipMumpEpm', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_with_brem()
    muons = m.make_rarecharm_muons()
    muplus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e- mu+')
    dzeros_km_pip_mup_em = m.make_rarecharm_d02hhel_dzeros(
        muplus_eminus, kaons, pions, 'D0 -> J/psi(1S) K- pi+')
    muminus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e+ mu-')
    dzeros_km_pip_mum_ep = m.make_rarecharm_d02hhel_dzeros(
        muminus_eplus, kaons, pions, 'D0 -> J/psi(1S) K- pi+')
    dzeros = ParticleContainersMerger(
        [dzeros_km_pip_mup_em, dzeros_km_pip_mum_ep])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [electrons, muons, kaons, pions, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimuews_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKpPimMumpEpm', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_with_brem()
    muons = m.make_rarecharm_muons()
    muplus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e- mu+')
    dzeros_kp_pim_mup_em = m.make_rarecharm_d02hhel_dzeros(
        muplus_eminus, kaons, pions, 'D0 -> J/psi(1S) K+ pi-')
    muminus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e+ mu-')
    dzeros_kp_pim_mum_ep = m.make_rarecharm_d02hhel_dzeros(
        muminus_eplus, kaons, pions, 'D0 -> J/psi(1S) K+ pi-')
    dzeros = ParticleContainersMerger(
        [dzeros_kp_pim_mup_em, dzeros_kp_pim_mum_ep])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [electrons, muons, kaons, pions, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimumusscontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpPimpMumpMump_RareCharmControl',
        prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    dimuons_plus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu+')
    dzeros_plus = m.make_rarecharm_d02hhmumu_dzeros(dimuons_plus, kaons, pions,
                                                    'D0 -> J/psi(1S) K+ pi+')
    dimuons_minus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu- mu-')
    dzeros_minus = m.make_rarecharm_d02hhmumu_dzeros(
        dimuons_minus, kaons, pions, 'D0 -> J/psi(1S) K- pi-')
    dzeros = ParticleContainersMerger([dzeros_plus, dzeros_minus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kaons, pions, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimuess_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpPimpMupmEpm', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    electrons = m.make_rarecharm_electrons_with_brem()
    kplus_emimus = m.make_rarecharm_displaced_HadronLepton(
        electrons, kaons, 'KS0 -> e- K+')
    piplus_mumimus = m.make_rarecharm_displaced_HadronLepton(
        muons, pions, 'KL0 -> mu- pi+')
    dzeros_kp_em_pip_mum = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kplus_emimus, piplus_mumimus, 'D0 -> KS0 KL0')
    kminus_eplus = m.make_rarecharm_displaced_HadronLepton(
        electrons, kaons, 'KS0 -> e+ K-')
    piminus_muplus = m.make_rarecharm_displaced_HadronLepton(
        muons, pions, 'KL0 -> mu+ pi-')
    dzeros_km_ep_pim_mup = m.make_rarecharm_d02hhll_sslepton_dzeros(
        kminus_eplus, piminus_muplus, 'D0 -> KS0 KL0')
    dzeros = ParticleContainersMerger(
        [dzeros_kp_em_pip_mum, dzeros_km_ep_pim_mup])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kaons, pions, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpieesscontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmpPimpEmpEmp_Control', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    electrons = m.make_rarecharm_electrons_no_brem()
    electrons_plus = m.get_positive_particles(electrons)
    dielectrons_plus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_plus, diElectonID="J/psi(1S)", isOS=False)
    dzeros_plus = m.make_rarecharm_d02hhel_dzeros(
        dielectrons_plus, kaons, pions, 'D0 -> J/psi(1S) K+ pi+')
    electrons_minus = m.get_negative_particles(electrons)
    dielectrons_minus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_minus, diElectonID="J/psi(1S)", isOS=False)
    dzeros_minus = m.make_rarecharm_d02hhel_dzeros(
        dielectrons_minus, kaons, pions, 'D0 -> J/psi(1S) K- pi-')
    dzeros = ParticleContainersMerger([dzeros_plus, dzeros_minus])
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, kaons, pions, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" Define the tagged hadronic D02HHHH control lines """


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipipipicontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPimPipPip_RareCharmControl',
        prescale=0.02):

    pions = m.make_rarecharm_pions()
    noPID_pions = m.make_rarecharm_noPID_pions()
    dipions = m.make_rarecharm_TwoPions(noPID_pions, noPID_pions,
                                        'J/psi(1S) -> pi+ pi-')
    dzeros = m.make_rarecharm_d02hhmumu_dzeros(
        dipions,
        pions,
        pions,
        'D0 -> J/psi(1S) pi- pi+',
        m_min=_MASS_M_MIN_CONTROL_HHLL,
        m_max=_MASS_M_MAX_CONTROL_HHLL)
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pions, dipions, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkpipicontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmKpPimPip_RareCharmControl',
        prescale=0.02):

    kaons = m.make_rarecharm_kaons()
    noPID_pions = m.make_rarecharm_noPID_pions()
    dipions = m.make_rarecharm_TwoPions(noPID_pions, noPID_pions,
                                        'J/psi(1S) -> pi+ pi-')
    dzeros = m.make_rarecharm_d02hhmumu_dzeros(
        dipions,
        kaons,
        kaons,
        'D0 -> J/psi(1S) K- K+',
        m_min=_MASS_M_MIN_CONTROL_HHLL,
        m_max=_MASS_M_MAX_CONTROL_HHLL)
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars_plus = m.make_dstars(dzeros, soft_pions, 'D*(2010)+ -> D0 pi+')
    dstars_minus = m.make_dstars(dzeros, soft_pions, 'D*(2010)- -> D0 pi-')
    dstars = ParticleContainersMerger([dstars_plus, dstars_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kaons, dipions, dzeros, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpipipirscontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPimPipPip_RareCharmControl',
        prescale=0.01):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    noPID_pions = m.make_rarecharm_noPID_pions()
    dipions = m.make_rarecharm_TwoPions(noPID_pions, noPID_pions,
                                        'J/psi(1S) -> pi+ pi-')
    dzeros_rs = m.make_rarecharm_d02hhmumu_dzeros(
        dipions,
        kaons,
        pions,
        '[D0 -> J/psi(1S) K- pi+]cc',
        m_min=_MASS_M_MIN_CONTROL_HHLL,
        m_max=_MASS_M_MAX_CONTROL_HHLL)
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_rs, soft_pions, '[D*(2010)+ -> D0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kaons, dipions, dzeros_rs, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpipipiwscontrol_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKpPimPimPip_RareCharmControl',
        prescale=0.01):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    noPID_pions = m.make_rarecharm_noPID_pions()
    dipions = m.make_rarecharm_TwoPions(noPID_pions, noPID_pions,
                                        'J/psi(1S) -> pi+ pi-')
    dzeros_ws = m.make_rarecharm_d02hhmumu_dzeros(
        dipions,
        kaons,
        pions,
        '[D0 -> J/psi(1S) K+ pi-]cc',
        m_min=_MASS_M_MIN_CONTROL_HHLL,
        m_max=_MASS_M_MAX_CONTROL_HHLL)
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_ws, soft_pions, '[D*(2010)+ -> D0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kaons, dipions, dzeros_ws, dstars],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


""" (untagged) four body D+ decays with neutrals  """


@register_line_builder(all_lines)
@configurable
def d2kksllmumuos_line(name='Hlt2Charm_DpDspToKsKpMumMup_LL', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    ds = m.make_rarecharm_d2hh0ll_ds(dimuons, ksll, kaons,
                                     '[D+ -> J/psi(1S) KS0 K+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kaons, dimuons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kksddmumuos_line(name='Hlt2Charm_DpDspToKsKpMumMup_DD', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    down_pions = m.filter_down_pions_forKs()
    ksdd = m.make_dd_ks(down_pions, down_pions)
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    ds = m.make_rarecharm_d2hh0ll_ds(dimuons, ksdd, kaons,
                                     '[D+ -> J/psi(1S) KS0 K+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kaons, dimuons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2piksllmumuos_line(name='Hlt2Charm_DpDspToKsPipMumMup_LL', prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    ds = m.make_rarecharm_d2hh0ll_ds(dimuons, ksll, pions,
                                     '[D+ -> J/psi(1S) KS0 pi+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, pions, dimuons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2piksddmumuos_line(name='Hlt2Charm_DpDspToKsPipMumMup_DD', prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    down_pions = m.filter_down_pions_forKs()
    ksdd = m.make_dd_ks(down_pions, down_pions)
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, "J/psi(1S) -> mu+ mu-")
    ds = m.make_rarecharm_d2hh0ll_ds(dimuons, ksdd, pions,
                                     '[D+ -> J/psi(1S) KS0 pi+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, pions, dimuons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kpi0mergedmumuos_line(name='Hlt2Charm_DpDspToPi0KpMumMup_M', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    pi0merged = m.make_rarecharm_merged_pi0s()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    ds = m.make_rarecharm_d2hh0ll_ds(dimuons, pi0merged, kaons,
                                     '[D+ -> J/psi(1S) pi0 K+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kaons, dimuons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kpi0resolvedmumuos_line(name='Hlt2Charm_DpDspToPi0KpMumMup_R',
                              prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    pi0resolved = m.make_rarecharm_resolved_pi0s()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    ds = m.make_rarecharm_d2hh0ll_ds(dimuons, pi0resolved, kaons,
                                     '[D+ -> J/psi(1S) pi0 K+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kaons, dimuons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kkslleeos_line(name='Hlt2Charm_DpDspToKsKpEmEp_LL', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds = m.make_rarecharm_d2hh0ll_ds(dielectrons, ksll, kaons,
                                     '[D+ -> J/psi(1S) KS0 K+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, kaons, dielectrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kksddeeos_line(name='Hlt2Charm_DpDspToKsKpEmEp_DD', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    down_pions = m.filter_down_pions_forKs()
    ksdd = m.make_dd_ks(down_pions, down_pions)
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds = m.make_rarecharm_d2hh0ll_ds(dielectrons, ksdd, kaons,
                                     '[D+ -> J/psi(1S) KS0 K+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, kaons, dielectrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2pikslleeos_line(name='Hlt2Charm_DpDspToKsPipEmEp_LL', prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds = m.make_rarecharm_d2hh0ll_ds(dielectrons, ksll, pions,
                                     '[D+ -> J/psi(1S) KS0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, pions, dielectrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2piksddeeos_line(name='Hlt2Charm_DpDspToKsPipEmEp_DD', prescale=1):

    pions = m.make_rarecharm_pions()
    down_pions = m.filter_down_pions_forKs()
    ksdd = m.make_dd_ks(down_pions, down_pions)
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds = m.make_rarecharm_d2hh0ll_ds(dielectrons, ksdd, pions,
                                     '[D+ -> J/psi(1S) KS0 pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, pions, dielectrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kpi0mergedeeos_line(name='Hlt2Charm_DpDspToPi0KpEmEp_M', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    pi0merged = m.make_merged_pi0s()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds = m.make_rarecharm_d2hh0ll_ds(dielectrons, pi0merged, kaons,
                                     '[D+ -> J/psi(1S) pi0 K+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, kaons, dielectrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def d2kpi0resolvedeeos_line(name='Hlt2Charm_DpDspToPi0KpEmEp_R', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    pi0resolved = m.make_rarecharm_resolved_pi0s()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds = m.make_rarecharm_d2hh0ll_ds(dielectrons, pi0resolved, kaons,
                                     '[D+ -> J/psi(1S) pi0 K+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, kaons, dielectrons, ds],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


""" baryonic lines """


@register_line_builder(all_lines)
@configurable
def Lc2pmumuos_line(name='Hlt2Charm_LcpToPpMumMup', prescale=1):

    protons = m.make_rarecharm_protons_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    lambdas_plus = m.make_rarecharm_lambdacs2pmumu(
        dimuons, protons, 'Lambda_c+ -> J/psi(1S) p+')
    lambdas_minus = m.make_rarecharm_lambdacs2pmumu(
        dimuons, protons, 'Lambda_c~- -> J/psi(1S) p~-')
    lambdas = ParticleContainersMerger([lambdas_plus, lambdas_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, muons, lambdas],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def Lc2pmumuss_line(name='Hlt2Charm_LcpToPmMupMup', prescale=1):

    protons = m.make_rarecharm_protons_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons_minus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu- mu-')
    lambdas_minus = m.make_rarecharm_lambdacs2pmumu(
        dimuons_minus, protons, 'Lambda_c~- -> J/psi(1S) p+')
    dimuons_plus = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu+')
    lambdas_plus = m.make_rarecharm_lambdacs2pmumu(
        dimuons_plus, protons, 'Lambda_c+ -> J/psi(1S) p~-')
    lambdas = ParticleContainersMerger([lambdas_plus, lambdas_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, muons, lambdas],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Lc2peeos_line(name='Hlt2Charm_LcpToPpEmEp', prescale=1):

    protons = m.make_rarecharm_protons_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    lambdas_plus = m.make_rarecharm_lambdacs2pel(dielectrons, protons,
                                                 'Lambda_c+ -> J/psi(1S) p+')
    lambdas_minus = m.make_rarecharm_lambdacs2pel(
        dielectrons, protons, 'Lambda_c~- -> J/psi(1S) p~-')
    lambdas = ParticleContainersMerger([lambdas_plus, lambdas_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, electrons, lambdas],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Lc2peess_line(name='Hlt2Charm_LcpToPmEpEp', prescale=1):

    protons = m.make_rarecharm_protons_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    electrons_plus = m.get_positive_particles(electrons)
    dielectrons_plus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_plus, diElectonID="J/psi(1S)", isOS=False)
    lambdas_plus = m.make_rarecharm_lambdacs2pel(dielectrons_plus, protons,
                                                 'Lambda_c+ -> J/psi(1S) p~-')
    electrons_minus = m.get_negative_particles(electrons)
    dielectrons_minus = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons_minus, diElectonID="J/psi(1S)", isOS=False)
    lambdas_minus = m.make_rarecharm_lambdacs2pel(
        dielectrons_minus, protons, 'Lambda_c~- -> J/psi(1S) p+')
    lambdas = ParticleContainersMerger([lambdas_plus, lambdas_minus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, electrons, lambdas],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Lc2pmumeos_line(name='Hlt2Charm_LcpToPpMumpEpm', prescale=1):

    protons = m.make_rarecharm_protons_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    electrons = m.make_rarecharm_electrons_with_brem()
    muplus_eminus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e- mu+')
    lambdap_mup_em = m.make_rarecharm_lambdacs2pmumu(
        muplus_eminus, protons, 'Lambda_c+ -> J/psi(1S) p+')
    lambdam_mup_em = m.make_rarecharm_lambdacs2pmumu(
        muplus_eminus, protons, 'Lambda_c~- -> J/psi(1S) p~-')
    muminus_eplus = m.make_rarecharm_ElectronMuon(electrons, muons,
                                                  'J/psi(1S) -> e+ mu-')
    lambdap_mum_ep = m.make_rarecharm_lambdacs2pmumu(
        muminus_eplus, protons, 'Lambda_c+ -> J/psi(1S) p+')
    lambdam_mum_ep = m.make_rarecharm_lambdacs2pmumu(
        muminus_eplus, protons, 'Lambda_c~- -> J/psi(1S) p~-')
    lambdas = ParticleContainersMerger(
        [lambdap_mup_em, lambdam_mup_em, lambdap_mum_ep, lambdam_mum_ep])
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, electrons, muons, lambdas],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Lc2pksllcontrol_line(name='Hlt2Charm_LcpToKsPp_LL_RareCharmControl',
                         prescale=0.2):
    protons = m.make_rarecharm_protons_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    lambdas = m.make_rarecharm_lambdacs2pks(ksll, protons,
                                            '[Lambda_c+ -> KS0 p+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, lambdas],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def Lc2pksddcontrol_line(name='Hlt2Charm_LcpToKsPp_DD_RareCharmControl',
                         prescale=0.2):

    protons = m.make_rarecharm_protons_tightIPCut()
    down_pions = m.filter_down_pions_forKs()
    ksdd = m.make_dd_ks(down_pions, down_pions)
    lambdas = m.make_rarecharm_lambdacs2pks(ksdd, protons,
                                            "[Lambda_c+ -> KS0 p+]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, lambdas],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def Lc2ppipicontrol_line(name='Hlt2Charm_LcpToPpPimPip_RareCharmControl',
                         prescale=0.01):

    protons = m.make_rarecharm_protons_tightIPCut()
    noPID_pions = m.make_rarecharm_noPID_pions_tightIPCut()
    dipions = m.make_rarecharm_TwoPions(noPID_pions, noPID_pions,
                                        'J/psi(1S) -> pi+ pi-')
    lambda_plus = m.make_rarecharm_lambdacs2pmumu(
        dipions,
        protons,
        'Lambda_c+ -> J/psi(1S) p+',
        m_min=_MASS_M_MIN_CONTROL_LAMBDAC,
        m_max=_MASS_M_MAX_CONTROL_LAMBDAC)
    lambda_minus = m.make_rarecharm_lambdacs2pmumu(
        dipions,
        protons,
        'Lambda_c~- -> J/psi(1S) p~-',
        m_min=_MASS_M_MIN_CONTROL_LAMBDAC,
        m_max=_MASS_M_MAX_CONTROL_LAMBDAC)
    lambdas = ParticleContainersMerger([lambda_plus, lambda_minus])
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, lambdas],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def xic2lambdallmumu_line(name='Hlt2Charm_Xic0ToL0MumMup_LL', prescale=1.0):

    protons = m.make_rarecharm_long_protons_fromLambda()
    pions = m.make_rarecharm_long_pions_fromLambda()
    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    lambdas = m.make_rarecharm_ll_lambda2ppi(protons, pions,
                                             '[Lambda0 -> p+ pi-]cc')
    xic = m.make_rarecharm_ll_xic2lambdall(lambdas, dimuons,
                                           '[Xi_c0 -> Lambda0 J/psi(1S)]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dimuons, xic],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def xic2lambdaddmumu_line(name='Hlt2Charm_Xic0ToL0MumMup_DD', prescale=1.0):

    protons = m.make_rarecharm_down_protons_fromLambda()
    pions = m.make_rarecharm_down_pions_fromLambda()
    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    lambdas = m.make_rarecharm_dd_lambda2ppi(protons, pions,
                                             '[Lambda0 -> p+ pi-]cc')
    xic = m.make_rarecharm_dd_xic2lambdall(lambdas, dimuons,
                                           '[Xi_c0 -> Lambda0 J/psi(1S)]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dimuons, xic],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def xic2lambdaddee_line(name='Hlt2Charm_Xic0ToL0EmEp_DD', prescale=1.0):

    protons = m.make_rarecharm_down_protons_fromLambda()
    pions = m.make_rarecharm_down_pions_fromLambda()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    lambdas = m.make_rarecharm_dd_lambda2ppi(protons, pions,
                                             '[Lambda0 -> p+ pi-]cc')
    xic = m.make_rarecharm_dd_xic2lambdall(lambdas, dielectrons,
                                           '[Xi_c0 -> Lambda0 J/psi(1S)]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dielectrons, xic],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def xic2lambdallee_line(name='Hlt2Charm_Xic0ToL0EmEp_LL', prescale=1.0):

    protons = m.make_rarecharm_long_protons_fromLambda()
    pions = m.make_rarecharm_long_pions_fromLambda()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    lambdas = m.make_rarecharm_ll_lambda2ppi(protons, pions,
                                             '[Lambda0 -> p+ pi-]cc')
    xic = m.make_rarecharm_ll_xic2lambdall(lambdas, dielectrons,
                                           '[Xi_c0 -> Lambda0 J/psi(1S)]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [electrons, dielectrons, xic],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


""" Semi-leptonically tagged lines """


@register_line_builder(all_lines)
@configurable
def b2dstarmu_dzero2kpieers_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKmPipEmEp', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    dzeros_rs = m.make_rarecharm_d02hhmumu_dzeros_fromB(
        dielectrons, kaons, pions, '[D0 -> J/psi(1S) K- pi+]cc')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_rs, soft_pions, '[D*(2010)+ -> D0 pi+]cc')
    tag_mu = m.make_rarecharm_tagging_muons()
    bs = m.make_rarecharm_bs(dstars, tag_mu, '[B- -> D*(2010)+ mu-]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [tag_mu, electrons, dielectrons, dzeros_rs, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def b2dstarmu_dzero2kpieews_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKpPimEmEp', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    dzeros_ws = m.make_rarecharm_d02hhmumu_dzeros_fromB(
        dielectrons, kaons, pions, '[D0 -> J/psi(1S) K+ pi-]cc')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_ws, soft_pions, '[D*(2010)+ -> D0 pi+]cc')
    tag_mu = m.make_rarecharm_tagging_muons()
    bs = m.make_rarecharm_bs(dstars, tag_mu, '[B- -> D*(2010)+ mu-]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [tag_mu, electrons, dielectrons, dzeros_ws, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def b2dstarmu_dzero2kpimumurs_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKmPipMumMup', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    dzeros_rs = m.make_rarecharm_d02hhmumu_dzeros_fromB(
        dimuons, kaons, pions, '[D0 -> J/psi(1S) K- pi+]cc')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_rs, soft_pions, '[D*(2010)+ -> D0 pi+]cc')
    tag_mu = m.make_rarecharm_tagging_muons()
    bs = m.make_rarecharm_bs(dstars, tag_mu, '[B- -> D*(2010)+ mu-]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, muons, dimuons, dzeros_rs, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def b2dstarmu_dzero2kpimumuws_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKpPimMumMup', prescale=1):

    kaons = m.make_rarecharm_kaons()
    pions = m.make_rarecharm_pions()
    muons = m.make_rarecharm_muons()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    dzeros_ws = m.make_rarecharm_d02hhmumu_dzeros_fromB(
        dimuons, kaons, pions, '[D0 -> J/psi(1S) K+ pi-]cc')
    soft_pions = m.make_selected_rarecharm_slowpions()
    dstars = m.make_dstars(dzeros_ws, soft_pions, '[D*(2010)+ -> D0 pi+]cc')
    tag_mu = m.make_rarecharm_tagging_muons()
    bs = m.make_rarecharm_bs(dstars, tag_mu, '[B- -> D*(2010)+ mu-]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, muons, dimuons, dzeros_ws, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def b2dmu_d2pieeos_line(name='Hlt2Charm_B2DpDspMumX_DpDspToPipEmEp',
                        prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds_plus = m.make_rarecharm_d2hll_ds_fromB(dielectrons, pions,
                                              'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds_fromB(dielectrons, pions,
                                               'D- -> J/psi(1S) pi-')
    tag_mu = m.make_rarecharm_tagging_muons()
    b_minus = m.make_rarecharm_bs(ds_plus, tag_mu, 'B- -> D+ mu-')
    b_plus = m.make_rarecharm_bs(ds_minus, tag_mu, 'B+ -> D- mu+')
    bs = ParticleContainersMerger([b_minus, b_plus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, electrons, dielectrons, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def b2dmu_d2keeos_line(name='Hlt2Charm_B2DpDspMumX_DpDspToKpEmEp', prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    ds_plus = m.make_rarecharm_d2hll_ds_fromB(dielectrons, kaons,
                                              'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds_fromB(dielectrons, kaons,
                                               'D- -> J/psi(1S) K-')
    tag_mu = m.make_rarecharm_tagging_muons()
    b_minus = m.make_rarecharm_bs(ds_plus, tag_mu, 'B- -> D+ mu-')
    b_plus = m.make_rarecharm_bs(ds_minus, tag_mu, 'B+ -> D- mu+')
    bs = ParticleContainersMerger([b_minus, b_plus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, electrons, dielectrons, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def b2dmu_d2pimumuos_line(name='Hlt2Charm_B2DpDspMumX_DpDspToPipMumMup',
                          prescale=1):

    pions = m.make_rarecharm_pions_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    ds_plus = m.make_rarecharm_d2hll_ds_fromB(dimuons, pions,
                                              'D+ -> J/psi(1S) pi+')
    ds_minus = m.make_rarecharm_d2hll_ds_fromB(dimuons, pions,
                                               'D- -> J/psi(1S) pi-')
    tag_mu = m.make_rarecharm_tagging_muons()
    b_minus = m.make_rarecharm_bs(ds_plus, tag_mu, 'B- -> D+ mu-')
    b_plus = m.make_rarecharm_bs(ds_minus, tag_mu, 'B+ -> D- mu+')
    bs = ParticleContainersMerger([b_minus, b_plus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, muons, dimuons, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def b2dmu_d2kmumuos_line(name='Hlt2Charm_B2DpDspMumX_DpDspToKpMumMup',
                         prescale=1):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    ds_plus = m.make_rarecharm_d2hll_ds_fromB(dimuons, kaons,
                                              'D+ -> J/psi(1S) K+')
    ds_minus = m.make_rarecharm_d2hll_ds_fromB(dimuons, kaons,
                                               'D- -> J/psi(1S) K-')
    tag_mu = m.make_rarecharm_tagging_muons()
    b_minus = m.make_rarecharm_bs(ds_plus, tag_mu, 'B- -> D+ mu-')
    b_plus = m.make_rarecharm_bs(ds_minus, tag_mu, 'B+ -> D- mu+')
    bs = ParticleContainersMerger([b_minus, b_plus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, muons, dimuons, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def b2lcmu_Lc2pmumuos_line(name='Hlt2Charm_B2LcpMum_LcpToPpMumMup',
                           prescale=1):

    protons = m.make_rarecharm_protons_tightIPCut()
    muons = m.make_rarecharm_muons_tightIPCut()
    dimuons = m.make_rarecharm_TwoMuons(muons, 'J/psi(1S) -> mu+ mu-')
    lambdas_plus = m.make_rarecharm_lambdacs2pll_fromB(
        dimuons, protons, 'Lambda_c+ -> J/psi(1S) p+')
    lambdas_minus = m.make_rarecharm_lambdacs2pll_fromB(
        dimuons, protons, 'Lambda_c~- -> J/psi(1S) p~-')
    tag_mu = m.make_rarecharm_tagging_muons()
    b_minus = m.make_rarecharm_bs(lambdas_plus, tag_mu,
                                  'Lambda_b0 -> Lambda_c+ mu-')
    b_plus = m.make_rarecharm_bs(lambdas_minus, tag_mu,
                                 'Lambda_b~0 -> Lambda_c~- mu+')
    bs = ParticleContainersMerger([b_minus, b_plus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, protons, muons, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def b2lcmu_Lc2peeos_line(name='Hlt2Charm_B2LcpMum_LcpToPpEmEp', prescale=1):

    protons = m.make_rarecharm_protons_tightIPCut()
    electrons = m.make_rarecharm_electrons_no_brem_tightIPCut()
    dielectrons = m.make_rarecharm_TwoElectrons_with_brem(
        electrons=electrons, diElectonID="J/psi(1S)")
    lambdas_plus = m.make_rarecharm_lambdacs2pll_fromB(
        dielectrons, protons, 'Lambda_c+ -> J/psi(1S) p+')
    lambdas_minus = m.make_rarecharm_lambdacs2pll_fromB(
        dielectrons, protons, 'Lambda_c~- -> J/psi(1S) p~-')
    tag_mu = m.make_rarecharm_tagging_muons()
    b_minus = m.make_rarecharm_bs(lambdas_plus, tag_mu,
                                  'Lambda_b0 -> Lambda_c+ mu-')
    b_plus = m.make_rarecharm_bs(lambdas_minus, tag_mu,
                                 'Lambda_b~0 -> Lambda_c~- mu+')
    bs = ParticleContainersMerger([b_minus, b_plus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, protons, electrons, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def b2lcmu_Lc2pksllcontrol_line(
        name='Hlt2Charm_B2LcpMumX_LcpToKsPp_LL_RareCharmControl',
        prescale=1.0):
    protons = m.make_rarecharm_protons_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    lambdas = m.make_rarecharm_lambdacs2pks_fromB(ksll, protons,
                                                  "[Lambda_c+ -> KS0 p+]cc")
    tag_mu = m.make_rarecharm_tagging_muons()
    bs = m.make_rarecharm_bs(lambdas, tag_mu, '[Lambda_b0 -> Lambda_c+ mu-]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, protons, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def b2lcmu_Lc2pksddcontrol_line(
        name='Hlt2Charm_B2LcpMumX_LcpToKsPp_DD_RareCharmControl',
        prescale=1.0):

    protons = m.make_rarecharm_protons_tightIPCut()
    down_pions = m.filter_down_pions_forKs()
    ksdd = m.make_dd_ks(down_pions, down_pions)
    lambdas = m.make_rarecharm_lambdacs2pks_fromB(ksdd, protons,
                                                  "[Lambda_c+ -> KS0 p+]cc")
    tag_mu = m.make_rarecharm_tagging_muons()
    bs = m.make_rarecharm_bs(lambdas, tag_mu, '[Lambda_b0 -> Lambda_c+ mu-]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, protons, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def b2dmu_d2kskcontrol_line(
        name='Hlt2Charm_B2DpDspMumX_DpDspToKsKp_LL_RareCharmControl',
        prescale=0.10):

    kaons = m.make_rarecharm_kaons_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    ds_plus = m.make_rarecharm_d2hll_ds_fromB(
        ksll,
        kaons,
        'D+ -> KS0 K+',
        m_min=_MASS_M_MIN_CONTROL_HHL,
        m_max=_MASS_M_MAX_CONTROL_HHL)
    ds_minus = m.make_rarecharm_d2hll_ds_fromB(
        ksll,
        kaons,
        'D- -> KS0 K-',
        m_min=_MASS_M_MIN_CONTROL_HHL,
        m_max=_MASS_M_MAX_CONTROL_HHL)
    tag_mu = m.make_rarecharm_tagging_muons()
    b_minus = m.make_rarecharm_bs(ds_plus, tag_mu, 'B- -> D+ mu-')
    b_plus = m.make_rarecharm_bs(ds_minus, tag_mu, 'B+ -> D- mu+')
    bs = ParticleContainersMerger([b_minus, b_plus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, kaons, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )


@register_line_builder(all_lines)
@configurable
def b2dmu_d2kspicontrol_line(
        name='Hlt2Charm_B2DpDspMumX_DpDspToKsPip_LL_RareCharmControl',
        prescale=0.10):

    pions = m.make_rarecharm_pions_tightIPCut()
    long_pions = m.filter_long_pions_forKs()
    ksll = m.make_ll_ks(long_pions, long_pions)
    ds_plus = m.make_rarecharm_d2hll_ds_fromB(
        ksll,
        pions,
        'D+ -> KS0 pi+',
        m_min=_MASS_M_MIN_CONTROL_HHL,
        m_max=_MASS_M_MAX_CONTROL_HHL)
    ds_minus = m.make_rarecharm_d2hll_ds_fromB(
        ksll,
        pions,
        'D- -> KS0 pi-',
        m_min=_MASS_M_MIN_CONTROL_HHL,
        m_max=_MASS_M_MAX_CONTROL_HHL)
    tag_mu = m.make_rarecharm_tagging_muons()
    b_minus = m.make_rarecharm_bs(ds_plus, tag_mu, 'B- -> D+ mu-')
    b_plus = m.make_rarecharm_bs(ds_minus, tag_mu, 'B+ -> D- mu+')
    bs = ParticleContainersMerger([b_minus, b_plus])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [tag_mu, pions, bs],
        prescale=prescale,
        monitoring_variables=m._MONITORING_VARIABLES_MASS,
    )
