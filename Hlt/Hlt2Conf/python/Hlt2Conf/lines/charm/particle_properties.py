###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Constant properties (masses, decay times) of the particles used in charm
analyses.

Values from PDG2020.
"""

from GaudiKernel.SystemOfUnits import MeV, picosecond

_PI0_M = 134.9768 * MeV  # +/- 0.0005
_PION_M = 139.57061 * MeV  # +/- 0.00024
_KAON_M = 493.677 * MeV  # +/- 0.016
_KS_M = 497.611 * MeV  # +/- 0.013
_ETA_M = 547.862 * MeV  # +/- 0.017
_D0_M = 1864.84 * MeV  # +/- 0.05
_DP_M = 1869.66 * MeV  # +/- 0.05
_DS_M = 1968.35 * MeV  # +/- 0.07
_DSTARP_M = 2010.26 * MeV  # +/- 0.05
_LC_M = 2286.46 * MeV  # +/- 0.14
_XICP_M = 2467.94 * MeV  # +/- 0.19
_XICZ_M = 2470.90 * MeV  # +/- 0.26
_OMEGAC_M = 2695.2 * MeV  # +/- 1.7
_D0_TAU = 0.4101 * picosecond  # +/- 0.0015
