###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Lambda_c+/Xi_c+ -> p+ K- pi+, Xic0 -> p+ K- K- pi+ HLT2 lines.

Primary physics use-case in spectroscopy studies.
Based on Charm Production Cross-section Early Measurements lines as a start point.

----

Hlt2Charm_LcpToPpKmPip_SP/PR
[Lambda_c+ -> p+ K- pi+]cc

Hlt2Charm_XicpToPpKmPip_SP/PR
[Xi_c+ -> p+ K- pi+]cc

Hlt2Charm_Xic0ToPpKmKmPip_SP/PR
[Xi_c0 -> p+ K- K- pi+]cc


TODO/Notes:
- !!! All lines are PersistReco, we should go to Selective Persistence eventually.
    - Cone algorithm being developed to reduce number of extra_outputs particles
    - to control PR rate we may need to prescale, hopefully this never becomes necessary
      (as we've should have gone to SP at that point + tuned cuts to keep rate acceptable)

- Cuts will need tightening to control rate:
    - Currently, cuts tuned according to latest available Upgrade MC
        - Sim10Ua1
    - Aim to minimise background bandwidth, go for high signal purity, even if
      signal efficiency lost.
    - Ideas from Run 2 studies may help inform cuts.
    - Keep enough mass-sideband for data-driven selection studies

    - From @mstahl:
        - Have you considered tuning cuts differently for Xic and Lc?
          You should be able/might need to cut harder on the Xic due
          to longer lifetime and Cabibbo suppression.
        - Has been done - thresholds not identical but quite similar

- (F.PID_P - F.PID_K) > dllp_m_dllk_min cut on the proton was added (from Run 2).

- Xi_c0 line had tighter PID cuts than the other two lines. Consider
    re-visiting those if needed (probably after PID is tuned?)

- FDCHI2 cut
    - Investigated. Added threshold at 50

- Tune PT and IP cuts for hadrons 
    - make_*_from_baryons.

"""
import Functors as F

from GaudiKernel.SystemOfUnits import MeV, GeV, picosecond as ps, mm
from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons,
                                         make_has_rich_long_protons)
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.lines.charm.prefilters import charm_prefilters

all_lines = {}


@configurable
def _make_protons_from_baryons(pt_min=200 * MeV,
                               p_min=10 * GeV,
                               mipchi2_min=6.0,
                               dllp_min=5.0,
                               dllp_m_dllk_min=5.0):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(F.PT > pt_min, F.P > p_min,
                          F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min,
                                         Vertices=pvs), F.PID_P > dllp_min,
                          (F.PID_P - F.PID_K) > dllp_m_dllk_min)))


@configurable
def _make_kaons_from_baryons(
        pt_min=200 * MeV,
        p_min=1 * GeV,
        mipchi2_min=6.0,
        dllk_min=5.0,
):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > pt_min,
                F.P > p_min,
                F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                # F.CHI2DOF < trchi2dof_max,
                F.PID_K > dllk_min,
            ), ),
    )


@configurable
def _make_pions_from_baryons(pt_min=200 * MeV,
                             p_min=1 * GeV,
                             mipchi2_min=6.0,
                             dllk_max=5.0):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > pt_min,
                F.P > p_min,
                F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                # F.CHI2DOF < trchi2dof_max,
                F.PID_K < dllk_max,
            ), ),
    )


@configurable
def _make_lc_or_xicp_pKpi(
        sum_pt_min=3 * GeV,
        maxtree_pt_min=1 * GeV,
        mipchi2_min_at_least_one=16.0,
        mipchi2_min_at_least_two=9.0,
        mintree_pt_min=200.0 * MeV,
        mintree_mipchi2_min=6.0,
        maxtree_mipchi2_min=16.0,
        doca_max=0.1 * mm,
        bpvvdrho_min=.15,
        bpvvdrho_max=9999,
        bpvdira_min=.9998,
        vxchi2ndof_max=6,
        bpvltime_min=.25 * ps,
        bpvfdchi2_min=50,

        #
        # Nominally this is configured for the Lambda_c+ mass range.
        comb_m_min=2_201 * MeV,
        comb_m_max=2_372 * MeV,  #2_553.0 * MeV,
        m_min=2_211 * MeV,
        m_max=2_362 * MeV,
        decay_descriptor="[Lambda_c+ -> p+ K- pi+]cc"):
    pvs = make_pvs()
    return ParticleCombiner(
        Inputs=[
            _make_protons_from_baryons(),
            _make_kaons_from_baryons(),
            _make_pions_from_baryons(),
        ],
        DecayDescriptor=decay_descriptor,
        name=
        f"Charm_Spec_{'Lcp' if 'Lambda_c+' in decay_descriptor else 'Xicp'}_PpKmPip",
        CombinationCut=F.require_all(
            F.math.in_range(
                comb_m_min,
                F.MASS,
                comb_m_max,
            ),
            F.MAXDOCACUT(doca_max),
            F.SUM(
                F.MINIPCHI2CUT(
                    IPChi2Cut=mipchi2_min_at_least_one, Vertices=pvs)) >= 1,
            F.SUM(
                F.MINIPCHI2CUT(
                    IPChi2Cut=mipchi2_min_at_least_two, Vertices=pvs)) >= 2,
            #
            F.SUM(F.PT) > sum_pt_min,
            F.MAX(F.PT) > maxtree_pt_min,
            F.MIN(F.PT) > mintree_pt_min,
            #
            F.MIN(F.MINIPCHI2(pvs)) > mintree_mipchi2_min,
            F.MAX(F.MINIPCHI2(pvs)) > maxtree_mipchi2_min,
        ),
        CompositeCut=F.require_all(F.CHI2DOF < vxchi2ndof_max,
                                   F.BPVDIRA(pvs) > bpvdira_min,
                                   F.BPVLTIME(pvs) > bpvltime_min,
                                   F.BPVVDRHO(pvs) > bpvvdrho_min,
                                   F.BPVVDRHO(pvs) < bpvvdrho_max,
                                   F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                   F.math.in_range(m_min, F.MASS, m_max)),
    )


@configurable
def _make_lcp_pKpi(
        comb_m_min=2_201 * MeV,  # +- 10 MeV
        comb_m_max=2_372 * MeV,
        m_min=2_211 * MeV,
        m_max=2_362 * MeV,
        bpvvdrho_max=4.,
        bpvvdrho_min=.15,
        bpvdira_min=.9998,
        vxchi2ndof_max=5,
        bpvltime_min=.25 * ps,
        bpvfdchi2_min=50,
):
    return _make_lc_or_xicp_pKpi(
        bpvvdrho_min=bpvvdrho_min,
        bpvdira_min=bpvdira_min,
        vxchi2ndof_max=vxchi2ndof_max,
        bpvltime_min=bpvltime_min,
        bpvfdchi2_min=bpvfdchi2_min,
        bpvvdrho_max=bpvvdrho_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        decay_descriptor="[Lambda_c+ -> p+ K- pi+]cc")


@configurable
def _make_xicp_pKpi(
        comb_m_min=2_382 * MeV,  # +- 10 MeV
        comb_m_max=2_553 * MeV,
        m_min=2_392 * MeV,
        m_max=2_543 * MeV,
        bpvvdrho_min=.15,
        bpvdira_min=.9998,
        vxchi2ndof_max=6,
        bpvltime_min=.25 * ps,
        bpvfdchi2_min=50,
):
    return _make_lc_or_xicp_pKpi(
        bpvvdrho_min=bpvvdrho_min,
        bpvdira_min=bpvdira_min,
        vxchi2ndof_max=vxchi2ndof_max,
        bpvltime_min=bpvltime_min,
        bpvfdchi2_min=bpvfdchi2_min,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        decay_descriptor="[Xi_c+ -> p+ K- pi+]cc")


### Xic0 line


@configurable
def _make_xic0_pkkpi(
        comb_m_min=2_386.0 * MeV,
        comb_m_max=2_780.0 * MeV,
        comb_sum_pt_min=3 * GeV,
        comb_mipchi2_min_at_least_one=8.0,
        comb_mipchi2_min_at_least_two=6.0,
        doca_max=0.1 * mm,
        #
        bpvvdrho_min=.2,
        bpvdira_min=.9998,
        vxchi2ndof_max=4,
        vxchi2ndof_min=.25,
        bpvltime_min=.25 * ps,
        m_min=2_396.0 * MeV,
        m_max=2_770.0 * MeV):

    pvs = make_pvs()
    decay_descriptor = "[Xi_c0 -> p+ K- K- pi+]cc"

    return ParticleCombiner(
        Inputs=[
            _make_protons_from_baryons(mipchi2_min=4.0),
            _make_kaons_from_baryons(mipchi2_min=4.0),
            _make_kaons_from_baryons(mipchi2_min=4.0),
            _make_pions_from_baryons(mipchi2_min=4.0),
        ],
        DecayDescriptor=decay_descriptor,
        name='Charm_Spec_Xic0_PpKmKmPip_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(
                comb_m_min,
                F.MASS,
                comb_m_max,
            ),
            F.MAXDOCACUT(doca_max),
            F.SUM(F.PT) > comb_sum_pt_min,
            F.SUM(F.MINIPCHI2(pvs) > comb_mipchi2_min_at_least_one) >= 1,
            F.SUM(F.MINIPCHI2(pvs) > comb_mipchi2_min_at_least_two) >= 2,
        ),
        CompositeCut=F.require_all(F.CHI2DOF < vxchi2ndof_max,
                                   F.CHI2DOF > vxchi2ndof_min,
                                   F.BPVDIRA(pvs) > bpvdira_min,
                                   F.BPVLTIME(pvs) > bpvltime_min,
                                   F.BPVVDRHO(pvs) > bpvvdrho_min,
                                   F.math.in_range(m_min, F.MASS, m_max)),
    )


## Baryon Line builders


@register_line_builder(all_lines)
def lcp2pkpi_line(name="Hlt2Charm_LcpToPpKmPip_PR", prescale=1.0):
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [_make_lcp_pKpi()],
        persistreco=True,  # FIXME take this out when SP works
        prescale=prescale)


@register_line_builder(all_lines)
def xicp2pkpi_line(name="Hlt2Charm_XicpToPpKmPip_PR",
                   prescale=1.0):  # FIXME go to SP in tuning
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [_make_xicp_pKpi()],
        persistreco=True,  # FIXME take this out when SP works
        prescale=prescale)


@register_line_builder(all_lines)
def xic02pkkpi_line(name="Hlt2Charm_Xic0ToPpKmKmPip_PR",
                    prescale=1.0):  # FIXME go to SP in tuning
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [_make_xic0_pkkpi()],
        persistreco=True,  # FIXME take this out when SP works
        prescale=prescale)
