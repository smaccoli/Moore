###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define HLT2 charm lines with at least one pi0 or eta meson in the final
state. The h0 is reconstructed as `h0 -> e+ e- gamma` or `h0 -> e+ e- e+ e-`
in lines 1,2,3,7 and only as `h0 -> e+ e- gamma` in lines 4,5,6,8.

Signal modes:
  1. D(s)+  -> pi+ h0
  2. D(s)+  -> K+  h0
  3. Lambda_c+ -> p+ h0
  4. D* -> (D0 -> pi0 pi0) pi+
  5. D* -> (D0 -> eta pi0) pi+
  6. D* -> (D0 -> eta eta) pi+

The following control modes are defined:
  7. [D0  -> K- pi+ pi0]cc
  8. [D0  -> K- pi+ eta]cc

Same-sign analogous lines for background studies are defined for each of the
signal and control lines above, reconstructing the unphysical decay chains
[h0 -> e+ e+ gamma]cc and [h0 -> e+ e+ e+ e+]cc.

TODO:
- Add D* -> (D0  -> e+ e- gamma) pi+ line
"""

from GaudiKernel.SystemOfUnits import MeV, picosecond
import Functors as F
from Functors.math import in_range
from PyConf import configurable
from PyConf.Algorithms import FunctionalDiElectronMaker
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.algorithms import ParticleContainersMerger
from Hlt2Conf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_photons,
    make_long_electrons_no_brem,
)
from .prefilters import charm_prefilters

all_lines = {}

###############################################################################
# Define particle filters
###############################################################################


@configurable
def photon_maker(pt_cut=350 * MeV, photon_cl=0.2):
    return make_photons(PtCut=pt_cut, ConfLevelCut=photon_cl)


@configurable
def filter_pions(particles,
                 pvs,
                 pt_min=500 * MeV,
                 p_min=2000 * MeV,
                 mipchi2_min=9,
                 dllk_max=5):
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.PID_K < dllk_max,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_kaons(
        particles,
        pvs,
        pt_min=500 * MeV,
        p_min=2000 * MeV,
        mipchi2_min=9,
        dllk_min=5,
):
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.PID_K > dllk_min,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_protons(particles,
                   pvs,
                   pt_min=500 * MeV,
                   p_min=2000 * MeV,
                   mipchi2_min=9,
                   dllp_min=5,
                   dllpK_min=-1):
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.PID_P > dllp_min,
        F.PID_P - F.PID_K > dllpK_min,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_pions_soft(particles, pt_min=200 * MeV):
    cut = F.require_all(F.PT > pt_min)
    return ParticleFilter(particles, F.FILTER(cut))


###############################################################################
# Make electron-positron pairs
###############################################################################


@configurable
def make_dielectron_with_brem(
        electron_maker=make_long_electrons_no_brem,
        opposite_sign=True,
        PIDe_min=2.,
        pt_e=250. * MeV,
        minipchi2=9.,
        # trghostprob=0.25,
        dielectron_ID="J/psi(1S)",
        pt_diE=0. * MeV,
        m_diE_min=0. * MeV,
        m_diE_max=1000. * MeV,
        # adocachi2cut=30.,
        bpvvdchi2=10.,
        vfaspfchi2ndof=10.):
    """Make detached Jpsi -> e+ e- or [Jpsi -> e+ e+ ]cc candidates adding
    bremsstrahlung correction to the electrons.
    """
    pvs = make_pvs()
    particles = electron_maker()

    code_e = F.require_all(
        F.PID_E > PIDe_min,
        F.PT > pt_e,
        F.MINIPCHI2(pvs) > minipchi2,
        # F.GHOSTPROB < trghostprob,
    )

    detached_e = ParticleFilter(particles, F.FILTER(code_e))

    detached_dielectron_with_brem = FunctionalDiElectronMaker(
        InputParticles=detached_e,
        MinDiElecPT=pt_diE,
        MinDiElecMass=m_diE_min,
        MaxDiElecMass=m_diE_max,
        DiElecID=dielectron_ID,
        OppositeSign=opposite_sign).Particles

    code_dielectron = F.require_all(
        # F.MAXDOCACHI2CUT(adocachi2cut), # This causes segfaults
        F.CHI2DOF < vfaspfchi2ndof,
        F.BPVFDCHI2(pvs) > bpvvdchi2,
    )

    return ParticleFilter(detached_dielectron_with_brem,
                          F.FILTER(code_dielectron))


###############################################################################
# Create neutral mesons
###############################################################################


@configurable
def make_pi0_eeg_adder(
        dielectron,
        photons,
        # pvs,
        am_min=10 * MeV,
        am_max=270 * MeV,
        m_min=20 * MeV,
        m_max=250 * MeV,
        apt_min=800 * MeV,
        pt_min=1000 * MeV,
        # amindoca_max=0.1 * mm,
        # vchi2pdof_max=10,
        # bpvfdchi2_min=25
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > apt_min,
        # F.MAXDOCACUT(amindoca_max),
    )
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        # F.CHI2DOF < vchi2pdof_max,
        # F.BPVFDCHI2(pvs) > bpvfdchi2_min,
    )

    return ParticleCombiner(
        name='Charm_DToHH0_Pi02EEG_{hash}',
        Inputs=[dielectron, photons],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="pi0 -> J/psi(1S) gamma",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_pi0_eeee(
        dielectron,
        pvs,
        am_min=10 * MeV,
        am_max=270 * MeV,
        m_min=20 * MeV,
        m_max=250 * MeV,
        apt_min=800 * MeV,
        pt_min=1000 * MeV,
        # amindoca_max=0.1 * mm,
        vchi2pdof_max=25,
        bpvfdchi2_min=25):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > apt_min,
        # F.MAXDOCACUT(amindoca_max),
    )
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
    )

    return ParticleCombiner(
        name='Charm_DToHH0_Pi02EEEE_{hash}',
        Inputs=[dielectron, dielectron],
        DecayDescriptor="pi0 -> J/psi(1S) J/psi(1S)",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_eta_eeg_adder(
        dielectron,
        photons,
        # pvs,
        am_min=350 * MeV,
        am_max=750 * MeV,
        m_min=400 * MeV,
        m_max=700 * MeV,
        apt_min=1800 * MeV,
        pt_min=2000 * MeV,
        # amindoca_max=0.1 * mm,
        # vchi2pdof_max=10,
        # bpvfdchi2_min=25
):

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > apt_min,
        # F.MAXDOCACUT(amindoca_max),
    )

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        # F.CHI2DOF < vchi2pdof_max,
        # F.BPVFDCHI2(pvs) > bpvfdchi2_min,
    )

    return ParticleCombiner(
        name='Charm_DToHH0_Eta2EEG_{hash}',
        Inputs=[dielectron, photons],
        DecayDescriptor="eta -> J/psi(1S) gamma",
        ParticleCombiner="ParticleAdder",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


###############################################################################
# D(s)+ hadron combiners
###############################################################################


@configurable
def make_d_to_hh0(
        particles,
        pvs,
        descriptor,
        name,
        am_min=1500 * MeV,
        am_max=2600 * MeV,
        m_min=1600 * MeV,
        m_max=2500 * MeV,
        apt_min=800 * MeV,
        pt_min=1000 * MeV,
        # amindoca_max=0.1 * mm,
        vchi2pdof_max=10,
        bpvfdchi2_min=25,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.999,
        bpvipchi2_max=25):

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > apt_min,
        # F.MAXDOCACUT(amindoca_max)
    )

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vchi2pdof_max,
        F.PT > pt_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_d0_to_hhh0(
        particles,
        pvs,
        descriptor,
        name,
        am_min=1500 * MeV,
        am_max=2600 * MeV,
        m_min=1600 * MeV,
        m_max=2500 * MeV,
        apt_min=800 * MeV,
        pt_min=1000 * MeV,
        # amindoca_max=0.1 * mm,
        vchi2pdof_max=10,
        bpvfdchi2_min=25,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.999,
        bpvipchi2_max=25):

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > apt_min,
        # F.MAXDOCACUT(amindoca_max)
    )

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_d0_to_h0h0(
        particles,
        pvs,
        descriptor,
        name,
        am_min=1500 * MeV,
        am_max=2600 * MeV,
        m_min=1600 * MeV,
        m_max=2500 * MeV,
        apt_min=800 * MeV,
        pt_min=1000 * MeV,
        # amindoca_max=0.1 * mm,
        vchi2pdof_max=10,
        bpvfdchi2_min=25,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.999,
        bpvipchi2_max=25):

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > apt_min,
        # F.MAXDOCACUT(amindoca_max),
    )

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_dstars(
        dzeros,
        soft_pions,
        descriptor,
        name,
        q_am_max=60 * MeV,
        q_m_max=40 * MeV,
        # vchi2pdof_max=25
):
    """Return D*+ maker for tagging a D0 with a soft pion."""
    combination_code = F.require_all(
        F.MASS - F.CHILD(1, F.MASS) - F.CHILD(2, F.MASS) < q_am_max)

    vertex_code = F.require_all(
        # F.CHI2DOF < vchi2pdof_max,
        F.MASS - F.CHILD(1, F.MASS) - F.CHILD(2, F.MASS) < q_m_max)

    return ParticleCombiner([dzeros, soft_pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


###############################################################################
# Define pi0 -> e+ e- gamma lines
###############################################################################

# ---------------------------------------
# D(s)+ -> pi+ pi0 with pi0 -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def dtopipi0_pi0toeeg_line(name="Hlt2Charm_DpDspToPipPi0_Pi0ToEmEpG",
                           prescale=1):
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[pi0s, pions],
        pvs=pvs,
        descriptor="[D+ -> pi0 pi+]cc",
        name='Charm_DToHH0_DpDspToPipPi0_OS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# D(s)+ -> pi+ pi0 with pi0 -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def dtopipi0_pi0toeeg_ss_line(name="Hlt2Charm_DpDspToPipPi0_Pi0ToEpEpG",
                              prescale=1):
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[pi0s, pions],
        pvs=pvs,
        descriptor="[D+ -> pi0 pi+]cc",
        name='Charm_DToHH0_DpDspToPipPi0_SS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# D(s)+ -> K+ pi0 with pi0 -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def dtokpi0_pi0toeeg_line(name="Hlt2Charm_DpDspToKpPi0_Pi0ToEmEpG",
                          prescale=1):
    pvs = make_pvs()
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[pi0s, kaons],
        pvs=pvs,
        descriptor="[D+ -> pi0 K+]cc",
        name='Charm_DToHH0_DpDspToKpPi0_OS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# D(s)+ -> K+ pi0 with pi0 -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def dtokpi0_pi0toeeg_ss_line(name="Hlt2Charm_DpDspToKpPi0_Pi0ToEpEpG",
                             prescale=1):
    pvs = make_pvs()
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[pi0s, kaons],
        pvs=pvs,
        descriptor="[D+ -> pi0 K+]cc",
        name='Charm_DToHH0_DpDspToKpPi0_SS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# Lambda_c+ -> p pi0 with pi0 -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def lctoppi0_pi0toeeg_line(name="Hlt2Charm_LcpToPpPi0_Pi0ToEmEpG", prescale=1):
    pvs = make_pvs()
    protons = filter_protons(make_has_rich_long_protons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[pi0s, protons],
        pvs=pvs,
        descriptor="[Lambda_c+ -> pi0 p+]cc",
        name='Charm_DToHH0_LcpToPpPi0_OS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# Lambda_c+ -> p+ pi0 with pi0 -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def lctoppi0_pi0toeeg_ss_line(name="Hlt2Charm_LcpToPpPi0_Pi0ToEpEpG",
                              prescale=1):
    pvs = make_pvs()
    protons = filter_protons(make_has_rich_long_protons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[pi0s, protons],
        pvs=pvs,
        descriptor="[Lambda_c+ -> pi0 p+]cc",
        name='Charm_DToHH0_LcpToPpPi0_SS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


###############################################################################
# Define pi0 -> e+ e- e+ e- lines
###############################################################################

# ---------------------------------------
# D(s)+ -> pi+ pi0 with pi0 -> e+ e- e+ e-
# ---------------------------------------


@register_line_builder(all_lines)
def dtopipi0_pi0toeeee_line(name="Hlt2Charm_DpDspToPipPi0_Pi0ToEmEmEpEp",
                            prescale=1):
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeee(dielectrons, pvs)
    ds = make_d_to_hh0(
        particles=[pi0s, pions],
        pvs=pvs,
        descriptor="[D+ -> pi0 pi+]cc",
        name='Charm_DToHH0_DpDspToPipPi0_EmEmEpEp_OS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# D(s)+ -> pi+ pi0 with pi0 -> e+ e+ e+ e+
# ---------------------------------------


@register_line_builder(all_lines)
def dtopipi0_pi0toeeee_ss_line(name="Hlt2Charm_DpDspToPipPi0_Pi0ToEpEpEpEp",
                               prescale=1):
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeee(dielectrons, pvs)
    ds = make_d_to_hh0(
        particles=[pi0s, pions],
        pvs=pvs,
        descriptor="[D+ -> pi0 pi+]cc",
        name='Charm_DToHH0_DpDspToPipPi0_EpEpEpEp_SS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# D(s)+ -> K+ pi0 with pi0 -> e+ e- e+ e-
# ---------------------------------------


@register_line_builder(all_lines)
def dtokpi0_pi0toeeee_line(name="Hlt2Charm_DpDspToKpPi0_Pi0ToEmEmEpEp",
                           prescale=1):
    pvs = make_pvs()
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeee(dielectrons, pvs)
    ds = make_d_to_hh0(
        particles=[pi0s, kaons],
        pvs=pvs,
        descriptor="[D+ -> pi0 K+]cc",
        name='Charm_DToHH0_DpDspToKpPi0_EmEmEpEp_OS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# D(s)+ -> K+ pi0 with pi0 -> e+ e+ e+ e+
# ---------------------------------------


@register_line_builder(all_lines)
def dtokpi0_pi0toeeee_ss_line(name="Hlt2Charm_DpDspToKpPi0_Pi0ToEpEpEpEp",
                              prescale=1):
    pvs = make_pvs()
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeee(dielectrons, pvs)
    ds = make_d_to_hh0(
        particles=[pi0s, kaons],
        pvs=pvs,
        descriptor="[D+ -> pi0 K+]cc",
        name='Charm_DToHH0_DpDspToKpPi0_EpEpEpEp_SS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# Lambda_c+ -> p pi0 with pi0 -> e+ e- e+ e-
# ---------------------------------------


@register_line_builder(all_lines)
def lctoppi0_pi0toeeee_line(name="Hlt2Charm_LcpToPpPi0_Pi0ToEmEmEpEp",
                            prescale=1):
    pvs = make_pvs()
    protons = filter_protons(make_has_rich_long_protons(), pvs)
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeee(dielectrons, pvs)
    ds = make_d_to_hh0(
        particles=[pi0s, protons],
        pvs=pvs,
        descriptor="[Lambda_c+ -> pi0 p+]cc",
        name='Charm_DToHH0_LcpToPpPi0_EmEmEpEp_OS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


# ---------------------------------------
# Lambda_c+ -> p+ pi0 with pi0 -> e+ e+ e+ e+
# ---------------------------------------


@register_line_builder(all_lines)
def lctoppi0_pi0toeeee_ss_line(name="Hlt2Charm_LcpToPpPi0_Pi0ToEpEpEpEp",
                               prescale=1):
    pvs = make_pvs()
    protons = filter_protons(make_has_rich_long_protons(), pvs)
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeee(dielectrons, pvs)
    ds = make_d_to_hh0(
        particles=[pi0s, protons],
        pvs=pvs,
        descriptor="[Lambda_c+ -> pi0 p+]cc",
        name='Charm_DToHH0_LcpToPpPi0_EpEpEpEp_SS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, ds],
        prescale=prescale,
    )


###############################################################################
#  Signal eta modes
###############################################################################

# ---------------------------------------
# D(s)+ -> pi+ eta with eta -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def dtopieta_etatoeeg_line(name="Hlt2Charm_DpDspToEtaPip_EtaToEmEpG",
                           prescale=1):
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    etas = make_eta_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[etas, pions],
        pvs=pvs,
        descriptor="[D+ -> eta pi+]cc",
        name='Charm_DToHH0_DpDspToEtaPip_OS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, ds],
        prescale=prescale,
    )


# ---------------------------------------
# D(s)+ -> pi+ eta with eta -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def dtopieta_etatoeeg_ss_line(name="Hlt2Charm_DpDspToEtaPip_EtaToEpEpG",
                              prescale=1):
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    etas = make_eta_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[etas, pions],
        pvs=pvs,
        descriptor="[D+ -> eta pi+]cc",
        name='Charm_DToHH0_DpDspToEtaPip_SS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, ds],
        prescale=prescale,
    )


# ---------------------------------------
# D(s)+ -> K+ eta with eta -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def dtoketa_etatoeeg_line(name="Hlt2Charm_DpDspToEtaKp_EtaToEmEpG",
                          prescale=1):
    pvs = make_pvs()
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    etas = make_eta_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[etas, kaons],
        pvs=pvs,
        descriptor="[D+ -> eta K+]cc",
        name='Charm_DToHH0_DpDspToEtaKp_OS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, ds],
        prescale=prescale,
    )


# ---------------------------------------
# D(s)+ -> K+ eta with eta -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def dtoketa_etatoeeg_ss_line(name="Hlt2Charm_DpDspToEtaKp_EtaToEpEpG",
                             prescale=1):
    pvs = make_pvs()
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    etas = make_eta_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[etas, kaons],
        pvs=pvs,
        descriptor="[D+ -> eta K+]cc",
        name='Charm_DToHH0_DpDspToEtaKp_SS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, ds],
        prescale=prescale,
    )


# ---------------------------------------
# Lambda_c+ -> p eta with eta -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def lctopeta_etatoeeg_line(name="Hlt2Charm_LcpToPpEta_EtaToEmEpG", prescale=1):
    pvs = make_pvs()
    protons = filter_protons(make_has_rich_long_protons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    etas = make_eta_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[etas, protons],
        pvs=pvs,
        descriptor="[Lambda_c+ -> eta p+]cc",
        name='Charm_DToHH0_LcpToPpEta_OS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, ds],
        prescale=prescale,
    )


# ---------------------------------------
# Lambda_c+ -> p+ eta with eta -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def lctopeta_etatoeeg_ss_line(name="Hlt2Charm_LcpToPpEta_EtaToEpEpG",
                              prescale=1):
    pvs = make_pvs()
    protons = filter_protons(make_has_rich_long_protons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    etas = make_eta_eeg_adder(dielectrons, photons)
    ds = make_d_to_hh0(
        particles=[etas, protons],
        pvs=pvs,
        descriptor="[Lambda_c+ -> eta p+]cc",
        name='Charm_DToHH0_LcpToPpEta_SS_{hash}')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, ds],
        prescale=prescale,
    )


###############################################################################
# Control modes
###############################################################################

# ---------------------------------------
# D0 -> K- pi+ pi0 with pi0 -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0tokpipi0_pi0toeeg_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmPipPi0_Pi0ToEmEpG", prescale=1):
    desc = "D0ToKmPipPi0_OS"
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_hhh0(
        particles=[pi0s, kaons, pions],
        pvs=pvs,
        descriptor="[D0 -> pi0 K- pi+]cc",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstar = make_dstars(dzeros, softpi, '[D*(2010)+ -> D0 pi+]cc',
                        "Charm_DToHH0_" + "DstpToD0Pip_" + desc)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> K- pi+ pi0 with pi0 -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0tokpipi0_pi0toeeg_ss_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmPipPi0_Pi0ToEpEpG", prescale=1):
    desc = "D0ToKmPipPi0_SS"
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_hhh0(
        particles=[pi0s, kaons, pions],
        pvs=pvs,
        descriptor="[D0 -> pi0 K- pi+]cc",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstar = make_dstars(dzeros, softpi, '[D*(2010)+ -> D0 pi+]cc',
                        "Charm_DToHH0_" + "DstpToD0Pip_" + desc)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> K- pi+ pi0 with pi0 -> e+ e- e+ e-
# ---------------------------------------


@register_line_builder(all_lines)
def d0tokpipi0_pi0toeeee_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmPipPi0_Pi0ToEmEmEpEp", prescale=1):
    desc = "D0ToKmPipPi0_EmEmEpEp"
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeee(dielectrons, pvs)
    dzeros = make_d0_to_hhh0(
        particles=[pi0s, kaons, pions],
        pvs=pvs,
        descriptor="[D0 -> pi0 K- pi+]cc",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstar = make_dstars(dzeros, softpi, '[D*(2010)+ -> D0 pi+]cc',
                        "Charm_DToHH0_" + "DstpToD0Pip_" + desc)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> K- pi+ pi0 with pi0 -> e+ e+ e+ e+
# ---------------------------------------


@register_line_builder(all_lines)
def d0tokpipi0_pi0toeeee_ss_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToKmPipPi0_Pi0ToEpEpEpEp", prescale=1):
    desc = "D0ToKmPipPi0_EpEpEpEp"
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeee(dielectrons, pvs)
    dzeros = make_d0_to_hhh0(
        particles=[pi0s, kaons, pions],
        pvs=pvs,
        descriptor="[D0 -> pi0 K- pi+]cc",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstar = make_dstars(dzeros, softpi, '[D*(2010)+ -> D0 pi+]cc',
                        "Charm_DToHH0_" + "DstpToD0Pip_" + desc)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> K- pi+ eta with eta -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0tokpieta_etatoeeg_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToEtaKmPip_EtaToEmEpG", prescale=1):
    desc = "D0ToEtaKmPip_OS"
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    etas = make_eta_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_hhh0(
        particles=[etas, kaons, pions],
        pvs=pvs,
        descriptor="[D0 -> eta K- pi+]cc",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstar = make_dstars(dzeros, softpi, '[D*(2010)+ -> D0 pi+]cc',
                        "Charm_DToHH0_" + "DstpToD0Pip_" + desc)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> K- pi+ eta with eta -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0tokpieta_etatoeeg_ss_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToEtaKmPip_EtaToEpEpG", prescale=1):
    desc = "D0ToEtaKmPip_SS"
    pvs = make_pvs()
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    etas = make_eta_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_hhh0(
        particles=[etas, kaons, pions],
        pvs=pvs,
        descriptor="[D0 -> eta K- pi+]cc",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstar = make_dstars(dzeros, softpi, '[D*(2010)+ -> D0 pi+]cc',
                        "Charm_DToHH0_" + "DstpToD0Pip_" + desc)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, dzeros, dstar],
        prescale=prescale,
    )


###############################################################################
# Double neutral modes
###############################################################################

# ---------------------------------------
# D0 -> pi0 pi0 with pi0 -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0topi0pi0_pi0toeeg_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToPi0Pi0_Pi0ToEmEpG", prescale=1):
    desc = "D0ToPi0Pi0_OS"
    pvs = make_pvs()
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_h0h0(
        particles=[pi0s, pi0s],
        pvs=pvs,
        descriptor="D0 -> pi0 pi0",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstarp = make_dstars(dzeros, softpi, 'D*(2010)+ -> D0 pi+',
                         "Charm_DToHH0_" + "DstpToD0Pip_" + desc)
    dstarm = make_dstars(dzeros, softpi, 'D*(2010)- -> D0 pi-',
                         "Charm_DToHH0_" + "DstmToD0Pim_" + desc)
    dstar = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> pi0 pi0 with pi0 -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0topi0pi0_pi0toeeg_ss_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToPi0Pi0_Pi0ToEpEpG", prescale=1):
    desc = "D0ToPi0Pi0_SS"
    pvs = make_pvs()
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_h0h0(
        particles=[pi0s, pi0s],
        pvs=pvs,
        descriptor="D0 -> pi0 pi0",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstarp = make_dstars(dzeros, softpi, 'D*(2010)+ -> D0 pi+',
                         "Charm_DToHH0_" + "DstpToD0Pip_" + desc)
    dstarm = make_dstars(dzeros, softpi, 'D*(2010)- -> D0 pi-',
                         "Charm_DToHH0_" + "DstmToD0Pim_" + desc)
    dstar = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> pi0 eta with pi0 -> e+ e- gamma and eta -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0topi0eta_pi0toeeg_etatoeeg_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToEtaPi0_Pi0ToEmEpG_EtaToEmEpG",
        prescale=1):
    desc = "D0ToEtaPi0_OS"
    pvs = make_pvs()
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    etas = make_eta_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_h0h0(
        particles=[pi0s, etas],
        pvs=pvs,
        descriptor="D0 -> pi0 eta",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstarp = make_dstars(dzeros, softpi, 'D*(2010)+ -> D0 pi+',
                         "Charm_DToHH0_" + "DstpToD0Pip_" + desc)
    dstarm = make_dstars(dzeros, softpi, 'D*(2010)- -> D0 pi-',
                         "Charm_DToHH0_" + "DstmToD0Pim_" + desc)
    dstar = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, etas, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> pi0 eta with pi0 -> e+ e+ gamma and eta -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0topi0eta_pi0toeeg_etatoeeg_ss_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToEtaPi0_Pi0ToEpEpG_EtaToEpEpG",
        prescale=1):
    desc = "D0ToEtaPi0_SS"
    pvs = make_pvs()
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    pi0s = make_pi0_eeg_adder(dielectrons, photons)
    etas = make_eta_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_h0h0(
        particles=[pi0s, etas],
        pvs=pvs,
        descriptor="D0 -> pi0 eta",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstarp = make_dstars(dzeros, softpi, 'D*(2010)+ -> D0 pi+',
                         "Charm_DToHH0_" + "DstpToD0Pip_" + desc)
    dstarm = make_dstars(dzeros, softpi, 'D*(2010)- -> D0 pi-',
                         "Charm_DToHH0_" + "DstmToD0Pim_" + desc)
    dstar = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, pi0s, etas, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> eta eta with eta -> e+ e- gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0toetaeta_etatoeeg_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToEtaEta_EtaToEmEpG", prescale=1):
    desc = "D0ToEtaEta_OS"
    pvs = make_pvs()
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem()
    etas = make_eta_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_h0h0(
        particles=[etas, etas],
        pvs=pvs,
        descriptor="D0 -> eta eta",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstarp = make_dstars(dzeros, softpi, 'D*(2010)+ -> D0 pi+',
                         "Charm_DToHH0_" + "DstpToD0Pip_" + desc)
    dstarm = make_dstars(dzeros, softpi, 'D*(2010)- -> D0 pi-',
                         "Charm_DToHH0_" + "DstmToD0Pim_" + desc)
    dstar = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, dzeros, dstar],
        prescale=prescale,
    )


# ---------------------------------------
# D0 -> eta eta with eta -> e+ e+ gamma
# ---------------------------------------


@register_line_builder(all_lines)
def d0toetaeta_etatoeeg_ss_line(
        name="Hlt2Charm_DstpToD0Pip_D0ToEtaEta_EtaToEpEpG", prescale=1):
    desc = "D0ToEtaEta_SS"
    pvs = make_pvs()
    photons = photon_maker()
    dielectrons = make_dielectron_with_brem(opposite_sign=False)
    etas = make_eta_eeg_adder(dielectrons, photons)
    dzeros = make_d0_to_h0h0(
        particles=[etas, etas],
        pvs=pvs,
        descriptor="D0 -> eta eta",
        name='Charm_DToHH0__{hash}' + desc)
    softpi = filter_pions_soft(make_has_rich_long_pions())
    dstarp = make_dstars(dzeros, softpi, 'D*(2010)+ -> D0 pi+',
                         "Charm_DToHH0_" + "DstpToD0Pip_" + desc)
    dstarm = make_dstars(dzeros, softpi, 'D*(2010)- -> D0 pi-',
                         "Charm_DToHH0_" + "DstmToD0Pim_" + desc)
    dstar = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dielectrons, etas, dzeros, dstar],
        prescale=prescale,
    )
