###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of D0 -> h- h+ gamma HLT2 lines.

The lines cover the whole mass spectrum for the h- h+ pairs, but the pairs are
reconstructed as a vector resonance in the decay descriptor.

All lines are promptly tagged with D*(2010)+ -> D0 pi+ decays.

  1. D0 -> phi(-> K- K+) gamma (Hlt2Charm_DstpToD0Pip_D0ToKmKpG)
  2. D0 -> rho(-> pi- pi+) gamma (Hlt2Charm_DstpToD0Pip_D0ToPimPipG)
  3. D0 -> K*(-> K- pi+) gamma (Hlt2Charm_DstpToD0Pip_D0ToKmPipG)
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, micrometer as um
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from Hlt2Conf.algorithms import ParticleContainersMerger
from .taggers import make_tagging_pions
from .prefilters import charm_prefilters
from ...standard_particles import (make_has_rich_long_pions,
                                   make_has_rich_long_kaons, make_photons)

###############################################################################
# Basic particles builders.
#   - kaon and pion cuts from Stripping29r2p1
#   - photon cuts from photons tool in standard particles (PT(gamma) > 2 GeV)
###############################################################################


def _make_charm_pions():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 500 * MeV,
                          F.MINIPCHI2CUT(IPChi2Cut=3., Vertices=make_pvs()),
                          F.PID_K < 0)))


def _make_charm_kaons():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 500 * MeV,
                          F.MINIPCHI2CUT(IPChi2Cut=3., Vertices=make_pvs()),
                          F.PID_K > 0)))


def _make_charm_soft_pions():
    """Return maker for tagging pions filtered by thresholds common to all
    D*+ selections.
    """
    return make_tagging_pions(for_b=False)


def _photon_maker():
    return make_photons(PtCut=2000 * MeV, ConfLevelCut=0.25)


###############################################################################
# Particles combiners
###############################################################################


def _make_phi2kk():
    kaons = _make_charm_kaons()
    return ParticleCombiner(
        [kaons, kaons],
        DecayDescriptor='phi(1020) -> K+ K-',
        name="Charm_D0Tohhgamma_PhiToKpKm",
        CombinationCut=F.require_all(
            F.MASS < 2150 * MeV,
            F.MAXDOCACHI2CUT(10.),
            F.DOCA(1, 2) < 200 * um,
            F.SUM(F.PT) > 0 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2130 * MeV,
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(make_pvs()) > 10.,
        ),
    )


def make_rho2pipi():
    pions = _make_charm_pions()
    return ParticleCombiner(
        [pions, pions],
        DecayDescriptor='rho(770)0 -> pi+ pi-',
        name="Charm_D0Tohhgamma_Rho0ToPipPim",
        CombinationCut=F.require_all(
            F.MASS < 2150 * MeV,
            F.MAXDOCACHI2CUT(10.),
            F.DOCA(1, 2) < 200 * um,
            F.SUM(F.PT) > 0 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2130 * MeV,
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(make_pvs()) > 10.,
        ),
    )


def make_kstar2kpi():
    kaons = _make_charm_kaons()
    pions = _make_charm_pions()
    return ParticleCombiner(
        [kaons, pions],
        DecayDescriptor='[K*(892)~0 -> K- pi+]cc',
        name="Charm_D0Tohhgamma_Kst0bToKmPip",
        CombinationCut=F.require_all(
            F.MASS < 2150 * MeV,
            F.MAXDOCACHI2CUT(10.),
            F.DOCA(1, 2) < 200 * um,
            F.SUM(F.PT) > 0 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2130 * MeV,
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(make_pvs()) > 10.,
        ),
    )


def make_dzeros(particle1, particle2, descriptor, name):
    """Combine the two body resonances and the photon to make the D0."""
    return ParticleCombiner(
        [particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=F.require_all(
            in_range(1610 * MeV, F.MASS, 2130 * MeV),
            F.MAXDOCACHI2CUT(10.),
            F.SUM(F.PT) > 0 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1660 * MeV, F.MASS, 2100 * MeV),
            F.CHI2DOF < 12.,
            F.PT > 2000 * MeV,
            F.BPVDIRA(make_pvs()) > 0.995,
            F.BPVIPCHI2(make_pvs()) < 80,
        ),
    )


def _make_dstar_from_descriptor(dzeros,
                                descriptor,
                                name,
                                comb_delta_m_max=180 * MeV,
                                delta_m_max=163 * MeV,
                                vchi2pdof_max=15.):
    """Make D*(2010)+ -> D0 pi+ candidates starting from D0/anti-D0 candidates
    and a decay descriptor. This is a helper function and should not be used
    in lines definitions. The function `make_dstars` should be used instead.

    Args:
        dzeros (DataHandle): Input D0 and anti-D0 candidates.
        decay_descriptor (string): Decay descriptor for the combination,
                                   e.g. `[D*(2010)+ -> D0 pi+]cc`
        name (string): Name for the combiner algorithm.

    Returns:
        Output of particle filtering algorithm as particle container
        DataHandle.
    """
    assert dzeros is not None, 'dzeros must be specified'
    soft_pions = _make_charm_soft_pions()
    combination_code = F.require_all(
        F.MASS - F.CHILD(1, F.MASS) < comb_delta_m_max)
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.MASS - F.CHILD(1, F.MASS) < delta_m_max)
    return ParticleCombiner([dzeros, soft_pions],
                            DecayDescriptor=descriptor,
                            name=name,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


def make_dstars(dzeros, self_conjugate_d0_decay):
    """Make a D*(2010)+ -> D0 pi+ combination. Both self-conjugate and non
    self-conjugate D0 decays are supported.

    Args:
        dzeros (DataHandle): Input D0 and anti-D0 candidates.
        self_conjugate_d0_decay (bool): True if the D0 final state is
                                        self-conjugate and anti-D0 are not
                                        reconstructed.

    Returns:
        Output of particle filtering algorithm as particle container
        DataHandle.
    """
    if self_conjugate_d0_decay:
        dstarp = _make_dstar_from_descriptor(
            dzeros, "D*(2010)+ -> D0 pi+",
            "Charm_D0Tohhgamma_DstpToD0Pip_{hash}")
        dstarm = _make_dstar_from_descriptor(
            dzeros, "D*(2010)- -> D0 pi-",
            "Charm_D0Tohhgamma_DstmToD0Pim_{hash}")
        return ParticleContainersMerger([dstarp, dstarm])
    else:
        return _make_dstar_from_descriptor(
            dzeros, "[D*(2010)+ -> D0 pi+]cc",
            "Charm_D0Tohhgamma_DstpToD0Pip_{hash}")


###############################################################################
# Build the lines
###############################################################################

all_lines = {}


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2phigamma_line(name='Hlt2Charm_DstpToD0Pip_D0ToKmKpG',
                                        prescale=1):
    phi = _make_phi2kk()
    photons = _photon_maker()
    dzeros = make_dzeros(phi, photons, 'D0 -> phi(1020) gamma',
                         "Charm_D0Tohhgamma_D0ToKmKpG")
    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [phi, dzeros, dstars],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2rhogamma_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPipG', prescale=1):
    rho = make_rho2pipi()
    photons = _photon_maker()
    dzeros = make_dzeros(rho, photons, 'D0 -> rho(770)0 gamma',
                         "Charm_D0Tohhgamma_D0ToPimPipG")
    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [rho, dzeros, dstars],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kstgamma_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPipG', prescale=1):
    kstar = make_kstar2kpi()
    photons = _photon_maker()
    dzeros = make_dzeros(kstar, photons, '[D0 -> K*(892)~0 gamma]cc',
                         "Charm_D0Tohhgamma_D0ToKmPipG")
    dstars = make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kstar, dzeros, dstars],
        prescale=prescale,
        persistreco=True)
