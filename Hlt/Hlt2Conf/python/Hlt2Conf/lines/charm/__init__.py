# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Submodule that defines all the charm HLT2 lines."""

from . import (
    cbaryon_to_phh,
    cbaryon_to_pk,
    cbaryon_to_sl,
    ccbaryon_hadronic,
    ccbaryon_to_cbaryon_sl,
    ccbaryon_to_hyperon_dh,
    charm_to_h0x,
    d_to_etah,
    d_to_hhh,
    d_to_hhhgamma,
    d_to_ksh,
    d0_to_hh,
    d0_to_hhgamma,
    d0_to_hhhh,
    d0_to_hhpi0,
    d0_to_kshh,
    d0_to_ksks,
    detection_asymmetry_lines,
    hyperons,
    lc_to_ksh_kshhh,
    prod_xsec,
    rare_charm_lines,
    cbaryon_spectroscopy,
    d0_to_hlnux,
    dst_to_dee,
)

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(cbaryon_to_phh.all_lines)
all_lines.update(cbaryon_to_pk.all_lines)
all_lines.update(cbaryon_to_sl.all_lines)
all_lines.update(ccbaryon_hadronic.all_lines)
all_lines.update(ccbaryon_to_cbaryon_sl.all_lines)
all_lines.update(ccbaryon_to_hyperon_dh.all_lines)
all_lines.update(charm_to_h0x.all_lines)
all_lines.update(d_to_etah.all_lines)
all_lines.update(d_to_hhh.all_lines)
all_lines.update(d_to_hhhgamma.all_lines)
all_lines.update(d_to_ksh.all_lines)
all_lines.update(d0_to_hh.all_lines)
all_lines.update(d0_to_hhgamma.all_lines)
all_lines.update(d0_to_hhhh.all_lines)
all_lines.update(d0_to_hhpi0.all_lines)
all_lines.update(d0_to_hlnux.all_lines)
all_lines.update(d0_to_kshh.all_lines)
all_lines.update(d0_to_ksks.all_lines)
all_lines.update(detection_asymmetry_lines.all_lines)
all_lines.update(hyperons.all_lines)
all_lines.update(lc_to_ksh_kshhh.all_lines)
all_lines.update(prod_xsec.all_lines)
all_lines.update(rare_charm_lines.all_lines)
all_lines.update(cbaryon_spectroscopy.all_lines)
all_lines.update(dst_to_dee.all_lines)
