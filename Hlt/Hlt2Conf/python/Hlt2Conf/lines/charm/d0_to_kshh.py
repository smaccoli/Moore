###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of D0 -> KsHH HLT2 lines.

The D0 is reconstructed in the following final states:

  1. D0 -> KS pi- pi+
  2. D0 -> KS K- K+
  3. D0 -> KS K- pi+
  4. D0 -> KS K+ pi-

where the KS is always reconstructed through the KS -> pi- pi+ decay.
Each of the final states is reconstructed in four different lines, with the
following features:

  a. untagged, with a prescale of 0.1 applied
  b. promptly tagged with the D*(2010)+ -> D0 pi+ decay
  c. SL singly tagged with the B -> D0 mu- X decay
  d. SL doubly tagged with the B -> D*(2010)+ mu- X decay, where the D*(2010)+
     meson decays as at point b.

Additionally, there are separate lines for each of the above for when the KS
is reconstructed with long or with downstream pions.
Lines (a) and (b) are reconstructed both with requirements similar to those
employed during Run 2, and with new low-bias requirements which avoid
selections based on significances like the ipchi2 and fdchi2.

TODO:
- add track chi2/ndof and ghost prob selections (if needed)
- add HLT1 filtering
"""
import math
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import (GeV, MeV, mm, micrometer as um,
                                       picosecond as ps)
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.standard_particles import (make_has_rich_down_pions,
                                         make_has_rich_long_kaons,
                                         make_has_rich_long_pions)
from Hlt2Conf.algorithms_thor import (ParticleFilter, ParticleCombiner)
from .prefilters import charm_prefilters
from .taggers import make_dstars, make_tagging_muons, make_sl_bs, make_dt_bs

all_lines = {}


def make_charm_kaons():
    """Return maker for kaons filtered by thresholds common to charm decay
    product selections.
    """
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 200. * MeV,
                F.P > 1. * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PID_K > 5.,
            ), ),
    )


def make_charm_pions():
    """Return maker for pions filtered by thresholds common to charm decay
    product selections.
    """
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 200. * MeV,
                F.P > 1. * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PID_K < 5.,
            ), ),
    )


def make_charm_kaons_lowbias():
    """Return maker for kaons filtered by thresholds common to charm decay
    product low-bias selections.
    """
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 500 * MeV,
                F.P > 5 * GeV,
                F.MINIPCUT(IPCut=60 * um, Vertices=make_pvs()),
                F.PID_K > 0.,
            ), ),
    )


def make_charm_pions_lowbias():
    """Return maker for pions filtered by thresholds common to charm decay
    product low-bias selections.
    """
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 500 * MeV,
                F.P > 5 * GeV,
                F.MINIPCUT(IPCut=60 * um, Vertices=make_pvs()),
                F.PID_K < 0.,
            ), ),
    )


def make_long_pions_from_ks():
    """Return maker for pions filtered by thresholds common to LL kshort
    decay product selections.
    """
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=16., Vertices=make_pvs()),
                F.PID_K < 5.,
            ), ),
    )


def make_down_pions_from_ks():
    """Return maker for pions filtered by thresholds common to DD kshort
    decay product selections.
    """
    return ParticleFilter(
        make_has_rich_down_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 175. * MeV,
                F.P > 3. * GeV,
                F.PID_K < 5.,
            ), ),
    )


def make_kshort_ll():
    """Return Kshort with two long pions maker"""
    return ParticleCombiner(
        [make_long_pions_from_ks(),
         make_long_pions_from_ks()],
        DecayDescriptor="KS0 -> pi- pi+",
        name='Charm_D0ToKshh_KS0_LL_{hash}',
        CombinationCut=F.require_all(
            in_range(435 * MeV, F.MASS, 560 * MeV),
            F.PT > 300 * MeV,
            F.P > 3.5 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            in_range(455 * MeV, F.MASS, 540 * MeV),
            F.PT > 350 * MeV,
            F.P > 4 * GeV,
            F.CHI2DOF < 9.,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.BPVFDCHI2(make_pvs()) > 80.,
        ),
    )


def make_kshort_dd():
    """Return Kshort with two down pions maker"""
    return ParticleCombiner(
        [make_down_pions_from_ks(),
         make_down_pions_from_ks()],
        DecayDescriptor="KS0 -> pi- pi+",
        name='Charm_D0ToKshh_KS0_DD_{hash}',
        CombinationCut=F.require_all(
            in_range(415 * MeV, F.MASS, 580 * MeV),
            F.PT > 400 * MeV,
            F.P > 4.5 * GeV,
            F.SUM(F.PT) > 500 * MeV,
            F.MAXDOCACUT(2 * mm),
            F.MAXDOCACHI2CUT(12.),
        ),
        CompositeCut=F.require_all(
            in_range(435 * MeV, F.MASS, 560 * MeV),
            F.PT > 450 * MeV,
            F.P > 5 * GeV,
            F.CHI2DOF < 12.,
        ),
    )


def make_dzeros(particle1,
                particle2,
                particle3,
                decay_descriptor,
                name='Charm_D0ToKsHH_DZeros_{hash}'):
    """Return D0 maker with selection tailored for a Kshh final state.

    Args:
        particle1 (DataHandle): KS to be used in combiner.
        particle2 (DataHandle): Hadron to be used in combiner.
        particle3 (DataHandle): Hadron to be used in combiner.
        decay_descriptor (string): Decay descriptor to be reconstructed.
    """
    return ParticleCombiner(
        [particle1, particle2, particle3],
        DecayDescriptor=decay_descriptor,
        name=name,
        Combination12Cut=F.MASS < 1851 * MeV,
        CombinationCut=F.require_all(
            in_range(1740 * MeV, F.MASS, 1990 * MeV),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1764 * MeV, F.MASS, 1924 * MeV),
            F.PT > 1800 * MeV,
            F.CHI2DOF < 5.,
            F.BPVDIRA(make_pvs()) > math.cos(0.0346),
            F.BPVFDCHI2(make_pvs()) > 20.,
            F.BPVLTIME(make_pvs()) > 0.1 * ps,
        ),
    )


def make_dzeros_lowbias(particle1,
                        particle2,
                        particle3,
                        decay_descriptor,
                        name='Charm_D0ToKsHH_DZeros_LowBias_{hash}'):
    """Return D0 maker with selection tailored for a Kshh low bias final state.

    Args:
        particle1 (DataHandle): KS to be used in combiner.
        particle2 (DataHandle): Hadron to be used in combiner.
        particle3 (DataHandle): Hadron to be used in combiner.
        decay_descriptor (string): Decay descriptor to be reconstructed.
    """
    return ParticleCombiner(
        [particle1, particle2, particle3],
        DecayDescriptor=decay_descriptor,
        name=name,
        Combination12Cut=F.MASS < 1851 * MeV,
        CombinationCut=F.require_all(
            in_range(1740 * MeV, F.MASS, 1990 * MeV),
            F.MAX(F.PT) > 1200 * MeV,
            F.SUM(F.PT) > 2000 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1804 * MeV, F.MASS, 1924 * MeV),
            F.CHI2DOF < 10.,
        ),
    )


##############################################################################
# Untagged lines
##############################################################################


@register_line_builder(all_lines)
def dzero2kspimpipLL_line(name='Hlt2Charm_D0ToKsPimPip_LL', prescale=0.1):
    pions = make_charm_pions()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, pions, pions, "D0 -> KS0 pi- pi+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kspimpipDD_line(name='Hlt2Charm_D0ToKsPimPip_DD', prescale=0.1):
    pions = make_charm_pions()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, pions, pions, "D0 -> KS0 pi- pi+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kspimpipLL_lowbiasline(name='Hlt2Charm_D0ToKsPimPip_LL_LowBias',
                                 prescale=0.1):
    pions = make_charm_pions_lowbias()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros_lowbias(kshorts, pions, pions, "D0 -> KS0 pi- pi+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kspimpipDD_lowbiasline(name='Hlt2Charm_D0ToKsPimPip_DD_LowBias',
                                 prescale=0.1):
    pions = make_charm_pions_lowbias()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros_lowbias(kshorts, pions, pions, "D0 -> KS0 pi- pi+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskmkpLL_line(name='Hlt2Charm_D0ToKsKmKp_LL', prescale=0.1):
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskmkpDD_line(name='Hlt2Charm_D0ToKsKmKp_DD', prescale=0.1):
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskmkpLL_lowbiasline(name='Hlt2Charm_D0ToKsKmKp_LL_LowBias',
                               prescale=0.1):
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros_lowbias(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskmkpDD_lowbiasline(name='Hlt2Charm_D0ToKsKmKp_DD_LowBias',
                               prescale=0.1):
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros_lowbias(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskmpipLL_line(name='Hlt2Charm_D0ToKsKmPip_LL', prescale=0.1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskmpipDD_line(name='Hlt2Charm_D0ToKsKmPip_DD', prescale=0.1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskmpipLL_lowbiasline(name='Hlt2Charm_D0ToKsKmPip_LL_LowBias',
                                prescale=0.1):
    pions = make_charm_pions_lowbias()
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros_lowbias(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskmpipDD_lowbiasline(name='Hlt2Charm_D0ToKsKmPip_DD_LowBias',
                                prescale=0.1):
    pions = make_charm_pions_lowbias()
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros_lowbias(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskppimLL_line(name='Hlt2Charm_D0ToKsKpPim_LL', prescale=0.1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskppimDD_line(name='Hlt2Charm_D0ToKsKpPim_DD', prescale=0.1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskppimLL_lowbiasline(name='Hlt2Charm_D0ToKsKpPim_LL_LowBias',
                                prescale=0.1):
    pions = make_charm_pions_lowbias()
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros_lowbias(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dzero2kskppimDD_lowbiasline(name='Hlt2Charm_D0ToKsKpPim_DD_LowBias',
                                prescale=0.1):
    pions = make_charm_pions_lowbias()
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros_lowbias(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale,
    )


###############################################################################
# Prompt lines
###############################################################################


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kspimpipLL_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsPimPip_LL', prescale=1):
    pions = make_charm_pions()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, pions, pions, "D0 -> KS0 pi- pi+")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kspimpipDD_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsPimPip_DD', prescale=1):
    pions = make_charm_pions()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, pions, pions, "D0 -> KS0 pi- pi+")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kspimpipLL_lowbiasline(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsPimPip_LL_LowBias', prescale=1):
    pions = make_charm_pions_lowbias()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros_lowbias(kshorts, pions, pions, "D0 -> KS0 pi- pi+")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kspimpipDD_lowbiasline(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsPimPip_DD_LowBias', prescale=1):
    pions = make_charm_pions_lowbias()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros_lowbias(kshorts, pions, pions, "D0 -> KS0 pi- pi+")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kskmkpLL_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmKp_LL', prescale=1):
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, kaons, "D0 -> KS0 K- K+")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kskmkpDD_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmKp_DD', prescale=1):
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, kaons, "D0 -> KS0 K- K+")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kskmkpLL_lowbiasline(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmKp_LL_LowBias', prescale=1):
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros_lowbias(kshorts, kaons, kaons, "D0 -> KS0 K- K+")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kskmkpDD_lowbiasline(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmKp_DD_LowBias', prescale=1):
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros_lowbias(kshorts, kaons, kaons, "D0 -> KS0 K- K+")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pipkmksLL_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmPip_LL', prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pipkmksDD_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmPip_DD', prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pipkmksLL_lowbiasline(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmPip_LL_LowBias', prescale=1):
    pions = make_charm_pions_lowbias()
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros_lowbias(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pipkmksDD_lowbiasline(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmPip_DD_LowBias', prescale=1):
    pions = make_charm_pions_lowbias()
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros_lowbias(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pimkpksLL_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKpPim_LL', prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pimkpksDD_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKpPim_DD', prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pimkpksLL_lowbiasline(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKpPim_LL_LowBias', prescale=1):
    pions = make_charm_pions_lowbias()
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros_lowbias(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pimkpksDD_lowbiasline(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKpPim_DD_LowBias', prescale=1):
    pions = make_charm_pions_lowbias()
    kaons = make_charm_kaons_lowbias()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros_lowbias(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")

    dstars = make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale,
    )


###############################################################################
# SL single-tagged Lines
###############################################################################


@register_line_builder(all_lines)
def b2dzeromux_dzero2kspimpipLL_line(name='Hlt2Charm_B2D0MumX_D0ToKsPimPip_LL',
                                     prescale=1):
    pions = make_charm_pions()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(pions, pions, kshorts, "D0 -> KS0 pi- pi+")
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dzeromux_dzero2kspimpipDD_line(name='Hlt2Charm_B2D0MumX_D0ToKsPimPip_DD',
                                     prescale=1):
    pions = make_charm_pions()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(pions, pions, kshorts, "D0 -> KS0 pi- pi+")
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dzeromumx_dzero2kskmkpLL_line(name='Hlt2Charm_B2D0MumX_D0ToKsKmKp_LL',
                                    prescale=1):
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dzeromumx_dzero2kskmkpDD_line(name='Hlt2Charm_B2D0MumX_D0ToKsKmKp_DD',
                                    prescale=1):
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dzeromumx_dzero2pipkmksLL_line(name='Hlt2Charm_B2D0MumX_D0ToKsKmPip_LL',
                                     prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dzeromumx_dzero2pipkmksDD_line(name='Hlt2Charm_B2D0MumX_D0ToKsKmPip_DD',
                                     prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dzeromumx_dzero2pimkpksLL_line(name='Hlt2Charm_B2D0MumX_D0ToKsKpPim_LL',
                                     prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dzeromumx_dzero2pimkpksDD_line(name='Hlt2Charm_B2D0MumX_D0ToKsKpPim_DD',
                                     prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, bs],
        prescale=prescale,
    )


###############################################################################
# SL double-tagged lines
###############################################################################


@register_line_builder(all_lines)
def b2dstarpmumx_dstarp2dzeropip_dzero2kspimpipLL_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKsPimPip_LL', prescale=1):
    pions = make_charm_pions()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(pions, pions, kshorts, "D0 -> KS0 pi- pi+")
    muons = make_tagging_muons()
    bs = make_dt_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, dzeros, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dstarpmumx_dstarp2dzeropip_dzero2kspimpipDD_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKsPimPip_DD', prescale=1):
    pions = make_charm_pions()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(pions, pions, kshorts, "D0 -> KS0 pi- pi+")
    muons = make_tagging_muons()
    bs = make_dt_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, dzeros, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dstarpmumx_dstarp2dzeropip_dzero2kskmkpLL_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKsKmKp_LL', prescale=1):
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    muons = make_tagging_muons()
    bs = make_dt_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, dzeros, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dstarpmumx_dstarp2dzeropip_dzero2kskmkpDD_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKsKmKp_DD', prescale=1):
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    muons = make_tagging_muons()
    bs = make_dt_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, dzeros, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dstarpmumx_dstarp2dzeropip_dzero2pipkmksLL_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKsKmPip_LL', prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")
    muons = make_tagging_muons()
    bs = make_dt_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, dzeros, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dstarpmumx_dstarp2dzeropip_dzero2pipkmksDD_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKsKmPip_DD', prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K- pi+]cc")
    muons = make_tagging_muons()
    bs = make_dt_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, dzeros, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dstarpmumx_dstarp2dzeropip_dzero2pimkpksLL_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKsKpPim_LL', prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_ll()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")
    muons = make_tagging_muons()
    bs = make_dt_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, dzeros, bs],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b2dstarpmumx_dstarp2dzeropip_dzero2pimkpksDD_line(
        name='Hlt2Charm_B2DstpMumX_DstpToD0Pip_D0ToKsKpPim_DD', prescale=1):
    pions = make_charm_pions()
    kaons = make_charm_kaons()
    kshorts = make_kshort_dd()
    dzeros = make_dzeros(kshorts, kaons, pions, "[D0 -> KS0 K+ pi-]cc")
    muons = make_tagging_muons()
    bs = make_dt_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, kshorts, dzeros, bs],
        prescale=prescale,
    )
