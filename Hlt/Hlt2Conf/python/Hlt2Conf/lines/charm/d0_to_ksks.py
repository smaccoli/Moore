###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Lines to select the decay D*+ -> D0 pi+ with D0 -> KS0 KS0 and KS0 -> pi+ pi-.
The KS0 can be reconstructed either from long or downstream tracks, resulting
in the combinations LLLL, LLDD, DDDD.
The tight lines apply tighter cuts on the KS0 and D*+ invariant masses for
the LLLL and LLDD lines, and on the KS0 invariant masses for the DDDD line.

"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.standard_particles import make_down_pions, make_long_pions
from Hlt2Conf.algorithms import ParticleContainersMerger
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from .particle_properties import _KS_M
from .prefilters import charm_prefilters
from .taggers import make_tagging_pions

#####################################################################
### Shortcuts for filters and builders used throughout the module ###
#####################################################################


def _make_long_pions_from_ks():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.MINIPCHI2CUT(IPChi2Cut=36., Vertices=make_pvs())))


def _make_down_pions_from_ks():
    return ParticleFilter(
        make_down_pions(),
        F.FILTER(F.require_all(F.PT > 175 * MeV, F.P > 3000 * MeV)))


def _make_ll_ks(descriptor="KS0 -> pi+ pi-"):
    """Returns maker for KS0 -> pi+ pi- constructed with two long pions."""
    return ParticleCombiner(
        [_make_long_pions_from_ks(),
         _make_long_pions_from_ks()],
        DecayDescriptor=descriptor,
        name='Charm_D0ToKsKs_Ks_LL_{hash}',
        CombinationCut=F.require_all(
            in_range(_KS_M - 50 * MeV, F.MASS, _KS_M + 50 * MeV),
            F.PT > 450 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(_KS_M - 35 * MeV, F.MASS, _KS_M + 35 * MeV),
            F.CHI2DOF < 30,  # TODO: study if this cut can be tightened
            in_range(-100 * mm, F.END_VZ, 500 * mm),
            F.PT > 500 * MeV,
        ),
    )


def _make_ll_ks_tight(descriptor="KS0 -> pi+ pi-"):
    """Returns maker for KS0 -> pi+ pi- constructed with two long pions with
    a tighter mass cut.
    """
    return ParticleCombiner(
        [_make_long_pions_from_ks(),
         _make_long_pions_from_ks()],
        DecayDescriptor=descriptor,
        name='Charm_D0ToKsKs_Ks_LL_Tight_{hash}',
        CombinationCut=F.require_all(
            in_range(_KS_M - 50 * MeV, F.MASS, _KS_M + 50 * MeV),
            F.PT > 450 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(_KS_M - 30 * MeV, F.MASS, _KS_M + 30 * MeV),
            F.CHI2DOF < 30,  # TODO: study if this cut can be tightened
            in_range(-100 * mm, F.END_VZ, 500 * mm),
            F.PT > 500 * MeV,
        ),
    )


def _make_dd_ks(descriptor="KS0 -> pi+ pi-"):
    """Returns maker for KS0 -> pi+ pi- constructed with two downstream pions.
    """
    return ParticleCombiner(
        [_make_down_pions_from_ks(),
         _make_down_pions_from_ks()],
        DecayDescriptor=descriptor,
        name='Charm_D0ToKsKs_Ks_DD_{hash}',
        CombinationCut=F.require_all(
            in_range(_KS_M - 80 * MeV, F.MASS, _KS_M + 80 * MeV),
            F.PT > 450 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(_KS_M - 64 * MeV, F.MASS, _KS_M + 64 * MeV),
            F.CHI2DOF < 30,  # TODO: study if this cut can be tightened
            in_range(300 * mm, F.END_VZ, 2275 * mm),
            F.PT > 500 * MeV,
        ),
    )


def _make_dd_ks_tight(descriptor="KS0 -> pi+ pi-"):
    """Returns maker for KS0 -> pi+ pi- constructed with two downstream pions
    with a tighter mass cut.
    """
    return ParticleCombiner(
        [_make_down_pions_from_ks(),
         _make_down_pions_from_ks()],
        DecayDescriptor=descriptor,
        name='Charm_D0ToKsKs_Ks_DD_Tight_{hash}',
        CombinationCut=F.require_all(
            in_range(_KS_M - 80 * MeV, F.MASS, _KS_M + 80 * MeV),
            F.PT > 450 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(_KS_M - 60 * MeV, F.MASS, _KS_M + 60 * MeV),
            F.CHI2DOF < 30,  # TODO: study if this cut can be tightened
            in_range(300 * mm, F.END_VZ, 2275 * mm),
            F.PT > 500 * MeV,
        ),
    )


def _make_dstars(dzeros, pions, descriptor):
    """Returns maker for D*+- -> D0 pi+-."""
    return ParticleCombiner(
        [dzeros, pions],
        DecayDescriptor=descriptor,
        name='Charm_D0ToKsKs_Dstars_{hash}',
        CombinationCut=F.MASS - F.CHILD(1, F.MASS) < 175 * MeV,
        CompositeCut=F.require_all(
            F.MASS - F.CHILD(1, F.MASS) < 170 * MeV,
            F.CHI2DOF < 25.,
        ),
    )


def _make_dstars_tight(dzeros, pions, descriptor):
    """Returns maker for D*+- -> D0 pi+- with a tighter mass cut."""
    return ParticleCombiner(
        [dzeros, pions],
        DecayDescriptor=descriptor,
        name='Charm_D0ToKsKs_DstarsTight_{hash}',
        CombinationCut=F.MASS - F.CHILD(1, F.MASS) < 170 * MeV,
        CompositeCut=F.require_all(
            F.MASS - F.CHILD(1, F.MASS) < 160 * MeV,
            F.CHI2DOF < 25,
        ),
    )


#########################
### Lines definition  ###
#########################

all_lines = {}


@register_line_builder(all_lines)
def dst_to_d0pi_d0toksks_ll_line(name="Hlt2Charm_DstpToD0Pip_D0ToKsKs_LLLL"):
    pvs = make_pvs()
    kshorts = _make_ll_ks()
    dzeros = ParticleCombiner(
        [kshorts, kshorts],
        DecayDescriptor="D0 -> KS0 KS0",
        name="Charm_D0ToKsKs_D0ToKsKs_LLLL",
        CombinationCut=F.require_all(
            in_range(1730 * MeV, F.MASS, 2000 * MeV),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1775 * MeV, F.MASS, 1955 * MeV),
            F.BPVFDCHI2(pvs) > 5,
            F.CHI2DOF < 10,
            F.BPVDIRA(pvs) > 0.9994,
        ),
    )

    dstarp = _make_dstars(dzeros, make_tagging_pions(), "D*(2010)+ -> D0 pi+")
    dstarm = _make_dstars(dzeros, make_tagging_pions(), "D*(2010)- -> D0 pi-")
    dstars = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name, algs=charm_prefilters() + [kshorts, dzeros, dstars])


@register_line_builder(all_lines)
def dst_to_d0pi_d0toksks_ll_tightline(
        name="Hlt2Charm_DstpToD0Pip_D0ToKsKs_LLLL_Tight"):
    pvs = make_pvs()
    kshorts = _make_ll_ks_tight()
    dzeros = ParticleCombiner(
        [kshorts, kshorts],
        DecayDescriptor="D0 -> KS0 KS0",
        name="Charm_D0ToKsKs_D0ToKsKs_LLLL_Tight",
        CombinationCut=F.require_all(
            in_range(1730 * MeV, F.MASS, 2000 * MeV),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1775 * MeV, F.MASS, 1955 * MeV),
            F.BPVFDCHI2(pvs) > 5,
            F.CHI2DOF < 10,
            F.BPVDIRA(pvs) > 0.9994,
        ),
    )

    dstarp = _make_dstars_tight(dzeros, make_tagging_pions(),
                                "D*(2010)+ -> D0 pi+")
    dstarm = _make_dstars_tight(dzeros, make_tagging_pions(),
                                "D*(2010)- -> D0 pi-")
    dstars = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name, algs=charm_prefilters() + [kshorts, dzeros, dstars])


@register_line_builder(all_lines)
def dst_to_d0pi_d0toksks_ld_line(name="Hlt2Charm_DstpToD0Pip_D0ToKsKs_LLDD"):
    pvs = make_pvs()
    dd_kshorts = _make_dd_ks("KS0 -> pi+ pi-")
    ll_kshorts = _make_ll_ks()
    dzeros = ParticleCombiner(
        [dd_kshorts, ll_kshorts],
        DecayDescriptor="D0 -> KS0 KS0",
        AllowDiffInputsForSameIDChildren=True,
        name="Charm_D0ToKsKs_D0ToKsKs_LLDD",
        CombinationCut=F.require_all(
            in_range(1730 * MeV, F.MASS, 2000 * MeV),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1775 * MeV, F.MASS, 1955 * MeV),
            F.CHI2DOF < 10,
            F.BPVDIRA(pvs) > 0.9994,
        ),
    )

    dstarp = _make_dstars(dzeros, make_tagging_pions(), "D*(2010)+ -> D0 pi+")
    dstarm = _make_dstars(dzeros, make_tagging_pions(), "D*(2010)- -> D0 pi-")
    dstars = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, dzeros, dstars])


@register_line_builder(all_lines)
def dst_to_d0pi_d0toksks_ld_tightline(
        name="Hlt2Charm_DstpToD0Pip_D0ToKsKs_LLDD_Tight"):
    pvs = make_pvs()
    dd_kshorts = _make_dd_ks_tight("KS0 -> pi+ pi-")
    ll_kshorts = _make_ll_ks_tight()
    dzeros = ParticleCombiner(
        [dd_kshorts, ll_kshorts],
        DecayDescriptor="D0 -> KS0 KS0",
        AllowDiffInputsForSameIDChildren=True,
        name="Charm_D0ToKsKs_D0ToKsKs_LLDD_Tight",
        CombinationCut=F.require_all(
            in_range(1730 * MeV, F.MASS, 2000 * MeV),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1775 * MeV, F.MASS, 1955 * MeV),
            F.CHI2DOF < 10,
            F.BPVDIRA(pvs) > 0.9994,
        ),
    )

    dstarp = _make_dstars_tight(dzeros, make_tagging_pions(),
                                "D*(2010)+ -> D0 pi+")
    dstarm = _make_dstars_tight(dzeros, make_tagging_pions(),
                                "D*(2010)- -> D0 pi-")
    dstars = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, dzeros, dstars])


@register_line_builder(all_lines)
def dst_to_d0pi_d0toksks_dd_line(name="Hlt2Charm_DstpToD0Pip_D0ToKsKs_DDDD"):
    pvs = make_pvs()
    dd_kshorts = _make_dd_ks()
    dzeros = ParticleCombiner(
        [dd_kshorts, dd_kshorts],
        DecayDescriptor="D0 -> KS0 KS0",
        name="Charm_D0ToKsKs_D0ToKsKs_DDDD",
        CombinationCut=F.require_all(
            in_range(1730 * MeV, F.MASS, 2000 * MeV),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1775 * MeV, F.MASS, 1955 * MeV),
            F.CHI2DOF < 10,
            F.BPVDIRA(pvs) > 0.9994,
        ),
    )

    dstarp = _make_dstars(dzeros, make_tagging_pions(), "D*(2010)+ -> D0 pi+")
    dstarm = _make_dstars(dzeros, make_tagging_pions(), "D*(2010)- -> D0 pi-")
    dstars = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, dzeros, dstars])


@register_line_builder(all_lines)
def dst_to_d0pi_d0toksks_dd_tightline(
        name="Hlt2Charm_DstpToD0Pip_D0ToKsKs_DDDD_Tight"):
    pvs = make_pvs()
    dd_kshorts = _make_dd_ks_tight()
    dzeros = ParticleCombiner(
        [dd_kshorts, dd_kshorts],
        DecayDescriptor="D0 -> KS0 KS0",
        name="Charm_D0ToKsKs_D0ToKsKs_DDDD_Tight",
        CombinationCut=F.require_all(
            in_range(1730 * MeV, F.MASS, 2000 * MeV),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1775 * MeV, F.MASS, 1955 * MeV),
            F.CHI2DOF < 10,
            F.BPVDIRA(pvs) > 0.9994,
        ),
    )

    dstarp = _make_dstars_tight(dzeros, make_tagging_pions(),
                                "D*(2010)+ -> D0 pi+")
    dstarm = _make_dstars_tight(dzeros, make_tagging_pions(),
                                "D*(2010)- -> D0 pi-")
    dstars = ParticleContainersMerger([dstarp, dstarm])

    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, dzeros, dstars])
