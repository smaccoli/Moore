###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of charm HLT2 lines related to measurements of the
reconstrction asymmetry of K-Pi+ and Pi-Pi+ (and K-K+) pairs.

Final states built are:

 1. D+ -> Ks0 pi+       Hlt2Charm_DpToKsPip_LL_ADet
 2. D+ -> K- pi+ pi+    Hlt2Charm_DpToKmPipPip_ADet
 3. Ds+ -> Ks0 K+       Hlt2Charm_DspToKsKp_LL_ADet
 4. Ds+ -> K- K+ pi+    Hlt2Charm_DspToKmKpPip_ADet
 5. Ds+ -> pi+ pi- pi+  Hlt2Charm_DspToPimPipPip_ADet
 6. Lc -> Ks0 p         Hlt2Charm_LcpToPpKs_LL_ADet
 7. Lc -> p K- pi+      Hlt2Charm_LcpToPpKmPip_ADet

All of these include their charge conjugate. As requirements on the
D related to the IPCHI2 and HLT1 conditions are enforced (when they
become available in the selection framework), looser requirements
on the pT of the charm daughters can be used.

TODO: add HLT1 filtering once it is possible
"""
import math
from GaudiKernel.SystemOfUnits import GeV, MeV, mrad, picosecond, mm
from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons,
                                         make_has_rich_long_protons)
import Functors as F
from Functors.math import in_range
from .prefilters import charm_prefilters

all_lines = {}


@configurable
def get_shared_detached_basic_particles(particles,
                                        pvs,
                                        trchi2ndf_max=99.,
                                        min_ipchi2_all_pvs=10.,
                                        pt_min=250 * MeV,
                                        p_min=4 * GeV,
                                        eta_max=5.1,
                                        eta_min=1.9):
    cut_list = F.require_all(F.PT > pt_min, F.P > p_min, F.ETA >= eta_min,
                             F.ETA <= eta_max, F.CHI2DOF < trchi2ndf_max,
                             F.MINIPCHI2(pvs) > min_ipchi2_all_pvs)

    return ParticleFilter(particles, Cut=F.FILTER(cut_list))


@configurable
def make_pions_for_kshort(detached_particles, pvs, max_pidk=10.,
                          min_ipchi2=20):
    """ These pions are used to construct KS -> pi pi candidates """
    requirements = F.require_all(F.PID_K < max_pidk,
                                 F.MINIPCHI2(pvs) > min_ipchi2)

    return ParticleFilter(detached_particles, Cut=F.FILTER(requirements))


@configurable
def make_probe_pions(detached_particles, max_pidk=10.):
    """ These pions originate directly from the charm, e.g. D+ -> K- pi+ ^pi+.
    As these are pions of which the asymmetry is measured, the
    cuts should be as loose as possible. """
    return ParticleFilter(detached_particles, Cut=F.FILTER(F.PID_K < max_pidk))


@configurable
def make_bachelor_pions(detached_particles, max_pidk=5.):
    """ These pions originate directly from the charm decay, but are
    typically matched to another. The cuts can be more stringent than
    the probe pions. """
    return ParticleFilter(detached_particles, Cut=F.FILTER(F.PID_K < max_pidk))


@configurable
def make_probe_kaons(detached_particles, min_pidk=-10.):
    """ These pions originate directly from the charm, e.g.
    Ds+ -> KS ^K+. These are used to probe the asymmetry of,
    so cuts have to be as loose as possible """
    return ParticleFilter(detached_particles, Cut=F.FILTER(F.PID_K > min_pidk))


@configurable
def make_bachelor_protons(detached_particles, pvs, min_pidp=5., min_ipchi2=20):
    """ These protons originate directly from the charm,
    e.g. Lc -> ^p K- pi+. As these are protons used to tag, not used
    to measure an asymmetry with, cuts can be tighter than for
    probe particles. """
    requirements = F.require_all(F.PID_P > min_pidp,
                                 F.MINIPCHI2(pvs) > min_ipchi2)
    return ParticleFilter(detached_particles, Cut=F.FILTER(requirements))


@configurable
def make_kshorts_to_pipi(pions_plus,
                         pions_minus,
                         pvs,
                         am_min=390 * MeV,
                         am_max=600 * MeV,
                         mass_min=440 * MeV,
                         mass_max=560 * MeV,
                         child_pt_min=250 * MeV,
                         doca_max=0.2 * mm,
                         fdchi2_pv_min=50,
                         vchi2pdof_max=10,
                         pt_min=0.4 * GeV):
    """ Composes KS0 -> pi+ pi- decays from pions, to be used later
    in Ds / D+ selections.
    """
    combination_cuts = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_min,
        F.MIN(F.PT) > child_pt_min, F.MAXDOCACUT(doca_max))

    vertex_cuts = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                in_range(mass_min, F.MASS, mass_max),
                                F.BPVFDCHI2(pvs) > fdchi2_pv_min,
                                F.PT > pt_min)
    return ParticleCombiner([pions_plus, pions_minus],
                            name='Charm_ADet_Ks_PipPim_Combiner_{hash}',
                            DecayDescriptor="KS0 -> pi+ pi-",
                            CombinationCut=combination_cuts,
                            CompositeCut=vertex_cuts)


@configurable
def make_prompt_dplus_to_kspi(kshorts,
                              bachelor_pions,
                              pvs,
                              suffix,
                              am_min=1789 * MeV,
                              am_max=1949 * MeV,
                              amaxchild_pt_min=1250 * MeV,
                              trk_2of3_pt_min=250 * MeV,
                              aptsum_min=1000 * MeV,
                              amaxchild_ipchi2_min=50,
                              trk_2of3_ipchi2_min=40,
                              vchi2pdof_max=10,
                              fdchi2_pv_min=150,
                              doca_max=0.25 * mm,
                              bpvltime_min=0.5 * picosecond,
                              acos_bpvdira_max=14.1 * mrad,
                              min_fd_kshort=2 * mm,
                              max_ipchi2_best_pv=100):
    """ Composes a D+ -> KS pi+ (CF) decay from kshorts,
    pions with minimal PID cuts, but with a cut on HLT1 TOS
    (not implemented yet) and IPCHI2.
    """
    combination_cuts = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAX(F.PT) > amaxchild_pt_min,
        F.SUM(F.PT) > aptsum_min,
        ((F.CHILD(1, F.CHILD(1, F.PT)) > trk_2of3_pt_min) +
         (F.CHILD(1, F.CHILD(2, F.PT)) > trk_2of3_pt_min) +
         (F.CHILD(2, F.PT) > trk_2of3_pt_min)) >
        1,  # to be replaced with INTREE
        F.MAX(F.MINIPCHI2(pvs)) > amaxchild_ipchi2_min,
        ((F.CHILD(1, F.CHILD(1, F.MINIPCHI2(pvs))) > trk_2of3_ipchi2_min) +
         (F.CHILD(1, F.CHILD(2, F.MINIPCHI2(pvs))) > trk_2of3_ipchi2_min) +
         (F.CHILD(2, F.MINIPCHI2(pvs)) > trk_2of3_ipchi2_min)) >
        1,  # to be replaced with INTREE
        F.MAXDOCACUT(doca_max),
    )

    cos_bpvdira_min = math.cos(acos_bpvdira_max)
    vertex_cuts = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > cos_bpvdira_min,
        F.BPVFDCHI2(pvs) > fdchi2_pv_min,
        F.MINIPCHI2(pvs) < max_ipchi2_best_pv,
        F.BPVLTIME(pvs) > bpvltime_min,
        ((F.CHILD(1, F.END_VX) - F.END_VX)**2 +
         (F.CHILD(1, F.END_VY) - F.END_VY)**2 +
         (F.CHILD(1, F.END_VZ) - F.END_VZ)**2) > min_fd_kshort**2

        # TODO below are cuts that are required for data taking
        # but are not supported by THOR

        # F.VDCHI2( CHILD(1), CHILD(0) ) > 10
        # this should be replaced with a FD CHI2 by the
        # time it's actually there...
    )

    return ParticleCombiner(
        [kshorts, bachelor_pions],
        name='Charm_ADet_DpToKsPip_Combiner_{hash}' + suffix,
        DecayDescriptor="[D+ -> KS0 pi+]cc",
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts)


@configurable
def make_prompt_ds_to_ksk(kshorts,
                          bachelor_kaons,
                          pvs,
                          suffix,
                          am_min=(1970 - 90) * MeV,
                          am_max=(1970 + 90) * MeV,
                          amaxchild_pt_min=1250 * MeV,
                          trk_2of3_pt_min=250 * MeV,
                          aptsum_min=1000 * MeV,
                          amaxchild_ipchi2_min=50,
                          trk_2of3_ipchi2_min=30,
                          vchi2pdof_max=10,
                          fdchi2_pv_min=150,
                          doca_max=0.25 * mm,
                          bpvltime_min=0.4 * picosecond,
                          acos_bpvdira_max=14.1 * mrad,
                          min_fd_kshort=2 * mm,
                          max_ipchi2_best_pv=100):
    """ Composes a D+ -> Ks K (CF) decay from kshorts, kaons
    with minimal PID cuts, but with a cut on HLT1 TOS (not implemented yet)
    and IPCHI2.
    """
    combination_cuts = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAX(F.PT) > amaxchild_pt_min,
        F.SUM(F.PT) > aptsum_min,
        ((F.CHILD(1, F.CHILD(1, F.PT)) > trk_2of3_pt_min) +
         (F.CHILD(1, F.CHILD(2, F.PT)) > trk_2of3_pt_min) +
         (F.CHILD(2, F.PT) > trk_2of3_pt_min)) >
        1,  # to be replaced with INTREE
        F.MAX(F.MINIPCHI2(pvs)) > amaxchild_ipchi2_min,
        ((F.CHILD(1, F.CHILD(1, F.MINIPCHI2(pvs))) > trk_2of3_ipchi2_min) +
         (F.CHILD(1, F.CHILD(2, F.MINIPCHI2(pvs))) > trk_2of3_ipchi2_min) +
         (F.CHILD(2, F.MINIPCHI2(pvs)) > trk_2of3_ipchi2_min)) >
        1,  # to be replaced with INTREE
        F.MAXDOCACUT(doca_max),
    )

    cos_bpvdira_min = math.cos(acos_bpvdira_max)
    vertex_cuts = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > cos_bpvdira_min,
        F.BPVFDCHI2(pvs) > fdchi2_pv_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.MINIPCHI2(pvs) < max_ipchi2_best_pv,
        ((F.CHILD(1, F.END_VX) - F.END_VX)**2 +  # FD of KS
         (F.CHILD(1, F.END_VY) - F.END_VY)**2 +
         (F.CHILD(1, F.END_VZ) - F.END_VZ)**2) > min_fd_kshort**2

        # TODO below are cuts that are required for data taking
        # but are not supported by THOR

        # F.VDCHI2( CHILD(1), CHILD(0) ) > 10
        # this should be replaced with a FD CHI2 by the
        # time it's actually there...
    )

    return ParticleCombiner(
        [kshorts, bachelor_kaons],
        name='Charm_ADet_DspToKsK_Combiner__{hash}' + suffix,
        DecayDescriptor="[D_s+ -> KS0 K+]cc",
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts)


@configurable
def make_prompt_lc_to_ksproton(kshorts,
                               bachelor_protons,
                               pvs,
                               am_min=(2286 - 90) * MeV,
                               am_max=(2286 + 90) * MeV,
                               amaxchild_pt_min=2000 * MeV,
                               trk_2of3_pt_min=250 * MeV,
                               aptsum_min=1000 * MeV,
                               amaxchild_ipchi2_min=50,
                               trk_2of3_ipchi2_min=30,
                               vchi2pdof_max=10,
                               fdchi2_pv_min=150,
                               doca_max=0.25 * mm,
                               bpvltime_min=0.4 * picosecond,
                               acos_bpvdira_max=14.1 * mrad,
                               min_fd_kshort=2 * mm,
                               max_ipchi2_best_pv=100):
    """ Composes a Lc -> KS p (CF) decay from kshorts, with
    a cut on HLT1 TOS (not implemented yet) and IPCHI2.
    """
    combination_cuts = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAX(F.PT) > amaxchild_pt_min,
        F.SUM(F.PT) > aptsum_min,
        ((F.CHILD(1, F.CHILD(1, F.PT)) > trk_2of3_pt_min) +
         (F.CHILD(1, F.CHILD(2, F.PT)) > trk_2of3_pt_min) +
         (F.CHILD(2, F.PT) > trk_2of3_pt_min)) >
        1,  # to be replaced with INTREE
        F.MAX(F.MINIPCHI2(pvs)) > amaxchild_ipchi2_min,
        ((F.CHILD(1, F.CHILD(1, F.MINIPCHI2(pvs))) > trk_2of3_ipchi2_min) +
         (F.CHILD(1, F.CHILD(2, F.MINIPCHI2(pvs))) > trk_2of3_ipchi2_min) +
         (F.CHILD(2, F.MINIPCHI2(pvs)) > trk_2of3_ipchi2_min)) >
        1,  # to be replaced with INTREE
        F.MAXDOCACUT(doca_max))

    cos_bpvdira_min = math.cos(acos_bpvdira_max)
    vertex_cuts = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > cos_bpvdira_min,
        F.BPVFDCHI2(pvs) > fdchi2_pv_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.MINIPCHI2(pvs) < max_ipchi2_best_pv,
        ((F.CHILD(1, F.END_VX) - F.END_VX)**2 +  # FD of KS
         (F.CHILD(1, F.END_VY) - F.END_VY)**2 +
         (F.CHILD(1, F.END_VZ) - F.END_VZ)**2) > min_fd_kshort**2

        # TODO below are cuts that are required for data taking
        # but are not supported by THOR

        # F.VDCHI2( CHILD(1), CHILD(0) ) > 10
        # this should be replaced with a FD CHI2
        # by the time it's actually there...
    )

    return ParticleCombiner([kshorts, bachelor_protons],
                            name='Charm_ADet_LcToKsPp_Combiner_{hash}',
                            DecayDescriptor="[Lambda_c+ -> KS0 p+]cc",
                            CombinationCut=combination_cuts,
                            CompositeCut=vertex_cuts)


@configurable
def make_prompt_dplus_to_kpipi(kaons,
                               soft_pion,
                               pvs,
                               am_min=1789 * MeV,
                               am_max=1949 * MeV,
                               amaxchild_pt_min=1250 * MeV,
                               trk_2of3_pt_min=250 * MeV,
                               aptsum_min=1000 * MeV,
                               amaxchild_ipchi2_min=50,
                               trk_2of3_ipchi2_min=40,
                               bachelor_pion_pidk_max=5.,
                               vchi2pdof_max=10,
                               fdchi2_pv_min=150,
                               doca_max=0.25 * mm,
                               bpvltime_min=0.5 * picosecond,
                               acos_bpvdira_max=14.1 * mrad,
                               max_ipchi2_best_pv=100):
    """ Composes a D+ -> K- pi+ pi+ (CF) decay from kaons, pions
    with minimal PID cuts, but with a cut on HLT1 TOS
    (not implemented yet) and IPCHI2.

    A lower pT threshold is used in comparison to the
    standard d0_hhh lines to provide a better agreement in
    the weighting procedure relevant for the KPi asymmetry
    (i.e. a better match with D+ -> Ks pi decays)
    """
    combination_cut = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAX(F.PT) > amaxchild_pt_min,
        F.SUM(F.PT > trk_2of3_pt_min) > 1,
        F.SUM(F.PT) > aptsum_min, F.MAXDOCACUT(doca_max),
        F.MAX(F.MINIPCHI2(pvs)) > amaxchild_ipchi2_min,
        F.SUM(F.MINIPCHI2(pvs) > trk_2of3_ipchi2_min) > 1,
        F.CHILD(1, F.PID_K < bachelor_pion_pidk_max))

    cos_bpvdira_min = math.cos(acos_bpvdira_max)
    vertex_cuts = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > cos_bpvdira_min,
        F.BPVFDCHI2(pvs) > fdchi2_pv_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.MINIPCHI2(pvs) < max_ipchi2_best_pv,
    )

    return ParticleCombiner([kaons, soft_pion, soft_pion],
                            DecayDescriptor="[D+ -> K- pi+ pi+]cc",
                            name='Charm_ADet_DpToKpipi_Combiner_{hash}',
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cuts)


@configurable
def make_prompt_ds_to_kkpi(kaons,
                           pions,
                           pvs,
                           am_min=(1970 - 90) * MeV,
                           am_max=(1970 + 90) * MeV,
                           amaxchild_pt_min=1250 * MeV,
                           trk_2of3_pt_min=250 * MeV,
                           aptsum_min=1000 * MeV,
                           amaxchild_ipchi2_min=50,
                           trk_2of3_ipchi2_min=40,
                           vchi2pdof_max=10,
                           fdchi2_pv_min=150,
                           doca_max=0.25 * mm,
                           bpvltime_min=0.4 * picosecond,
                           acos_bpvdira_max=14.1 * mrad,
                           max_ipchi2_best_pv=100,
                           phi_mass_min=(1020 - 50) * MeV,
                           phi_mass_max=(1020 + 50) * MeV,
                           kstar_mass_min=(890 - 90) * MeV,
                           kstar_mass_max=(890 + 90) * MeV):
    """ Composes a Ds+ -> K- K+ pi+ (CF) decay from kaons,
    pions with minimal PID cuts, but with a cut on HLT1 TOS
    (not implemented yet) and IPCHI2.

    TODO: Investigate whether it's really needed to only accept
    K* or phi events
    """
    combination_cut = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAX(F.PT) > amaxchild_pt_min,
        F.SUM(F.PT > trk_2of3_pt_min) > 1,
        F.SUM(F.PT) > aptsum_min, F.MAXDOCACUT(doca_max),
        F.MAX(F.MINIPCHI2(pvs)) > amaxchild_ipchi2_min,
        F.SUM(F.MINIPCHI2(pvs) > trk_2of3_ipchi2_min) > 1, (F.SUBCOMB(
            Functor=in_range(phi_mass_min, F.MASS, phi_mass_max),
            Indices=[1, 2]) | F.SUBCOMB(
                Functor=in_range(kstar_mass_min, F.MASS, kstar_mass_max),
                Indices=[2, 3])))

    cos_bpvdira_min = math.cos(acos_bpvdira_max)
    vertex_cuts = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > cos_bpvdira_min,
        F.BPVFDCHI2(pvs) > fdchi2_pv_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.MINIPCHI2(pvs) < max_ipchi2_best_pv,
    )

    return ParticleCombiner([kaons, kaons, pions],
                            DecayDescriptor="[D_s+ -> K- K+ pi+]cc",
                            name='Charm_ADet_DspToKmKpiPip_Combiner_{hash}',
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cuts)


@configurable
def make_prompt_ds_to_pipipi(pions,
                             pvs,
                             am_min=(1970 - 90) * MeV,
                             am_max=(1970 + 90) * MeV,
                             amaxchild_pt_min=1250 * MeV,
                             trk_2of3_pt_min=400 * MeV,
                             aptsum_min=1000 * MeV,
                             amaxchild_ipchi2_min=50,
                             trk_2of3_ipchi2_min=40,
                             vchi2pdof_max=10,
                             fdchi2_pv_min=150,
                             doca_max=0.25 * mm,
                             bpvltime_min=0.4 * picosecond,
                             acos_bpvdira_max=14.1 * mrad,
                             max_ipchi2_best_pv=100,
                             d_eta_max=5.0,
                             d_pt_min=2500 * MeV,
                             d_pt_max=25 * GeV):
    """ Composes a Ds+ -> pi- pi+ pi+ (CF) decay from pions
    with minimal PID cuts, but with a cut on HLT1 TOS
    (not implemented yet) and IPCHI2.
    """
    combination_cut = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAX(F.PT) > amaxchild_pt_min,
        F.SUM(F.PT > trk_2of3_pt_min) > 1,
        F.SUM(F.PT) > aptsum_min,
        F.MAXDOCACUT(doca_max),
        F.MAX(F.MINIPCHI2(pvs)) > amaxchild_ipchi2_min,
        F.SUM(F.MINIPCHI2(pvs) > trk_2of3_ipchi2_min) > 1,
    )

    cos_bpvdira_min = math.cos(acos_bpvdira_max)
    vertex_cuts = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > cos_bpvdira_min,
        F.BPVFDCHI2(pvs) > fdchi2_pv_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.ETA < d_eta_max,
        in_range(d_pt_min, F.PT, d_pt_max),
        F.MINIPCHI2(pvs) < max_ipchi2_best_pv,
    )

    return ParticleCombiner([pions, pions, pions],
                            DecayDescriptor="[D_s+ -> pi- pi+ pi+]cc",
                            name='Charm_ADet_DspToPimPipPip_Combiner_{hash}',
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cuts)


@configurable
def make_prompt_lc_to_pkpi(protons,
                           kaons,
                           pions,
                           pvs,
                           am_min=(2286 - 90) * MeV,
                           am_max=(2286 + 90) * MeV,
                           amaxchild_pt_min=1500 * MeV,
                           trk_2of3_pt_min=250 * MeV,
                           aptsum_min=1000 * MeV,
                           amaxchild_ipchi2_min=50,
                           trk_2of3_ipchi2_min=40,
                           vchi2pdof_max=10,
                           fdchi2_pv_min=150,
                           doca_max=0.25 * mm,
                           bpvltime_min=0.4 * picosecond,
                           acos_bpvdira_max=14.1 * mrad,
                           max_ipchi2_best_pv=100):
    """ Composes a Lc+ -> p K- pi+ (CF) decay from protons, kaons,
    pions with minimal PID cuts, but with a cut on HLT1 TOS
    (not implemented yet) and IPCHI2.
    """
    combination_cut = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAX(F.PT) > amaxchild_pt_min,
        F.SUM(F.PT > trk_2of3_pt_min) > 1,
        F.SUM(F.PT) > aptsum_min,
        F.MAXDOCACUT(doca_max),
        F.MAX(F.MINIPCHI2(pvs)) > amaxchild_ipchi2_min,
        F.SUM(F.MINIPCHI2(pvs) > trk_2of3_ipchi2_min) > 1,
    )

    cos_bpvdira_min = math.cos(acos_bpvdira_max)
    vertex_cuts = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > cos_bpvdira_min,
        F.BPVFDCHI2(pvs) > fdchi2_pv_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.MINIPCHI2(pvs) < max_ipchi2_best_pv,
    )

    return ParticleCombiner([protons, kaons, pions],
                            DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
                            name='Charm_ADet_LcpToPpKmPip_Combiner_{hash}',
                            CombinationCut=combination_cut,
                            CompositeCut=vertex_cuts)


# D+ lines
@register_line_builder(all_lines)
def d2kspi_line(name="Hlt2Charm_DpToKsPip_LL_ADet", prescale=1):
    pvs = make_pvs()

    my_detached_pions = get_shared_detached_basic_particles(
        make_has_rich_long_pions(), pvs)
    pions_for_ks = make_pions_for_kshort(my_detached_pions, pvs)
    bachelor_pions = make_bachelor_pions(my_detached_pions)

    my_kshorts = make_kshorts_to_pipi(pions_for_ks, pions_for_ks, pvs)
    my_dplus = make_prompt_dplus_to_kspi(
        my_kshorts, bachelor_pions, pvs, suffix="_for_" + name)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [my_kshorts, my_dplus],
        prescale=prescale)


@register_line_builder(all_lines)
def d2kpipi_line(name="Hlt2Charm_DpToKmPipPip_ADet", prescale=0.3):
    pvs = make_pvs()

    my_detached_pions = get_shared_detached_basic_particles(
        make_has_rich_long_pions(), pvs)
    probe_pions = make_probe_pions(my_detached_pions)

    my_detached_kaons = get_shared_detached_basic_particles(
        make_has_rich_long_kaons(), pvs)
    probe_kaons = make_probe_kaons(my_detached_kaons)

    my_dplus = make_prompt_dplus_to_kpipi(probe_kaons, probe_pions, pvs)

    return Hlt2Line(
        name=name, algs=charm_prefilters() + [my_dplus], prescale=prescale)


# Ds lines
@register_line_builder(all_lines)
def ds2ksk_line(name="Hlt2Charm_DspToKsKp_LL_ADet", prescale=1):
    pvs = make_pvs()

    my_detached_pions = get_shared_detached_basic_particles(
        make_has_rich_long_pions(), pvs)
    pions_for_ks = make_pions_for_kshort(my_detached_pions, pvs)
    my_kshorts = make_kshorts_to_pipi(pions_for_ks, pions_for_ks, pvs)

    my_detached_kaons = get_shared_detached_basic_particles(
        make_has_rich_long_kaons(), pvs)
    probe_kaons = make_probe_kaons(my_detached_kaons)

    my_ds = make_prompt_ds_to_ksk(my_kshorts, probe_kaons, pvs, "_for_" + name)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [my_kshorts, my_ds],
        prescale=prescale)


@register_line_builder(all_lines)
def ds2kkpi_line(name="Hlt2Charm_DspToKmKpPip_ADet", prescale=0.3):
    pvs = make_pvs()

    my_detached_pions = get_shared_detached_basic_particles(
        make_has_rich_long_pions(), pvs)
    probe_pions = make_probe_pions(my_detached_pions)

    my_detached_kaons = get_shared_detached_basic_particles(
        make_has_rich_long_kaons(), pvs)
    tag_kaons = make_probe_kaons(my_detached_kaons)

    my_ds = make_prompt_ds_to_kkpi(tag_kaons, probe_pions, pvs)

    return Hlt2Line(
        name=name, algs=charm_prefilters() + [my_ds], prescale=prescale)


@register_line_builder(all_lines)
def ds2pipipi_line(name="Hlt2Charm_DspToPimPipPip_ADet", prescale=0.3):
    pvs = make_pvs()

    my_detached_pions = get_shared_detached_basic_particles(
        make_has_rich_long_pions(), pvs)
    probe_pions = make_probe_pions(my_detached_pions)

    my_ds = make_prompt_ds_to_pipipi(probe_pions, pvs)

    return Hlt2Line(
        name=name, algs=charm_prefilters() + [my_ds], prescale=prescale)


# Lambda_c lines
@register_line_builder(all_lines)
def Lc2ksp_line(name="Hlt2Charm_LcpToPpKs_LL_ADet", prescale=1):
    pvs = make_pvs()

    my_detached_pions = get_shared_detached_basic_particles(
        make_has_rich_long_pions(), pvs)
    pions_for_ks = make_pions_for_kshort(my_detached_pions, pvs)
    my_kshorts = make_kshorts_to_pipi(pions_for_ks, pions_for_ks, pvs)

    my_detached_protons = get_shared_detached_basic_particles(
        make_has_rich_long_protons(), pvs)
    bachelor_protons = make_bachelor_protons(my_detached_protons, pvs)

    my_lc = make_prompt_lc_to_ksproton(my_kshorts, bachelor_protons, pvs)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [my_kshorts, my_lc],
        prescale=prescale)


@register_line_builder(all_lines)
def Lc2pkpi_line(name="Hlt2Charm_LcpToPpKmPip_ADet", prescale=0.3):
    pvs = make_pvs()

    my_detached_pions = get_shared_detached_basic_particles(
        make_has_rich_long_pions(), pvs)
    probe_pions = make_probe_pions(my_detached_pions)

    my_detached_kaons = get_shared_detached_basic_particles(
        make_has_rich_long_kaons(), pvs)
    probe_kaons = make_probe_kaons(my_detached_kaons)

    my_detached_protons = get_shared_detached_basic_particles(
        make_has_rich_long_protons(), pvs)
    bachelor_protons = make_bachelor_protons(my_detached_protons, pvs)

    my_lc = make_prompt_lc_to_pkpi(bachelor_protons, probe_kaons, probe_pions,
                                   pvs)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [bachelor_protons, my_lc],
        prescale=prescale)
