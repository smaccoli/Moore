###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Lines for SL decays for single chamed baryons

  1. Lambda_c+ / Xi_c+ -> Lambda0(LL/DD)     mu nu (Hlt2Charm_LcpXicpToL0MupNu_LL and Hlt2Charm_LcpXicpToL0MupNu_DD)
  2. Lambda_c+ / Xi_c+ -> p+  h-(pi-/K-)     mu nu (Hlt2Charm_LcpXicpToPpKmMupNu and Hlt2Charm_LcpXicpToPpPimMupNu)
  3. Omega_c0          -> Omega-(LL/DD)L     mu nu (Hlt2Charm_Oc0ToOmMupNu_LLL and Hlt2Charm_Oc0ToOmMupNu_DDL)
  4. Xi_c0 / Omega_c0  -> Xi-(LL/DD)L        mu nu (Hlt2Charm_Xic0Oc0ToXimMupNu_LLL and Hlt2Charm_Xic0Oc0ToXimMupNu_DDL)
  5. Xi_c+             -> Xi-(LL/DD)L    pi+ mu nu (Hlt2Charm_XicpToXimPipMupNu_LLL and Hlt2Charm_XicpToXimPipMupNu_DDL)
  6. Xi_c0 / Omega_c0  -> Lambda0(LL/DD) K-  mu nu (Hlt2Charm_Xic0Oc0ToL0KmMupNu_LL and Hlt2Charm_Xic0Oc0ToL0KmMupNu_DD)

where the daughter baryon is reconstructed via:
  Lambda0 -> p pi-  (LL/DD)
  Xi-     -> Lambda0(LL/DD) pi- (L)
  Omega-  -> Lambda0(LL/DD) K-  (L)

The following channels can be used as the normalization channels:
  Lambda_c+ -> Lambda0(LL/DD) pi+
  Lambda_c+ -> p+  h-(pi-/K-) pi+
  Omega_c0  -> Omega-(LL/DD)L pi+
  Omega_c0  -> Xi-(LL/DD)L    pi+
  Xi_c+     -> Lambda0(LL/DD) pi+
  Xi_c0     -> Xi-(LL/DD)L    pi+

TODO:
    add requirements on tracks chi2/ndf
    add HLT1 filtering
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

import Functors as F
from Functors.math import in_range
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.standard_particles import (
    make_long_pions, make_long_kaons, make_down_pions, make_down_protons,
    make_has_rich_long_kaons, make_has_rich_long_pions, make_long_protons,
    make_has_rich_long_protons, make_ismuon_long_muon)
from Hlt2Conf.algorithms_thor import (ParticleFilter, ParticleCombiner)
from .prefilters import charm_prefilters

all_lines = {}


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


###############################################################################
# Track filters
###############################################################################


def _filter_long_pions():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_K < 5.), ),
    )


def _filter_long_kaons():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 300 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_K > 5.), ),
    )


def _filter_long_protons():
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 500 * MeV,
                F.P > 3 * GeV,
                _MIPCHI2_MIN(4.),
                F.PID_P > 5.,
                F.PID_P - F.PID_K > 0.,
            ), ),
    )


def _filter_long_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 10 * GeV,
                _MIPCHI2_MIN(4.),
                F.PID_MU > 0.,
            ), ),
    )


def _filter_tight_long_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 1 * GeV,
                F.P > 10 * GeV,
                _MIPCHI2_MIN(9.),
                F.PID_MU > 3.,
                #trghostprob_max=None, # TODO
            ), ),
    )


def _filter_long_pions_from_lambda():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(
            F.PT > 100 * MeV,
            _MIPCHI2_MIN(36.),
        ), ),
    )


def _filter_long_protons_from_lambda():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 1 * GeV,
                _MIPCHI2_MIN(6.),
                F.PID_P > 5.,
                F.PID_P - F.PID_K > 0.,
            ), ),
    )


def _filter_down_pions_from_lambda():
    return ParticleFilter(
        make_down_pions(),
        F.FILTER(F.require_all(
            F.PT > 100 * MeV,
            F.P > 1 * GeV,
        ), ),
    )


def _filter_down_protons_from_lambda():
    return ParticleFilter(
        make_down_protons(),
        F.FILTER(F.require_all(
            F.PT > 300 * MeV,
            F.P > 5 * GeV,
        ), ),
    )


def _filter_long_pions_from_xi():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(
            F.PT > 100 * MeV,
            _MIPCHI2_MIN(8.),
        ), ),
    )


def _filter_long_kaons_from_omega():
    return ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 180 * MeV,
                F.P > 3 * GeV,
                _MIPCHI2_MIN(9.),
                F.PID_K > -2.,
            ), ),
    )


###############################################################################
# Basic combiners
###############################################################################


def make_lambdall():
    """Make Lambda -> p+ pi- from long tracks."""
    protons = _filter_long_protons_from_lambda()
    pions = _filter_long_pions_from_lambda()
    return ParticleCombiner(
        [protons, pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name='Charm_CBaryonToSl_make_lambdall_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1160 * MeV,
            F.MAXDOCACUT(0.2 * mm),
            F.MAXDOCACHI2CUT(16.),
            F.SUM(F.PT) > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1095 * MeV, F.MASS, 1140 * MeV),
            F.CHI2DOF < 9.,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.END_VZ > -100 * mm,
            F.END_VZ < 500 * mm,
        ),
    )


def make_lambdadd():
    """Make Lambda -> p+ pi- from downstream tracks."""
    pions = _filter_down_pions_from_lambda()
    protons = _filter_down_protons_from_lambda()
    return ParticleCombiner(
        [protons, pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name='Charm_CBaryonToSl_make_lambdadd_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1180 * MeV,
            F.MAXDOCACUT(2 * mm),
            F.MAXDOCACHI2CUT(16.),
            F.SUM(F.PT) > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1095 * MeV, F.MASS, 1140 * MeV),
            F.CHI2DOF < 9.,
            F.END_VZ > 300 * mm,
            F.END_VZ < 2275 * mm,
        ),
    )


def make_xim_to_lambdapi(lambdas, pions, ll=True):
    """Return a Xi- -> Lambda0 pi- decay maker."""
    composite_cut = F.require_all(
        in_range(1258 * MeV, F.MASS, 1386 * MeV),
        F.CHI2 < 10.,
    )
    if ll:
        composite_cut &= (F.BPVFDCHI2(make_pvs()) > 10.)
    return ParticleCombiner(
        [lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name='Charm_CBaryonToSl_make_xim_to_lambdapi_{hash}',
        CombinationCut=F.require_all(
            in_range(1242 * MeV, F.MASS, 1402 * MeV),
            F.MAXDOCACUT((0.2 if ll else 2) * mm),
            F.SUM(F.PT) > 500 * MeV,
        ),
        CompositeCut=composite_cut,
    )


def make_omegam_to_lambdak(lambdas, kaons, ll=True):
    """Return a Omega- -> Lambda0 K- decay maker."""
    composite_cut = F.require_all(
        in_range(1608 * MeV, F.MASS, 1736 * MeV),
        F.CHI2 < 10.,
    )
    if ll:
        composite_cut &= (F.BPVFDCHI2(make_pvs()) > 10.)
    return ParticleCombiner(
        [lambdas, kaons],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name='Charm_CBaryonToSl_make_omegam_to_lambdak_{hash}',
        CombinationCut=F.require_all(
            in_range(1592 * MeV, F.MASS, 1752 * MeV),
            F.MAXDOCACUT((0.2 if ll else 2) * mm),
            F.SUM(F.PT) > 500 * MeV,
        ),
        CompositeCut=composite_cut,
    )


###############################################################################
# Lines definitions
###############################################################################


@register_line_builder(all_lines)
def lambdacpxicp_to_lambda0llmu_line(name="Hlt2Charm_LcpXicpToL0MupNu_LL",
                                     prescale=1):
    pvs = make_pvs()
    muons = _filter_long_muons()
    lambdas = make_lambdall()
    line_alg = ParticleCombiner(
        [lambdas, muons],
        DecayDescriptor="[Lambda_c+ -> Lambda0 mu+]cc",
        name="Charm_CBaryonToSl_LcpXicpToLmdLLMu",
        CombinationCut=F.require_all(
            F.PT > 1 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.BPVCORRM(pvs) < 2.8 * GeV,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def lambdacpxicp_to_lambda0ddmu_line(name="Hlt2Charm_LcpXicpToL0MupNu_DD",
                                     prescale=1):
    pvs = make_pvs()
    muons = _filter_tight_long_muons()
    lambdas = make_lambdadd()
    line_alg = ParticleCombiner(
        [lambdas, muons],
        DecayDescriptor="[Lambda_c+ -> Lambda0 mu+]cc",
        name="Charm_CBaryonToSl_LcpXicpToLmdDDMu",
        CombinationCut=F.require_all(
            F.PT > 1 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.BPVCORRM(pvs) < 2.8 * GeV,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def lambdacp_to_pkmu_line(name="Hlt2Charm_LcpXicpToPpKmMupNu", prescale=1):
    pvs = make_pvs()
    muons = _filter_tight_long_muons()
    kaons = _filter_long_kaons()
    protons = _filter_long_protons()
    line_alg = ParticleCombiner(
        [muons, protons, kaons],
        DecayDescriptor="[Lambda_c+ -> mu+ p+ K-]cc",
        name="Charm_CBaryonToSl_LcToPKMu",
        Combination12Cut=F.MAXDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 2.8 * GeV,
            F.P > 16 * GeV,
            F.BPVVDZ(pvs) > 0.0 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
def lambdacp_to_ppimu_line(name="Hlt2Charm_LcpXicpToPpPimMupNu", prescale=1):
    pvs = make_pvs()
    muons = _filter_tight_long_muons()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    line_alg = ParticleCombiner(
        [muons, protons, pions],
        DecayDescriptor="[Lambda_c+ -> mu+ p+ pi-]cc",
        name="Charm_CBaryonToSl_LcToPPiMu",
        Combination12Cut=F.MAXDOCACUT(0.15 * mm, ),
        CombinationCut=F.require_all(
            F.PT > 1.4 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 2.8 * GeV,
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 0.2 * mm,
            F.CHI2DOF < 4.,
            F.BPVFDCHI2(pvs) > 18.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
def omegac0_to_omegamlllmu_line(name="Hlt2Charm_Oc0ToOmMupNu_LLL", prescale=1):
    pvs = make_pvs()
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdall()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=True)
    line_alg = ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmLLLMu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.3 * mm,
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, omegam, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def omegac0_to_omegamddlmu_line(name="Hlt2Charm_Oc0ToOmMupNu_DDL", prescale=1):
    pvs = make_pvs()
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdadd()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=False)
    line_alg = ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDLMu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.3 * mm,
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, omegam, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xic0omegac0_to_ximlllmu_line(name="Hlt2Charm_Xic0Oc0ToXimMupNu_LLL",
                                 prescale=1):
    pvs = make_pvs()
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdall()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=True)
    line_alg = ParticleCombiner(
        [xim, muons],
        DecayDescriptor="[Xi_c0 -> Xi- mu+]cc",
        name="Charm_CBaryonToSl_Xic0Omc0ToXimLLLMu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, xim, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xic0omegac0_to_ximddlmu_line(name="Hlt2Charm_Xic0Oc0ToXimMupNu_DDL",
                                 prescale=1):
    pvs = make_pvs()
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdadd()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=False)
    line_alg = ParticleCombiner(
        [xim, muons],
        DecayDescriptor="[Xi_c0 -> Xi- mu+]cc",
        name="Charm_CBaryonToSl_Xic0Omc0ToXimDDLMu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, xim, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xicp_to_ximlllpipmu_line(name="Hlt2Charm_XicpToXimPipMupNu_LLL",
                             prescale=1):
    pvs = make_pvs()
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    pions_from_xi = _filter_long_pions_from_xi()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions_from_xi, ll=True)
    line_alg = ParticleCombiner(
        [xim, muons, pions],
        DecayDescriptor="[Xi_c+ -> Xi- mu+ pi+]cc",
        name="Charm_CBaryonToSl_XicpToXimLLLPipMu",
        Combination12Cut=F.MAXDOCACUT(0.2 * mm, ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 2.8 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0.0 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 9.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, xim, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xicp_to_ximddlpipmu_line(name="Hlt2Charm_XicpToXimPipMupNu_DDL",
                             prescale=1):
    pvs = make_pvs()
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    pions_from_xi = _filter_long_pions_from_xi()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions_from_xi, ll=False)
    line_alg = ParticleCombiner(
        [xim, muons, pions],
        DecayDescriptor="[Xi_c+ -> Xi- mu+ pi+]cc",
        name="Charm_CBaryonToSl_XicpToXimDDLPipMu",
        Combination12Cut=F.MAXDOCACUT(0.25 * mm, ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.25 * mm),
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 2.8 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.2 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 9.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, xim, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xic0_to_lambda0llkmmu_line(name="Hlt2Charm_Xic0Oc0ToL0KmMupNu_LL",
                               prescale=1):
    pvs = make_pvs()
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdall()
    line_alg = ParticleCombiner(
        [lambdas, muons, kaons],
        DecayDescriptor="[Xi_c0 -> Lambda0 mu+ K-]cc",
        name="Charm_CBaryonToSl_Xic0ToLmdLLKMu",
        Combination12Cut=F.MAXDOCACUT(0.2 * mm, ),
        CombinationCut=F.require_all(
            F.PT > 1.6 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            F.MAXDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > -0.3 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 9.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xic0_to_lambda0ddkmmu_line(name="Hlt2Charm_Xic0Oc0ToL0KmMupNu_DD",
                               prescale=1):
    pvs = make_pvs()
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdadd()
    line_alg = ParticleCombiner(
        [lambdas, muons, kaons],
        DecayDescriptor="[Xi_c0 -> Lambda0 mu+ K-]cc",
        name="Charm_CBaryonToSl_Xic0ToLmdDDKMu",
        Combination12Cut=F.MAXDOCACUT(2 * mm, ),
        CombinationCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.P > 15 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 0.3 * mm,
        ),
        CompositeCut=F.require_all(
            F.BPVCORRM(pvs) < 3 * GeV,
            F.P > 18 * GeV,
            F.BPVVDZ(pvs) > -0.4 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 9.,
            F.BPVDIRA(pvs) > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, line_alg],
        prescale=prescale)
