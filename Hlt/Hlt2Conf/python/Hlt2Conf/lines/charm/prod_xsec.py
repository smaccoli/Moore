###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''Definition of Charm Meson and Baryon lines for Production Cross Section

For now this targets Early Run 3 Measurement.

Cuts are (mostly) the same as for Run 2. The idea is to have as loose kinematic
cuts as possible.

--------------------
two-body lines:
--------------------

[D0 -> K- pi+]cc                              Hlt2Charm_D0ToKmPip_XSec
[D*(2010)+ -> (D0 -> K- pi+ ) pi+]cc          Hlt2Charm_DstpToD0Pip_D0ToKmPip_XSec
[D*(2010)+ -> (D0 -> K- pi- pi+ pi+ ) pi+]cc  Hlt2Charm_DstpToD0Pip_D0ToKmPimPipPip_XSec
[D*(2010)+ -> (D0 -> KS0(LL) pi+ pi- ) pi+]cc Hlt2Charm_DstpToD0Pip_D0ToKsPimPip_LL_XSec
[D*(2010)+ -> (D0 -> KS0(DD) pi+ pi- ) pi+]cc Hlt2Charm_DstpToD0Pip_D0ToKsPimPip_DD_XSec
[D*(2010)+ -> (D0 -> KS0(LL) K+ K- ) pi+]cc   Hlt2Charm_DstpToD0Pip_D0ToKsKmKp_LL_XSec
[D*(2010)+ -> (D0 -> KS0(DD) K+ K- ) pi+]cc   Hlt2Charm_DstpToD0Pip_D0ToKsKmKp_DD_XSec

double same sign (SS) charm lines using:      Hlt2DoubleCharmSS_XSec
[D0 -> K- pi+], [D+ -> K- pi+ pi+], [D_s+ -> K- K+ pi+], [Lambda_c+ -> p+ K- pi+]

[psi(4415) -> D0 D+]cc
[psi(4415) -> D0 D0]cc
[psi(4415) -> D0 D_s+]cc
[psi(4415) -> D0 Lambda_c+]cc
[psi(4415) -> D+ D+]cc
[psi(4415) -> D+ D_s+]cc
[psi(4415) -> D+ Lambda_c+]cc
[psi(4415) -> D_s+ D_s+]cc
[psi(4415) -> D_s+ Lambda_c+]cc
[psi(4415) -> Lambda_c+ Lambda_c+]cc

double opposite sign (OS) charm lines using:  Hlt2DoubleCharmOS_XSec
[D0 -> K- pi+], [D+ -> K- pi+ pi+], [D_s+ -> K- K+ pi+], [Lambda_c+ -> p+ K- pi+]

psi(4415) -> D0 D~0
psi(4415) -> D+ D-
psi(4415) -> D_s+ D_s-
psi(4415) -> Lambda_c+ Lambda_c~-
[psi(4415) -> D0 D-]cc
[psi(4415) -> D0 Lambda_c~-]cc
[psi(4415) -> D+ D_s-]cc
[psi(4415) -> D+ Lambda_c~-]cc
[psi(4415) -> D_s+ D~0]cc
[psi(4415) -> D_s+ Lambda_c~-]cc


--------------------
three-body lines:
--------------------

[D+ -> K- pi+ pi+]cc       Hlt2Charm_DpToKmPipPip_XSec
[D+ -> K- K+ pi+]cc        Hlt2Charm_DpToKmKpPip_XSec
[D_s+ -> K- K+ pi+]cc      Hlt2Charm_DspToKmKpPip_XSec
[Lambda_c+ -> p+ K- pi+]cc Hlt2Charm_LcpToPpKmPip_XSec
[Xi_c+ -> p+ K- pi+]cc     Hlt2Charm_XicpToPpKmPip_XSec

KS0 lines with either LL or DD [KS0 -> pi- pi+]:

D0 -> KS0(LL) pi+ pi-      Hlt2Charm_D0ToKsPimPip_LL_XSec
D0 -> KS0(DD) pi+ pi-      Hlt2Charm_D0ToKsPimPip_DD_XSec
D0 -> KS0(LL) K+ K-        Hlt2Charm_D0ToKsKmKp_LL_XSec
D0 -> KS0(DD) K+ K-        Hlt2Charm_D0ToKsKmKp_DD_XSec

--------------------
four-body lines:
--------------------

[D0 -> K- pi- pi+ pi+]cc   Hlt2Charm_D0ToKmPimPipPip_XSec
[Xi_c0 -> p+ K- K- pi+]cc  Hlt2Charm_Xic0ToPpKmKmPip_XSec

'''
import math

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, picosecond, GeV, mm, mrad
from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.standard_particles import (
    make_long_pions, make_has_rich_long_pions, make_down_pions,
    make_has_rich_long_kaons, make_has_rich_long_protons)
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.algorithms import ParticleContainersMerger
from .prefilters import charm_prefilters
from .particle_properties import _PION_M, _D0_M, _DP_M, _DS_M, _KS_M

#################################################################################
############################## CHARM MESONS #####################################
#################################################################################


def _xsec_make_loose_long_pions_from_d() -> ParticleFilter:
    """Maker for loose long pion tracks for/from D meson

    Returns:
        ParticleFilter: Object applying cuts to particles.

    Todo:
        * could tighten MINIPCHI2CUT to 9
    """
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 2 * GeV,
                F.PID_K < 5.,
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
            ), ),
    )


def _xsec_make_long_pions_from_d() -> ParticleFilter:
    """Maker for long pion tracks for/from D meson

    Returns:
        ParticleFilter: Object applying cuts to particles.
    """
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 2 * GeV,
                F.PID_K < 5.,
                F.MINIPCHI2CUT(IPChi2Cut=16., Vertices=make_pvs()),
            ), ),
    )


def _xsec_make_loose_long_kaons_from_d() -> ParticleFilter:
    """Maker for loose long kaon tracks for/from D meson

    Returns:
        ParticleFilter: Object applying cuts to particles.

    Todo:
        * could tighten MINIPCHI2CUT to 9
    """
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 2 * GeV,
                F.PID_K > 5.,
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
            ), ),
    )


def _xsec_make_long_kaons_from_d() -> ParticleFilter:
    """Maker for long kaon tracks for/from D meson

    Returns:
        ParticleFilter: Object applying cuts to particles.
    """
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 2 * GeV,
                F.PID_K > 5.,
                F.MINIPCHI2CUT(IPChi2Cut=16., Vertices=make_pvs()),
            ), ),
    )


def _xsec_make_long_soft_pions_from_d() -> ParticleFilter:
    """Maker for soft long pion tracks for/from D* meson

    Returns:
        ParticleFilter: Object applying cuts to particles.
    """
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(
            F.PT > 100 * MeV,
            F.P > 1.5 * GeV,
        ), ),
    )


def _xsec_make_dzeros_Kpi(
        comb_m_window: float = 90 * MeV,
        m_window: float = 80 * MeV,
        sum_pt_min: float = 1500 * MeV,
        doca_max: float = 0.1 * mm,
        vchi2dof_max: float = 10.,
        bpvfdchi2_min: float = 49.,
        bpvdira_min: float = math.cos(20 * mrad),
) -> ParticleCombiner:
    """Maker of Charm Production XSec D0->Kpi.

    Args:
        comb_m_window (float, optional): Mass window around M(HEAD) after simple four-momenta combination. Defaults to 90*MeV.
        m_window (float, optional): Mass window around M(HEAD) after vertex fit. Defaults to 80*MeV.
        sum_pt_min (float, optional): Maximum scalar sum of daughter transverse momenta. Defaults to 1500*MeV.
        doca_max (float, optional): Maximum distance of closest approach of daughters. Defaults to 0.1*mm.
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 10..
        bpvfdchi2_min (float, optional): Minimum flight distance chi2 of HEAD from best PV. Defaults to 49..
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(20 * mrad).
        bpvvdrho_max (float, optional): Maximum radial distance from HEAD's endvertex to best PV. Defaults to 3*mm.

    Returns:
        ParticleCombiner: Combination of :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_long_kaons_from_d`
        and :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_long_pions_from_d`

    Todo:
        * bpvltime_max: float=4 * picosecond -- F.BPVLTIME(make_pvs()) < bpvltime_max reduces background in the forward region
        * could profit from checking that all daughters come from same PV
        * bpvvdrho_max: float = 3 * mm -- F.BPVVDRHO(make_pvs()) < bpvvdrho_max this removes material IA
        * track ghost probability and track chi2 per ndof are not set because cuts from the reco side are unclear (May 2022)
    """

    return ParticleCombiner(
        [_xsec_make_long_kaons_from_d(),
         _xsec_make_long_pions_from_d()],
        DecayDescriptor="[D0 -> K- pi+]cc",
        name='Charm_XSec_D0_KmPip_{hash}',
        CombinationCut=F.require_all(
            F.SUM(F.PT) > sum_pt_min,
            in_range(_D0_M - comb_m_window, F.MASS, _D0_M + comb_m_window),
            F.MAXDOCACUT(doca_max),
        ),
        CompositeCut=F.require_all(
            F.CHI2DOF < vchi2dof_max,
            in_range(_D0_M - m_window, F.MASS, _D0_M + m_window),
            F.BPVFDCHI2(make_pvs()) > bpvfdchi2_min,
            F.BPVDIRA(make_pvs()) > bpvdira_min,
        ),
    )


def _xsec_make_dzeros_Kpipipi(
        comb_m_window=90 * MeV,
        m_window=80 * MeV,
        sum_pt_min=1500 * MeV,
        doca_max=0.2 * mm,
        bpvfdchi2_min=49.,
        vchi2dof_max=10.,
        bpvdira_min=math.cos(20 * mrad),
        sum_mipchi2_min=50,
):
    """Maker of Charm Production XSec D0->Kpipipi.
    Args:
        comb_m_window (float, optional): Mass window around M(HEAD) after simple four-momenta combination. Defaults to 90*MeV.
        m_window (float, optional): Mass window around M(HEAD) after vertex fit. Defaults to 80*MeV.
        sum_pt_min (float, optional): Minimum scalar sum of daughter transverse momenta. Defaults to 1500*MeV.
        doca_max (float, optional): Maximum distance of closest approach of daughters. Defaults to 0.2*mm.
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 10..
        bpvfdchi2_min (float, optional): Minimum flight distance chi2 of HEAD from best PV. Defaults to 49..
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(20 * mrad).
        sum_mipchi2_min (float, optional): Minimum scalar sum of daughter minimum IPchi2. Defaults to 50.
    Returns:
        ParticleCombiner: Combination of :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_long_kaons_from_d`
        and :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_long_pions_from_d`
    TODO:
        check possible cut on lifetime (F.BPVLTIME(make_pvs())) and radial distance (F.BPVVDRHO(make_pvs()))
    """
    return ParticleCombiner(
        [
            _xsec_make_loose_long_kaons_from_d(),
            _xsec_make_loose_long_pions_from_d(),
            _xsec_make_loose_long_pions_from_d(),
            _xsec_make_loose_long_pions_from_d()
        ],
        DecayDescriptor="[D0 -> K- pi- pi+ pi+]cc",
        name='Charm_XSec_D0_KmPimPipPip_{hash}',
        Combination12Cut=F.MASS < _D0_M + comb_m_window - 2 * _PION_M,
        Combination123Cut=F.MASS < _D0_M + comb_m_window - _PION_M,
        CombinationCut=F.require_all(
            F.SUM(F.PT) > sum_pt_min,
            in_range(_D0_M - comb_m_window, F.MASS, _D0_M + comb_m_window),
            F.MAXDOCACUT(doca_max),
            F.SUM(F.MINIPCHI2(make_pvs())) > sum_mipchi2_min,
        ),
        CompositeCut=F.require_all(
            in_range(_D0_M - m_window, F.MASS, _D0_M + m_window),
            F.CHI2DOF < vchi2dof_max,
            F.BPVFDCHI2(make_pvs()) > bpvfdchi2_min,
            F.BPVDIRA(make_pvs()) > bpvdira_min,
        ),
    )


def _xsec_make_dplus_Kpipi(
        comb_m_window: float = 90 * MeV,
        m_window: float = 80 * MeV,
        sum_pt_min: float = 1500 * MeV,
        sum_mipchi2_min: float = 64.,
        vchi2dof_max: float = 10.,
        bpvfdchi2_min: float = 50.,
        bpvltime_min: float = 0.15 * picosecond,
        bpvdira_min: float = math.cos(30 * mrad),
        doca_max: float = 0.2 * mm,
) -> ParticleCombiner:
    """Maker of Charm Production XSec D+->Kpipi.

    Args:
        comb_m_window (float, optional): Mass window around M(HEAD) after simple four-momenta combination.. Defaults to 90*MeV.
        m_window (float, optional): Mass window around M(HEAD) after vertex fit. Defaults to 80*MeV.
        sum_pt_min (float, optional): Maximum scalar sum of daughter transverse momenta. Defaults to 1500*MeV.
        min_sum_mipchi2 (float, optional): Minimum sum of IPChi2 w.r.t best PV of daughters. Defaults to 64..
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 10..
        bpvfdchi2_min (float, optional): Minimum flight distance chi2 of HEAD from best PV. Defaults to 50..
        bpvltime_min (float, optional): Minimum HEAD's decay time w.r.t best PV. Defaults to 0.15*picosecond.
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(30 * mrad).
        doca_max (float, optional): Maximum distance of closest approach of daughters. Defaults to 0.2*mm.

    Returns:
        ParticleCombiner: Combination of :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_loose_long_kaons_from_d`,
        :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_loose_long_pions_from_d` and :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_loose_long_pions_from_d`

    Todo:
        * bpvltime_max: float=10 * picosecond -- F.BPVLTIME(make_pvs()) < bpvltime_max reduces background in the forward region
        * could profit from checking that all daughters come from same PV
        * bpvvdrho_max: float = 3 * mm -- F.BPVVDRHO(make_pvs()) < bpvvdrho_max this removes material IA
        * track ghost probability and track chi2 per ndof are not set because cuts from the reco side are unclear (May 2022)
        * tighten BPVFDCHI2 to > 65
        * require at least two daughters to have IPCHI2 > 16
        * tighten DIRA to < 20 mrad
    """
    return ParticleCombiner(
        [
            _xsec_make_loose_long_kaons_from_d(),
            _xsec_make_loose_long_pions_from_d(),
            _xsec_make_loose_long_pions_from_d()
        ],
        DecayDescriptor="[D+ -> K- pi+ pi+]cc",
        name='Charm_XSec_Dp_KmPipPip_{hash}',
        Combination12Cut=F.MASS < _DP_M + comb_m_window - _PION_M,
        CombinationCut=F.require_all(
            F.SUM(F.PT) > sum_pt_min,
            in_range(_DP_M - comb_m_window, F.MASS, _DP_M + comb_m_window),
            F.SUM(F.MINIPCHI2(make_pvs())) > sum_mipchi2_min,
            F.MAXDOCACUT(doca_max),
        ),
        CompositeCut=F.require_all(
            F.CHI2DOF < vchi2dof_max,
            in_range(_DP_M - m_window, F.MASS, _DP_M + m_window),
            F.BPVDIRA(make_pvs()) > bpvdira_min,
            F.BPVFDCHI2(make_pvs()) > bpvfdchi2_min,
            F.BPVLTIME(make_pvs()) > bpvltime_min,
        ),
    )


def _xsec_make_dplus_KKpi(
        comb_m_window: float = 90 * MeV,
        m_window: float = 80 * MeV,
        sum_pt_min: float = 1500 * MeV,
        sum_mipchi2_min: float = 64.,
        vchi2dof_max: float = 10.,
        bpvfdchi2_min: float = 50.,
        bpvltime_min: float = 0.15 * picosecond,
        bpvdira_min: float = math.cos(30 * mrad),
        doca_max: float = 0.2 * mm,
) -> ParticleCombiner:
    """Maker of Charm Production XSec D+->KKpi.

    Args:
        comb_m_window (float, optional): Mass window around M(HEAD) after simple four-momenta combination.. Defaults to 90*MeV.
        m_window (float, optional): Mass window around M(HEAD) after vertex fit. Defaults to 80*MeV.
        sum_pt_min (float, optional): Maximum scalar sum of daughter transverse momenta. Defaults to 1500*MeV.
        min_sum_mipchi2 (float, optional): Minimum sum of IPChi2 w.r.t best PV of daughters. Defaults to 64..
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 10..
        bpvfdchi2_min (float, optional): Minimum flight distance chi2 of HEAD from best PV. Defaults to 50..
        bpvltime_min (float, optional): Minimum HEAD's decay time w.r.t best PV. Defaults to 0.15*picosecond.
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(30 * mrad).
        doca_max (float, optional): Maximum distance of closest approach of daughters. Defaults to 0.2*mm.

    Returns:
        ParticleCombiner: Combination of :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_loose_long_kaons_from_d`,
        :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_loose_long_kaons_from_d` and :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_loose_long_pions_from_d`

    Todo:
        * bpvltime_max: float=10 * picosecond -- F.BPVLTIME(make_pvs()) < bpvltime_max reduces background in the forward region
        * could profit from checking that all daughters come from same PV
        * bpvvdrho_max: float = 3 * mm -- F.BPVVDRHO(make_pvs()) < bpvvdrho_max this removes material IA
        * track ghost probability and track chi2 per ndof are not set because cuts from the reco side are unclear (May 2022)
        * tighten BPVFDCHI2 to > 65
        * require at least two daughters to have IPCHI2 > 16
        * tighten DIRA to < 20 mrad
    """
    return ParticleCombiner(
        [
            _xsec_make_loose_long_kaons_from_d(),
            _xsec_make_loose_long_kaons_from_d(),
            _xsec_make_loose_long_pions_from_d()
        ],
        DecayDescriptor="[D+ -> K- K+ pi+]cc",
        name='Charm_XSec_Dp_KmKpPip_{hash}',
        Combination12Cut=F.MASS < _DP_M + comb_m_window - _PION_M,
        CombinationCut=F.require_all(
            F.SUM(F.PT) > sum_pt_min,
            in_range(_DP_M - comb_m_window, F.MASS, _DP_M + comb_m_window),
            F.SUM(F.MINIPCHI2(make_pvs())) > sum_mipchi2_min,
            F.MAXDOCACUT(doca_max),
        ),
        CompositeCut=F.require_all(
            F.CHI2DOF < vchi2dof_max,
            in_range(_DP_M - m_window, F.MASS, _DP_M + m_window),
            F.BPVDIRA(make_pvs()) > bpvdira_min,
            F.BPVFDCHI2(make_pvs()) > bpvfdchi2_min,
            F.BPVLTIME(make_pvs()) > bpvltime_min,
        ),
    )


def _xsec_make_d_splus_KKpi(
        comb_m_window: float = 90 * MeV,
        m_window: float = 80 * MeV,
        sum_pt_min: float = 1500 * MeV,
        sum_mipchi2_min: float = 64.,
        vchi2dof_max: float = 10.,
        bpvfdchi2_min: float = 50.,
        bpvltime_min: float = 0.15 * picosecond,
        bpvdira_min: float = math.cos(30 * mrad),
        doca_max: float = 0.2 * mm,
) -> ParticleCombiner:
    """Maker of Charm Production XSec Ds+->KKpi.

    Args:
        comb_m_window (float, optional): Mass window around M(HEAD) after simple four-momenta combination.. Defaults to 90*MeV.
        m_window (float, optional): Mass window around M(HEAD) after vertex fit. Defaults to 80*MeV.
        sum_pt_min (float, optional): Maximum scalar sum of daughter transverse momenta. Defaults to 1500*MeV.
        min_sum_mipchi2 (float, optional): Minimum sum of IPChi2 w.r.t best PV of daughters. Defaults to 64..
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 10..
        bpvfdchi2_min (float, optional): Minimum flight distance chi2 of HEAD from best PV. Defaults to 50..
        bpvltime_min (float, optional): Minimum HEAD's decay time w.r.t best PV. Defaults to 0.15*picosecond.
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(30 * mrad).
        doca_max (float, optional): Maximum distance of closest approach of daughters. Defaults to 0.2*mm.

    Returns:
        ParticleCombiner: Combination of :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_loose_long_kaons_from_d`,
        :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_loose_long_kaons_from_d` and :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_loose_long_pions_from_d`

    Todo:
        * bpvltime_max: float=10 * picosecond -- F.BPVLTIME(make_pvs()) < bpvltime_max reduces background in the forward region
        * could profit from checking that all daughters come from same PV
        * bpvvdrho_max: float = 3 * mm -- F.BPVVDRHO(make_pvs()) < bpvvdrho_max this removes material IA
        * track ghost probability and track chi2 per ndof are not set because cuts from the reco side are unclear (May 2022)
        * tighten BPVFDCHI2 to > 65
        * require at least two daughters to have IPCHI2 > 16
        * tighten DIRA to < 20 mrad
    """
    return ParticleCombiner(
        [
            _xsec_make_loose_long_kaons_from_d(),
            _xsec_make_loose_long_kaons_from_d(),
            _xsec_make_loose_long_pions_from_d()
        ],
        DecayDescriptor="[D_s+ -> K- K+ pi+]cc",
        name='Charm_XSec_Dsp_KmKpPip_{hash}',
        Combination12Cut=F.MASS < _DS_M + comb_m_window - _PION_M,
        CombinationCut=F.require_all(
            F.SUM(F.PT) > sum_pt_min,
            in_range(_DS_M - comb_m_window, F.MASS, _DS_M + comb_m_window),
            F.SUM(F.MINIPCHI2(make_pvs())) > sum_mipchi2_min,
            F.MAXDOCACUT(doca_max),
        ),
        CompositeCut=F.require_all(
            F.CHI2DOF < vchi2dof_max,
            in_range(_DS_M - m_window, F.MASS, _DS_M + m_window),
            F.BPVDIRA(make_pvs()) > bpvdira_min,
            F.BPVFDCHI2(make_pvs()) > bpvfdchi2_min,
            F.BPVLTIME(make_pvs()) > bpvltime_min,
        ),
    )


def _xsec_make_dstars_D0pi(
        dzeros: ParticleCombiner,
        pions: ParticleFilter,
        comb_delta_m_max: float = 165 * MeV,
        delta_m_max: float = 160 * MeV,
        vchi2dof_max: float = 25.,
        self_conjugate_d0_decay: bool = False) -> ParticleCombiner:
    """Maker of Charm Production XSec D*+->D0pi+.

    Args:
        dzeros (ParticleCombiner): Combination representing D0s.
        pions (ParticleFilter): Filtered (slow) pions.
        comb_delta_m_max (float, optional): Mass difference between D0 and pion combination and D0 composite. Defaults to 165*MeV.
        delta_m_max (float, optional): Mass difference between D*+ composite and D0 composite. Defaults to 160*MeV.
        vchi2dof_max (float, optional): Vertex chi2 per degree of freedom. Defaults to 25.

    Returns:
        ParticleCombiner: Combination of D0 and (slow) pions.
    """
    if self_conjugate_d0_decay:
        dstarp = ParticleCombiner(
            [dzeros, pions],
            DecayDescriptor="D*(2010)+ -> D0 pi+",
            name='Charm_XSec_Dstp_D0pip_{hash}',
            CombinationCut=F.require_all(
                F.MASS - F.CHILD(1, F.MASS) < comb_delta_m_max),
            CompositeCut=F.require_all(
                F.MASS - F.CHILD(1, F.MASS) < delta_m_max,
                F.CHI2DOF < vchi2dof_max,
            ),
        )
        dstarm = ParticleCombiner(
            [dzeros, pions],
            DecayDescriptor="D*(2010)- -> D0 pi-",
            name='Charm_XSec_Dstm_D0pim_{hash}',
            CombinationCut=F.require_all(
                F.MASS - F.CHILD(1, F.MASS) < comb_delta_m_max),
            CompositeCut=F.require_all(
                F.MASS - F.CHILD(1, F.MASS) < delta_m_max,
                F.CHI2DOF < vchi2dof_max,
            ),
        )
        return ParticleContainersMerger([dstarp, dstarm])
    else:
        return ParticleCombiner(
            [dzeros, pions],
            DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
            name='Charm_XSec_Dstp_D0pip_{hash}',
            CombinationCut=F.require_all(
                F.MASS - F.CHILD(1, F.MASS) < comb_delta_m_max),
            CompositeCut=F.require_all(
                F.MASS - F.CHILD(1, F.MASS) < delta_m_max,
                F.CHI2DOF < vchi2dof_max,
            ),
        )


all_lines = {}


@register_line_builder(all_lines)
@configurable
def dzero2kpi_line(name: str = 'Hlt2Charm_D0ToKmPip_XSec',
                   prescale: float = 1.) -> Hlt2Line:
    """Trigger line for D0->KmPip for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_D0ToKmPip_XSec'.
        prescale (float, optional): Defaults to 1..

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """
    dzeros = _xsec_make_dzeros_Kpi()
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2kmpip_line(
        name: str = 'Hlt2Charm_DstpToD0Pip_D0ToKmPip_XSec',
        prescale: float = 1.) -> Hlt2Line:
    """Trigger line for D*+->(D0->KmPip)Pip for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_DstpToD0Pip_D0ToKmPip_XSec'.
        prescale (float, optional): Defaults to 1..

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """
    dzeros = _xsec_make_dzeros_Kpi()
    dstars = _xsec_make_dstars_D0pi(dzeros,
                                    _xsec_make_long_soft_pions_from_d())
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dzero2k3pi_line(name: str = 'Hlt2Charm_D0ToKmPimPipPip_XSec',
                    prescale: float = 1.) -> Hlt2Line:
    """Trigger line for D0->KmPimPipPip for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_D0ToKmPimPipPip_XSec'.
        prescale (float, optional): Defaults to 1..

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """
    dzeros = _xsec_make_dzeros_Kpipipi()
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dzeros], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2k3pi_line(
        name: str = 'Hlt2Charm_DstpToD0Pip_D0ToKmPimPipPip_XSec',
        prescale: float = 1.) -> Hlt2Line:
    """Trigger line for D*+->(D0->KmPimPipPip)Pip for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_DstpToD0Pip_D0ToKmPimPipPip_XSec'.
        prescale (float, optional): Defaults to 1..

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """
    dzeros = _xsec_make_dzeros_Kpipipi()
    dstars = _xsec_make_dstars_D0pi(dzeros,
                                    _xsec_make_long_soft_pions_from_d())
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros, dstars],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dplus2kpipi_line(name: str = 'Hlt2Charm_DpToKmPipPip_XSec',
                     prescale: float = 1.) -> Hlt2Line:
    """Trigger line for Dp->KmPipPip for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_DpToKmPipPip_XSec'.
        prescale (float, optional): Defaults to 1..

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """
    dpluses = _xsec_make_dplus_Kpipi()
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dpluses], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dplus2kkpi_line(name: str = 'Hlt2Charm_DpToKmKpPip_XSec',
                    prescale: float = 1.) -> Hlt2Line:
    """Trigger line for Dp->KmKpPip for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_DpToKmKpPip_XSec'.
        prescale (float, optional): Defaults to 1..

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """
    dpluses = _xsec_make_dplus_KKpi()
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dpluses], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dsplus2kkpi_line(name: str = 'Hlt2Charm_DspToKmKpPip_XSec',
                     prescale: float = 1.) -> Hlt2Line:
    """Trigger line for Dsp->KmKpPip for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_DspToKmKpPip_XSec'.
        prescale (float, optional): Defaults to 1..

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """
    dspluses = _xsec_make_d_splus_KKpi()
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dspluses], prescale=prescale)


###########################################################################
############################ CHARM BARYONS ################################
###########################################################################


def _xsec_make_protons_from_baryons(
        pt_min=200 * MeV,
        p_min=10 * GeV,
        mipchi2_min=6.0,
        dllp_min=5.0,
):
    """Maker for protons for/from baryons

    Returns:
        ParticleFilter: Object applying cuts to particles, PT and P are tuned accordingly to the HLT2 lines used for the PID calibration.

    To do:

        * F.CHI2DOF < trchi2dof_max (3) removed for the beginning of the data taking
    """
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > pt_min,
                F.P > p_min,
                F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                F.PID_P > dllp_min,
            )))


def _xsec_make_kaons_from_baryons(
        pt_min=250 * MeV,
        p_min=2 * GeV,
        mipchi2_min=6.0,
        dllk_min=5.0,
):
    """Maker for kaons for/from baryons

    Returns:
        ParticleFilter: Object applying cuts to particles, PT and P are tuned accordingly to the HLT2 lines used for the PID calibration.

    To do:

        * F.CHI2DOF < trchi2dof_max (3) removed for the beginning of the data taking
    """
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > pt_min,
                F.P > p_min,
                F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                F.PID_K > dllk_min,
            ), ),
    )


def _xsec_make_pions_from_baryons(
        pt_min=250 * MeV,
        p_min=2 * GeV,
        mipchi2_min=6.0,
        dllk_max=5.0,
):
    """Maker for pions for/from baryons

    Returns:
        ParticleFilter: Object applying cuts to particles, PT and P are tuned accordingly to the HLT2 lines used for the PID calibration.

    To do:

        * F.CHI2DOF < trchi2dof_max (3) removed for the beginning of the data taking
    """
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > pt_min,
                F.P > p_min,
                F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                F.PID_K < dllk_max,
            ), ),
    )


def _xsec_make_lc_or_xicp_pKpi(
        comb_m_min=2_201.0 * MeV,
        comb_m_max=2_553.0 * MeV,
        sum_pt_min=1.5 * GeV,
        comb_pt_min_at_least_one=500 * MeV,
        comb_pt_min_at_least_two=400 * MeV,
        mipchi2_min_at_least_one=16.0,
        mipchi2_min_at_least_two=9.0,
        bpvltime_min=0.15 * picosecond,
        bpvltime_max=10 * picosecond,
        vchi2dof_max=5.0,
        bpvfdchi2_min=50,
        doca_max=0.1 * mm,
        bpvdira_min=math.cos(20 * mrad),
        sum_mipchi2_min=30,
        # Nominally this is configured for the Lambda_c+ mass range.
        m_min=2_211.0 * MeV,
        m_max=2_362.0 * MeV,
        decay_descriptor="[Lambda_c+ -> p+ K- pi+]cc",
        name='LcpToPpKmPip_{hash}'):
    pvs = make_pvs()
    """Maker of Charm Production XSec Lc+->pKpi

    Args:
        comb_m_min (float, optional): minimum value of the Mass window, default set to 2201 MeV
        comb_m_max (float, optional): maximum value of the Mass window, default set to 2553 MeV
        sum_pt_min (float, optional): Maximum scalar sum of daughter transverse momenta. Defaults to 1.5*GeV.
        comb_pt_min_at_least_one (float, optional): any 1 requirement for PT, default set at 500*MeV
        comb_pt_min_at_least_two (float, optional): any 2 requirement for PT, default set to 400*MeV
        mipchi2_min_at_least_one (float, optional): any 1 requirement for ipchi2, default 16
        mipchi2_min_at_least_two (float, optional): any 2 requirement for ipchi2, default 9
        bpvltime_min (float, optional): lower limit for lifetime, set to 0.15*ps
        bpvltime_max (float, optional): upper limit for lifetime, set to 10*ps.
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 5
        bpvfdchi2_min (float, optional): Minimum flight distance chi2 of HEAD from best PV. Defaults to 50.
        doca_max (float, optional): Maximum distance of closest approach of daughters. Defaults to 0.1*mm.
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(20 * mrad).
        sum_mipchi2_min (float, optional): Minimum sum of IPChi2 w.r.t best PV of daughters. Defaults to 30.

    Return:
        ParticleCombiner: Combination of :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_protons_from_baryons` with dllp_min=2.0
        and :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_kaons_from_baryons` with dllk_min=2.0
        and :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_pions_from_baryons` with dllk_max=8.0

    To do:

        * bpvltime_max: float=5 * picosecond
        * bpvvdrho_max: float = 3 * mm -- F.BPVVDRHO(make_pvs()) < bpvvdrho_max this removes material IA


    """

    return ParticleCombiner(
        Inputs=[
            _xsec_make_protons_from_baryons(dllp_min=2.0),
            _xsec_make_kaons_from_baryons(dllk_min=2.0),
            _xsec_make_pions_from_baryons(dllk_max=8.0),
        ],
        DecayDescriptor=decay_descriptor,
        name=f"Charm_XSec_{name}",
        CombinationCut=F.require_all(
            F.math.in_range(
                comb_m_min,
                F.MASS,
                comb_m_max,
            ),
            F.SUM(F.PT) > sum_pt_min * MeV,
            F.SUM(F.PT > comb_pt_min_at_least_one) >= 1,
            F.SUM(F.PT > comb_pt_min_at_least_two) >= 2,
            F.SUM(
                F.MINIPCHI2CUT(
                    IPChi2Cut=mipchi2_min_at_least_one, Vertices=pvs)) >= 1,
            F.SUM(
                F.MINIPCHI2CUT(
                    IPChi2Cut=mipchi2_min_at_least_two, Vertices=pvs)) >= 2,
            F.MAXDOCACUT(doca_max),
            F.SUM(F.MINIPCHI2(pvs)) > sum_mipchi2_min,
        ),
        CompositeCut=F.require_all(F.CHI2DOF < vchi2dof_max,
                                   F.BPVDIRA(pvs) > bpvdira_min,
                                   F.BPVLTIME(pvs) > bpvltime_min,
                                   F.BPVLTIME(pvs) < bpvltime_max,
                                   F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                   F.math.in_range(m_min, F.MASS, m_max)),
    )


def _xsec_make_lcp_pKpi(
        m_min=2_211.0 * MeV,
        m_max=2_362.0 * MeV,
):
    return _xsec_make_lc_or_xicp_pKpi(m_min=m_min, m_max=m_max)


def _xsec_make_xicp_pKpi(
        m_min=2_392.0 * MeV,
        m_max=2_543.0 * MeV,
):
    return _xsec_make_lc_or_xicp_pKpi(
        m_min=m_min,
        m_max=m_max,
        decay_descriptor="[Xi_c+ -> p+ K- pi+]cc",
        name='XicpToPpKmPip_{hash}')


### Xic0 line
def _xsec_make_xic0_pkkpi(
        comb_m_min=2_386.0 * MeV,
        comb_m_max=2_780.0 * MeV,
        sum_pt_min=1 * GeV,
        comb_pt_min_at_least_one=450 * MeV,
        #No PT(any2) for 4 body decays
        comb_pt_min_at_least_two=0 * MeV,
        comb_mipchi2_min_at_least_one=8.0,
        comb_mipchi2_min_at_least_two=6.0,
        bpvltime_min=0.1 * picosecond,
        bpvltime_max=10 * picosecond,
        vchi2dof_max=5.0,
        bpvfdchi2_min=40,
        doca_max=0.1 * mm,
        bpvdira_min=math.cos(20 * mrad),
        sum_mipchi2_min=24,
        m_min=2_396.0 * MeV,
        m_max=2_770.0 * MeV):
    pvs = make_pvs()
    decay_descriptor = "[Xi_c0 -> p+ K- K- pi+]cc"
    """Maker of Charm Production XSec Xic0->pKKpi

    Args:
        comb_m_min (float, optional): minimum value of the Mass window, default set to 2386 MeV
        comb_m_max (float, optional): maximum value of the Mass window, default set to 2780 MeV
        sum_pt_min (float, optional): Maximum scalar sum of daughter transverse momenta. Defaults to 1*GeV.
        comb_pt_min_at_least_one (float, optional): any 1 requirement for PT, default set at 450*MeV
        comb_pt_min_at_least_two (float, optional): any 2 requirement for PT, default set to 0*MeV to investigate the low region
        mipchi2_min_at_least_one (float, optional): any 1 requirement for ipchi2, default 8
        mipchi2_min_at_least_two (float, optional): any 2 requirement for ipchi2, default 6
        bpvltime_min (float, optional): lower limit for lifetime, set to 0.1*ps
        bpvltime_max (float, optional): upper limit for lifetime, set to 10*ps.
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 5
        bpvfdchi2_min (float, optional): Minimum flight distance chi2 of HEAD from best PV. Defaults to 40.
        doca_max (float, optional): Maximum distance of closest approach of daughters. Defaults to 0.1*mm.
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(20 * mrad).
        sum_mipchi2_min (float, optional): Minimum sum of IPChi2 w.r.t best PV of daughters. Defaults to 24.

    Return:
        ParticleCombiner: Combination of :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_protons_from_baryons` with dllp_min as default, 5.0
        and :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_kaons_from_baryons` with dllk_min as default, 5.0
        and :func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_pions_from_baryons` with dllk_max as default, 5.0

    To do:

        * bpvltime_max: float=8 * picosecond
        * bpvvdrho_max: float = 3 * mm -- F.BPVVDRHO(make_pvs()) < bpvvdrho_max this removes material IA


    """

    return ParticleCombiner(
        Inputs=[
            _xsec_make_protons_from_baryons(mipchi2_min=4.0),
            _xsec_make_kaons_from_baryons(mipchi2_min=4.0),
            _xsec_make_kaons_from_baryons(mipchi2_min=4.0),
            _xsec_make_pions_from_baryons(mipchi2_min=4.0),
        ],
        DecayDescriptor=decay_descriptor,
        name='Charm_XSec_Xic0ToPpKmKmPip_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(
                comb_m_min,
                F.MASS,
                comb_m_max,
            ),
            F.SUM(F.PT) > sum_pt_min,
            F.SUM(F.PT > comb_pt_min_at_least_one) >= 1,
            F.SUM(F.PT > comb_pt_min_at_least_two) >= 2,
            F.SUM(F.MINIPCHI2(pvs) > comb_mipchi2_min_at_least_one) >= 1,
            F.SUM(F.MINIPCHI2(pvs) > comb_mipchi2_min_at_least_two) >= 2,
            F.MAXDOCACUT(doca_max),
            F.SUM(F.MINIPCHI2(pvs)) >
            sum_mipchi2_min,  #not so powerful for the rate reduction
        ),
        CompositeCut=F.require_all(F.CHI2DOF < vchi2dof_max,
                                   F.BPVDIRA(pvs) > bpvdira_min,
                                   F.math.in_range(m_min, F.MASS, m_max),
                                   F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                   F.BPVLTIME(pvs) > bpvltime_min,
                                   F.BPVLTIME(pvs) < bpvltime_max),
    )


## Baryon Line builders
@register_line_builder(all_lines)
@configurable
def lcp2pkpi_line(name="Hlt2Charm_LcpToPpKmPip_XSec", prescale=1.0):
    """Trigger line for Lc+->pKpi for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_LcpToPpKmPip_XSec'.
        prescale (float, optional): Defaults to 1.

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [_xsec_make_lcp_pKpi()],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def xicp2pkpi_line(name="Hlt2Charm_XicpToPpKmPip_XSec", prescale=1.0):
    """Trigger line for Xic+->pKpi for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_XicpToPpKmPip_XSec'.
        prescale (float, optional): Defaults to 1.

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [_xsec_make_xicp_pKpi()],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def xic02pkkpi_line(name="Hlt2Charm_Xic0ToPpKmKmPip_XSec", prescale=1.0):
    """Trigger line for Xic0->pKKpi for charm cross section early Run 3 measurement.

    Args:
        name (str, optional): Defaults to 'Hlt2Charm_Xic0ToPpKmKmPip_XSec'.
        prescale (float, optional): Defaults to 1.

    Returns:
        Hlt2Line: The HLT2 line.

    Todo:
        * avoid unnecessary computations by using `hlt1_filter_code=["Hlt1TwoTrackLowPtCharmDecision", "Hlt1GECPassthroughDecision"]`
    """

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [_xsec_make_xic0_pkkpi()],
        prescale=prescale)


###############################################################################
######################## DOUBLE CHARM #########################################
###############################################################################


def _xsec_make_double_charm(charm_hadrons: ParticleContainersMerger,
                            descriptor: str) -> ParticleCombiner:
    """Maker for double charm combinations.

    Args:
        charm_hadrons (ParticleContainersMerger): Container for several types of charm hadrons to be combined.
        descriptor (str): The decay descriptor for a specific combination. E.g. '[psi(4415) -> D0 Lambda_c+]cc'.

    Returns:
        ParticleCombiner: The combination of two charm hadron species.
    """
    combination_code = F.require_all(F.ALL)
    vertex_code = F.require_all(F.ALL)
    return ParticleCombiner([charm_hadrons, charm_hadrons],
                            name='Charm_XSec_DoubleCharm_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


def _xsec_make_charm_hadron_container() -> ParticleContainersMerger:
    """Maker of a merged container for different charm hadron species.

    Returns:
        ParticleContainersMerger: The containers merged together.
    """
    dzeros = _xsec_make_dzeros_Kpi()
    dplus = _xsec_make_dplus_Kpipi()
    d_splus = _xsec_make_d_splus_KKpi()
    lcps = _xsec_make_lcp_pKpi()
    return ParticleContainersMerger(
        [dzeros, dplus, d_splus, lcps],
        name='Charm_XSec_DoubleCharm_Merger_{hash}')


def _xsec_make_double_SS_charm_combinations() -> ParticleContainersMerger:
    """Maker of same sign double charm combinations.

    Returns:
        ParticleContainersMerger: All decays merged together in one container.
    """
    descriptors = [
        '[psi(4415) -> D0 D+]cc',
        '[psi(4415) -> D0 D0]cc',
        '[psi(4415) -> D0 D_s+]cc',
        '[psi(4415) -> D0 Lambda_c+]cc',
        '[psi(4415) -> D+ D+]cc',
        '[psi(4415) -> D+ D_s+]cc',
        '[psi(4415) -> D+ Lambda_c+]cc',
        '[psi(4415) -> D_s+ D_s+]cc',
        '[psi(4415) -> D_s+ Lambda_c+]cc',
        '[psi(4415) -> Lambda_c+ Lambda_c+]cc',
    ]
    hadrons = _xsec_make_charm_hadron_container()
    comb = []
    for d in descriptors:
        comb.append(_xsec_make_double_charm(hadrons, d))
    return ParticleContainersMerger(
        comb, name='Charm_XSec_DoubleCharmSS_Merger_{hash}')


def _xsec_make_double_OS_charm_combinations() -> ParticleContainersMerger:
    """Maker of opposite sign double charm combinations.

    Returns:
        ParticleContainersMerger: All decays merged together in one container.
    """
    descriptors = [
        'psi(4415) -> D0 D~0',
        'psi(4415) -> D+ D-',
        'psi(4415) -> D_s+ D_s-',
        'psi(4415) -> Lambda_c+ Lambda_c~-',
        '[psi(4415) -> D0 D-]cc',
        '[psi(4415) -> D0 Lambda_c~-]cc',
        '[psi(4415) -> D+ D_s-]cc',
        '[psi(4415) -> D+ Lambda_c~-]cc',
        '[psi(4415) -> D_s+ D~0]cc',
        '[psi(4415) -> D_s+ Lambda_c~-]cc',
    ]
    hadrons = _xsec_make_charm_hadron_container()
    comb = []
    for d in descriptors:
        comb.append(_xsec_make_double_charm(hadrons, d))
    return ParticleContainersMerger(
        comb, name='Charm_XSec_DoubleCharmOS_Merger_{hash}')


@register_line_builder(all_lines)
@configurable
def double_charm_SS_line(name: str = 'Hlt2Charm_DoubleOpenCharmSS_PR_XSec',
                         prescale: float = 1.,
                         persistreco: bool = True) -> Hlt2Line:
    """Triggerline for same sign double charm production.
    Note:
        This line persists the reconstruction.
    Args:
        name (str, optional): Name of the triggerline. Defaults to 'Hlt2Charm_DoubleOpenCharmSS_PR_XSec'.
        prescale (float, optional): Prescale of the line. Defaults to 1..

    Returns:
        Hlt2Line: The triggerline object.
    """
    dicharm = _xsec_make_double_SS_charm_combinations()
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dicharm],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def double_charm_OS_line(name: str = 'Hlt2Charm_DoubleOpenCharmOS_PR_XSec',
                         prescale: float = 1.,
                         persistreco: bool = True) -> Hlt2Line:
    """Triggerline for opposite sign double charm production.

    Args:
        name (str, optional): _description_. Defaults to 'Hlt2Charm_DoubleOpenCharmOS_PR_XSec'.
        prescale (float, optional): _description_. Defaults to 1..
    Note:
        This line persists the reconstruction.
    Returns:
        Hlt2Line: The triggerline object.
    """
    dicharm = _xsec_make_double_OS_charm_combinations()
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dicharm],
        prescale=prescale,
        persistreco=persistreco)


#######################################################################################
############################ CHARM MESONS w/ KS #######################################
#######################################################################################


def _xsec_make_long_pions_from_ks():
    """Maker for long pion tracks for/from Kshort(LL)

    Returns:
        ParticleFilter: Object applying cuts to particles.
    """

    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=36., Vertices=make_pvs()), ), ),
    )


def _xsec_make_down_pions_from_ks():
    """Maker for down pion tracks for/from Kshort(DD)

    Returns:
        ParticleFilter: Object applying cuts to particles.
    """
    return ParticleFilter(
        make_down_pions(),
        F.FILTER(F.require_all(
            F.PT > 175 * MeV,
            F.P > 3000 * MeV,
        ), ),
    )


def _xsec_make_KSLL_from_d():
    """Maker for Kshort(LL)->pipi for/from D meson

    Returns:
        ParticleCombiner: Object applying cuts to particles.
    """
    return ParticleCombiner(
        [_xsec_make_long_pions_from_ks(),
         _xsec_make_long_pions_from_ks()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name='Charm_XSec_Ks0LL_PipPim_{hash}',
        CombinationCut=F.require_all(
            in_range(_KS_M - 50 * MeV, F.MASS, _KS_M + 50 * MeV), ),
        CompositeCut=F.require_all(
            in_range(_KS_M - 35 * MeV, F.MASS, _KS_M + 35 * MeV),
            F.CHI2DOF < 30.,
            F.BPVDIRA(make_pvs()) > math.cos(14.1 * mrad),
        ),
    )


def _xsec_make_KSDD_from_d():
    """Maker for Kshort(DD)->pipi for/from D meson

    Returns:
        ParticleCombiner: Object applying cuts to particles.
    """
    return ParticleCombiner(
        [_xsec_make_down_pions_from_ks(),
         _xsec_make_down_pions_from_ks()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name='Charm_XSec_Ks0DD_PipPim_{hash}',
        CombinationCut=F.require_all(
            in_range(_KS_M - 80 * MeV, F.MASS, _KS_M + 80 * MeV), ),
        CompositeCut=F.require_all(
            in_range(_KS_M - 64 * MeV, F.MASS, _KS_M + 64 * MeV),
            F.CHI2DOF < 30.,
            F.BPVVDZ(make_pvs()) > 400 * mm,
        ),
    )


def _xsec_make_dzero_Kshh(kshort,
                          hadron1,
                          hadron2,
                          descriptor,
                          comb_m_window=130 * MeV,
                          sum_pt_min=1500 * MeV,
                          doca23_max=0.200 * mm,
                          bpvdira_min=math.cos(14.1 * mrad),
                          vchi2dof_max=5.,
                          bpvltime_min=0.1 * picosecond,
                          bpvfdchi2_max=6000.,
                          m_window=100 * MeV):
    """
    Could go tight with 3000 for the fdchi2
    """
    """Maker of Charm Production XSec D0 -> KS0 H+ H-.

    Args:
        comb_m_window (float, optional): Mass window around M(HEAD) after simple four-momenta combination. Defaults to 130*MeV.
        sum_pt_min (float, optional): Maximum scalar sum of daughter transverse momenta. Defaults to 1500*MeV.
        doca23_max (float, optional): Maximum distance of closest approach of the daughters 2-3. Defaults to 0.200*mm.
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(14.1 * mrad).
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 5..
        bpvltime_min (float, optional): Minimum HEAD's decay time w.r.t best PV. Defaults to 0.1*picosecond.
        bpvfdchi2_max (float, optional): Maximum flight distance chi2 of HEAD from best PV. Defaults to 6000..
        m_window (float, optional): Mass window around M(HEAD) after vertex fit. Defaults to 100*MeV.

    Returns:
        ParticleCombiner:
            Combination of :func:`~Hlt2Conf.lines.charm.prod_xsec.xsec_make_KSDD_from_d`
                            or func:`~Hlt2Conf.lines.charm.prod_xsec.xsec_make_KSLL_from_d`
                            and :
                                (func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_long_pions_from_d` and func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_long_pions_from_d`)
                                or (func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_long_kaons_from_d` and func:`~Hlt2Conf.lines.charm.prod_xsec._xsec_make_long_kaons_from_d`)

    Notes:
        * bpvfdchi2_max = 3000, possible tighter cut
    """

    return ParticleCombiner(
        [kshort, hadron1, hadron2],
        DecayDescriptor=descriptor,
        name='Charm_XSec_D0_Ks0hh_{hash}',
        Combination12Cut=F.MASS < _D0_M + comb_m_window - _PION_M,
        CombinationCut=F.require_all(
            in_range(_D0_M - comb_m_window, F.MASS, _D0_M + comb_m_window),
            F.SUM(F.PT) > sum_pt_min,
            F.DOCA(2, 3) < doca23_max,
        ),
        CompositeCut=F.require_all(
            in_range(_D0_M - m_window, F.MASS, _D0_M + m_window),
            F.CHI2DOF < vchi2dof_max,
            F.BPVDIRA(make_pvs()) > bpvdira_min,
            F.BPVFDCHI2(make_pvs()) < bpvfdchi2_max,
            F.BPVLTIME(make_pvs()) > bpvltime_min,
        ),
    )


## D0 -> KS0 h+ h-  Line builders
@register_line_builder(all_lines)
@configurable
def dzero2ksLLpipi_line(name='Hlt2Charm_D0ToKsPimPip_LL_XSec', prescale=1.):
    """
    D0 --> KS0(LL) pi+ pi- line maker
    """
    kshorts = _xsec_make_KSLL_from_d()
    pions = _xsec_make_long_pions_from_d()
    dzeros = _xsec_make_dzero_Kshh(kshorts, pions, pions, "D0 -> KS0 pi- pi+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dzero2ksDDpipi_line(name='Hlt2Charm_D0ToKsPimPip_DD_XSec', prescale=1.):
    """
    D0 --> KS0(DD) pi+ pi- line maker
    """
    kshorts = _xsec_make_KSDD_from_d()
    pions = _xsec_make_long_pions_from_d()
    dzeros = _xsec_make_dzero_Kshh(kshorts, pions, pions, "D0 -> KS0 pi- pi+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dzero2ksLLkk_line(name='Hlt2Charm_D0ToKsKmKp_LL_XSec', prescale=1.):
    """
    D0 --> KS0(DD) K+ K- line maker
    """
    kshorts = _xsec_make_KSLL_from_d()
    kaons = _xsec_make_long_kaons_from_d()
    dzeros = _xsec_make_dzero_Kshh(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dzero2ksDDkk_line(name='Hlt2Charm_D0ToKsKmKp_DD_XSec', prescale=1.):
    """
    D0 --> KS0(DD) K+ K- line maker
    """
    kshorts = _xsec_make_KSDD_from_d()
    kaons = _xsec_make_long_kaons_from_d()
    dzeros = _xsec_make_dzero_Kshh(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2ksLLpipi_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsPimPip_LL_XSec', prescale=1.):
    """
    D*+ --> D0 pi+;  D0 --> KS0(LL) pi+ pi- and C.C. line maker

    Used for D0-D0bar tagging
    """
    kshorts = _xsec_make_KSLL_from_d()
    pions = _xsec_make_long_pions_from_d()
    dzeros = _xsec_make_dzero_Kshh(kshorts, pions, pions, "D0 -> KS0 pi- pi+")
    dstars = _xsec_make_dstars_D0pi(
        dzeros,
        _xsec_make_long_soft_pions_from_d(),
        self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2ksDDpipi_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsPimPip_DD_XSec', prescale=1.):
    """
    D*+ --> D0 pi+;  D0 --> KS0(DD) pi+ pi- and C.C. line maker

    Used for D0-D0bar tagging
    """
    kshorts = _xsec_make_KSDD_from_d()
    pions = _xsec_make_long_pions_from_d()
    dzeros = _xsec_make_dzero_Kshh(kshorts, pions, pions, "D0 -> KS0 pi- pi+")
    dstars = _xsec_make_dstars_D0pi(
        dzeros,
        _xsec_make_long_soft_pions_from_d(),
        self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2ksLLkk_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmKp_LL_XSec', prescale=1.):
    """
    D*+ --> D0 pi+;  D0 --> KS0(LL) K+ K- and C.C. line maker

    Used for D0-D0bar tagging
    """
    kshorts = _xsec_make_KSLL_from_d()
    kaons = _xsec_make_long_kaons_from_d()
    dzeros = _xsec_make_dzero_Kshh(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    dstars = _xsec_make_dstars_D0pi(
        dzeros,
        _xsec_make_long_soft_pions_from_d(),
        self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2ksDDkk_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKsKmKp_DD_XSec', prescale=1.):
    """
    D*+ --> D0 pi+;  D0 --> KS0(DD) K+ K- and C.C. line maker

    Used for D0-D0bar tagging
    """
    kshorts = _xsec_make_KSDD_from_d()
    kaons = _xsec_make_long_kaons_from_d()
    dzeros = _xsec_make_dzero_Kshh(kshorts, kaons, kaons, "D0 -> KS0 K- K+")
    dstars = _xsec_make_dstars_D0pi(
        dzeros,
        _xsec_make_long_soft_pions_from_d(),
        self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kshorts, dzeros, dstars],
        prescale=prescale)
