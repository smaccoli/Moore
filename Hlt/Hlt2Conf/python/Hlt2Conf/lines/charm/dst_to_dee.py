###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of HLT2 lines for the dark photon search A'(-> e+ e-) produced in a D* decay.
   The lines also preserve photons near the dielectron candidates in order to reconstruct,
   later, {pi0, eta} -> e+ e- gamma, for background studies.

Prompt lines
============

 1. D*0  -> D0    e+ e-, D0    -> K- pi+                 Hlt2Charm_Dst0ToD0EmEp_D0ToKmPip
 2. D*0  -> D0    e+ e-, D0    -> K- pi- pi+ pi+         Hlt2Charm_Dst0ToD0EmEp_D0ToKmPimPipPip
 3. Ds*+ -> D(s)+ e+ e-, D(s)+ -> K- K+  pi+             Hlt2Charm_DstpToDpDspEmEp_DpDspToKmKpPip


TODO: displaced lines, include upstream tracks, tune when possible: F.CHI2DOF, F.CHI2, F.GHOSTPROB, F.P, F.BPVLTIME 
"""
import math
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mrad, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from PyConf.Algorithms import WeightedRelTableAlg, FunctionalDiElectronMaker
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import (ParticleFilter, ParticleCombiner)
from Hlt2Conf.standard_particles import (
    make_long_electrons_no_brem,
    make_has_rich_long_pions,
    make_has_rich_long_kaons,
    make_photons,
)
from Hlt2Conf.lines.charm.particle_properties import _PION_M
from Hlt2Conf.lines.charm.prefilters import charm_prefilters

################################################################################
# Prompt lines
################################################################################


def make_pions_for_d0_to_hh():
    """Return pions maker for D0 -> hh decay."""
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K < 5.,
            ), ),
    )


def make_kaons_for_d0_to_hh():
    """Return kaons maker for D0 -> hh decay."""
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K > 5.,
            ), ),
    )


def make_pions_for_d0_to_hhhh():
    """Return pions maker for D0 -> hhhh decay.
    TODO:F.CHI2DOF, F.GHOSTPROB and F.P tunning. 
    """
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=3.5, Vertices=make_pvs()),
                F.PT > 250 * MeV,
                # TODO,
                # F.P > p_min,
                # F.CHI2DOF < trchi2dof_max,
                # F.GHOSTPROB < trghostprob,
                F.PID_K < 5.,
            ), ),
    )


def make_kaons_for_d0_to_hhhh():
    """Return kaons maker for D0 -> hhhh decay.
    TODO:F.CHI2DOF, F.GHOSTPROB and F.P tunning. 
    """
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=3.5, Vertices=make_pvs()),
                F.PT > 250 * MeV,
                # TODO,
                # F.P > p_min,
                # F.CHI2DOF < trchi2dof_max,
                # F.GHOSTPROB < trghostprob,
                F.PID_K > 5.,
            ), ),
    )


def make_pions_for_d_to_hhh():
    """Return pions maker for Ds+ -> hhh decay.
    TODO: F.GHOSTPROB tunning.
    """

    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 2 * GeV,
                F.CHI2DOF < 99.,
                # TODO,
                #F.GHOSTPROB < trghostprob,
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PID_K < 5.,
            ), ),
    )


def make_kaons_for_d_to_hhh():
    """
    Return kaons maker for Ds+ -> hhh decay.
    TODO: F.GHOSTPROB tunning.
    """

    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 2 * GeV,
                F.CHI2DOF < 99.,
                #TODO
                #F.GHOSTPROB < trghostprob,
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PID_K > 5.,
            ), ),
    )


@configurable
def make_photons_to_preserve():
    """Return photon maker filtered for preservation."""
    return ParticleFilter(
        make_photons(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 0 * MeV,
                F.IS_PHOTON > 0.0,
                F.IS_NOT_H > 0.2,
                F.CALO_NEUTRAL_SHOWER_SHAPE > 0,
            ), ),
    )


@configurable
def make_prompt_electrons_no_brem(pt_min=50 * MeV):
    """Return prompt electron maker without bremsstrahlung correction.
    TODO: F.GHOSTPROB and F.CHI2 tunning. 
    """

    return ParticleFilter(
        make_long_electrons_no_brem(),
        F.FILTER(
            F.require_all(
                F.PT > pt_min,
                # TODO,
                #F.GHOSTPROB < trghostprob,
                #F.CHI2 < trchi2_max,
                F.MINIPCHI2CUT(IPChi2Cut=0., Vertices=make_pvs()),
                F.PID_E > 0.,
            ), ),
    )


@configurable
def make_TwoElectrons_with_brem(electrons,
                                diElectonID="gamma",
                                isOS=True,
                                pt_diE_min=0 * MeV,
                                m_diE_min=0 * MeV,
                                m_diE_max=250 * MeV,
                                vfaspfchi2ndof=9.0):
    """Make gamma -> e+ e- candidates adding bremsstrahlung correction to the
    electrons"""

    dielectron_with_brem = FunctionalDiElectronMaker(
        InputParticles=electrons,
        MinDiElecPT=pt_diE_min,
        MinDiElecMass=m_diE_min,
        MaxDiElecMass=m_diE_max,
        DiElecID=diElectonID,
        OppositeSign=isOS).Particles

    vertex_code = F.require_all(F.CHI2DOF < vfaspfchi2ndof)

    return ParticleFilter(dielectron_with_brem, F.FILTER(vertex_code))


def make_dzeros_for_hh(particle1, particle2, descriptor):
    """Return D0 maker with selection tailored for two-body hadronic final states.

    Args:
        particles (list of DataHandles): Input particles used in the combination.
        descriptor (string): Decay descriptor to be reconstructed.
    """
    if descriptor == "[D0 -> K- pi+]cc":
        name = 'Charm_D0ToHH_BuilderD0ToKmPip'
    else:
        raise RuntimeError(
            f"charm.dst_to_dee.make_dzeros_for_hh called with unsupported decay descriptor '{descriptor}'"
        )
    return ParticleCombiner(
        [particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=F.require_all(
            in_range(1685 * MeV, F.MASS, 2045 * MeV),
            F.PT > 2 * GeV,
            F.MAX(F.PT) > 1200 * MeV,
            F.MAXDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            in_range(1715 * MeV, F.MASS, 2015 * MeV),
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(make_pvs()) > 25.,
            F.BPVDIRA(make_pvs()) > 0.99985,
        ),
    )


def make_dzeros_for_hhhh(particle1, particle2, particle3, particle4,
                         descriptor):
    """Return D0 maker with selection tailored for four-body hadronic final states.

    Args:
        particles (list of DataHandles): Input particles used in the combination.
        descriptor (string): Decay descriptor to be reconstructed.
    TODO: F.BPVLTIME tunning
    """
    if descriptor == '[D0 -> K- pi- pi+ pi+]cc':
        name = "Charm_D0ToHHHH_BuilderD0ToKmPimPipPip"
    else:
        raise RuntimeError(
            f"charm.dst_to_dee.make_dzeros_for_hhhh called with unsupported decay descriptor '{descriptor}'"
        )
    # CombinationCut
    comb_m_max = 2100 * MeV
    combination_cut = F.require_all(
        in_range(1700 * MeV, F.MASS, comb_m_max), F.P > 25 * GeV,
        F.PT > 1900 * MeV,
        F.MAX(F.PT) > 1200 * MeV, F.MAXDOCACUT(0.1 * mm))
    # CompositeCut
    composite_cut = F.require_all(
        in_range(1790 * MeV, F.MASS, 2010 * MeV),
        F.P > 30 * GeV,
        F.PT > 2500 * MeV,
        F.CHI2DOF < 12.,
        F.BPVFDCHI2(make_pvs()) > 25.,
        # F.BPVLTIME(make_pvs()) > 0.1 * ps, # TODO
        F.BPVDIRA(make_pvs()) > 0.9998)

    return ParticleCombiner(
        [particle1, particle2, particle3, particle4],
        DecayDescriptor=descriptor,
        name=name,
        Combination12Cut=F.require_all(
            F.MASS < comb_m_max - 2 * _PION_M,
            F.MAXDOCACUT(0.1 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < comb_m_max - _PION_M,
            F.MAXDOCACUT(0.1 * mm),
        ),
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


def make_ds_to_hhh(particle1, particle2, particle3, descriptor,
                   with_cuts=True):
    """Return Ds+ maker with selection tailored for three-body hadronic final states.

    Args:
        particles (list of DataHandles): Input particles used in the combination.
        descriptor (string): Decay descriptor to be reconstructed.
        witht_cuts (boolean): True if the selection includes IPCHI2 and DIRA requirements.
    """
    if descriptor == '[D_s+ -> K- K+ pi+]cc':
        name = "Charm_DToHHH_BuilderDspToKmKpPip"
    else:
        raise RuntimeError(
            f"charm.dst_to_dee.make_ds_to_hhh called with unsupported decay descriptor '{descriptor}'"
        )
    pvs = make_pvs()

    # CombinationCut
    combination_cut = F.require_all(
        in_range(1779 * MeV, F.MASS, 2059 * MeV),
        F.MAX(F.PT) > 1 * GeV,
        F.SUM(F.PT > 400 * MeV) > 1,
        F.SUM(F.PT) > 3 * GeV,
        F.MAX(F.MINIPCHI2(pvs)) > 50,
        F.SUM(F.MINIPCHI2(pvs) > 10.) > 1,
        F.DOCA(1, 3) < 0.1 * mm,
        F.DOCA(2, 3) < 0.1 * mm)

    # CompositeCut
    composite_cut = F.require_all(
        in_range(1789 * MeV, F.MASS, 2049 * MeV), F.CHI2DOF < 9.,
        F.BPVLTIME(pvs) > 0.2 * picosecond, F.PT > 2.5 * GeV,
        F.BPVDIRA(pvs) > math.cos(14.1 * mrad),
        F.BPVIPCHI2(pvs) < 20.)

    return ParticleCombiner(
        Inputs=[particle1, particle2, particle3],
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=F.require_all(F.MAXDOCACUT(0.1 * mm)),
        CombinationCut=combination_cut,
        CompositeCut=composite_cut)


@configurable
def make_dst_to_dee(dzeros,
                    dielectron,
                    descriptor,
                    name,
                    adm_max=270 * MeV,
                    dm_max=250 * MeV,
                    vchi2pdof_max=25.):
    """
    Return a D*0(Ds*+) made of a D0(Ds+) with a e+ e- dielectron pair.

    Args:
        dzeros (DataHandle): Input D0(Ds+).
        descriptor (string): Decay descriptor for the combination, e.g.
                            `[D*(2007)0 -> D0 gamma]cc`,
                            gamma represents e+ e- pair
        name (string): Name for the combiner algorithm.
    """
    return ParticleCombiner(
        [dzeros, dielectron],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=F.MASS - F.CHILD(1, F.MASS) < adm_max,
        CompositeCut=F.require_all(F.MASS - F.CHILD(1, F.MASS) < dm_max,
                                   F.CHI2DOF < vchi2pdof_max))


###############################################################################
# Lines definition
###############################################################################

all_lines = {}


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpi_line(name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPip",
                                 prescale=1.):
    kaons = make_kaons_for_d0_to_hh()
    pions = make_pions_for_d0_to_hh()
    dzeros = make_dzeros_for_hh(kaons, pions, "[D0 -> K- pi+]cc")

    electrons = make_prompt_electrons_no_brem()
    dielectrons = make_TwoElectrons_with_brem(electrons)

    dst0s = make_dst_to_dee(dzeros, dielectrons, "D*(2007)0 -> D0 gamma",
                            "Dst0ToD0EmEp_D0ToKmPip_combiner")

    photons_table = WeightedRelTableAlg(
        InputCandidates=make_photons_to_preserve(),
        ReferenceParticles=dielectrons,
        Cut=in_range(0. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s, photons_table],
        prescale=prescale,
        persistreco=True,
        extra_outputs=[('Dst0ToD0EmEp_D0ToKmPip_prompt_Photons',
                        photons_table.OutputRelations)],
        #hlt1_filter_code=["Hlt1.*MVADecision"],
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpipipi_line(
        name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPimPipPip", prescale=1.):
    kaons = make_kaons_for_d0_to_hhhh()
    pions = make_pions_for_d0_to_hhhh()
    dzeros = make_dzeros_for_hhhh(kaons, pions, pions, pions,
                                  '[D0 -> K- pi- pi+ pi+]cc')

    electrons = make_prompt_electrons_no_brem()
    dielectrons = make_TwoElectrons_with_brem(electrons)

    dst0s = make_dst_to_dee(dzeros, dielectrons, "D*(2007)0 -> D0 gamma",
                            "Dst0ToD0EmEp_D0ToKmPimPipPip_combiner")

    photons_table = WeightedRelTableAlg(
        InputCandidates=make_photons_to_preserve(),
        ReferenceParticles=dielectrons,
        Cut=in_range(0. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s, photons_table],
        prescale=prescale,
        persistreco=True,
        extra_outputs=[('Dst0ToD0EmEp_D0ToKmPimPipPip_prompt_Photons',
                        photons_table.OutputRelations)],
        #hlt1_filter_code=["Hlt1.*MVADecision"]
    )


@register_line_builder(all_lines)
@configurable
def dstarplus2dsee_dsp2kkpi_line(
        name="Hlt2Charm_DstpToDpDspEmEp_DpDspToKmKpPip", prescale=1.):
    kaons = make_kaons_for_d_to_hhh()
    pions = make_pions_for_d_to_hhh()
    dsplus = make_ds_to_hhh(kaons, kaons, pions, '[D_s+ -> K- K+ pi+]cc')

    electrons = make_prompt_electrons_no_brem()
    dielectrons = make_TwoElectrons_with_brem(electrons)

    dstp = make_dst_to_dee(dsplus, dielectrons, "[D*_s+ -> D_s+ gamma]cc",
                           "DstpToDpDspEmEp_DpDspToKmKpPip_combiner")

    photons_table = WeightedRelTableAlg(
        InputCandidates=make_photons_to_preserve(),
        ReferenceParticles=dielectrons,
        Cut=in_range(0. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstp, photons_table],
        prescale=prescale,
        persistreco=True,
        extra_outputs=[('DstpToDpDspEmEp_DpDspToKmKpPip_prompt_Photons',
                        photons_table.OutputRelations)],
        #hlt1_filter_code=["Hlt1.*MVADecision"],
    )
