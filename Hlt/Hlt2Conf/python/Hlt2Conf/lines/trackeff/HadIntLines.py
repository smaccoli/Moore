###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''Definition of Charm Meson and Baryon lines for Production Cross Section

For now this targets Early Run 3 Measurement.

Cuts are (mostly) the same as for Run 2. The idea is to have as loose kinematic
cuts as possible.

--------------------
two-body lines:
--------------------

[D*(2010)+ -> (D0 -> K- pi+ ) pi+]cc          Hlt2Charm_DstpToD0Pip_D0ToKmPip_Hadint
[D*(2010)+ -> (D0 -> K- pi- pi+ pi+ ) pi+]cc  Hlt2Charm_DstpToD0Pip_D0ToKmPimPipPip_Hadint

'''
import math

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, mrad
from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.standard_particles import (
    make_long_pions, make_has_rich_long_pions, make_has_rich_long_kaons)
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from ..charm.prefilters import charm_prefilters
from ..charm.particle_properties import _PION_M, _D0_M

#################################################################################
############################## CHARM MESONS #####################################
#################################################################################


def _hadint_make_long_pions_from_d() -> ParticleFilter:
    """Maker for long pion tracks for/from D meson
    Returns:
        ParticleFilter: Object applying cuts to particles.
    """

    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 2 * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=9., Vertices=make_pvs()),
            ), ),
    )


def _hadint_make_long_kaons_from_d() -> ParticleFilter:
    """Maker for long kaon tracks for/from D meson
    Returns:
        ParticleFilter: Object applying cuts to particles.
    """
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 2 * GeV,
                F.PID_K > 0.,
                F.MINIPCHI2CUT(IPChi2Cut=9., Vertices=make_pvs()),
            ), ),
    )


def _hadint_make_long_soft_pions_from_d() -> ParticleFilter:
    """Maker for soft long pion tracks for/from D* meson
    Returns:
        ParticleFilter: Object applying cuts to particles.
    """
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(
            F.PT > 100 * MeV,
            F.P > 1.5 * GeV,
        ), ),
    )


@configurable
def _hadint_make_dzeros_Kpi(
        comb_m_window: float = 130 * MeV,
        m_window: float = 120 * MeV,
        sum_pt_min: float = 2000 * MeV,
        doca_max: float = 0.15 * mm,
        vchi2dof_max: float = 10.,
        bpvfdchi2_min: float = 49.,
        bpvdira_min: float = math.cos(17 * mrad),
        sum_mipchi2_min: float = 50,
) -> ParticleCombiner:
    """Maker of D0->Kpi.

    Args:
        comb_m_window (float, optional): Mass window around M(HEAD) after simple four-momenta combination. Defaults to 130*MeV.
        m_window (float, optional): Mass window around M(HEAD) after vertex fit. Defaults to 120*MeV.
        sum_pt_min (float, optional): Maximum scalar sum of daughter transverse momenta. Defaults to 1500*MeV.
        doca_max (float, optional): Maximum distance of closest approach of daughters. Defaults to 0.1*mm.
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 10..
        bpvfdchi2_min (float, optional): Minimum flight distance chi2 of HEAD from best PV. Defaults to 49..
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(20 * mrad).
        bpvvdrho_max (float, optional): Maximum radial distance from HEAD's endvertex to best PV. Defaults to 3*mm.
        sum_mipchi2_min (float, optional): Minimum sum of minIPchi2 values for datas. Defaults to 50.

    Returns:
        ParticleCombiner: Combination of :func:`~Hlt2Conf.lines.charm.prod_hadint._hadint_make_long_kaons_from_d`
        and :func:`~Hlt2Conf.lines.charm.prod_hadint._hadint_make_long_pions_from_d`
    """
    return ParticleCombiner(
        [_hadint_make_long_kaons_from_d(),
         _hadint_make_long_pions_from_d()],
        DecayDescriptor="[D0 -> K- pi+]cc",
        name='Charm_Hadint_D0_KmPip_{hash}',
        CombinationCut=F.require_all(
            F.SUM(F.PT) > sum_pt_min,
            in_range(_D0_M - comb_m_window, F.MASS, _D0_M + comb_m_window),
            F.MAXDOCACUT(doca_max),
            F.SUM(F.MINIPCHI2(make_pvs())) > sum_mipchi2_min,
        ),
        CompositeCut=F.require_all(
            F.CHI2DOF < vchi2dof_max,
            in_range(_D0_M - m_window, F.MASS, _D0_M + m_window),
            F.BPVFDCHI2(make_pvs()) > bpvfdchi2_min,
            F.BPVDIRA(make_pvs()) > bpvdira_min,
        ),
    )


@configurable
def _hadint_make_dzeros_Kpipipi(
        comb_m_window=130 * MeV,
        m_window=120 * MeV,
        sum_pt_min=2000 * MeV,
        doca_max=0.15 * mm,
        bpvfdchi2_min=49.,
        vchi2dof_max=10.,
        bpvdira_min=math.cos(17 * mrad),
        sum_mipchi2_min=50,
):
    """Maker of D0->Kpipipi.

    Args:
        comb_m_window (float, optional): Mass window around M(HEAD) after simple four-momenta combination. Defaults to 130*MeV.
        m_window (float, optional): Mass window around M(HEAD) after vertex fit. Defaults to 120*MeV.
        sum_pt_min (float, optional): Maximum scalar sum of daughter transverse momenta. Defaults to 2000*MeV.
        doca_max (float, optional): Maximum distance of closest approach of daughters. Defaults to 0.15*mm.
        vchi2dof_max (float, optional): Maximum Chi2/NDoF of fitted HEAD vertex. Defaults to 10..
        bpvfdchi2_min (float, optional): Minimum flight distance chi2 of HEAD from best PV. Defaults to 49..
        bpvdira_min (float, optional): Minimum direction angle of HEAD from best PV. Defaults to math.cos(17 * mrad).
        bpvvdrho_max (float, optional): Maximum radial distance from HEAD's endvertex to best PV. Defaults to 3*mm.
        sum_mipchi2_min (float, optional): Minimum sum of minIPchi2 values for datas. Defaults to 50.

    Returns:
        ParticleCombiner: Combination of :func:`~Hlt2Conf.lines.charm.prod_hadint._hadint_make_long_kaons_from_d`
        and :func:`~Hlt2Conf.lines.charm.prod_hadint._hadint_make_long_pions_from_d`
    """
    return ParticleCombiner(
        [
            _hadint_make_long_kaons_from_d(),
            _hadint_make_long_pions_from_d(),
            _hadint_make_long_pions_from_d(),
            _hadint_make_long_pions_from_d()
        ],
        DecayDescriptor="[D0 -> K- pi- pi+ pi+]cc",
        name='Charm_Hadint_D0_KmPimPipPip_{hash}',
        Combination12Cut=F.MASS < _D0_M + comb_m_window - 2 * _PION_M,
        Combination123Cut=F.MASS < _D0_M + comb_m_window - _PION_M,
        CombinationCut=F.require_all(
            F.SUM(F.PT) > sum_pt_min,
            in_range(_D0_M - comb_m_window, F.MASS, _D0_M + comb_m_window),
            F.MAXDOCACUT(doca_max),
            F.SUM(F.MINIPCHI2(make_pvs())) > sum_mipchi2_min,
        ),
        CompositeCut=F.require_all(
            in_range(_D0_M - m_window, F.MASS, _D0_M + m_window),
            F.CHI2DOF < vchi2dof_max,
            F.BPVFDCHI2(make_pvs()) > bpvfdchi2_min,
            F.BPVDIRA(make_pvs()) > bpvdira_min,
        ),
    )


@configurable
def _hadint_make_dstars_D0pi(dzeros,
                             pions,
                             comb_delta_m_max=165 * MeV,
                             delta_m_max=160 * MeV,
                             vchi2dof_max=25.):
    return ParticleCombiner(
        [dzeros, pions],
        DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
        name='Charm_Hadint_Dstp_D0pip_{hash}',
        CombinationCut=F.require_all(
            F.MASS - F.CHILD(1, F.MASS) < comb_delta_m_max),
        CompositeCut=F.require_all(
            F.MASS - F.CHILD(1, F.MASS) < delta_m_max,
            F.CHI2DOF < vchi2dof_max,
        ),
    )


all_lines = {}


@register_line_builder(all_lines)
def dzero2kpi_hadint_line(name='Hlt2HadInt_D0ToKmPip', prescale=1.):
    dzeros = _hadint_make_dzeros_Kpi()
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dzeros], prescale=prescale)


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kmpip_hadint_line(
        name='Hlt2HadInt_DstpToD0Pip_D0ToKmPip', prescale=1.):
    dzeros = _hadint_make_dzeros_Kpi()
    dstars = _hadint_make_dstars_D0pi(dzeros,
                                      _hadint_make_long_soft_pions_from_d())
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros, dstars],
        prescale=prescale)


@register_line_builder(all_lines)
def dzero2k3pi_hadint_line(name='Hlt2HadInt_D0ToKmPimPipPip', prescale=1.):
    dzeros = _hadint_make_dzeros_Kpipipi()
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dzeros], prescale=prescale)


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2k3pi_hadint_line(
        name='Hlt2HadInt_DstpToD0Pip_D0ToKmPimPipPip', prescale=1.):
    dzeros = _hadint_make_dzeros_Kpipipi()
    dstars = _hadint_make_dstars_D0pi(dzeros,
                                      _hadint_make_long_soft_pions_from_d())
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros, dstars],
        prescale=prescale)
