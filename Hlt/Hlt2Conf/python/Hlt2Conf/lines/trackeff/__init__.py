# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the Tracking efficiency HLT2 lines
"""

from . import DiMuonTrackEfficiency, HadIntLines, Velo2Long_B2JpsiK_MuonProbe, Velo2Long_B2JpsiK_ElectronProbe

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(DiMuonTrackEfficiency.all_lines)
all_lines.update(HadIntLines.all_lines)
all_lines.update(Velo2Long_B2JpsiK_MuonProbe.all_lines)
all_lines.update(Velo2Long_B2JpsiK_ElectronProbe.all_lines)
