###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of detached D/D0/Ds/... meson decays that are shared between many
B2OC selections, and therefore are defined centrally.
"""
from GaudiKernel.SystemOfUnits import MeV, GeV, mm

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf import algorithms as algorithms_loki
from Hlt2Conf.algorithms import ParticleContainersMerger

import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all

from PyConf import configurable

from . import basic_builder

_D0_M = 1864.84 * MeV  # +/- 0.05, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
_DP_M = 1869.66 * MeV  # +/- 0.05, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
_DS_M = 1968.35 * MeV  # +/- 0.07, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
_DST2010_M = 2010.26 * MeV  # +/- 0.05, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
_DST2460_M = 2460.7 * MeV  # +/- 0.4, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.

###############################################################################
# Specific D decay builders, overrides default cuts where needed              #
###############################################################################

#####################
# Two body D decays #
#####################


@configurable
def _make_dzero_to_kpi(name,
                       descriptor,
                       am_min=1764.84 * MeV,
                       am_max=1964.84 * MeV,
                       pi_pidk_max=5,
                       k_pidk_min=-5,
                       pi_p_min=2 * GeV,
                       pi_pt_min=250 * MeV,
                       k_p_min=2 * GeV,
                       k_pt_min=250 * MeV,
                       **decay_arguments):
    """ helper constructor to unify definition of kmpip and kppim lines """
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_twobody(
        particles=[kaons, pions],
        name=name,
        descriptor=descriptor,
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_dzero_to_kmpip(**decay_arguments):
    return _make_dzero_to_kpi(
        name='B2OCD02KmPipCombiner_{hash}',
        descriptor='D0 -> K- pi+',
        **decay_arguments)


@configurable
def make_dzero_to_kppim(**decay_arguments):
    return _make_dzero_to_kpi(
        name='B2OCD02KpPimCombiner_{hash}',
        descriptor='D0 -> K+ pi-',
        **decay_arguments)


@configurable
def make_dzero_to_pippim(am_min=1764.84 * MeV,
                         am_max=1964.84 * MeV,
                         pi_pidk_max=5,
                         pi_p_min=2 * GeV,
                         pi_pt_min=250 * MeV,
                         **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    return basic_builder.make_twobody(
        particles=[pions, pions],
        descriptor='D0 -> pi+ pi-',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD02PipPimCombiner_{hash}',
        **decay_arguments)


@configurable
def make_dzero_to_kpkm(am_min=1764.84 * MeV,
                       am_max=1964.84 * MeV,
                       k_pidk_min=-5,
                       k_p_min=2 * GeV,
                       k_pt_min=250 * MeV,
                       **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_twobody(
        particles=[kaons, kaons],
        descriptor='D0 -> K+ K-',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD02KpKmCombiner_{hash}',
        **decay_arguments)


@configurable
def make_dzero_to_kpi(name='B2OCD02KPiCombiner_{hash}', **decay_arguments):
    dzero_to_kmpip = make_dzero_to_kmpip(**decay_arguments)
    dzero_to_kppim = make_dzero_to_kppim(**decay_arguments)
    return ParticleContainersMerger([dzero_to_kmpip, dzero_to_kppim],
                                    name=name)


@configurable
def make_dzero_to_hh(name='B2OCD02HHCombiner_{hash}',
                     pi_pidk_max=5,
                     k_pidk_min=-5,
                     pi_p_min=2 * GeV,
                     pi_pt_min=250 * MeV,
                     k_p_min=2 * GeV,
                     k_pt_min=250 * MeV,
                     **decay_arguments):
    dzero_to_kmpip = make_dzero_to_kmpip(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    dzero_to_kppim = make_dzero_to_kppim(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    dzero_to_pippim = make_dzero_to_pippim(
        pi_pidk_max=pi_pidk_max,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        **decay_arguments)
    dzero_to_kpkm = make_dzero_to_kpkm(
        k_pidk_min=k_pidk_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    return ParticleContainersMerger(
        [dzero_to_kmpip, dzero_to_kppim, dzero_to_pippim, dzero_to_kpkm],
        name=name)


@configurable
def make_tight_dzero_to_kpi(name='B2OCD02KPiTightCombiner_{hash}',
                            am_min=1805 * MeV,
                            am_max=1925 * MeV,
                            pi_pidk_max=5,
                            k_pidk_min=-5,
                            **decay_arguments):
    """ make D0->Kpi with tighter mass window, 60 MeV and tighter K, pi selections  """
    return make_dzero_to_kpi(
        name=name,
        am_min=am_min,
        am_max=am_max,
        pi_p_min=3 * GeV,
        pi_pt_min=300 * MeV,
        pi_pidk_max=pi_pidk_max,
        k_p_min=5 * GeV,
        k_pt_min=500 * MeV,
        k_pidk_min=k_pidk_min,
        **decay_arguments)


@configurable
def make_tight_dzero_to_kpi_for_xibc(**decay_arguments):
    """ make D0->Kpi with tighter mass window, 50 MeV and tighter K, pi and FD&vtx selections """
    return make_tight_dzero_to_kpi(
        name='B2OCD02KPiTightForXibcCombiner_{hash}',
        am_min=1815 * MeV,
        am_max=1915 * MeV,
        pi_pidk_max=2,
        k_pidk_min=-2,
        vchi2pdof_max=7,
        bpvvdchi2_min=36,
        **decay_arguments)


@configurable
def make_tight_dzero_to_hh(pi_pidk_max=2, k_pidk_min=-2, **decay_arguments):
    """ tighter D0's for B->D0 hhh """
    return make_dzero_to_hh(
        name='B2OCD02HHTightCombiner_{hash}',
        am_min=1805 * MeV,
        am_max=1925 * MeV,
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        **decay_arguments)


@configurable
def make_dzero_to_kspi0(k_shorts,
                        pi0,
                        am_min=1764.84 * MeV,
                        am_max=1964.84 * MeV,
                        **decay_arguments):
    return basic_builder.make_twobody(
        particles=[k_shorts, pi0],
        descriptor='D0 -> KS0 pi0',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD02KSPi0Combiner_{hash}',
        **decay_arguments)


########################
# Two body WS D decays #
########################


@configurable
def _make_dzero_to_hh_ws(name,
                         particles,
                         descriptor,
                         am_min=1764.84 * MeV,
                         am_max=1964.84 * MeV,
                         **decay_arguments):
    return basic_builder.make_twobody(
        particles=particles,
        descriptor=descriptor,
        am_min=am_min,
        am_max=am_max,
        name=name,
        **decay_arguments)


@configurable
def make_dzero_to_hh_ws(name='B2OCD02HHWSCombiner_{hash}',
                        pi_pidk_max=5,
                        k_pidk_min=-5,
                        pi_p_min=2 * GeV,
                        pi_pt_min=250 * MeV,
                        k_p_min=2 * GeV,
                        k_pt_min=250 * MeV,
                        **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    dzero_to_kppip_ws = _make_dzero_to_hh_ws(
        name='B2OCD02KpPipWSCombiner_{hash}',
        particles=[kaons, pions],
        descriptor='D0 -> K+ pi+',
        **decay_arguments)
    dzero_to_kmpim_ws = _make_dzero_to_hh_ws(
        name='B2OCD02KmPimWSCombiner_{hash}',
        particles=[kaons, pions],
        descriptor='D0 -> K- pi-',
        **decay_arguments)
    dzero_to_pippip_ws = _make_dzero_to_hh_ws(
        name='B2OCD02PipPipWSCombiner_{hash}',
        particles=[pions, pions],
        descriptor='D0 -> pi+ pi+',
        **decay_arguments)
    dzero_to_pimpim_ws = _make_dzero_to_hh_ws(
        name='B2OCD02PimPimWSCombiner_{hash}',
        particles=[pions, pions],
        descriptor='D0 -> pi- pi-',
        **decay_arguments)
    dzero_to_kpkp_ws = _make_dzero_to_hh_ws(
        name='B2OCD02KpKpWSCombiner_{hash}',
        particles=[kaons, kaons],
        descriptor='D0 -> K+ K+',
        **decay_arguments)
    dzero_to_kmkm_ws = _make_dzero_to_hh_ws(
        name='B2OCD02KmKmWSCombiner_{hash}',
        particles=[kaons, kaons],
        descriptor='D0 -> K- K-',
        **decay_arguments)
    return ParticleContainersMerger([
        dzero_to_kppip_ws, dzero_to_kmpim_ws, dzero_to_pippip_ws,
        dzero_to_pimpim_ws, dzero_to_kpkp_ws, dzero_to_kmkm_ws
    ],
                                    name=name)


@configurable
def make_tight_dzero_to_hh_ws(pi_pidk_max=2, k_pidk_min=-2, **decay_arguments):
    """ tighter D0's for B->D0 hh """
    return make_dzero_to_hh_ws(
        name='B2OCD02HHWSTightCombiner_{hash}',
        am_min=1805 * MeV,
        am_max=1925 * MeV,
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        **decay_arguments)


#######################
# Three body D decays #
#######################


@configurable
def make_dzero_to_kspippim(k_shorts,
                           am_min=1764.84 * MeV,
                           am_max=1964.84 * MeV,
                           pi_pidk_max=5,
                           pi_p_min=2 * GeV,
                           pi_pt_min=250 * MeV,
                           **decay_arguments):
    """
    Defines the default B2OC D->Kshh decay (Ks pi+ pi- in this functiono)

    Parameters
    ----------
    k_shorts :
      selected KS candidate maker (typically choose LL or DD)
    **decay_arguments : keyword arguments are passed on to the D->3body
                        maker
    """
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    return basic_builder.make_threebody(
        particles=[k_shorts, pions, pions],
        descriptor='D0 -> KS0 pi+ pi-',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD02KSPipPimCombiner_{hash}',
        adoca12_max=None,
        adoca13_max=None,
        **decay_arguments)


@configurable
def make_dzero_to_kskppim(k_shorts,
                          am_min=1764.84 * MeV,
                          am_max=1964.84 * MeV,
                          pi_pidk_max=5,
                          k_pidk_min=-5,
                          pi_p_min=2 * GeV,
                          pi_pt_min=250 * MeV,
                          k_p_min=2 * GeV,
                          k_pt_min=250 * MeV,
                          **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_threebody(
        particles=[k_shorts, kaons, pions],
        descriptor='D0 -> KS0 K+ pi-',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD02KSKpPimCombiner_{hash}',
        adoca12_max=None,
        adoca13_max=None,
        **decay_arguments)


@configurable
def make_dzero_to_kskmpip(k_shorts,
                          am_min=1764.84 * MeV,
                          am_max=1964.84 * MeV,
                          pi_pidk_max=5,
                          k_pidk_min=-5,
                          pi_p_min=2 * GeV,
                          pi_pt_min=250 * MeV,
                          k_p_min=2 * GeV,
                          k_pt_min=250 * MeV,
                          **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_threebody(
        particles=[k_shorts, kaons, pions],
        descriptor='D0 -> KS0 K- pi+',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD02KSKmPipCombiner_{hash}',
        adoca12_max=None,
        adoca13_max=None,
        **decay_arguments)


@configurable
def make_dzero_to_kskpkm(k_shorts,
                         am_min=1764.84 * MeV,
                         am_max=1964.84 * MeV,
                         k_pidk_min=-5,
                         k_p_min=2 * GeV,
                         k_pt_min=250 * MeV,
                         **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_threebody(
        particles=[k_shorts, kaons, kaons],
        descriptor='D0 -> KS0 K+ K-',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD02KSKpKmCombiner_{hash}',
        adoca12_max=None,
        adoca13_max=None,
        **decay_arguments)


@configurable
def make_dzero_to_kshh(k_shorts,
                       name="B2OCD02KSHHCombiner_{hash}",
                       pi_pidk_max=5,
                       k_pidk_min=-5,
                       pi_p_min=2 * GeV,
                       pi_pt_min=250 * MeV,
                       k_p_min=2 * GeV,
                       k_pt_min=250 * MeV,
                       **decay_arguments):
    dzero_to_kspippim = make_dzero_to_kspippim(
        k_shorts,
        pi_pidk_max=pi_pidk_max,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        **decay_arguments)
    dzero_to_kskppim = make_dzero_to_kskppim(
        k_shorts,
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    dzero_to_kskmpip = make_dzero_to_kskmpip(
        k_shorts,
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    dzero_to_kskpkm = make_dzero_to_kskpkm(
        k_shorts,
        k_pidk_min=k_pidk_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    return ParticleContainersMerger([
        dzero_to_kspippim, dzero_to_kskppim, dzero_to_kskmpip, dzero_to_kskpkm
    ],
                                    name=name)


#######################
# D+ decays           #
#######################


@configurable
def make_dplus_to_pippippim(am_min=1830 * MeV,
                            am_max=1910 * MeV,
                            pi_p_min=2 * GeV,
                            pi_pt_min=250 * MeV,
                            pi_pidk_max=5,
                            **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    return basic_builder.make_threebody(
        particles=[pions, pions, pions],
        descriptor='[D+ -> pi+ pi+ pi-]cc',
        am_min=am_min,
        am_max=am_max,
        name='B2OCDs2PiPiPiCombiner_{hash}',
        **decay_arguments)


@configurable
def make_dplus_to_kmpippip(am_min=1830 * MeV,
                           am_max=1910 * MeV,
                           pi_p_min=2 * GeV,
                           pi_pt_min=250 * MeV,
                           pi_pidk_max=5,
                           k_p_min=2 * GeV,
                           k_pt_min=250 * MeV,
                           k_pidk_min=-5,
                           **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_threebody(
        particles=[kaons, pions, pions],
        descriptor='[D+ -> K- pi+ pi+]cc',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD2KPiPiCombiner_{hash}',
        **decay_arguments)


@configurable
def make_dplus_to_kpkmpip(am_min=1830 * MeV,
                          am_max=1910 * MeV,
                          pi_p_min=2 * GeV,
                          pi_pt_min=250 * MeV,
                          pi_pidk_max=5,
                          k_p_min=2 * GeV,
                          k_pt_min=250 * MeV,
                          k_pidk_min=-5,
                          **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_threebody(
        particles=[kaons, kaons, pions],
        descriptor='[D+ -> K+ K- pi+]cc',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD2KKPiCombiner_{hash}',
        **decay_arguments)


@configurable
def make_dplus_to_kpipi_or_kkpi(name='B2OCD2KPiPiOrKKPiCombiner_{hash}',
                                **decay_arguments):
    dplus_to_kmpippip = make_dplus_to_kmpippip(**decay_arguments)
    dplus_to_kpkmpip = make_dplus_to_kpkmpip(**decay_arguments)
    return ParticleContainersMerger([dplus_to_kmpippip, dplus_to_kpkmpip],
                                    name=name)


@configurable
def make_dplus_to_hhh(name='B2OCD2HHHCombiner_{hash}',
                      pi_p_min=2 * GeV,
                      pi_pt_min=250 * MeV,
                      pi_pidk_max=5,
                      k_p_min=2 * GeV,
                      k_pt_min=250 * MeV,
                      k_pidk_min=-5,
                      **decay_arguments):
    dplus_to_pippippim = make_dplus_to_pippippim(
        pi_pidk_max=pi_pidk_max,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        **decay_arguments)
    dplus_to_kmpippip = make_dplus_to_kmpippip(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    dplus_to_kpkmpip = make_dplus_to_kpkmpip(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    return ParticleContainersMerger(
        [dplus_to_pippippim, dplus_to_kmpippip, dplus_to_kpkmpip], name=name)


@configurable
def make_tight_dplus_to_kmpippip(pi_pidk_max=5,
                                 k_pidk_min=-5,
                                 **decay_arguments):
    """ make D+->Kpipi with tighter K, pi selections  """
    return make_dplus_to_kmpippip(
        #name='B2OCDp2KPiPiTightCombiner',
        pi_p_min=3 * GeV,
        pi_pt_min=300 * MeV,
        pi_pidk_max=pi_pidk_max,
        k_p_min=5 * GeV,
        k_pt_min=500 * MeV,
        k_pidk_min=k_pidk_min,
        **decay_arguments)


@configurable
def make_tight_dplus_to_kmpippip_for_xibc(**decay_arguments):
    """ make D+->Kpipi with tighter mass & K, pi selections  """
    return make_tight_dplus_to_kmpippip(
        am_min=1830 * MeV,
        am_max=1910 * MeV,
        pi_pidk_max=2,
        k_pidk_min=-2,
        **decay_arguments)


@configurable
def make_dsplus_to_pippippim(am_min=1930 * MeV,
                             am_max=2010 * MeV,
                             pi_p_min=2 * GeV,
                             pi_pt_min=250 * MeV,
                             pi_pidk_max=5,
                             **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    return basic_builder.make_threebody(
        particles=[pions, pions, pions],
        descriptor='[D_s+ -> pi+ pi+ pi-]cc',
        am_min=am_min,
        am_max=am_max,
        name='B2OCDs2PiPiPiCombiner_{hash}',
        **decay_arguments)


@configurable
def make_dsplus_to_kppippim(am_min=1930 * MeV,
                            am_max=2010 * MeV,
                            pi_p_min=2 * GeV,
                            pi_pt_min=250 * MeV,
                            pi_pidk_max=5,
                            k_p_min=2 * GeV,
                            k_pt_min=250 * MeV,
                            k_pidk_min=-5,
                            **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_threebody(
        particles=[kaons, pions, pions],
        descriptor='[D_s+ -> K+ pi+ pi-]cc',
        am_min=am_min,
        am_max=am_max,
        name='B2OCDs2KPiPiCombiner_{hash}',
        **decay_arguments)


@configurable
def make_dsplus_to_kpkmpip(am_min=1930 * MeV,
                           am_max=2010 * MeV,
                           pi_p_min=2 * GeV,
                           pi_pt_min=250 * MeV,
                           pi_pidk_max=5,
                           k_p_min=2 * GeV,
                           k_pt_min=250 * MeV,
                           k_pidk_min=-5,
                           **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_threebody(
        particles=[kaons, kaons, pions],
        descriptor='[D_s+ -> K+ K- pi+]cc',
        am_min=am_min,
        am_max=am_max,
        name='B2OCDs2KKPiCombiner_{hash}',
        **decay_arguments)


@configurable
def make_dsplus_to_hhh(name='B2OCDs2HHHCombiner_{hash}',
                       pi_p_min=2 * GeV,
                       pi_pt_min=250 * MeV,
                       pi_pidk_max=5,
                       k_p_min=2 * GeV,
                       k_pt_min=250 * MeV,
                       k_pidk_min=-5,
                       **decay_arguments):
    dsplus_to_pippippim = make_dsplus_to_pippippim(
        pi_pidk_max=pi_pidk_max,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        **decay_arguments)
    dsplus_to_kppippim = make_dsplus_to_kppippim(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    dsplus_to_kpkmpip = make_dsplus_to_kpkmpip(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    return ParticleContainersMerger(
        [dsplus_to_pippippim, dsplus_to_kppippim, dsplus_to_kpkmpip],
        name=name)


# D -> hhh WS KKPi, KPiPi, PiPiPi #
@configurable
def _make_dplus_to_hhh_ws(name,
                          particles,
                          descriptor,
                          am_min=1830. * MeV,
                          am_max=1910. * MeV,
                          **decay_arguments):
    return basic_builder.make_threebody(
        particles=particles,
        descriptor=descriptor,
        am_min=am_min,
        am_max=am_max,
        name=name,
        **decay_arguments)


@configurable
def make_dplus_to_hhh_ws(name='B2OCD2HHHWSCombiner_{hash}',
                         pi_p_min=2 * GeV,
                         pi_pt_min=250 * MeV,
                         pi_pidk_max=5,
                         k_p_min=2 * GeV,
                         k_pt_min=250 * MeV,
                         k_pidk_min=-5,
                         **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    dplus_to_pippippip_ws = _make_dplus_to_hhh_ws(
        name='B2OCD2PiPiPiWSCombiner_{hash}',
        particles=[pions, pions, pions],
        descriptor='[D+ -> pi+ pi+ pi+]cc',
        **decay_arguments)
    dplus_to_kppippip_ws = _make_dplus_to_hhh_ws(
        name='B2OCD2KPiPiWSCombiner_{hash}',
        particles=[kaons, pions, pions],
        descriptor='[D+ -> K+ pi+ pi+]cc',
        **decay_arguments)
    dplus_to_kpkppip_ws = _make_dplus_to_hhh_ws(
        name='B2OCD2KKPiWSCombiner_{hash}',
        particles=[kaons, kaons, pions],
        descriptor='[D+ -> K+ K+ pi+]cc',
        **decay_arguments)
    return ParticleContainersMerger(
        [dplus_to_pippippip_ws, dplus_to_kppippip_ws, dplus_to_kpkppip_ws],
        name=name)


# Ds -> hhh WS KKPi, KPiPi, PiPiPi #
@configurable
def _make_dsplus_to_hhh_ws(name,
                           particles,
                           descriptor,
                           am_min=1930. * MeV,
                           am_max=2010. * MeV,
                           **decay_arguments):
    return basic_builder.make_threebody(
        particles=particles,
        descriptor=descriptor,
        am_min=am_min,
        am_max=am_max,
        name=name,
        **decay_arguments)


@configurable
def make_dsplus_to_hhh_ws(name='B2OCDs2HHHWSCombiner_{hash}',
                          pi_p_min=2 * GeV,
                          pi_pt_min=250 * MeV,
                          pi_pidk_max=5,
                          k_p_min=2 * GeV,
                          k_pt_min=250 * MeV,
                          k_pidk_min=-5,
                          **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    dsplus_to_pippippip_ws = _make_dsplus_to_hhh_ws(
        name='B2OCDs2PiPiPiWSCombiner_{hash}',
        particles=[pions, pions, pions],
        descriptor='[D_s+ -> pi+ pi+ pi+]cc',
        **decay_arguments)
    dsplus_to_kppippip_ws = _make_dsplus_to_hhh_ws(
        name='B2OCD2KPiPiWSCombiner_{hash}',
        particles=[kaons, pions, pions],
        descriptor='[D_s+ -> K+ pi+ pi+]cc',
        **decay_arguments)
    dsplus_to_kpkppip_ws = _make_dsplus_to_hhh_ws(
        name='B2OCDs2KKPiWSCombiner_{hash}',
        particles=[kaons, kaons, pions],
        descriptor='[D_s+ -> K+ K+ pi+]cc',
        **decay_arguments)
    return ParticleContainersMerger(
        [dsplus_to_pippippip_ws, dsplus_to_kppippip_ws, dsplus_to_kpkppip_ws],
        name=name)


@configurable
def make_dzero_to_threebody(name,
                            particles,
                            descriptor,
                            am_min=1764.84 * MeV,
                            am_max=1964.84 * MeV,
                            **decay_arguments):
    return basic_builder.make_threebody(
        particles=particles,
        descriptor=descriptor,
        am_min=am_min,
        am_max=am_max,
        name=name,
        **decay_arguments)


@configurable
def make_dzero_to_hhpi0(pi0,
                        name='B2OCD02HHPi0Combiner_{hash}',
                        pi_pidk_max=5,
                        k_pidk_min=-5,
                        pi_p_min=2 * GeV,
                        pi_pt_min=250 * MeV,
                        k_p_min=2 * GeV,
                        k_pt_min=250 * MeV,
                        **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    dzero_to_kmpippi0 = make_dzero_to_threebody(
        name='B2OCD02KmPipPi0Combiner_{hash}',
        particles=[kaons, pions, pi0],
        descriptor='D0 -> K- pi+ pi0',
        **decay_arguments)
    dzero_to_kppimpi0 = make_dzero_to_threebody(
        name='B2OCD02KpPimPi0WSCombiner_{hash}',
        particles=[kaons, pions, pi0],
        descriptor='D0 -> K+ pi- pi0',
        **decay_arguments)
    dzero_to_pippimpi0 = make_dzero_to_threebody(
        name='B2OCD02PipPimPi0WSCombiner_{hash}',
        particles=[pions, pions, pi0],
        descriptor='D0 -> pi+ pi- pi0',
        **decay_arguments)
    dzero_to_kpkmpi0 = make_dzero_to_threebody(
        name='B2OCD02KpKmPi0WSCombiner_{hash}',
        particles=[kaons, kaons, pi0],
        descriptor='D0 -> K+ K- pi0',
        **decay_arguments)
    return ParticleContainersMerger([
        dzero_to_kmpippi0, dzero_to_kppimpi0, dzero_to_pippimpi0,
        dzero_to_kpkmpi0
    ],
                                    name=name)


##########################
# Three body WS D decays #
##########################


@configurable
def _make_dzero_to_kshh_ws(name,
                           particles,
                           descriptor,
                           am_min=1764.84 * MeV,
                           am_max=1964.84 * MeV,
                           **decay_arguments):
    return basic_builder.make_threebody(
        particles=particles,
        descriptor=descriptor,
        am_min=am_min,
        am_max=am_max,
        name=name,
        adoca12_max=None,
        adoca13_max=None,
        **decay_arguments)


@configurable
def make_dzero_to_kshh_ws(k_shorts,
                          name='B2OCD02KSHHWSCombiner_{hash}',
                          pi_pidk_max=5,
                          k_pidk_min=-5,
                          pi_p_min=2 * GeV,
                          pi_pt_min=250 * MeV,
                          k_p_min=2 * GeV,
                          k_pt_min=250 * MeV,
                          **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    dzero_to_kspippip_ws = _make_dzero_to_kshh_ws(
        name='B2OCD02KSPipPipWSCombiner_{hash}',
        particles=[k_shorts, pions, pions],
        descriptor='D0 -> KS0 pi+ pi+',
        **decay_arguments)
    dzero_to_kspimpim_ws = _make_dzero_to_kshh_ws(
        name='B2OCD02KSPimPimWSCombiner_{hash}',
        particles=[k_shorts, pions, pions],
        descriptor='D0 -> KS0 pi- pi-',
        **decay_arguments)
    dzero_to_kskppip_ws = _make_dzero_to_kshh_ws(
        name='B2OCD02KSKpPipWSCombiner_{hash}',
        particles=[k_shorts, kaons, pions],
        descriptor='D0 -> KS0 K+ pi+',
        **decay_arguments)
    dzero_to_kskmpim_ws = _make_dzero_to_kshh_ws(
        name='B2OCD02KSKmPimWSCombiner_{hash}',
        particles=[k_shorts, kaons, pions],
        descriptor='D0 -> KS0 K- pi-',
        **decay_arguments)
    dzero_to_kskpkp_ws = _make_dzero_to_kshh_ws(
        name='B2OCD02KSKpKpWSCombiner_{hash}',
        particles=[k_shorts, kaons, kaons],
        descriptor='D0 -> KS0 K+ K+',
        **decay_arguments)
    dzero_to_kskmkm_ws = _make_dzero_to_kshh_ws(
        name='B2OCD02KSKmKmWSCombiner_{hash}',
        particles=[k_shorts, kaons, kaons],
        descriptor='D0 -> KS0 K- K-',
        **decay_arguments)
    return ParticleContainersMerger([
        dzero_to_kspippip_ws, dzero_to_kspimpim_ws, dzero_to_kskppip_ws,
        dzero_to_kskmpim_ws, dzero_to_kskpkp_ws, dzero_to_kskmkm_ws
    ],
                                    name=name)


@configurable
def make_dzero_to_hhpi0_ws(pi0,
                           name='B2OCD02HHPi0WSCombiner_{hash}',
                           pi_pidk_max=5,
                           k_pidk_min=-5,
                           pi_p_min=2 * GeV,
                           pi_pt_min=250 * MeV,
                           k_p_min=2 * GeV,
                           k_pt_min=250 * MeV,
                           **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    dzero_to_kppippi0 = make_dzero_to_threebody(
        name='B2OCD02KpPipPi0WSCombiner_{hash}',
        particles=[kaons, pions, pi0],
        descriptor='D0 -> K+ pi+ pi0',
        **decay_arguments)
    dzero_to_kmpimpi0 = make_dzero_to_threebody(
        name='B2OCD02KmPimPi0WSCombiner_{hash}',
        particles=[kaons, pions, pi0],
        descriptor='D0 -> K- pi- pi0',
        **decay_arguments)
    dzero_to_pippippi0 = make_dzero_to_threebody(
        name='B2OCD02PipPipPi0WSCombiner_{hash}',
        particles=[pions, pions, pi0],
        descriptor='D0 -> pi+ pi+ pi0',
        **decay_arguments)
    dzero_to_pimpimpi0 = make_dzero_to_threebody(
        name='B2OCD02PimPimPi0WSCombiner_{hash}',
        particles=[pions, pions, pi0],
        descriptor='D0 -> pi- pi- pi0',
        **decay_arguments)
    dzero_to_kmkmpi0 = make_dzero_to_threebody(
        name='B2OCD02KmKmPi0WSCombiner_{hash}',
        particles=[kaons, kaons, pi0],
        descriptor='D0 -> K- K- pi0',
        **decay_arguments)
    dzero_to_kpkppi0 = make_dzero_to_threebody(
        name='B2OCD02KpKpPi0WSCombiner_{hash}',
        particles=[kaons, kaons, pi0],
        descriptor='D0 -> K+ K+ pi0',
        **decay_arguments)
    return ParticleContainersMerger([
        dzero_to_kppippi0, dzero_to_kmpimpi0, dzero_to_pippippi0,
        dzero_to_pimpimpi0, dzero_to_kmkmpi0, dzero_to_kpkppi0
    ],
                                    name=name)


######################
# Four body D decays #
######################


@configurable
def make_dzero_to_pippippimpim(am_min=1764.84 * MeV,
                               am_max=1964.84 * MeV,
                               pi_pidk_max=5,
                               pi_p_min=2 * GeV,
                               pi_pt_min=250 * MeV,
                               **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    return basic_builder.make_fourbody(
        particles=[pions, pions, pions, pions],
        descriptor='D0 -> pi+ pi+ pi- pi-',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD02PipPipPimPimCombiner_{hash}',
        **decay_arguments)


@configurable
def _make_dzero_to_kpipipi(name,
                           descriptor,
                           am_min=1764.84 * MeV,
                           am_max=1964.84 * MeV,
                           pi_pidk_max=5,
                           k_pidk_min=-5,
                           pi_p_min=2 * GeV,
                           pi_pt_min=250 * MeV,
                           k_p_min=2 * GeV,
                           k_pt_min=250 * MeV,
                           **decay_arguments):
    """ helper constructor to unify definition of kmpimpipip and kppippimpim lines """
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_fourbody(
        name=name,
        descriptor=descriptor,
        particles=[kaons, pions, pions, pions],
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_dzero_to_kmpimpippip(name='B2OCD02KmPimPipPipCombiner_{hash}',
                              descriptor='D0 -> K- pi- pi+ pi+',
                              **decay_arguments):
    return _make_dzero_to_kpipipi(
        name=name, descriptor=descriptor, **decay_arguments)


@configurable
def make_dzero_to_kppippimpim(name='B2OCD02KpPipPimPimCombiner_{hash}',
                              descriptor='D0 -> K+ pi+ pi- pi-',
                              **decay_arguments):
    return _make_dzero_to_kpipipi(
        name=name, descriptor=descriptor, **decay_arguments)


@configurable
def make_dzero_to_kpkmpippim(am_min=1764.84 * MeV,
                             am_max=1964.84 * MeV,
                             pi_pidk_max=5,
                             k_pidk_min=-5,
                             pi_p_min=2 * GeV,
                             pi_pt_min=250 * MeV,
                             k_p_min=2 * GeV,
                             k_pt_min=250 * MeV,
                             **decay_arguments):
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    return basic_builder.make_fourbody(
        particles=[kaons, kaons, pions, pions],
        descriptor='D0 -> K+ K- pi+ pi-',
        am_min=am_min,
        am_max=am_max,
        name='B2OCD02KpKmPipPimCombiner_{hash}',
        **decay_arguments)


#############################
# Kpi and Kpipipi D0 decays #
#############################


@configurable
def make_dzero_to_kpipipi(name='B2OCD02KPiPiPiCombiner_{hash}',
                          **decay_arguments):
    dzero_to_kmpimpippip = make_dzero_to_kmpimpippip(**decay_arguments)
    dzero_to_kppippimpim = make_dzero_to_kppippimpim(**decay_arguments)
    return ParticleContainersMerger(
        [dzero_to_kmpimpippip, dzero_to_kppippimpim], name=name)


@configurable
def make_tight_dzero_to_kpipipi(name='B2OCD02KPiPiPiTightCombiner_{hash}',
                                am_min=1805 * MeV,
                                am_max=1925 * MeV,
                                pi_pidk_max=5,
                                k_pidk_min=-5,
                                **decay_arguments):
    """ make D0->Kpi with tighter mass window, 60 MeV and tighter K, pi selections  """
    return make_dzero_to_kpipipi(
        name=name,
        am_min=am_min,
        am_max=am_max,
        pi_p_min=3 * GeV,
        pi_pt_min=300 * MeV,
        pi_pidk_max=pi_pidk_max,
        k_p_min=5 * GeV,
        k_pt_min=500 * MeV,
        k_pidk_min=k_pidk_min,
        **decay_arguments)


@configurable
def make_tight_dzero_to_kpipipi_for_xibc(**decay_arguments):
    """ make D0->Kpi with tighter mass window, 40 MeV and tighter K, pi selections and FD & vertex """
    return make_tight_dzero_to_kpipipi(
        name='B2OCD02KPiPiPiTightForXibcCombiner_{hash}',
        am_min=1825 * MeV,
        am_max=1905 * MeV,
        pi_pidk_max=2,
        k_pidk_min=-2,
        vchi2pdof_max=7,
        bpvvdchi2_min=49,
        **decay_arguments)


def make_dzero_to_kpi_or_kpipipi(name='B2OCD02KPiOrKPiPiPiCombiner_{hash}',
                                 **decay_arguments):
    dzero_to_kpi = make_dzero_to_kpi(**decay_arguments)
    dzero_to_kpipipi = make_dzero_to_kpipipi(**decay_arguments)
    return ParticleContainersMerger([dzero_to_kpi, dzero_to_kpipipi],
                                    name=name)


@configurable
def make_tight_dzero_to_kpi_or_kpipipi_for_xibc(
        name='B2OCD02KPiOrKPiPiPiTightForXibcCombiner_{hash}',
        **decay_arguments):
    dzero_to_kpi = make_tight_dzero_to_kpi_for_xibc(**decay_arguments)
    dzero_to_kpipipi = make_tight_dzero_to_kpipipi_for_xibc(**decay_arguments)
    return ParticleContainersMerger([dzero_to_kpi, dzero_to_kpipipi],
                                    name=name)


@configurable
def make_dzero_to_hhhh(name='B2OCD02HHHHCombiner_{hash}',
                       pi_p_min=2 * GeV,
                       pi_pt_min=250 * MeV,
                       pi_pidk_max=5,
                       k_p_min=2 * GeV,
                       k_pt_min=250 * MeV,
                       k_pidk_min=-5,
                       **decay_arguments):
    dzero_to_pippippimpim = make_dzero_to_pippippimpim(
        pi_pidk_max=pi_pidk_max,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        **decay_arguments)
    dzero_to_kppippimpim = make_dzero_to_kppippimpim(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    dzero_to_kmpimpippip = make_dzero_to_kmpimpippip(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    dzero_to_kpkmpippim = make_dzero_to_kpkmpippim(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments)
    return ParticleContainersMerger([
        dzero_to_pippippimpim, dzero_to_kppippimpim, dzero_to_kmpimpippip,
        dzero_to_kpkmpippim
    ],
                                    name=name)


@configurable
def make_tight_dzero_to_hhhh(pi_pidk_max=2, k_pidk_min=-2, **decay_arguments):
    """ tighter D0's for B->D0 hh """
    return make_dzero_to_hhhh(
        name='B2OCD02HHHHTightCombiner_{hash}',
        am_min=1805 * MeV,
        am_max=1925 * MeV,
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        **decay_arguments)


@configurable
def make_dzero_to_fourbody(name,
                           particles,
                           descriptor,
                           am_min=1764.84 * MeV,
                           am_max=1964.84 * MeV,
                           **decay_arguments):
    return basic_builder.make_fourbody(
        particles=particles,
        descriptor=descriptor,
        am_min=am_min,
        am_max=am_max,
        name=name,
        **decay_arguments)


@configurable
def make_dzero_to_kshhpi0(k_shorts,
                          pi0,
                          name='B2OCD02KSHHPi0Combiner_{hash}',
                          pi_pidk_max=5,
                          k_pidk_min=-5,
                          pi_p_min=2 * GeV,
                          pi_pt_min=250 * MeV,
                          k_p_min=2 * GeV,
                          k_pt_min=250 * MeV,
                          **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    dzero_to_kspippimpi0 = make_dzero_to_fourbody(
        name='B2OCD02KSPipPimPi0Combiner_{hash}',
        particles=[k_shorts, pions, pions, pi0],
        descriptor='D0 -> KS0 pi+ pi- pi0',
        **decay_arguments)
    dzero_to_kskppimpi0 = make_dzero_to_fourbody(
        name='B2OCD02KSKpPimPi0Combiner_{hash}',
        particles=[k_shorts, kaons, pions, pi0],
        descriptor='D0 -> KS0 K+ pi- pi0',
        **decay_arguments)
    dzero_to_kskmpippi0 = make_dzero_to_fourbody(
        name='B2OCD02KSKmPipPi0Combiner_{hash}',
        particles=[k_shorts, kaons, pions, pi0],
        descriptor='D0 -> KS0 K- pi+ pi0',
        **decay_arguments)
    dzero_to_kskpkmpi0 = make_dzero_to_fourbody(
        name='B2OCD02KSKpKmPi0Combiner_{hash}',
        particles=[k_shorts, kaons, kaons, pi0],
        descriptor='D0 -> KS0 K+ K- pi0',
        **decay_arguments)
    return ParticleContainersMerger([
        dzero_to_kspippimpi0, dzero_to_kskppimpi0, dzero_to_kskmpippi0,
        dzero_to_kskpkmpi0
    ],
                                    name=name)


#########################
# Four body WS D decays #
#########################


@configurable
def make_dzero_to_hhhh_ws(name='B2OCD02HHHHWSCombiner_{hash}',
                          pi_pidk_max=5,
                          k_pidk_min=-5,
                          pi_p_min=2 * GeV,
                          pi_pt_min=250 * MeV,
                          k_p_min=2 * GeV,
                          k_pt_min=250 * MeV,
                          **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    dzero_to_pippimpimpim_ws = make_dzero_to_fourbody(
        name='B2OCD02PipPimPimPimWSCombiner_{hash}',
        particles=[pions, pions, pions, pions],
        descriptor='D0 -> pi+ pi- pi- pi-',
        **decay_arguments)
    dzero_to_pimpippippip_ws = make_dzero_to_fourbody(
        name='B2OCD02PimPipPipPipWSCombiner_{hash}',
        particles=[pions, pions, pions, pions],
        descriptor='D0 -> pi- pi+ pi+ pi+',
        **decay_arguments)
    dzero_to_kppippippim_ws = make_dzero_to_fourbody(
        name='B2OCD02KpPipPipPimWSCombiner_{hash}',
        particles=[kaons, pions, pions, pions],
        descriptor='D0 -> K+ pi+ pi+ pi-',
        **decay_arguments)
    dzero_to_kmpimpimpip_ws = make_dzero_to_fourbody(
        name='B2OCD02KmPimPimPipWSCombiner_{hash}',
        particles=[kaons, pions, pions, pions],
        descriptor='D0 -> K- pi- pi- pi+',
        **decay_arguments)
    dzero_to_kpkmpippip_ws = make_dzero_to_fourbody(
        name='B2OCD02KpKmPipPipWSCombiner_{hash}',
        particles=[kaons, kaons, pions, pions],
        descriptor='D0 -> K+ K- pi+ pi+',
        **decay_arguments)
    dzero_to_kmkppimpim_ws = make_dzero_to_fourbody(
        name='B2OCD02KmKpPimPimWSCombiner_{hash}',
        particles=[kaons, kaons, pions, pions],
        descriptor='D0 -> K- K+ pi- pi-',
        **decay_arguments)
    return ParticleContainersMerger([
        dzero_to_pippimpimpim_ws, dzero_to_pimpippippip_ws,
        dzero_to_kppippippim_ws, dzero_to_kmpimpimpip_ws,
        dzero_to_kpkmpippip_ws, dzero_to_kmkppimpim_ws
    ],
                                    name=name)


@configurable
def make_tight_dzero_to_hhhh_ws(pi_pidk_max=2,
                                k_pidk_min=-2,
                                **decay_arguments):
    """ tighter D0's for B->D0 hh """
    return make_dzero_to_hhhh_ws(
        name='B2OCD02HHHHWSTightCombiner_{hash}',
        am_min=1805 * MeV,
        am_max=1925 * MeV,
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        **decay_arguments)


@configurable
def make_dzero_to_kshhpi0_ws(k_shorts,
                             pi0,
                             name='B2OCD02KSHHPi0WSCombiner_{hash}',
                             pi_pidk_max=5,
                             k_pidk_min=-5,
                             pi_p_min=2 * GeV,
                             pi_pt_min=250 * MeV,
                             k_p_min=2 * GeV,
                             k_pt_min=250 * MeV,
                             **decay_arguments):
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    dzero_to_kspippippi0_ws = make_dzero_to_fourbody(
        name='B2OCD02KSPipPipPi0WSCombiner_{hash}',
        particles=[k_shorts, pions, pions, pi0],
        descriptor='D0 -> KS0 pi+ pi+ pi0',
        **decay_arguments)
    dzero_to_kspimpimpi0_ws = make_dzero_to_fourbody(
        name='B2OCD02KSPimPimPi0WSCombiner_{hash}',
        particles=[k_shorts, pions, pions, pi0],
        descriptor='D0 -> KS0 pi- pi- pi0',
        **decay_arguments)
    dzero_to_kskppippi0_ws = make_dzero_to_fourbody(
        name='B2OCD02KSKpPipPi0WSCombiner_{hash}',
        particles=[k_shorts, kaons, pions, pi0],
        descriptor='D0 -> KS0 K+ pi+ pi0',
        **decay_arguments)
    dzero_to_kskmpimpi0_ws = make_dzero_to_fourbody(
        name='B2OCD02KSKmPimPi0WSCombiner_{hash}',
        particles=[k_shorts, kaons, pions, pi0],
        descriptor='D0 -> KS0 K- pi- pi0',
        **decay_arguments)
    dzero_to_kskpkppi0_ws = make_dzero_to_fourbody(
        name='B2OCD02KSKpKpPi0WSCombiner_{hash}',
        particles=[k_shorts, kaons, kaons, pi0],
        descriptor='D0 -> KS0 K+ K+ pi0',
        **decay_arguments)
    dzero_to_kskmkmpi0_ws = make_dzero_to_fourbody(
        name='B2OCD02KSKmKmPi0WSCombiner_{hash}',
        particles=[k_shorts, kaons, kaons, pi0],
        descriptor='D0 -> KS0 K- K- pi0',
        **decay_arguments)
    return ParticleContainersMerger([
        dzero_to_kspippippi0_ws, dzero_to_kspimpimpi0_ws,
        dzero_to_kskppippi0_ws, dzero_to_kskmpimpi0_ws, dzero_to_kskpkppi0_ws,
        dzero_to_kskmkmpi0_ws
    ],
                                    name=name)


####################
# Excited D decays #
####################

############################################
# Generic D to neutral (gamma/pi0) builder #
# LoKi-based functions, to be replaced     #
############################################


@configurable
def make_excitedd_to_dneutral(particles,
                              descriptors,
                              child_meson,
                              name="B2OCDNeutralCombiner_{hash}",
                              deltamass_max=250 * MeV,
                              deltamass_min=80 * MeV):

    combination_code = algorithms_loki.require_all("(AALL)")
    mother_code = algorithms_loki.require_all(
        "(M-MAXTREE(ABSID=='{d_meson}',M) < {deltamass_max})",
        "(M-MINTREE(ABSID=='{d_meson}',M) > {deltamass_min})").format(
            d_meson=child_meson,
            deltamass_max=deltamass_max,
            deltamass_min=deltamass_min)

    return algorithms_loki.ParticleCombiner(
        name=name,
        particles=particles,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=mother_code)


#############################################
# Specific D to neutral (gamma/pi0) builder #
#############################################


@configurable
def make_dsst_to_dsplusgamma(ds, **decay_arguments):

    particles = [ds, basic_builder.make_photons()]
    descriptors = ['[D*_s+ -> D_s+ gamma]cc']
    child_meson = 'D_s+'

    return make_excitedd_to_dneutral(
        particles, descriptors, child_meson, name="B2OCDsGammaCombiner_{hash}")


@configurable
def make_dzerost_to_dzerogamma(dzero, **decay_arguments):

    particles = [dzero, basic_builder.make_photons()]
    descriptors = ['D*(2007)0 -> D0 gamma']
    child_meson = 'D0'

    return make_excitedd_to_dneutral(
        particles, descriptors, child_meson, name="B2OCD0GammaCombiner_{hash}")


@configurable
def make_dzerost_to_dzeropi0(dzero, pi0, **decay_arguments):

    particles = [dzero, pi0]
    descriptors = ['D*(2007)0 -> D0 pi0']
    child_meson = 'D0'

    return make_excitedd_to_dneutral(
        particles, descriptors, child_meson, name="B2OCD0Pi0Combiner_{hash}")


##################################################
# Generic D to neutral (gamma/pi0) builder       #
# ThOr equivalent functions, not working for now #
##################################################

#@configurable
#def make_excitedd_to_dneutral(particles,
#                              descriptor,
#                              d_mass=_D0_M,
#                              name="B2OCDNeutralCombiner",
#                              deltamass_max=250 * MeV,
#                              deltamass_min=80 * MeV):
#
#    combination_code = require_all(F.ALL)
#    vertex_code = require_all(in_range(d_mass+deltamass_min, F.MASS, d_mass+deltamass_max))
#
#    return ParticleCombiner(
#        particles,
#        name=name,
#        DecayDescriptor=descriptor,
#        CombinationCut=combination_code,
#        CompositeCut=vertex_code)

##################################################
# Specific D to neutral (gamma/pi0) builder      #
# ThOr equivalent functions, not working for now #
##################################################

#@configurable
#def make_dsst_to_dsplusgamma(ds, **decay_arguments):
#
#    particles = [ds, basic_builder.make_photons()]
#    descriptor = '[D*_s+ -> D_s+ gamma]cc'
#
#    return make_excitedd_to_dneutral(
#        particles, descriptor, d_mass=_DS_M, name="B2OCDsst2DsGammaCombiner_{hash}")
#
#
#@configurable
#def make_dzerost_to_dzerogamma(dzero, **decay_arguments):
#
#    particles = [dzero, basic_builder.make_photons()]
#    descriptor = 'D*(2007)0 -> D0 gamma'
#
#    return make_excitedd_to_dneutral(
#        particles, descriptor, d_mass=_D0_M, name="B2OCDst02D0GammaCombiner_{hash}")
#
#
#@configurable
#def make_dzerost_to_dzeropi0(dzero, pi0, **decay_arguments):
#
#    particles = [dzero, pi0]
#    descriptor = 'D*(2007)0 -> D0 pi0'
#
#    return make_excitedd_to_dneutral(
#        particles, descriptor, d_mass=_D0_M, name="B2OCDst02D0Pi0Combiner_{hash}")

##############################
# Charged excited D  builder #
##############################


@configurable
def make_dstar_to_dzeropi(dzero,
                          name='B2OCDst2D0PiCombiner_{hash}',
                          make_pvs=make_pvs,
                          adoca12_max=0.5 * mm,
                          mass_window=600 * MeV,
                          deltamass_max=200 * MeV,
                          deltamass_min=90 * MeV,
                          vchi2pdof_max=10,
                          bpvvdchi2_min=36,
                          bpvdira_min=0,
                          soft_pi_pidk_max=5.,
                          soft_pi_p_min=2 * GeV,
                          soft_pi_pt_min=250 * MeV,
                          soft_pi_mipchi2_min=4):
    """
    Return D*+ maker for tagging a D0 with a soft pion.

    Parameters
    ----------
    make_pvs : callable
        Primary vertex maker function.
        Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        in_range(_DST2010_M - mass_window, F.MASS, _DST2010_M + mass_window),
        F.DOCA(1, 2) < adoca12_max)
    pvs = make_pvs()
    vertex_code = require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        in_range(_D0_M + deltamass_min, F.MASS, _D0_M + deltamass_max))

    soft_pions = basic_builder.make_pions(
        pi_pidk_max=soft_pi_pidk_max,
        p_min=soft_pi_p_min,
        pt_min=soft_pi_pt_min,
        mipchi2_min=soft_pi_mipchi2_min)

    dstp_to_dzeropip = ParticleCombiner([dzero, soft_pions],
                                        name='B2OCDstp2D0PipCombiner_{hash}',
                                        DecayDescriptor='D*(2010)+ -> D0 pi+',
                                        CombinationCut=combination_code,
                                        CompositeCut=vertex_code)
    dstm_to_dzeropim = ParticleCombiner([dzero, soft_pions],
                                        name='B2OCDstm2D0PimCombiner_{hash}',
                                        DecayDescriptor='D*(2010)- -> D0 pi-',
                                        CombinationCut=combination_code,
                                        CompositeCut=vertex_code)

    return ParticleContainersMerger([dstp_to_dzeropip, dstm_to_dzeropim],
                                    name=name)


#############################################
# Charged excited D  builder (CF mode only) #
#############################################


@configurable
def make_dstar_to_dzeropi_cf(name='B2OCDst2D0PiCFCombiner_{hash}',
                             make_pvs=make_pvs,
                             adoca12_max=0.5 * mm,
                             mass_window=600 * MeV,
                             deltamass_max=200 * MeV,
                             deltamass_min=90 * MeV,
                             vchi2pdof_max=10,
                             bpvvdchi2_min=36,
                             bpvdira_min=0,
                             soft_pi_pidk_max=20,
                             soft_pi_p_min=1 * GeV,
                             soft_pi_pt_min=100 * MeV,
                             **decay_arguments):
    """
    Return D*+ maker for tagging a D0 with a soft pion. Only Cabibbo favor mode provide.

    Parameters
    ----------
    make_pvs : callable
        Primary vertex maker function.
        Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        in_range(_DST2010_M - mass_window, F.MASS, _DST2010_M + mass_window),
        F.DOCA(1, 2) < adoca12_max)
    pvs = make_pvs()
    vertex_code = require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        in_range(_D0_M + deltamass_min, F.MASS, _D0_M + deltamass_max))

    # D0->K-pi+
    dzero_to_kmpip = make_dzero_to_kmpip(**decay_arguments)
    # D~0->K+pi-
    dzero_to_kppim = make_dzero_to_kppim(**decay_arguments)
    # D0->K-pi-pi+pi+
    dzero_to_kmpimpippip = make_dzero_to_kmpimpippip(**decay_arguments)
    dzero = ParticleContainersMerger([dzero_to_kmpip, dzero_to_kmpimpippip],
                                     name="B2OCD02KPiOrK3PiCFCombiner_{hash}")
    # D~0->K+pi+pi-pi-
    dzero_to_kppippimpim = make_dzero_to_kppippimpim(**decay_arguments)
    dzerobar = ParticleContainersMerger(
        [dzero_to_kppim, dzero_to_kppippimpim],
        name="B2OCD0bar2KPiOrK3PiCFCombiner_{hash}")

    # Accompanying pion
    soft_pions = basic_builder.make_pions(
        pi_pidk_max=soft_pi_pidk_max,
        p_min=soft_pi_p_min,
        pt_min=soft_pi_pt_min)

    dstp_to_dzeropip = ParticleCombiner([dzero, soft_pions],
                                        name='B2OCDstpD0PipCFCombiner_{hash}',
                                        DecayDescriptor='D*(2010)+ -> D0 pi+',
                                        CombinationCut=combination_code,
                                        CompositeCut=vertex_code)
    dstm_to_dzeropim = ParticleCombiner([dzerobar, soft_pions],
                                        name='B2OCDstmD0PimCFCombiner_{hash}',
                                        DecayDescriptor='D*(2010)- -> D0 pi-',
                                        CombinationCut=combination_code,
                                        CompositeCut=vertex_code)

    return ParticleContainersMerger([dstp_to_dzeropip, dstm_to_dzeropim],
                                    name=name)


#################################
# Neutral excited D2460 builder #
#################################


@configurable
def make_dst2460_to_dpluspi(dplus,
                            name='B2OCDst24602D0PiCombiner_{hash}',
                            make_pvs=make_pvs,
                            adoca12_max=0.5 * mm,
                            mass_window=600 * MeV,
                            deltamass_max=200 * MeV,
                            deltamass_min=90 * MeV,
                            vchi2pdof_max=10,
                            bpvvdchi2_min=36,
                            bpvdira_min=0):
    combination_code = require_all(
        in_range(_DST2460_M - mass_window, F.MASS, _DST2460_M + mass_window),
        F.DOCA(1, 2) < adoca12_max)
    pvs = make_pvs()
    vertex_code = require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        in_range(_D0_M + deltamass_min, F.MASS, _D0_M + deltamass_max))

    soft_pions = basic_builder.make_pions()

    return ParticleCombiner([dplus, soft_pions],
                            name=name,
                            DecayDescriptor='[D*_2(2460)0 -> D+ pi-]cc',
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)
