###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* B-meson BDT filters:
* _ b_bdt_filter: could be applied to ANY line
* _ b_to_dh_bdt_filter: to be applied to Dh lines (D0/D+/Ds+/... + pi/K/...)
* _ BDT .xml files are in https://gitlab.cern.ch/lhcb-datapkg/ParamFiles/data
"""
from __future__ import absolute_import, division, print_function

import Functors as F
import Functors.math as fmath

from Hlt2Conf.algorithms_thor import ParticleFilter
from Functors import require_all


def b_sigmanet_filter(particles, pvs, MVACut=0.):

    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Hlt2B2OC_B_SigmaNet_Run3.json",
            'Name':
            "B2OC_SigmaNet_Generic",
            'Lambda':
            "2.0",
            'NLayers':
            "3",
            'InputSize':
            "6",
            'Monotone_Constraints':
            '[1,-1,-1,-1,-1,-1]',
            'Variables':
            'log_B_PT,B_ETA,log_B_DIRA,log_B_ENDVERTEX_CHI2,log_B_IPCHI2_OWNPV,log_B_IP_OWNPV',
        },
        Inputs={
            "log_B_PT": fmath.log(F.PT),
            "B_ETA": F.ETA,
            "log_B_DIRA": fmath.log(1. - F.BPVDIRA(pvs)),
            "log_B_ENDVERTEX_CHI2": fmath.log(F.CHI2DOF),
            "log_B_IPCHI2_OWNPV": fmath.log(F.BPVIPCHI2(pvs)),
            "log_B_IP_OWNPV": fmath.log(F.BPVIP(pvs)),
        }) > MVACut)

    code = require_all(mva)

    return ParticleFilter(particles, F.FILTER(code))
