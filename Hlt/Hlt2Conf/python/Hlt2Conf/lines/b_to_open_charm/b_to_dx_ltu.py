###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDX LTUnbiased lines
"""
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from GaudiKernel.SystemOfUnits import MeV, GeV

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder


@check_process
def make_BdToDsmK_DsmToKpKmPim_LTU(process):
    kaon = basic_builder.make_tight_kaons(k_pidk_min=0)
    d = d_builder.make_dsplus_to_kpkmpip()
    line_alg = b_builder.make_lifetime_unbiased_b2x(
        particles=[d, kaon],
        descriptors=['[B0 -> D_s- K+]cc'],
        am_min=5000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=7000 * MeV,
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BdToDsmKPiPi_DsmToKmKpPim_LTU(process):
    kaon = basic_builder.make_tight_kaons()
    pion = basic_builder.make_pions()
    d = d_builder.make_dsplus_to_kpkmpip()
    line_alg = b_builder.make_lifetime_unbiased_b2x(
        particles=[d, kaon, pion, pion],
        descriptors=['[B0 -> D_s- K+ pi+ pi-]cc'])
    return line_alg
