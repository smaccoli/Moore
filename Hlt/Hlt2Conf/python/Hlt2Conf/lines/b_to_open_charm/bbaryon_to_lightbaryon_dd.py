###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from GaudiKernel.SystemOfUnits import GeV

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder

#############################################################################
# Form the Xi_bc+ -> D0 D0 p,  D0 --> K- pi+
# NB: Here, D0 == Kpi & K3pi
##############################################################################


@check_process
def make_XibcpToPD0D0_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        protons = basic_builder.make_protons()
    elif process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        protons = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
    line_alg = b_builder.make_xibc2ccx(
        particles=[Dz, Dz, protons],
        descriptors=['Xi_bc+ -> D0 D0 p+', 'Xi_bc~- -> D0 D0 p~-'],
    )
    return line_alg


########################
# Add some B2DDH lines #
# Author: Ruiting Ma   #
########################


# Lambda_b0 -> D0 D_s- p+
@check_process
def make_LbToD0DsmP_D0ToKPiOrKPiPiPi_DsmToHHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=20, k_pidk_min=-10)
        dsplus = d_builder.make_dsplus_to_hhh(pi_pidk_max=20, k_pidk_min=-10)
        proton = basic_builder.make_tight_protons(p_pidp_min=-10)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kpi_or_kpipipi()
        dsplus = d_builder.make_dsplus_to_hhh()
        proton = basic_builder.make_tight_protons(p_pidp_min=0)
    line_alg = b_builder.make_lb(
        particles=[dzero, dsplus, proton],
        descriptors=['Lambda_b0 -> D0 D_s- p+', 'Lambda_b0 -> D0 D_s+ p~-'],
        sum_pt_min=6 * GeV)
    return line_alg


##########################
# DDLambda lines
##########################


@check_process
def make_LbToDpDmLambdaLL_DpToHHH(process):
    if process == 'spruce':
        d = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        d = d_builder.make_dplus_to_hhh()
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d, d, hyperon],
        descriptors=['[Lambda_b0 -> D+ D- Lambda0]cc'])
    return line_alg


@check_process
def make_LbToDpDmLambdaDD_DpToHHH(process):
    if process == 'spruce':
        d = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        d = d_builder.make_dplus_to_hhh()
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d, d, hyperon],
        descriptors=['[Lambda_b0 -> D+ D- Lambda0]cc'])
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        d = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        d = d_builder.make_dzero_to_kpi_or_kpipipi()
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d, d, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ])
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        d = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        d = d_builder.make_dzero_to_kpi_or_kpipipi()
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d, d, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ])
    return line_alg


@check_process
def make_LbToDstDLambdaLL_DstToD0Pi_D0ToHH_DToHHH(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dplus = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_hh()
        dplus = d_builder.make_dplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dz)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[dst, dplus, hyperon],
        descriptors=[
            '[Lambda_b0 -> D*(2010)- D+ Lambda0]cc',
            '[Lambda_b0 -> D*(2010)+ D- Lambda0]cc'
        ])
    return line_alg


@check_process
def make_LbToDstDLambdaDD_DstToD0Pi_D0ToHH_DToHHH(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dplus = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_hh()
        dplus = d_builder.make_dplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dz)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[dst, dplus, hyperon],
        descriptors=[
            '[Lambda_b0 -> D*(2010)- D+ Lambda0]cc',
            '[Lambda_b0 -> D*(2010)+ D- Lambda0]cc'
        ])
    return line_alg


@check_process
def make_LbToDstDLambdaLL_DstToD0Pi_D0ToKPiPiPi_DToHHH(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpipipi(pi_pidk_max=None, k_pidk_min=None)
        dplus = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpipipi()
        dplus = d_builder.make_dplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dz)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[dst, dplus, hyperon],
        descriptors=[
            '[Lambda_b0 -> D*(2010)- D+ Lambda0]cc',
            '[Lambda_b0 -> D*(2010)+ D- Lambda0]cc'
        ])
    return line_alg


@check_process
def make_LbToDstDLambdaDD_DstToD0Pi_D0ToKPiPiPi_DToHHH(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpipipi(pi_pidk_max=None, k_pidk_min=None)
        dplus = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpipipi()
        dplus = d_builder.make_dplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dz)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[dst, dplus, hyperon],
        descriptors=[
            '[Lambda_b0 -> D*(2010)- D+ Lambda0]cc',
            '[Lambda_b0 -> D*(2010)+ D- Lambda0]cc'
        ])
    return line_alg
