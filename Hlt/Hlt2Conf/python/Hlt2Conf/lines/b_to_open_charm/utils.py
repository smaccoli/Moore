###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Utilities used in B2OC selection lines

check_process: make sure the `process` argument is 'hlt2' or 'spruce'
update_makers: read and store line makers in dictionaries

"""
from inspect import getmembers, isfunction

from PyConf.utilities import ConfigurationError


def check_process(func):
    def wrapper(*args, process, **kwargs):
        if process not in ['hlt2', 'spruce']:
            raise ConfigurationError(
                '`process` should be either "hlt2" or "spruce".')
        return func(*args, process=process, **kwargs)

    return wrapper


def update_makers(line_makers, line_module):
    '''Store line makers in a dictionary.

    Makers defined in independent modules (e.g. b_to_dh)
    is read by this function and updated as an item to a dictionary.
    The name of makers (e.g. make_BuToD0K_D0ToHH) is saved as the key,
    and the function itself is the value of item.

    This is achieved by `getmembers` from `inspect` package.

    Args:
        line_makers: Dictionary to save makers.
        line_module: From which module to update the makers.
    '''
    line_makers.update({
        k: v
        for k, v in dict(getmembers(line_module, isfunction)).items()
        if k.startswith('make')
    })
