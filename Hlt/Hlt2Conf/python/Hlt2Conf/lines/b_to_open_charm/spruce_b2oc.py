###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of B2OC sprucing lines, notice PROCESS = 'spruce'

Usage:
add line names in corresponding list/dict,
or register lines separately at the very end.

Non trivial imports:
prefilters and line_alg ("bare" line builders) like b_to_dh.make_BdToDsmK_DsmToHHH

Output:
updated dictionary of sprucing_lines

To be noted:
"bare" line builders, like like b_to_dh.make_BdToDsmK_DsmToHHH, have PROCESS as
argument to allow ad hoc settings

"""
from Moore.config import SpruceLine, register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import MeV

from PyConf.Algorithms import GetFlavourTaggingParticles
from PyConf.Algorithms import Monitor__ParticleRange

import Functors as F

from Hlt2Conf.standard_particles import make_has_rich_long_pions

from Hlt2Conf.lines.b_to_open_charm.utils import update_makers

from Hlt2Conf.lines.b_to_open_charm import prefilters

from Hlt2Conf.lines.b_to_open_charm import b_to_dh
from Hlt2Conf.lines.b_to_open_charm import b_to_dx_ltu
from Hlt2Conf.lines.b_to_open_charm import b_to_dll
from Hlt2Conf.lines.b_to_open_charm import b_to_dhh
from Hlt2Conf.lines.b_to_open_charm import b_to_dmunu
from Hlt2Conf.lines.b_to_open_charm import b_to_dhhh
from Hlt2Conf.lines.b_to_open_charm import b_to_dd
from Hlt2Conf.lines.b_to_open_charm import b_to_ddh_standalone
from Hlt2Conf.lines.b_to_open_charm import b_to_ddhh
from Hlt2Conf.lines.b_to_open_charm import b_to_cbaryon_h
from Hlt2Conf.lines.b_to_open_charm import b_to_cbaryon_hh
from Hlt2Conf.lines.b_to_open_charm import b_to_cbaryons_h
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_h
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_hh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_hhh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_d
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_dh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_dhh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_lightbaryon_d
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_lightbaryon_dh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_lightbaryon_dd
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_lightbaryon_ddh

PROCESS = 'spruce'
sprucing_lines = {}

##############################################
# Read and store all line makers in one dict #
##############################################

line_makers = {}
update_makers(line_makers, b_to_dh)
update_makers(line_makers, b_to_dx_ltu)
update_makers(line_makers, b_to_dll)
update_makers(line_makers, b_to_dhh)
update_makers(line_makers, b_to_dmunu)
update_makers(line_makers, b_to_dhhh)
update_makers(line_makers, b_to_dd)
update_makers(line_makers, b_to_ddh_standalone)
update_makers(line_makers, b_to_ddhh)
update_makers(line_makers, b_to_cbaryons_h)
update_makers(line_makers, b_to_cbaryon_h)
update_makers(line_makers, b_to_cbaryon_hh)
update_makers(line_makers, bbaryon_to_cbaryon_h)
update_makers(line_makers, bbaryon_to_cbaryon_hh)
update_makers(line_makers, bbaryon_to_cbaryon_hhh)
update_makers(line_makers, bbaryon_to_cbaryon_d)
update_makers(line_makers, bbaryon_to_cbaryon_dh)
update_makers(line_makers, bbaryon_to_cbaryon_dhh)
update_makers(line_makers, bbaryon_to_lightbaryon_d)
update_makers(line_makers, bbaryon_to_lightbaryon_dh)
update_makers(line_makers, bbaryon_to_lightbaryon_dd)
update_makers(line_makers, bbaryon_to_lightbaryon_ddh)

############################################
# Define functions for line booking        #
# Make it possible to register lines       #
# outside this file (e.g. in test scripts) #
############################################


# default lines
def make_default_sprucing_lines(line_dict=sprucing_lines,
                                line_makers=line_makers,
                                default_lines=None):
    if not default_lines: return
    for decay in default_lines:

        @register_line_builder(line_dict)
        def make_sprucing_line(name='SpruceB2OC_%s' % decay,
                               maker_name='make_%s' % decay,
                               prescale=1):
            line_alg = line_makers[maker_name](process=PROCESS)
            return SpruceLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg])


# lines need non-default prescale value
def make_prescaled_sprucing_lines(line_dict=sprucing_lines,
                                  line_makers=line_makers,
                                  prescaled_lines=None):
    if not prescaled_lines: return
    for decay, prescale in sorted(prescaled_lines.items()):

        @register_line_builder(line_dict)
        def make_sprucing_line(name='SpruceB2OC_%s' % decay,
                               maker_name='make_%s' % decay,
                               prescale=prescale):
            line_alg = line_makers[maker_name](process=PROCESS)
            return SpruceLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg])


# lines need flavor tagging
def make_flavor_tagging_sprucing_lines(line_dict=sprucing_lines,
                                       line_makers=line_makers,
                                       flavor_tagging_lines=None):
    if not flavor_tagging_lines: return
    for decay in flavor_tagging_lines:

        @register_line_builder(line_dict)
        def make_sprucing_line(name='SpruceB2OC_%s' % decay,
                               maker_name='make_%s' % decay,
                               prescale=1):
            line_alg = line_makers[maker_name](process=PROCESS)
            pvs = make_pvs()
            longPions = make_has_rich_long_pions()
            longTaggingParticles = GetFlavourTaggingParticles(
                BCandidates=line_alg,
                TaggingParticles=longPions,
                PrimaryVertices=pvs)

            return SpruceLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg],
                extra_outputs=[('LongTaggingParticles', longTaggingParticles)])


# lines need isolation info, currently the same as default
def make_isolation_sprucing_lines(line_dict=sprucing_lines,
                                  line_makers=line_makers,
                                  isolation_lines=None):
    if not isolation_lines: return
    for decay in isolation_lines:

        @register_line_builder(line_dict)
        def make_sprucing_line(name='SpruceB2OC_%s' % decay,
                               maker_name='make_%s' % decay,
                               prescale=1):
            line_alg = line_makers[maker_name](process=PROCESS)
            return SpruceLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg])


# lines need a MVA post-filter
def make_mva_filtered_sprucing_lines(line_dict=sprucing_lines,
                                     line_makers=line_makers,
                                     mva_filtered_lines=None):
    if not mva_filtered_lines: return
    for decay, MVACut in sorted(mva_filtered_lines.items()):

        @register_line_builder(line_dict)
        def make_sprucing_line(name='SpruceB2OC_%s' % decay,
                               MVACut=MVACut,
                               maker_name='make_%s' % decay,
                               prescale=1):
            line_alg = line_makers[maker_name](process=PROCESS, MVACut=MVACut)
            return SpruceLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg])


#############################################
# Lists and dicts of lines to register      #
# Authors should add decays here            #
# NO `SpruceB2OC_` prefix or `` suffix #
#############################################

# default lines without any further configuration
default_lines = [
    # lines from b_to_dh

    # commented out due to Rec#208
    #'BdToDsstmK_DsstmToDsmGamma_DsmToHHH',

    # commented out due to Rec#208
    #'BuToD0Pi_D0ToKsLLPi0Resolved',
    #'BuToD0Pi_D0ToKsDDPi0Resolved',
    #'BuToD0Pi_D0ToKsLLPi0Merged',
    #'BuToD0Pi_D0ToKsDDPi0Merged',
    #'BuToD0K_D0ToKsLLPi0Resolved',
    #'BuToD0K_D0ToKsDDPi0Resolved',
    #'BuToD0K_D0ToKsLLPi0Merged',
    #'BuToD0K_D0ToKsDDPi0Merged',
    'BuToD0Pi_PartialD0ToKsLLHH',
    #'BuToD0Pi_PartialD0ToKsDDHH',
    'BuToD0K_PartialD0ToKsLLHH',
    #'BuToD0K_PartialD0ToKsDDHH',

    # commented out due to Rec#208
    #'BuToD0Pi_D0ToKsLLHHPi0Resolved',
    #'BuToD0Pi_D0ToKsDDHHPi0Resolved',
    #'BuToD0Pi_D0ToKsLLHHPi0Merged',
    #'BuToD0Pi_D0ToKsDDHHPi0Merged',
    #'BuToD0K_D0ToKsLLHHPi0Resolved',
    #'BuToD0K_D0ToKsDDHHPi0Resolved',
    #'BuToD0K_D0ToKsLLHHPi0Merged',
    #'BuToD0K_D0ToKsDDHHPi0Merged',

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHH',

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHH',

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHHHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHHH',
    'BcToD0Pi_D0ToHH',
    'BcToD0K_D0ToHH',
    'BcToD0Pi_D0ToKsLLHH',
    #'BcToD0Pi_D0ToKsDDHH',
    'BcToD0K_D0ToKsLLHH',
    #'BcToD0K_D0ToKsDDHH',
    'BcToD0Pi_D0ToHHHH',
    'BcToD0K_D0ToHHHH',
    'BcToDmKst0_DmToHHH',
    'BcToDsmKst0_DsmToHHH',
    'BcToDmPhi_DmToHHH',
    'BcToDsmPhi_DsmToHHH',
    'BcToDmKsLL_DmToHHH',
    #'BcToDmKsDD_DmToHHH',
    'BcToDsmKsLL_DsmToHHH',
    #'BcToDsmKsDD_DsmToHHH',
    'BcToDmF0_DmToHHH',
    'BcToDsmF0_DsmToHHH',

    # lines from b_to_dll
    'BcToDsmMupMum_DsmToHHH',
    'BcToDspMumMum_DspToHHH',

    # lines from b_to_dhh
    'BdToD0KPi_D0ToHH',
    'BdToD0PiPiWS_D0ToHH',
    'BdToD0KPiWS_D0ToHH',
    'BdToD0KKWS_D0ToHH',
    'BdToD0KPi_D0ToKsLLHH',
    #'BdToD0KPi_D0ToKsDDHH',
    'BdToD0PiPiWS_D0ToKsLLHH',
    #'BdToD0PiPiWS_D0ToKsDDHH',
    'BdToD0KPiWS_D0ToKsLLHH',
    #'BdToD0KPiWS_D0ToKsDDHH',
    'BdToD0KKWS_D0ToKsLLHH',
    #'BdToD0KKWS_D0ToKsDDHH',
    'BdToD0KPi_D0ToHHHH',
    'BdToD0PiPiWS_D0ToHHHH',
    'BdToD0KPiWS_D0ToHHHH',
    'BdToD0KKWS_D0ToHHHH',
    'BdToD0PbarP_D0ToHH',
    'BdToD0PbarPWS_D0ToHH',
    'BdToDstpKsLLPi_DstpToD0Pi_D0ToHH',
    #'BdToDstpKsDDPi_DstpToD0Pi_D0ToHH',
    'BdToDstpKsLLK_DstpToD0Pi_D0ToHH',
    #'BdToDstpKsDDK_DstpToD0Pi_D0ToHH',
    'BdToDstpKsLLPi_DstpToD0Pi_D0ToHHHH',
    #'BdToDstpKsDDPi_DstpToD0Pi_D0ToHHHH',
    'BdToDstpKsLLK_DstpToD0Pi_D0ToHHHH',
    #'BdToDstpKsDDK_DstpToD0Pi_D0ToHHHH',

    # commented out due to Rec#208
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHH',

    # commented out due to Rec#208
    #'BdToDsstmKsLLPi_DsstmToDsmGamma_DsmToHHH',
    #'BdToDsstmKsDDPi_DsstmToDsmGamma_DsmToHHH',
    'BuToDmPiPi_DmToHHH',
    'BuToDmKPi_DmToHHH',
    'BuToDmKK_DmToHHH',
    'BuToD0KsLLPi_D0ToHH',
    #'BuToD0KsDDPi_D0ToHH',
    'BuToD0KsLLPi_D0ToKsLLHH',
    #'BuToD0KsDDPi_D0ToKsLLHH',
    #'BuToD0KsLLPi_D0ToKsDDHH',
    #'BuToD0KsDDPi_D0ToKsDDHH',
    'BuToD0KsLLPi_D0ToHHHH',
    #'BuToD0KsDDPi_D0ToHHHH',
    'BuToDspPiPi_DspToHHH',
    'BuToDspPbarP_DspToHHH',
    'BuToDspKPi_DspToHHH',
    'BuToDspKK_DspToHHH',
    'BuToDsmKPi_DsmToHHH',
    'BuToDpPbarP_DpToHHH',
    'BuToDpKPi_DpToHHH',
    'BuToDpKK_DpToHHH',
    'BdToDmKsLLPi_DmToHHH',

    # lines from b_to_dmunu
    #'BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHHPi0Resolved',
    #'BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHHPi0Resolved',
    #'BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHHPi0Merged',
    #'BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHHPi0Merged',

    # lines from b_to_dhhh
    'BdToDstpPiPiPi_DstpToD0Pi_D0ToHH',
    'BdToDstpKPiPi_DstpToD0Pi_D0ToHH',
    'BdToDstpKKPi_DstpToD0Pi_D0ToHH',
    'BdToDstpKKK_DstpToD0Pi_D0ToHH',
    'BdToDstpPiPiPi_DstpToD0Pi_D0ToHHHH',
    'BdToDstpKPiPi_DstpToD0Pi_D0ToHHHH',
    'BdToDstpKKPi_DstpToD0Pi_D0ToHHHH',
    'BdToDstpKKK_DstpToD0Pi_D0ToHHHH',
    'BdToDstpPbarPPi_DstpToD0Pi_D0ToHH',
    'BdToDstpPbarPK_DstpToD0Pi_D0ToHH',
    'BdToDstpPbarPPi_DstpToD0Pi_D0ToHHHH',
    'BdToDstpPbarPK_DstpToD0Pi_D0ToHHHH',
    'BuToD0PiPiPi_D0ToHHHH',
    'BuToD0KPiPi_D0ToHHHH',
    'BuToD0PiPiPi_D0ToKsLLHH',
    #'BuToD0PiPiPi_D0ToKsDDHH',
    'BuToD0KPiPi_D0ToKsLLHH',
    #'BuToD0KPiPi_D0ToKsDDHH',
    'BdToDsmPbarPPi_DsmToKmKpPim',
    'BdToDmPbarPPi_DmToPimPimKp',
    'BdToDmKKPi_DmToPimPimKp',

    # lines from b_to_ddh_standalone
    'BdToD0D0Phi_D0ToKPiOrKPiPiPi',
    'BdToDpDmPhi_DpToHHH',
    'BdToDsD0Pi_DsToHHH_D0ToKPiOrKPiPiPi',
    'BdToDsDPhi_DsToHHH_DToHHH',
    'BdToDspDsmPhi_DspToHHH',
    'BdToDstDsPhi_DstToD0Pi_D0ToKPiOrKPiPiPi_DsToHHH',
    'BdToDstpDstmPhi_DstpToD0Pi_D0ToKPiOrKPiPiPi',
    'BuToDsD0Phi_DsToHHH_D0ToKPiOrKPiPiPi',
    'BuToDspDmPi_DspToHHH_DmToHHH',
    'BuToDstmDspPi_DstmToD0Pi_D0ToKPiOrKPiPiPi_DspToHHH',
    'BuToDspDsmPi_DsToHHH',
    'BuToDspDsmK_DspToHHH',
    'BuToDspD0Rho0_DspToHHH_D0ToKPiOrKPiPiPi',
    'BuToD0DpKst_D0ToKPiOrKPiPiPi_DpToHHH',
    'BuToD0D0Pi_D0ToKPiOrKPiPiPi',
    'BuToD0D0K_D0ToKPiOrKPiPiPi',
    'BdToDspDsmKst_DspToHHH',
    'BdToDspDmRho0_DspToHHH_DmToHHH',
    'BdToD0DPi_D0ToKPiOrKPiPiPi_DToHHH',
    'BdToD0DK_D0ToKPiOrKPiPiPi_DToHHH',

    # lines from b_to_cbaryon_hh
    'BuToLcmPPi_LcmToPKPi',
    'BuToLcmPK_LcmToPKPi',
    'BdToOmc0PPi_Omc0ToPKKPi',
    'BdToOmc0PK_Omc0ToPKKPi',

    # lines from bbaryon_to_cbaryon_h
    #'Xibc0ToXicpPi_XicpToPKPi',
    #'Ombc0ToXicpK_XicpToPKPi',

    # lines from bbaryon_to_cbaryon_hh
    'XibmToLcpPiPi_LcpToPKPi',
    'XibmToLcpKPi_LcpToPKPi',
    'XibmToLcpKK_LcpToPKPi',
    'XibmToXicpPiPi_XicpToPKPi',
    'XibmToXicpKPi_XicpToPKPi',
    'XibmToXicpKK_XicpToPKPi',
    #'XibcpToXicpPiPi_XicpToPKPi',
    #'XibcpToLcpPiPi_LcpToPKPi',
    #'XibcpToLcpKmPip_LcpToPKPi',
    'Xib0ToXicpPiPiPi_XicpToPKPi',
    'LbToLcpKsLLK_LcpToPKPi',
    #'LbToLcpKsDDK_LcpToPKPi',

    # lines from bbaryon_to_cbaryon_hhh
    'Xib0ToXicpPbarPPi_XicpToPKPi',
    'Xib0ToXicpKPiPi_XicpToPKPi',
    'Xib0ToXicpKKPi_XicpToPKPi',
    'LbToLcpPiPiPi_LcpToPPiPi',
    'LbToLcpPiPiPi_LcpToPKPi',
    'LbToLcpPbarPPi_LcpToPKPi',
    'LbToLcpKKPi_LcpToPKPi',

    # lines from bbaryon_to_cbaryon_dh
    #'Xibc0ToLcpD0Pi_LcpToPKPi_D0ToKPi',
    'Xib0ToXicpD0K_XicpToPKPi_D0ToKPi',
    'Xib0ToXicpD0K_XicpToPKPi_D0ToKPiPiPi',
    #'Ombc0ToLcpD0K_LcpToPKPi_D0ToKPi',

    # lines from bbaryon_to_lightbaryon_d
    #'XibcpToPD0_D0ToKPi',
    'LbToDsmP_DsmToHHH',

    # lines from bbaryon_to_lightbaryon_dh
    'XibmToDmPK_DmToHHH',
    'XibmToDmPPi_DmToHHH',
    'XibmToDsmPK_DsmToHHH',
    'XibmToDsmPPi_DsmToHHH',
    #'XibcpToPDpK_DpToKmPipPip',
    #'Xibc0ToPD0K_D0ToKPi',
    'LbToD0PPi_D0ToHH',
    'LbToD0PK_D0ToHH',

    # lines from bbaryon_to_lightbaryon_dd
    'LbToDpDmLambdaLL_DpToHHH',
    #'LbToDpDmLambdaDD_DpToHHH',
    'LbToD0D0LambdaLL_D0ToKPiOrKPiPiPi',
    #'LbToD0D0LambdaDD_D0ToKPiOrKPiPiPi',
    'LbToDstDLambdaLL_DstToD0Pi_D0ToHH_DToHHH',
    #'LbToDstDLambdaDD_DstToD0Pi_D0ToHH_DToHHH',
    'LbToDstDLambdaLL_DstToD0Pi_D0ToKPiPiPi_DToHHH',
    #'LbToDstDLambdaDD_DstToD0Pi_D0ToKPiPiPi_DToHHH',
    #'XibcpToPD0D0_D0ToKPi',
    'LbToD0DsmP_D0ToKPiOrKPiPiPi_DsmToHHH',
]

# lines need non-default prescale value
prescaled_lines = {
    # lines from b_to_dh
    'BuToD0Pi_PartialD0ToKsLLHHWS': 0.1,
    #'BuToD0Pi_PartialD0ToKsDDHHWS': 0.1,
    'BuToD0K_PartialD0ToKsLLHHWS': 0.1,
    #'BuToD0K_PartialD0ToKsDDHHWS': 0.1,

    # commented out due to Rec#208
    #'BuToD0Pi_D0ToKsLLHHWSPi0Resolved': 0.1,
    #'BuToD0Pi_D0ToKsDDHHWSPi0Resolved': 0.1,
    #'BuToD0Pi_D0ToKsLLHHWSPi0Merged': 0.1,
    #'BuToD0Pi_D0ToKsDDHHWSPi0Merged': 0.1,
    #'BuToD0K_D0ToKsLLHHWSPi0Resolved': 0.1,
    #'BuToD0K_D0ToKsDDHHWSPi0Resolved': 0.1,
    #'BuToD0K_D0ToKsLLHHWSPi0Merged': 0.1,
    #'BuToD0K_D0ToKsDDHHWSPi0Merged': 0.1,

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHWS': 0.1,

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHHWS': 0.1,

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHHHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHHHWS': 0.1,
    'BcToD0Pi_D0ToHHWS': 0.1,
    'BcToD0K_D0ToHHWS': 0.1,
    'BcToD0Pi_D0ToKsLLHHWS': 0.1,
    #'BcToD0Pi_D0ToKsDDHHWS': 0.1,
    'BcToD0K_D0ToKsLLHHWS': 0.1,
    #'BcToD0K_D0ToKsDDHHWS': 0.1,
    'BcToD0Pi_D0ToHHHHWS': 0.1,
    'BcToD0K_D0ToHHHHWS': 0.1,
    'BcToDmKst0_DmToHHHWS': 0.1,
    'BcToDsmKst0_DsmToHHHWS': 0.1,

    # lines from b_to_dll
    'BcToDsmMumMumWS_DsmToHHH': 0.1,

    # lines from b_to_dx_ltu
    'BdToDsmK_DsmToKpKmPim_LTU': 0.01,
    'BdToDsmKPiPi_DsmToKmKpPim_LTU': 0.04,

    # lines from b_to_dhh
    'BuToD0KsLLPi_D0ToHHWS': 0.1,
    #'BuToD0KsDDPi_D0ToHHWS': 0.1,
    'BuToD0KsLLPi_D0ToKsLLHHWS': 0.1,
    #'BuToD0KsDDPi_D0ToKsLLHHWS': 0.1,
    #'BuToD0KsLLPi_D0ToKsDDHHWS': 0.1,
    #'BuToD0KsDDPi_D0ToKsDDHHWS': 0.1,
    'BuToD0KsLLPi_D0ToHHHHWS': 0.1,
    #'BuToD0KsDDPi_D0ToHHHHWS': 0.1,

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToHH': 0.1,
    #'BuToD0PiPi0Merged_D0ToHH': 0.1,
    #'BuToD0KPi0Resolved_D0ToHH': 0.1,
    #'BuToD0KPi0Merged_D0ToHH': 0.1,

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToHHWS': 0.1,
    #'BuToD0PiPi0Merged_D0ToHHWS': 0.1,
    #'BuToD0KPi0Resolved_D0ToHHWS': 0.1,
    #'BuToD0KPi0Merged_D0ToHHWS': 0.1,

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToKsLLHH': 0.1,
    #'BuToD0PiPi0Resolved_D0ToKsDDHH': 0.1,
    #'BuToD0PiPi0Merged_D0ToKsLLHH': 0.1,
    #'BuToD0PiPi0Merged_D0ToKsDDHH': 0.1,
    #'BuToD0KPi0Resolved_D0ToKsLLHH': 0.1,
    #'BuToD0KPi0Resolved_D0ToKsDDHH': 0.1,
    #'BuToD0KPi0Merged_D0ToKsLLHH': 0.1,
    #'BuToD0KPi0Merged_D0ToKsDDHH': 0.1,

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToKsLLHHWS': 0.1,
    #'BuToD0PiPi0Resolved_D0ToKsDDHHWS': 0.1,
    #'BuToD0PiPi0Merged_D0ToKsLLHHWS': 0.1,
    #'BuToD0PiPi0Merged_D0ToKsDDHHWS': 0.1,
    #'BuToD0KPi0Resolved_D0ToKsLLHHWS': 0.1,
    #'BuToD0KPi0Resolved_D0ToKsDDHHWS': 0.1,
    #'BuToD0KPi0Merged_D0ToKsLLHHWS': 0.1,
    #'BuToD0KPi0Merged_D0ToKsDDHHWS': 0.1,

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToHHHH': 0.1,
    #'BuToD0PiPi0Merged_D0ToHHHH': 0.1,
    #'BuToD0KPi0Resolved_D0ToHHHH': 0.1,
    #'BuToD0KPi0Merged_D0ToHHHH': 0.1,

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToHHHHWS': 0.1,
    #'BuToD0PiPi0Merged_D0ToHHHHWS': 0.1,
    #'BuToD0KPi0Resolved_D0ToHHHHWS': 0.1,
    #'BuToD0KPi0Merged_D0ToHHHHWS': 0.1,

    # lines from b_to_dhhh
    'BuToD0PiPiPi_D0ToHHWS': 0.1,
    'BuToD0KPiPi_D0ToHHWS': 0.1,
    'BuToD0PiPiPi_D0ToHHHHWS': 0.1,
    'BuToD0KPiPi_D0ToHHHHWS': 0.1,

    # lines of beauty -> charm + charged hadron(s) + neutral decays
    # prescale for now. When photon CL and AM12 get available we will further optimize the selection
    #'OmbmToOmc0PiGammaWS_Omc0ToPKKPi': 0.5,
    #'OmbmToOmc0PiGamma_Omc0ToPKKPi': 0.5,
    #'OmbmToOmc0PiPi0ResolvedWS_Omc0ToPKKPi': 0.1,
    #'OmbmToOmc0PiPi0Resolved_Omc0ToPKKPi': 0.1,
    #'OmbmToXicpKPiGammaWS_XicpToPKPi': 0.5,
    #'OmbmToXicpKPiGamma_XicpToPKPi': 0.5,
}

# lines need flavor tagging
flavor_tagging_lines = [
    # lines from b_to_dh
    'BdToDsmPi_DsmToKpKmPim',
    'BdToDsmK_DsmToKpKmPim',

    # commented out due to Rec#208
    #'BdToDsstmPi_DsstmToDsmGamma_DsmToHHH',
    #'BdToDsstmK_DsstmToDsmGamma_DsmToHHH',
    'BdToDmPi_DmToPimPimKp',

    # lines from b_to_dhh
    'BdToD0KK_D0ToHH',
    'BdToD0KK_D0ToHHHH',

    # commented out due to Rec#208
    'BdToD0KK_D0ToKsLLHH',
    #'BdToD0KK_D0ToKsDDHH',
    'BdToD0PiPi_D0ToHH',
    'BdToD0PiPi_D0ToHHHH',

    # commented out due to Rec#208
    'BdToD0PiPi_D0ToKsLLHH',
    #'BdToD0PiPi_D0ToKsDDHH',

    # commented out due to Rec#208
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDmKsDDPi_DmToHHH',
    'BdToDsmKsLLPi_DsmToHHH',
    #'BdToDsmKsDDPi_DsmToHHH',

    # lines from b_to_dhhh
    'BdToDsmKPiPi_DsmToKmKpPim',
    'BdToDsmPiPiPi_DsmToKmKpPim',
    'BdToDmKPiPi_DmToPimPimKp',
    'BdToDmPiPiPi_DmToPimPimKp',

    # lines from b_to_dd
    'BdToD0D0_D0ToHHOrHHHH',
    'BdToDpDm_DpToHHH',
    'BdToDspDm_DspToHHH_DmToHHH',
    'BdToDspDsm_DspToHHH',
    'BdToDstpDm_DstpToD0Pi_D0ToHHHH_DmToHHH',
    'BdToDstpDm_DstpToD0Pi_D0ToHH_DmToHHH',
    'BdToDstpDsm_DstpToD0Pi_D0ToHHHH_DsmToHHH',
    'BdToDstpDsm_DstpToD0Pi_D0ToHH_DsmToHHH',
    'BdToDstpDstm_DstpToD0Pi_D0ToHH',
    'BdToDstpDstm_DstpToD0Pi_D0ToHHHH',
    'BdToDstpDstm_DstpToD0Pi_D0ToHH_D0ToHHHH',
]

# lines need isolation variables
isolation_lines = []

# lines need to be filtered by a MVA algorithm
mva_filtered_lines = {
    # lines from b_to_dh
    'BdToDmK_DmToPimPimKp': -1.,
}

####################################
# Register lines in sprucing_lines #
# with generic functions           #
####################################

make_default_sprucing_lines(
    line_dict=sprucing_lines,
    line_makers=line_makers,
    default_lines=default_lines)
make_prescaled_sprucing_lines(
    line_dict=sprucing_lines,
    line_makers=line_makers,
    prescaled_lines=prescaled_lines)
make_flavor_tagging_sprucing_lines(
    line_dict=sprucing_lines,
    line_makers=line_makers,
    flavor_tagging_lines=flavor_tagging_lines)
make_isolation_sprucing_lines(
    line_dict=sprucing_lines,
    line_makers=line_makers,
    isolation_lines=isolation_lines)
make_mva_filtered_sprucing_lines(
    line_dict=sprucing_lines,
    line_makers=line_makers,
    mva_filtered_lines=mva_filtered_lines)

####################################
# Register lines in sprucing_lines #
# with unique functions            #
# for lines have special purpose   #
####################################

# test spruce line for FEST
# A Monitor for the b mass is added for development of SprucingDQ
# To save the histogram in a root file the line
# histo_file : 'my_histograms.root'
# has to be added to the lbexec yaml file


@register_line_builder(sprucing_lines)
def BdToDsmK_DsmToHHH_sprucing_FEST_line(
        name='SpruceB2OC_BdToDsmK_DsmToHHH_FEST', prescale=1):
    line_alg = b_to_dh.make_BdToDsmK_DsmToHHH_FEST(process=PROCESS)
    b_mon = Monitor__ParticleRange(
        name="Test_B_Monitor",
        HistogramName="m",
        Input=line_alg,
        Variable=F.MASS,
        Bins=50,
        Range=(5000 * MeV, 7000 * MeV))

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=prefilters.b2oc_prefilters() + [b_mon, line_alg])
