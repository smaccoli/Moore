###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of B2OC hlt2 lines, notice PROCESS = 'hlt2'

Usage:
add line names in corresponding list/dict,
or register lines separately at the very end.

Non trivial imports:
prefilters and line_alg ("bare" line builders) like b_to_dh.make_BdToDsmK_DsmToHHH

Output:
updated dictionary of hlt2_lines

To be noted:
"bare" line builders, like like b_to_dh.make_BdToDsmK_DsmToHHH, have PROCESS as
argument to allow ad hoc settings

"""
from Moore.config import HltLine, register_line_builder

###from RecoConf.event_filters import default_ft_decoding_version

from RecoConf.reconstruction_objects import make_pvs

from PyConf.Algorithms import GetFlavourTaggingParticles

from Hlt2Conf.standard_particles import make_has_rich_long_pions

from Hlt2Conf.lines.b_to_open_charm.utils import update_makers

from Hlt2Conf.lines.b_to_open_charm import prefilters

from Hlt2Conf.lines.b_to_open_charm import b_to_dh
from Hlt2Conf.lines.b_to_open_charm import b_to_dx_ltu
from Hlt2Conf.lines.b_to_open_charm import b_to_dhh
from Hlt2Conf.lines.b_to_open_charm import b_to_dll
from Hlt2Conf.lines.b_to_open_charm import b_to_dmunu
from Hlt2Conf.lines.b_to_open_charm import b_to_dhhh
from Hlt2Conf.lines.b_to_open_charm import b_to_dd
from Hlt2Conf.lines.b_to_open_charm import b_to_ddh
from Hlt2Conf.lines.b_to_open_charm import b_to_ddh_standalone
from Hlt2Conf.lines.b_to_open_charm import b_to_ddhh
from Hlt2Conf.lines.b_to_open_charm import b_to_cbaryon_h
from Hlt2Conf.lines.b_to_open_charm import b_to_cbaryon_hh
from Hlt2Conf.lines.b_to_open_charm import b_to_cbaryons_h
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_h
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_hh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_hhh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_d
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_dh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_cbaryon_dhh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_lightbaryon_d
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_lightbaryon_dh
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_lightbaryon_dd
from Hlt2Conf.lines.b_to_open_charm import bbaryon_to_lightbaryon_ddh

PROCESS = 'hlt2'
hlt2_lines = {}

##############################################
# Read and store all line makers in one dict #
##############################################

line_makers = {}
update_makers(line_makers, b_to_dh)
update_makers(line_makers, b_to_dx_ltu)
update_makers(line_makers, b_to_dhh)
update_makers(line_makers, b_to_dll)
update_makers(line_makers, b_to_dmunu)
update_makers(line_makers, b_to_dhhh)
update_makers(line_makers, b_to_dd)
update_makers(line_makers, b_to_ddh)
update_makers(line_makers, b_to_ddh_standalone)
update_makers(line_makers, b_to_ddhh)
update_makers(line_makers, b_to_cbaryon_h)
update_makers(line_makers, b_to_cbaryon_hh)
update_makers(line_makers, b_to_cbaryons_h)
update_makers(line_makers, bbaryon_to_cbaryon_h)
update_makers(line_makers, bbaryon_to_cbaryon_hh)
update_makers(line_makers, bbaryon_to_cbaryon_hhh)
update_makers(line_makers, bbaryon_to_cbaryon_d)
update_makers(line_makers, bbaryon_to_cbaryon_dh)
update_makers(line_makers, bbaryon_to_cbaryon_dhh)
update_makers(line_makers, bbaryon_to_lightbaryon_d)
update_makers(line_makers, bbaryon_to_lightbaryon_dh)
update_makers(line_makers, bbaryon_to_lightbaryon_dd)
update_makers(line_makers, bbaryon_to_lightbaryon_ddh)

###default_ft_decoding_version.global_bind(value=6)

############################################
# Define functions for line booking        #
# Make it possible to register lines       #
# outside this file (e.g. in test scripts) #
############################################


# default lines
def make_default_hlt2_lines(line_dict=hlt2_lines,
                            line_makers=line_makers,
                            default_lines=None):
    if not default_lines: return
    for decay in default_lines:

        @register_line_builder(line_dict)
        def make_hlt2_line(name='Hlt2B2OC_%s' % decay,
                           maker_name='make_%s' % decay,
                           prescale=1):
            line_alg = line_makers[maker_name](process=PROCESS)
            return HltLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg])


# lines need non-default prescale value
def make_prescaled_hlt2_lines(line_dict=hlt2_lines,
                              line_makers=line_makers,
                              prescaled_lines=None):
    if not prescaled_lines: return
    for decay, prescale in sorted(prescaled_lines.items()):

        @register_line_builder(line_dict)
        def make_hlt2_line(name='Hlt2B2OC_%s' % decay,
                           maker_name='make_%s' % decay,
                           prescale=prescale):
            line_alg = line_makers[maker_name](process=PROCESS)
            return HltLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg])


# lines need flavor tagging
def make_flavor_tagging_hlt2_lines(line_dict=hlt2_lines,
                                   line_makers=line_makers,
                                   flavor_tagging_lines=None):
    if not flavor_tagging_lines: return
    for decay in flavor_tagging_lines:

        @register_line_builder(line_dict)
        def make_hlt2_line(name='Hlt2B2OC_%s' % decay,
                           maker_name='make_%s' % decay,
                           prescale=1):
            line_alg = line_makers[maker_name](process=PROCESS)
            pvs = make_pvs()
            longPions = make_has_rich_long_pions()
            longTaggingParticles = GetFlavourTaggingParticles(
                BCandidates=line_alg,
                TaggingParticles=longPions,
                PrimaryVertices=pvs)

            return HltLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg],
                extra_outputs=[('LongTaggingParticles', longTaggingParticles)])


# lines need isolation info, currently the same as default
def make_isolation_hlt2_lines(line_dict=hlt2_lines,
                              line_makers=line_makers,
                              isolation_lines=None):
    if not isolation_lines: return
    for decay in isolation_lines:

        @register_line_builder(line_dict)
        def make_hlt2_line(name='Hlt2B2OC_%s' % decay,
                           maker_name='make_%s' % decay,
                           prescale=1):
            line_alg = line_makers[maker_name](process=PROCESS)
            return HltLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg])


# lines need a MVA post-filter
def make_mva_filtered_hlt2_lines(line_dict=hlt2_lines,
                                 line_makers=line_makers,
                                 mva_filtered_lines=None):
    if not mva_filtered_lines: return
    for decay, MVACut in sorted(mva_filtered_lines.items()):

        @register_line_builder(line_dict)
        def make_hlt2_line(name='Hlt2B2OC_%s' % decay,
                           MVACut=MVACut,
                           maker_name='make_%s' % decay,
                           prescale=1):
            line_alg = line_makers[maker_name](process=PROCESS, MVACut=MVACut)
            return HltLine(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg])


###########################################
# Lists and dicts of lines to register    #
# Authors should add decays here          #
# NO `Hlt2B2OC_` prefix or `` suffix #
###########################################

# default lines without any further configuration
default_lines = [
    # lines from b_to_dh
    'BuToD0Pi_D0ToHH',
    'BuToD0K_D0ToHH',
    ###'BdToDsmPi_DsmToKpKmPim_Turcal',

    # commented out due to Rec#208
    #'BuToD0Pi_D0ToKsLLPi0Resolved',
    #'BuToD0Pi_D0ToKsDDPi0Resolved',
    #'BuToD0Pi_D0ToKsLLPi0Merged',
    #'BuToD0Pi_D0ToKsDDPi0Merged',
    #'BuToD0K_D0ToKsLLPi0Resolved',
    #'BuToD0K_D0ToKsDDPi0Resolved',
    #'BuToD0K_D0ToKsLLPi0Merged',
    #'BuToD0K_D0ToKsDDPi0Merged',
    'BuToD0Pi_D0ToKsLLHH',
    #'BuToD0Pi_D0ToKsDDHH',
    'BuToD0K_D0ToKsLLHH',
    #'BuToD0K_D0ToKsDDHH',

    # commented out due to Rec#208
    #'BuToD0K_D0ToHHPi0Resolved',
    #'BuToD0Pi_D0ToHHPi0Resolved',
    #'BuToD0K_D0ToHHPi0Merged',
    #'BuToD0Pi_D0ToHHPi0Merged',
    'BuToD0Pi_D0ToHHHH',
    'BuToD0K_D0ToHHHH',

    # commented out due to Rec#208
    #'BuToD0Pi_D0ToKsLLHHPi0Resolved',
    #'BuToD0Pi_D0ToKsDDHHPi0Resolved',
    #'BuToD0Pi_D0ToKsLLHHPi0Merged',
    #'BuToD0Pi_D0ToKsDDHHPi0Merged',
    #'BuToD0K_D0ToKsLLHHPi0Resolved',
    #'BuToD0K_D0ToKsDDHHPi0Resolved',
    #'BuToD0K_D0ToKsLLHHPi0Merged',
    #'BuToD0K_D0ToKsDDHHPi0Merged',

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHH',

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHH',

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHHHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHHH',
    'TbcToDpKm',
    'TbcToD0KsLL_D0ToKPiOrKPiPiPi',
    'TbcToD0KsDD_D0ToKPiOrKPiPiPi',

    # lines from b_to_dhh
    'BdToD0KPi_D0ToHH',
    'BdToD0KPi_D0ToKsLLHH',
    #'BdToD0KPi_D0ToKsDDHH',

    # commented out due to Rec#208
    #'BdToD0KPi_D0ToHHPi0Resolved',
    #'BdToD0KPi_D0ToHHPi0Merged',
    'BdToD0KPi_D0ToHHHH',
    'BdToD0PbarP_D0ToHHHH',

    # commented out due to Rec#208
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHH',

    # commented out due to Rec#208
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToKsDDHH',

    # commented out due to Rec#208
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHHPi0Resolved',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHHPi0Merged',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',

    # commented out due to Rec#208
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHHHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHHHH',

    # commented out due to Rec#208
    #'BdToDsstmKsLLPi_DsstmToDsmGamma_DsmToHHH',
    #'BdToDsstmKsDDPi_DsstmToDsmGamma_DsmToHHH',
    'BuToDmPiPi_DmToHHH',
    'BuToDpPiPi_DpToHHH',
    'BuToDmKK_DmToHHH',
    'BuToDsmPiPi_DsmToHHH',
    'BuToDsmKK_DsmToHHH',
    'BuToDstmPiPi_DstmToD0Pi_D0ToHH',
    'BuToDstpPiPi_DstpToD0Pi_D0ToHH',
    'BuToDstmKPi_DstmToD0Pi_D0ToHH',
    'BuToDstpKPi_DstpToD0Pi_D0ToHH',
    'BuToDstmKK_DstmToD0Pi_D0ToHH',
    'BuToDstpKK_DstpToD0Pi_D0ToHH',
    'BuToDstpPbarP_DstpToD0Pi_D0ToHH',

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToHH',
    #'BuToD0PiPi0Merged_D0ToHH',
    #'BuToD0KPi0Resolved_D0ToHH',
    #'BuToD0KPi0Merged_D0ToHH',

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToKsLLHH',
    #'BuToD0PiPi0Resolved_D0ToKsDDHH',
    #'BuToD0PiPi0Merged_D0ToKsLLHH',
    #'BuToD0PiPi0Merged_D0ToKsDDHH',
    #'BuToD0KPi0Resolved_D0ToKsLLHH',
    #'BuToD0KPi0Resolved_D0ToKsDDHH',
    #'BuToD0KPi0Merged_D0ToKsLLHH',
    #'BuToD0KPi0Merged_D0ToKsDDHH',

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToHHHH',
    #'BuToD0PiPi0Merged_D0ToHHHH',
    #'BuToD0KPi0Resolved_D0ToHHHH',
    #'BuToD0KPi0Merged_D0ToHHHH',
    'TbcToD0KmPip_D0ToKPiOrKPiPiPi',
    'TbcToD0PipPim_D0ToKPiOrKPiPiPi',

    # lines from b_to_dmunu
    'BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHH',
    #'BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHH',
    'BdToDstmMuNu_DstmToD0Pi_D0ToHHHH',

    # lines from b_to_dhhh
    'BdToDsmKKPi_DsmToKmKpPim',

    # lines from b_to_dd
    'BuToD0Dp_D0ToHH_DpToHHH',
    'BuToD0Dp_D0ToHHHH_DpToHHH',
    'BuToD0Dsp_D0ToHH_DspToHHH',
    'BuToD0Dsp_D0ToHHHH_DspToHHH',
    'BuToDstD0_DstToD0Pi_D0ToHH_D0ToHH',
    'BuToDstD0_DstToD0Pi_D0ToHH_D0ToHHHH',
    'BuToDstD0_DstToD0Pi_D0ToHHHH_D0ToHH',
    'BuToDstD0_DstToD0Pi_D0ToHHHH_D0ToHHHH',
    'BcToD0Dp_D0ToHH_DpToHHH',
    'BcToD0Dp_D0ToHHHH_DpToHHH',
    'BcToD0Dp_D0ToKsLLHH_DpToHHH',
    #'BcToD0Dp_D0ToKsDDHH_DpToHHH',
    'BcToD0Dsp_D0ToHH_DspToHHH',
    'BcToD0Dsp_D0ToHHHH_DspToHHH',
    'BcToD0Dsp_D0ToKsLLHH_DspToHHH',
    #'BcToD0Dsp_D0ToKsDDHH_DspToHHH',
    'BcToDstD0_DstToD0Pi_D0ToHH_D0ToHH',
    'BcToDstD0_DstToD0Pi_D0ToHH_D0ToHHHH',
    'BcToDstD0_DstToD0Pi_D0ToHH_D0ToKsLLHH',
    #'BcToDstD0_DstToD0Pi_D0ToHH_D0ToKsDDHH',
    'BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToHH',
    'BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToHHHH',
    #'BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToKsDDHH',
    'BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToKsLLHH',
    'TbcToD0D0_D0ToKPiOrKPiPiPi',

    # lines from b_to_ddh
    'BdToD0D0Kst_D0ToHHOrHHHH',
    'BdToDstD0K_DstToD0Pi_D0ToHH_D0ToHH',

    # lines from b_to_ddh_standalone
    'BdToD0D0KsLL_D0ToKPiOrKPiPiPi',
    #'BdToD0D0KsDD_D0ToKPiOrKPiPiPi',
    'BdToDpDmKsLL_DpToHHH',
    #'BdToDpDmKsDD_DpToHHH',
    'BdToDpDmKst_DpToHHH',
    'BdToDpDmPhi_DpToHHH',
    'BdToDspDsmPhi_DspToHHH',
    'BdToDstD0Pi_DstToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi',
    'BdToDstD0K_DstToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi',
    'BdToDstDmKSLL_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH',
    #'BdToDstDmKsDD_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH',
    'BdToDstpDmKst_DstpToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH',
    'BdToDstmDpKst_DstmToD0Pi_D0ToKPiOrKPiPiPi_DpToHHH',
    #'BdToDst0DK_Dst0ToD0Gamma_D0ToKPiOrKPiPiPi_DToHHH',
    #'BdToDst0DK_Dst0ToD0Pi0Resolved_D0ToKPiOrKPiPiPi_DToHHH',
    #'BdToDst0DK_Dst0ToD0Pi0Merged_D0ToKPiOrKPiPiPi_DToHHH',
    'BdToDstDsPhi_DstToD0Pi_D0ToKPiOrKPiPiPi_DsToHHH',
    'BdToDstmDspRho0_DstmToD0Pi_D0ToKPiOrKPiPiPi_DspToHHH',
    #'BdToDst0DspPi_Dst0ToD0Gamma_D0ToKPiOrKPiPiPi_DspToHHH',
    #'BdToDst0DsPi_Dst0ToD0Pi0Resolved_D0ToKPiOrKPiPiPi_DsToHHH',
    #'BdToDst0DsPi_Dst0ToD0Pi0Merged_D0ToKPiOrKPiPiPi_DsToHHH',
    'BdToDstpDstmKsLL_DstpToD0Pi_D0ToKPiorKPiPiPi',
    #'BdToDstpDstmKsDD_DstpToD0Pi_D0ToKPiorKPiPiPi',
    'BdToDstpDstmPhi_DstpToD0Pi_D0ToKPiOrKPiPiPi',
    #'BdToDsstpD0Pi_DsstpToDspGamma_DspToHHH_D0ToKPiOrKPiPiPi',
    'BuToDpDmPi_DpToHHH',
    'BuToDpDmK_DpToHHH',
    'BuToD0DpKsLL_D0ToKPiOrKPiPiPi_DpToHHH',
    #'BuToD0DpKsDD_D0ToKPiOrKPiPiPi_DpToHHH',
    'BuToDstDPi_DstToD0Pi_D0ToKPiOrKPiPiPi_DToHHH',
    'BuToDstDK_DstToD0Pi_D0ToKPiOrKPiPiPi_DToHHH',
    'BuToDstpD0KsLL_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi',
    #'BuToDstpD0KsDD_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi',
    'BuToDstpD0Kst_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi',
    'BuToDstmDspPi_DstmToD0Pi_D0ToKPiOrKPiPiPi_DspToHHH',
    #'BuToDst0D0K_Dst0ToD0Gamma_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi',
    #'BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi',
    #'BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi',
    #'BuToDsstpDmPi_DsstpToDspGamma_DspToHHH_DmToHHH',
    'BuToDstpDstmPi_DstpToD0Pi_D0ToKPiOrKPiPiPi',
    'BuToDstpDstmK_DstToD0Pi_D0ToKPiOrKPiPiPi',
    'BuToDstpDstmKst_DstpToD0Pi_D0ToKPiOrKPiPiPi',

    # lines from b_to_ddhh
    'TbcToD0D0PipPim_D0ToKPiOrKPiPiPi',

    # lines from b_to_cbaryon_h
    'TbcToXiccpP_XiccpToXic0Pi_Xic0ToPKKPi',
    'TbcToXiccpP_XiccpToLcpKmPip_LcpToPKPi',
    'TbcToXiccpP_XiccpToPDpKm_DpToPipPipKm',

    # lines from b_to_cbaryon_hh
    'BdToXic0PPi_Xic0ToPKKPi',
    'BdToXic0PK_Xic0ToPKKPi',
    'BuToLcmPPi_LcmToPKPi',
    'BuToLcpPPi_LcpToPKPi',
    'BuToLcmPK_LcmToPKPi',
    'BuToLcpPK_LcpToPKPi',
    'BuToXicmPPi_XicmToPKPi',
    'BuToXicpPPi_XicpToPKPi',
    'BuToXicmPK_XicmToPKPi',
    'BuToXicpPK_XicpToPKPi',

    # lines from b_to_cbaryons_h
    'BuToLcpLcmK_LcpToPKPi',
    'BuToLcpXicmPi_LcpToPKPi_XicmToPKPi',
    'BdToScmmLcpK_ScmmToLcmPi_LcmToPKPi_LcpToPKPi',

    # lines from bbaryon_to_cbaryon_h
    'LbToLcpPi_LcpToPPiPi',
    'LbToLcpPi_LcpToPKPi',
    'LbToLcpPi_LcpToPKK',
    'LbToLcpK_LcpToPKPi',
    'LbToLcpPi_LcpToLambdaLLPi',
    #'LbToLcpPi_LcpToLambdaDDPi',
    'LbToLcpPi_LcpToLambdaLLK',
    #'LbToLcpPi_LcpToLambdaDDK',
    'LbToLcpK_LcpToLambdaLLPi',
    #'LbToLcpK_LcpToLambdaDDPi',
    'LbToLcpK_LcpToLambdaLLK',
    #'LbToLcpK_LcpToLambdaDDK',
    'LbToLcpPi_LcpToPKsLL',
    #'LbToLcpPi_LcpToPKsDD',
    'LbToLcpK_LcpToPKsLL',
    #'LbToLcpK_LcpToPKsDD',
    'LbToXicpK_XicpToPKPi',
    'Xib0ToXicpPi_XicpToPKPi',
    'Xib0ToXicpK_XicpToPKPi',
    'XibmToXic0Pi_Xic0ToPKKPi',
    'XibmToXic0PiWS_Xic0ToPKKPi',
    'XibmToXic0K_Xic0ToPKKPi',
    'OmbmToOmc0Pi_Omc0ToPKKPi',
    'OmbmToOmc0K_Omc0ToPKKPi',
    'Xibc0ToXiccpPi_XiccpToXic0Pi_Xic0ToPKKPi',
    'Xibc0ToXiccpPi_XiccpToLcpKmPip_LcpToPKPi',
    'Xibc0ToXiccpPi_XiccpToPDpKm_DpToPipPipKm',
    'Xibc0ToLcpPi_LcpToPKPi',
    'Xibc0ToLcpK_LcpToPKPi',
    'Xibc0ToXicpPi_XicpToPKPi',
    'XibcpToXiccppPi_XiccppToXicpPi_XicpToPKPi',
    'XibcpToXiccppPi_XiccppToLcpKPiPi_LcpToPKPi',
    'XibcpToXic0Pi_Xic0ToPKKPi',
    "Ombc0ToXicpK_XicpToPKPi",

    # lines from bbaryon_to_cbaryon_hh
    'LbToXic0PiPi_Xic0ToPKKPi',
    'LbToXic0KPi_Xic0ToPKKPi',
    'LbToXic0KK_Xic0ToPKKPi',
    'LbToOmc0PiPi_Omc0ToPKKPi',
    'LbToOmc0KPi_Omc0ToPKKPi',
    'LbToOmc0KK_Omc0ToPKKPi',
    'XibmToLcpPiPi_LcpToPKPi',
    'XibmToLcpKPi_LcpToPKPi',
    'XibmToLcpKK_LcpToPKPi',
    'XibmToXicpPiPi_XicpToPKPi',
    'XibmToXicpKPi_XicpToPKPi',
    'Xibc0ToXic0PiPi_Xic0ToPKKPi',
    'XibcpToLcpKmPip_LcpToPKPi',
    'XibcpToXicpPiPi_XicpToPKPi',
    'XibcpToLcpPiPi_LcpToPKPi',
    'Ombc0ToXic0KmPip_Xic0ToPKKPi',
    'OmbmToXicpKPi_XicpToPKPi',

    # lines from bbaryon_to_cbaryon_hhh
    'LbToLcpPiPiPi_LcpToPKK',
    'LbToLcpKPiPi_LcpToPKPi',
    'XibmToXic0PiPiPi_Xic0ToPKKPi',
    'XibmToXic0KPiPi_Xic0ToPKKPi',
    'XibmToXic0KKPi_Xic0ToPKKPi',
    'XibmToXic0PbarPPi_Xic0ToPKKPi',
    'OmbmToOmc0PiPiPi_Omc0ToPKKPi',
    'OmbmToOmc0KPiPi_Omc0ToPKKPi',
    'OmbmToOmc0KKPi_Omc0ToPKKPi',
    'OmbmToOmc0PbarPPi_Omc0ToPKKPi',
    'OmbmToXic0KmPipPim_Omc0ToPKKPi',
    'OmbmToXic0KmPipPimWS_Omc0ToPKKPi',

    # lines from bbaryon_to_cbaryon_d
    'LbToLcpDm_LcpToPKPi_DmToPimPimKp',
    'LbToLcpDsm_LcpToPKPi_DsmToHHH',
    'XibcpToLcpD0_LcpToPKPi_D0ToKPiOrKPiPiPi',
    'Ombc0ToXic0D0_Xic0ToPKKPi_D0ToKPiOrKPiPiPi',

    # lines from bbaryon_to_cbaryon_dh
    'Xibc0ToLcpD0Pi_LcpToPKPi_D0ToKPiOrKPiPiPi',
    "Ombc0ToLcpD0K_LcpToPKPi_D0ToKPiOrKPiPiPi",

    # lines from bbaryon_to_cbaryon_dhh
    'XibcpToLcpD0PipPim_LcpToPKPi_D0ToKPiOrKPiPiPi',

    # lines from bbaryon_to_lightbaryon_d
    'LbToLambdaLLD0_D0ToKsLLHH',
    #'LbToLambdaLLD0_D0ToKsDDHH',
    #'LbToLambdaDDD0_D0ToKsLLHH',
    #'LbToLambdaDDD0_D0ToKsDDHH',
    'LbToXimDsp_DspToKKPi_XimToLambdaLLPi',
    #'LbToXimDsp_DspToKKPi_XimToLambdaDDPi',
    'Xib0ToXimDp_DpToKPiPi_XimToLambdaLLPi',
    #'Xib0ToXimDp_DpToKPiPi_XimToLambdaDDPi',
    'XibmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaLLPi',
    #'XibmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaDDPi',
    'OmbmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaLLPi',
    #'OmbmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaDDPi',
    'OmbmToOmmD0_D0ToKPiOrKPiPiPi_OmmToLambdaLLK',
    #'OmbmToOmmD0_D0ToKPiOrKPiPiPi_OmmToLambdaDDK',
    'XibcpToPD0_D0ToKPiOrKPiPiPi',
    'Xibc0ToD0LambdaLL_D0ToKPiOrKPiPiPi',
    'Xibc0ToD0LambdaDD_D0ToKPiOrKPiPiPi',
    'XibcpToDpLambdaLL_DpToKmPipPip',
    'XibcpToDpLambdaDD_DpToKmPipPip',

    # lines from bbaryon_to_lightbaryon_dh
    'LbToD0PPi_D0ToKsLLHH',
    #'LbToD0PPi_D0ToKsDDHH',
    'LbToD0PK_D0ToKsLLHH',
    #'LbToD0PK_D0ToKsDDHH',
    'LbToD0PPi_D0ToHHHH',
    'LbToD0PK_D0ToHHHH',
    'XibmToDstmPPi_DstmToD0Pi_D0ToHH',
    'XibmToDstmPK_DstmToD0Pi_D0ToHH',
    'LbToXimD0Kp_D0ToKPiOrKPiPiPi_XimToLambdaLLPi',
    #'LbToXimD0Kp_D0ToKPiOrKPiPiPi_XimToLambdaDDPi',
    'Xib0ToXimD0Pip_D0ToKPiOrKPiPiPi_XimToLambdaLLPi',
    #'Xib0ToXimD0Pip_D0ToKPiOrKPiPiPi_XimToLambdaDDPi',
    'Xibc0ToPD0K_D0ToKPiOrKPiPiPi',
    'XibcpToPDpK_DpToKmPipPip',
    'XibcpToD0LambdaLLPip_D0ToKPiOrKPiPiPi',
    'XibcpToD0LambdaDDPip_D0ToKPiOrKPiPiPi',

    # lines from bbaryon_to_lightbaryon_dd
    'XibcpToPD0D0_D0ToKPiOrKPiPiPi',

    # lines from bbaryon_to_lightbaryon_ddh
    'LbToDpDmPK_DpToHHH',
    'Xibc0ToD0D0PPim_D0ToKPiOrKPiPiPi',
    'XibcpToD0DpPPim_D0ToKPiOrKPiPiPi_DpToKmPipPip',
]

# lines need non-default prescale value
prescaled_lines = {
    # lines from b_to_dh
    'BuToD0Pi_D0ToHHWS': 0.1,
    'BuToD0K_D0ToHHWS': 0.1,
    'BuToD0Pi_D0ToKsLLHHWS': 0.1,
    #'BuToD0Pi_D0ToKsDDHHWS': 0.1,
    'BuToD0K_D0ToKsLLHHWS': 0.1,
    #'BuToD0K_D0ToKsDDHHWS': 0.1,

    # commented out due to Rec#208
    #'BuToD0Pi_D0ToHHPi0ResolvedWS': 0.1,
    #'BuToD0Pi_D0ToHHPi0MergedWS': 0.1,
    #'BuToD0K_D0ToHHPi0ResolvedWS': 0.1,
    #'BuToD0K_D0ToHHPi0MergedWS': 0.1,
    'BuToD0Pi_D0ToHHHHWS': 0.1,
    'BuToD0K_D0ToHHHHWS': 0.1,

    # commented out due to Rec#208
    #'BuToD0Pi_D0ToKsLLHHWSPi0Resolved': 0.1,
    #'BuToD0Pi_D0ToKsDDHHWSPi0Resolved': 0.1,
    #'BuToD0Pi_D0ToKsLLHHWSPi0Merged': 0.1,
    #'BuToD0Pi_D0ToKsDDHHWSPi0Merged': 0.1,
    #'BuToD0K_D0ToKsLLHHWSPi0Resolved': 0.1,
    #'BuToD0K_D0ToKsDDHHWSPi0Resolved': 0.1,
    #'BuToD0K_D0ToKsLLHHWSPi0Merged': 0.1,
    #'BuToD0K_D0ToKsDDHHWSPi0Merged': 0.1,

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHWS': 0.1,

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHHWS': 0.1,

    # commented out due to Rec#208
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHHHWS': 0.1,
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHHHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHHHWS': 0.1,
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHHHWS': 0.1,

    # lines from b_to_dx_ltu
    'BdToDsmK_DsmToKpKmPim_LTU': 0.01,

    # lines from b_to_dhh
    'BdToD0PiPiWS_D0ToHH': 0.1,
    'BdToD0KPiWS_D0ToHH': 0.1,
    'BdToD0KKWS_D0ToHH': 0.1,
    'BdToD0PiPiWS_D0ToKsLLHH': 0.1,
    #'BdToD0PiPiWS_D0ToKsDDHH': 0.1,
    'BdToD0KPiWS_D0ToKsLLHH': 0.1,
    #'BdToD0KPiWS_D0ToKsDDHH': 0.1,
    'BdToD0KKWS_D0ToKsLLHH': 0.1,
    #'BdToD0KKWS_D0ToKsDDHH': 0.1,
    'BdToD0PiPiWS_D0ToHHHH': 0.1,
    'BdToD0KPiWS_D0ToHHHH': 0.1,
    'BdToD0KKWS_D0ToHHHH': 0.1,
    'BdToD0PbarPWS_D0ToHHHH': 0.1,

    # commented out due to Rec#208
    #'BdToD0PiPiWS_D0ToHHPi0Resolved': 0.1,
    #'BdToD0PiPiWS_D0ToHHPi0Merged': 0.1,
    #'BdToD0KPiWS_D0ToHHPi0Resolved': 0.1,
    #'BdToD0KPiWS_D0ToHHPi0Merged': 0.1,
    #'BdToD0KKWS_D0ToHHPi0Resolved': 0.1,
    #'BdToD0KKWS_D0ToHHPi0Merged': 0.1,

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToHHWS': 0.1,
    #'BuToD0PiPi0Merged_D0ToHHWS': 0.1,
    #'BuToD0KPi0Resolved_D0ToHHWS': 0.1,
    #'BuToD0KPi0Merged_D0ToHHWS': 0.1,

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToKsLLHHWS': 0.1,
    #'BuToD0PiPi0Resolved_D0ToKsDDHHWS': 0.1,
    #'BuToD0PiPi0Merged_D0ToKsLLHHWS': 0.1,
    #'BuToD0PiPi0Merged_D0ToKsDDHHWS': 0.1,
    #'BuToD0KPi0Resolved_D0ToKsLLHHWS': 0.1,
    #'BuToD0KPi0Resolved_D0ToKsDDHHWS': 0.1,
    #'BuToD0KPi0Merged_D0ToKsLLHHWS': 0.1,
    #'BuToD0KPi0Merged_D0ToKsDDHHWS': 0.1,

    # commented out because pi0 builder crash hlt2_pp_thor test
    #'BuToD0PiPi0Resolved_D0ToHHHHWS': 0.1,
    #'BuToD0PiPi0Merged_D0ToHHHHWS': 0.1,
    #'BuToD0KPi0Resolved_D0ToHHHHWS': 0.1,
    #'BuToD0KPi0Merged_D0ToHHHHWS': 0.1,

    # commented out due to Rec#208
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHH': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHH': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHH': 0.1,
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHH': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHH': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHH': 0.1,
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToKsLLHH': 0.1,
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToKsDDHH': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToKsLLHH': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToKsDDHH': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToKsLLHH': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToKsDDHH': 0.1,
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH': 0.1,
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH': 0.1,

    # commented out due to Rec#208
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHPi0Resolved': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHPi0Resolved': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHPi0Resolved': 0.1,
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved': 0.1,
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHPi0Merged': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHPi0Merged': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHPi0Merged': 0.1,
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged': 0.1,

    # commented out due to Rec#208
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHHH': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHHH': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHHH': 0.1,
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHHH': 0.1,
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHHH': 0.1,
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHHH': 0.1,

    # lines from b_to_dhhh

    # lines from bbaryon_to_cbaryon_h
    'LbToLcpPiWS_LcpToPPiPi': 0.1,
    'LbToLcpPiWS_LcpToPKK': 0.1,
    'OmbmToOmc0PiWS_Omc0ToPKKPi': 0.1,

    # lines from bbaryon_to_cbaryon_hh
    'XibmToLcpKPiWS_LcpToPKPi': 0.1,
    'OmbmToXicpKPiWS_XicpToPKPi': 0.1,

    # lines from bbaryon_to_cbaryon_hhh
    'LbToLcpPiPiPiWS_LcpToPPiPi': 0.1,
    'LbToLcpPiPiPiWS_LcpToPKK': 0.1,
    'OmbmToOmc0PiPiPiWS_Omc0ToPKKPi': 0.1,

    # lines from bbaryon_to_lightbaryon_dh
    'LbToD0PPi_D0ToKsLLHHWS': 0.1,
    #'LbToD0PPi_D0ToKsDDHHWS': 0.1,
    'LbToD0PK_D0ToKsLLHHWS': 0.1,
    #'LbToD0PK_D0ToKsDDHHWS': 0.1,
    'LbToD0PPiWS_D0ToKsLLHH': 0.1,
    #'LbToD0PPiWS_D0ToKsDDHH': 0.1,
    'LbToD0PKWS_D0ToKsLLHH': 0.1,
    #'LbToD0PKWS_D0ToKsDDHH': 0.1,
    'LbToD0PPi_D0ToHHHHWS': 0.1,
    'LbToD0PK_D0ToHHHHWS': 0.1,
    'LbToD0PPiWS_D0ToHHHH': 0.1,
    'LbToD0PKWS_D0ToHHHH': 0.1,
}

# lines need flavor tagging
flavor_tagging_lines = [
    # lines from b_to_dh
    'BdToDsmPi_DsmToKpKmPim',
    'BdToDsmK_DsmToKpKmPim',

    # commented out due to Rec#208
    #'BdToDsstmPi_DsstmToDsmGamma_DsmToKpKmPim',
    #'BdToDsstmK_DsstmToDsmGamma_DsmToKpKmPim',
    'BdToDmPi_DmToPimPimKp',

    # lines from b_to_dhh
    'BdToD0KK_D0ToHH',
    'BdToD0KK_D0ToHHHH',

    # commented out due to Rec#208
    #'BdToD0KK_D0ToHHPi0Resolved',
    #'BdToD0KK_D0ToHHPi0Merged',
    'BdToD0KK_D0ToKsLLHH',
    #'BdToD0KK_D0ToKsDDHH',
    'BdToD0PiPi_D0ToHH',
    'BdToD0PiPi_D0ToHHHH',

    # commented out due to Rec#208
    #'BdToD0PiPi_D0ToHHPi0Resolved',
    #'BdToD0PiPi_D0ToHHPi0Merged',
    'BdToD0PiPi_D0ToKsLLHH',
    #'BdToD0PiPi_D0ToKsDDHH',

    # commented out due to Rec#208
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHHHH',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHHPi0Merged',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHHPi0Resolved',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHPi0Merged',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHPi0Resolved',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
]

# lines need isolation variables
isolation_lines = []

# lines need to be filtered by a MVA algorithm
mva_filtered_lines = {
    # lines from b_to_dh
    'BdToDmK_DmToPimPimKp': 0.,

    # lines from b_to_dhhh
    # commented out due to missing ThOr implementantion of TMVA
    #'BuToD0PiPiPi_D0ToHH': -0.7,
    #'BuToD0KPiPi_D0ToHH': -0.7,
    #'BuToD0KKPi_D0ToKPi': -0.7,
    #'BuToD0PbarPPi_D0ToKPi': -0.9,
}

################################
# Register lines in hlt2_lines #
# with generic functions       #
################################

make_default_hlt2_lines(
    line_dict=hlt2_lines, line_makers=line_makers, default_lines=default_lines)
make_prescaled_hlt2_lines(
    line_dict=hlt2_lines,
    line_makers=line_makers,
    prescaled_lines=prescaled_lines)
make_flavor_tagging_hlt2_lines(
    line_dict=hlt2_lines,
    line_makers=line_makers,
    flavor_tagging_lines=flavor_tagging_lines)
make_isolation_hlt2_lines(
    line_dict=hlt2_lines,
    line_makers=line_makers,
    isolation_lines=isolation_lines)
make_mva_filtered_hlt2_lines(
    line_dict=hlt2_lines,
    line_makers=line_makers,
    mva_filtered_lines=mva_filtered_lines)

##################################
# Register lines in hlt2_lines   #
# with unique functions          #
# for lines have special purpose #
##################################

#
# test line, add any new line above this last one
#
#@register_line_builder(hlt2_line)
#def BdToDsmK_DsmToHHH_Test_hlt2_line(
#        name='Hlt2B2OC_BdToDsmK_DsmToHHH_2or3bodyTOPO',
#        min_twobody_mva=0.1,
#        min_threebody_mva=0.1,
#        MVACut=-1.,
#        prescale=1):
#    line_alg = b_to_dh.make_BdToDsmK_DsmToHHH_Test(
#        process=PROCESS, MVACut=MVACut)
#    return HltLine(
#        name=name,
#        prescale=prescale,
#        algs=prefilters.b2oc_prefilters(
#            require_topo=True,
#            min_twobody_mva=min_twobody_mva,
#            min_threebody_mva=min_threebody_mva) + [line_alg])
