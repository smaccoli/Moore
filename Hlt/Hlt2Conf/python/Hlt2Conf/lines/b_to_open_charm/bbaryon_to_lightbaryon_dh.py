###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond

import Functors as F

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder

###########################################################
# Form the Xi_bc0 -> D0 pK-,  D0 --> Kpi & K3pi
# NB: Here, D0 == K- ... AND K+ ...
##########################################################


@check_process
def make_Xibc0ToPD0K_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons()
        proton = basic_builder.make_tightpid_tight_protons()
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
    elif process == 'hlt2':
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)
        proton = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
        cmeson = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()

    line_alg = b_builder.make_xibc2cx(
        particles=[cmeson, proton, kaon],
        descriptors=['Xi_bc0 -> D0 p+ K-', 'Xi_bc~0 -> D0 p~- K+'],
        sum_pt_hbach_min=2.25 * GeV,
    )
    return line_alg


###########################################################
# Form the Xi_bc+ -> D+ pK-,  D+ --> K- pi+ pi+
##########################################################
@check_process
def make_XibcpToPDpK_DpToKmPipPip(process):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons()
        proton = basic_builder.make_tightpid_tight_protons()
        cmeson = d_builder.make_dplus_to_kmpippip()
    elif process == 'hlt2':
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)
        proton = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
        cmeson = d_builder.make_tight_dplus_to_kmpippip_for_xibc()
    line_alg = b_builder.make_xibc2cx(
        particles=[cmeson, proton, kaon],
        descriptors=['[Xi_bc+ -> D+ p+ K-]cc'],
        sum_pt_hbach_min=2.25 * GeV,
    )
    return line_alg


#############################################################################
# Form the Xibc -> D0 Lambda pi+, D0 --> Kpi & K3pi
# Lambda0 : LL and DD
##############################################################################


@check_process
def make_XibcpToD0LambdaLLPip_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        pions = basic_builder.make_soft_pions()
    if process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        pions = basic_builder.make_tightpid_soft_pions()
    lambdas = basic_builder.make_lambda_LL()

    ### applying hard cuts on pion if it's not forming D* with D0
    Dst_13 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 3]) - F.CHILD(1, F.MASS))
              < 150 * MeV)
    tight3 = ((F.CHILD(3, F.PT) > 500 * MeV) & (F.CHILD(3, F.P) > 5 * GeV))

    comb_cut_add = (Dst_13 | tight3)

    line_alg = b_builder.make_xibc2cx(
        particles=[Dz, lambdas, pions],
        descriptors=['Xi_bc+ -> D0 Lambda0 pi+', 'Xi_bc~- -> D0 Lambda~0 pi-'],
        sum_pt_hbach_min=2.25 * GeV,
        comb_cut_add=comb_cut_add,
    )
    return line_alg


@check_process
def make_XibcpToD0LambdaDDPip_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        pions = basic_builder.make_soft_pions()
    if process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        pions = basic_builder.make_tightpid_soft_pions()
    lambdas = basic_builder.make_lambda_DD()

    ### applying hard cuts on pion if it's not forming D* with D0
    Dst_13 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 3]) - F.CHILD(1, F.MASS))
              < 150 * MeV)
    tight3 = ((F.CHILD(3, F.PT) > 500 * MeV) & (F.CHILD(3, F.P) > 5 * GeV))

    comb_cut_add = (Dst_13 | tight3)

    line_alg = b_builder.make_xibc2cx(
        particles=[Dz, lambdas, pions],
        descriptors=['Xi_bc+ -> D0 Lambda0 pi+', 'Xi_bc~- -> D0 Lambda~0 pi-'],
        sum_pt_hbach_min=2.25 * GeV,
        comb_cut_add=comb_cut_add,
    )
    return line_alg


###########################################################
# Form the Lambda_b (Xi_b0) -> D0 p pi-,  D0 --> h+ h-
##########################################################
@check_process
def make_LbToD0PPi_D0ToHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh()
        proton = basic_builder.make_tightpid_soft_protons()
        pion = basic_builder.make_tightpid_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi- D0', 'Lambda_b~0 -> p~- pi+ D0'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the Lambda_b (Xi_b0) -> D0 p K-,  D0 --> h+ h-
##########################################################
@check_process
def make_LbToD0PK_D0ToHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh()
        proton = basic_builder.make_tightpid_soft_protons()
        kaon = basic_builder.make_tightpid_soft_kaons()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K- D0', 'Lambda_b~0 -> p~- K+ D0'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the Lambda_b (Xi_b0) -> D0 p pi-,  D0 --> 4h
##########################################################
@check_process
def make_LbToD0PPi_D0ToHHHH(process):
    if process == 'spruce':
        dzero = d_builder.make_tight_dzero_to_hhhh(
            pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hhhh(
            am_min=1.82 * GeV, am_max=1.9 * GeV)
        proton = basic_builder.make_soft_protons(
            p_pidp_min=3, p_min=10 * GeV, pt_min=0.5 * GeV)
        pion = basic_builder.make_soft_pions(
            pi_pidk_max=2, p_min=3.5 * GeV, pt_min=0.25 * GeV)
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi- D0', 'Lambda_b~0 -> p~- pi+ D0'],
        sum_pt_min=6 * GeV,
        am_min=5.5 * GeV,
        am_max=5.75 * GeV,
        bpvltime_min=0.3 * picosecond)
    return line_alg


@check_process
def make_LbToD0PPi_D0ToHHHHWS(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hhhh_ws()
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi- D0', 'Lambda_b~0 -> p~- pi+ D0'])
    return line_alg


@check_process
def make_LbToD0PPiWS_D0ToHHHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hhhh()
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi+ D0', 'Lambda_b~0 -> p~- pi- D0'])
    return line_alg


###########################################################
# Form the Lambda_b (Xi_b0) -> D0 p K-,  D0 --> 4h
##########################################################
@check_process
def make_LbToD0PK_D0ToHHHH(process):
    if process == 'spruce':
        dzero = d_builder.make_tight_dzero_to_hhhh(
            pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        #dzero = d_builder.make_tight_dzero_to_hhhh()
        dzero = d_builder.make_dzero_to_hhhh(
            am_min=1.82 * GeV, am_max=1.9 * GeV)
        proton = basic_builder.make_soft_protons(
            p_pidp_min=3, p_min=10 * GeV, pt_min=0.5 * GeV)
        kaon = basic_builder.make_soft_kaons(
            k_pidk_min=-2, p_min=3.5 * GeV, pt_min=0.25 * GeV)
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K- D0', 'Lambda_b~0 -> p~- K+ D0'],
        sum_pt_min=6 * GeV,
        am_min=5.5 * GeV,
        am_max=5.75 * GeV,
        bpvltime_min=0.3 * picosecond)
    return line_alg


@check_process
def make_LbToD0PK_D0ToHHHHWS(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hhhh_ws()
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K- D0', 'Lambda_b~0 -> p~- K+ D0'])
    return line_alg


@check_process
def make_LbToD0PKWS_D0ToHHHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hhhh()
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K+ D0', 'Lambda_b~0 -> p~- K- D0'])
    return line_alg


#######################################################################
# Form the Lambda_b (Xi_b0) -> D0 p pi-, D0 --> KShh (LL and DD)
######################################################################
@check_process
def make_LbToD0PPi_D0ToKsLLHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL())
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi- D0', 'Lambda_b~0 -> p~- pi+ D0'])
    return line_alg


@check_process
def make_LbToD0PPi_D0ToKsLLHHWS(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh_ws(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh_ws(
            k_shorts=basic_builder.make_ks_LL())
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi- D0', 'Lambda_b~0 -> p~- pi+ D0'])
    return line_alg


@check_process
def make_LbToD0PPiWS_D0ToKsLLHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL())
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi+ D0', 'Lambda_b~0 -> p~- pi- D0'])
    return line_alg


@check_process
def make_LbToD0PPi_D0ToKsDDHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD())
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi- D0', 'Lambda_b~0 -> p~- pi+ D0'])
    return line_alg


@check_process
def make_LbToD0PPi_D0ToKsDDHHWS(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh_ws(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh_ws(
            k_shorts=basic_builder.make_ks_DD())
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi- D0', 'Lambda_b~0 -> p~- pi+ D0'])
    return line_alg


@check_process
def make_LbToD0PPiWS_D0ToKsDDHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD())
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, pion, dzero],
        descriptors=['Lambda_b0 -> p+ pi+ D0', 'Lambda_b~0 -> p~- pi- D0'])
    return line_alg


#######################################################################
# Form the Lambda_b (Xi_b0) -> D0 p K-, D0 --> KShh (LL and DD)
######################################################################
@check_process
def make_LbToD0PK_D0ToKsLLHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL())
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K- D0', 'Lambda_b~0 -> p~- K+ D0'])
    return line_alg


@check_process
def make_LbToD0PK_D0ToKsLLHHWS(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh_ws(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh_ws(
            k_shorts=basic_builder.make_ks_LL())
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K- D0', 'Lambda_b~0 -> p~- K+ D0'])
    return line_alg


@check_process
def make_LbToD0PKWS_D0ToKsLLHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL())
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K+ D0', 'Lambda_b~0 -> p~- K- D0'])
    return line_alg


@check_process
def make_LbToD0PK_D0ToKsDDHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD())
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K- D0', 'Lambda_b~0 -> p~- K+ D0'])
    return line_alg


@check_process
def make_LbToD0PK_D0ToKsDDHHWS(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh_ws(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh_ws(
            k_shorts=basic_builder.make_ks_DD())
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K- D0', 'Lambda_b~0 -> p~- K+ D0'])
    return line_alg


@check_process
def make_LbToD0PKWS_D0ToKsDDHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD())
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
    line_alg = b_builder.make_lb2chh(
        particles=[proton, kaon, dzero],
        descriptors=['Lambda_b0 -> p+ K+ D0', 'Lambda_b~0 -> p~- K- D0'])
    return line_alg


###########################################################
# Form the Xi_b- (Omega_b-) -> D- p pi-,  D- --> 3h
##########################################################
@check_process
def make_XibmToDmPPi_DmToHHH(process):
    if process == 'spruce':
        d = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        d = d_builder.make_dplus_to_hhh()
        proton = basic_builder.make_tightpid_soft_protons()
        pion = basic_builder.make_tightpid_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_xib2chh(
        particles=[proton, pion, d],
        descriptors=['[Xi_b- -> p+ pi- D-]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the Xi_b- (Omega_b-) -> D- p K-,  D- --> 3h
##########################################################
@check_process
def make_XibmToDmPK_DmToHHH(process):
    if process == 'spruce':
        d = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        d = d_builder.make_dplus_to_hhh()
        proton = basic_builder.make_tightpid_soft_protons()
        kaon = basic_builder.make_tightpid_soft_kaons()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_xib2chh(
        particles=[proton, kaon, d],
        descriptors=['[Xi_b- -> p+ K- D-]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the Xi_b- (Omega_b-) -> Ds- p pi-,  Ds- --> 3h
##########################################################
@check_process
def make_XibmToDsmPPi_DsmToHHH(process):
    if process == 'spruce':
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        ds = d_builder.make_dsplus_to_hhh()
        proton = basic_builder.make_tightpid_soft_protons()
        pion = basic_builder.make_tightpid_soft_pions()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_xib2chh(
        particles=[proton, pion, ds],
        descriptors=['[Xi_b- -> p+ pi- D_s-]cc'],
        sum_pt_min=6 * GeV,
        bpvltime_min=0.3 * picosecond)
    return line_alg


###########################################################
# Form the Xi_b- (Omega_b-) -> Ds- p K-,  Ds- --> 3h
##########################################################
@check_process
def make_XibmToDsmPK_DsmToHHH(process):
    if process == 'spruce':
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        ds = d_builder.make_dsplus_to_hhh()
        proton = basic_builder.make_tightpid_soft_protons()
        kaon = basic_builder.make_tightpid_soft_kaons()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_xib2chh(
        particles=[proton, kaon, ds],
        descriptors=['[Xi_b- -> p+ K- D_s-]cc'],
        sum_pt_min=6 * GeV,
        bpvltime_min=0.3 * picosecond)
    return line_alg


###########################################################
# Form the Xi_b- (Omega_b-) -> D*- p pi-,  D*- --> D0bar pi-
##########################################################
@check_process
def make_XibmToDstmPPi_DstmToD0Pi_D0ToHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(p_pidp_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh()
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
    dstar = d_builder.make_dstar_to_dzeropi(dzero)
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_xib2chh(
        particles=[proton, pion, dstar],
        descriptors=['[Xi_b- -> p+ pi- D*(2010)-]cc'])
    return line_alg


###########################################################
# Form the Xi_b- (Omega_b-) -> D*- p K-,  D*- --> D0bar pi-
##########################################################
@check_process
def make_XibmToDstmPK_DstmToD0Pi_D0ToHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh()
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
    dstar = d_builder.make_dstar_to_dzeropi(dzero)
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. h+h- combination
    line_alg = b_builder.make_xib2chh(
        particles=[proton, kaon, dstar],
        descriptors=['[Xi_b- -> p+ K- D*(2010)-]cc'])
    return line_alg


##########################################################
# Form the Lambda_b0 -> Xi- D0 K+,
# Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-, D0 -> K- pi+ & K- pi- pi+ pi+
# Xi- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_LbToXimD0Kp_D0ToKPiOrKPiPiPi_XimToLambdaLLPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        kaons = basic_builder.make_kaons(k_pidk_min=None)
    if process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
        kaons = basic_builder.make_kaons()
    line_alg = b_builder.make_lb(
        particles=[hyperons, cmeson, kaons],
        descriptors=['Lambda_b0 -> Xi- D0 K+', 'Lambda_b~0 -> Xi~+ D0 K-'])
    return line_alg


@check_process
def make_LbToXimD0Kp_D0ToKPiOrKPiPiPi_XimToLambdaDDPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        kaons = basic_builder.make_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
        kaons = basic_builder.make_kaons()
    line_alg = b_builder.make_lb(
        particles=[hyperons, cmeson, kaons],
        descriptors=['Lambda_b0 -> Xi- D0 K+', 'Lambda_b~0 -> Xi~+ D0 K-'])
    return line_alg


###########################################################
# Form the Xi_b0 -> Xi- D0 pi+,
# Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-, D0 -> K- pi+ & K- pi- pi+ pi+
# Xi- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_Xib0ToXimD0Pip_D0ToKPiOrKPiPiPi_XimToLambdaLLPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        pions = basic_builder.make_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
        pions = basic_builder.make_pions()
    line_alg = b_builder.make_xib(
        particles=[hyperons, cmeson, pions],
        descriptors=['Xi_b0 -> Xi- D0 pi+', 'Xi_b~0 -> Xi~+ D0 pi-'])
    return line_alg


@check_process
def make_Xib0ToXimD0Pip_D0ToKPiOrKPiPiPi_XimToLambdaDDPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        pions = basic_builder.make_pions(pi_pidk_max=None)
    if process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
        pions = basic_builder.make_pions()
    line_alg = b_builder.make_xib(
        particles=[hyperons, cmeson, pions],
        descriptors=['Xi_b0 -> Xi- D0 pi+', 'Xi_b~0 -> Xi~+ D0 pi-'])
    return line_alg
