###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC B2DDh lines
"""
from Hlt2Conf.algorithms import ParticleContainersMerger

from Hlt2Conf.lines.b_to_open_charm.filters import b_sigmanet_filter
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder


@check_process
def make_BdToD0D0Kst_D0ToHHOrHHHH(process, MVACut=0.5):
    if process == 'spruce':
        kst = basic_builder.make_kstar0(am_min=600, am_max=1600)
        dzero_hh = d_builder.make_dzero_to_hh(
            pi_pidk_max=None, k_pidk_min=None)
        dzero_hhhh = d_builder.make_dzero_to_hhhh(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
        dzero_hh = d_builder.make_dzero_to_hh(k_pidk_min=-2)
        dzero_hhhh = d_builder.make_dzero_to_hhhh(k_pidk_min=-2)
    d = ParticleContainersMerger([dzero_hh, dzero_hhhh],
                                 name='B2OCD02HHorHHHHMerger')
    b = b_builder.make_b2cch(
        particles=[d, d, kst],
        descriptors=['B0 -> D0 D0 K*(892)0', 'B0 -> D0 D0 K*(892)~0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)

    return line_alg


@check_process
def make_BdToDstD0K_DstToD0Pi_D0ToHH_D0ToHH(process):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dst = d_builder.make_dstar_to_dzeropi(dzero)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
        dzero = d_builder.make_dzero_to_hh()
        dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dst, dzero, kaon],
        descriptors=['B0 -> D*(2010)- D0 K+', 'B0 -> D*(2010)+ D0 K-'])
    return line_alg


#############################################################################
# Form the Tbc -> D0 D+ pi-, D0 --> Kpi & K3pi, D+ --> Kpipi
##############################################################################


@check_process
def make_TbcToD0DpPim_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        Dp = d_builder.make_dplus_to_kmpippip()
        pion = basic_builder.make_pions()
    if process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        Dp = d_builder.make_tight_dplus_to_kmpippip_for_xibc()
        pion = basic_builder.make_tightpid_tight_pions()
    line_alg = b_builder.make_tbc2ccx(
        particles=[Dz, Dp, pion],
        descriptors=['Xi_bc0 -> D0 D+ pi-', 'Xi_bc~0 -> D0 D- pi+'],
    )
    return line_alg
