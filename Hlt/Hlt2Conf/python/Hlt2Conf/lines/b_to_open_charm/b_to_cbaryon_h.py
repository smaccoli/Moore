###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder


##################################
# Tbc -> Xicc+ p~
# Xicc+ ->
#   ->  Xic0 pi+
#   ->  Lc+ K- pi+
#   ->  D+ p K-
##################################
@check_process
def make_TbcToXiccpP_XiccpToXic0Pi_Xic0ToPKKPi(process):
    xicc = cbaryon_builder.make_xiccp_to_xic0pi()
    proton = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
    line_alg = b_builder.make_tbc2cx(
        particles=[xicc, proton], descriptors=['[Xi_bc0 -> Xi_cc+ p~-]cc'])
    return line_alg


@check_process
def make_TbcToXiccpP_XiccpToLcpKmPip_LcpToPKPi(process):
    xicc = cbaryon_builder.make_xiccp_to_lcpkmpip()
    proton = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
    line_alg = b_builder.make_tbc2cx(
        particles=[xicc, proton], descriptors=['[Xi_bc0 -> Xi_cc+ p~-]cc'])
    return line_alg


@check_process
def make_TbcToXiccpP_XiccpToPDpKm_DpToPipPipKm(process):
    xicc = cbaryon_builder.make_xiccp_to_dppk()
    proton = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
    line_alg = b_builder.make_tbc2cx(
        particles=[xicc, proton], descriptors=['[Xi_bc0 -> Xi_cc+ p~-]cc'])
    return line_alg
