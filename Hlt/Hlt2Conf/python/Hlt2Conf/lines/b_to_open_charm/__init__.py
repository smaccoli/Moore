# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the B2OC selection lines
"""
from . import hlt2_b2oc
from . import spruce_b2oc

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(hlt2_b2oc.hlt2_lines)

sprucing_lines = {}
sprucing_lines.update(spruce_b2oc.sprucing_lines)
