###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC B2DDhh lines
"""
from GaudiKernel.SystemOfUnits import GeV, MeV

import Functors as F

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

#############################################################################
# Form the Tbc -> D0 D0 pi+ pi-, D0 --> Kpi & K3pi
##############################################################################


@check_process
def make_TbcToD0D0PipPim_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        pion = basic_builder.make_soft_pions()
    if process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        pion = basic_builder.make_tightpid_soft_pions()

    ### applying hard cuts on pion if it's not forming D* with any of D0
    Dst_13 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 3]) - F.CHILD(1, F.MASS))
              < 150 * MeV)
    Dst_23 = ((F.SUBCOMB(Functor=F.MASS, Indices=[2, 3]) - F.CHILD(2, F.MASS))
              < 150 * MeV)
    Dst_14 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 4]) - F.CHILD(1, F.MASS))
              < 150 * MeV)
    Dst_24 = ((F.SUBCOMB(Functor=F.MASS, Indices=[2, 4]) - F.CHILD(2, F.MASS))
              < 150 * MeV)

    tight3 = ((F.CHILD(3, F.PT) > 500 * MeV) & (F.CHILD(3, F.P) > 5 * GeV))
    tight4 = ((F.CHILD(4, F.PT) > 500 * MeV) & (F.CHILD(4, F.P) > 5 * GeV))

    comb123_cut_add = (Dst_13 | Dst_23 | tight3)
    comb_cut_add = (Dst_14 | Dst_24 | tight4)

    line_alg = b_builder.make_tbc2ccx(
        particles=[Dz, Dz, pion, pion],
        descriptors=['Xi_bc0 -> D0 D0 pi+ pi-'],
        sum_pt_hbach_min=1.25 * GeV,
        comb123_cut_add=comb123_cut_add,
        comb_cut_add=comb_cut_add,
    )
    return line_alg
