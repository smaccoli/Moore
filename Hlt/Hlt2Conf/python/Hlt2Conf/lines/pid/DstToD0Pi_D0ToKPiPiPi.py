###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for D*(2010)+ -> D0 pi+ with D0 -> K- pi- pi+ pi+.
"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond

from Moore.config import HltLine, register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (
    make_long_kaons,
    make_long_pions,
)

from Hlt2Conf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.pid.utils import constants, filters as flt
from Hlt2Conf.lines.pid.utils.d_builders import make_dst_to_dzeropi

all_lines = {}


def make_dzero_to_kpipipi(
        am_min=constants.D0_M - 85 * MeV,
        am_max=constants.D0_M + 85 * MeV,
        a_p_min=25 * GeV,
        m_min=1800 * MeV,
        m_max=1915 * MeV,
        comb_pt_min=2500 * MeV,
        kpi_p_min=1 * GeV,
        kpi_pt_min=250 * MeV,
        kpi_trchi2_max=99999,
        kpi_mipchi2_min=5,
        doca_max=100 * mm,
        doca_chi2_max=10.0,
        bpvfdchi2_min=50,
        p_min=30 * GeV,
        pt_min=2 * GeV,
        vchi2pdof_max=12,
        bpvltime_min=0.1 * picosecond,
        bpvdira_min=0.9999,
        double_misid_veto=25 * MeV,
        two_particle_veto=1671 * MeV,
        three_particle_veto=1810.5 * MeV,
):
    pvs = make_pvs()
    pions = flt.filter_particles(
        make_long_pions(),
        pvs,
        pt_min=kpi_pt_min,
        p_min=kpi_p_min,
        trchi2_max=kpi_trchi2_max,
        mipchi2_min=kpi_mipchi2_min,
    )
    kaons = flt.filter_particles(
        make_long_kaons(),
        pvs,
        pt_min=kpi_pt_min,
        p_min=kpi_p_min,
        trchi2_max=kpi_trchi2_max,
        mipchi2_min=kpi_mipchi2_min,
    )
    # two_body_combination_code =F.require_all(
    #     F.MAXDOCACUT(doca_max), F.MAXDOCACHI2CUT(doca_chi2_max)
    # )
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.P > a_p_min,
        F.SUM(F.PT) > comb_pt_min,
        F.SUBCOMB(Functor=F.MASS, Indices=(1, 3)) < two_particle_veto,
        F.SUBCOMB(Functor=F.MASS, Indices=(1, 4)) < two_particle_veto,
        F.SUBCOMB(Functor=F.MASS, Indices=(1, 3, 4)) < three_particle_veto,
        F.DOCA(1, 2) < doca_max,
        F.DOCACHI2(1, 2) < doca_chi2_max,
        F.DOCA(1, 3) < doca_max,
        F.DOCACHI2(1, 3) < doca_chi2_max,
        F.DOCA(1, 4) < doca_max,
        F.DOCACHI2(1, 4) < doca_chi2_max,
        F.DOCA(2, 3) < doca_max,
        F.DOCACHI2(2, 3) < doca_chi2_max,
        F.DOCA(2, 4) < doca_max,
        F.DOCACHI2(2, 4) < doca_chi2_max,
        F.DOCA(3, 4) < doca_max,
        F.DOCACHI2(3, 4) < doca_chi2_max,
    )
    composite_code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        in_range(m_min, F.MASS, m_max),
        (F.MASSWITHHYPOTHESES(
            ("pi-", "pi-", "pi+", "K+")) < constants.D0_M - double_misid_veto)
        | (F.MASSWITHHYPOTHESES(
            ("pi-", "pi-", "pi+", "K+")) > constants.D0_M + double_misid_veto),
        (F.MASSWITHHYPOTHESES(
            ("pi-", "pi-", "K+", "pi+")) < constants.D0_M - double_misid_veto)
        | (F.MASSWITHHYPOTHESES(
            ("pi-", "pi-", "K+", "pi+")) > constants.D0_M + double_misid_veto),
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVLTIME(pvs) > bpvltime_min,
    )

    return ParticleCombiner(
        [kaons, pions, pions, pions],
        name='PID_D0ToKPiPiPi_Combiner_{hash}',
        DecayDescriptor="[D0 -> K- pi- pi+ pi+]cc",
        # Combination12Cut=two_body_combination_code,
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def DstToD0Pi_D0ToKPiPiPi(name="Hlt2PID_DstToD0Pi_D0ToKPiPiPi", prescale=0.1):
    pvs = make_pvs()
    slow_pions = flt.filter_particles(
        make_long_pions(),
        pvs,
        pt_min=0.1 * GeV,
        p_min=1.0 * GeV,
        trchi2_max=99999,
        mipchi2_min=0.0,
    )
    d0s = make_dzero_to_kpipipi()
    dsts = make_dst_to_dzeropi(d0s, slow_pions)

    return HltLine(
        name=name,
        algs=flt.pid_prefilters() + [dsts],
        prescale=prescale,
        persistreco=True,
    )
