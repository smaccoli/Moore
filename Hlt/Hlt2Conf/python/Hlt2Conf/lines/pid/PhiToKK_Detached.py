###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for phi -> K+ K-.
"""
import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV

from Moore.config import HltLine, register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (
    make_long_kaons,
    make_has_rich_long_kaons,
)

from Hlt2Conf.algorithms_thor import (
    ParticleFilter,
    ParticleCombiner,
)

from Hlt2Conf.lines.pid.utils import constants, filters as flt

all_lines = {}


def make_tag_kaons(
        particles,
        pvs,
        charge,
        trchi2dof_max=99999,
        trghostprob_max=0.2,
        p_min=3 * GeV,
        pt_min=0.2 * GeV,
        mipchi2dv_min=16.0,
        dllk_min=0.0,
):
    code = F.require_all(
        F.CHARGE > 0 if charge > 0 else F.CHARGE < 0,
        F.CHI2DOF < trchi2dof_max,
        F.GHOSTPROB < trghostprob_max,
        F.P > p_min,
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2dv_min,
        F.PID_K > dllk_min,
    )
    return ParticleFilter(particles, F.FILTER(code))


def make_probe_kaons(
        particles,
        pvs,
        charge,
        trchi2dof_max=99999,
        p_min=3 * GeV,
        pt_min=0 * MeV,
        mipchi2dv_min=9.0,
):
    code = F.require_all(
        F.CHARGE > 0 if charge > 0 else F.CHARGE < 0,
        F.CHI2DOF < trchi2dof_max,
        F.P > p_min,
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2dv_min,
    )
    return ParticleFilter(particles, F.FILTER(code))


def make_phis_tagged(
        positive_kaons,
        negative_kaons,
        am_min=constants.PHI_M - 40 * MeV,
        am_max=constants.PHI_M + 40 * MeV,
        m_min=constants.PHI_M - 20 * MeV,
        m_max=constants.PHI_M + 20 * MeV,
        achi2doca_max=15.0,
        vchi2_max=15.0,
        bpvfdchi2_min=150.0,
        mipchi2_min=25.0,
        bpvdira=0.99,
        pt_min=200 * MeV,
):
    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.DOCACHI2(1, 2) < achi2doca_max,
    )

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2 < vchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.BPVDIRA(pvs) > bpvdira,
        F.PT > pt_min,
    )

    return ParticleCombiner(
        [positive_kaons, negative_kaons],
        name='PID_PhiToKKTagged_Combiner_{hash}',
        DecayDescriptor="phi(1020) -> K+ K-",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_phis_unbiased(
        kaons,
        am_min=constants.PHI_M - 40 * MeV,
        am_max=constants.PHI_M + 40 * MeV,
        m_min=constants.PHI_M - 20 * MeV,
        m_max=constants.PHI_M + 20 * MeV,
        achi2doca_max=15.0,
        vchi2_max=15.0,
        bpvfdchi2_min=150.0,
        mipchi2_min=25.0,
        bpvdira=0.995,
        pt_min=800 * MeV,
        comb_pt_min=200 * MeV,
        single_child_mipchi2_min=40.0 * MeV,
        single_child_pt_min=200 * MeV,
):
    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.DOCACHI2(1, 2) < achi2doca_max,
        F.SUM(F.PT) > comb_pt_min,
        F.CHILD(
            Index=1,
            Functor=F.MINIPCHI2CUT(
                IPChi2Cut=single_child_mipchi2_min, Vertices=pvs),
        )
        | F.CHILD(
            Index=2,
            Functor=F.MINIPCHI2CUT(
                IPChi2Cut=single_child_mipchi2_min, Vertices=pvs),
        ),
        F.CHILD(Index=1, Functor=F.PT > single_child_pt_min)
        | F.CHILD(Index=2, Functor=F.PT > single_child_pt_min),
    )

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2 < vchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.BPVDIRA(pvs) > bpvdira,
        F.PT > pt_min,
    )

    return ParticleCombiner(
        [kaons, kaons],
        name='PID_PhiToKKUnbiased_Combiner_{hash}',
        DecayDescriptor="phi(1020) -> K+ K-",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def PhiToKmKpTagged_Detached(name="Hlt2PID_PhiToKmKpTagged_Detached",
                             prescale=0.005):
    pvs = make_pvs()
    tag_kaons = make_tag_kaons(make_has_rich_long_kaons(), pvs, +1)
    probe_kaons = make_probe_kaons(make_long_kaons(), pvs, -1)
    phis = make_phis_tagged(
        positive_kaons=tag_kaons, negative_kaons=probe_kaons)
    return HltLine(
        name=name,
        algs=flt.pid_prefilters() + [phis],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def PhiToKpKmTagged_Detached(name="Hlt2PID_PhiToKpKmTagged_Detached",
                             prescale=0.005):
    pvs = make_pvs()
    tag_kaons = make_tag_kaons(make_has_rich_long_kaons(), pvs, -1)
    probe_kaons = make_probe_kaons(make_long_kaons(), pvs, +1)
    phis = make_phis_tagged(
        positive_kaons=probe_kaons, negative_kaons=tag_kaons)
    return HltLine(
        name=name,
        algs=flt.pid_prefilters() + [phis],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def PhiToKK_Unbiased_Detached(name="Hlt2PID_PhiToKK_Unbiased_Detached",
                              prescale=0.001):
    pvs = make_pvs()
    kaons = ParticleFilter(make_long_kaons(), F.FILTER(F.MINIPCHI2(pvs) > 16))
    phis = make_phis_unbiased(kaons=kaons)
    return HltLine(
        name=name,
        algs=flt.pid_prefilters() + [phis],
        prescale=prescale,
        persistreco=True,
    )
