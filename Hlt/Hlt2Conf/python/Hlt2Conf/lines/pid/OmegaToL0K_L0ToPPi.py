###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 PID line for Omega- -> Lambda_0 K-, Lambda_0 -> p pi- LL and DD.
"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns

from Moore.config import HltLine
# from Moore.config import register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (
    make_long_pions,
    make_down_pions,
    make_long_kaons,
    make_down_kaons,
    make_long_protons,
    make_down_protons,
)
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

from Hlt2Conf.lines.pid.utils import constants, filters as flt

all_lines = {}


def filter_long_kaons(particles, pvs, mipchi2_min=16):
    code = F.require_all(F.MINIPCHI2(pvs) > mipchi2_min)
    return ParticleFilter(particles, F.FILTER(code))


def make_l0s(
        protons,
        pions,
        pvs,
        am_min=constants.LAMBDA_M - 50.0 * MeV,
        am_max=constants.LAMBDA_M + 50.0 * MeV,
        m_min=1110.0 * MeV,
        m_max=1120.0 * MeV,
        bpvvdz_min=-100 * mm,
        bpvvdz_max=500 * mm,
        bpvltime_min=0.002 * ns,
        vchi2_max=30,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        in_range(bpvvdz_min, F.BPVVDZ(pvs), bpvvdz_max),
        F.CHI2 < vchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
    )

    return ParticleCombiner(
        [protons, pions],
        name='PID_Lambda0_Combiner_{hash}',
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_omegas(
        l0s,
        kaons,
        pvs,
        am_min=constants.OMEGA_M - 42.5 * MeV,
        am_max=constants.OMEGA_M + 42.5 * MeV,
        apt_min=0.5 * GeV,
        m_min=1640.0 * MeV,
        m_max=1705.0 * MeV,
        bpvipchi2_max=25.0,
        vchi2_max=10.0,
        bpvdira_min=0.9994,
        bpvvdchi2_min=10,
        bpvltime_min=0.002 * ns,
        mipchi2dv_max=25,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > apt_min)

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2 < vchi2_max,
        F.MINIPCHI2(pvs) < mipchi2dv_max if mipchi2dv_max else F.ALL,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min,
    )

    return ParticleCombiner(
        [l0s, kaons],
        name='PID_Omega0ToLambda0K_Combiner_{hash}',
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


# @register_line_builder(all_lines)
def OmegaToL0K_L0ToPPi_LLL(name="Hlt2PID_OmegaToL0K_L0ToPPi_LLL", prescale=1):
    pvs = make_pvs()
    protons = flt.filter_long_particles(
        make_long_protons(), pvs, trchi2_max=99999, mipchi2_min=36)
    pions = flt.filter_long_particles(
        make_long_pions(), pvs, trchi2_max=99999, mipchi2_min=36)
    kaons = filter_long_kaons(make_long_kaons(), pvs)
    l0lls = make_l0s(protons, pions, pvs)
    omegallls = make_omegas(l0lls, kaons, pvs)

    return HltLine(
        name=name,
        algs=flt.pid_prefilters() + [omegallls],
        prescale=prescale,
        persistreco=True,
    )


# @register_line_builder(all_lines)
def OmegaToL0K_L0ToPPi_DDD(name="Hlt2PID_OmegaToL0K_L0ToPPi_DDD", prescale=1):
    pvs = make_pvs()
    protons = flt.filter_down_particles(
        make_down_protons(),
        p_min=3.0 * GeV,
        pt_min=0.175 * GeV,
        trchi2_max=99999,
    )
    pions = flt.filter_down_particles(
        make_down_pions(),
        p_min=3.0 * GeV,
        pt_min=0.175 * GeV,
        trchi2_max=99999,
    )
    kaons = make_down_kaons()
    l0dds = make_l0s(
        protons,
        pions,
        pvs,
        am_min=constants.LAMBDA_M - 80.0 * MeV,
        am_max=constants.LAMBDA_M + 80.0 * MeV,
        bpvvdz_min=300 * mm,
        bpvvdz_max=2275 * mm,
        bpvltime_min=0.0045 * ns)
    omegaddds = make_omegas(l0dds, kaons, pvs, mipchi2dv_max=None)

    return HltLine(
        name=name,
        algs=flt.pid_prefilters() + [omegaddds],
        prescale=prescale,
        persistreco=True,
    )
