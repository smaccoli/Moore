###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for J/psi -> mu+ mu-.
"""
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (
    make_long_muons,
    make_ismuon_long_muon,
)

from Hlt2Conf.lines.pid.utils.charmonium import (
    make_tag_muons,
    make_probe_muons,
    make_jpsis,
    make_detached_jpsis,
)

from Hlt2Conf.lines.pid.utils.filters import pid_prefilters

all_lines = {}


@register_line_builder(all_lines)
def JpsiToMuMumTagged_Detached(name="Hlt2PID_JpsiToMuMumTagged_Detached",
                               prescale=1):
    pvs = make_pvs()
    tag_muons = make_tag_muons(make_ismuon_long_muon(), pvs, charge=-1)
    probe_muons = make_probe_muons(make_long_muons(), pvs, charge=+1)
    jpsis = make_jpsis(mu_neg=tag_muons, mu_pos=probe_muons)
    detached_jpsis = make_detached_jpsis(jpsis, pvs)
    return Hlt2Line(
        name=name,
        algs=pid_prefilters() + [detached_jpsis],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def JpsiToMuMupTagged_Detached(name="Hlt2PID_JpsiToMuMupTagged_Detached",
                               prescale=1):
    pvs = make_pvs()
    tag_muons = make_tag_muons(make_ismuon_long_muon(), pvs, charge=+1)
    probe_muons = make_probe_muons(make_long_muons(), pvs, charge=-1)
    jpsis = make_jpsis(mu_pos=tag_muons, mu_neg=probe_muons)
    detached_jpsis = make_detached_jpsis(jpsis, pvs)
    return Hlt2Line(
        name=name,
        algs=pid_prefilters() + [detached_jpsis],
        prescale=prescale,
        persistreco=True,
    )
