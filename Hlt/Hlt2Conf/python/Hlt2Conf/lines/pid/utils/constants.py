###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define useful constants shared among PID lines
"""
from GaudiKernel.SystemOfUnits import MeV

BP_M = 5279.34 * MeV  # +/- 0.12, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
D0_M = 1864.84 * MeV  # +/- 0.05, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
DP_M = 1869.66 * MeV  # +/- 0.05, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
DSTAR_M = 2010.26 * MeV  # +/- 0.05, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
DS_M = 1968.35 * MeV  # +/- 0.07, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
JPSI_M = 3096.900 * MeV  # +/- 0.006, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
K0_M = 497.611 * MeV  # +/- 0.013, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
LAMBDAB_M = 5619.60 * MeV  # +/- 0.17, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
LAMBDAC_M = 2286.46 * MeV  # +/- 0.14, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
LAMBDA_M = 1115.683 * MeV  # +/- 0.006, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
OMEGA_M = 1672.45 * MeV  # +/- 0.29, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
PHI_M = 1019.461 * MeV  # +/- 0.016, PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
