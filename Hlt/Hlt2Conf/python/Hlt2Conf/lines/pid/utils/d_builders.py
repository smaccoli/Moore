###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of detached D/D0/Ds/... meson decays that are shared between many
PID selections, and therefore are defined centrally.
"""
from GaudiKernel.SystemOfUnits import MeV

import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleCombiner

from PyConf import configurable

from Hlt2Conf.lines.pid.utils import constants

###############################################################################
# Specific D decay builders, overrides default cuts where needed              #
###############################################################################


@configurable
def make_dst_to_dzeropi(
        d0s,
        pions,
        comb_m_min=constants.DSTAR_M - 112.5 * MeV,
        comb_m_max=constants.DSTAR_M + 112.5 * MeV,
        m_min=constants.DSTAR_M - 95 * MeV,
        m_max=constants.DSTAR_M + 95 * MeV,
        delta_mm_min=135 * MeV,
        delta_mm_max=175 * MeV,
        vchi2pdof_max=10,
):
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MASS - F.CHILD(1, F.MASS) > delta_mm_min,
        F.MASS - F.CHILD(1, F.MASS) < delta_mm_max,
    )

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vchi2pdof_max)

    return ParticleCombiner(
        [d0s, pions],
        name='PID_DstToD0Pi_Combiner_{hash}',
        DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
