###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of detached charm baryon decays that are shared between several PID
selections.
"""
from GaudiKernel.SystemOfUnits import MeV, GeV, ns

import Functors as F
from Functors.math import in_range

from PyConf import configurable
from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions,
    make_has_rich_long_kaons,
    make_long_protons,
)
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.pid.utils import constants, filters as flt

###############################################################################
# Specific charm baryon decay builders, overrides default cuts where needed   #
###############################################################################


@configurable
def make_lc_to_pkpi(
        m_min=constants.LAMBDAC_M - 75 * MeV,
        m_max=constants.LAMBDAC_M + 75 * MeV,
        mm_min=constants.LAMBDAC_M - 85 * MeV,
        mm_max=constants.LAMBDAC_M + 85 * MeV,
        ds_veto_window=30 * MeV,
        dp_veto_window=30 * MeV,
        pi0_veto_min=1740 * MeV,
        comb_pt_min=2000 * MeV,
        vchi2pdof_max=10,
        bpvfdchi2_min=50,
        bpvltime_min=0.0001 * ns,
        # bpvdira_min=0,
        mipchi2_max=None,
        two_child_minipchi2_min=12.0,
        one_child_minipchi2_min=16.0,
        k_dllk_min=6,
        pi_dllk_max=-6,
):
    """
    Return a Lambda_c+ -> p K- pi+ candidates.
    """
    pvs = make_pvs()
    protons = flt.filter_particles_ghostprob_max(
        flt.filter_particles(
            make_long_protons(),
            pvs=pvs,
            pt_min=1.0 * GeV,
            p_min=1.0 * GeV,
            trchi2_max=99999,
            mipchi2_min=6,
        ),
        ghostprob_max=1e9,
    )

    kaons = flt.filter_particles_ghostprob_max(
        flt.filter_particles_dll(
            flt.filter_particles(
                make_has_rich_long_kaons(),
                pvs=pvs,
                pt_min=0.4 * GeV,
                p_min=1.0 * GeV,
                trchi2_max=99999,
                mipchi2_min=9,
            ),
            dll_type="k",
            dll_lim=k_dllk_min,
            min=True,
        ),
        ghostprob_max=1e9,
    )

    pions = flt.filter_particles_ghostprob_max(
        flt.filter_particles_dll(
            flt.filter_particles(
                make_has_rich_long_pions(),
                pvs=pvs,
                pt_min=0.4 * GeV,
                p_min=1.0 * GeV,
                trchi2_max=99999,
                mipchi2_min=9,
            ),
            dll_type="k",
            dll_lim=pi_dllk_max,
            min=False,
        ),
        ghostprob_max=1e9,
    )

    combination_code = F.require_all(
        F.SUM(F.PT) > comb_pt_min,
        in_range(mm_min, F.MASS, mm_max),
        (F.CHILD(
            Index=1,
            Functor=F.MINIPCHI2CUT(
                IPChi2Cut=two_child_minipchi2_min, Vertices=pvs))
         & F.CHILD(
             Index=2,
             Functor=F.MINIPCHI2CUT(
                 IPChi2Cut=two_child_minipchi2_min, Vertices=pvs)))
        | (F.CHILD(
            Index=1,
            Functor=F.MINIPCHI2CUT(
                IPChi2Cut=two_child_minipchi2_min, Vertices=pvs))
           & F.CHILD(
               Index=3,
               Functor=F.MINIPCHI2CUT(
                   IPChi2Cut=two_child_minipchi2_min, Vertices=pvs)))
        | (F.CHILD(
            Index=2,
            Functor=F.MINIPCHI2CUT(
                IPChi2Cut=two_child_minipchi2_min, Vertices=pvs))
           & F.CHILD(
               Index=3,
               Functor=F.MINIPCHI2CUT(
                   IPChi2Cut=two_child_minipchi2_min, Vertices=pvs))),
        F.CHILD(
            Index=1,
            Functor=F.MINIPCHI2CUT(
                IPChi2Cut=one_child_minipchi2_min, Vertices=pvs))
        | F.CHILD(
            Index=2,
            Functor=F.MINIPCHI2CUT(
                IPChi2Cut=one_child_minipchi2_min, Vertices=pvs))
        | F.CHILD(
            Index=3,
            Functor=F.MINIPCHI2CUT(
                IPChi2Cut=one_child_minipchi2_min, Vertices=pvs)),
    )

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vchi2pdof_max,
        F.MASSWITHHYPOTHESES(("pi+", "K-", "pi-")) > pi0_veto_min,
        (F.MASSWITHHYPOTHESES(
            ("K+", "K-", "pi-")) < constants.DS_M - ds_veto_window)
        | (F.MASSWITHHYPOTHESES(
            ("K+", "K-", "pi-")) > constants.DS_M + ds_veto_window),
        (F.MASSWITHHYPOTHESES(
            ("pi+", "K-", "pi-")) < constants.DP_M - dp_veto_window)
        | (F.MASSWITHHYPOTHESES(
            ("pi+", "K-", "pi-")) > constants.DP_M + dp_veto_window),
        F.MINIPCHI2(pvs) < mipchi2_max if mipchi2_max else F.ALL,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        # F.BPVDIRA(pvs) > bpvdira_min,
        # TODO: Add Hlt1(Two)?TrackMVADecision TOS filter
    )
    return ParticleCombiner(
        [protons, kaons, pions],
        name='PID_LcToPKPi_Combiner_{hash}',
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )
