###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 PID line for Lambda_b0 -> Lambda_c+ pi-.
"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV

from Moore.config import HltLine, register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import make_long_pions
from Hlt2Conf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.pid.utils import (
    cbaryon_builders,
    constants,
    filters as flt,
)

all_lines = {}


def make_lambdabs(
        lambdacs,
        pions,
        pvs,
        mm_min=constants.LAMBDAB_M - 350 * MeV,
        mm_max=constants.LAMBDAB_M + 350 * MeV,
        m_min=constants.LAMBDAB_M - 300 * MeV,
        m_max=constants.LAMBDAB_M + 300 * MeV,
        comb_pt_min=3000 * MeV,
        vchi2_max=25,
        bpvfdchi2_min=100,
        bpvdira_min=0.9999,
        mipchi2_max=10,
):
    combination_code = F.require_all(
        in_range(mm_min, F.MASS, mm_max),
        F.SUM(F.PT) > comb_pt_min)

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2 < vchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.MINIPCHI2(pvs) < mipchi2_max,
        F.BPVDIRA(pvs) > bpvdira_min,
    )

    return ParticleCombiner(
        [lambdacs, pions],
        name='PID_Lambdab0ToLambdacPi_Combiner_{hash}',
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def LbToLcPi_LcToPKPi(name="Hlt2PID_LbToLcPi_LcToPKPi", prescale=1):
    pvs = make_pvs()
    lambdacs = cbaryon_builders.make_lc_to_pkpi()

    pions = flt.filter_particles_dll(
        flt.filter_particles(
            make_long_pions(),
            pvs,
            pt_min=0.5 * GeV,
            p_min=3.0 * GeV,
            trchi2_max=99999,
            mipchi2_min=25,
        ),
        dll_type="k",
        dll_lim=-5,
        min=False,
    )
    lambdabs = make_lambdabs(lambdacs, pions, pvs)

    return HltLine(
        name=name,
        algs=flt.pid_prefilters() + [lambdabs],
        prescale=prescale,
        persistreco=True,
    )
