###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 PID line for Lambda_b0 -> Lambda_c+ mu-.
"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV

from Moore.config import HltLine, register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import make_ismuon_long_muon
from Hlt2Conf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.pid.utils import (
    constants,
    cbaryon_builders,
    filters as flt,
)

all_lines = {}


def make_lambdabs(
        lambdacs,
        muons,
        pvs,
        mm_min=0 * MeV,
        mm_max=6000 * MeV,
        m_min=constants.LAMBDAB_M - 750 * MeV,
        m_max=constants.LAMBDAB_M + 750 * MeV,
        comb_pt_min=3000 * MeV,
        vchi2_max=25,
        bpvfdchi2_min=100,
        mipchi2_max=200,
        bpvdira_min=0.99,
):
    combination_code = F.require_all(
        in_range(mm_min, F.MASS, mm_max),
        F.SUM(F.PT) > comb_pt_min)

    composite_code = F.require_all(
        in_range(m_min, F.BPVCORRM(pvs), m_max),
        F.CHI2 < vchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.MINIPCHI2(pvs) < mipchi2_max,
        F.BPVDIRA(pvs) > bpvdira_min,
    )

    return ParticleCombiner(
        [lambdacs, muons],
        name='PID_Lambdab0ToLambdacMu_Combiner_{hash}',
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ mu-]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def LbToLcMuNu_LcToPKPi(name="Hlt2PID_LbToLcMuNu_LcToPKPi", prescale=1):
    pvs = make_pvs()
    lambdacs = cbaryon_builders.make_lc_to_pkpi()

    muons = flt.filter_particles_dll(
        flt.filter_particles(
            make_ismuon_long_muon(),
            pvs,
            pt_min=0.5 * GeV,
            p_min=3.0 * GeV,
            trchi2_max=99999,
            mipchi2_min=16,
        ),
        dll_lim=0,
        dll_type="mu",
        min=True,
    )
    lambdabs = make_lambdabs(lambdacs, muons, pvs)

    return HltLine(
        name=name,
        algs=flt.pid_prefilters() + [lambdabs],
        prescale=prescale,
        persistreco=True,
    )
