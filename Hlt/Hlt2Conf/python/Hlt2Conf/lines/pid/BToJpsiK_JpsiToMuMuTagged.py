###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for B+ -> (J/psi -> mu+ mu-) K+.
"""
import Functors as F

from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV

from Moore.lines import Hlt2Line
from Moore.config import register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (
    make_long_muons,
    make_ismuon_long_muon,
    make_long_kaons,
)

from Hlt2Conf.algorithms_thor import (
    ParticleFilter,
    ParticleCombiner,
)

from Hlt2Conf.lines.pid.utils import constants, filters

from Hlt2Conf.lines.pid.utils.charmonium import (
    make_tag_muons,
    make_probe_muons,
    make_jpsis,
    make_detached_jpsis,
)

all_lines = {}


def make_kaons(
        particles,
        pvs,
        p_min=3 * GeV,
        pt_min=0.3 * GeV,
        mipchi2dv_min=9.0,
        dllkpi_min=5.0,
):
    code = F.require_all(
        F.P > p_min,
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2dv_min,
        F.PID_K > dllkpi_min,
    )
    return ParticleFilter(particles, F.FILTER(code))


def make_bplus(
        detached_jpsis,
        kaons,
        pvs,
        am_min=constants.BP_M - 520 * MeV,
        am_max=constants.BP_M + 520 * MeV,
        m_min=constants.BP_M - 500 * MeV,
        m_max=constants.BP_M + 500 * MeV,
        comb_pt_min=2000 * MeV,
        vchi2pdof_max=25,
        mipchi2_max=25,
        bpvfdchi2_min=15,
        bpvdira_min=0.9999,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > comb_pt_min)
    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2 < vchi2pdof_max,
        F.MINIPCHI2(pvs) < mipchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
    )

    return ParticleCombiner(
        [detached_jpsis, kaons],
        name='PID_BToJpsiK_Combiner_{hash}',
        DecayDescriptor="[B+ -> J/psi(1S) K+]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def BToJpsiK_JpsiToMuMupTagged_Detached(
        name="Hlt2PID_BToJpsiK_JpsiToMuMupTagged_Detached", prescale=1):
    pvs = make_pvs()
    tag_muons = make_tag_muons(make_ismuon_long_muon(), pvs, +1)
    probe_muons = make_probe_muons(make_long_muons(), pvs, -1)
    jpsis = make_jpsis(mu_neg=tag_muons, mu_pos=probe_muons)
    detached_jpsis = make_detached_jpsis(jpsis, pvs)
    kaons = make_kaons(make_long_kaons(), pvs)
    bplus = make_bplus(detached_jpsis, kaons, pvs)
    return Hlt2Line(
        name=name,
        algs=filters.pid_prefilters() + [jpsis, bplus],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def BToJpsiK_JpsiToMuMumTagged_Detached(
        name="Hlt2PID_BToJpsiK_JpsiToMuMumTagged_Detached", prescale=1):
    pvs = make_pvs()
    tag_muons = make_tag_muons(make_ismuon_long_muon(), pvs, -1)
    probe_muons = make_probe_muons(make_long_muons(), pvs, +1)
    jpsis = make_jpsis(mu_pos=tag_muons, mu_neg=probe_muons)
    detached_jpsis = make_detached_jpsis(jpsis, pvs)
    kaons = make_kaons(make_long_kaons(), pvs)
    bplus = make_bplus(detached_jpsis, kaons, pvs)
    return Hlt2Line(
        name=name,
        algs=filters.pid_prefilters() + [jpsis, bplus],
        prescale=prescale,
        persistreco=True,
    )
