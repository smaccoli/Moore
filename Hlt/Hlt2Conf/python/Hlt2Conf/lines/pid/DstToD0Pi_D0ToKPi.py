###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for D*(2010)+ -> D0 pi+ with D0 -> K- pi+.
"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import HltLine, register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (
    make_long_kaons,
    make_long_pions,
)

from Hlt2Conf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.pid.utils import constants, filters as flt
from Hlt2Conf.lines.pid.utils.d_builders import make_dst_to_dzeropi

all_lines = {}


def make_d0s(
        kaons,
        pions,
        pvs,
        comb_m_min=constants.D0_M - 95.0 * MeV,
        comb_m_max=constants.D0_M + 95.0 * MeV,
        apt_min=1500 * MeV,
        single_pt_min=1000 * MeV,
        comb_doca_max=0.1 * mm,
        m_min=constants.D0_M - 75 * MeV,
        m_max=constants.D0_M + 75 * MeV,
        vchi2pdof_max=10,
        bpvfdchi2_min=49,
        bpvdira_min=0.9999,
        double_misid_veto=25 * MeV,
        single_misid_veto=25 * MeV,
):
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.PT > apt_min,
        F.MAXDOCACUT(comb_doca_max),
    )

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        (F.MASSWITHHYPOTHESES(
            ("pi-", "K+")) < constants.D0_M - double_misid_veto)
        | (F.MASSWITHHYPOTHESES(
            ("pi-", "K+")) > constants.D0_M + double_misid_veto),
        (F.MASSWITHHYPOTHESES(
            ("pi-", "pi+")) < constants.D0_M - single_misid_veto)
        | (F.MASSWITHHYPOTHESES(
            ("pi-", "pi+")) > constants.D0_M + single_misid_veto),
        (F.CHILD(1, F.PT) > single_pt_min) |
        (F.CHILD(2, F.PT) > single_pt_min),
    )

    return ParticleCombiner(
        [kaons, pions],
        name='PID_D0ToKPi_Combiner_{hash}',
        DecayDescriptor="[D0 -> K- pi+]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@register_line_builder(all_lines)
def DstToD0Pi_D0ToKPi(name="Hlt2PID_DstToD0Pi_D0ToKPi", prescale=1):
    pvs = make_pvs()
    kaons = flt.filter_particles(
        make_long_kaons(),
        pvs,
        pt_min=0.25 * GeV,
        p_min=2.0 * GeV,
        trchi2_max=99999,
        mipchi2_min=16,
    )
    pions = flt.filter_particles(
        make_long_pions(),
        pvs,
        pt_min=0.25 * GeV,
        p_min=2.0 * GeV,
        trchi2_max=99999,
        mipchi2_min=16,
    )
    slow_pions = flt.filter_particles(
        make_long_pions(),
        pvs,
        pt_min=0.1 * GeV,
        p_min=1.0 * GeV,
        trchi2_max=99999,
        mipchi2_min=0.0,
    )
    d0s = make_d0s(kaons, pions, pvs)
    dsts = make_dst_to_dzeropi(d0s, slow_pions)

    return HltLine(
        name=name,
        algs=flt.pid_prefilters() + [dsts],
        prescale=prescale,
        persistreco=True,
    )
