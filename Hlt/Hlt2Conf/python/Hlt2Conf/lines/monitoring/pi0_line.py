###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Functors as F
from RecoConf.reconstruction_objects import upfront_reconstruction
from PyConf.Algorithms import Monitor__ParticleRange
from PyConf.tonic import configurable
from Hlt2Conf.standard_particles import make_photons
from .pi0_builders_thor import filter_photons, make_pi0
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

all_lines = {}


@configurable
def pi0_monitoring(name,
                   photon_et=250,
                   photon_E19=0.7,
                   pi0_et=1400,
                   max_mass=250,
                   bins=50):
    photons = filter_photons(
        make_photons(), pt_min=photon_et, E19_min=photon_E19)
    pi0s = make_pi0([photons, photons],
                    "pi0 -> gamma gamma",
                    pt_min=pi0_et,
                    m_max=max_mass)

    checkPi0_M = Monitor__ParticleRange(
        name=f"Monitor_{name}Pi0Monitor",
        Variable=F.MASS,
        Input=pi0s,
        Range=(0, max_mass),
        Bins=bins)
    return [checkPi0_M, pi0s]


@register_line_builder(all_lines)
def monitoring_pi0_line(name="Hlt2MonitoringPi0", prescale=1e-6):
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + pi0_monitoring(name),
        prescale=prescale,
        monitoring_variables=(),
    )
