###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiKernel.SystemOfUnits import MeV
import Functors as F
from PyConf.tonic import configurable

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner


@configurable
def filter_photons(particles, pt_min=250 * MeV, E19_min=0):
    """Returns photons for monitoring"""
    code = F.require_all(F.PT > pt_min,
                         F.CALO_NEUTRAL_1TO9_ENERGY_RATIO > E19_min)
    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_pi0(photons, descriptors, pt_min=1400 * MeV, m_max=300 * MeV):
    """Builds pi0s for monitoring"""
    combination_code = F.require_all(
        F.PT > pt_min,
        F.MASS < m_max,
    )

    vertex_code = F.require_all(F.ALL)

    return ParticleCombiner(
        photons,
        ParticleCombiner="ParticleAdder",
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
