###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Initial versions of 2- and 3-body topological lines.

MVAs trained with VLoose HLT1 configuration, working points tuned by eye.

Functions `make_filtered_topo_twobody` and `make_unfiltered_topo_twobody`
return two body combinations with and without an MVA cut, for use in other
selections.
"""
import Functors as F
import Functors.math as fmath

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import HltLine, register_line_builder
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from PyConf import configurable
from PyConf.control_flow import CompositeNode, NodeLogic
from ...standard_particles import make_has_rich_long_pions, make_ismuon_long_muon
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.algorithms import ParticleContainersMerger

all_lines = {}


@configurable
def make_input_particles(
        make_particles=make_has_rich_long_pions,
        make_pvs=make_pvs,
        name="Topo_has_rich_long_pion_{hash}",
        mipchi2_min=4,
        pt_min=0 * MeV,
        p_min=0 * GeV,
):
    """Return particles used as input to all topo combiners."""
    pvs = make_pvs()
    code = F.require_all(F.PT > pt_min, F.P > p_min,
                         F.MINIPCHI2(pvs) > mipchi2_min)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_two_body_particles(
        particles,
        descriptors,
        name="Topo_TwoBody_Combiner_{hash}",
        amaxchild_pt_min=1500 * MeV,
        apt_min=1000 * MeV,
        amindoca_max=0.3 * mm,
        vchi2pdof_max=10,
        bpvvdchi2_min=16,
        dira_min=0,
        pvs=make_pvs,
):
    """Return two-body vertices filtered by some rectangular cuts.

    Parameters
    ----------
    particles : DataHandle
        Input Particle container to use in the combination.
    descriptors : list of str
        Decay descriptors used to define the decay tree.
    pvs : DataHandle
        Container of primary vertices.
    """

    pvs = make_pvs()
    combination_code = F.require_all(
        F.MAX(F.PT) > amaxchild_pt_min, F.PT > apt_min,
        F.MAXDOCACUT(amindoca_max))

    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVDIRA(pvs) > dira_min,
    )
    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_three_body_particles(
        particles,
        descriptors,
        name="Topo_ThreeBody_Combiner_{hash}",
        amaxchild_pt_min=1500 * MeV,
        apt_min=2000 * MeV,
        amindoca_max=0.3 * mm,  # NOTE:TO BE ADJUSTED
        bpvvdchi2_min=16,
        dira_min=0,
        pvs=make_pvs,
):
    """Return three-body vertices filtered by some rectangular cuts.

    Parameters
    ----------
    particles : DataHandle
        Input Particle container to use in the combination.
    descriptors : list of str
        Decay descriptors used to define the decay tree.
    pvs : DataHandle
        Container of primary vertices.
    """
    combination_code = F.require_all(
        F.MAX(F.PT) > amaxchild_pt_min, F.PT > apt_min,
        F.MAXDOCACUT(amindoca_max))
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVDIRA(pvs) > dira_min)

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_filtered_two_body_particles(particles, MVACut, pvs=make_pvs):
    pvs = make_pvs()
    mva = (
        F.MVA(
            MVAType="SigmaNet",
            Config={
                "File":
                "paramfile://data/TwoBody_topo_sigmanet_upgrade.json",
                "Name":
                "Topo_TwoBody_SigmaNet",
                "Lambda":
                "1.75",
                "NLayers":
                "4",
                "InputSize":
                "8",
                "Monotone_Constraints":
                "[1, 1, 1, 0, 1, 0, 0, 0]",
                "Variables":
                "min_PT_final_state_tracks,sum_PT_final_state_tracks,min_FS_IPCHI2_OWNPV,max_FS_IPCHI2_OWNPV,TwoBody_PT,TwoBody_ENDVERTEX_DOCAMAX,TwoBody_FDCHI2_OWNPV,TwoBody_ENDVERTEX_CHI2",
            },
            Inputs={
                "min_PT_final_state_tracks":
                (F.MIN_ELEMENT @ F.MAP(F.PT) @ F.GET_ALL_BASICS) / GeV,
                "sum_PT_final_state_tracks":
                (F.CHILD(1, F.PT) + F.CHILD(2, F.PT)) / GeV,
                "min_FS_IPCHI2_OWNPV":
                fmath.log(F.MIN_ELEMENT @ F.MAP(F.MINIPCHI2(pvs))
                          @ F.GET_ALL_BASICS),
                "max_FS_IPCHI2_OWNPV":
                fmath.log(F.MAX_ELEMENT @ F.MAP(F.MINIPCHI2(pvs))
                          @ F.GET_ALL_BASICS),
                "TwoBody_PT":
                F.PT() / GeV,
                "TwoBody_ENDVERTEX_DOCAMAX":
                F.MAXSDOCA *
                10,  # Offline Training in the Range of 100 Microns
                "TwoBody_FDCHI2_OWNPV":
                fmath.log(F.BPVFDCHI2(pvs)),
                "TwoBody_ENDVERTEX_CHI2":
                F.CHI2DOF(),
            },
        ) > MVACut)

    code = F.require_all(mva)

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_filtered_three_body_particles(
        particles,
        MVACut,
        make_pvs=make_pvs,
):
    pvs = make_pvs()
    mva = (
        F.MVA(
            MVAType="SigmaNet",
            Config={
                "File":
                "paramfile://data/ThreeBody_topo_sigmanet_upgrade.json",
                "Name":
                "Topo_ThreeBody_SigmaNet",
                "Lambda":
                "2.0",
                "NLayers":
                "4",
                "InputSize":
                "15",
                "Monotone_Constraints":
                "[1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0]",
                "Variables":
                "min_FS_IPCHI2_OWNPV,max_FS_IPCHI2_OWNPV,sum_PT_TRACK12,min_PT_TRACK12,sum_PT_final_state_tracks,min_PT_final_state_tracks,TwoBody_ENDVERTEX_DOCAMAX,ThreeBody_ENDVERTEX_DOCAMAX,TwoBody_FDCHI2_OWNPV,TwoBody_IPCHI2_OWNPV,ThreeBody_FDCHI2_OWNPV,ThreeBody_PT,TwoBody_PT,TwoBody_ENDVERTEX_CHI2,ThreeBody_ENDVERTEX_CHI2",
            },
            Inputs={
                "min_FS_IPCHI2_OWNPV":
                fmath.log(F.MIN_ELEMENT @ F.MAP(F.MINIPCHI2(pvs))
                          @ F.GET_ALL_BASICS),
                "max_FS_IPCHI2_OWNPV":
                fmath.log(F.MAX_ELEMENT @ F.MAP(F.MINIPCHI2(pvs))
                          @ F.GET_ALL_BASICS),
                "sum_PT_TRACK12":
                (F.CHILD(1, F.CHILD(1, F.PT)) + F.CHILD(1, F.CHILD(2, F.PT))) /
                GeV,
                "min_PT_TRACK12":
                F.CHILD(
                    1, (F.MIN_ELEMENT @ F.MAP(F.PT) @ F.GET_ALL_BASICS)) / GeV,
                "sum_PT_final_state_tracks":
                (F.CHILD(1, F.CHILD(1, F.PT())) + F.CHILD(
                    1, F.CHILD(2, F.PT())) + F.CHILD(2, F.PT())) / GeV,
                "min_PT_final_state_tracks":
                (F.MIN_ELEMENT @ F.MAP(F.PT) @ F.GET_ALL_BASICS) / GeV,
                "TwoBody_ENDVERTEX_DOCAMAX":
                F.CHILD(1, F.MAXDOCA()) *
                10,  # Offline Training in the Range of 100 Microns
                "ThreeBody_ENDVERTEX_DOCAMAX":
                F.MAXDOCA() *
                10,  # Offline Training in the Range of 100 Microns
                "TwoBody_FDCHI2_OWNPV":
                fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
                "TwoBody_IPCHI2_OWNPV":
                fmath.log(F.CHILD(1, F.MINIPCHI2(pvs))),
                "ThreeBody_FDCHI2_OWNPV":
                fmath.log(F.BPVFDCHI2(pvs)),
                "ThreeBody_PT":
                F.PT() / GeV,
                "TwoBody_PT":
                F.CHILD(1, F.PT()) / GeV,
                "TwoBody_ENDVERTEX_CHI2":
                F.CHILD(1, F.CHI2DOF()),
                "ThreeBody_ENDVERTEX_CHI2":
                fmath.log(
                    F.CHI2DOF()),  # take log here since it has higher range
            },
        ) > MVACut)

    return ParticleFilter(particles, F.FILTER(mva))


def line_prefilters():
    """Control flow filters common to all topological lines."""
    return [require_pvs(make_pvs())]


def make_unfiltered_topo_twobody(make_particles=make_input_particles,
                                 make_pvs=make_pvs):
    """Return two-body candidates used by the topological lines."""
    pvs = make_pvs()
    pions = make_particles()
    decay_descriptors = ["B0 -> pi- pi+", "B0 -> pi+ pi+", "B0 -> pi- pi-"]

    two_bodys = []
    for decaydescriptor in decay_descriptors:
        two_body = make_two_body_particles(
            particles=[pions, pions], descriptors=decaydescriptor, pvs=pvs)
        two_bodys.append(two_body)

    two_body_merge = ParticleContainersMerger(
        two_bodys, name="Topo_unfiltered_twobody_merger")
    return two_body_merge


def make_filtered_topo_twobody(MVACut):
    """Return two-body candidates used by the topological lines, filtered by the MVA."""
    two_bodys = make_unfiltered_topo_twobody()
    filtered = make_filtered_two_body_particles(
        particles=two_bodys, MVACut=MVACut)
    return filtered


def make_unfiltered_topo_threebody(make_particles=make_input_particles,
                                   make_pvs=make_pvs):
    """Return three-body candidates used by the topological lines."""
    pions = make_particles()
    pvs = make_pvs()
    # Explicitly pass the same makers we use for the 2+1 combination
    two_body = make_unfiltered_topo_twobody(
        make_particles=make_input_particles, make_pvs=make_pvs)
    decay_descriptors = ["B_s0 -> B0 pi+", "B_s0 -> B0 pi-"]

    three_bodys = []
    for decaydescriptor in decay_descriptors:
        three_body = make_three_body_particles(
            particles=[two_body, pions], descriptors=decaydescriptor, pvs=pvs)
        three_bodys.append(three_body)

    three_body_merge = ParticleContainersMerger(
        three_bodys, name="Topo_unfiltered_threebody_merger")
    return three_body_merge


def make_filtered_topo_threebody(MVACut):
    """Return three-body candidates used by the topological lines, filtered by the MVA."""
    three_body = make_unfiltered_topo_threebody()
    filtered = make_filtered_three_body_particles(
        particles=three_body, MVACut=MVACut)
    return filtered


# Muon Topo Functions
def make_unfiltered_topo_mu_twobody(make_particles=make_input_particles,
                                    make_pvs=make_pvs):
    """Return two-body candidates used by the muonic topological lines."""
    pvs = make_pvs()
    pions = make_particles()
    muons = make_ismuon_long_muon()
    decay_descriptors = [
        "B0 -> mu- pi+",
        "B0 -> mu+ pi+",
        "B0 -> mu- pi-",
        "B0 -> mu+ pi-",
    ]

    two_bodys = []
    for decaydescriptor in decay_descriptors:
        two_body = make_two_body_particles(
            particles=[
                muons,
                pions,
            ],  # Be careful with Th0r: Ordering must fit to descriptor! (So Muon, Pion)
            descriptors=decaydescriptor,
            pvs=pvs,
        )
        two_bodys.append(two_body)

    two_body_muon_merge = ParticleContainersMerger(
        two_bodys, name="Muontopo_unfiltered_twobody_merger")
    return two_body_muon_merge


def make_filtered_topo_mu_twobody(MVACut):
    """Return two-body muon candidates used by the muonic topological lines, filtered by the MVA."""
    two_bodys = make_unfiltered_topo_mu_twobody()
    filtered = make_filtered_two_body_particles(
        particles=two_bodys, MVACut=MVACut)
    return filtered


@register_line_builder(all_lines)
@configurable
def twobody_line(name="Hlt2Topo2Body", prescale=1, persistreco=True):
    candidates = make_filtered_topo_twobody(MVACut=0.9855)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def threebody_line(name="Hlt2Topo3Body", prescale=1, persistreco=True):
    candidates = make_filtered_topo_threebody(MVACut=0.9855)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def twobody_mu_line(name="Hlt2TopoMu2Body", prescale=1, persistreco=True):
    candidates = make_filtered_topo_mu_twobody(MVACut=0.9855)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco,
    )


def require_topo_candidate(min_twobody_mva, min_threebody_mva):
    node_name = "Topo_2or3body" + str(min_twobody_mva) + str(min_threebody_mva)
    node_decision = CompositeNode(
        node_name,
        [
            make_filtered_topo_twobody(MVACut=min_twobody_mva),
            make_filtered_topo_threebody(MVACut=min_threebody_mva),
        ],
        combine_logic=NodeLogic.NONLAZY_OR,
    )
    return node_decision
