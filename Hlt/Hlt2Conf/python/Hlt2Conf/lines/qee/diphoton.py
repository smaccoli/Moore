###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of ALPs->GammaGamma lines including B2GammaGamma
- B/ALPs -> Gamma Gamma

author: Titus Mombächer
date: 23.11.2021

10.12.2021: at the moment misses photon PID. This is reported here https://gitlab.cern.ch/lhcb/Rec/-/issues/240 and this line needs review once Photon PID is available.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.standard_particles import make_photons

all_lines = {}


@configurable
def filter_qee_photons(cl_min,
                       et_min,
                       e_min,
                       name='filter_qee_photons_{hash}',
                       make_particles=make_photons):
    """Photon filter, currently missing CL functor"""

    code = F.require_all(F.PT > et_min, F.P > e_min)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_diphoton(
        min_cl_gamma=0.3,  # CL is not implemented currently
        min_pt_gamma=5 * GeV,
        min_p_gamma=16. * GeV,
        min_sumpt=12. * GeV,
        min_B_pt=7. * GeV,
        min_B_mass=2. * GeV,
        min_pt_asym=0.1):
    """
    Return Diphotons from B/ALPs around a few GeV.
    The descriptor head is called "B_s0" for well-defined particle properties, however the name has no impact on the reconstruction.
    """
    descriptor = 'B_s0 -> gamma gamma'
    photons = filter_qee_photons(
        cl_min=min_cl_gamma, et_min=min_pt_gamma,
        e_min=min_p_gamma)  #CL is not implemented at the moment

    sum_pt = F.CHILD(1, F.PT) + F.CHILD(2, F.PT)
    pt_asym = (F.CHILD(1, F.PT) - F.CHILD(2, F.PT)) / (
        F.CHILD(1, F.PT) + F.CHILD(2, F.PT))
    combination_code = F.require_all(
        sum_pt > min_sumpt, pt_asym * pt_asym > min_pt_asym * min_pt_asym)
    vertex_code = F.require_all(F.PT > min_B_pt, F.MASS > min_B_mass)
    return ParticleCombiner(
        ParticleCombiner="ParticleAdder",
        Inputs=[photons, photons],
        name='make_qee_diphotons_{hash}',
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@register_line_builder(all_lines)
def Hlt2_ALPsToGammaGamma(name="Hlt2QEE_ALPsToGammaGamma",
                          prescale=1,
                          persistreco=False):
    """
    Register B2GammaGamma/ALPs2GammaGamma line
    """
    diphotons = make_diphoton()

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [diphotons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )
