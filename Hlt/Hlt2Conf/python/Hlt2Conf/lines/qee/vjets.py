###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builder functions for the vector boson (decaying to muons or electrons)+jets sprucing lines
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable
from PyConf.utilities import ConfigurationError

from Hlt2Conf.algorithms import ParticleContainersMerger
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Hlt2Conf.lines.qee.qee_builders import muon_filter, elec_filter, make_jets


### Builders that apply to both muonic and electronic boson decays
@configurable
def make_Z0(inputleptons, decay_descriptor, min_mass):
    #Muon-Muon (or electron-electron) combination with only a mass cut

    # mass region of Z0
    combination_code = F.require_all(F.MASS > min_mass)

    return ParticleCombiner([inputleptons, inputleptons],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code)


@configurable
def make_Z0jet(inputZ0, inputjets, decay_descriptor='H_10 -> Z0 CELLjet'):
    #Z0-jet combination

    return ParticleCombiner([inputZ0, inputjets],
                            ParticleCombiner="ParticleAdder",
                            DecayDescriptor=decay_descriptor)


@configurable
def make_Z0jetjet(inputZ0,
                  inputjets,
                  decay_descriptor='H_10 -> Z0 CELLjet CELLjet'):
    #Z0-jet-jet combination

    return ParticleCombiner([inputZ0, inputjets, inputjets],
                            ParticleCombiner="ParticleAdder",
                            DecayDescriptor=decay_descriptor)


@configurable
def make_Wjet(inputleptons, inputjets, decay_descriptors, name):
    #W + jet ->lepton + jet combination
    #iterative procedure for several decay descriptors
    if len(decay_descriptors) == 0:
        raise ConfigurationError("Decay descriptors not given")
    cand = []
    for descriptor in decay_descriptors:
        cand.append(
            ParticleCombiner([inputleptons, inputjets],
                             ParticleCombiner="ParticleAdder",
                             DecayDescriptor=descriptor))

    return ParticleContainersMerger(cand, name=name)


@configurable
def make_Wjetjet(inputleptons, inputjets, decay_descriptors, name):
    #W-jet-jet combination
    if len(decay_descriptors) == 0:
        raise ConfigurationError("Decay descriptors not given")
    cand = []
    for descriptor in decay_descriptors:
        cand.append(
            ParticleCombiner([inputleptons, inputjets, inputjets],
                             ParticleCombiner="ParticleAdder",
                             DecayDescriptor=descriptor))

    return ParticleContainersMerger(cand, name=name)


### Muon-specific calls of above builders
@configurable
def make_Z0_mumu(inputmuons,
                 decay_descriptor='Z0 -> mu+ mu-',
                 min_mass=40. * GeV):
    return make_Z0(inputmuons, decay_descriptor, min_mass)


@configurable
def make_ssdimuon(inputmuons,
                  decay_descriptor='[Z0 -> mu- mu-]cc',
                  min_mass=20. * GeV):
    return make_Z0(inputmuons, decay_descriptor, min_mass)


@configurable
def make_W_mu_jet(
        inputmuons,
        inputjets,
        decay_descriptors=['H_10 -> mu+ CELLjet', 'H_10 -> mu- CELLjet'],
        name="WJetMuMuMerger_{hash}"):
    return make_Wjet(inputmuons, inputjets, decay_descriptors, name)


@configurable
def make_W_mu_jetjet(inputmuons,
                     inputjets,
                     decay_descriptors=[
                         'H_10 -> mu+ CELLjet CELLjet',
                         'H_10 -> mu- CELLjet CELLjet'
                     ],
                     name="WJetJetMuMuMerger_{hash}"):
    return make_Wjetjet(inputmuons, inputjets, decay_descriptors, name)


### Electron-specific callers of above builders
@configurable
def make_Z0_ee(inputelectrons,
               decay_descriptor='Z0 -> e+ e-',
               min_mass=40. * GeV):
    return make_Z0(inputelectrons, decay_descriptor, min_mass)


@configurable
def make_W_elec_jet(
        inputelectrons,
        inputjets,
        decay_descriptors=['H_10 -> e+ CELLjet', 'H_10 -> e- CELLjet'],
        name="WJetEEMerger_{hash}"):
    return make_Wjet(inputelectrons, inputjets, decay_descriptors, name)


@configurable
def make_W_elec_jetjet(inputelectrons,
                       inputjets,
                       decay_descriptors=[
                           'H_10 -> e+ CELLjet CELLjet',
                           'H_10 -> e- CELLjet CELLjet'
                       ],
                       name="WJetJetEEMerger_{hash}"):
    return make_Wjetjet(inputelectrons, inputjets, decay_descriptors, name)


### Line algorithms
@configurable
def make_Z0jet_cand():

    muons = muon_filter(min_pt=20. * GeV)
    Z0 = make_Z0_mumu(muons)
    jets = make_jets(min_pt=10 * GeV)
    line_alg = make_Z0jet(Z0, jets)

    return line_alg


@configurable
def make_Z0SVjet_cand():

    muons = muon_filter(min_pt=20. * GeV)
    Z0 = make_Z0_mumu(muons)
    jets = make_jets(min_pt=10, tags='SV')
    line_alg = make_Z0jet(Z0, jets)

    return line_alg


@configurable
def make_Z0jetjet_cand():

    muons = muon_filter(min_pt=20. * GeV)
    Z0 = make_Z0_mumu(muons)
    jets = make_jets(min_pt=10 * GeV)
    line_alg = make_Z0jetjet(Z0, jets)

    return line_alg


@configurable
def make_Z0SVjetSVjet_cand():

    muons = muon_filter(min_pt=20. * GeV)
    Z0 = make_Z0_mumu(muons)
    jets = make_jets(min_pt=10 * GeV, tags='SV')
    line_alg = make_Z0jetjet(Z0, jets)

    return line_alg


@configurable
def make_Wjet_cand():

    muons = muon_filter(min_pt=20. * GeV)
    jets = make_jets(min_pt=10 * GeV)
    line_alg = make_W_mu_jet(muons, jets)

    return line_alg


@configurable
def make_WSVjet_cand():

    muons = muon_filter(min_pt=20. * GeV)
    jets = make_jets(min_pt=10 * GeV, tags='SV')
    line_alg = make_W_mu_jet(muons, jets)

    return line_alg


@configurable
def make_Wjetjet_cand():

    muons = muon_filter(min_pt=20. * GeV)
    jets = make_jets(min_pt=10 * GeV)
    line_alg = make_W_mu_jetjet(muons, jets)

    return line_alg


@configurable
def make_WSVjetSVjet_cand():

    muons = muon_filter(min_pt=20. * GeV)
    jets = make_jets(min_pt=10 * GeV, tags='SV')
    line_alg = make_W_mu_jetjet(muons, jets)

    return line_alg


@configurable
def make_ssdimuonjet_cand():

    muons = muon_filter(min_pt=10. * GeV)
    ssdimuon = make_ssdimuon(muons)
    jets = make_jets(min_pt=10 * GeV)
    line_alg = make_Z0jet(ssdimuon, jets)

    return line_alg


@configurable
def make_Z0_elec_jet_cand():

    electrons = elec_filter(min_pt=20. * GeV)
    Z0 = make_Z0_ee(electrons)
    jets = make_jets(min_pt=10 * GeV)
    line_alg = make_Z0jet(Z0, jets)

    return line_alg


@configurable
def make_Z0_elec_SVjet_cand():

    electrons = elec_filter(min_pt=20. * GeV)
    Z0 = make_Z0_ee(electrons)
    jets = make_jets(min_pt=10, tags='SV')
    line_alg = make_Z0jet(Z0, jets)

    return line_alg


@configurable
def make_Z0_elec_jetjet_cand():

    electrons = elec_filter(min_pt=20. * GeV)
    Z0 = make_Z0_ee(electrons)
    jets = make_jets(min_pt=10 * GeV)
    line_alg = make_Z0jetjet(Z0, jets)

    return line_alg


@configurable
def make_Z0_elec_SVjetSVjet_cand():

    electrons = elec_filter(min_pt=20. * GeV)
    Z0 = make_Z0_ee(electrons)
    jets = make_jets(min_pt=10 * GeV, tags='SV')
    line_alg = make_Z0jetjet(Z0, jets)

    return line_alg


@configurable
def make_W_elec_jet_cand():

    electrons = elec_filter(min_pt=20. * GeV)
    jets = make_jets(min_pt=10 * GeV)
    line_alg = make_W_elec_jet(electrons, jets)

    return line_alg


@configurable
def make_W_elec_SVjet_cand():

    electrons = elec_filter(min_pt=20. * GeV)
    jets = make_jets(min_pt=10 * GeV, tags='SV')
    line_alg = make_W_elec_jet(electrons, jets)

    return line_alg


@configurable
def make_W_elec_jetjet_cand():

    electrons = elec_filter(min_pt=20. * GeV)
    jets = make_jets(min_pt=10 * GeV)
    line_alg = make_W_elec_jetjet(electrons, jets)

    return line_alg


@configurable
def make_W_elec_SVjetSVjet_cand():

    electrons = elec_filter(min_pt=20. * GeV)
    jets = make_jets(min_pt=10 * GeV, tags='SV')
    line_alg = make_W_elec_jetjet(electrons, jets)

    return line_alg
