###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 single high-PT muon lines.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable
from PyConf.Algorithms import WeightedRelTableAlg

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction

from Hlt2Conf.algorithms_thor import ParticleFilter
from Hlt2Conf.standard_particles import make_ismuon_long_muon, make_long_muons
from Hlt2Conf.standard_jets import make_onlytrack_particleflow

all_lines = {}


@configurable
def make_highpt_muons(name='HighPtMuonMaker_{hash}_{hash}',
                      muons=make_ismuon_long_muon,
                      min_muon_pt=15. * GeV):

    code = F.require_all(F.PT > min_muon_pt)
    return ParticleFilter(muons(), F.FILTER(code), name=name)


@configurable
def make_isolated_muons(high_pt_muons,
                        name='HighPtIsolatedMuonMaker_{hash}',
                        max_cone_pt=10.0 * GeV,
                        pflow_output=make_onlytrack_particleflow()):

    ftAlg = WeightedRelTableAlg(
        ReferenceParticles=high_pt_muons,
        InputCandidates=pflow_output,
        Cut=(F.DR2 < (0.5**2)))

    ftAlg_Rels = ftAlg.OutputRelations

    # FIXME Signal is (for now) manually removed from cone with - F.PT.
    code = F.require_all(
        in_range(
            0,
            F.VALUE_OR(0) @ F.SUMCONE(Functor=F.PT, Relations=ftAlg_Rels) -
            F.PT, max_cone_pt))

    return ParticleFilter(high_pt_muons, F.FILTER(code), name=name)


@register_line_builder(all_lines)
@configurable
def single_muon_highpt_line(name='Hlt2QEE_SingleHighPtMuon',
                            prescale=1,
                            persistreco=True):
    """High PT single muon line"""

    high_pt_muons = make_highpt_muons(min_muon_pt=15. * GeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_muons],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def single_muon_highpt_prescale_line(name='Hlt2QEE_SingleHighPtMuonPrescale',
                                     prescale=0.05,
                                     persistreco=True):
    """High PT single muon line with lower pT threshold, prescaled to reduce the rate."""

    high_pt_muons = make_highpt_muons(min_muon_pt=10. * GeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_muons],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def single_muon_highpt_iso_line(name='Hlt2QEE_SingleHighPtMuonIso',
                                prescale=1,
                                persistreco=True):
    """High PT single muon line with lower pT threshold, and isolation to keep the rate down. 
    Make PT cut first to drastically reduce number of times isolation algorithm is called."""

    pflow = make_onlytrack_particleflow()
    high_pt_muons = make_highpt_muons(
        min_muon_pt=12.5 * GeV, name="HighPtMuonMakerForIso")
    high_pt_isolated_muons = make_isolated_muons(
        high_pt_muons, pflow_output=pflow)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [high_pt_muons, high_pt_isolated_muons],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def single_muon_highpt_nomuid_line(name='Hlt2QEE_SingleHighPtMuonNoMuID',
                                   prescale=0.1):
    """High PT single muon line with No ISMUON requirement for background and MuonID efficiency studies
    . Prescale required to control the rate."""

    high_pt_nomuid_muons = make_highpt_muons(
        muons=make_long_muons, min_muon_pt=15. * GeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_nomuid_muons],
        prescale=prescale,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )
