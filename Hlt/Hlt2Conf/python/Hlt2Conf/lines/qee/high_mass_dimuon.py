###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 Z->MuMu and same-sign dimuon (control sample) lines.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon, make_long_muons

all_lines = {}


@configurable
def muon_filter_for_Z(particles, min_pt=0. * GeV):
    """ A muon filter: PT
    """
    return ParticleFilter(particles, F.FILTER(F.PT > min_pt))


@configurable
def make_Z_cand(with_PID=True):
    muons = make_ismuon_long_muon() if with_PID else make_long_muons()
    muons_for_Z = muon_filter_for_Z(muons, min_pt=3. * GeV)
    return make_dimuon_novxt(muons_for_Z, "Z0 -> mu+ mu-", 40 * GeV)


@configurable
def make_Zss_cand():
    muons = muon_filter_for_Z(make_ismuon_long_muon(), min_pt=3. * GeV)
    return make_dimuon_novxt(muons, "[Z0 -> mu- mu-]cc", 16. * GeV)


@configurable
def make_dimuon_novxt(input_muons, decaydescriptor, min_mass=40. * GeV):
    """Dimuon combination with only a mass cut
    """

    # mass region of dimuons
    combination_code = (F.MASS > min_mass)

    return ParticleCombiner([input_muons, input_muons],
                            DecayDescriptor=decaydescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=F.ALL)


@register_line_builder(all_lines)
@configurable
def z_to_mu_mu_line(name='Hlt2QEE_ZToMuMu', prescale=1, persistreco=True):
    """Z0 boson decay to two muons line"""

    muons = muon_filter_for_Z(make_ismuon_long_muon(), min_pt=3. * GeV)
    z02mumu = make_dimuon_novxt(muons, "Z0 -> mu+ mu-", min_mass=40. * GeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [z02mumu],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def same_sign_dimuon_line(name='Hlt2QEE_DiMuonSameSign',
                          prescale=1,
                          persistreco=True):
    """Z0 boson decay to two same-sign muons line"""

    muons = muon_filter_for_Z(make_ismuon_long_muon(), min_pt=3. * GeV)
    mumuss = make_dimuon_novxt(muons, "[Z0 -> mu- mu-]cc", min_mass=16. * GeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [mumuss],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )
