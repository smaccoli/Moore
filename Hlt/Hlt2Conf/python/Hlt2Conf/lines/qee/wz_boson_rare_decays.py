###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the W/Z rare decay HLT2 lines, including W/Z -> photon + hadron.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, picosecond

from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_has_rich_long_pions, make_has_rich_long_kaons, make_has_rich_long_protons
from Hlt2Conf.lines.charmonium_to_dimuon import make_charmonium_dimuon as make_dimuon
from Hlt2Conf.lines.qee.qee_builders import make_jets, make_photons

all_lines = {}


@configurable
def make_EW_pions(pvs,
                  p_min=1 * GeV,
                  min_pt=0.25 * GeV,
                  p_pidk_max=5.,
                  mipchi2_min=0):

    code = F.require_all(F.P > p_min, F.PT > min_pt, F.PID_K < p_pidk_max,
                         F.MINIPCHI2(pvs) > mipchi2_min)
    particles = make_has_rich_long_pions()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_EW_kaons(pvs,
                  p_min=1 * GeV,
                  min_pt=0.25 * GeV,
                  p_pidk_min=-5,
                  mipchi2_min=0):

    code = F.require_all(F.P > p_min, F.PT > min_pt, F.PID_K > p_pidk_min,
                         F.MINIPCHI2(pvs) > mipchi2_min)
    particles = make_has_rich_long_kaons()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_EW_protons(pvs,
                    p_min=1 * GeV,
                    min_pt=0.25 * GeV,
                    p_pidp_min=-5,
                    mipchi2_min=0):

    code = F.require_all(F.P > p_min, F.PT > min_pt, F.PID_P > p_pidp_min,
                         F.MINIPCHI2(pvs) > mipchi2_min)
    particles = make_has_rich_long_protons()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_EW_detached_Jpsi(pvs,
                          name='EW_Detached_Jpsi_{hash}',
                          minIPChi2_muon=4,
                          bpvdls_min=9.,
                          minMass_dimuon=3.0 * GeV,
                          maxMass_dimuon=3.2 * GeV):

    make_particles = make_dimuon(
        minIPChi2_muon=minIPChi2_muon,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon)
    code = (F.BPVDLS(pvs) > bpvdls_min)

    return ParticleFilter(make_particles, name=name, Cut=F.FILTER(code))


@configurable
def make_EW_Prompt_Jpsi(name='EW_Prompt_Jpsi_{hash}',
                        minMass_dimuon=3. * GeV,
                        maxMass_dimuon=4. * GeV,
                        min_pt=0.5 * GeV,
                        maxVertexChi2=20.):

    return make_dimuon(
        name=name,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        minPt_dimuon=min_pt,
        maxVertexChi2=maxVertexChi2)


@configurable
def make_EW_Upsilon(name='EW_Upsilon_{hash}',
                    minMass_dimuon=9. * GeV,
                    maxMass_dimuon=11. * GeV,
                    min_pt=0.5 * GeV,
                    maxVertexChi2=20.):

    return make_dimuon(
        name=name,
        DecayDescriptor='Upsilon(1S) -> mu+ mu-',
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        minPt_dimuon=min_pt,
        maxVertexChi2=maxVertexChi2)


@configurable
def make_EW_PhiToKK(pvs,
                    name='EW_PhiToKK_{hash}',
                    min_pt=0.5 * GeV,
                    comb_m_min=0.98 * GeV,
                    comb_m_max=1.06 * GeV,
                    max_docachi2=30.,
                    max_vchi2pdof=10,
                    mipchi2_min=4.):

    kaons = make_EW_kaons(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACHI2CUT(max_docachi2))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof)

    return ParticleCombiner(
        name=name,
        Inputs=[kaons, kaons],
        DecayDescriptor='phi(1020) -> K+ K-',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_KstToKPi(pvs,
                     name='EW_KstToKPi_{hash}',
                     min_pt=0.5 * GeV,
                     comb_m_min=0.826 * GeV,
                     comb_m_max=0.966 * GeV,
                     max_docachi2=30.,
                     max_vchi2pdof=10,
                     mipchi2_min=4.):

    pions = make_EW_pions(pvs, mipchi2_min=mipchi2_min)
    kaons = make_EW_kaons(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACHI2CUT(max_docachi2))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof)

    return ParticleCombiner(
        name=name,
        Inputs=[kaons, pions],
        DecayDescriptor='[K*(892)0 -> K+ pi-]cc',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_LcToPKPi(pvs,
                     name='EW_LcToPKPi_{hash}',
                     min_pt=0.5 * GeV,
                     comb_m_min=2.22 * GeV,
                     comb_m_max=2.35 * GeV,
                     max_docachi2=30.,
                     max_vchi2pdof=10,
                     mipchi2_min=3,
                     DecayDescriptor='[Lambda_c+ -> p+ K- pi+]cc'):

    pions = make_EW_pions(pvs, mipchi2_min=mipchi2_min)
    kaons = make_EW_kaons(pvs, mipchi2_min=mipchi2_min)
    protons = make_EW_protons(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACHI2CUT(max_docachi2))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof)

    return ParticleCombiner(
        name=name,
        Inputs=[protons, kaons, pions],
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_Xic0ToPKKPi(pvs,
                        name='EW_Xic0ToPKKPi_{hash}',
                        min_pt=0.5 * GeV,
                        comb_m_min=2.4 * GeV,
                        comb_m_max=2.53 * GeV,
                        max_docachi2=30.,
                        max_vchi2pdof=10,
                        mipchi2_min=3):

    pions = make_EW_pions(pvs, mipchi2_min=mipchi2_min)
    kaons = make_EW_kaons(pvs, mipchi2_min=mipchi2_min)
    protons = make_EW_protons(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACHI2CUT(max_docachi2))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof)

    return ParticleCombiner(
        name=name,
        Inputs=[protons, kaons, kaons, pions],
        DecayDescriptor='[Xi_c0 -> p+ K- K- pi+]cc',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_DpToKPiPi(pvs,
                      name='EW_DpToKPiPi_{hash}',
                      min_pt=0.5 * GeV,
                      comb_m_min=1.80 * GeV,
                      comb_m_max=1.93 * GeV,
                      max_vchi2pdof=9,
                      mipchi2_min=4,
                      bpvltime_min=0.2 * picosecond,
                      bpvfdchi2_min=100.):

    pions = make_EW_pions(pvs, mipchi2_min=mipchi2_min)
    kaons = make_EW_kaons(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof,
                                F.BPVLTIME(pvs) > bpvltime_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    return ParticleCombiner(
        name=name,
        Inputs=[kaons, pions, pions],
        DecayDescriptor='[D+ -> K- pi+ pi+]cc',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_DspToKKPi(pvs,
                      name='EW_DspToKKPi_{hash}',
                      min_pt=0.5 * GeV,
                      comb_m_min=1.91 * GeV,
                      comb_m_max=2.03 * GeV,
                      max_vchi2pdof=9,
                      mipchi2_min=4,
                      bpvltime_min=0.2 * picosecond,
                      bpvfdchi2_min=100.):

    pions = make_EW_pions(pvs, mipchi2_min=mipchi2_min)
    kaons = make_EW_kaons(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof,
                                F.BPVLTIME(pvs) > bpvltime_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    return ParticleCombiner(
        name=name,
        Inputs=[kaons, kaons, pions],
        DecayDescriptor='[D_s+ -> K- K+ pi+]cc',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_D0ToKPi(pvs,
                    name='EW_D0ToKPi_{hash}',
                    min_pt=0.5 * GeV,
                    comb_m_min=1.80 * GeV,
                    comb_m_max=1.93 * GeV,
                    max_vchi2pdof=9,
                    mipchi2_min=4,
                    bpvfdchi2_min=25.):

    pions = make_EW_pions(pvs, mipchi2_min=mipchi2_min)
    kaons = make_EW_kaons(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    return ParticleCombiner(
        name=name,
        Inputs=[kaons, pions],
        DecayDescriptor='[D0 -> K- pi+]cc',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_B0ToJPsiKst(pvs,
                        name='EW_B0ToJPsiKst_{hash}',
                        min_pt=0.5 * GeV,
                        comb_m_min=5.0 * GeV,
                        comb_m_max=5.6 * GeV,
                        max_vchi2pdof=9,
                        mipchi2_min=4,
                        bpvfdchi2_min=10.,
                        lifetime=0.2 * picosecond):

    jpsi = make_EW_detached_Jpsi(pvs, minIPChi2_muon=mipchi2_min)
    kst = make_EW_KstToKPi(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                F.BPVLTIME(pvs) > lifetime)

    return ParticleCombiner(
        name=name,
        Inputs=[jpsi, kst],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor='[B0 -> J/psi(1S) K*(892)0]cc',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_BsToJPsiPhi(pvs,
                        name='EW_BsToJPsiPhi_{hash}',
                        min_pt=0.5 * GeV,
                        comb_m_min=5.0 * GeV,
                        comb_m_max=5.6 * GeV,
                        max_vchi2pdof=9,
                        mipchi2_min=4,
                        bpvfdchi2_min=10.,
                        lifetime=0.2 * picosecond):

    jpsi = make_EW_detached_Jpsi(pvs, minIPChi2_muon=mipchi2_min)
    phi = make_EW_PhiToKK(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                F.BPVLTIME(pvs) > lifetime)

    return ParticleCombiner(
        name=name,
        Inputs=[jpsi, phi],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor='B_s0 -> J/psi(1S) phi(1020)',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_BpToJPsiK(pvs,
                      name='EW_BpToJPsiK_{hash}',
                      min_pt=0.5 * GeV,
                      comb_m_min=5.0 * GeV,
                      comb_m_max=5.6 * GeV,
                      max_vchi2pdof=9,
                      mipchi2_min=4,
                      bpvfdchi2_min=10.,
                      lifetime=0.2 * picosecond):

    jpsi = make_EW_detached_Jpsi(pvs, minIPChi2_muon=mipchi2_min)
    kaons = make_EW_kaons(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                F.BPVLTIME(pvs) > lifetime)

    return ParticleCombiner(
        name=name,
        Inputs=[jpsi, kaons],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor='[B+ -> J/psi(1S) K+]cc',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_BcToJPsiPi(pvs,
                       name='EW_BcToJPsiPi_{hash}',
                       min_pt=0.5 * GeV,
                       comb_m_min=6.09 * GeV,
                       comb_m_max=6.51 * GeV,
                       max_vchi2pdof=9,
                       mipchi2_min=4,
                       bpvfdchi2_min=10.,
                       lifetime=0.1 * picosecond):

    jpsi = make_EW_detached_Jpsi(pvs, minIPChi2_muon=mipchi2_min)
    pions = make_EW_pions(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                F.BPVLTIME(pvs) > lifetime)

    return ParticleCombiner(
        name=name,
        Inputs=[jpsi, pions],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor='[B_c+ -> J/psi(1S) pi+]cc',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EW_LbToLcPi(pvs,
                     name='EW_LbToLcPi_{hash}',
                     min_pt=0.5 * GeV,
                     comb_m_min=5.35 * GeV,
                     comb_m_max=5.85 * GeV,
                     max_vchi2pdof=9,
                     mipchi2_min=4,
                     bpvfdchi2_min=10.,
                     lifetime=0.2 * picosecond):

    lc = make_EW_LcToPKPi(pvs, mipchi2_min=mipchi2_min)
    pions = make_EW_pions(pvs, mipchi2_min=mipchi2_min)

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(F.PT > min_pt, F.CHI2 < max_vchi2pdof,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min,
                                F.BPVLTIME(pvs) > lifetime)

    return ParticleCombiner(
        name=name,
        Inputs=[lc, pions],
        DecayDescriptor='[Lambda_b0 -> Lambda_c+ pi-]cc',
        ParticleCombiner="ParticleAdder",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_PhotonHadron(particles,
                      min_mass=40. * GeV,
                      decay_descriptor='H_10 -> mu+ mu-'):

    combination_code = F.require_all(F.MASS > min_mass)

    return ParticleCombiner(
        particles,
        ParticleCombiner="ParticleAdder",
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code)


@configurable
def make_WRareDecay_DiMuonDst_cand():

    pvs = make_pvs()
    dimuon = make_EW_Prompt_Jpsi(min_pt=10. * GeV)
    dsplus = make_EW_DspToKKPi(pvs, min_pt=10. * GeV)
    decay_gammahadron = '[W+ -> J/psi(1S) D_s+]cc'
    line_alg = make_PhotonHadron(
        particles=[dimuon, dsplus],
        min_mass=40. * GeV,
        decay_descriptor=decay_gammahadron)

    return line_alg


@configurable
def Get_HadronFuns(decay_type="DpGamma"):

    dict_fun = {
        "JetGamma": make_jets,
        "DpGamma": make_EW_DpToKPiPi,
        "DspGamma": make_EW_DspToKKPi,
        "LcpGamma": make_EW_LcToPKPi,
        "XicpGamma": make_EW_LcToPKPi,
        "BpGamma": make_EW_BpToJPsiK,
        "BcpGamma": make_EW_BcToJPsiPi,
        "JPsiGamma": make_EW_Prompt_Jpsi,
        "UpsilonGamma": make_EW_Upsilon,
        "DzGamma": make_EW_D0ToKPi,
        "Xic0Gamma": make_EW_Xic0ToPKKPi,
        "B0Gamma": make_EW_B0ToJPsiKst,
        "BsGamma": make_EW_BsToJPsiPhi,
        "LbGamma": make_EW_LbToLcPi,
    }
    hadron_fun = dict_fun.get(decay_type)
    return hadron_fun


@configurable
def Get_DecayDescriptor(decay_type="DpGamma"):

    dict_decay = {
        "JetGamma": "Z0 -> CELLjet gamma",
        "DpGamma": "[W+ -> D+ gamma]cc",
        "DspGamma": "[W+ -> D_s+ gamma]cc",
        "LcpGamma": "[W+ -> Lambda_c+ gamma]cc",
        "XicpGamma": "[W+ -> Xi_c+ gamma]cc",
        "BpGamma": "[W+ -> B+ gamma]cc",
        "BcpGamma": "[W+ -> B_c+ gamma]cc",
        "JPsiGamma": "Z0 -> J/psi(1S) gamma",
        "UpsilonGamma": "Z0 -> Upsilon(1S) gamma",
        "DzGamma": "[Z0 -> D0 gamma]cc",
        "Xic0Gamma": "[Z0 -> Xi_c0 gamma]cc",
        "B0Gamma": "[Z0 -> B0 gamma]cc",
        "BsGamma": "[Z0 -> B_s0 gamma]cc",
        "LbGamma": "[Z0 -> Lambda_b0 gamma]cc",
    }

    DecayDescriptor = dict_decay.get(decay_type)
    return DecayDescriptor


@configurable
def make_WZRareDecay_HadronGamma_cand(pvs,
                                      photon_type="DD",
                                      decay_type="DpGamma"):

    hadron_fun = Get_HadronFuns(decay_type=decay_type)
    min_mass = 3. * GeV
    if decay_type == "XicpGamma":
        hadron = hadron_fun(
            pvs,
            min_pt=5. * GeV,
            comb_m_min=2.4 * GeV,
            comb_m_max=2.53 * GeV,
            DecayDescriptor='[Xi_c+ -> p+ K- pi+]cc')
    elif decay_type == "JetGamma":
        hadron = hadron_fun(min_pt=20. * GeV)
        min_mass = 10. * GeV
    elif decay_type in ["UpsilonGamma", "JPsiGamma"]:
        hadron = hadron_fun(min_pt=5. * GeV)
    else:
        hadron = hadron_fun(pvs, min_pt=5. * GeV)

    if photon_type == "All" or decay_type == "JetGamma":
        gamma = make_photons(photon_type=photon_type, pt_min=15. * GeV)
    else:
        gamma = make_photons(photon_type=photon_type, pt_min=10. * GeV)

    decay_gammahadron = Get_DecayDescriptor(decay_type=decay_type)

    line_alg = make_PhotonHadron(
        particles=[hadron, gamma],
        min_mass=min_mass,
        decay_descriptor=decay_gammahadron)

    return line_alg


@register_line_builder(all_lines)
@configurable
def Dimuon_Dst_line(name='Hlt2QEE_WZRareJPsiDst', prescale=1,
                    persistreco=True):
    """W+ -> Jpsi Ds+ line"""

    pvs = make_pvs()
    jpsi = make_EW_Prompt_Jpsi(min_pt=10. * GeV)
    dsplus = make_EW_DspToKKPi(pvs, min_pt=10. * GeV)
    decay_gammahadron = '[W+ -> J/psi(1S) D_s+]cc'
    had_gamma = make_PhotonHadron(
        particles=[jpsi, dsplus],
        min_mass=40. * GeV,
        decay_descriptor=decay_gammahadron)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), had_gamma],
        prescale=prescale,
        persistreco=persistreco)


for decay_type in [
        "JetGamma", "DpGamma", "DspGamma", "LcpGamma", "XicpGamma", "BpGamma",
        "BcpGamma", "JPsiGamma", "UpsilonGamma", "DzGamma", "Xic0Gamma",
        "B0Gamma", "BsGamma", "LbGamma"
]:
    for photon_type, line_suffix in zip(["LL", "DD", "All"], ["LL", "DD", ""]):

        @register_line_builder(all_lines)
        @configurable
        def WZRareDecay_HadronGamma_line(
                name=f'Hlt2QEE_WZRare{decay_type}{line_suffix}',
                decay_type=f'{decay_type}',
                photon_type=f'{photon_type}',
                prescale=1,
                persistreco=True):
            f"""{decay_type} ({photon_type} photons) line"""

            pvs = make_pvs()
            had_gamma = make_WZRareDecay_HadronGamma_cand(
                pvs, photon_type=photon_type, decay_type=decay_type)

            return Hlt2Line(
                name=name,
                algs=upfront_reconstruction() + [require_pvs(pvs), had_gamma],
                prescale=prescale,
                persistreco=persistreco)
