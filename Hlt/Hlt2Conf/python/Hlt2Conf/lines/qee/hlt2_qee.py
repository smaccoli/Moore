###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of all the QEE HLT2 lines.
"""

from Hlt2Conf.lines.qee import single_high_pt_muon
from Hlt2Conf.lines.qee import single_high_pt_electron
from Hlt2Conf.lines.qee import di_bjet
from Hlt2Conf.lines.qee import high_mass_dimuon
from Hlt2Conf.lines.qee import high_mass_dielec
from Hlt2Conf.lines.qee import diphoton
from Hlt2Conf.lines.qee import high_mass_dimuon_tracking_efficiency
from Hlt2Conf.lines.qee import wz_boson_rare_decays
from Hlt2Conf.lines.qee import drellyan

full_lines = {}
full_lines.update(single_high_pt_muon.all_lines)
full_lines.update(single_high_pt_electron.all_lines)
full_lines.update(di_bjet.all_lines)
full_lines.update(high_mass_dimuon.all_lines)
full_lines.update(high_mass_dielec.all_lines)
full_lines.update(high_mass_dimuon_tracking_efficiency.all_lines)
full_lines.update(wz_boson_rare_decays.all_lines)
full_lines.update(drellyan.all_lines)

turbo_lines = {}
turbo_lines.update(diphoton.all_lines)
