###############################################################################
# Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration          #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of useful QEE filters and builders
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon
from Hlt2Conf.standard_particles import make_long_electrons_with_brem, make_down_electrons_no_brem
from Hlt2Conf.standard_jets import make_pf_particles, build_jets, tag_jets
from Hlt2Conf.lines.jets.topobits import make_topo_2body
from Hlt2Conf.standard_particles import make_photons
from Hlt2Conf.lines.qee.high_mass_dimuon import make_dimuon_novxt
from Hlt2Conf.lines.qee.high_mass_dielec import make_dielec_novxt


@configurable
def muon_filter(min_pt=0. * GeV):
    #A muon filter: PT

    code = (F.PT > min_pt)
    particles = make_ismuon_long_muon()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter(min_pt=0. * GeV, min_electron_id=-1):
    #An electron filter: PT

    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_long_electrons_with_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter_down(min_pt=0. * GeV, min_electron_id=-1):
    #An down type electron filter: PT

    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_down_electrons_no_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_jets(name='SimpleJets_{hash}',
              min_pt=10 * GeV,
              JetsByVtx=False,
              tags=None):
    #Build and tag jets

    pflow = make_pf_particles()
    svtags = make_topo_2body()
    jets = build_jets(pflow, JetsByVtx, name='JetBuilder' + name)

    if tags is not None:
        taggedjets = tag_jets(jets, svtags, name="Tags" + name)
        jets = taggedjets

    code = (F.PT > min_pt)

    return ParticleFilter(jets, F.FILTER(code))


@configurable
def make_qee_photons(name="qee_photons",
                     make_particles=make_photons,
                     pt_min=5. * GeV):
    code = (F.PT > pt_min)
    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_qee_gamma_DD(name="qee_gamma_DD",
                      descriptor="gamma -> e+ e-",
                      am_max=0.5 * GeV,
                      m_max=0.1 * GeV,
                      pt_min=5. * GeV,
                      min_elec_pt=0. * GeV,
                      min_elec_id=0.,
                      maxVertexChi2=16):

    electrons = elec_filter_down(
        min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_qee_gamma_LL(name="qee_gamma_LL",
                      descriptor="gamma -> e+ e-",
                      am_max=0.5 * GeV,
                      m_max=0.1 * GeV,
                      pt_min=5. * GeV,
                      min_elec_pt=0. * GeV,
                      min_elec_id=0.,
                      maxVertexChi2=16):

    electrons = elec_filter(min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_photons(photon_type="DD", pt_min=10. * GeV):

    if photon_type == "LL":
        gamma = make_qee_gamma_LL()
    elif photon_type == "DD":
        gamma = make_qee_gamma_DD()
    else:
        gamma = make_qee_photons()

    code = F.require_all(F.PT > pt_min)

    return ParticleFilter(gamma, F.FILTER(code))


@configurable
def lepton_filter(lepton_type="mu", min_pt=10. * GeV):

    if lepton_type == "mu":
        lepts = muon_filter(min_pt=min_pt)
    else:
        lepts = elec_filter(min_pt=min_pt)

    return lepts


@configurable
def Z_To_lepts_filter(lepton_type="mu", min_pt=10. * GeV):

    if lepton_type == "mu":
        muons_for_Z = muon_filter(min_pt=min_pt)
        zcand = make_dimuon_novxt(
            input_muons=muons_for_Z,
            decaydescriptor="Z0 -> mu+ mu-",
            min_mass=40 * GeV)
    else:
        electrons_for_Z = elec_filter(min_pt=min_pt)
        zcand = make_dielec_novxt(
            input_elecs=electrons_for_Z,
            decaydescriptor="Z0 -> e+ e-",
            min_mass=40. * GeV)

    return zcand
