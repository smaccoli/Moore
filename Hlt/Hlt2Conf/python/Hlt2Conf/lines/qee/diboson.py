###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Helper functions for building di-vector-boson selections in QEE sprucing lines. See also spruce_qee.py
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable

from Hlt2Conf.algorithms_thor import ParticleCombiner
from Hlt2Conf.lines.qee.qee_builders import muon_filter, elec_filter, make_photons, lepton_filter, Z_To_lepts_filter


@configurable
def make_DiBoson(particles,
                 min_mass=40. * GeV,
                 decay_descriptor='H_10 -> mu+ mu-'):

    combination_code = F.require_all(F.MASS > min_mass)

    return ParticleCombiner(
        particles,
        ParticleCombiner="ParticleAdder",
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code)


@configurable
def make_DiBoson_Base(particles,
                      min_mass=40. * GeV,
                      decay_descriptor='H_10 -> mu+ mu-'):

    combination_code = F.require_all(F.MASS > min_mass)

    return ParticleCombiner(
        particles,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code)


@configurable
def make_WW_emu_cand():

    muons = muon_filter(min_pt=18. * GeV)
    electrons = elec_filter(min_pt=18. * GeV)
    decay_WW = '[H_10 -> mu+ e-]cc'

    line_alg = make_DiBoson_Base(
        particles=[muons, electrons],
        min_mass=20. * GeV,
        decay_descriptor=decay_WW)

    return line_alg


@configurable
def make_WZ_Zll_Wlnu_cand(lepton_type1="mu", lepton_type2="e"):

    zcand = Z_To_lepts_filter(lepton_type=lepton_type1, min_pt=15. * GeV)
    lepts = lepton_filter(lepton_type=lepton_type2, min_pt=20. * GeV)
    decay_WZ = ('[W+ -> Z0 %s+]cc' % lepton_type2)

    line_alg = make_DiBoson(
        particles=[zcand, lepts],
        min_mass=60. * GeV,
        decay_descriptor=decay_WZ)

    return line_alg


## ZZ selection, based on https://indico.cern.ch/event/959802/contributions/4034260/attachments/2110848/3550697/present_formc2.pdf
@configurable
def make_ZZ_Zllplusll_cand(lepton_type1="mu", lepton_type2="e"):

    zcand = Z_To_lepts_filter(lepton_type=lepton_type1, min_pt=15. * GeV)
    lepts = lepton_filter(lepton_type=lepton_type2, min_pt=4. * GeV)
    decay_ZZ = ('H_10 -> Z0 %s+ %s-' % (lepton_type2, lepton_type2))

    line_alg = make_DiBoson(
        particles=[zcand, lepts, lepts],
        min_mass=70. * GeV,
        decay_descriptor=decay_ZZ)

    return line_alg


## W/Z + photon (converted)
@configurable
def make_Wleptgamma_cand(lepton_type="mu", photon_type="DD"):

    lepts = lepton_filter(lepton_type, min_pt=15. * GeV)
    gamma = make_photons(photon_type=photon_type)
    decay_Wgamma = ('[W+ -> %s+ gamma]cc' % lepton_type)

    line_alg = make_DiBoson_Base(
        particles=[lepts, gamma],
        min_mass=20. * GeV,
        decay_descriptor=decay_Wgamma)

    return line_alg


@configurable
def make_Zllgamma_cand(lepton_type="mu", photon_type="DD"):

    zcand = Z_To_lepts_filter(lepton_type=lepton_type, min_pt=15. * GeV)
    gamma = make_photons(photon_type=photon_type)
    decay_Zgamma = 'H_10 -> Z0 gamma'

    line_alg = make_DiBoson(
        particles=[zcand, gamma],
        min_mass=60. * GeV,
        decay_descriptor=decay_Zgamma)

    return line_alg
