##############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Set of Hlt2-lines selecting Z->mu^+ mu^- candidates for tracking efficiency studies in Run 3:
 - Combination of long 'tag' track and partially reconstructed 'probe' track
 - Matching of probe with long track
 - running without UT: use the VeloMuon lines
"""
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable
from PyConf.Algorithms import MuonProbeToLongMatcher

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon
from Hlt2Conf.probe_muons import make_velomuon_muons, make_downstream_muons, make_muonut_muons
from Hlt2Conf.lines.trackeff.DiMuonTrackEfficiency import make_LongFilter

all_lines = {}

#use Velo+Muon track for efficiency of SciFi(+UT)
VeloMuon_cuts = {
    'min_ProbePT': 10 * GeV,
    'min_TagPT': 10 * GeV,
    'min_Z0mass': 50 * GeV,
    'max_Z0mass': 120 * GeV,
    'checkVP': True,
    'checkFT': False,
    'checkUT': False,
    'checkMuon': True,
    'require_ismuon': False,
}
#use SciFi+UT+Muon track for efficiency of Velo
Downstream_cuts = {
    'min_ProbePT': 10 * GeV,
    'min_TagPT': 10 * GeV,
    'min_Z0mass': 50 * GeV,
    'max_Z0mass': 120 * GeV,
    'checkVP': False,
    'checkFT': True,
    'checkUT': True,
    'checkMuon': True,
    'require_ismuon': True,
}
#use UT+Muon track for efficiency of Velo+SciFi
UTMuon_cuts = {
    'min_ProbePT': 10 * GeV,
    'min_TagPT': 10 * GeV,
    'min_Z0mass': 50 * GeV,
    'max_Z0mass': 120 * GeV,
    'checkVP': False,
    'checkFT': False,
    'checkUT': True,
    'checkMuon': True,
    'require_ismuon': False,
}


@configurable
def make_Z0Combiner(particles, probe_charge, get_cuts, line):
    combination_cut = F.require_all(
        in_range(get_cuts['min_Z0mass'], F.MASS, get_cuts['max_Z0mass']), )
    mother_cut = F.require_all(
        in_range(get_cuts['min_Z0mass'], F.MASS, get_cuts['max_Z0mass']), )

    decay_descriptor = "Z0 -> mu- mu+" if probe_charge == 1 else "Z0 -> mu+ mu-"
    return ParticleCombiner(
        Inputs=particles,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cut,
        CompositeCut=mother_cut,
        name=f"ZToMuMu_TrackEff_Z_{line}_{{hash}}")


@configurable
def make_Tag_Filter(particles, probe_charge, get_cuts, line):
    code = F.require_all(
        (F.CHARGE < 0) if probe_charge == 1 else (F.CHARGE > 0),
        F.PT > get_cuts['min_TagPT'],
        F.ISMUON(),
    )
    return ParticleFilter(
        particles,
        F.FILTER(code),
        name=f"ZToMuMu_TrackEff_TagTrack_{line}_{{hash}}")


@configurable
def make_Probe_Filter(particles, probe_charge, get_cuts, line):
    code = F.require_all(
        (F.CHARGE > 0) if probe_charge == 1 else (F.CHARGE < 0),
        F.PT > get_cuts['min_ProbePT'],
    )
    if get_cuts['require_ismuon']:
        code = F.require_all(code, F.ISMUON())
    return ParticleFilter(
        particles,
        F.FILTER(code),
        name=f"ZToMuMu_TrackEff_ProbeTrack_{line}_{{hash}}")


@configurable
def create_Tag_Line(name, line, prescale, persistreco, probe_charge, get_cuts,
                    particles):
    probe_muons = make_Probe_Filter(particles, probe_charge, get_cuts, line)
    tag_muons = make_Tag_Filter(make_ismuon_long_muon(), probe_charge,
                                get_cuts, line)
    Z0 = make_Z0Combiner([tag_muons, probe_muons], probe_charge, get_cuts,
                         line)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [tag_muons, probe_muons, Z0],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"))


@configurable
def create_Match_Line(name, line, prescale, persistreco, probe_charge,
                      get_cuts, particles):
    probe_muons = make_Probe_Filter(particles, probe_charge, get_cuts, line)
    tag_muons = make_Tag_Filter(make_ismuon_long_muon(), probe_charge,
                                get_cuts, line)
    long_track = make_LongFilter(
        make_ismuon_long_muon(),
        probe_charge,
    )
    Z0 = make_Z0Combiner([tag_muons, probe_muons], probe_charge, get_cuts,
                         line)
    matcher = MuonProbeToLongMatcher(
        TwoBodyComposites=Z0,
        LongTracks=long_track,
        checkVP=get_cuts['checkVP'],
        checkFT=get_cuts['checkFT'],
        checkUT=get_cuts['checkUT'],
        checkMuon=get_cuts['checkMuon'])
    Z0_matched = matcher.MatchedComposites
    long_matched = matcher.MatchedLongTracks
    long_save = make_LongFilter(long_matched, probe_charge)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [tag_muons, probe_muons, Z0, Z0_matched],
        extra_outputs=[("LongMatched", long_save)],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"))


for method, get_cuts, myparticles in zip(
    ['VeloMuon', 'Downstream', 'UTMuon'],
    [VeloMuon_cuts, Downstream_cuts, UTMuon_cuts],
    [make_velomuon_muons, make_downstream_muons, make_muonut_muons]):
    for charge, probe in zip([1, -1], ['mup', 'mum']):

        @register_line_builder(all_lines)
        @configurable
        def LineWithTagging(
                name=f"Hlt2QEE_TrackEff_ZToMuMu_{method}_{probe}_Tag",
                prescale=1,
                persistreco=True,
                probe_charge=charge,
                get_cuts=get_cuts,
                get_particles=myparticles):
            return create_Tag_Line(name, method + '_' + probe, prescale,
                                   persistreco, probe_charge, get_cuts,
                                   get_particles())

        @register_line_builder(all_lines)
        @configurable
        def LineWithMatching(
                name=f"Hlt2QEE_TrackEff_ZToMuMu_{method}_{probe}_Match",
                prescale=1,
                persistreco=True,
                probe_charge=charge,
                get_cuts=get_cuts,
                get_particles=myparticles):
            return create_Match_Line(name, method + '_' + probe, prescale,
                                     persistreco, probe_charge, get_cuts,
                                     get_particles())
