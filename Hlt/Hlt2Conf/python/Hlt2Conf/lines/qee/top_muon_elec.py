###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the top lines
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable
from PyConf.utilities import ConfigurationError

from Hlt2Conf.algorithms import ParticleContainersMerger
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Hlt2Conf.lines.qee.qee_builders import muon_filter, elec_filter, make_jets


@configurable
def make_ttbar_MuE(inputmuons,
                   inputelectrons,
                   min_mass=40. * GeV,
                   decay_descriptor='[Z0 -> mu+ e-]cc'):
    #Muon-electron combination with only a mass cut

    # mass region of MuE
    combination_code = (F.MASS > min_mass)

    return ParticleCombiner([inputmuons, inputelectrons],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code)


@configurable
def combine_ttbar_MuEBjet(inputmuons, inputelectrons, inputjets, min_mass,
                          decay_descriptor):
    #Muon-electron-bjet  combination with only a mass cut

    # mass region of MuE_Bjet
    combination_code = (F.MASS > min_mass)

    return ParticleCombiner([inputmuons, inputelectrons, inputjets],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code)


@configurable
def make_ttbar_MuEBjet(
        inputmuons,
        inputelectrons,
        inputjets,
        min_mass=40. * GeV,
        decay_descriptors=['Z0 -> mu+ e- CELLjet', 'Z0 -> mu- e+ CELLjet'],
        name='TTbarMerger_{hash}'):
    #iterative procedure for several decay descriptors
    if len(decay_descriptors) == 0:
        raise ConfigurationError("Decay descriptors not given")
    cand = []
    for descriptor in decay_descriptors:
        cand.append(
            combine_ttbar_MuEBjet(
                inputmuons=inputmuons,
                inputelectrons=inputelectrons,
                inputjets=inputjets,
                min_mass=min_mass,
                decay_descriptor=descriptor))

    return ParticleContainersMerger(cand, name=name)


@configurable
def make_ttbar_MuE_cand():

    muons = muon_filter(min_pt=20. * GeV)
    electrons = elec_filter(min_pt=20. * GeV)
    line_alg = make_ttbar_MuE(muons, electrons, 40 * GeV)

    return line_alg


@configurable
def make_ttbar_MuEBjet_cand():

    muons = muon_filter(min_pt=20. * GeV)
    electrons = elec_filter(min_pt=20. * GeV)
    bjets = make_jets(min_pt=10 * GeV, tags='SV')
    line_alg = make_ttbar_MuEBjet(muons, electrons, bjets, 40 * GeV)

    return line_alg
