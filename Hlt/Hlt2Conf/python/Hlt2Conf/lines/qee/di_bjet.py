###############################################################################
# (c) Copyright 2020-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs
from RecoConf.event_filters import require_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_jets import make_pf_particles, build_jets, tag_jets
from Hlt2Conf.lines.jets.topobits import make_topo_2body, make_topo_2body_with_svtag

all_lines = {}


@configurable
def make_jets(name='SimpleJets_{hash}',
              pt_min=10 * GeV,
              JetsByVtx=False,
              tags=None):
    pflow = make_pf_particles()
    jets = build_jets(pflow, JetsByVtx, name='JetBuilder' + name)
    tagObjs = None
    if tags == 'SV':
        tagObjs = make_topo_2body_with_svtag()
    elif tags == 'TOPO':
        tagObjs = make_topo_2body()

    if tags is not None:
        taggedjets = tag_jets(
            jets, tagObjs, useflightdirection=True, name="Tags" + name)
        jets = taggedjets

    code = F.require_all(F.IS_ABS_ID("CELLjet"), F.PT > pt_min)
    return ParticleFilter(jets, F.FILTER(code), name=name)


@configurable
def make_dijets(tagpair=(None, None), prod_pt_min=10 * GeV, min_dphi=0.0):
    """Make two-jet combinations
    """
    jets = make_jets(pt_min=prod_pt_min, tags=tagpair[0])
    tagfilt0 = F.ALL
    tagfilt1 = F.ALL

    if (min_dphi > 0.0):
        delta = F.ADJUST_ANGLE @ (F.CHILD(1, F.PHI) - F.CHILD(2, F.PHI))
        combination_code = F.require_all(tagfilt0, tagfilt1,
                                         F.ABS @ delta > min_dphi)
    else:
        combination_code = F.require_all(tagfilt0, tagfilt1)

    return ParticleCombiner(
        Inputs=[jets, jets],
        DecayDescriptor="CLUSjet -> CELLjet CELLjet",
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
        ParticleCombiner="ParticleAdder")


@configurable
def make_Trijets(tagpair=(None, None, None), prod_pt_min=10 * GeV):
    """Make three-jet combinations
    """
    jets = make_jets(pt_min=prod_pt_min, tags=tagpair[0])
    tagfilt0 = F.ALL
    tagfilt1 = F.ALL
    tagfilt2 = F.ALL

    combination_code = F.require_all(tagfilt0, tagfilt1, tagfilt2)

    return ParticleCombiner(
        Inputs=[jets, jets, jets],
        DecayDescriptor="CLUSjet -> CELLjet CELLjet CELLjet",
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
        ParticleCombiner="ParticleAdder")


##### Functions for sprucing lines #####
@configurable
def make_SVTagDijets_cand():
    line_alg = make_dijets(
        tagpair=('SV', 'SV'), prod_pt_min=30 * GeV, min_dphi=1.5)
    return line_alg


@configurable
def make_Dijets_cand():
    line_alg = make_dijets(
        tagpair=(None, None), prod_pt_min=30 * GeV, min_dphi=1.5)
    return line_alg


@configurable
def make_Trijets_cand():
    line_alg = make_Trijets(tagpair=(None, None, None), prod_pt_min=30 * GeV)
    return line_alg


@configurable
def make_TrijetsTwoSVTag_cand():
    line_alg = make_Trijets(tagpair=('SV', 'SV', None), prod_pt_min=30 * GeV)
    return line_alg


############ SV Tag ####################
@register_line_builder(all_lines)
@configurable
def diBjet10GeV_line(name='Hlt2QEE_DiSVTagJet10GeV',
                     prescale=0.025,
                     persistreco=True):
    jets = make_dijets(
        tagpair=('SV', 'SV'), prod_pt_min=10 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diBjet15GeV_line(name='Hlt2QEE_DiSVTagJet15GeV',
                     prescale=0.1,
                     persistreco=True):
    jets = make_dijets(
        tagpair=('SV', 'SV'), prod_pt_min=15 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diBjet20GeV_line(name='Hlt2QEE_DiSVTagJet20GeV',
                     prescale=1.0,
                     persistreco=True):
    jets = make_dijets(
        tagpair=('SV', 'SV'), prod_pt_min=20 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diBjet25GeV_line(name='Hlt2QEE_DiSVTagJet25GeV',
                     prescale=1.0,
                     persistreco=True):
    jets = make_dijets(
        tagpair=('SV', 'SV'), prod_pt_min=25 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diBjet30GeV_line(name='Hlt2QEE_DiSVTagJet30GeV',
                     prescale=1.0,
                     persistreco=True):
    jets = make_dijets(
        tagpair=('SV', 'SV'), prod_pt_min=30 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diBjet35GeV_line(name='Hlt2QEE_DiSVTagJet35GeV',
                     prescale=1.0,
                     persistreco=True):
    jets = make_dijets(
        tagpair=('SV', 'SV'), prod_pt_min=35 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


############ Topo Tag ####################


@register_line_builder(all_lines)
@configurable
def diTopojet10GeV_line(name='Hlt2QEE_DiTopoTagJet10GeV',
                        prescale=0.005,
                        persistreco=True):
    jets = make_dijets(
        tagpair=('TOPO', 'TOPO'), prod_pt_min=10 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopojet15GeV_line(name='Hlt2QEE_DiTopoTagJet15GeV',
                        prescale=0.01,
                        persistreco=True):
    jets = make_dijets(
        tagpair=('TOPO', 'TOPO'), prod_pt_min=15 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopojet20GeV_line(name='Hlt2QEE_DiTopoTagJet20GeV',
                        prescale=0.05,
                        persistreco=True):
    jets = make_dijets(
        tagpair=('TOPO', 'TOPO'), prod_pt_min=20 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopojet25GeV_line(name='Hlt2QEE_DiTopoTagJet25GeV',
                        prescale=0.5,
                        persistreco=True):
    jets = make_dijets(
        tagpair=('TOPO', 'TOPO'), prod_pt_min=25 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopojet30GeV_line(name='Hlt2QEE_DiTopoTagJet30GeV',
                        prescale=0.5,
                        persistreco=True):
    jets = make_dijets(
        tagpair=('TOPO', 'TOPO'), prod_pt_min=30 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopojet35GeV_line(name='Hlt2QEE_DiTopoTagJet35GeV',
                        prescale=1.0,
                        persistreco=True):
    jets = make_dijets(
        tagpair=('TOPO', 'TOPO'), prod_pt_min=35 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


############# Light jets ####################


@register_line_builder(all_lines)
@configurable
def diLightjet10GeV_line(name='Hlt2QEE_DiLightJet10GeV',
                         prescale=0.001,
                         persistreco=True):
    jets = make_dijets(prod_pt_min=10 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diLightjet15GeV_line(name='Hlt2QEE_DiLightJet15GeV',
                         prescale=0.01,
                         persistreco=True):
    jets = make_dijets(prod_pt_min=15 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diLightjet20GeV_line(name='Hlt2QEE_DiLightJet20GeV',
                         prescale=0.05,
                         persistreco=True):
    jets = make_dijets(prod_pt_min=20 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diLightjet25GeV_line(name='Hlt2QEE_DiLightJet25GeV',
                         prescale=0.25,
                         persistreco=True):
    jets = make_dijets(prod_pt_min=25 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diLightjet30GeV_line(name='Hlt2QEE_DiLightJet30GeV',
                         prescale=0.5,
                         persistreco=True):
    jets = make_dijets(prod_pt_min=30 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diLightjet35GeV_line(name='Hlt2QEE_DiLightJet35GeV',
                         prescale=1.0,
                         persistreco=True):
    jets = make_dijets(prod_pt_min=35 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        persistreco=persistreco)
