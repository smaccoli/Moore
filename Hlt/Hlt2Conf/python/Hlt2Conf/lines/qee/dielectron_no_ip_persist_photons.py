###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of an HLT2 line to preserve dielectron candidates and nearby photons
in order to reconstruct, later, {pi0, eta} -> e+ e- gamma.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV

from PyConf import configurable
from PyConf.Algorithms import WeightedRelTableAlg

from Moore.lines import Hlt2Line
from Moore.config import register_line_builder

from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter
from Hlt2Conf.standard_particles import make_long_electrons_no_brem, make_detached_dielectron_with_brem, make_photons

all_lines = {}


@configurable
def electron_filter(min_pt=0. * MeV):
    """ An electron filter: PT"""
    code = F.require_all(F.PT > min_pt)

    return ParticleFilter(make_long_electrons_no_brem(), F.FILTER(code))


@configurable
def photon_filter(name='inputphotons_{hash}',
                  make_particles=make_photons,
                  et_min=1000 * MeV,
                  e_min=0 * MeV,
                  is_photon_min=0.0,
                  is_not_h_min=0.2,
                  shower_shape_min=0):

    code = F.require_all(F.PT > et_min, F.P > e_min,
                         F.IS_PHOTON > is_photon_min,
                         F.IS_NOT_H > is_not_h_min,
                         F.CALO_NEUTRAL_SHOWER_SHAPE > shower_shape_min)

    return ParticleFilter(make_particles(), F.FILTER(code))


@register_line_builder(all_lines)
@configurable
def dielectron_noip_sp_prompt_line(
        name='Hlt2QEE_DiElectronNoIPPrompt_PersistPhotons',
        prescale=1,
        persistreco=True,
        make_pvs=make_pvs):
    """Aiming for pi0/eta -> gamma e+ e-. Label the dielectron as J/psi(1S) [during
    brem-recovery-reconstruction of dielectron pair] and save photons inside a cone
    around the dielectron candidate"""
    pvs = make_pvs()

    dielectrons = make_detached_dielectron_with_brem(
        electron_filter,
        dielectron_ID="J/psi(1S)",
        PIDe_min=0.,
        pt_e=0.,
        trghostprob=0.25,
        pt_diE=0 * MeV,
        m_diE_min=5 * MeV,
        m_diE_max=300 * MeV,
        adocachi2cut=30,
        bpvvdchi2=0,
        vfaspfchi2ndof=10,
        opposite_sign=True)

    code = F.require_all(F.BPVIPCHI2(pvs) < 2.)
    dielectrons_prompt = ParticleFilter(dielectrons, F.FILTER(code))

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_prompt,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dielectrons_prompt],
        persistreco=persistreco,
        extra_outputs=[('NoIpDielectrons_prompt_Photons',
                        photons_table.OutputRelations)],
        prescale=prescale,
        #hlt1_filter_code="HLT_PASS_RE('Hlt1LowMassNoipDielectronDecision')"
    )


@register_line_builder(all_lines)
@configurable
def dielectron_noip_sp_displaced_line(
        name='Hlt2QEE_DiElectronNoIPDisplaced_PersistPhotons',
        prescale=1,
        persistreco=True,
        make_pvs=make_pvs):
    """Aiming for pi0/eta -> gamma e+ e-. Label the dielectron as J/psi(1S) [during
    brem-recovery-reconstruction of dielectron pair] and save photons inside a cone
    around the dielectron candidate"""
    pvs = make_pvs()

    dielectrons = make_detached_dielectron_with_brem(
        electron_filter,
        dielectron_ID="J/psi(1S)",
        PIDe_min=0.,
        pt_e=0.,
        trghostprob=0.25,
        pt_diE=0 * MeV,
        m_diE_min=5 * MeV,
        m_diE_max=300 * MeV,
        adocachi2cut=30,
        bpvvdchi2=0,
        vfaspfchi2ndof=10,
        opposite_sign=True)

    code = F.require_all(F.BPVIPCHI2(pvs) > 2.)
    dielectrons_displaced = ParticleFilter(dielectrons, F.FILTER(code))

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_displaced,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), dielectrons_displaced],
        persistreco=persistreco,
        extra_outputs=[('NoIpDielectrons_displaced_Photons',
                        photons_table.OutputRelations)],
        prescale=prescale,
        #hlt1_filter_code="HLT_PASS_RE('Hlt1LowMassNoipDielectronDecision')"
    )


@register_line_builder(all_lines)
@configurable
def dielectron_noip_sp_prompt_same_sign_line(
        name='Hlt2QEE_DiElectronNoIPPrompt_PersistPhotonsSS',
        prescale=1,
        persistreco=True,
        make_pvs=make_pvs):
    """Aiming for pi0/eta -> gamma e+ e+ or gamma e- e-. Label the doubly-charged dielectron
    as J/psi(1S) and save nearby photons"""
    pvs = make_pvs()

    dielectrons = make_detached_dielectron_with_brem(
        electron_filter,
        dielectron_ID="J/psi(1S)",
        PIDe_min=0.,
        pt_e=0.,
        trghostprob=0.25,
        pt_diE=0 * MeV,
        m_diE_min=5 * MeV,
        m_diE_max=300 * MeV,
        adocachi2cut=30,
        bpvvdchi2=0,
        vfaspfchi2ndof=10,
        opposite_sign=False)

    code = F.require_all(F.BPVIPCHI2(pvs) < 2.)
    dielectrons_prompt = ParticleFilter(dielectrons, F.FILTER(code))

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_prompt,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dielectrons_prompt],
        persistreco=persistreco,
        extra_outputs=[('NoIpDielectrons_prompt_ss_Photons',
                        photons_table.OutputRelations)],
        prescale=prescale,
        #hlt1_filter_code="HLT_PASS_RE('Hlt1LowMassNoipDielectron_SSDecision')"
    )


@register_line_builder(all_lines)
@configurable
def dielectron_noip_sp_displaced_same_sign_line(
        name='Hlt2QEE_DiElectronNoIPDisplaced_PersistPhotonsSS',
        prescale=1,
        persistreco=True,
        make_pvs=make_pvs):
    """Aiming for pi0/eta -> gamma e+ e+ or gamma e- e-. Label the doubly-charged dielectron
    as J/psi(1S) and save nearby photons"""
    pvs = make_pvs()

    dielectrons = make_detached_dielectron_with_brem(
        electron_filter,
        dielectron_ID="J/psi(1S)",
        PIDe_min=0.,
        pt_e=0.,
        trghostprob=0.25,
        pt_diE=0 * MeV,
        m_diE_min=5 * MeV,
        m_diE_max=300 * MeV,
        adocachi2cut=30,
        bpvvdchi2=0,
        vfaspfchi2ndof=10,
        opposite_sign=False)

    code = F.require_all(F.BPVIPCHI2(pvs) > 2.)
    dielectrons_displaced = ParticleFilter(dielectrons, F.FILTER(code))

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_displaced,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), dielectrons_displaced],
        persistreco=persistreco,
        extra_outputs=[('NoIpDielectrons_displaced_ss_Photons',
                        photons_table.OutputRelations)],
        prescale=prescale,
        #hlt1_filter_code="HLT_PASS_RE('Hlt1LowMassNoipDielectron_SSDecision')"
    )
