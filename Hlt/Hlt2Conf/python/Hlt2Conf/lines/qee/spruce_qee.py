###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definitions of the QEE sprucing lines.
"""

from PyConf import configurable

from Moore.config import register_line_builder, SpruceLine

from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs
from GaudiKernel.SystemOfUnits import GeV

from Hlt2Conf.lines.qee.high_mass_dimuon import make_Z_cand, make_Zss_cand
from Hlt2Conf.lines.qee.high_mass_dielec import make_Zee_cand, make_Zeess_cand
from Hlt2Conf.lines.qee.vjets import make_Z0jet_cand, make_ssdimuonjet_cand, make_Z0SVjet_cand, make_Wjet_cand, make_WSVjet_cand, make_Z0jetjet_cand, make_Z0SVjetSVjet_cand, make_Wjetjet_cand, make_WSVjetSVjet_cand
from Hlt2Conf.lines.qee.vjets import make_Z0_elec_jet_cand, make_Z0_elec_SVjet_cand, make_W_elec_jet_cand, make_W_elec_SVjet_cand, make_Z0_elec_jetjet_cand, make_Z0_elec_SVjetSVjet_cand, make_W_elec_jetjet_cand, make_W_elec_SVjetSVjet_cand
from Hlt2Conf.lines.qee.top_muon_elec import make_ttbar_MuE_cand, make_ttbar_MuEBjet_cand
from Hlt2Conf.lines.qee.diboson import make_WW_emu_cand, make_WZ_Zll_Wlnu_cand, make_ZZ_Zllplusll_cand, make_Wleptgamma_cand, make_Zllgamma_cand
from Hlt2Conf.lines.qee.wz_boson_rare_decays import make_WZRareDecay_HadronGamma_cand, make_WRareDecay_DiMuonDst_cand
from Hlt2Conf.lines.qee.qee_builders import muon_filter

sprucing_lines = {}


@register_line_builder(sprucing_lines)
@configurable
def z_to_mu_mu_sprucing_line(name='SpruceQEE_ZToMuMu', prescale=1):
    """Z0 boson decay to two muons line"""

    line_alg = make_Z_cand(with_PID=True)
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_to_mu_mu_nopid_sprucing_line(name='SpruceQEE_ZToMuMuNoPID', prescale=1):
    line_alg = make_Z_cand(with_PID=False)
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_to_mu_mu_ss_sprucing_line(name='SpruceQEE_ZToMuMuSS', prescale=1):
    """Z0 boson decay to two same sign muons line"""

    line_alg = make_Zss_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_to_e_e_sprucing_line(name='SpruceQEE_ZToEE', prescale=1):
    """Z0 boson decay to two electrons line"""

    line_alg = make_Zee_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_to_e_e_ss_sprucing_line(name='SpruceQEE_ZToEESS', prescale=1):
    """Z0 boson decay to two same sign electrons line"""

    line_alg = make_Zeess_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_jet_sprucing_line(name='SpruceQEE_ZJet', prescale=1):
    """Z0 boson decay to two muons + jet line"""

    line_alg = make_Z0jet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_jet_persistreco_sprucing_line(name='SpruceQEE_ZJetPersistReco',
                                    prescale=1):
    """Z0 boson decay to two muons + jet line full event information are persisted"""

    line_alg = make_Z0jet_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def ssdimuon_jet_persistreco_sprucing_line(
        name='SpruceQEE_SSDiMuonJetPersistReco', prescale=1):
    """same sign dimuons + jet line full event information are persisted"""

    line_alg = make_ssdimuonjet_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_svjet_sprucing_line(name='SpruceQEE_ZSVJet', prescale=1):
    """Z0 boson decay to two muons + SV jet line"""

    line_alg = make_Z0SVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_jetjet_sprucing_line(name='SpruceQEE_ZJetJet', prescale=1):
    """Z0 boson decay to two muons + jet jet line"""

    line_alg = make_Z0jetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_svjetsvjet_sprucing_line(name='SpruceQEE_ZSVJetSVJet', prescale=1):
    """Z0 boson decay to two muons + SV jet SV jet line"""

    line_alg = make_Z0SVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def w_jet_sprucing_line(name='SpruceQEE_WJet', prescale=1):
    """High pt muon + jet line"""

    line_alg = make_Wjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def w_svjet_sprucing_line(name='SpruceQEE_WSVJet', prescale=1):
    """High pt muon + SV jet line"""

    line_alg = make_WSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def w_jetjet_sprucing_line(name='SpruceQEE_WJetJet', prescale=1):
    """high pt muon + jet jet line"""

    line_alg = make_Wjetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def w_svjetsvjet_sprucing_line(name='SpruceQEE_WSVJetSVJet', prescale=1):
    """high pt muon + SV jet SV jet line"""

    line_alg = make_WSVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def zee_jet_sprucing_line(name='SpruceQEE_ZEEJet', prescale=1):
    """Z0 boson decay to two electrons + jet line"""

    line_alg = make_Z0_elec_jet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def zee_svjet_sprucing_line(name='SpruceQEE_ZEESVJet', prescale=1):
    """Z0 boson decay to two electrons + SV jet line"""

    line_alg = make_Z0_elec_SVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def zee_jetjet_sprucing_line(name='SpruceQEE_ZEEJetJet', prescale=1):
    """Z0 boson decay to two electrons + jet jet line"""

    line_alg = make_Z0_elec_jetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def zee_svjetsvjet_sprucing_line(name='SpruceQEE_ZEESVJetSVJet', prescale=1):
    """Z0 boson decay to two electrons + SV jet SV jet line"""

    line_alg = make_Z0_elec_SVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def we_jet_sprucing_line(name='SpruceQEE_WEJet', prescale=1):
    """High pt electron + jet line"""

    line_alg = make_W_elec_jet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def we_svjet_sprucing_line(name='SpruceQEE_WESVJet', prescale=1):
    """High pt electron + SV jet line"""

    line_alg = make_W_elec_SVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def we_jetjet_sprucing_line(name='SpruceQEE_WEJetJet', prescale=1):
    """high pt electrons + jet jet line"""

    line_alg = make_W_elec_jetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def we_svjetsvjet_sprucing_line(name='SpruceQEE_WESVJetSVJet', prescale=1):
    """high pt electrons + SV jet SV jet line"""

    line_alg = make_W_elec_SVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def ttbar_to_mu_e_sprucing_line(name='SpruceQEE_TTbarToMuE', prescale=1):
    """ttbar to muon-electron line"""

    line_alg = make_ttbar_MuE_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def ttbar_to_mu_e_bjet_sprucing_line(name='SpruceQEE_TTbarToMuEBjet',
                                     prescale=1):
    """ttbar to muon-electron-bjet line"""

    line_alg = make_ttbar_MuEBjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def WW_to_e_mu_sprucing_line(name='SpruceQEE_WWToMuE', prescale=1):
    """WW to e mu nu nu line"""

    line_alg = make_WW_emu_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


for lepton_type1, lepton_type2, line_suffix in zip(
    ["mu", "mu", "e", "e"], ["e", "mu", "mu", "e"],
    ["MuMuE", "MuMuMu", "EEMu", "EEE"]):

    @register_line_builder(sprucing_lines)
    @configurable
    def DiBoson_WZ_lll_sprucing_line(name=f'SpruceQEE_WZTo{line_suffix}',
                                     lepton_type1=f'{lepton_type1}',
                                     lepton_type2=f'{lepton_type2}',
                                     prescale=1):
        f"""WZ to {lepton_type1} {lepton_type1} {lepton_type2} line"""

        line_alg = make_WZ_Zll_Wlnu_cand(
            lepton_type1=lepton_type1, lepton_type2=lepton_type2)
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [line_alg],
            prescale=prescale)


for lepton_type1, lepton_type2, line_suffix in zip(
    ["mu", "mu", "e", "e"], ["mu", "e", "e", "mu"],
    ["MuMuMuMu", "MuMuEE", "EEEE", "EEMuMu"]):

    @register_line_builder(sprucing_lines)
    @configurable
    def ZZ_to_llll_sprucing_line(name=f'SpruceQEE_ZZTo{line_suffix}',
                                 lepton_type1=f'{lepton_type1}',
                                 lepton_type2=f'{lepton_type2}',
                                 prescale=1):
        f"""ZZ to {lepton_type1} {lepton_type1} {lepton_type2} {lepton_type2} line"""

        line_alg = make_ZZ_Zllplusll_cand(
            lepton_type1=lepton_type1, lepton_type2=lepton_type2)
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [line_alg],
            prescale=prescale)


for lepton_type, lepton_suffix in zip(["mu", "e"], ["Mu", "E"]):
    for photon_type, line_suffix in zip(["LL", "DD", "All"], ["LL", "DD", ""]):

        @register_line_builder(sprucing_lines)
        @configurable
        def WGamma_to_lgamma_sprucing_line(
                name=f'SpruceQEE_WGammaTo{lepton_suffix}Photon{line_suffix}',
                lepton_type=f'{lepton_type}',
                photon_type=f'{photon_type}',
                prescale=1):
            f"""Wgamma to {lepton_type} gamma ({photon_type}) line"""

            line_alg = make_Wleptgamma_cand(
                lepton_type=lepton_type, photon_type=photon_type)
            return SpruceLine(
                name=name,
                algs=upfront_reconstruction() + [line_alg],
                prescale=prescale)

        @register_line_builder(sprucing_lines)
        @configurable
        def ZGamma_to_mu_mu_gamma_LL_sprucing_line(
                name=f'SpruceQEE_ZGammaTo{lepton_suffix}{lepton_suffix}Photon{line_suffix}',
                lepton_type=f'{lepton_type}',
                photon_type=f'{photon_type}',
                prescale=1):
            f"""Zgamma to {lepton_suffix} {lepton_suffix} gamma {line_suffix}"""

            line_alg = make_Zllgamma_cand(
                lepton_type=lepton_type, photon_type=photon_type)
            return SpruceLine(
                name=name,
                algs=upfront_reconstruction() + [line_alg],
                prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def W_to_JPsi_Dsp_Mu_Mu_K_K_Pi_sprucing_line(name='SpruceQEE_JPsiDsp',
                                             prescale=1):
    """W -> JPsi D_s+"""

    line_alg = make_WRareDecay_DiMuonDst_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


for decay_type in [
        "JetGamma", "DpGamma", "DspGamma", "LcpGamma", "XicpGamma", "BpGamma",
        "BcpGamma", "JPsiGamma", "UpsilonGamma", "DzGamma", "Xic0Gamma",
        "B0Gamma", "BsGamma", "LbGamma"
]:
    for photon_type, line_suffix in zip(["LL", "DD", "All"], ["LL", "DD", ""]):

        @register_line_builder(sprucing_lines)
        @configurable
        def WZRareDecay_HadronGamma_sprucing_line(
                name=f'SpruceQEE_{decay_type}_{line_suffix}',
                decay_type=f'{decay_type}',
                photon_type=f'{photon_type}',
                prescale=1):
            f"""{decay_type} ({photon_type} photons) spruce line"""

            pvs = make_pvs()
            line_alg = make_WZRareDecay_HadronGamma_cand(
                pvs, photon_type=photon_type, decay_type=decay_type)

            return SpruceLine(
                name=name,
                algs=upfront_reconstruction() + [require_pvs(pvs), line_alg],
                prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def w_to_mu_numu_sprucing_line(name='SpruceQEE_WToMuNuMu', prescale=1):
    """W -> Mu NuMu, passthrough after Hlt2SingleHighPtMuon"""

    line_alg = muon_filter(15. * GeV)
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)
