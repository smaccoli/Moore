###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the QEE selection lines
"""

from . import hlt2_qee
from . import spruce_qee
from . import spruce_qee_jets

hlt2_turbo_lines = {}
hlt2_turbo_lines.update(hlt2_qee.turbo_lines)

hlt2_full_lines = {}
hlt2_full_lines.update(hlt2_qee.full_lines)

all_lines = {}
all_lines.update(hlt2_turbo_lines)
all_lines.update(hlt2_full_lines)

sprucing_lines = {}
sprucing_lines.update(spruce_qee.sprucing_lines)
sprucing_lines.update(spruce_qee_jets.sprucing_jet_lines)
