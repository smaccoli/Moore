###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definitions of the jets QEE sprucing lines.
"""

from PyConf import configurable

from Moore.config import register_line_builder, SpruceLine

from RecoConf.reconstruction_objects import upfront_reconstruction

from Hlt2Conf.lines.qee.di_bjet import make_SVTagDijets_cand, make_Dijets_cand, make_Trijets_cand, make_TrijetsTwoSVTag_cand

sprucing_jet_lines = {}


@register_line_builder(sprucing_jet_lines)
@configurable
def SVTagDijets_sprucing_line(name='SpruceQEE_SVTagDijets', prescale=1):
    """SV Tag Dijets sprucing line"""

    line_alg = make_SVTagDijets_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_jet_lines)
@configurable
def Dijets_sprucing_line(name='SpruceQEE_Dijets', prescale=1):
    """Dijets sprucing line"""

    line_alg = make_Dijets_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_jet_lines)
@configurable
def Trijets_sprucing_line(name='SpruceQEE_Trijets', prescale=1):
    """Trijets sprucing line"""

    line_alg = make_Trijets_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_jet_lines)
@configurable
def TrijetsTwoSVTag_sprucing_line(name='SpruceQEE_TrijetsTwoSVTag',
                                  prescale=1):
    """Trijets sprucing line"""

    line_alg = make_TrijetsTwoSVTag_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)
