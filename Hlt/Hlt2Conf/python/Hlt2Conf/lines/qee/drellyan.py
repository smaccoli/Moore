###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""These lines define the dimuon lines used to measure Drell-Yan
production. These consist of multiple lines, split by mass region,
the overall charge of the dilepton candidate, and some control regions
based on the displacedness of the candidate.

Cuts applied are inspired by the Run-2 DY analysis.

If there are default function arguments, the cut values applied
are set/configured there. If they are absent, they are intended
to be set downstream.

Contact: Laurent Dufour <laurent.dufour@cern.ch>
"""
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, mm

from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs
from RecoConf.event_filters import require_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon

all_lines = {}

### Line configuration
mass_ranges = [
    [2.9 * GeV, 5.0 * GeV, "VLowMass", 0.2,
     True],  # first two arguments: min/max
    [5.0 * GeV, 10. * GeV, "LowMass", 0.5, True],  # third argument: name
    [10. * GeV, 400 * GeV, "Nominal", 1.0, True]
]
# fourth argument: prescale
# last argument: persist reco on/off


@configurable
def filter_muons(
        particles,
        pvs,
        min_ip,
        max_ip,
        min_probnnmu,
        min_pt=1.4 * GeV,
        min_eta=1.9,
        max_eta=4.9,
        min_p=20. * GeV,
):
    muon_requirements = F.require_all(
        in_range(min_eta, F.ETA, max_eta), F.PT >= min_pt, F.P >= min_p,
        F.PROBNN_MU >= min_probnnmu, in_range(min_ip, F.MINIP(pvs), max_ip))

    return ParticleFilter(particles, F.FILTER(muon_requirements))


@configurable
def combine_muons_for_decay_descriptor(decay_descriptor,
                                       pvs,
                                       name,
                                       min_mass,
                                       max_mass,
                                       min_ip,
                                       max_ip,
                                       min_probnnmu,
                                       max_doca=0.15 * mm,
                                       vchi2pdof_max=16):
    all_muons = make_ismuon_long_muon()

    filtered_muons = filter_muons(
        all_muons,
        pvs,
        min_probnnmu=min_probnnmu,
        max_ip=max_ip,
        min_ip=min_ip)

    combination_cuts = F.require_all(
        in_range(min_mass, F.MASS, max_mass),
        F.DOCA(1, 2) < max_doca)
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max)

    return ParticleCombiner([filtered_muons, filtered_muons],
                            name=name,
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_cuts,
                            CompositeCut=vertex_code)


@configurable
def make_dimuon_candidates(is_same_sign, name, **kwargs):
    if is_same_sign:
        return combine_muons_for_decay_descriptor(
            decay_descriptor="[Z0 -> mu+ mu+]cc", name=name, **kwargs)
    else:
        return combine_muons_for_decay_descriptor(
            decay_descriptor="Z0 -> mu+ mu-", name=name, **kwargs)


for is_ss in [True, False]:
    ss_string = "SS" if is_ss else "OS"

    for displaced in [True, False]:
        displaced_string = "Displaced" if displaced else "Prompt"

        if displaced:
            # rate tests show this prescale is not needed now, kept for flexibility
            displaced_prescale = 1.0
            min_ip = 0.4 * mm
            max_ip = 2.0 * mm
        else:
            min_ip = -1 * mm
            # no cut
            max_ip = 0.4 * mm
            displaced_prescale = 1.0

        for mass_range in mass_ranges:
            prescale = mass_range[3] * displaced_prescale
            name = f"Hlt2QEE_DiMuon_DrellYan_{mass_range[2]}_{ss_string}_{displaced_string}"

            min_probnnmu = 0.0  # disabled option for now.

            @register_line_builder(all_lines)
            def drell_yan_line(name=name,
                               min_mass=mass_range[0],
                               max_mass=mass_range[1],
                               max_ip=max_ip,
                               min_ip=min_ip,
                               is_ss=is_ss,
                               prescale=prescale,
                               persist_reco=mass_range[4],
                               min_probnnmu=min_probnnmu):
                pvs = make_pvs()
                dimuons = make_dimuon_candidates(
                    is_ss,
                    name + "_combiner",
                    pvs=pvs,
                    min_mass=min_mass,
                    max_mass=max_mass,
                    max_ip=max_ip,
                    min_ip=min_ip,
                    min_probnnmu=min_probnnmu)

                return Hlt2Line(
                    name=name,
                    algs=upfront_reconstruction() +
                    [require_pvs(pvs), dimuons],
                    persistreco=persist_reco,
                    prescale=prescale)
