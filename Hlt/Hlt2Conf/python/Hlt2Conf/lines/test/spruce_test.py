###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of test sprucing lines, notice PROCESS = 'spruce'

In Test_sprucing_line a mass monitor for Ds mass is included,
to save the outputs add the following line to your yaml:
histo_file : 'my_histograms.root'

Non trivial imports:
prefilters and line_alg ("bare" line builders) like b_to_dh.make_BdToDsmK_DsmToHHH

Output:
updated dictionary of sprucing_lines

To be noted:
"bare" line builders, like b_to_dh.make_BdToDsmK_DsmToHHH, have PROCESS as
argument to allow ad hoc settings

"""
from Moore.config import SpruceLine

from PyConf.Algorithms import Monitor__ParticleRange

from GaudiKernel.SystemOfUnits import MeV, GeV, picosecond

import Functors as F

from Hlt2Conf.standard_particles import make_long_pions
from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm import prefilters
from Hlt2Conf.lines.b_to_open_charm import b_to_dh

PROCESS = 'spruce'
sprucing_lines = {}

##############################################
# From the B2OC BToDh_Builder
##############################################

##Note these test lines do not need registering to `sprucing_lines` as they are not intended to run as part of the Sprucing


def Test_sprucing_line(name='SpruceTest', prescale=1):
    kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    d = d_builder.make_dsplus_to_kpkmpip(pi_pidk_max=None, k_pidk_min=None)

    line_alg = b_builder.make_b2x(
        particles=[d, kaon],
        descriptors=['[B0 -> D_s- K+]cc'],
        am_min=5000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=7000 * MeV,
        sum_pt_min=6 * GeV,
        bpvltime_min=0.3 * picosecond)

    #d Mass monitor
    d_mon = Monitor__ParticleRange(
        name=name + "_Ds_Monitor",
        HistogramName="m",
        Input=d,
        Variable=F.MASS,
        Bins=50,
        Range=((1968.35 - 5 * 5.0) * MeV, (1968.35 + 5 * 5.0) * MeV))

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=prefilters.b2oc_prefilters() + [d_mon, line_alg])


def Test_extraoutputs_sprucing_line(name='SpruceTest_ExtraOutputs',
                                    prescale=1):
    line_alg = b_to_dh.make_BdToDsmK_DsmToKpKmPim(process=PROCESS)
    LongT = basic_builder.get_particles_WithPVs(make_long_pions, mipchi2_min=0)
    return SpruceLine(
        name=name,
        prescale=prescale,
        extra_outputs=[("LongTracks", LongT)],
        algs=prefilters.b2oc_prefilters() + [line_alg])


def Test_persistreco_sprucing_line(name='SpruceTest_PersistReco', prescale=1):
    line_alg = b_to_dh.make_BdToDsmK_DsmToKpKmPim(process=PROCESS)
    return SpruceLine(
        name=name,
        prescale=prescale,
        persistreco=True,
        algs=prefilters.b2oc_prefilters() + [line_alg])
