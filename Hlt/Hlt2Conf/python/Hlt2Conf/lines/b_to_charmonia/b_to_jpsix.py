###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Bu/Bd/Bs/Lb... meson decays to JpsiX
Definition of B -> J/psi X HLT2/Sprucing lines for B2CC.
"""
from Hlt2Conf.lines.b_to_charmonia.builders import basic_builder
from Hlt2Conf.lines.b_to_charmonia.builders import special_builder
from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi
from Hlt2Conf.lines.charmonium_to_dimuon_detached import make_detached_jpsi
from GaudiKernel.SystemOfUnits import MeV, picosecond
from Hlt2Conf.standard_particles import make_long_muons
from Hlt2Conf.algorithms import ParticleContainersMerger
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all
from RecoConf.reconstruction_objects import make_pvs
import Functors as F
from Functors.math import in_range


def make_B2JpsiX(particles,
                 descriptor,
                 name='B2CC_BToJpsiX_Combiner_{hash}',
                 comb_m_min=3600 * MeV,
                 comb_m_max=6000 * MeV,
                 vtx_m_min=3600 * MeV,
                 vtx_m_max=6000 * MeV,
                 lifetime=None,
                 dira=-10.0,
                 max_vtxchi2pdof=10,
                 max_ipchi2=None,
                 pvs=make_pvs):
    """
    A generic B->Jpsi X decay maker.
    """
    combination_code = (in_range(comb_m_min, F.MASS, comb_m_max))
    vertex_code = require_all(
        in_range(vtx_m_min, F.MASS, vtx_m_max), (F.BPVDIRA(make_pvs()) > dira),
        (F.CHI2DOF < max_vtxchi2pdof))

    if lifetime:
        vertex_code = require_all(vertex_code, F.BPVLTIME(pvs()) > lifetime)
    if max_ipchi2:
        vertex_code = require_all(vertex_code, F.BPVIPCHI2(pvs()) < max_ipchi2)

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_Bs2Jpsif0(particles,
                   descriptor,
                   name='B2CC_BsToJpsif0_Combiner_{hash}',
                   comb_m_min=5100. * MeV,
                   comb_m_max=5720. * MeV,
                   vtx_m_min=5150. * MeV,
                   vtx_m_max=5670. * MeV,
                   lifetime=0.2 * picosecond,
                   dira=0.999,
                   max_vtxchi2pdof=10.,
                   max_ipchi2=25):
    """
    A generic Bs->Jpsi f0 decay maker.
    """
    return make_B2JpsiX(
        particles=particles,
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        lifetime=lifetime,
        dira=dira,
        max_vtxchi2pdof=max_vtxchi2pdof,
        max_ipchi2=max_ipchi2)


def make_Bd2JpsieeKshort_detached(
        particles,
        descriptor,
        name='B2CC_BdToJpsieeKshort_Detached_Combiner_{hash}',
        comb_m_min=4400 * MeV,
        comb_m_max=6000 * MeV,
        vtx_m_min=4400 * MeV,
        vtx_m_max=6000 * MeV,
        lifetime=0.1 * picosecond,
        dira=-10.0,
        max_vtxchi2pdof=5):
    """
    A generic B0->Jpsi(ee) Kshort decay maker.
    """
    return make_B2JpsiX(
        particles=particles,
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        lifetime=lifetime,
        dira=dira,
        max_vtxchi2pdof=max_vtxchi2pdof)


def make_Bd2JpsimumuKshort_detached(
        particles,
        descriptor,
        name='B2CC_BdToJpsimumuKshort_Detached_Combiner_{hash}',
        comb_m_min=5000 * MeV,
        comb_m_max=6000 * MeV,
        vtx_m_min=5000 * MeV,
        vtx_m_max=6000 * MeV,
        lifetime=0.2 * picosecond,
        dira=-10.0,
        max_vtxchi2pdof=10):
    """
    A generic B0->Jpsi(mumu) Kshort decay maker.
    """
    return make_B2JpsiX(
        particles=particles,
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        lifetime=lifetime,
        dira=dira,
        max_vtxchi2pdof=max_vtxchi2pdof)


### Define function for each line
def make_BsToJpsiPhi_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    phi = basic_builder.make_selected_phi()
    jpsi = basic_builder.make_selected_jpsi2mumu()

    if process == 'spruce':
        jpsi = make_detached_jpsi()

    line_alg = make_B2JpsiX(
        name='B2CC_BsToJpsiPhi_Detached_Combiner_{hash}',
        particles=[jpsi, phi],
        descriptor='B_s0 -> J/psi(1S) phi(1020)',
        comb_m_min=5050 * MeV,
        comb_m_max=5650 * MeV,
        vtx_m_min=5150 * MeV,
        vtx_m_max=5550 * MeV,
        dira=-10.,
        lifetime=0.2 * picosecond,
        max_vtxchi2pdof=20)

    return [jpsi, phi, line_alg]


def make_BsToJpsiPhi_extramuonline(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    phi = basic_builder.make_selected_phi()
    jpsi = basic_builder.make_selected_jpsi2mumu()
    extra_muons = basic_builder.make_muons(pt=1000 * MeV, mipchi2_min=0.)

    if process == 'spruce':
        jpsi = make_detached_jpsi()

    b = make_B2JpsiX(
        name='B2CC_BsToJpsiPhi_Detached_Combiner_{hash}',
        particles=[jpsi, phi],
        descriptor='B_s0 -> J/psi(1S) phi(1020)',
        comb_m_min=5050 * MeV,
        comb_m_max=5650 * MeV,
        vtx_m_min=5150 * MeV,
        vtx_m_max=5550 * MeV,
        dira=-10,
        lifetime=0.1 * picosecond,
        max_vtxchi2pdof=20)

    line_alg = special_builder.make_X2BsLep(
        particles=[b, extra_muons], descriptor='[system -> B_s0 mu+]cc')

    return [jpsi, phi, extra_muons, b, line_alg]


def make_BsToJpsiPhi_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    phi = basic_builder.make_selected_phi()
    jpsi = basic_builder.make_selected_jpsi2mumu()
    if process == 'spruce':
        jpsi = make_jpsi()

    line_alg = make_B2JpsiX(
        name='B2CC_BsToJpsiPhi_Prompt_Combiner_{hash}',
        particles=[jpsi, phi],
        descriptor='B_s0 -> J/psi(1S) phi(1020)',
        comb_m_min=5050 * MeV,
        comb_m_max=5650 * MeV,
        vtx_m_min=5150 * MeV,
        vtx_m_max=5550 * MeV,
        dira=-10,
        max_vtxchi2pdof=20)

    return [jpsi, phi, line_alg]


def make_BsToJpsieePhi_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    phi = basic_builder.make_selected_phi_bs2jpsieephi()
    jpsi = basic_builder.make_selected_jpsi2ee(mipchi2_e=0)
    jpsi_ws = basic_builder.make_selected_jpsi2ee_wrongsign(mipchi2_e=0)
    jpsi_merge = ParticleContainersMerger(
        [jpsi, jpsi_ws], name='B2CC_Jpsi2EE_MergeContainer_{hash}')

    b2jpsieephi = make_B2JpsiX(
        name='B2CC_BsToJpsieePhi_Detached_Combiner_{hash}',
        particles=[jpsi_merge, phi],
        descriptor='B_s0 -> J/psi(1S) phi(1020)',
        comb_m_min=4300 * MeV,
        comb_m_max=5800 * MeV,
        vtx_m_min=4300 * MeV,
        vtx_m_max=5800 * MeV,
        lifetime=0.3 * picosecond,
        max_vtxchi2pdof=10)

    return [jpsi_merge, phi, b2jpsieephi]


def make_BsToJpsieePhi_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    phi = basic_builder.make_selected_phi_bs2jpsieephi()
    jpsi = basic_builder.make_selected_jpsi2ee()
    jpsi_ws = basic_builder.make_selected_jpsi2ee_wrongsign()
    jpsi_merge = ParticleContainersMerger(
        [jpsi, jpsi_ws], name='B2CC_Jpsi2EE_MergeContainer_{hash}')

    b2jpsieephi = make_B2JpsiX(
        name='B2CC_BsToJpsieePhi_Prompt_Combiner_{hash}',
        particles=[jpsi_merge, phi],
        descriptor='B_s0 -> J/psi(1S) phi(1020)',
        comb_m_min=4000 * MeV,
        comb_m_max=5900 * MeV,
        vtx_m_min=4000 * MeV,
        vtx_m_max=5900 * MeV,
        dira=0.9995,
        max_vtxchi2pdof=10)

    return [jpsi_merge, phi, b2jpsieephi]


def make_BdToJpsieeKstar_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    jpsi = basic_builder.make_selected_jpsi2ee(mipchi2_e=0)
    jpsi_ws = basic_builder.make_selected_jpsi2ee_wrongsign(mipchi2_e=0)
    jpsi_merge = ParticleContainersMerger(
        [jpsi, jpsi_ws], name='B2CC_Jpsi2EE_MergeContainer_{hash}')

    kstar = basic_builder.make_selected_kstar2kpi(
        comb_m_min=826 * MeV,
        comb_m_max=966 * MeV,
        vtx_m_min=842 * MeV,
        vtx_m_max=942 * MeV)
    #kstar_ws = basic_builder.make_selected_kstar2kpi_wrongsign(
    #comb_m_min=842 * MeV, comb_m_max=942 * MeV, vtx_m_min=842 * MeV, vtx_m_max=942 * MeV)
    #kstar_merge = ParticleContainersMerger(
    #    [kstar, kstar_ws], name='B2CC_Kstar2KPi_MergeContainer_{hash}')

    b2jpsikstar = make_B2JpsiX(
        name='B2CC_BdToJpsiEEKstar_Detached_Combiner_{hash}',
        particles=[jpsi_merge, kstar],
        descriptor='[B0 -> J/psi(1S) K*(892)0]cc',
        comb_m_min=4300 * MeV,
        comb_m_max=5800 * MeV,
        vtx_m_min=4300 * MeV,
        vtx_m_max=5800 * MeV,
        lifetime=0.3 * picosecond,
        max_vtxchi2pdof=10)

    return [jpsi_merge, kstar, b2jpsikstar]


def make_BdToJpsieeKstar_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    kstar = basic_builder.make_selected_kstar2kpi()
    kstar_ws = basic_builder.make_selected_kstar2kpi_wrongsign()
    kstar_merge = ParticleContainersMerger(
        [kstar, kstar_ws], name='B2CC_Kstar2KPi_MergeContainer_{hash}')

    jpsi = basic_builder.make_selected_jpsi2ee()
    jpsi_ws = basic_builder.make_selected_jpsi2ee_wrongsign()
    jpsi_merge = ParticleContainersMerger(
        [jpsi, jpsi_ws], name='B2CC_Jpsi2EE_MergeContainer_{hash}')

    b2jpsikstar = make_B2JpsiX(
        name='B2CC_BdToJpsieeKstar_Combiner_{hash}',
        particles=[jpsi_merge, kstar_merge],
        descriptor='[B0 -> J/psi(1S) K*(892)0]cc',
        comb_m_min=4000 * MeV,
        comb_m_max=5900 * MeV,
        vtx_m_min=4000 * MeV,
        vtx_m_max=5900 * MeV,
        dira=0.9995,
        max_vtxchi2pdof=10)

    return [jpsi_merge, kstar_merge, b2jpsikstar]


def make_BsToJpsiKstar_line(process,
                            comb_m_min_jpsi=2916 * MeV,
                            comb_m_max_jpsi=3276 * MeV,
                            vtx_m_min_jpsi=3016 * MeV,
                            vtx_m_max_jpsi=3176 * MeV,
                            comb_m_min=5100 * MeV,
                            comb_m_max=5700 * MeV,
                            vtx_m_min=5150 * MeV,
                            vtx_m_max=5600 * MeV):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2mumu(
        comb_m_min=comb_m_min_jpsi,
        comb_m_max=comb_m_max_jpsi,
        vtx_m_min=vtx_m_min_jpsi,
        vtx_m_max=vtx_m_max_jpsi)
    kstar = basic_builder.make_selected_kstar2kpi_widerange()

    b2jpsikstar = make_B2JpsiX(
        name='B2CC_BsToJpsiKstar_Combiner_{hash}',
        particles=[jpsi, kstar],
        descriptor='[B_s~0 -> J/psi(1S) K*(892)0]cc',
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        lifetime=0.2 * picosecond,
        dira=0.999,
        max_vtxchi2pdof=10.)

    return [jpsi, kstar, b2jpsikstar]


def make_BsToJpsif0_line(process,
                         comb_m_min_jpsi=2916 * MeV,
                         comb_m_max_jpsi=3276 * MeV,
                         vtx_m_min_jpsi=3016 * MeV,
                         vtx_m_max_jpsi=3176 * MeV):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2mumu(
        comb_m_min=comb_m_min_jpsi,
        comb_m_max=comb_m_max_jpsi,
        vtx_m_min=vtx_m_min_jpsi,
        vtx_m_max=vtx_m_max_jpsi)
    f0 = basic_builder.make_selected_f0()

    b2jpsif0 = make_Bs2Jpsif0(
        name='B2CC_BsToJpsif0_Combiner_{hash}',
        particles=[jpsi, f0],
        descriptor='B_s0 -> J/psi(1S) f_0(980)')

    return [jpsi, f0, b2jpsif0]


def make_BsToJpsif0kaon_line(process,
                             comb_m_min_jpsi=2916 * MeV,
                             comb_m_max_jpsi=3276 * MeV,
                             vtx_m_min_jpsi=3016 * MeV,
                             vtx_m_max_jpsi=3176 * MeV):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2mumu(
        comb_m_min=comb_m_min_jpsi,
        comb_m_max=comb_m_max_jpsi,
        vtx_m_min=vtx_m_min_jpsi,
        vtx_m_max=vtx_m_max_jpsi)
    f0kaon = basic_builder.make_selected_f0kaon()

    b2jpsif0kaon = make_Bs2Jpsif0(
        name='B2CC_BsToJpsif0KK_Combiner_{hash}',
        particles=[jpsi, f0kaon],
        descriptor='B_s0 -> J/psi(1S) f_0(980)')

    return [jpsi, f0kaon, b2jpsif0kaon]


def make_BsToJpsif0ws_line(process,
                           comb_m_min_jpsi=2916 * MeV,
                           comb_m_max_jpsi=3276 * MeV,
                           vtx_m_min_jpsi=3016 * MeV,
                           vtx_m_max_jpsi=3176 * MeV):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2mumu(
        comb_m_min=comb_m_min_jpsi,
        comb_m_max=comb_m_max_jpsi,
        vtx_m_min=vtx_m_min_jpsi,
        vtx_m_max=vtx_m_max_jpsi)
    f0ws = basic_builder.make_selected_f0ws()
    b2jpsif0ws = make_Bs2Jpsif0(
        name='B2CC_BsToJpsif0PiPi_WS_Combiner_{hash}',
        particles=[jpsi, f0ws],
        descriptor='B_s0 -> J/psi(1S) f_0(980)')

    return [jpsi, f0ws, b2jpsif0ws]


def make_BsToJpsif0Prescaled_line(process,
                                  am_min_jpsi=2896 * MeV,
                                  am_max_jpsi=3296 * MeV,
                                  am_min=5170 * MeV,
                                  am_max=5470 * MeV,
                                  lifetime=-5.0 * picosecond,
                                  dira=-10.0):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2mumu(
        comb_m_min=am_min_jpsi,
        comb_m_max=am_max_jpsi,
        vtx_m_min=am_min_jpsi,
        vtx_m_max=am_max_jpsi)
    f0Unbiased = basic_builder.make_selected_f0Unbiased()

    b2jpsif0Prescaled = make_Bs2Jpsif0(
        name='B2CC_BsToJpsif0_Unbiased_Combiner_{hash}',
        particles=[jpsi, f0Unbiased],
        descriptor='B_s0 -> J/psi(1S) f_0(980)',
        comb_m_min=am_min,
        comb_m_max=am_max,
        vtx_m_min=am_min,
        vtx_m_max=am_max,
        lifetime=lifetime,
        dira=dira)

    return [jpsi, f0Unbiased, b2jpsif0Prescaled]


def make_LbToJpsipH_line(process,
                         comb_m_min_jpsi=2916 * MeV,
                         comb_m_max_jpsi=3276 * MeV,
                         vtx_m_min_jpsi=3016 * MeV,
                         vtx_m_max_jpsi=3176 * MeV,
                         comb_m_min=5020 * MeV,
                         comb_m_max=6220 * MeV,
                         vtx_am_min=5120 * MeV,
                         vtx_am_max=6120 * MeV):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2mumu(
        comb_m_min=comb_m_min_jpsi,
        comb_m_max=comb_m_max_jpsi,
        vtx_m_min=vtx_m_min_jpsi,
        vtx_m_max=vtx_m_max_jpsi)
    lambda0 = basic_builder.make_selected_lambda0()

    Lb2jpsipH = make_Bs2Jpsif0(
        name='B2CC_LbToJpsipH_Combiner_{hash}',
        particles=[jpsi, lambda0],
        descriptor='[Lambda_b0 -> J/psi(1S) Lambda(1520)0 ]cc',
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_am_min,
        vtx_m_max=vtx_am_max)

    return [jpsi, lambda0, Lb2jpsipH]


def make_BdToJpsieeKshort_LL_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    jpsi = basic_builder.make_selected_jpsi2ee(pid_e=1)
    kshort_LL = basic_builder.make_ks_LL(bpvvdchi2=5, chi2vx=20)

    b2jpsikshort = make_Bd2JpsieeKshort_detached(
        name='B2CC_BdToJpsieeKshortLL_Detached_Combiner_{hash}',
        particles=[jpsi, kshort_LL],
        descriptor='B0 -> J/psi(1S) KS0',
        lifetime=0.1 * picosecond)

    return [jpsi, kshort_LL, b2jpsikshort]


def make_BdToJpsieeKshort_DD_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2ee(pid_e=1)
    kshort_DD = basic_builder.make_ks_DD(bpvvdchi2=5, chi2vx=20)
    b2jpsikshort = make_Bd2JpsieeKshort_detached(
        name='B2CC_BdToJpsieeKshortDD_Detached_Combiner_{hash}',
        particles=[jpsi, kshort_DD],
        descriptor='B0 -> J/psi(1S) KS0',
        lifetime=0.1 * picosecond)

    return [jpsi, kshort_DD, b2jpsikshort]


def make_BdToJpsieeKshort_LL_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2ee(pid_e=2, mipchi2_e=1)
    kshort_LL = basic_builder.make_ks_LL(bpvvdchi2=5, chi2vx=20)

    b2jpsikshort = make_Bd2JpsieeKshort_detached(
        name='B2CC_BdToJpsieeKshortLL_Tight_Combiner_{hash}',
        particles=[jpsi, kshort_LL],
        descriptor='B0 -> J/psi(1S) KS0',
        lifetime=None)

    return [jpsi, kshort_LL, b2jpsikshort]


def make_BdToJpsieeKshort_DD_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2ee(pid_e=2, mipchi2_e=1)
    kshort_DD = basic_builder.make_ks_DD(bpvvdchi2=5, chi2vx=20)

    b2jpsikshort = make_Bd2JpsieeKshort_detached(
        name='B2CC_BdToJpsieeKshortDD_Tight_Combiner_{hash}',
        particles=[jpsi, kshort_DD],
        descriptor='B0 -> J/psi(1S) KS0',
        lifetime=None)

    return [jpsi, kshort_DD, b2jpsikshort]


def make_BuToJpsieeKplus_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    jpsi = basic_builder.make_selected_jpsi2ee(pid_e=1)
    kplus = basic_builder.make_kaons(pt=500 * MeV)
    b2jpsikplus = make_Bd2JpsieeKshort_detached(
        comb_m_min=4900 * MeV,
        vtx_m_min=5000 * MeV,
        lifetime=0.1 * picosecond,
        particles=[jpsi, kplus],
        descriptor="[B+ -> J/psi(1S) K+]cc")

    return [jpsi, kplus, b2jpsikplus]


def make_BdToJpsimumuKshort_LL_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kshort_LL = basic_builder.make_ks_LL(bpvvdchi2=5, chi2vx=30)
    b2jpsikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=None,
        particles=[jpsi, kshort_LL],
        descriptor='B0 -> J/psi(1S) KS0')

    return [jpsi, kshort_LL, b2jpsikshort]


def make_BdToJpsimumuKshort_DD_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kshort_DD = basic_builder.make_ks_DD(bpvvdchi2=5, chi2vx=30)
    b2jpsikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=None,
        particles=[jpsi, kshort_DD],
        descriptor='B0 -> J/psi(1S) KS0')

    return [jpsi, kshort_DD, b2jpsikshort]


def make_BdToJpsimumuKshort_LD_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kshort_LD = basic_builder.make_ks_LD(bpvvdchi2=5, chi2vx=30)
    b2jpsikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=None,
        particles=[jpsi, kshort_LD],
        descriptor='B0 -> J/psi(1S) KS0')

    return [jpsi, kshort_LD, b2jpsikshort]


def make_BdToJpsimumuKshort_UL_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kshort_UL = basic_builder.make_ks_UL(bpvvdchi2=5, chi2vx=30)
    b2jpsikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=None,
        particles=[jpsi, kshort_UL],
        descriptor='B0 -> J/psi(1S) KS0')

    return [jpsi, kshort_UL, b2jpsikshort]


def make_BdToJpsimumuKshort_LL_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kshort_LL = basic_builder.make_ks_LL(bpvvdchi2=5, chi2vx=30)

    b2jpsikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=0.1 * picosecond,
        particles=[jpsi, kshort_LL],
        descriptor='B0 -> J/psi(1S) KS0')

    return [jpsi, kshort_LL, b2jpsikshort]


def make_BdToJpsimumuKshort_DD_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kshort_DD = basic_builder.make_ks_DD(bpvvdchi2=5, chi2vx=30)

    b2jpsikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=0.1 * picosecond,
        particles=[jpsi, kshort_DD],
        descriptor='B0 -> J/psi(1S) KS0')

    return [jpsi, kshort_DD, b2jpsikshort]


def make_BdToJpsimumuKshort_LD_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kshort_LD = basic_builder.make_ks_LD(bpvvdchi2=5, chi2vx=30)

    b2jpsikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=0.1 * picosecond,
        particles=[jpsi, kshort_LD],
        descriptor='B0 -> J/psi(1S) KS0')

    return [jpsi, kshort_LD, b2jpsikshort]


def make_BdToJpsimumuKshort_UL_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kshort_UL = basic_builder.make_ks_UL(bpvvdchi2=5, chi2vx=30)

    b2jpsikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=0.1 * picosecond,
        particles=[jpsi, kshort_UL],
        descriptor='B0 -> J/psi(1S) KS0')

    return [jpsi, kshort_UL, b2jpsikshort]


def make_BdTopsitwosmumuKshort_LL_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    psi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(
        psi2S=True, muons=muons)
    kshort_LL = basic_builder.make_ks_LL(bpvvdchi2=5, chi2vx=30)

    b2psikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=None,
        particles=[psi, kshort_LL],
        descriptor='B0 -> psi(2S) KS0')

    return [psi, kshort_LL, b2psikshort]


def make_BdTopsitwosmumuKshort_DD_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    psi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(
        psi2S=True, muons=muons)
    kshort_DD = basic_builder.make_ks_DD(bpvvdchi2=5, chi2vx=30)

    b2psikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=None,
        particles=[psi, kshort_DD],
        descriptor='B0 -> psi(2S) KS0')

    return [psi, kshort_DD, b2psikshort]


def make_BdTopsitwosmumuKshort_LL_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    psi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(
        psi2S=True, muons=muons)
    kshort_LL = basic_builder.make_ks_LL(bpvvdchi2=5, chi2vx=30)

    b2psikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=0.1 * picosecond,
        particles=[psi, kshort_LL],
        descriptor='B0 -> psi(2S) KS0')

    return [psi, kshort_LL, b2psikshort]


def make_BdTopsitwosmumuKshort_DD_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    psi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(
        psi2S=True, muons=muons)
    kshort_DD = basic_builder.make_ks_DD(bpvvdchi2=5, chi2vx=30)

    b2psikshort = make_Bd2JpsimumuKshort_detached(
        lifetime=0.1 * picosecond,
        particles=[psi, kshort_DD],
        descriptor='B0 -> psi(2S) KS0')

    return [psi, kshort_DD, b2psikshort]


def make_BuToJpsimumuKplus_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kplus = basic_builder.make_kaons(pt=800 * MeV, pid=0, p=4000 * MeV)

    b2jpsikplus = make_Bd2JpsimumuKshort_detached(
        comb_m_min=5150 * MeV,
        comb_m_max=5450 * MeV,
        vtx_m_min=5150 * MeV,
        vtx_m_max=5450 * MeV,
        lifetime=None,
        particles=[jpsi, kplus],
        descriptor="[B+ -> J/psi(1S) K+]cc")

    return [jpsi, kplus, b2jpsikplus]


def make_BuToJpsimumuKplus_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kplus = basic_builder.make_kaons(pt=800 * MeV, pid=-1, p=3000 * MeV)
    b2jpsikplus = make_Bd2JpsimumuKshort_detached(
        particles=[jpsi, kplus],
        descriptor="[B+ -> J/psi(1S) K+]cc",
        lifetime=0.1 * picosecond,
        comb_m_min=5150 * MeV,
        comb_m_max=5450 * MeV,
        vtx_m_min=5150 * MeV,
        vtx_m_max=5450 * MeV)

    return [jpsi, kplus, b2jpsikplus]


def make_BdToJpsimumuKstar_detached_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), pidmu=1, mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kstar = basic_builder.make_selected_kstar2kpi(
        comb_m_min=826 * MeV,
        comb_m_max=966 * MeV,
        vtx_m_min=826 * MeV,
        vtx_m_max=966 * MeV,
        max_vchi2pdof=16,
        max_docachi2=20.0,
        pt=500. * MeV,
        pt_pi=500. * MeV,
        p_pi=1000. * MeV,
        tr_chi2pdof_pi=10,
        pt_k=500. * MeV,
        p_k=1000. * MeV,
        tr_chi2pdof_k=10,
        pid_k=-2,
        pid_pi=3)
    b2jpsikstar = make_Bd2JpsimumuKshort_detached(
        particles=[jpsi, kstar],
        descriptor='[B0 -> J/psi(1S) K*(892)0]cc',
        lifetime=0.1 * picosecond,
        comb_m_min=5150 * MeV,
        comb_m_max=5450 * MeV,
        vtx_m_min=5150 * MeV,
        vtx_m_max=5450 * MeV)

    return [jpsi, kstar, b2jpsikstar]


def make_BdToJpsimumuKstar_tight_line(process):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    muons = basic_builder.filter_muons_loose(
        particles=make_long_muons(), pidmu=1.5, mu_pidk=20, mu_pidp=20)
    jpsi = basic_builder.make_selected_jpsi_bd2jpsimumukshort(muons=muons)
    kstar = basic_builder.make_selected_kstar2kpi(
        comb_m_min=826 * MeV,
        comb_m_max=966 * MeV,
        vtx_m_min=826 * MeV,
        vtx_m_max=966 * MeV,
        max_vchi2pdof=16,
        max_docachi2=20.0,
        pt=500. * MeV,
        pt_pi=500. * MeV,
        p_pi=1500. * MeV,
        tr_chi2pdof_pi=10,
        pt_k=500. * MeV,
        p_k=1500. * MeV,
        tr_chi2pdof_k=10,
        pid_k=0,
        pid_pi=0)
    b2jpsikstar = make_Bd2JpsimumuKshort_detached(
        particles=[jpsi, kstar],
        descriptor='[B0 -> J/psi(1S) K*(892)0]cc',
        lifetime=None,
        comb_m_min=5150 * MeV,
        comb_m_max=5450 * MeV,
        vtx_m_min=5150 * MeV,
        vtx_m_max=5450 * MeV)

    return [jpsi, kstar, b2jpsikstar]
