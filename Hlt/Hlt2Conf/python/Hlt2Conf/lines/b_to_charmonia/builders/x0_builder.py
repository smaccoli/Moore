###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B2CC neutral objects: pi0, eta, eta' ...
"""
from GaudiKernel.SystemOfUnits import GeV, MeV
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Functors import require_all
from Hlt2Conf.lines.b_to_charmonia.builders import basic_builder
from Hlt2Conf.standard_particles import make_photons, make_resolved_pi0s
import Functors as F
from Functors.math import in_range


def filter_pi0s(particles, pi0pt_min=1.5 * GeV):
    code = require_all(F.PT > pi0pt_min)
    return ParticleFilter(particles, F.FILTER(code))


def filter_gammas(particles, gpt_min=500. * MeV, gpt_max=200. * GeV):
    code = require_all(in_range(gpt_min, F.PT, gpt_max))
    return ParticleFilter(particles, F.FILTER(code))


####################################
# 1-body+GammaGamma combiners      #
####################################


def make_onebodygg(particles,
                   descriptor,
                   comb_m_min,
                   comb_m_max,
                   name='B2CC_OneBodyGG_Combiner_{hash}',
                   pt=2000. * MeV):
    """
    Combine a track and two photons
    """

    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.PT > pt)

    vertex_code = require_all(F.PT > pt)

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        ParticleCombiner="ParticleAdder",
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


####################################
# 2-body+Gamma combiners           #
####################################


def make_twobodyg(particles,
                  descriptor,
                  comb_m_min,
                  comb_m_max,
                  name='B2CC_TwoBodyG_Combiner_{hash}',
                  pt=1000. * MeV):
    """
    Combine two tracks and one photon
    """

    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.PT > pt)

    vertex_code = require_all(F.PT > pt)

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        ParticleCombiner="ParticleAdder",
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


####################################
# K*+ -> K+ Pi0(resolved) decays   #
####################################


def make_selected_kst2kpi0r(name='B2CC_Kst2KPi0R_Filter_{hash}',
                            descriptor='[K*(892)+ -> K+ pi0]cc',
                            comb_m_min=792. * MeV,
                            comb_m_max=992. * MeV,
                            pi0pt_min=1000. * MeV,
                            pt=1500. * MeV,
                            pid_k=0,
                            pt_k=500. * MeV):

    kaon = basic_builder.make_kaons(pid=pid_k, pt=pt_k)

    pi0 = filter_pi0s(make_resolved_pi0s(), pi0pt_min=pi0pt_min)

    return make_onebodygg(
        particles=[kaon, pi0],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        pt=pt)


####################################
# Chi_c -> Jpsi[MuMu] Gamma decays #
####################################


def make_selected_chic2jpsig(name='B2CC_Chic2JpsiG_Filter_{hash}',
                             descriptor='chi_c1(1P) -> J/psi(1S) gamma',
                             comb_m_min=3410 * MeV,
                             comb_m_max=3610 * MeV,
                             gpt_min=500. * MeV,
                             pt=1000. * MeV):

    jpsi = basic_builder.make_selected_jpsi2mumu()

    gamma = filter_gammas(make_photons(), gpt_min=gpt_min)

    return make_twobodyg(
        particles=[jpsi, gamma],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        pt=pt)
