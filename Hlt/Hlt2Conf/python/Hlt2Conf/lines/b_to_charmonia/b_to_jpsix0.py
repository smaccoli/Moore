###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Bu/Bd/Bs/Lb... meson decays to JpsiX0
Definition of B -> J/psi X0 HLT2/Sprucing lines for B2CC.
"""
from Hlt2Conf.lines.b_to_charmonia.builders import basic_builder, x0_builder
from GaudiKernel.SystemOfUnits import MeV
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all
from RecoConf.reconstruction_objects import make_pvs
import Functors as F
from Functors.math import in_range

####################################
# generic combiners with neutrals  #
####################################


def make_B2JpsiX0(particles,
                  descriptor,
                  name='B2CC_B2JpsiX0_Combiner_{hash}',
                  comb_m_min=4600 * MeV,
                  comb_m_max=7000 * MeV,
                  vtx_m_min=4600 * MeV,
                  vtx_m_max=7000 * MeV,
                  lifetime=None,
                  ip_max=0.2,
                  ipchi2_max=20,
                  dira_min=0.9995,
                  vtxchi2pdof_max=10,
                  pvs=make_pvs):
    """
    A generic B->MuMu(H)(HH)(G)(GG) decay maker.
    """
    combination_code = (in_range(comb_m_min, F.MASS, comb_m_max))
    vertex_code = require_all(in_range(vtx_m_min, F.MASS, vtx_m_max))

    if lifetime:
        vertex_code = require_all(vertex_code, F.BPVLTIME(pvs()) > lifetime)

    if dira_min:
        vertex_code = require_all(vertex_code,
                                  F.BPVDIRA(make_pvs()) > dira_min)

    if ipchi2_max:
        vertex_code = require_all(vertex_code, F.BPVIPCHI2(pvs()) < ipchi2_max)

    if ip_max:
        vertex_code = require_all(vertex_code, F.BPVIP(pvs()) < ip_max)

    if vtxchi2pdof_max:
        vertex_code = require_all(vertex_code, F.CHI2DOF < vtxchi2pdof_max)

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_B2JpsiKst2KPi0R_line(process,
                              comb_m_min=4700 * MeV,
                              comb_m_max=6100 * MeV,
                              vtx_m_min=4800 * MeV,
                              vtx_m_max=6000 * MeV,
                              pi0pt_min=1000. * MeV):

    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    jpsi = basic_builder.make_selected_jpsi2mumu()

    kstr = x0_builder.make_selected_kst2kpi0r(pi0pt_min=pi0pt_min)

    b2jpsikstr = make_B2JpsiX0(
        particles=[jpsi, kstr],
        descriptor='[B+ -> J/psi(1S) K*(892)+]cc',
        name='B2CC_B2JpsiKst2KPi0R_Combiner_{hash}',
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max)

    return [jpsi, kstr, b2jpsikstr]


def make_B2Chic2JpsiGK_line(process,
                            comb_m_min=4700 * MeV,
                            comb_m_max=6100 * MeV,
                            vtx_m_min=4800 * MeV,
                            vtx_m_max=6000 * MeV,
                            gpt_min=500. * MeV,
                            pid_k=0,
                            pt_k=500. * MeV):

    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    chic = x0_builder.make_selected_chic2jpsig(gpt_min=gpt_min)

    kaon = basic_builder.make_kaons(pid=pid_k, pt=pt_k)

    b2chick = make_B2JpsiX0(
        particles=[chic, kaon],
        descriptor='[B+ -> chi_c1(1P) K+]cc',
        name='B2CC_B2Chic2JpsiGK_Combiner_{hash}',
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max)

    return [chic, kaon, b2chick]
