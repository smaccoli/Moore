###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B -> J/psi X HLT2 lines.
"""
from Moore.config import HltLine, register_line_builder

from Hlt2Conf.standard_particles import make_has_rich_long_pions, make_has_rich_up_pions
from Hlt2Conf.lines.b_to_charmonia.prefilters import b2cc_prefilters
from Hlt2Conf.lines.b_to_charmonia import b_to_jpsix, b_to_jpsix0
from RecoConf.reconstruction_objects import make_pvs
from PyConf.Algorithms import GetFlavourTaggingParticles

PROCESS = 'hlt2'
all_lines = {}


def extra_outputs_for_FlavourTagging(BCandidates):

    pvs = make_pvs()  #version 1 pv

    longTaggingParticles = GetFlavourTaggingParticles(
        BCandidates=BCandidates,
        TaggingParticles=make_has_rich_long_pions(),
        PrimaryVertices=pvs)
    #TODO: check if upstream track are properly fitted, No fitted upstream in fastest_reco
    upstreamTaggingParticles = GetFlavourTaggingParticles(
        BCandidates=BCandidates,
        TaggingParticles=make_has_rich_up_pions(),
        PrimaryVertices=pvs)
    extra_outputs = [('LongTaggingParticles', longTaggingParticles),
                     ('UpstreamTaggingParticles', upstreamTaggingParticles)]

    return extra_outputs


@register_line_builder(all_lines)
def BsToJpsiPhi_detachedline(name='Hlt2B2CC_BsToJpsiPhi_Detached',
                             prescale=1,
                             persistreco=True):
    line_alg = b_to_jpsix.make_BsToJpsiPhi_detached_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BsToJpsiPhi_extramuonline(name='Hlt2B2CC_BsToJpsiPhi_ExtraMuon',
                              prescale=1,
                              persistreco=True):
    line_alg = b_to_jpsix.make_BsToJpsiPhi_extramuonline(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[4]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BsToJpsiPhi_line(name='Hlt2B2CC_BsToJpsiPhi',
                     prescale=0.1,
                     persistreco=False):
    line_alg = b_to_jpsix.make_BsToJpsiPhi_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco)


@register_line_builder(all_lines)
def BsToJpsieePhi_detached_line(name='Hlt2B2CC_BsToJpsiPhi_JpsiToEE_Detached',
                                prescale=1,
                                persistreco=True):
    line_alg = b_to_jpsix.make_BsToJpsieePhi_detached_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BsToJpsieePhi_line(name='Hlt2B2CC_BsToJpsiPhi_JpsiToEE',
                       prescale=0.1,
                       persistreco=False):

    line_alg = b_to_jpsix.make_BsToJpsieePhi_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsieeKstar_detached_line(
        name='Hlt2B2CC_BdToJpsiKstar_JpsiToEE_Detached',
        prescale=1,
        persistreco=True):

    line_alg = b_to_jpsix.make_BdToJpsieeKstar_detached_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsieeKstar_line(name='Hlt2B2CC_BdToJpsiKstar_JpsiToEE',
                         prescale=0.05,
                         persistreco=False):

    line_alg = b_to_jpsix.make_BdToJpsieeKstar_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco)


@register_line_builder(all_lines)
def BsToJpsif0_line(name='Hlt2B2CC_BsToJpsif0', prescale=1, persistreco=True):

    line_alg = b_to_jpsix.make_BsToJpsif0_line(process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]))


@register_line_builder(all_lines)
def BsToJpsif0kaon_line(name='Hlt2B2CC_BsToJpsif0Kaon',
                        prescale=1,
                        persistreco=True):

    line_alg = b_to_jpsix.make_BsToJpsif0kaon_line(process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]))


@register_line_builder(all_lines)
def BsToJpsif0ws_line(name='Hlt2B2CC_BsToJpsif0ws',
                      prescale=1,
                      persistreco=False):

    line_alg = b_to_jpsix.make_BsToJpsif0ws_line(process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]))


@register_line_builder(all_lines)
def BsToJpsif0Prescaled_line(name='Hlt2B2CC_BsToJpsif0Prescaled',
                             prescale=0.03,
                             persistreco=False):

    line_alg = b_to_jpsix.make_BsToJpsif0Prescaled_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco)


@register_line_builder(all_lines)
def BsToJpsikstar_line(name='Hlt2B2CC_BsToJpsiKstar',
                       prescale=1,
                       persistreco=True):

    line_alg = b_to_jpsix.make_BsToJpsiKstar_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]))


@register_line_builder(all_lines)
def LbToJpsipH_line(name='Hlt2B2CC_LbToJpsipH', prescale=1, persistreco=False):

    line_alg = b_to_jpsix.make_LbToJpsipH_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsieeKshort_LL_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToEE_LL_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdToJpsieeKshort_LL_detached_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsieeKshort_DD_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToEE_DD_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdToJpsieeKshort_DD_detached_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsieeKshort_LL_tight_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToEE_LL_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsieeKshort_LL_tight_line(process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsieeKshort_DD_tight_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToEE_DD_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsieeKshort_DD_tight_line(process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BuToJpsieeKplus_detached_line(
        name='Hlt2B2CC_BuToJpsiKplus_JpsiToEE_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BuToJpsieeKplus_detached_line(process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_LL_tight_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_LL_Prompt',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_LL_tight_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_DD_tight_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_DD_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_DD_tight_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_LD_tight_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_LD_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_LD_tight_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_UL_tight_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_UL_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_UL_tight_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_LL_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_LL_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_LL_detached_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_DD_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_DD_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_DD_detached_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_LD_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_LD_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_LD_detached_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_UL_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_UL_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_UL_detached_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdTopsitwosmumuKshort_LL_tight_line(
        name='Hlt2B2CC_BdToPsitwosKshort_PsitwosToMuMu_LL_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdTopsitwosmumuKshort_LL_tight_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdTopsitwosmumuKshort_DD_tight_line(
        name='Hlt2B2CC_BdToPsitwosKshort_PsitwosToMuMu_DD_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdTopsitwosmumuKshort_DD_tight_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdTopsitwosmumuKshort_LL_detached_line(
        name='Hlt2B2CC_BdToPsitwosKshort_PsitwosToMuMu_LL_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdTopsitwosmumuKshort_LL_detached_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdTopsitwosmumuKshort_DD_detached_line(
        name='Hlt2B2CC_BdToPsitwosKshort_PsitwosToMuMu_DD_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdTopsitwosmumuKshort_DD_detached_line(
        process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BuToJpsimumuKplus_tight_line(
        name='Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BuToJpsimumuKplus_tight_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BuToJpsimumuKplus_detached_line(
        name='Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BuToJpsimumuKplus_detached_line(process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKstar_detached_line(
        name='Hlt2B2CC_BdToJpsiKstar_JpsiToMuMu_Detached',
        prescale=1,
        persistreco=True):
    line_alg = b_to_jpsix.make_BdToJpsimumuKstar_detached_line(process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BdToJpsimumuKstar_tight_line(
        name='Hlt2B2CC_BdToJpsiKstar_JpsiToMuMu_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKstar_tight_line(process=PROCESS)

    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        extra_outputs=extra_outputs_for_FlavourTagging(line_alg[2]),
        persistreco=persistreco)


@register_line_builder(all_lines)
def BuToJpsiKstPlus(name='Hlt2B2CC_BuToJpsiKstPlus',
                    prescale=1.0,
                    persistreco=False):
    line_alg = b_to_jpsix0.make_B2JpsiKst2KPi0R_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco)


@register_line_builder(all_lines)
def BuToChicKPlus(name='Hlt2B2CC_BuToChicKPlus',
                  prescale=1.0,
                  persistreco=False):
    line_alg = b_to_jpsix0.make_B2Chic2JpsiGK_line(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco)
