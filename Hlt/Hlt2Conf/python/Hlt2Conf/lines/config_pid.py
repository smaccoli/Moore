###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration for B&Q selection
"""

from PyConf import configurable


@configurable
def nopid_hadrons(ignore_pid=False):
    return ignore_pid


@configurable
def nopid_muons(ignore_pid=False):
    return ignore_pid


@configurable
def nopid_electrons(ignore_pid=False):
    return ignore_pid
