###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 HLT2 lines
"""
from PyConf import configurable
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder

#from RecoConf.reconstruction_objects import upfront_reconstruction
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from RecoConf.reconstruction_objects import make_pvs
#from RecoConf.event_filters import require_gec

PROCESS = "hlt2"
all_lines = {}


@register_line_builder(all_lines)
@configurable
def smog2_passthrough_PV(name='Hlt2IFT_SMOG2Passthrough_PV_in_SMOG2',
                         prescale=1,
                         persistreco=True):
    pvs = make_pvs

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs),
        prescale=prescale,
        persistreco=persistreco,
    )


'''# commented out until hlt1_filter_code works
#mstahl TODO: either require pvs or switch off/configure monitoring variables for this line
@register_line_builder(all_lines)
@configurable
def smog2_passthrough(name='Hlt2IFT_SMOG2GECPassthrough',
                      prescale=1,
                      persistreco=True):
    gec = require_gec()
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [gec],
        prescale=prescale,
        persistreco=persistreco,
        hlt1_filter_code=list(
            map(lambda s: s + "Decision", [
                "Hlt1_SMOG2_eta2pp", "Hlt1_SMOG2_D2Kpi",
                "Hlt1_SMOG2_2BodyGeneric", "Hlt1_SMOG2_SingleTrack",
                "Hlt1_SMOG2_DiMuonHighMass", "Hlt1GECPassThrough_LowMult5",
                "Hlt1_BESMOG2_NoBias", "Hlt1_BESMOG2_LowMult10",
                "Hlt1_SMOG2_MinimumBias", "Hlt1Passthrough_PV_in_SMOG2"
            ])))
'''
