###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 muon HLT2 lines
"""
from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_muons_builders import make_Jpsi2mumu, make_low_dimumu, make_DY2mumu, make_DY2mumu_ExcludeCCBar, make_Ups2mumu
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import MeV

PROCESS = "hlt2"
all_lines = {}

#========================================
#    Mass windows
#========================================
_MASSWINDOW_DY_BELOWJPSI = [2500.0 * MeV, 2900.0 * MeV]
_MASSWINDOW_DY_INTERMEDIATE = [3300.0 * MeV, 3500.0 * MeV]
_MASSWINDOW_DY_ABOVEPSI2S = [4000.0 * MeV, 8000.0 * MeV]

_MASSWINDOW_JPSI = [2900.0 * MeV, 4000.0 * MeV]
_MASSWINDOW_PSI2S = [3500.0 * MeV, 4000.0 * MeV]
_MASSWINDOW_UPS = [8000.0 * MeV, 12000.0 * MeV]

#========================================
#           Low mass dimuons
#========================================


@register_line_builder(all_lines)
@configurable
def smog2_lowdimuon_line(
        name="Hlt2IFT_SMOG2LowDiMuon",
        prescale=1,
        persistreco=True,
):
    """
    SMOG2 low mass dimuon HLT2 trigger line
    """
    pvs = make_pvs
    low_dimuons = make_low_dimumu(process='hlt2', pvs=pvs)

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [low_dimuons],
        prescale=prescale,
        persistreco=persistreco)


#========================================
#              Charmonia
#========================================


@register_line_builder(all_lines)
@configurable
def smog2_Jpsi2mumu_line(
        name='Hlt2IFT_SMOG2Jpsi2MuMu',
        prescale=1,
        persistreco=True,
        massWind_Jpsi=_MASSWINDOW_JPSI,
):
    """
    SMOG2 J/psi(1S) -> mu mu and Psi(2S) -> mu mu HLT2 trigger line
    """
    pvs = make_pvs
    jpsis = make_Jpsi2mumu(
        process='hlt2',
        pvs=pvs,
        massWind_Jpsi=massWind_Jpsi,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [jpsis],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_Jpsi2mumu_SS_line(
        name="Hlt2IFT_SMOG2Jpsi2MuMuSS",
        prescale=1,
        persistreco=True,
        massWind_Jpsi=_MASSWINDOW_JPSI,
):
    """
    SMOG2 J/psi(1S) -> mu mu and Psi(2S) -> mu mu (same sign) HLT2 trigger line
    """
    pvs = make_pvs

    jpsis_SS = make_Jpsi2mumu(
        process='hlt2',
        descriptor='[J/psi(1S) -> mu+ mu+]cc',
        pvs=pvs,
        massWind_Jpsi=massWind_Jpsi)

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [jpsis_SS],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_Ups2mumu_line(
        name='Hlt2IFT_SMOG2Ups2MuMu',
        prescale=1,
        persistreco=True,
        massWind_Ups=_MASSWINDOW_UPS,
):
    """
    SMOG2 Ups(1S) -> mu mu trigger line
    """
    pvs = make_pvs

    ups = make_Ups2mumu(
        process='hlt2',
        pvs=pvs,
        massWind_Ups=massWind_Ups,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [ups],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_Ups2mumu_SS_line(
        name='Hlt2IFT_SMOG2Ups2MuMuSS',
        prescale=1,
        persistreco=True,
        massWind_Ups=_MASSWINDOW_UPS,
):
    """
    SMOG2 Ups(1S) -> mu mu (same sign) trigger line
    """
    pvs = make_pvs

    ups_SS = make_Ups2mumu(
        process='hlt2',
        descriptor='[J/psi(1S) -> mu+ mu+]cc',
        pvs=pvs,
        massWind_Ups=massWind_Ups,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [ups_SS],
        prescale=prescale,
        persistreco=persistreco)


#========================================
#             Drell-Yan
#========================================


@register_line_builder(all_lines)
@configurable
def smog2_DY2mumu_line(name="Hlt2IFT_SMOG2DY2MuMu",
                       prescale=1,
                       persistreco=True,
                       minm=2500 * MeV):
    """
    SMOG2 DY ->mu mu HLT2 trigger line
    """
    pvs = make_pvs

    DY_dimuons = make_DY2mumu(process='hlt2', pvs=pvs, minm=minm)

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [DY_dimuons],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_DY2mumu_SS_line(name="Hlt2IFT_SMOG2DY2MuMuSS",
                          prescale=1,
                          persistreco=True,
                          minm=2500 * MeV):
    """
    SMOG2 DY ->mu mu (same sign) HLT2 trigger line
    """
    pvs = make_pvs

    DY_dimuons_SS = make_DY2mumu(
        process='hlt2', descriptor='[Z0 -> mu+ mu+]cc', pvs=pvs, minm=minm)

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [DY_dimuons_SS],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_DY2mumuExcludeCCBarLow_line(
        name="Hlt2IFT_SMOG2DY2MuMuExcludeCCBarLow",
        prescale=1,
        persistreco=True,
        massWind_DY=_MASSWINDOW_DY_BELOWJPSI,
):
    """
    SMOG2 DY ->mu mu HLT2 trigger line below the J/psi resonance
    """
    pvs = make_pvs

    DY_dimuons = make_DY2mumu_ExcludeCCBar(
        name="DY2mumu_BelowJpsi",
        massWind_DY=massWind_DY,
        process='hlt2',
        pvs=pvs)

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [DY_dimuons],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_DY2mumuExcludeCCBarIntermediate_line(
        name="Hlt2IFT_SMOG2DY2MuMuExcludeCCBarIntermediate",
        prescale=1,
        persistreco=True,
        massWind_DY=_MASSWINDOW_DY_INTERMEDIATE,
):
    """
    SMOG2 DY ->mu mu HLT2 trigger line between the J/psi and Psi(2S) resonances
    """
    pvs = make_pvs

    DY_dimuons = make_DY2mumu_ExcludeCCBar(
        name="DY2mumu_Intermediate",
        massWind_DY=massWind_DY,
        process='hlt2',
        pvs=pvs)

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [DY_dimuons],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_DY2mumuExcludeCCBarHigh_line(
        name="Hlt2IFT_SMOG2DY2MuMuExcludeCCBarHigh",
        prescale=1,
        persistreco=True,
        massWind_DY=_MASSWINDOW_DY_ABOVEPSI2S,
):
    """
    SMOG2 DY ->mu mu HLT2 trigger line above the Psi(2S) resonance
    """
    pvs = make_pvs

    DY_dimuons = make_DY2mumu_ExcludeCCBar(
        name="DY2mumu_High", massWind_DY=massWind_DY, process='hlt2', pvs=pvs)

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [DY_dimuons],
        prescale=prescale,
        persistreco=persistreco)
