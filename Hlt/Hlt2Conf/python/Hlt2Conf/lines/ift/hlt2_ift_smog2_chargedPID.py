###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 HLT2 lines for charged PID calibration
"""

from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_chargedPID_builders import make_smog2_L02ppi_line, make_smog2_ks2pipi, make_smog2_phi2kk
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import MeV, GeV, mm

PROCESS = "hlt2"
all_lines = {}

_MASSWINDOW_KS = [(497.7 - 50) * MeV, (497.7 + 50) * MeV]
_MASSWINDOW_LAMBDA0 = [(1115.683 - 25) * MeV, (1115.683 + 25) * MeV]

_MASSWINDOW_COMB_PHI = [(1019.445 - 40) * MeV, (1019.445 + 40) * MeV]
_MASSWINDOW_VERTEX_PHI = [(1019.445 - 20) * MeV, (1019.445 + 20) * MeV]

_MASS_KS = 497.7 * MeV
_MASS_Lambda0 = 1115.683 * MeV
_MASS_PHI = 1019.445 * MeV

#################################################################
#################################################################
###################  CHARGED PID LINES  #########################
#################################################################
#################################################################


@register_line_builder(all_lines)
@configurable
def smog2_ks2pipi_line(
        name="Hlt2IFT_SMOG2KS2PiPi",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        chi2dof=16,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        end_vz=2200 * mm,
        apt_min=0 * MeV,
        vchi2pdof_max=25.,
):
    """
    SMOG2 KS -> pi pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    TO DO: 
        - implement same line for children reconstructed as downstream tracks
    """
    pvs = make_pvs

    ks2pipi = make_smog2_ks2pipi(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        chi2dof=chi2dof,
        mmin=mmin,
        mmax=mmax,
        end_vz=end_vz,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        process="hlt2")
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [ks2pipi],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_L02ppi_line(name="Hlt2IFT_SMOG2Lamda02PPi",
                      prescale=1,
                      persistreco=True,
                      min_p=2 * GeV,
                      max_trchi2dof=5,
                      min_bpvipchi2=25,
                      chi2dof=16,
                      mmin=_MASSWINDOW_LAMBDA0[0],
                      mmax=_MASSWINDOW_LAMBDA0[1],
                      end_vz=2200 * mm,
                      apt_min=0 * MeV,
                      vchi2pdof_max=25.):
    """
    SMOG2 Lambda0 -> p pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    TO DO: 
        - include requirement in BPVLTFITCHI2 and WM
        - implement same line for children reconstructed as downstream tracks
    """
    pvs = make_pvs

    l02ppi = make_smog2_L02ppi_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        chi2dof=chi2dof,
        mmin=mmin,
        mmax=mmax,
        end_vz=end_vz,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        process="hlt2")
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [l02ppi],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_phi2kk_kmProbe_line(
        name="Hlt2IFT_SMOG2Phi2kk_Kmprobe",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=380 * MeV,
        max_trchi2dof=5,
        max_ghostprob=0.25,
        min_pidk=15,
        mmincomb=_MASSWINDOW_COMB_PHI[0],
        mmaxcomb=_MASSWINDOW_COMB_PHI[1],
        mminver=_MASSWINDOW_VERTEX_PHI[0],
        mmaxver=_MASSWINDOW_VERTEX_PHI[1],
        apt_min=0 * MeV,
        maxdoca=10 * mm,
        vchi2pdof_max=16.,
):
    """
    SMOG2 phi(1020) -> K K (Km probe) HLT2 trigger line for PID calibration
    """
    name = "Hlt2IFT_SMOG2Phi2kk_Kmprobe"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxdoca=maxdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="hlt2")
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_phi2kk_kpProbe_line(name="Hlt2IFT_SMOG2Phi2kk_Kpprobe",
                              prescale=1,
                              persistreco=True,
                              min_p=2 * GeV,
                              min_pt=380 * MeV,
                              max_trchi2dof=5,
                              max_ghostprob=0.25,
                              min_pidk=15,
                              mmincomb=_MASSWINDOW_COMB_PHI[0],
                              mmaxcomb=_MASSWINDOW_COMB_PHI[1],
                              mminver=_MASSWINDOW_VERTEX_PHI[0],
                              mmaxver=_MASSWINDOW_VERTEX_PHI[1],
                              apt_min=0 * MeV,
                              maxdoca=10 * mm,
                              vchi2pdof_max=16.):
    """
    SMOG2 phi(1020) -> K K (Kp probe) HLT2 trigger line for PID calibration
    """
    name = "Hlt2IFT_SMOG2Phi2kk_Kpprobe"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxdoca=maxdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="hlt2")
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        prescale=prescale,
        persistreco=persistreco)
