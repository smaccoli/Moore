###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all ift HLT2 lines
"""

from . import hlt2_ift_smog2
from . import hlt2_ift_smog2_charm
from . import hlt2_ift_smog2_chargedPID
from . import hlt2_ift_smog2_muons

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(hlt2_ift_smog2.all_lines)
all_lines.update(hlt2_ift_smog2_charm.all_lines)
all_lines.update(hlt2_ift_smog2_chargedPID.all_lines)
all_lines.update(hlt2_ift_smog2_muons.all_lines)
