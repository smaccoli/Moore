###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.standard_particles import make_ismuon_long_muon
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_common_particles, sv_in_smog2, bpv_in_smog2
from GaudiKernel.SystemOfUnits import MeV, mm

import Functors as F
from PyConf import configurable

#mass windows
_MASSWINDOW_DY_BELOWJPSI = [2500.0 * MeV, 2900.0 * MeV]
_MASSWINDOW_DY_INTERMEDIATE = [3300.0 * MeV, 3500.0 * MeV]
_MASSWINDOW_DY_ABOVEPSI2S = [4000.0 * MeV, 8000.0 * MeV]

_MASSWINDOW_JPSI = [2900.0 * MeV, 4000.0 * MeV]
_MASSWINDOW_PSI2S = [3500.0 * MeV, 4000.0 * MeV]
_MASSWINDOW_UPS = [8000.0 * MeV, 12000.0 * MeV]

#pid cuts for sprucing and HLT2
_SPRUCE_PIDmu_JPSI = -5.0
_SPRUCE_PIDmu_PSI2S = -5.0
_SPRUCE_PIDmu_UPSILON = -5.0
_SPRUCE_PIDmu_DY = -5.0

_HLT2_PIDmu_JPSI = None
_HLT2_PIDmu_PSI2S = None
_HLT2_PIDmu_UPSILON = None
_HLT2_PIDmu_DY = None


#muon builder
@configurable
def make_muons(pid_cut=None, min_pt=200 * MeV, max_trchi2dof=3):
    """
    Return maker for muons filtered by thresholds
    """
    return make_smog2_common_particles(
        make_particles=make_ismuon_long_muon,
        max_trchi2dof=max_trchi2dof,
        min_pt=min_pt,
        pid_cut=pid_cut,
        particle='Muon')


## constructor for the different Dimuon lines
@configurable
def make_dimuon(particles,
                descriptors,
                pvs=make_pvs,
                max_doca=None,
                apt_min=0 * MeV,
                vchi2pdof_max=25):

    combination_code = F.require_all(F.PT > apt_min)
    if (max_doca is not None):
        combination_code += F.require_all(F.MAXDOCACUT(max_doca))

    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, sv_in_smog2(),
                                bpv_in_smog2(pvs))

    return ParticleCombiner(
        Inputs=[particles, particles],
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


#builder for Jpsi -> mu+ mu- and Psi(2S) -> mu+ mu- lines
@configurable
def make_Jpsi2mumu(
        name='Jpsi2mumu_{hash}',
        process="hlt2",
        descriptor='[J/psi(1S) -> mu- mu+]cc',
        pvs=make_pvs,
        massWind_Jpsi=_MASSWINDOW_JPSI,
):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    code = F.require_all(F.MASS > massWind_Jpsi[0], F.MASS < massWind_Jpsi[1])

    if process == 'spruce':
        muons = make_muons(max_trichi2dof=3, pid_cut=_SPRUCE_PIDmu_JPSI)
        dimuon = make_dimuon(
            particles=muons,
            descriptors=descriptor,
            pvs=pvs,
            max_doca=2 * mm,
            vchi2pdof_max=16,
        )
    if process == 'hlt2':
        muons = make_muons(
            max_trchi2dof=5, min_pt=500 * MeV, pid_cut=_HLT2_PIDmu_JPSI)
        dimuon = make_dimuon(particles=muons, descriptors=descriptor, pvs=pvs)

    return ParticleFilter(dimuon, Cut=F.FILTER(code), name=name)


#builder for Upsilon(1S) -> mu+ mu- lines
@configurable
def make_Ups2mumu(
        name='Ups2mumu_{hash}',
        process="hlt2",
        descriptor='[J/psi(1S) -> mu- mu+]cc',
        pvs=make_pvs,
        massWind_Ups=_MASSWINDOW_UPS,
):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    code = F.require_all(F.MASS > massWind_Ups[0], F.MASS < massWind_Ups[1])

    if process == 'spruce':
        muons = make_muons(pid_cut=_SPRUCE_PIDmu_JPSI)
        dimuon = make_dimuon(
            particles=muons,
            descriptors=descriptor,
            pvs=pvs,
            max_doca=2 * mm,
            vchi2pdof_max=16,
        )
    if process == 'hlt2':
        muons = make_muons(
            max_trchi2dof=5, min_pt=500 * MeV, pid_cut=_HLT2_PIDmu_JPSI)
        dimuon = make_dimuon(particles=muons, descriptors=descriptor, pvs=pvs)

    return ParticleFilter(dimuon, Cut=F.FILTER(code), name=name)


#builder for low-mass dimuon lines
@configurable
def make_low_dimumu(
        name='LowDiMuon_{hash}',
        process="hlt2",
        pvs=make_pvs,
):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    code = F.require_all(F.MASS < 2900 * MeV)
    descriptor = '[J/psi(1S) -> mu- mu+]cc'

    if process == 'spruce':
        muons = make_muons(pid_cut=_SPRUCE_PIDmu_JPSI)
        dimuon = make_dimuon(
            particles=muons,
            descriptors=descriptor,
            pvs=pvs,
            max_doca=2 * mm,
            vchi2pdof_max=16,
        )
    if process == 'hlt2':
        muons = make_muons(
            max_trchi2dof=5, min_pt=500 * MeV, pid_cut=_HLT2_PIDmu_JPSI)
        dimuon = make_dimuon(particles=muons, descriptors=descriptor, pvs=pvs)

    return ParticleFilter(dimuon, Cut=F.FILTER(code), name=name)


#builder for Drell-Yan lines
@configurable
def make_DY2mumu(name='DY2mumu_{hash}',
                 process="hlt2",
                 descriptor='[Z0 -> mu+ mu-]cc',
                 pvs=make_pvs,
                 minm=2500 * MeV):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    code = F.require_all(F.MASS > minm)

    if process == 'spruce':
        muons = make_muons(pid_cut=_SPRUCE_PIDmu_JPSI)
        dimuon = make_dimuon(
            particles=muons,
            descriptors=descriptor,
            pvs=pvs,
            max_doca=2 * mm,
            vchi2pdof_max=16,
        )
    if process == 'hlt2':
        muons = make_muons(
            max_trchi2dof=5, min_pt=500 * MeV, pid_cut=_HLT2_PIDmu_JPSI)
        dimuon = make_dimuon(particles=muons, descriptors=descriptor, pvs=pvs)

    return ParticleFilter(dimuon, Cut=F.FILTER(code), name=name)


#builder for Drell-Yan lines excluding the J/psi and Psi(2S) resonances
@configurable
def make_DY2mumu_ExcludeCCBar(
        name='DY2mumu_ExcludeCCBar_{hash}',
        massWind_DY=_MASSWINDOW_DY_BELOWJPSI,
        process="hlt2",
        descriptor='[Z0 -> mu+ mu-]cc',
        pvs=make_pvs,
):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    code = F.require_all(F.MASS > massWind_DY[0], F.MASS < massWind_DY[1])

    if process == 'spruce':
        muons = make_muons(pid_cut=_SPRUCE_PIDmu_JPSI)
        dimuon = make_dimuon(
            particles=muons,
            descriptors=descriptor,
            pvs=pvs,
            max_doca=2 * mm,
            vchi2pdof_max=16,
        )
    if process == 'hlt2':
        muons = make_muons(
            max_trchi2dof=5, min_pt=500 * MeV, pid_cut=_HLT2_PIDmu_JPSI)
        dimuon = make_dimuon(particles=muons, descriptors=descriptor, pvs=pvs)

    return ParticleFilter(dimuon, Cut=F.FILTER(code), name=name)
