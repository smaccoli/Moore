###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders for SMOG2 HLT2 lines for charged PID calibration
"""
from Hlt2Conf.lines.ift.builders.smog2_builders import sv_in_smog2, make_smog2_common_particles, bpv_in_smog2
from Hlt2Conf.standard_particles import make_has_rich_long_pions, make_has_rich_long_protons, make_has_rich_long_kaons
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner

from GaudiKernel.SystemOfUnits import MeV, GeV, mm
import Functors as F
from Functors.math import in_range
from RecoConf.reconstruction_objects import make_pvs

all_lines = {}

_MASSWINDOW_KS = [(497.7 - 50) * MeV, (497.7 + 50) * MeV]
_MASSWINDOW_LAMBDA0 = [(1115.683 - 25) * MeV, (1115.683 + 25) * MeV]

_MASSWINDOW_COMB_PHI = [(1019.445 - 40) * MeV, (1019.445 + 40) * MeV]
_MASSWINDOW_VERTEX_PHI = [(1019.445 - 20) * MeV, (1019.445 + 20) * MeV]

_MASS_KS = 497.7 * MeV
_MASS_Lambda0 = 1115.683 * MeV
_MASS_PHI = 1019.445 * MeV


def make_probe_kaon(probe_charge, min_p, min_pt, max_trchi2dof):
    kaons = make_smog2_common_particles(
        make_has_rich_long_kaons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
    )
    return ParticleFilter(kaons, Cut=F.FILTER(probe_charge))


def make_tag_kaon(tag_charge, min_p, min_pt, max_trchi2dof, max_ghostprob,
                  min_pidk):
    kaons = make_smog2_common_particles(
        make_has_rich_long_kaons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
        pid_cut=min_pidk,
        particle='Kaon',
    )
    return ParticleFilter(
        kaons,
        Cut=F.FILTER(F.require_all(tag_charge, F.GHOSTPROB < max_ghostprob)))


def make_phi(pvs, ktag, kprobe, name, descriptor, mmincomb, mmaxcomb, mminver,
             mmaxver, apt_min, maxdoca, vchi2pdof_max):

    combination_code = F.require_all(F.PT > apt_min,
                                     in_range(mmincomb, F.MASS, mmaxcomb),
                                     F.MAXDOCACUT(maxdoca))
    vertex_code = F.require_all(
        in_range(mminver, F.MASS, mmaxver), F.CHI2DOF < vchi2pdof_max,
        sv_in_smog2(), bpv_in_smog2(pvs))

    return ParticleCombiner(
        Inputs=[ktag, kprobe],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_detached_dihadron(pvs, particle1, particle2, name, descriptors, mmin,
                           mmax, end_vz, apt_min, vchi2pdof_max):

    combination_code = F.require_all(F.PT > apt_min,
                                     in_range(mmin, F.MASS, mmax))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.END_VZ < end_vz,
                                bpv_in_smog2(pvs))

    return ParticleCombiner(
        Inputs=[particle1, particle2],
        DecayDescriptor=descriptors,
        name=name,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_smog2_ks2pipi(
        pvs=make_pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        chi2dof=16,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        end_vz=2200 * mm,
        apt_min=0 * MeV,
        vchi2pdof_max=25.,
        process="hlt2",
):

    name = 'smog2_ks2pipi'
    descriptor = '[KS0 -> pi+ pi-]cc'
    pions = make_smog2_common_particles(
        make_has_rich_long_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        min_bpvipchi2=min_bpvipchi2)

    return make_detached_dihadron(pvs, pions, pions, name, descriptor, mmin,
                                  mmax, end_vz, apt_min, vchi2pdof_max)


def make_smog2_L02ppi_line(pvs=make_pvs,
                           min_p=2 * GeV,
                           max_trchi2dof=5,
                           min_bpvipchi2=25,
                           chi2dof=16,
                           mmin=_MASSWINDOW_LAMBDA0[0],
                           mmax=_MASSWINDOW_LAMBDA0[1],
                           end_vz=2200 * mm,
                           apt_min=0 * MeV,
                           vchi2pdof_max=25,
                           process="hlt2"):

    name = 'smog2_L02ppi'
    descriptor = '[Lambda0 -> p+ pi-]cc'
    pions = make_smog2_common_particles(
        make_has_rich_long_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_bpvipchi2=min_bpvipchi2)
    protons = make_smog2_common_particles(
        make_has_rich_long_protons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        min_bpvipchi2=min_bpvipchi2)

    return make_detached_dihadron(pvs, protons, pions, name, descriptor, mmin,
                                  mmax, end_vz, apt_min, vchi2pdof_max)


def make_smog2_phi2kk(name,
                      pvs=make_pvs,
                      min_p=2 * GeV,
                      min_pt=380 * MeV,
                      max_trchi2dof=5,
                      max_ghostprob=0.25,
                      min_pidk=15,
                      mmincomb=_MASSWINDOW_COMB_PHI[0],
                      mmaxcomb=_MASSWINDOW_COMB_PHI[1],
                      mminver=_MASSWINDOW_VERTEX_PHI[0],
                      mmaxver=_MASSWINDOW_VERTEX_PHI[1],
                      apt_min=0 * MeV,
                      maxdoca=10 * mm,
                      vchi2pdof_max=16.,
                      process="hlt2"):

    tag_charge = F.CHARGE > 0 if 'Kmprobe' in name else F.CHARGE < 0
    probe_charge = F.CHARGE < 0 if 'Kmprobe' in name else F.CHARGE > 0

    descriptor = 'phi(1020) -> K+ K-' if "Kmprobe" in name else 'phi(1020) -> K- K+'

    return make_phi(
        pvs,
        make_tag_kaon(tag_charge, min_p, min_pt, max_trchi2dof, max_ghostprob,
                      min_pidk),
        make_probe_kaon(probe_charge, min_p, min_pt,
                        max_trchi2dof), name, descriptor, mmincomb, mmaxcomb,
        mminver, mmaxver, apt_min, maxdoca, vchi2pdof_max)
