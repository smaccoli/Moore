###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of common SMOG2 HLT2 builders
"""
from PyConf import configurable

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm

from Hlt2Conf.algorithms_thor import ParticleFilter, PVFilter

from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs
from RecoConf.event_filters import require_gec


def filter_pvs_by_z(pvs, min_z=-999999 * mm, max_z=9999999 * mm):
    return PVFilter(
        Input=pvs(),
        Cut=F.FILTER(in_range(min_z, (F.Z_COORDINATE @ F.POSITION), max_z)))


@configurable
def make_smog2_prefilters(pvs=make_pvs, min_z=-541. * mm, max_z=-341. * mm):
    """
    Common smog2 prefilters
    """

    gec = require_gec()
    require_smog2_pvs = filter_pvs_by_z(pvs=pvs, min_z=min_z, max_z=max_z)

    return upfront_reconstruction() + [gec, require_smog2_pvs]


@configurable
def make_smog2_common_particles(make_particles,
                                max_trchi2dof=5,
                                min_p=1000 * MeV,
                                min_pt=200 * MeV,
                                pid_cut=None,
                                particle=None,
                                pvs=make_pvs,
                                min_bpvipchi2=None,
                                min_bpvip=None):
    """
    Common smog2 particles maker

    Parameters
    ----------
    make_particles: Particle maker function
    max_trchi2dof : Maximum track chi2ndof (5 by default)
    min_p         : Minimum momentum (1 GeV by default)
    min_pt        : Minimum transverse momentum (200 MeV by default)
    pid_cut       : PID cut (for PIDx variables, PIDmu, PIDK, etc.) entered as a float
    particle      : Type of particle (Muon, Kaon, Pion, Proton, Electron) to select which PIDx variable to use
    """

    code = F.require_all(F.P > min_p, F.PT > min_pt, F.CHI2DOF < max_trchi2dof)

    if pid_cut is not None:
        if particle == 'Muon': code &= F.PID_MU > pid_cut
        elif particle == 'Pion': code &= F.PID_K < pid_cut
        elif particle == 'Kaon': code &= F.PID_K > pid_cut
        elif particle == 'Proton': code &= F.PID_P > pid_cut

    if min_bpvip: code &= F.BPVIP(pvs()) >= min_bpvip
    if min_bpvipchi2: code &= F.BPVIPCHI2(pvs()) >= min_bpvipchi2

    return ParticleFilter(make_particles(), Cut=F.FILTER(code))


@configurable
def in_smog2(functor, min_z=-541 * mm, max_z=-341 * mm):
    """
    Filter defining the z range of SMOG2
    """

    return in_range(min_z, functor, max_z)


def sv_in_smog2():
    """
    Filter requiring a composite particle to have a DV within in a z range
    """

    return in_smog2(functor=F.END_VZ)


def bpv_in_smog2(pvs=make_pvs):
    """
    Filter requiring a particle to have a Best Primary Vertex (BPV) within the SMOG2 z range
    """

    return in_smog2(functor=F.BPVZ(pvs()))
