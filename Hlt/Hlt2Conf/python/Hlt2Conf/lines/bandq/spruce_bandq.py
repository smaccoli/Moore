###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" 
Booking of B&Q sprucing lines, notice PROCESS = 'spruce'

Output:
updated dictionary of sprucing_lines

Comment out lines related to neutral particles and downstream tracks for now
"""
from Moore.config import SpruceLine, register_line_builder

from PyConf import configurable
from Hlt2Conf.lines.bandq.builders import b_to_jpsiX_lines, b_to_etacX_lines, xibc_lines, Bc_lines, qqbar_to_hadrons, dimuon_sprucing_lines
from Hlt2Conf.lines.bandq.builders.prefilters import make_prefilters

PROCESS = 'spruce'
sprucing_lines = {}

#################################
# dimuon sprucings, tighter cut than hlt2 lines
#################################


@register_line_builder(sprucing_lines)
@configurable
def JpsiToMuMuDetached_sprucing_line(name='SpruceBandQ_JpsiToMuMuDetached',
                                     prescale=1):
    """Detached Jpsi -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_detached_jpsi_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True)


@register_line_builder(sprucing_lines)
@configurable
def Psi2SToMuMuDetached_sprucing_line(name='SpruceBandQ_Psi2SToMuMuDetached',
                                      prescale=1):
    """Detached psi(2S) -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_detached_psi2s_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True)


@register_line_builder(sprucing_lines)
@configurable
def JpsiToMuMuTightPrompt_sprucing_line(
        name='SpruceBandQ_JpsiToMuMuTightPrompt', prescale=1):
    """Prompt Jpsi -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_tight_prompt_jpsi_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True)


@register_line_builder(sprucing_lines)
@configurable
def Psi2SToMuMuTightPrompt_sprucing_line(
        name='SpruceBandQ_Psi2SToMuMuTightPrompt', prescale=1):
    """Prompt psi(2S) -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_tight_prompt_psi2s_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True)


#################################
# Book b->JpsiX sprucing lines  #
#################################


@register_line_builder(sprucing_lines)
@configurable
def BuToJpsiKp_JpsiToMuMu_sprucing_line(
        name='SpruceBandQ_BpToJpsiKp_JpsiToMuMu', prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  K+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToJpsiKp_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


###################################################
# Book b -> ccbar X, ccbar->hadron sprucing lines #
###################################################
@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToHHHH_sprucing_line(
        name='SpruceBandQ_LbToEtacPpKm_EtacToHHHH', prescale=1):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToHHHH()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToHHHH_sprucing_line(
        name='SpruceBandQ_BdToEtacKpPim_EtacToHHHH', prescale=1):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToHHHH()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToHHHH_sprucing_line(
        name='SpruceBandQ_BuToEtacKpPhi_EtacToHHHH', prescale=1):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToHHHH()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToPpPm_sprucing_line(
        name='SpruceBandQ_LbToEtacPpKm_EtacToPpPm', prescale=1):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToPpPm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToPpPm_sprucing_line(
        name='SpruceBandQ_BdToEtacKpPim_EtacToPpPm', prescale=1):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToPpPm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToPpPm_sprucing_line(
        name='SpruceBandQ_BuToEtacKpPhi_EtacToPpPm', prescale=1):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToPpPm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToKpKm_sprucing_line(
        name='SpruceBandQ_LbToEtacPpKm_EtacToKpKm', prescale=1):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToKpKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToKpKm_sprucing_line(
        name='SpruceBandQ_BdToEtacKpPim_EtacToKpKm', prescale=1):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToKpKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToKpKm_sprucing_line(
        name='SpruceBandQ_BuToEtacKpPhi_EtacToKpKm', prescale=1):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToKpKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToPipPim_sprucing_line(
        name='SpruceBandQ_LbToEtacPpKm_EtacToPipPim', prescale=1):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToPipPim()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToPipPim_sprucing_line(
        name='SpruceBandQ_BdToEtacKpPim_EtacToPipPim', prescale=1):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToPipPim()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToPipPim_sprucing_line(
        name='SpruceBandQ_BuToEtacKpPhi_EtacToPipPim', prescale=1):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToPipPim()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


############################################
# Book detached ccbar->ppbar sprucing line #
############################################
@register_line_builder(sprucing_lines)
@configurable
def ccbarToPpPmDetached_sprucing_line(name='SpruceBandQ_ccbarToPpPmDetached',
                                      prescale=1):
    """ccbar->ppbar detached line"""
    line_alg = qqbar_to_hadrons.make_detached_ccbarToPpPm_spruce()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


############################################
# Book Xibc/Tbc sprucing lines
############################################


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiLc_sprucing_line(name='SpruceBandQ_XibcToJpsiLc', prescale=1):
    """Xi_bc+ -> J/psi(1S) Lambda_c+ line"""
    line_alg = xibc_lines.make_XibcToJpsiLc()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiXicz_sprucing_line(name='SpruceBandQ_XibcToJpsiXicz',
                                 prescale=1):
    """Xi_bc+ -> J/psi(1S) Xi_c0 line"""
    line_alg = xibc_lines.make_XibcToJpsiXicz()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiLcKm_sprucing_line(name='SpruceBandQ_XibcToJpsiLcKm',
                                 prescale=1):
    """Xi_bc0 -> J/psi(1S) Lambda_c+ K- line"""
    line_alg = xibc_lines.make_XibcToJpsiLcKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiLcKmPip_sprucing_line(name='SpruceBandQ_XibcToJpsiLcKmPip',
                                    prescale=1):
    """Xi_bc+ -> J/psi(1S) Lambda_c+ K- pi+ line"""
    line_alg = xibc_lines.make_XibcToJpsiLcKmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDzPKm_sprucing_line(name='SpruceBandQ_XibcToJpsiDzPKm',
                                  prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 p+ K- line"""
    line_alg = xibc_lines.make_XibcToJpsiDzPKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDpPKm_sprucing_line(name='SpruceBandQ_XibcToJpsiDpPKm',
                                  prescale=1):
    """Xi_bc+ -> J/psi(1S) D+ p+ K- line"""
    line_alg = xibc_lines.make_XibcToJpsiDpPKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDzPKmPip_sprucing_line(name='SpruceBandQ_XibcToJpsiDzPKmPip',
                                     prescale=1):
    """Xi_bc+ -> J/psi(1S) D0 p+ K- pi+ line"""
    line_alg = xibc_lines.make_XibcToJpsiDzPKmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDzLam_sprucing_line(name='SpruceBandQ_XibcToJpsiDzLam',
                                  prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 Lambda0 line"""
    line_alg = xibc_lines.make_XibcToJpsiDzLam()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDpLam_sprucing_line(name='SpruceBandQ_XibcToJpsiDpLam',
                                  prescale=1):
    """Xi_bc+ -> J/psi(1S) D+ Lambda0 line"""
    line_alg = xibc_lines.make_XibcToJpsiDpLam()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


############################################
# Book Tbc sprucing lines
############################################


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDz_sprucing_line(name='SpruceBandQ_TbcToJpsiDz', prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 line"""
    line_alg = xibc_lines.make_TbcToJpsiDz()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDpKm_sprucing_line(name='SpruceBandQ_TbcToJpsiDpKm', prescale=1):
    """Xi_bc0 -> J/psi(1S) D+ K- line"""
    line_alg = xibc_lines.make_TbcToJpsiDpKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDzKmPip_sprucing_line(name='SpruceBandQ_TbcToJpsiDzKmPip',
                                   prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 K- pi+ line"""
    line_alg = xibc_lines.make_TbcToJpsiDzKmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDzKs_sprucing_line(name='SpruceBandQ_TbcToJpsiDzKs', prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 KS0 line"""
    line_alg = xibc_lines.make_TbcToJpsiDzKs()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


############################################
# Book Bc sprucing lines
############################################


### Bc -> Jpsi X
@register_line_builder(sprucing_lines)
@configurable
def BcToJpsiPip_sprucing_line(name='SpruceBandQ_BcToJpsiPip', prescale=1):
    """B_c+ -> J/psi(1S) pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsiPip_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToJpsiPipPipPim_sprucing_line(name='SpruceBandQ_BcToJpsiPipPipPim',
                                    prescale=1):
    """B_c+ -> J/psi(1S) pi+ pi+ pi- line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsiPipPipPim_JpsiToMuMu(
        process=PROCESS)
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToJpsi5Pi_sprucing_line(name='SpruceBandQ_BcToJpsi5Pi', prescale=1):
    """B_c+ -> J/psi(1S) pi+ pi+ pi+ pi- pi- line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsi5Pi_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPsi2SPip_sprucing_line(name='SpruceBandQ_BcToPsi2SPip', prescale=1):
    """B_c+ -> psi(2S) pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToPsi2SPip_Psi2SToMuMu(process=PROCESS)
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPsi2SPipPipPim_sprucing_line(name='SpruceBandQ_BcToPsi2SPipPipPim',
                                     prescale=1):
    """B_c+ -> psi(2S) pi+ pi+ pi- line"""
    line_alg = b_to_jpsiX_lines.make_BcToPsi2SPipPipPim_Psi2SToMuMu(
        process=PROCESS)
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


### Bc+ -> hhh


@register_line_builder(sprucing_lines)
@configurable
def BcToPpPmKp_sprucing_line(name='SpruceBandQ_BcToPpPmKp', prescale=1):
    """B_c+ -> p+ p~- K+ line"""
    line_alg = Bc_lines.make_BcToPpPmKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPpPmPip_sprucing_line(name='SpruceBandQ_BcToPpPmPip', prescale=1):
    """B_c+ -> p+ p~- pi+ line"""
    line_alg = Bc_lines.make_BcToPpPmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPipPimPip_sprucing_line(name='SpruceBandQ_BcToPipPimPip', prescale=1):
    """B_c+ -> pi+ pi- pi+ line"""
    line_alg = Bc_lines.make_BcToPipPimPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToKpKmKp_sprucing_line(name='SpruceBandQ_BcToKpKmKp', prescale=1):
    """B_c+ -> K+ K- K+ line"""
    line_alg = Bc_lines.make_BcToKpKmKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPipPimKp_sprucing_line(name='SpruceBandQ_BcToPipPimKp', prescale=1):
    """B_c+ -> pi+ pi- K+ line"""
    line_alg = Bc_lines.make_BcToPipPimKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToKpKmPip_sprucing_line(name='SpruceBandQ_BcToKpKmPip', prescale=1):
    """B_c+ -> K+ K- pi+ line"""
    line_alg = Bc_lines.make_BcToKpKmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPpPmKp_sprucing_line(name='SpruceBandQ_BuToPpPmKp', prescale=1):
    """B_c+ -> p+ p~- K+ control channel"""
    line_alg = Bc_lines.make_BuToPpPmKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPpPmPip_sprucing_line(name='SpruceBandQ_BuToPpPmPip', prescale=1):
    """B_c+ -> p+ p~- pi+ control channel"""
    line_alg = Bc_lines.make_BuToPpPmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPipPimPip_sprucing_line(name='SpruceBandQ_BuToPipPimPip', prescale=1):
    """B_c+ -> pi+ pi- pi+ control channel"""
    line_alg = Bc_lines.make_BuToPipPimPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToKpKmKp_sprucing_line(name='SpruceBandQ_BuToKpKmKp', prescale=1):
    """B_c+ -> K+ K- K+ control channel"""
    line_alg = Bc_lines.make_BuToKpKmKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPipPimKp_sprucing_line(name='SpruceBandQ_BuToPipPimKp', prescale=1):
    """B_c+ -> pi+ pi- K+ control channel"""
    line_alg = Bc_lines.make_BuToPipPimKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToKpKmPip_sprucing_line(name='SpruceBandQ_BuToKpKmPip', prescale=1):
    """B_c+ -> K+ K- pi+ control channel"""
    line_alg = Bc_lines.make_BuToKpKmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


######################################################
#comment out lines related to neutral particles and downstream tracks for now
######################################################

#@register_line_builder(sprucing_lines)
#@configurable
#def XibToJpsiXi_sprucing_line(name='SpruceBandQ_XibToJpsiXi', prescale=1):
#    """Xi_b- -> Xi- J/psi(1S)"""
#    line_alg = b_to_jpsiX_lines.make_XibToJpsiXi_JpsiToMuMu()
#    return SpruceLine(
#        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)

#@register_line_builder(sprucing_lines)
#@configurable
#def XibToJpsiXiPi_sprucing_line(name='SpruceBandQ_XibToJpsiXiPi',
#                                prescale=1):
#    """Xi_b0 -> Xi- J/psi(1S) pi+"""
#    line_alg = b_to_jpsiX_lines.make_XibToJpsiXiPi_JpsiToMuMu()
#    return SpruceLine(
#        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)

#@register_line_builder(sprucing_lines)
#@configurable
#def OmegabToJpsiOmega_sprucing_line(name='SpruceBandQ_OmegabToJpsiOmega',
#                                    prescale=1):
#    """Omega_b- -> Omega- J/psi(1S)"""
#    line_alg = b_to_jpsiX_lines.make_OmegabToJpsiOmega_JpsiToMuMu()
#    return SpruceLine(
#        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)

#@register_line_builder(sprucing_lines)
#@configurable
#def BcToDs1Gamma_sprucing_line(name='SpruceBandQ_BcToDs1Gamma',
#                               prescale=1):
#    """B_c+ -> D_s1(2536)+ gamma"""
#    line_alg = Bc_lines.make_BcToDs1Gamma()
#    return SpruceLine(
#        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)

#@register_line_builder(sprucing_lines)
#@configurable
#def BcToDs1GammaWS_sprucing_line(name='SpruceBandQ_BcToDs1GammaWS',
#                                 prescale=0.3):
#    """B_c+ -> D_s1(2536)+ gamma wrong sign D_s1 decay"""
#    line_alg = Bc_lines.make_BcToDs1GammaWS()
#    return SpruceLine(
#        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)
