###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define a set of particle combiners using Ttrack particles as input
"""
from PyConf import configurable
from GaudiKernel.SystemOfUnits import GeV, MeV, meter
import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs

from RecoConf.ttrack_selections_reco import PVF_with_single_extrapolation

from Hlt2Conf.standard_particles import (make_ttrack_pions,
                                         make_ttrack_protons)

from PyConf.Tools import ParticleVertexFitter


@configurable
def make_ttrack_protons_for_V0(make_protons=make_ttrack_protons,
                               make_pvs=make_pvs,
                               p_min=20. * GeV,
                               pt_min=400. * MeV,
                               minip_max=400.,
                               minipchi2_max=30000.,
                               minipchi2_min=0.):
    pvs = make_pvs()
    protons = make_protons()
    proton_cuts = F.require_all(
        F.P > p_min,
        F.PT > pt_min,
        in_range(2., F.ETA, 5.),
        # F.GHOSTPROB < 0.4,
        F.MINIP(pvs) < minip_max,
        F.MINIPCHI2(pvs) < minipchi2_max,
        F.MINIPCHI2(pvs) > minipchi2_min,
    )
    return ParticleFilter(protons, F.FILTER(proton_cuts))


@configurable
def make_ttrack_pions_for_V0(make_pions=make_ttrack_pions,
                             make_pvs=make_pvs,
                             pt_min=75. * MeV,
                             p_min=0. * GeV,
                             minipchi2_min=0.):
    pvs = make_pvs()
    pions = make_pions()
    pion_cuts = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.ECALPIDE < 1.,  # electron veto
        in_range(2., F.ETA, 5.),
        # F.GHOSTPROB < 0.4,
        F.MINIPCHI2(pvs) > minipchi2_min,
    )
    return ParticleFilter(pions, F.FILTER(pion_cuts))


@configurable
def _make_V0TT(particles,
               descriptors,
               pvs,
               yz_intersection_z_min=1000.,
               maxdoca=50.,
               maxdocachi2=300.,
               vchi2pdof_max=50,
               p_min=10 * GeV,
               pt_min=1 * GeV,
               max_chi2=150,
               vertex_z_min=2.5 * meter,
               vertex_z_max=8. * meter,
               bpv_dira_min=0.9995,
               bpv_ip_max=100.,
               bpv_ip_chi2_max=200.,
               bpv_vdrho_min=80.,
               RKextrapolation=False,
               name="V0TTCombiner"):
    """Make T-T V0 -> h+ h'- candidates
    """
    if RKextrapolation:
        fitter_tool = PVF_with_single_extrapolation
    else:
        fitter_tool = ParticleVertexFitter

    combination_code = F.require_all(
        F.TWOBODY_YZ_INTERSECTION_Z > yz_intersection_z_min,
        F.TWOBODY_TILT("Y") > 0.,
        F.CHILD(1, F.math.sign(F.TY)) == F.math.sign(
            F.POD(F.TWOBODY_YZ_INTERSECTION_Y)), F.MAXSDOCACUT(maxdoca),
        F.MAXSDOCACHI2CUT(maxdocachi2))

    vertex_code = F.require_all(F.P > p_min, F.PT > pt_min, F.CHI2 < max_chi2,
                                F.BPVDIRA(pvs) > bpv_dira_min,
                                F.BPVIP(pvs) < bpv_ip_max,
                                F.BPVIPCHI2(pvs) < bpv_ip_chi2_max,
                                F.BPVVDRHO(pvs) > bpv_vdrho_min,
                                in_range(vertex_z_min, F.END_VZ, vertex_z_max))

    return ParticleCombiner(
        Inputs=particles,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        ParticleCombiner=fitter_tool(),
        name=name,
    )


@configurable
def make_LambdaTT(pions, protons, pvs):
    '''
    Make Lambda -> p pi (TT) candidates with a linear extrapolation in all iterations of the vertex fit.
    Vertex convergence can help to suppress background, but with the linear extrapolator the vertex position and mass are inaccurate.
    '''
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0TT(
        particles=[protons, pions],
        descriptors=descriptors,
        pvs=pvs,
        name="Lambda0TTCombiner")


@configurable
def make_LambdaRKTT(pions, protons, pvs):
    '''
    Make Lambda -> p pi (TT) candidates with a Runge-Kutta (RK) extrapolation in the first iteration only of vertex fit, linear extrapolation in subsequent iterations.
    Using the RK extrapolator once improves the vertex bias and mass bias compared to linear extrapolator, but mass resolution is still poor. 
    This improves efficiency in the first half of the magnet wrt purely linear extrapolation.
    '''
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0TT(
        particles=[protons, pions],
        descriptors=descriptors,
        pvs=pvs,
        RKextrapolation=True,
        name="Lambda0RKTTCombiner")


@configurable
def make_KsTT(pions, pvs):
    '''
    Make KS0 -> pi pi (TT) candidates with a linear extrapolation in all iterations of the vertex fit.
    Vertex convergence can help to suppress background, but with the linear extrapolator the vertex position and mass are inaccurate.
    This method is very inefficient for decays in the first half of magnet.
    '''
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0TT(
        particles=[pions, pions],
        pvs=pvs,
        descriptors=descriptors,
        name="KsTTCombiner")


@configurable
def make_KsRKTT(pions, pvs):
    '''
    Make KS0 -> pi pi (TT) candidates with a Runge-Kutta (RK) extrapolation in the first iteration only of vertex fit, linear extrapolation in subsequent iterations.
    Using the RK extrapolator once improves the vertex bias and mass bias compared to linear extrapolator, but mass resolution is still poor.
    This improves efficiency in the first half of the magnet wrt purely linear extrapolation.
    '''
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0TT(
        particles=[pions, pions],
        pvs=pvs,
        RKextrapolation=True,
        descriptors=descriptors,
        name="KsRKTTCombiner",
        pt_min=1000 * MeV,
        bpv_vdrho_min=150.,
        p_min=2 * GeV)
