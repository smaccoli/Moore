###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q Lb -> Lc+ Ds- X combinations.
"""

from GaudiKernel.SystemOfUnits import MeV

from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import charged_hadrons, b_hadrons, c_to_hadrons

########################################
# Lc+->p+K-pi+,    Ds-->K+K-pi-        #
# directly start from registers        #
########################################


@configurable
def make_LbToLcDsmPiPi(name="bandq_LbToLcDsmPiPi_{hash}"):
    lc = c_to_hadrons.make_tight_LcToPpKmPip()
    ds = c_to_hadrons.make_tight_DspToKmKpPip()
    pion = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_lb(
        name=name,
        particles=[lc, ds, pion, pion],
        descriptor='[ Lambda_b0 -> Lambda_c+ D_s- pi+ pi- ]cc',
        am_min=5450. * MeV,
        am_max=5800. * MeV,
        m_min=5480. * MeV,
        m_max=5770. * MeV)
    return line_alg


@configurable
def make_LbToLcDsmKK(name="bandq_LbToLcDsmKK_{hash}"):
    lc = c_to_hadrons.make_tight_LcToPpKmPip()
    ds = c_to_hadrons.make_tight_DspToKmKpPip()
    kaon = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_lb(
        name=name,
        particles=[lc, ds, kaon, kaon],
        descriptor='[ Lambda_b0 -> Lambda_c+ D_s- K+ K- ]cc',
        am_min=5450. * MeV,
        am_max=5800. * MeV,
        m_min=5480. * MeV,
        m_max=5770. * MeV)
    return line_alg


@configurable
def make_XibToLcDsmK(name="bandq_XibToLcDsmK_{hash}"):
    lc = c_to_hadrons.make_tight_LcToPpKmPip()
    ds = c_to_hadrons.make_tight_DspToKmKpPip()
    kaon = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_b_baryons(
        name=name,
        particles=[lc, ds, kaon],
        descriptor='[ Xi_b- -> Lambda_c+ D_s- K- ]cc',
        am_min=5445. * MeV,
        am_max=6145. * MeV,
        m_min=5545. * MeV,
        m_max=6045. * MeV)
    return line_alg
