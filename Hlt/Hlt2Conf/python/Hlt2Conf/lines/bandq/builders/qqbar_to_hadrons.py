###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q dimuon combinations.
"""
import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import MeV

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleCombiner
from Hlt2Conf.algorithms import ParticleContainersMerger

from PyConf import configurable

from .charged_hadrons import make_detached_protons
from .charged_hadrons import (make_detached_protons_tightpid,
                              make_detached_pions_tightpid,
                              make_detached_kaons_tightpid)
from .charged_hadrons import make_prompt_protons
from .charged_hadrons import make_prompt_phi

#from Hlt2Conf.standard_particles import make_phi2kk

############################
#qqbar (mainly ccbar) decays to hadronic final states
#similar as the dimuon module. Contain both the line builders and intermediate constructors
###########################

#basic constructors of the ccbar vertex
#By default cut on mass and vertex quality


@configurable
def make_ccbar_to_hadrons(
        particles,
        descriptor,
        name='bandq_ccbar_to_hadrons_{hash}',
        am_min=2650 *
        MeV,  # by-default cover the Jpsi and etac region. For etac analysis, Jpsi as control mode.
        am_max=3350 * MeV,
        apt_min=0 * MeV,
        maxdoca_max=20.,  # the maximum doca of possible pair combinations should be smaller than this value
        m_min=2700 * MeV,
        m_max=3300 * MeV,
        pt_min=0 * MeV,
        vtx_chi2pdof_max=16,
        ip_chi2_min=0.):

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(maxdoca_max),
        (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) > ip_chi2_min, (F.PT > pt_min))

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


#basic constructors of the ccbar vertex
#By default cut on mass and vertex quality


@configurable
def make_bbbar_to_hadrons(particles,
                          descriptor,
                          name='bandq_qqbar_to_hadrons_{hash}',
                          am_min=8800 * MeV,
                          am_max=10700 * MeV,
                          apt_min=0 * MeV,
                          maxdoca_max=20.,
                          m_min=8900 * MeV,
                          m_max=10600 * MeV,
                          pt_min=0 * MeV,
                          vtx_chi2pdof_max=16,
                          ip_chi2_min=0.):

    return make_ccbar_to_hadrons(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        apt_min=apt_min,
        maxdoca_max=maxdoca_max,
        m_min=m_min,
        m_max=m_max,
        pt_min=pt_min,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        ip_chi2_min=ip_chi2_min)


#basic combiner of two ccbar particles
@configurable
def make_double_ccbar_to_hadrons(particles,
                                 descriptor,
                                 name='bandq_double_ccbar_to_hadrons_{hash}',
                                 am_min=4950 * MeV,
                                 apt_min=4950 * MeV,
                                 maxdoca_max=20.,
                                 m_min=5000 * MeV,
                                 pt_min=5000 * MeV,
                                 vtx_chi2pdof_max=16,
                                 ip_chi2_min=0.):

    pvs = make_pvs()

    combination_code = F.require_all((F.MASS > am_min),
                                     F.MAXDOCACHI2CUT(maxdoca_max),
                                     (F.PT > apt_min))

    vertex_code = F.require_all((F.MASS > m_min), F.CHI2DOF < vtx_chi2pdof_max,
                                F.BPVIPCHI2(pvs) > ip_chi2_min,
                                (F.PT > pt_min))

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


#A lot of detached ccbar combiner with different final states
@configurable
def make_detached_ccbarToPpPm(  ##### Default detached ccbar to ppbar
        name='bandq_detached_ccbarToPpPm_{hash}',
        ip_chi2_min=9.,
        pt_min_proton=200 * MeV,
        pid_p_min=0,
        pid_p_k_min=0,
        pt_min_ccbar=0 * MeV,
        apt_min_ccbar=0 * MeV,
        am_min=2650 * MeV,
        am_max=3350 * MeV,
        m_min=2700 * MeV,
        m_max=3300 * MeV):
    protons = make_detached_protons(
        pt_min=pt_min_proton,
        pid=F.require_all(F.PID_P > pid_p_min,
                          (F.PID_P - F.PID_K) > pid_p_k_min))
    return make_ccbar_to_hadrons(
        name=name,
        particles=[protons, protons],
        descriptor='eta_c(1S) -> p+ p~-',
        ip_chi2_min=ip_chi2_min,
        pt_min=pt_min_ccbar,
        apt_min=apt_min_ccbar,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_prompt_ccbarToPpPm(  ##### Default prompt ccbar to ppbar
        name='bandq_prompt_ccbarToPpPm_{hash}',
        ip_chi2_min=0.,
        am_min=2650 * MeV,
        am_max=3350 * MeV,
        m_min=2700 * MeV,
        m_max=3300 * MeV):
    protons = make_prompt_protons(
        pt_min=1900 * MeV,
        pid=F.require_all(F.PID_P > 20., (F.PID_P - F.PID_K) > 10.))
    return make_ccbar_to_hadrons(
        name=name,
        particles=[protons, protons],
        descriptor='eta_c(1S) -> p+ p~-',
        ip_chi2_min=ip_chi2_min,
        pt_min=5000 * MeV,
        apt_min=4950 * MeV,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_detached_ccbarToKpKm(name='bandq_detached_ccbarToKpKm_{hash}',
                              ip_chi2_min=9.,
                              pt_min_ccbar=0 * MeV,
                              apt_min_ccbar=0 * MeV,
                              am_min=2650 * MeV,
                              am_max=3350 * MeV,
                              m_min=2700 * MeV,
                              m_max=3300 * MeV):
    kaons = make_detached_kaons_tightpid()
    return make_ccbar_to_hadrons(
        name=name,
        particles=[kaons, kaons],
        descriptor='eta_c(1S) -> K+ K-',
        ip_chi2_min=ip_chi2_min,
        pt_min=pt_min_ccbar,
        apt_min=apt_min_ccbar,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_detached_ccbarToPipPim(name='bandq_detached_ccbarToPipPim_{hash}',
                                ip_chi2_min=9.,
                                pt_min_ccbar=0 * MeV,
                                apt_min_ccbar=0 * MeV,
                                am_min=2650 * MeV,
                                am_max=3350 * MeV,
                                m_min=2700 * MeV,
                                m_max=3300 * MeV):
    pions = make_detached_pions_tightpid()
    return make_ccbar_to_hadrons(
        name=name,
        particles=[pions, pions],
        descriptor='eta_c(1S) -> pi+ pi-',
        ip_chi2_min=ip_chi2_min,
        pt_min=pt_min_ccbar,
        apt_min=apt_min_ccbar,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_detached_ccbarToPipPipPimPim(
        name='bandq_detached_ccbarToPipPipPimPim_{hash}',
        ip_chi2_min=9.,
        am_min=2650 * MeV,
        am_max=3350 * MeV,
        m_min=2700 * MeV,
        m_max=3300 * MeV):
    pions = make_detached_pions_tightpid()
    return make_ccbar_to_hadrons(
        name=name,
        particles=[pions, pions, pions, pions],
        descriptor='eta_c(1S) -> pi+ pi+ pi- pi-',
        ip_chi2_min=ip_chi2_min,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_detached_ccbarToKpKmPipPim(
        name='bandq_detached_ccbarToKpKmPipPim_{hash}',
        ip_chi2_min=9.,
        am_min=2650 * MeV,
        am_max=3350 * MeV,
        m_min=2700 * MeV,
        m_max=3300 * MeV):
    kaons = make_detached_kaons_tightpid()
    pions = make_detached_pions_tightpid()
    return make_ccbar_to_hadrons(
        name=name,
        particles=[kaons, kaons, pions, pions],
        descriptor='eta_c(1S) -> K+ K- pi+ pi-',
        ip_chi2_min=ip_chi2_min,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_detached_ccbarToKpKpKmKm(name='bandq_detached_ccbarToKpKpKmKm_{hash}',
                                  ip_chi2_min=9.,
                                  am_min=2650 * MeV,
                                  am_max=3350 * MeV,
                                  m_min=2700 * MeV,
                                  m_max=3300 * MeV):
    kaons = make_detached_kaons_tightpid()
    return make_ccbar_to_hadrons(
        name=name,
        particles=[kaons, kaons, kaons, kaons],
        descriptor='eta_c(1S) -> K+ K+ K- K-',
        ip_chi2_min=ip_chi2_min,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_detached_ccbarToPpPmKpKm(name='bandq_detached_ccbarToPpPmKpKm_{hash}',
                                  ip_chi2_min=9.,
                                  am_min=2650 * MeV,
                                  am_max=3350 * MeV,
                                  m_min=2700 * MeV,
                                  m_max=3300 * MeV):
    kaons = make_detached_kaons_tightpid()
    protons = make_detached_protons_tightpid()
    return make_ccbar_to_hadrons(
        name=name,
        particles=[protons, protons, kaons, kaons],
        descriptor='eta_c(1S) -> p+ p~- K+ K-',
        ip_chi2_min=ip_chi2_min,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_detached_ccbarToPpPmPipPim(
        name='bandq_detached_ccbarToPpPmPipPim_{hash}',
        ip_chi2_min=9.,
        am_min=2650 * MeV,
        am_max=3350 * MeV,
        m_min=2700 * MeV,
        m_max=3300 * MeV):
    pions = make_detached_pions_tightpid()
    protons = make_detached_protons_tightpid()
    return make_ccbar_to_hadrons(
        name=name,
        particles=[protons, protons, pions, pions],
        descriptor='eta_c(1S) -> p+ p~- pi+ pi-',
        ip_chi2_min=ip_chi2_min,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_detached_Etac1S2SToHadrons(
        name='bandq_detached_Etac1S2SToHadrons_{hash}',
        am_min=2700 * MeV,
        am_max=3800 * MeV,
        m_min=2750 * MeV,
        m_max=3750 * MeV):

    PpPm = make_detached_ccbarToPpPm(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    KpKm = make_detached_ccbarToKpKm(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    PipPim = make_detached_ccbarToPipPim(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    PipPipPimPim = make_detached_ccbarToPipPipPimPim(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    KpKpKmKm = make_detached_ccbarToKpKpKmKm(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    KpKmPipPim = make_detached_ccbarToKpKmPipPim(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    PpPmPipPim = make_detached_ccbarToPpPmPipPim(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    PpPmKpKm = make_detached_ccbarToPpPmKpKm(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)

    return ParticleContainersMerger([
        PpPm, KpKm, PipPim, PipPipPimPim, KpKpKmKm, KpKmPipPim, PpPmPipPim,
        PpPmKpKm
    ],
                                    name=name)


@configurable
def make_detached_Etac1S2SToHHHH(name='bandq_detached_Etac1S2SToHHHH_{hash}',
                                 am_min=2700 * MeV,
                                 am_max=4550 * MeV,
                                 m_min=2750 * MeV,
                                 m_max=4500 * MeV):

    PipPipPimPim = make_detached_ccbarToPipPipPimPim(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    KpKpKmKm = make_detached_ccbarToKpKpKmKm(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    KpKmPipPim = make_detached_ccbarToKpKmPipPim(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    PpPmPipPim = make_detached_ccbarToPpPmPipPim(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)
    PpPmKpKm = make_detached_ccbarToPpPmKpKm(
        am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)

    return ParticleContainersMerger(
        [PipPipPimPim, KpKpKmKm, KpKmPipPim, PpPmPipPim, PpPmKpKm], name=name)


@configurable
def make_detached_Etac1S2SToPpPm(name='bandq_detached_Etac1S2SToPpPm_{hash}',
                                 am_min=2700 * MeV,
                                 am_max=4550 * MeV,
                                 m_min=2750 * MeV,
                                 m_max=4500 * MeV):

    return make_detached_ccbarToPpPm(
        name=name, am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)


@configurable
def make_detached_Etac1S2SToKpKm(name='bandq_detached_Etac1S2SToKpKm_{hash}',
                                 am_min=2700 * MeV,
                                 am_max=4550 * MeV,
                                 m_min=2750 * MeV,
                                 m_max=4500 * MeV):

    return make_detached_ccbarToKpKm(
        name=name, am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)


@configurable
def make_detached_Etac1S2SToPipPim(
        name='bandq_detached_Etac1S2SToPipPim_{hash}',
        am_min=2700 * MeV,
        am_max=4550 * MeV,
        m_min=2750 * MeV,
        m_max=4500 * MeV):

    return make_detached_ccbarToPipPim(
        name=name, am_min=am_min, am_max=am_max, m_min=m_min, m_max=m_max)


#############################
#Following are line builders
#############################


@configurable
def make_bbbar_to_phiphi(name='bandq_bbbar_to_phiphi_{hash}'):

    phi = make_prompt_phi()

    return make_bbbar_to_hadrons(
        name=name,
        particles=[phi, phi],
        descriptor='Upsilon(1S) -> phi(1020) phi(1020)')


@configurable
def make_detached_ccbarToPpPm_spruce(  ##### Default detached ccbar to ppbar sprucing
        name='bandq_detached_ccbarToPpPm_spruce_{hash}',
        ip_chi2_min=9.,
        pt_min_proton=1000 * MeV,
        pid_p_min=5,
        pid_p_k_min=0,
        pt_min_ccbar=0 * MeV,
        apt_min_ccbar=0 * MeV,
        am_min=2650 * MeV,
        am_max=4050 * MeV,
        m_min=2700 * MeV,
        m_max=4000 * MeV):
    protons = make_detached_protons(
        pt_min=pt_min_proton,
        pid=F.require_all(F.PID_P > pid_p_min,
                          (F.PID_P - F.PID_K) > pid_p_k_min))
    return make_ccbar_to_hadrons(
        name=name,
        particles=[protons, protons],
        descriptor='eta_c(1S) -> p+ p~-',
        ip_chi2_min=ip_chi2_min,
        pt_min=pt_min_ccbar,
        apt_min=apt_min_ccbar,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_prompt_ccbarToPpPmHigh(  ##### High mass prompt ccbar to ppbar
        name='bandq_prompt_ccbarToPpPmHigh_{hash}',
        ip_chi2_min=0.,
        am_min=3250 * MeV,
        am_max=5050 * MeV,
        m_min=3300 * MeV,
        m_max=5000 * MeV):
    protons = make_prompt_protons(
        pt_min=1900 * MeV,
        pid=F.require_all(F.PID_P > 20., (F.PID_P - F.PID_K) > 10.))
    return make_ccbar_to_hadrons(
        name=name,
        particles=[protons, protons],
        descriptor='eta_c(1S) -> p+ p~-',
        ip_chi2_min=ip_chi2_min,
        pt_min=6500 * MeV,
        apt_min=6450 * MeV,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_prompt_doubleCcbarToPpPm(  ##### Double prompt ccbar to ppbar
        name='bandq_prompt_doubleCcbarToPpPm_{hash}',
        ip_chi2_min=0.,
        am_min=5350 * MeV,
        m_min=5400 * MeV):
    eta_c = make_prompt_ccbarToPpPm(
        am_min=2650 * MeV,
        am_max=5050 * MeV,
        m_min=2700 * MeV,
        m_max=5000 * MeV)
    return make_double_ccbar_to_hadrons(
        name=name,
        particles=[eta_c, eta_c],
        descriptor='Upsilon(1S) -> eta_c(1S) eta_c(1S)',
        ip_chi2_min=ip_chi2_min,
        am_min=am_min,
        m_min=m_min)
