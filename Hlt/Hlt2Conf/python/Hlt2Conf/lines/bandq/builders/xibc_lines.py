###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q xibc decays
"""

from GaudiKernel.SystemOfUnits import MeV

from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import c_to_hadrons, b_hadrons
from Hlt2Conf.lines.bandq.builders import charged_hadrons, longlived_hadrons

from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi

#from c_to_hadrons import make_LcTopKmPip_TightMass, make_XiczTopKmKmPip
#from b_hadrons import make_xibc

# Most of Xibc lines covered by B2OC. This
# This file for file is for Xibc decays with Jpsi as part of final state
# Also contains similar decay channels of Tbc tetraquark [bcu~d~] assuming it's stable with mass ~<7148MeV

_JPSI_PDG_MASS_ = 3096.9 * MeV
_MASSWINDOW_LOW_JPSI_ = 80 * MeV  # 80 MeV allowing for longer left tail caused by radiative losses
_MASSWINDOW_HIGH_JPSI_ = 50 * MeV
_MASSMIN_JPSI = _JPSI_PDG_MASS_ - _MASSWINDOW_LOW_JPSI_
_MASSMAX_JPSI = _JPSI_PDG_MASS_ + _MASSWINDOW_HIGH_JPSI_


@configurable
def make_jpsi_fromXibc(name='jpsi_fromXibc_{hash}'):
    return make_jpsi(
        name=name,
        minMass_dimuon=_MASSMIN_JPSI,
        maxMass_dimuon=_MASSMAX_JPSI,
        minPt_muon=650. * MeV)


@configurable
def make_XibcToJpsiLc(name='bandq_XibcToJpsiLc_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Lc = c_to_hadrons.make_LcToPpKmPip()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Lc],
        descriptor='[Xi_bc+ -> J/psi(1S) Lambda_c+]cc')
    return line_alg


@configurable
def make_XibcToJpsiXicz(name='bandq_XibcToJpsiXicz_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Xicz = c_to_hadrons.make_XiczToPpKmKmPip()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Xicz],
        descriptor='[Xi_bc0 -> J/psi(1S) Xi_c0]cc')
    return line_alg


### add more of Xibc decay lines
@configurable
def make_XibcToJpsiLcKm(name='bandq_XibcToJpsiLcKm_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Lc = c_to_hadrons.make_LcToPpKmPip()
    kaons = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Lc, kaons],
        descriptor='[Xi_bc0 -> J/psi(1S) Lambda_c+ K-]cc')
    return line_alg


@configurable
def make_XibcToJpsiLcKmPip(name='bandq_XibcToJpsiLcKmPip_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Lc = c_to_hadrons.make_LcToPpKmPip()
    kaons = charged_hadrons.make_detached_kaons()
    pions = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Lc, kaons, pions],
        descriptor='[Xi_bc+ -> J/psi(1S) Lambda_c+ K- pi+]cc')
    return line_alg


@configurable
def make_XibcToJpsiDzPKm(name='bandq_XibcToJpsiDzPKm_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Dz = c_to_hadrons.make_Dz()
    protons = charged_hadrons.make_detached_protons()
    kaons = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Dz, protons, kaons],
        descriptor='[Xi_bc0 -> J/psi(1S) D0 p+ K-]cc')
    return line_alg


@configurable
def make_XibcToJpsiDpPKm(name='bandq_XibcToJpsiDpPKm_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Dp = c_to_hadrons.make_DpToKmPipPip()
    protons = charged_hadrons.make_detached_protons()
    kaons = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Dp, protons, kaons],
        descriptor='[Xi_bc+ -> J/psi(1S) D+ p+ K-]cc')
    return line_alg


@configurable
def make_XibcToJpsiDzPKmPip(name='bandq_XibcToJpsiDzPKmPip_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Dz = c_to_hadrons.make_Dz()
    protons = charged_hadrons.make_detached_protons()
    kaons = charged_hadrons.make_detached_kaons()
    pions = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Dz, protons, kaons, pions],
        descriptor='[Xi_bc+ -> J/psi(1S) D0 p+ K- pi+]cc')
    return line_alg


@configurable
def make_XibcToJpsiDzLam(name='bandq_XibcToJpsiDzLam_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Dz = c_to_hadrons.make_Dz()
    Lambda = longlived_hadrons.make_Lambda_merged()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Dz, Lambda],
        descriptor='[Xi_bc0 -> J/psi(1S) D0 Lambda0]cc')
    return line_alg


@configurable
def make_XibcToJpsiDpLam(name='bandq_XibcToJpsiDpLam_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Dp = c_to_hadrons.make_DpToKmPipPip()
    Lambda = longlived_hadrons.make_Lambda_merged()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Dp, Lambda],
        descriptor='[Xi_bc+ -> J/psi(1S) D+ Lambda0]cc')
    return line_alg


###########################################
##   Decays of Tbc tetraquark
##   As Tbc mass is expected to be ~7.148 MeV (if long-lived),
##   the default mass window of make_xibc of 6.29<m<7.95 is fine
##########################################


@configurable
def make_TbcToJpsiDz(name='bandq_TbcToJpsiDz_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Dz = c_to_hadrons.make_Dz()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Dz],
        descriptor='[Xi_bc0 -> J/psi(1S) D0]cc')
    return line_alg


@configurable
def make_TbcToJpsiDpKm(name='bandq_TbcToJpsiDpKm_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Dp = c_to_hadrons.make_DpToKmPipPip()
    kaons = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Dp, kaons],
        ## Tbc mass is expected to be ~7.148 MeV (if long-lived),
        ## thus the Xibc mass window of 6.29<m<7.95 is fine
        descriptor='[Xi_bc0 -> J/psi(1S) D+ K-]cc')
    return line_alg


@configurable
def make_TbcToJpsiDzKmPip(name='bandq_TbcToJpsiDzKmPip_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Dz = c_to_hadrons.make_Dz()
    kaons = charged_hadrons.make_detached_kaons()
    pions = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Dz, kaons, pions],
        descriptor='[Xi_bc0 -> J/psi(1S) D0 K- pi+]cc')
    return line_alg


@configurable
def make_TbcToJpsiDzKs(name='bandq_TbcToJpsiDzKs_{hash}'):
    Jpsi = make_jpsi_fromXibc()
    Dz = c_to_hadrons.make_Dz()
    Ks = longlived_hadrons.make_Ks_merged()
    line_alg = b_hadrons.make_xibc(
        name=name,
        particles=[Jpsi, Dz, Ks],
        descriptor='[Xi_bc0 -> J/psi(1S) D0 KS0]cc')
    return line_alg
