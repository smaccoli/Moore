###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the Chic2Jpsigamma builder
"""

import Functors as F
from Functors.math import in_range

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Functors import require_all

from GaudiKernel.SystemOfUnits import MeV

from PyConf import configurable

from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi_tight

from Hlt2Conf.lines.bandq.builders.neutral_particles import make_gamma2ee_DD, make_gamma2ee_LL

from Hlt2Conf.algorithms import ParticleContainersMerger

########################
#chic input for lines  #
########################


@configurable
def make_chic2jpsigamma_convDD(name='bandq_chic2jpsigamma_convDD_{hash}',
                               descriptor="chi_c1(1P) -> J/psi(1S) gamma",
                               am_min=3400. * MeV,
                               am_max=3650. * MeV,
                               m_min=3450. * MeV,
                               m_max=3600. * MeV,
                               maxVertexChi2=25):
    jpsi = make_jpsi_tight()
    gamma = make_gamma2ee_DD()
    combination_code = in_range(am_min, F.MASS, am_max)
    vertex_code = require_all(
        in_range(m_min, F.MASS, am_max), F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[jpsi, gamma],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_chic2jpsigamma_convLL(name='bandq_chic2jpsigamma_convLL_{hash}',
                               descriptor="chi_c1(1P) -> J/psi(1S) gamma",
                               am_min=3400. * MeV,
                               am_max=3650. * MeV,
                               m_min=3450. * MeV,
                               m_max=3600. * MeV,
                               maxVertexChi2=25):
    jpsi = make_jpsi_tight()
    gamma = make_gamma2ee_LL()
    combination_code = in_range(am_min, F.MASS, am_max)
    vertex_code = require_all(
        in_range(m_min, F.MASS, am_max), F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[jpsi, gamma],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


#basic chic combiner for ccbar -> chic mu mu
@configurable
def make_chic_fromccbar(name='bandq_chic_fromccbar_{hash}',
                        massMin_Chic=3450 * MeV,
                        massMax_Chic=3600 * MeV):
    code = (F.PT > 600 * MeV)
    chic2jpsigamma_convLL = ParticleFilter(
        make_chic2jpsigamma_convLL(),
        name='chic2jpsigamma_convLL_fromccbar_{hash}',
        Cut=F.FILTER(code))
    chic2jpsigamma_convDD = ParticleFilter(
        make_chic2jpsigamma_convDD(),
        name='chic2jpsigamma_convDD_fromccbar_{hash}',
        Cut=F.FILTER(code))
    return ParticleContainersMerger(
        [chic2jpsigamma_convLL, chic2jpsigamma_convDD], name=name)
