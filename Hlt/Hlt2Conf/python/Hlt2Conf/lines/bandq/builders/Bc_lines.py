###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q b-hadrons
"""
import Functors as F

from GaudiKernel.SystemOfUnits import MeV, GeV

from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import charged_hadrons, b_hadrons, neutral_particles, c_to_hadrons

#############################
#charged hadrons for Bc->hhh#
#############################


@configurable
def make_proton_forBc3h(name='bandq_proton_forBc3h_{hash}',
                        pt_min=800. * MeV,
                        p_min=10. * GeV,
                        pid=(F.PID_P > 5.),
                        mipchi2dvprimary_min=9.):
    return charged_hadrons.make_detached_protons(
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        pid=pid,
        mipchi2dvprimary_min=mipchi2dvprimary_min)


@configurable
def make_pion_forBc3h(name='bandq_pion_forBc3h_{hash}',
                      pt_min=1000. * MeV,
                      p_min=3.2 * GeV,
                      pid=(F.PID_K < -5.),
                      mipchi2dvprimary_min=12.):
    return charged_hadrons.make_detached_pions(
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        pid=pid,
        mipchi2dvprimary_min=mipchi2dvprimary_min)


@configurable
def make_kaon_forBc3h(name='bandq_kaon_forBc3h_{hash}',
                      pt_min=1000. * MeV,
                      p_min=3.2 * GeV,
                      pid=(F.PID_K > 5.),
                      mipchi2dvprimary_min=12.):
    return charged_hadrons.make_detached_kaons(
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        pid=pid,
        mipchi2dvprimary_min=mipchi2dvprimary_min)


#############################
#Bc -> hhh line constructors#
#############################


@configurable
def make_BcToPpPmKp(name='bandq_BcToPpPmKp_{hash}'):
    proton = make_proton_forBc3h()
    kaon = make_kaon_forBc3h()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[proton, proton, kaon],
        descriptor="[B_c+ -> p+ p~- K+ ]cc")
    return line_alg


@configurable
def make_BcToPpPmPip(name='bandq_BcToPpPmPip_{hash}'):
    proton = make_proton_forBc3h()
    pion = make_pion_forBc3h()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[proton, proton, pion],
        descriptor="[B_c+ -> p+ p~- pi+ ]cc")
    return line_alg


@configurable
def make_BcToPipPimPip(name='bandq_BcToPipPimPip_{hash}'):
    pion = make_pion_forBc3h()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[pion, pion, pion],
        descriptor="[B_c+ -> pi+ pi+ pi- ]cc")
    return line_alg


@configurable
def make_BcToKpKmKp(name='bandq_BcToKpKmKp_{hash}'):
    kaon = make_kaon_forBc3h()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[kaon, kaon, kaon],
        descriptor="[B_c+ -> K+ K+ K- ]cc")
    return line_alg


@configurable
def make_BcToKpKmPip(name='bandq_BcToKpKmPip_{hash}'):
    pion = make_pion_forBc3h()
    kaon = make_kaon_forBc3h()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[kaon, kaon, pion],
        descriptor="[B_c+ -> K+ K- pi+ ]cc")
    return line_alg


@configurable
def make_BcToPipPimKp(name='bandq_BcToPipPimKp_{hash}'):
    pion = make_pion_forBc3h()
    kaon = make_kaon_forBc3h()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[pion, pion, kaon],
        descriptor="[B_c+ -> pi+ pi- K+ ]cc")
    return line_alg


#############################
#Bu -> hhh control mode     #
#############################


@configurable
def make_BuToPpPmKp(name='bandq_BuToPpPmKp_{hash}'):
    proton = make_proton_forBc3h()
    kaon = make_kaon_forBc3h()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[proton, proton, kaon],
        descriptor="[B_c+ -> p+ p~- K+ ]cc")
    return line_alg


@configurable
def make_BuToPpPmPip(name='bandq_BuToPpPmPip_{hash}'):
    proton = make_proton_forBc3h()
    pion = make_pion_forBc3h()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[proton, proton, pion],
        descriptor="[B_c+ -> p+ p~- pi+ ]cc")
    return line_alg


@configurable
def make_BuToPipPimPip(name='bandq_BuToPipPimPip_{hash}'):
    pion = make_pion_forBc3h()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[pion, pion, pion],
        descriptor="[B_c+ -> pi+ pi+ pi- ]cc")
    return line_alg


@configurable
def make_BuToKpKmKp(name='bandq_BuToKpKmKp_{hash}'):
    kaon = make_kaon_forBc3h()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[kaon, kaon, kaon],
        descriptor="[B_c+ -> K+ K+ K- ]cc")
    return line_alg


@configurable
def make_BuToKpKmPip(name='bandq_BuToKpKmPip_{hash}'):
    pion = make_pion_forBc3h()
    kaon = make_kaon_forBc3h()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[kaon, kaon, pion],
        descriptor="[B_c+ -> K+ K- pi+ ]cc")
    return line_alg


@configurable
def make_BuToPipPimKp(name='bandq_BuToPipPimKp_{hash}'):
    pion = make_pion_forBc3h()
    kaon = make_kaon_forBc3h()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[pion, pion, kaon],
        descriptor="[B_c+ -> pi+ pi- K+ ]cc")
    return line_alg


########################
# Bc -> charm + neutral#
########################


@configurable
def make_BcToDs1Gamma(name='bandq_BcToDs1Gamma_{hash}'):
    Ds1 = c_to_hadrons.make_Ds1ToD0Kp_D0ToKmPip()
    gamma = neutral_particles.make_bandq_photons_tight()
    line_alg = b_hadrons.make_withneutrals_bc(
        name=name,
        particles=[Ds1, gamma],
        descriptor="[B_c+ -> D_s1(2536)+ gamma]cc")
    return line_alg


@configurable
def make_BcToDs1GammaWS(name='bandq_BcToDs1GammaWS_{hash}'):
    Ds1 = c_to_hadrons.make_Ds1ToD0Km_D0ToKmPip()
    gamma = neutral_particles.make_bandq_photons_tight()
    line_alg = b_hadrons.make_withneutrals_bc(
        name=name,
        particles=[Ds1, gamma],
        descriptor="[B_c+ -> D_s1(2536)+ gamma]cc")
    return line_alg
