###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q B -> dimuon X combinations.
"""

import Functors as F

from PyConf import configurable
from PyConf import ConfigurationError

from Hlt2Conf.lines.bandq.builders import charged_hadrons, b_hadrons, longlived_hadrons
from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi, make_psi2s
from Hlt2Conf.lines.charmonium_to_dimuon_detached import make_detached_jpsi
from Hlt2Conf.lines.charmonium_to_dimuon import make_charmonium_muons as make_bandq_muons
from Hlt2Conf.standard_particles import make_KsLL, make_KsDD

from GaudiKernel.SystemOfUnits import MeV

########################################
# Jpsi->mumu helps to control the rate #
# directly start from registers        #
########################################


@configurable
def make_BuToJpsiKp_JpsiToMuMu(process,
                               name='bandq_BuToJpsiKp_JpsiToMuMu_{hash}',
                               **decay_arguments):
    if process not in ['hlt2', 'spruce']:
        raise ConfigurationError(
            "process for make_BuToJpsiKp_JpsiToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == 'spruce':
        kaons = charged_hadrons.make_detached_kaons()
    elif process == 'hlt2':
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.))
    jpsi = make_jpsi()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[jpsi, kaons],
        descriptor='[B+ -> J/psi(1S) K+]cc',
        **decay_arguments)
    return line_alg


@configurable
def make_BuToPsi2SKp_Psi2SToMuMu(process,
                                 name='bandq_BuToPsi2SKp_Psi2SToMuMu_{hash}'):
    if process not in ['hlt2', 'spruce']:
        raise ConfigurationError(
            "process fomake_LbToEtacKp_Etac2SToKpKmPipPii2SKp_Psi2SToMuMur make_LbToEtacKp_Etac2SToKpKmPipPim must be hlt2 or spruce. Please check !"
        )
    if process == 'spruce':
        kaons = charged_hadrons.make_detached_kaons()
    elif process == 'hlt2':
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.))
    psi = make_psi2s()
    line_alg = b_hadrons.make_bu(
        name=name, particles=[psi, kaons], descriptor='[B+ -> psi(2S) K+]cc')
    return line_alg


@configurable
def make_BuToJpsiPip_JpsiToMuMu(process,
                                name='bandq_BuToJpsiPip_JpsiToMuMu_{hash}'):
    if process not in ['hlt2', 'spruce']:
        raise ConfigurationError(
            "process for make_BuToJpsiPip_JpsiToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == 'spruce':
        pions = charged_hadrons.make_detached_pions()
    elif process == 'hlt2':
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.))
    jpsi = make_jpsi()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[jpsi, pions],
        descriptor='[B+ -> J/psi(1S) pi+]cc')
    return line_alg


@configurable
def make_BuToPsi2SPip_Psi2SToMuMu(
        process, name='bandq_BuToPsi2SPip_Psi2SToMuMu_{hash}'):
    if process not in ['hlt2', 'spruce']:
        raise ConfigurationError(
            "process for make_BuToPsi2SPip_Psi2SToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == 'spruce':
        pions = charged_hadrons.make_detached_pions()
    elif process == 'hlt2':
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.))
    psi = make_psi2s()
    line_alg = b_hadrons.make_bu(
        name=name, particles=[psi, pions], descriptor='[B+ -> psi(2S) pi+]cc')
    return line_alg


@configurable
def make_BdToJpsiKpPim_JpsiToMuMu(process,
                                  name="bandq_BdToJpsiKpPim_JpsiToMuMu_{hash}",
                                  **decay_arguments):
    if process == 'spruce':
        kaons = charged_hadrons.make_detached_kaons()
        pions = charged_hadrons.make_detached_pions()
    elif process == 'hlt2':
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.))
        pions = charged_hadrons.make_detached_pions()
    jpsi = make_jpsi()
    line_alg = b_hadrons.make_bds(
        name=name,
        particles=[jpsi, kaons, pions],
        descriptor='[B0 -> J/psi(1S) K+ pi-]cc',
        **decay_arguments)
    return line_alg


@configurable
def make_BsToJpsiKpKm_JpsiToMuMu(process,
                                 name="bandq_BsToJpsiKpKm_JpsiToMuMu_{hash}",
                                 **decay_arguments):
    if process == 'spruce':
        kaons = charged_hadrons.make_detached_kaons()
    elif process == 'hlt2':
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.))
    jpsi = make_jpsi()
    line_alg = b_hadrons.make_bds(
        name=name,
        particles=[jpsi, kaons, kaons],
        descriptor='[B_s0 -> J/psi(1S) K+ K-]cc',
        **decay_arguments)
    return line_alg


@configurable
def make_LbToJpsiPKm_JpsiToMuMu(process,
                                name="bandq_LbToJpsiPKm_JpsiToMuMu_{hash}",
                                **decay_arguments):
    if process == 'spruce':
        kaons = charged_hadrons.make_detached_kaons()
        protons = charged_hadrons.make_detached_protons()
    elif process == 'hlt2':
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.))
        protons = charged_hadrons.make_detached_protons()
    jpsi = make_jpsi()
    line_alg = b_hadrons.make_lb(
        name=name,
        particles=[jpsi, protons, kaons],
        descriptor='[Lambda_b0 -> J/psi(1S) p+ K-]cc',
        **decay_arguments)
    return line_alg


@configurable
def make_BcToJpsiPip_JpsiToMuMu(process,
                                name='bandq_BcToJpsiPip_JpsiToMuMu_{hash}',
                                **decay_arguments):
    if process not in ['hlt2', 'spruce']:
        raise ConfigurationError(
            "process for make_BcToJpsiPip_JpsiToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == 'spruce':
        pions = charged_hadrons.make_detached_pions()
    elif process == 'hlt2':
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.))
    jpsi = make_jpsi()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[jpsi, pions],
        descriptor='[B_c+ -> J/psi(1S) pi+]cc',
        **decay_arguments)
    return line_alg


@configurable
def make_BcToJpsiPipPipPim_JpsiToMuMu(
        process,
        name="bandq_BcToJpsiPipPipPim_JpsiToMuMu_{hash}",
        **decay_arguments):
    if process not in ['hlt2', 'spruce']:
        raise ConfigurationError(
            "process must be hlt2 or spruce. Please check !")
    if process == 'spruce':
        pions = charged_hadrons.make_detached_pions()
    elif process == 'hlt2':
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.))
    jpsi = make_jpsi()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[jpsi, pions, pions, pions],
        descriptor='[B_c+ -> J/psi(1S) pi+ pi+ pi-]cc',
        **decay_arguments)
    return line_alg


@configurable
def make_BcToJpsi5Pi_JpsiToMuMu(process,
                                name="bandq_BcToJpsi5Pi_JpsiToMuMu_{hash}",
                                **decay_arguments):
    if process not in ['hlt2', 'spruce']:
        raise ConfigurationError(
            "process must be hlt2 or spruce. Please check !")
    if process == 'spruce':
        pions = charged_hadrons.make_detached_pions()
    elif process == 'hlt2':
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.))
    jpsi = make_jpsi()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[jpsi, pions, pions, pions, pions, pions],
        descriptor='[B_c+ -> J/psi(1S) pi+ pi+ pi+ pi- pi-]cc',
        **decay_arguments)
    return line_alg


@configurable
def make_BcToPsi2SPip_Psi2SToMuMu(
        process, name='bandq_BcToPsi2SPip_Psi2SToMuMu_{hash}'):
    if process not in ['hlt2', 'spruce']:
        raise ConfigurationError(
            "process for make_BcToPsi2SPip_Psi2SToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == 'spruce':
        pions = charged_hadrons.make_detached_pions()
    elif process == 'hlt2':
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.))
    psi = make_psi2s()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[psi, pions],
        descriptor='[B_c+ -> psi(2S) pi+]cc')
    return line_alg


@configurable
def make_BcToPsi2SPipPipPim_Psi2SToMuMu(
        process, name="bandq_BcToPsi2SPipPipPim_Psi2SToMuMu_{hash}"):
    if process not in ['hlt2', 'spruce']:
        raise ConfigurationError(
            "process for make_BcToPsi2SPip_Psi2SToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == 'spruce':
        pions = charged_hadrons.make_detached_pions()
    elif process == 'hlt2':
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.))
    psi = make_psi2s()
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[psi, pions, pions, pions],
        descriptor='[B_c+ -> psi(2S) pi+ pi+ pi-]cc')
    return line_alg


@configurable
def make_BcToJpsiMu_JpsiToMuMu(process,
                               name='bandq_BcToJpsiMu_JpsiToMuMu_{hash}',
                               minPt_muon=500 * MeV,
                               minIPChi2_muon=4,
                               minPIDmu=-5,
                               am_min=3100 * MeV,
                               am_max=7200 * MeV,
                               m_min=3200 * MeV,
                               m_max=7000 * MeV,
                               **decay_arguments):
    jpsi = make_detached_jpsi()
    #muon = make_bandq_muons(minPt_muon=2500. * MeV, minPIDmu=-10.) # isn't 2.5 GeV too tight? and minPIDmu>-10 too loose?
    # also need a detached muon
    muon = make_bandq_muons(
        minPt_muon=minPt_muon,
        minIPChi2_muon=minIPChi2_muon,
        minPIDmu=minPIDmu)
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[jpsi, muon],
        descriptor='[ B_c+ -> J/psi(1S) mu+ ]cc',
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)
    return line_alg


@configurable
def make_XibToJpsiXi_JpsiToMuMu(process,
                                name='bandq_XibToJpsiXi_JpsiToMuMu_{hash}',
                                am_min=5395. * MeV,
                                am_max=6195. * MeV,
                                m_min=5495. * MeV,
                                m_max=6095. * MeV,
                                **decay_arguments):
    jpsi = make_detached_jpsi()
    Xi = longlived_hadrons.make_XiToLambdaPi()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, Xi],
        descriptor='[Xi_b- -> J/psi(1S) Xi-]cc',
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)
    return line_alg


@configurable
def make_XibToJpsiXiPi_JpsiToMuMu(process,
                                  name='bandq_XibToJpsiXiPi_JpsiToMuMu_{hash}',
                                  am_min=5195. * MeV,
                                  am_max=6395. * MeV,
                                  m_min=5295. * MeV,
                                  m_max=6295. * MeV,
                                  **decay_arguments):
    jpsi = make_detached_jpsi()
    Xi = longlived_hadrons.make_XiToLambdaPi()
    pions = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, Xi, pions],
        descriptor='[Xi_b0 -> J/psi(1S) Xi- pi+]cc',
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)
    return line_alg


@configurable
def make_XibToJpsiPKK_JpsiToMuMu(process,
                                 name='bandq_XibToJpsiPKK_JpsiToMuMu_{hash}',
                                 am_min=5395. * MeV,
                                 am_max=6195. * MeV,
                                 m_min=5495. * MeV,
                                 m_max=6095. * MeV,
                                 **decay_arguments):

    jpsi = make_detached_jpsi()
    protons = charged_hadrons.make_detached_protons()
    kaons = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, protons, kaons, kaons],
        descriptor='[Xi_b- -> J/psi(1S) p+ K- K-]cc',
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)
    return line_alg


@configurable
def make_XibToJpsiPKKPi_JpsiToMuMu(
        process,
        name='bandq_XibToJpsiPKKPi_JpsiToMuMu_{hash}',
        am_min=5395. * MeV,
        am_max=6195. * MeV,
        m_min=5495. * MeV,
        m_max=6095. * MeV,
        **decay_arguments):
    jpsi = make_detached_jpsi()
    protons = charged_hadrons.make_detached_protons()
    kaons = charged_hadrons.make_detached_kaons()
    pions = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, protons, kaons, kaons, pions],
        descriptor='[Xi_b0 -> J/psi(1S) p+ K- K- pi+]cc',
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)
    return line_alg


@configurable
def make_OmegabToJpsiOmega_JpsiToMuMu(
        process,
        name='bandq_OmegabToJpsiOmega_JpsiToMuMu_{hash}',
        am_min=5449. * MeV,
        am_max=6649. * MeV,
        m_min=5549. * MeV,
        m_max=6549. * MeV,
        **decay_arguments):
    jpsi = make_detached_jpsi()
    Omega = longlived_hadrons.make_OmegaToLambdaK()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, Omega],
        descriptor='[Omega_b- -> J/psi(1S) Omega-]cc',
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)
    return line_alg


@configurable
def make_OmegabToJpsiPKKKPi_JpsiToMuMu(
        process,
        name='bandq_OmegabToJpsiPKKKPi_JpsiToMuMu_{hash}',
        am_min=5449. * MeV,
        am_max=6649. * MeV,
        m_min=5549. * MeV,
        m_max=6549. * MeV,
        **decay_arguments):
    jpsi = make_detached_jpsi()
    protons = charged_hadrons.make_detached_protons()
    kaons = charged_hadrons.make_detached_kaons()
    pions = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, protons, kaons, kaons, kaons, pions],
        descriptor='[Omega_b- -> J/psi(1S) p+ K- K- K- pi+]cc',
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)
    return line_alg


#########################
# Xib -> Jpsi + X       #
#########################


@configurable
def make_XibToJpsiPKsLLK_JpsiToMuMu(name="bandq_XibToJpsiPKsLLK_JpsiToMuMu"):
    jpsi = make_detached_jpsi()
    proton = charged_hadrons.make_detached_protons()
    ks = make_KsLL()
    kaon = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_b_baryons(
        name=name,
        particles=[jpsi, proton, ks, kaon],
        descriptor='[ Xi_b0 -> J/psi(1S) p+ KS0 K- ]cc',
        am_min=5395. * MeV,
        am_max=6195. * MeV,
        m_min=5495. * MeV,
        m_max=6095. * MeV)
    return line_alg


@configurable
def make_XibToJpsiPKsDDK_JpsiToMuMu(name="bandq_XibToJpsiPKsDDK_JpsiToMuMu"):
    jpsi = make_detached_jpsi()
    proton = charged_hadrons.make_detached_protons()
    ks = make_KsDD()
    kaon = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_b_baryons(
        name=name,
        particles=[jpsi, proton, ks, kaon],
        descriptor='[ Xi_b0 -> J/psi(1S) p+ KS0 K- ]cc',
        am_min=5395. * MeV,
        am_max=6195. * MeV,
        m_min=5495. * MeV,
        m_max=6095. * MeV)
    return line_alg


@configurable
def make_XibToJpsiPKPi_JpsiToMuMu(name="bandq_XibToJpsiPKPi_JpsiToMuMu"):
    jpsi = make_detached_jpsi()
    proton = charged_hadrons.make_detached_protons()
    kaon = charged_hadrons.make_detached_kaons()
    pion = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_b_baryons(
        name=name,
        particles=[jpsi, proton, kaon, pion],
        descriptor='[ Xi_b- -> J/psi(1S) p+ K- pi- ]cc',
        am_min=5445. * MeV,
        am_max=6145. * MeV,
        m_min=5545. * MeV,
        m_max=6045. * MeV)
    return line_alg
