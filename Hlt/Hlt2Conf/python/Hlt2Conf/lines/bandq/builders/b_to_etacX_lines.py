###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q B -> ccbar (->hadron) X combinations.
"""

from GaudiKernel.SystemOfUnits import mm

from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import charged_hadrons, b_hadrons, qqbar_to_hadrons

####################################################
#Customize tighter cuts for b2etacX lines          #
#Rate for etac->hadron usually higher than dimuon  #
####################################################


@configurable
def make_lb_for_etacX(name, particles, descriptor):
    return b_hadrons.make_lb(
        name=name,
        particles=particles,
        descriptor=descriptor,
        minVDz=2.0 * mm,
        minRho=0.1 * mm,
        minBPVDira=0.9999)


@configurable
def make_bu_for_etacX(name, particles, descriptor):
    return b_hadrons.make_bu(
        name=name,
        particles=particles,
        descriptor=descriptor,
        minVDz=2.0 * mm,
        minRho=0.1 * mm,
        minBPVDira=0.9999)


######################
#Line registers start#
######################


@configurable
def make_LbToEtacPpKm_EtacToHHHH(name='bandq_LbToEtacPpKm_EtacToHHHH_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    protons = charged_hadrons.make_detached_protons_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToHHHH()
    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor='[Lambda_b0 -> eta_c(1S) p+ K-]cc')
    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToHHHH(
        name='bandq_BdToEtacKpPim_EtacToHHHH_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    pions = charged_hadrons.make_detached_pions_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToHHHH()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor='[B0 -> eta_c(1S) K+ pi-]cc')
    return line_alg


@configurable
def make_BuToEtacKpPhi_EtacToHHHH(
        name='bandq_BuToEtacKpPhi_EtacToHHHH_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    phi = charged_hadrons.make_detached_phi()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToHHHH()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, phi, kaons],
        descriptor='[B0 -> eta_c(1S) phi(1020) K+]cc')
    return line_alg


@configurable
def make_LbToEtacPpKm_EtacToPpPm(name='bandq_LbToEtacPpKm_EtacToPpPm_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    protons = charged_hadrons.make_detached_protons_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPpPm()
    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor='[Lambda_b0 -> eta_c(1S) p+ K-]cc')
    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToPpPm(
        name='bandq_BdToEtacKpPim_EtacToPpPm_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    pions = charged_hadrons.make_detached_pions_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPpPm()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor='[B0 -> eta_c(1S) K+ pi-]cc')
    return line_alg


@configurable
def make_BuToEtacKpPhi_EtacToPpPm(
        name='bandq_BuToEtacKpPhi_EtacToPpPm_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    phi = charged_hadrons.make_detached_phi()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPpPm()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, phi, kaons],
        descriptor='[B0 -> eta_c(1S) phi(1020) K+]cc')
    return line_alg


@configurable
def make_LbToEtacPpKm_EtacToKpKm(name='bandq_LbToEtacPpKm_EtacToKpKm_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    protons = charged_hadrons.make_detached_protons_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToKpKm()
    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor='[Lambda_b0 -> eta_c(1S) p+ K-]cc')
    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToKpKm(
        name='bandq_BdToEtacKpPim_EtacToKpKm_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    pions = charged_hadrons.make_detached_pions_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToKpKm()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor='[B0 -> eta_c(1S) K+ pi-]cc')
    return line_alg


@configurable
def make_BuToEtacKpPhi_EtacToKpKm(
        name='bandq_BuToEtacKpPhi_EtacToKpKm_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    phi = charged_hadrons.make_detached_phi()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToKpKm()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, phi, kaons],
        descriptor='[B0 -> eta_c(1S) phi(1020) K+]cc')
    return line_alg


@configurable
def make_LbToEtacPpKm_EtacToPipPim(
        name='bandq_LbToEtacPpKm_EtacToPipPim_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    protons = charged_hadrons.make_detached_protons_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPipPim()
    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor='[Lambda_b0 -> eta_c(1S) p+ K-]cc')
    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToPipPim(
        name='bandq_BdToEtacKpPim_EtacToPipPim_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    pions = charged_hadrons.make_detached_pions_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPipPim()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor='[B0 -> eta_c(1S) K+ pi-]cc')
    return line_alg


@configurable
def make_BuToEtacKpPhi_EtacToPipPim(
        name='bandq_BuToEtacKpPhi_EtacToPipPim_{hash}'):

    kaons = charged_hadrons.make_detached_kaons_tightpid()
    phi = charged_hadrons.make_detached_phi()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPipPim()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, phi, kaons],
        descriptor='[B0 -> eta_c(1S) phi(1020) K+]cc')
    return line_alg
