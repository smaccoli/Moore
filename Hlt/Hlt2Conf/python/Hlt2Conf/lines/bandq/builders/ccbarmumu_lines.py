###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the Chic2JpsiMuMu line
"""

import Functors as F
from Functors.math import in_range

from Hlt2Conf.algorithms_thor import ParticleCombiner

from GaudiKernel.SystemOfUnits import MeV

from PyConf import configurable

from Hlt2Conf.standard_particles import make_ismuon_long_muon

from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi_tight as make_jpsi_fromccbar
from Hlt2Conf.lines.bandq.builders.dimuon_lines import make_upsilon_tight as make_upsilon_fromccbar
from Hlt2Conf.lines.bandq.builders.qqbar_to_hadrons import make_prompt_ccbarToPpPm as make_etac_fromccbar
from Hlt2Conf.lines.bandq.builders.chic import make_chic_fromccbar

##########################################
#define the input particles for ccbarmumu#
##########################################


#basic combiner
@configurable
def make_ccbarmumu(descriptor,
                   particles,
                   name='bandq_ccbarmumu_{hash}',
                   massMin_ccbar=3300 * MeV,
                   massMax_ccbar=3600 * MeV,
                   maxVertexChi2=25):

    combination_code = in_range(massMin_ccbar, F.MASS, massMax_ccbar)

    vertex_code = (F.CHI2DOF < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


#line constructors
@configurable
def make_chic2jpsimumu(name='bandq_chic2jpsimumu_{hash}'):
    return make_ccbarmumu(
        name=name,
        descriptor="chi_c1(1P) -> J/psi(1S) mu+ mu-",
        particles=[
            make_jpsi_fromccbar(),
            make_ismuon_long_muon(),
            make_ismuon_long_muon()
        ],
        massMin_ccbar=3300 * MeV,
        massMax_ccbar=3600 * MeV)


@configurable
def make_X2jpsimumu(name='bandq_X2jpsimumu_{hash}'):
    return make_ccbarmumu(
        name=name,
        descriptor="X_1(3872) -> J/psi(1S) mu+ mu-",
        particles=[
            make_jpsi_fromccbar(),
            make_ismuon_long_muon(),
            make_ismuon_long_muon()
        ],
        massMin_ccbar=3773 * MeV,
        massMax_ccbar=3922 * MeV)


@configurable
def make_hc2jpsimumu(name='bandq_hc2jpsimumu_{hash}'):
    return make_ccbarmumu(
        name=name,
        descriptor="chi_c1(1P) -> eta_c(1S) mu+ mu-",
        particles=[
            make_etac_fromccbar(),
            make_ismuon_long_muon(),
            make_ismuon_long_muon()
        ],
        massMin_ccbar=3190 * MeV,
        massMax_ccbar=3922 * MeV)


@configurable
def make_X2chicmumu(name='bandq_X2chicmumu_{hash}'):
    return make_ccbarmumu(
        name=name,
        descriptor="X_1(3872) -> chi_c1(1P) mu+ mu-",
        particles=[
            make_chic_fromccbar(),
            make_ismuon_long_muon(),
            make_ismuon_long_muon()
        ],
        massMin_ccbar=3773 * MeV,
        massMax_ccbar=3922 * MeV)


@configurable
def make_chib2upsilonmumu(name='bandq_chib2upsilonmumu_{hash}'):
    return make_ccbarmumu(
        name=name,
        descriptor="chi_b0(1P) -> Upsilon(1S) mu+ mu-",
        particles=[
            make_upsilon_fromccbar(),
            make_ismuon_long_muon(),
            make_ismuon_long_muon()
        ],
        massMin_ccbar=8500 * MeV,
        massMax_ccbar=10000 * MeV)
