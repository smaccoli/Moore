###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q double charm combinations.
"""

from Hlt2Conf.algorithms import ParticleContainersMerger
import Functors as F
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all

from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import c_to_hadrons
from Hlt2Conf.lines.charm import d_to_hhh, d0_to_hh, cbaryon_spectroscopy
from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi, make_psi2s


@configurable
def _make_doublecharm(particles,
                      descriptor,
                      name='bandq_doublecharm_singledecay_template_{hash}'):
    combination_code = require_all(F.ALL)
    vertex_code = require_all(F.ALL)
    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_doublecharm(particles,
                     descriptors,
                     name='bandq_doublecharm_template_{hash}'):
    assert len(descriptors) > 0
    c_hadrons = []
    for descriptor in descriptors:
        c_hadrons.append(
            _make_doublecharm(particles=particles, descriptor=descriptor))
    return ParticleContainersMerger(c_hadrons, name=name)


@configurable
def make_doublecharm_samesign(name='bandq_doublecharm_samesign_{hash}'):
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi+]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dp = d_to_hhh._make_d_or_ds_to_hhh(
        [
            d_to_hhh._make_kaons(),
            d_to_hhh._make_pions(),
            d_to_hhh._make_pions()
        ],
        #name='DToHHH_DpDspToKmPipPip_forDoubleCharm_{hash}',
        name='Charm_DToHHH_DpDspToKmPipPip_{hash}',
        descriptor='[D+ -> K- pi+ pi+]cc'
    )  #taken from Hlt2Charm_DpDspToKmPipPip
    ds = d_to_hhh._make_d_or_ds_to_hhh(
        [
            d_to_hhh._make_kaons(),
            d_to_hhh._make_kaons(),
            d_to_hhh._make_pions()
        ],
        #name='DToHHH_DpDspToKmKpPip_forDoubleCharm_{hash}',
        name='Charm_DToHHH_DpDspToKmKpPip_{hash}',
        descriptor='[D_s+ -> K- K+ pi+]cc'
    )  #taken from Hlt2Charm_DpDspToKmKpPip
    lc = cbaryon_spectroscopy._make_lcp_pKpi(
    )  #taken from Hlt2Charm_LcpToPpKmPip_PR
    xicz = cbaryon_spectroscopy._make_xic0_pkkpi(
    )  #taken from Hlt2Charm_Xic0ToPpKmKmPip_PR
    omegac = c_to_hadrons.make_TightOmegaczToPpKmKmPip(
    )  #taken from BandQ combiner
    jpsi = make_jpsi()  #taken from BandQ & B2CC shared combiner
    psi2s = make_psi2s()  #taken from BandQ & B2CC shared combiner
    c_hadron = ParticleContainersMerger(
        [dz, dp, ds, lc, xicz, omegac, jpsi, psi2s],
        name='bandq_charmToHadrons_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[c_hadron, c_hadron],
        descriptors=[
            '[psi(3770) -> D0 D0]cc',
            '[psi(3770) -> D0 D+]cc',
            '[psi(3770) -> D0 D_s+]cc',
            '[psi(3770) -> D0 Lambda_c+]cc',
            '[psi(3770) -> D0 Xi_c0]cc',
            '[psi(3770) -> D0 Omega_c0]cc',
            '[psi(3770) -> D+ D+]cc',
            '[psi(3770) -> D+ D_s+]cc',
            '[psi(3770) -> D+ Lambda_c+]cc',
            '[psi(3770) -> D+ Xi_c0]cc',
            '[psi(3770) -> D+ Omega_c0]cc',
            '[psi(3770) -> D_s+ D_s+]cc',
            '[psi(3770) -> D_s+ Lambda_c+]cc',
            '[psi(3770) -> D_s+ Xi_c0]cc',
            '[psi(3770) -> D_s+ Omega_c0]cc',
            '[psi(3770) -> Lambda_c+ Lambda_c+]cc',
            '[psi(3770) -> Lambda_c+ Xi_c0]cc',
            '[psi(3770) -> Lambda_c+ Omega_c0]cc',
            '[psi(3770) -> Xi_c0 Xi_c0]cc',
            '[psi(3770) -> Xi_c0 Omega_c0]cc',
            '[psi(3770) -> Omega_c0 Omega_c0]cc',
            '[psi(3770) -> J/psi(1S) D0]cc',
            '[psi(3770) -> J/psi(1S) D+]cc',
            '[psi(3770) -> J/psi(1S) D_s+]cc',
            '[psi(3770) -> J/psi(1S) Lambda_c+]cc',
            '[psi(3770) -> J/psi(1S) Xi_c0]cc',
            '[psi(3770) -> J/psi(1S) Omega_c0]cc',
            '[psi(3770) -> psi(2S) D0]cc',
            '[psi(3770) -> psi(2S) D+]cc',
            '[psi(3770) -> psi(2S) D_s+]cc',
            '[psi(3770) -> psi(2S) Lambda_c+]cc',
            '[psi(3770) -> psi(2S) Xi_c0]cc',
            '[psi(3770) -> psi(2S) Omega_c0]cc',
        ])
    return line_alg


@configurable
def make_doublecharm_oppositesign(
        name='bandq_doublecharm_oppositesign_{hash}'):
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi+]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dp = d_to_hhh._make_d_or_ds_to_hhh(
        [
            d_to_hhh._make_kaons(),
            d_to_hhh._make_pions(),
            d_to_hhh._make_pions()
        ],
        #name='DToHHH_DpDspToKmPipPip_forDoubleCharm_{hash}',
        name='Charm_DToHHH_DpDspToKmPipPip_{hash}',
        descriptor='[D+ -> K- pi+ pi+]cc'
    )  #taken from Hlt2Charm_DpDspToKmPipPip
    ds = d_to_hhh._make_d_or_ds_to_hhh(
        [
            d_to_hhh._make_kaons(),
            d_to_hhh._make_kaons(),
            d_to_hhh._make_pions()
        ],
        #name='DToHHH_DpDspToKmKpPip_forDoubleCharm_{hash}',
        name='Charm_DToHHH_DpDspToKmKpPip_{hash}',
        descriptor='[D_s+ -> K- K+ pi+]cc'
    )  #taken from Hlt2Charm_DpDspToKmKpPip
    lc = cbaryon_spectroscopy._make_lcp_pKpi(
    )  #taken from Hlt2Charm_LcpToPpKmPip_PR
    xicz = cbaryon_spectroscopy._make_xic0_pkkpi(
    )  #taken from Hlt2Charm_Xic0ToPpKmKmPip_PR
    omegac = c_to_hadrons.make_TightOmegaczToPpKmKmPip(
    )  #taken from BandQ combiner
    c_hadron = ParticleContainersMerger([dz, dp, ds, lc, xicz, omegac],
                                        name='bandq_charmToHadrons_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[c_hadron, c_hadron],
        descriptors=[
            '[psi(3770) -> D~0 D0]cc', '[psi(3770) -> D~0 D+]cc',
            '[psi(3770) -> D~0 D_s+]cc', '[psi(3770) -> D~0 Lambda_c+]cc',
            '[psi(3770) -> D~0 Xi_c0]cc', '[psi(3770) -> D~0 Omega_c0]cc',
            '[psi(3770) -> D- D+]cc', '[psi(3770) -> D- D_s+]cc',
            '[psi(3770) -> D- Lambda_c+]cc', '[psi(3770) -> D- Xi_c0]cc',
            '[psi(3770) -> D- Omega_c0]cc', '[psi(3770) -> D_s- D_s+]cc',
            '[psi(3770) -> D_s- Lambda_c+]cc', '[psi(3770) -> D_s- Xi_c0]cc',
            '[psi(3770) -> D_s- Omega_c0]cc',
            '[psi(3770) -> Lambda_c~- Lambda_c+]cc',
            '[psi(3770) -> Lambda_c~- Xi_c0]cc',
            '[psi(3770) -> Lambda_c~- Omega_c0]cc',
            '[psi(3770) -> Xi_c~0 Xi_c0]cc',
            '[psi(3770) -> Xi_c~0 Omega_c0]cc',
            '[psi(3770) -> Omega_c~0 Omega_c0]cc'
        ])
    return line_alg
