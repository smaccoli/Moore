###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q b-hadrons
"""
import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import MeV, picosecond, mm

from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all
from PyConf import configurable

from RecoConf.reconstruction_objects import make_pvs

#############################################
# b-hadron makers for all types of decays   #
# Cuts on mass window, vtxchi2,  detachment #
#############################################


@configurable
def make_b_hadron(particles,
                  descriptor,
                  name='bandq_b_hadron_{hash}',
                  am_min=5100 * MeV,
                  am_max=5550 * MeV,
                  m_min=5140 * MeV,
                  m_max=5510 * MeV,
                  vtx_chi2pdof_max=20,
                  bpvltime_min=0.2 * picosecond,
                  minVDz=0. * mm,
                  minRho=0. * mm,
                  minBPVDira=0.):

    pvs = make_pvs()

    combination_code = (in_range(am_min, F.MASS, am_max))

    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVVDRHO(pvs) > minRho,
        F.BPVVDZ(pvs) > minVDz,
        F.BPVDIRA(pvs) > minBPVDira)

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


####################################
# B0/B0s                           #
####################################


@configurable
def make_bds(particles, descriptor, name='bandq_Bds_{hash}',
             **decay_arguments):
    """
    Return B&Q B0 and Bs with charged final state particles
    """
    return make_b_hadron(particles, descriptor, name=name, **decay_arguments)


@configurable
def make_withneutrals_bds(particles,
                          descriptor,
                          name='bandq_withNeutrals_Bds_{hash}',
                          am_min=4500 * MeV,
                          am_max=6000 * MeV,
                          m_min=4550 * MeV,
                          m_max=5950 * MeV,
                          bpvltime_min=0.3 * picosecond,
                          **decay_arguments):
    """
    Return B&Q B0 and Bs with final state involving neutrals
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


####################################
# B+                               #
####################################


@configurable
def make_bu(particles,
            descriptor,
            name='bandq_Bu_{hash}',
            minVDz=0. * mm,
            minRho=0. * mm,
            minBPVDira=0.,
            **decay_arguments):
    """
    Return B&Q B+.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        minVDz=minVDz,
        minRho=minRho,
        minBPVDira=minBPVDira,
        **decay_arguments)


#Wide mass window due to worse resolution
@configurable
def make_withneutrals_bu(particles,
                         descriptor,
                         name='bandq_withNeutrals_Bu_{hash}',
                         am_min=4500 * MeV,
                         am_max=6000 * MeV,
                         m_min=4550 * MeV,
                         m_max=5950 * MeV,
                         bpvltime_min=0.3 * picosecond,
                         **decay_arguments):
    """
    Return B&Q B+ with final state involving neutrals
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


####################################
# Bc+                              #
####################################


@configurable
def make_bc(particles,
            descriptor,
            name='bandq_Bc_{hash}',
            am_min=6050 * MeV,
            am_max=6555 * MeV,
            m_min=6090 * MeV,
            m_max=6510 * MeV,
            bpvltime_min=0.1 * picosecond,
            **decay_arguments):
    """
    Return B&Q Bc+.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


#Wide mass window due to worse resolution
@configurable
def make_withneutrals_bc(particles,
                         descriptor,
                         name='bandq_withNeutrals_Bc_{hash}',
                         am_min=4500 * MeV,
                         am_max=6750 * MeV,
                         m_min=4550 * MeV,
                         m_max=6700 * MeV,
                         bpvltime_min=0.3 * picosecond,
                         **decay_arguments):
    """
    Return B&Q Bc with final state involving neutrals
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


####################################
# Lb                               #
####################################


@configurable
def make_lb(particles,
            descriptor,
            name='bandq_Lb_{hash}',
            am_min=5350 * MeV,
            am_max=5850 * MeV,
            m_min=5390 * MeV,
            m_max=5810 * MeV,
            **decay_arguments):
    """
    Return B&Q Lb.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)


#Wide mass window due to worse resolution
@configurable
def make_withneutrals_lb(particles,
                         descriptor,
                         name='bandq_withNeutrals_Bds_{hash}',
                         am_min=5000 * MeV,
                         am_max=6500 * MeV,
                         m_min=5050 * MeV,
                         m_max=6450 * MeV,
                         bpvltime_min=0.3 * picosecond,
                         **decay_arguments):
    """
    Return B&Q Lb with final state involving neutrals
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


####################################
# Xib,Sigmab,Omegab                #
####################################


@configurable
def make_b_baryons(particles,
                   descriptor,
                   name='bandq_b_baryons_{hash}',
                   am_min=5350 * MeV,
                   am_max=6460 * MeV,
                   m_min=5390 * MeV,
                   m_max=6410 * MeV,
                   **decay_arguments):
    """
    Return B&Q Xib, Sigmab and Omegab.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)


####################################
# Xibc & di-baryons                #
####################################


@configurable
def make_xibc(particles,
              descriptor,
              name='bandq_Xibc_{hash}',
              am_min=6250 * MeV,
              am_max=8000 * MeV,
              m_min=6290 * MeV,
              m_max=7950 * MeV,
              bpvltime_min=0.1 * picosecond,
              **decay_arguments):
    """
    Return B&Q Xibc.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)
