###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from PyConf import configurable

from Hlt2Conf.standard_particles import make_photons
from Hlt2Conf.standard_particles import make_down_electrons_no_brem, make_long_electrons_with_brem

from Hlt2Conf.lines.config_pid import nopid_electrons

import Functors as F

######################
# converted photons  #
######################
#now only use it for b hadron constructor. See Rec#208.


@configurable
def make_bandq_photons(name='bandq_photons_{hash}',
                       make_particles=make_photons,
                       pt_min=150 * MeV):
    """For the time being just a dummy selection"""
    code = (F.PT > pt_min)
    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_bandq_photons_tight(
        name='bandq_photons_tight_{hash}',
        make_particles=make_photons,
        pt_min=3000 *
        MeV):  #very tight pT cut, as CL is not available at this moment
    """For the time being just a dummy selection"""
    code = (F.PT > pt_min)
    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


################################
#Customize the converted photon#
################################
#electron (down)
@configurable
def make_down_electrons_tightPID(name='bandq_down_electrons_tightPID_{hash}',
                                 make_particles=make_down_electrons_no_brem,
                                 pid=(F.PID_E > 0.)):
    if nopid_electrons():
        pid_cut = None
    else:
        pid_cut = F.FILTER(pid)
    return ParticleFilter(make_particles(), name=name, Cut=pid_cut)


#electron (long, brem recovered)
@configurable
def make_long_electrons_tightPID(name='bandq_long_electrons_tightPID_{hash}',
                                 make_particles=make_long_electrons_with_brem,
                                 pid=(F.PID_E > 0.)):
    if nopid_electrons():
        pid_cut = None
    else:
        pid_cut = F.FILTER(pid)
    return ParticleFilter(make_particles(), name=name, Cut=pid_cut)


#gamma combiners
@configurable
def make_gamma2ee_DD(name='bandq_gamma2ee_DD_{hash}',
                     descriptor="gamma -> e+ e-",
                     am_max=500. * MeV,
                     m_max=100. * MeV,
                     pt_min=580. * MeV,
                     maxVertexChi2=16):

    electrons = make_down_electrons_tightPID()
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_gamma2ee_LL(name='bandq_gamma2ee_LL_{hash}',
                     descriptor="gamma -> e+ e-",
                     am_max=500. * MeV,
                     m_max=100. * MeV,
                     pt_min=580. * MeV,
                     maxVertexChi2=16):

    electrons = make_long_electrons_tightPID()
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
