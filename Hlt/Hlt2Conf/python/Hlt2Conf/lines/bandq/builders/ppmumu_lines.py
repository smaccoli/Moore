###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q dimuon combinations.
"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import MeV, GeV

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from PyConf import configurable

from Hlt2Conf.lines.charmonium_to_dimuon import make_charmonium_muons as make_bandq_muons
from Hlt2Conf.lines.bandq.builders.charged_hadrons import make_prompt_protons

from Hlt2Conf.lines.config_pid import nopid_hadrons, nopid_muons

#################################
#muon and proton basic selector #
#################################


@configurable
def make_ppmumu_basic_muon(name='bandq_ppmumu_basic_muon_{hash}',
                           make_particles=make_bandq_muons,
                           minPt_muon=250 * MeV,
                           minPIDmu=0.):

    code = F.require_all(F.PT > minPt_muon)

    if not nopid_muons():
        code &= F.require_all(F.PID_MU > minPIDmu)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_ppmumu_basic_proton(name='bandq_ppmumu_basic_proton_{hash}',
                             make_particles=make_prompt_protons,
                             minPt_proton=1000 * MeV,
                             minPIDp=0.):

    code = F.require_all(F.PT > minPt_proton)

    if not nopid_hadrons():
        code &= F.require_all(F.PID_P > minPIDp)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


##########################
#ppmumu in charm hadron region
#contain a further selection of p, mu and also the line builder
##########################


#ppmumu in charm hadron region
@configurable
def make_ppmumu_Hc_muon(name='bandq_ppmumu_Hc_muon_{hash}',
                        make_particles=make_ppmumu_basic_muon,
                        mipchi2dvprimary_max=8.,
                        minPIDmu=5.):

    pvs = make_pvs()

    code = F.require_all(F.MINIPCHI2(pvs) < mipchi2dvprimary_max)

    if not nopid_muons():
        code &= F.require_all(F.PID_MU > minPIDmu)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_ppmumu_Hc_proton(name='bandq_ppmumu_Hc_proton_{hash}',
                          make_particles=make_ppmumu_basic_proton,
                          mipchi2dvprimary_max=8.,
                          minPIDp=5.):

    pvs = make_pvs()

    code = F.require_all(F.MINIPCHI2(pvs) < mipchi2dvprimary_max)

    if not nopid_hadrons():
        code &= F.require_all(F.PID_P > minPIDp)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_ppmumu_Hc(name='bandq_ppmumu_Hc_{hash}',
                   am_min=2.8 * GeV,
                   am_max=4.2 * GeV,
                   m_min=3.0 * GeV,
                   m_max=4.0 * GeV,
                   docachi2_max=15.,
                   vtxchi2ndof_max=9):

    muons = make_ppmumu_Hc_muon()
    protons = make_ppmumu_Hc_proton()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(docachi2_max))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtxchi2ndof_max)

    return ParticleCombiner(
        name=name,
        Inputs=[protons, protons, muons, muons],
        DecayDescriptor="h_c(1P) -> p+ p~- mu+ mu-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


##########################
#ppmumu in high mass region
#contain a further selection of p, mu and also the line builder
##########################
@configurable
def make_ppmumu_High_muon(name='bandq_ppmumu_High_muon_{hash}',
                          make_particles=make_ppmumu_basic_muon,
                          mipchi2dvprimary_max=8.,
                          minPt_muon=800 * MeV,
                          minPIDmu=5.):

    pvs = make_pvs()

    code = F.require_all(
        F.MINIPCHI2(pvs) < mipchi2dvprimary_max, F.PT > minPt_muon)

    if not nopid_muons():
        code &= F.require_all(F.PID_MU > minPIDmu)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_ppmumu_High_proton(name='bandq_ppmumu_High_proton_{hash}',
                            make_particles=make_ppmumu_basic_proton,
                            mipchi2dvprimary_max=8.,
                            minPt_proton=1600 * MeV,
                            minPIDp=5.):

    pvs = make_pvs()

    code = F.require_all(
        F.MINIPCHI2(pvs) < mipchi2dvprimary_max, F.PT > minPt_proton)

    if not nopid_hadrons():
        code &= F.require_all(F.PID_P > minPIDp)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_ppmumu_High(name='bandq_ppmumu_High_{hash}',
                     am_min=3.8 * GeV,
                     m_min=4.0 * GeV,
                     docachi2_max=20.,
                     vtxchi2ndof_max=9):

    muons = make_ppmumu_High_muon()
    protons = make_ppmumu_High_proton()

    combination_code = F.require_all(F.MASS > am_min,
                                     F.MAXDOCACHI2CUT(docachi2_max))

    vertex_code = F.require_all(F.MASS > m_min, F.CHI2DOF < vtxchi2ndof_max)

    return ParticleCombiner(
        name=name,
        Inputs=[protons, protons, muons, muons],
        DecayDescriptor="h_c(1P) -> p+ p~- mu+ mu-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


##########################
#ppmumu detached from PV. Loosen cuts in other aspects
#contain a further selection of p, mu and also the line builder
##########################
@configurable
def make_ppmumu_Detached_muon(name='bandq_ppmumu_Detached_muon_{hash}',
                              make_particles=make_ppmumu_basic_muon,
                              mipchi2dvprimary_min=5.):

    pvs = make_pvs()

    code = (F.MINIPCHI2(pvs) > mipchi2dvprimary_min)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_ppmumu_Detached_proton(name='bandq_ppmumu_Detached_proton_{hash}',
                                make_particles=make_ppmumu_basic_proton,
                                mipchi2dvprimary_min=5.):

    pvs = make_pvs()

    code = (F.MINIPCHI2(pvs) < mipchi2dvprimary_min)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_ppmumu_Detached(name='bandq_ppmumu_Detached_{hash}',
                         am_min=2.8 * GeV,
                         am_max=25.0 * GeV,
                         m_min=3.0 * GeV,
                         m_max=20.0 * GeV,
                         docachi2_max=20.,
                         vtxchi2ndof_max=9):

    muons = make_ppmumu_Detached_muon()
    protons = make_ppmumu_Detached_proton()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(docachi2_max))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtxchi2ndof_max)

    return ParticleCombiner(
        name=name,
        Inputs=[protons, protons, muons, muons],
        DecayDescriptor="h_c(1P) -> p+ p~- mu+ mu-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
