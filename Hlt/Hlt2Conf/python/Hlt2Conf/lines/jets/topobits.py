###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Construct topo 2-body combinations as tag in put to HltJetBuilder
"""
import Functors as F
import Functors.math as Fmath
from PyConf import configurable
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from Hlt2Conf.standard_particles import (make_has_rich_long_kaons, make_KsLL,
                                         make_KsDD, make_LambdaLL,
                                         make_LambdaDD)
from Hlt2Conf.algorithms import ParticleContainersMerger
from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond


@configurable
def make_topo_kaons(particles,
                    pvs,
                    pt_min=200 * MeV,
                    p_min=3000 * MeV,
                    trchi2dof_max=3.0,
                    ipchi2_min=4.0):
    """
    Filter kaons for input to Topo 2-body combinations.
    """
    code = F.require_all(F.PT > pt_min, F.P > p_min, F.CHI2DOF < trchi2dof_max,
                         F.MINIPCHI2CUT(pvs, ipchi2_min))
    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_topo_v0s(particles, pvs, bpvltime_min=0.0 * picosecond):
    """Filter V0s for input to Topo 2-body combinations.
    This is an unnecessary pass-through for any standard V0 with a
    displacement cut.
    """
    ## TODO:  Eliminate this step if it remains redundant
    code = F.require_all(F.BPVLTIME(pvs) > bpvltime_min)
    return ParticleFilter(particles, F.FILTER(code))


def make_topo_ksll():
    return make_topo_v0s(make_KsLL(), make_pvs())


def make_topo_ksdd():
    return make_topo_v0s(make_KsDD(), make_pvs())


def make_topo_lambdall():
    return make_topo_v0s(make_LambdaLL(), make_pvs())


def make_topo_lambdadd():
    return make_topo_v0s(make_LambdaDD(), make_pvs())


@configurable
def prepare_topo_particles(pvs):
    """
    Prepare inputs and 2-body combinations for making Topo candidates 
    """
    kaons = make_topo_kaons(make_has_rich_long_kaons(), pvs)
    lambdall = make_topo_lambdall()
    lambdadd = make_topo_lambdadd()

    particles = {
        'K+': kaons,
        'K-': kaons,
        'KS0': make_topo_ksll(),
        'Lambda0': lambdall,
        'Lambda~0': lambdall,
        'KS0dd': make_topo_ksdd(),
        'Lambda0dd': lambdadd,
        'Lambda~0dd': lambdadd
    }
    inputs = {}
    for pid in particles:
        descriptor = f'K*(892)0 -> K+ {pid}'
        inputs[descriptor] = [kaons, particles[pid]]
    for pid in particles.keys() - {'K+'}:
        descriptor = f'K*(892)0 -> K- {pid}'
        inputs[descriptor] = [kaons, particles[pid]]
    return inputs


@configurable
def make_topo_2body(apt_min=2000. * MeV,
                    adocachi2_max=1000.,
                    am_max=10. * GeV,
                    ipchi2_max_an=16.,
                    an_ipchi2_max_an=2,
                    bpvvdchi2_min=16.,
                    bpveta_min=2.0,
                    bpveta_max=5.0):
    """
    Make generic Topo 2-body candidates used as tagging input to jet builder.
    """
    pvs = make_pvs()
    combination_code = F.require_all(
        F.PT > apt_min,
        F.MASS() < am_max,
        F.MAXDOCACHI2CUT(adocachi2_max),
        F.SUM(F.IS_ABS_ID("K+") & (F.BPVIPCHI2(pvs) < ipchi2_max_an)) <
        an_ipchi2_max_an,
    )
    vertex_code = F.require_all(
        F.CHI2DOF() < adocachi2_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        Fmath.in_range(bpveta_min, F.BPVETA(pvs), bpveta_max),
    )

    inputs = prepare_topo_particles(pvs)
    outputs = []
    for descriptor, particles in inputs.items():
        decaydescriptor = descriptor.strip("dd")
        output = ParticleCombiner(
            Inputs=particles,
            DecayDescriptor=decaydescriptor,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
            name=descriptor + '_topo_2body')
        outputs.append(output)

    outputs_merged = ParticleContainersMerger(outputs, name="topo_2body")
    return outputs_merged


@configurable
def make_topo_2body_with_svtag(apt_min=2000. * MeV,
                               adocachi2_max=10.,
                               prod_ghostprob_max=0.2,
                               prod_pt_min=500. * MeV,
                               prod_ipchi2_min=16.,
                               am_max=10. * GeV,
                               ipchi2_max_an=16.,
                               an_ipchi2_max_an=2,
                               bpvvdchi2_min=25.,
                               bpveta_min=2.0,
                               bpveta_max=5.0):
    """
    Make generic Topo 2-body candidates used as tagging input to jet builder,
    including cuts to do SV tag
    """
    pvs = make_pvs()
    combination_code = F.require_all(
        F.PT > apt_min,
        F.MASS() < am_max,
        F.MAXDOCACHI2CUT(adocachi2_max),
        F.SUM(F.IS_ABS_ID("K+") & (F.BPVIPCHI2(pvs) < ipchi2_max_an)) <
        an_ipchi2_max_an,
    )
    vertex_code = F.require_all(
        F.MINTREE(F.IS_ABS_ID('K+'), F.GHOSTPROB()) < prod_ghostprob_max,
        F.MINTREE(F.IS_ABS_ID('K+'), F.MINIPCHI2(pvs)) > prod_ipchi2_min,
        F.MINTREE(F.ALL, F.PT) > prod_pt_min,
        F.CHI2DOF() < adocachi2_max,
        Fmath.in_range(bpveta_min, F.BPVETA(pvs), bpveta_max),
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )

    inputs = prepare_topo_particles(pvs)
    outputs = []
    for descriptor, particles in inputs.items():
        decaydescriptor = descriptor.strip("dd")
        output = ParticleCombiner(
            Inputs=particles,
            DecayDescriptor=decaydescriptor,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
            name=descriptor + '_topo_2body_SVtag')
        outputs.append(output)

    outputs_merged = ParticleContainersMerger(
        outputs, name="topo_2body_with_svtag")
    return outputs_merged
