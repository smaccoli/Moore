###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builders of cut-based inclusive dilepton selection lines.
Based on Rare Decays b -> ll and b -> X ll builders (Titus Mombächer/Richard Williams).

Includes:
    B_s0 -> e+ e- (cc)
    B_s0 -> e+ mu- (cc)
    B_s0 -> mu+ mu- (cc)

    B+ -> J/psi(1S)  (-> e+ e- (cc)) pi+ (cc)
    B+ -> J/psi(1S)  (-> e+ mu- (cc)) pi+ (cc)
    B+ -> J/psi(1S)  (-> mu+ mu- (cc)) pi+ (cc)

    B+ -> J/psi(1S)  (-> e+ e- (cc)) (pi+ pi- (cc)) (cc)
    B+ -> J/psi(1S)  (-> e+ mu- (cc)) (pi+ pi- (cc)
    B+ -> J/psi(1S)  (-> mu+ mu- (cc)) pi+ pi- (cc)

Each filter has kwargs for same-sign and fake (reversed PID) variants.

Author: Jamie Gooding (jamie.gooding@cern.ch)
Date: 03.02.2023
"""

import Functors as F
from PyConf import configurable

from Hlt2Conf.algorithms_thor import ParticleCombiner
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_electrons, make_rd_detached_muons, make_rd_detached_pions

from GaudiKernel.SystemOfUnits import MeV
from RecoConf.reconstruction_objects import make_pvs


####################################
# Base builders                    #
####################################
@configurable
def make_inclusive_cut_dilepton(
        lepton1,
        lepton2,
        pvs,
        DecayDescriptor,
        name="make_inclusive_cut_dilepton",
        two_body=False,
        #dilepton (as resonance) cuts
        vchi2pdof_dilepton_max=9.,
        bpvFDchi2_dilepton_min=300.,
        adocachi2cut_dilepton_max=30.,
        #dilepton (as B) cuts
        vchi2pdof_B_max=9.,
        bpvIPchi2_B_max=16.,
        bpvFDchi2_B_min=300.,
):

    combination_code = F.require_all(
        F.MAXDOCACHI2CUT(adocachi2cut_dilepton_max))

    pvs = make_pvs()

    if two_body:  # Build as initial state candidate
        vertex_code = F.require_all(
            F.CHI2DOF < vchi2pdof_B_max,
            F.BPVIPCHI2(pvs) < bpvIPchi2_B_max,
            F.BPVFDCHI2(pvs) > bpvFDchi2_B_min,
        )
    else:  # Build as intermediate candidate
        vertex_code = F.require_all(
            F.CHI2DOF < vchi2pdof_dilepton_max,
            F.BPVFDCHI2(pvs) > bpvFDchi2_dilepton_min,
        )

    return ParticleCombiner([lepton1, lepton2],
                            name=name,
                            AllowDiffInputsForSameIDChildren=True,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_inclusive_cut_dilepton_plus_tracks(
        dileptons,
        pvs,
        DecayDescriptor,
        name="make_inclusive_cut_dilepton_tracks_{hash}",
        n_tracks=1,
        #track cuts
        p_pion_min=2000. * MeV,
        pt_pion_min=250. * MeV,
        ipchi2_pion_min=9.,
        #b cuts
        pt_B_min=0 * MeV,
        vchi2pdof_B_max=9,
        bpvIPchi2_B_max=16.,
        bpvFDchi2_B_min=300.):

    tracks = make_rd_detached_pions(
        name="make_inclusive_cut_dilepton_tracks",
        p_min=p_pion_min,
        pt_min=pt_pion_min,
        mipchi2dvprimary_min=ipchi2_pion_min,
        pid=None)

    combination_code = F.require_all(F.SUM(F.PT) > pt_B_min, )

    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_B_max,
        F.BPVIPCHI2(pvs) < bpvIPchi2_B_max,
        F.BPVFDCHI2(pvs) > bpvFDchi2_B_min,
    )

    particles = [dileptons]
    for i in range(n_tracks):
        particles.append(tracks)

    return ParticleCombiner(
        particles,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        name=name,
        CompositeCut=vertex_code,
    )


####################################
# Specific dilepton builders       #
####################################


@configurable
def make_inclusive_cut_dielectron(
        name="make_inclusive_cut_dielectron_{hash}",
        parent_id='J/psi(1S)',
        #electron cuts
        pt_electron_min=0. * MeV,
        p_electron_min=0. * MeV,
        ipchi2_electron_min=36.,
        pide_electron_min=2.,
        #line categories
        same_sign=False,
        fake_electrons=0,
        two_body=False):
    """
    Make the detached dielectron, opposite-sign or same-sign with option to reverse PID (fake line).
    """
    assert type(
        fake_electrons
    ) == int and 0 <= fake_electrons <= 2, "Invalid number of fake electrons in builder, fake_electrons must be an integer between 0 and 2."
    if fake_electrons == 1:
        electrons = [
            make_rd_detached_electrons(
                name=electrons_name,
                pt_min=pt_electron_min,
                p_min=p_electron_min,
                mipchi2dvprimary_min=ipchi2_electron_min,
                pid=pid) for electrons_name, pid in zip((
                    "make_inclusive_cut_electron_{hash}",
                    "make_inclusive_cut_electron_fake_{hash}"), (
                        (F.PID_E > pide_electron_min),
                        (F.PID_E < pide_electron_min)))
        ]
    else:
        if fake_electrons == 2:
            electrons_name = "make_inclusive_cut_electron_fake_{hash}"
            pid = (F.PID_E < pide_electron_min)
        else:
            electrons_name = "make_inclusive_cut_electron_{hash}"
            pid = (F.PID_E > pide_electron_min)
        electron = make_rd_detached_electrons(
            name=electrons_name,
            pt_min=pt_electron_min,
            p_min=p_electron_min,
            mipchi2dvprimary_min=ipchi2_electron_min,
            pid=pid)
        electrons = [electron, electron]

    DecayDescriptor = f'{parent_id} -> e+ e-'
    if same_sign: DecayDescriptor = f'[{parent_id} -> e+ e+]cc'

    pvs = make_pvs()

    return make_inclusive_cut_dilepton(
        *electrons,
        pvs,
        DecayDescriptor=DecayDescriptor,
        name=name,
        two_body=two_body)


@configurable
def make_inclusive_cut_electronmuon(
        name="make_inclusive_cut_electronmuon_{hash}",
        parent_id='J/psi(1S)',
        #lepton cuts
        pt_lepton_min=0. * MeV,
        p_lepton_min=0. * MeV,
        ipchi2_lepton_min=36.,
        pide_electron_min=2.,
        pidmu_muon_min=2.,
        #line categories
        same_sign=False,
        fake_electron=False,
        fake_muon=False,
        two_body=False):
    """
    Make the detached electron-muon pair, opposite-sign or same-sign with option to reverse PID (fake line).
    """
    if fake_electron:
        electrons_name = "make_inclusive_cut_electron_fake_{hash}"
        pide = (F.PID_E < pide_electron_min)
    else:
        electrons_name = "make_inclusive_cut_electron_{hash}"
        pide = (F.PID_E > pide_electron_min)
    if fake_muon:
        muons_name = "make_inclusive_cut_muon_fake_{hash}"
        pidmu = (F.PID_MU < pidmu_muon_min)
    else:
        muons_name = "make_inclusive_cut_muon_{hash}"
        pidmu = F.require_all(F.ISMUON, F.PID_MU > pidmu_muon_min)

    electron = make_rd_detached_electrons(
        name=electrons_name,
        pt_min=pt_lepton_min,
        p_min=p_lepton_min,
        mipchi2dvprimary_min=ipchi2_lepton_min,
        pid=pide)

    muon = make_rd_detached_muons(
        name=muons_name,
        pt_min=pt_lepton_min,
        p_min=p_lepton_min,
        mipchi2dvprimary_min=ipchi2_lepton_min,
        pid=pidmu)

    DecayDescriptor = f'[{parent_id} -> e+ mu-]cc'
    if same_sign: DecayDescriptor = f'[{parent_id} -> e+ mu+]cc'

    pvs = make_pvs()

    return make_inclusive_cut_dilepton(
        electron,
        muon,
        pvs,
        DecayDescriptor=DecayDescriptor,
        name=name,
        two_body=two_body)


@configurable
def make_inclusive_cut_dimuon(
        name="make_inclusive_cut_dimuon_{hash}",
        parent_id='J/psi(1S)',
        #muon cuts
        pt_muon_min=0. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=36.,
        pidmu_muon_min=2.,
        #line categories
        same_sign=False,
        fake_muons=0,
        two_body=False):
    """
    Make the detached dimuon, opposite-sign or same-sign with option to reverse PID (fake line).
    """
    assert type(
        fake_muons
    ) == int and 0 <= fake_muons <= 2, "Invalid number of fake muons in builder, fake_muons must be an integer between 0 and 2."
    if fake_muons == 1:
        muons = [
            make_rd_detached_muons(
                name=muons_name,
                pt_min=pt_muon_min,
                p_min=p_muon_min,
                mipchi2dvprimary_min=ipchi2_muon_min,
                pid=pid) for muons_name, pid in zip((
                    "make_inclusive_cut_muon_{hash}",
                    "make_inclusive_cut_muon_fake_{hash}"), (
                        F.require_all(F.ISMUON, F.PID_MU > pidmu_muon_min),
                        (F.PID_MU < pidmu_muon_min)))
        ]
    else:
        if fake_muons == 2:
            muons_name = "make_inclusive_cut_muon_fake_{hash}"
            pid = (F.PID_MU < pidmu_muon_min)
        else:
            muons_name = "make_inclusive_cut_muon_{hash}"
            pid = F.require_all(F.ISMUON, F.PID_MU > pidmu_muon_min)
        muon = make_rd_detached_muons(
            name=muons_name,
            pt_min=pt_muon_min,
            p_min=p_muon_min,
            mipchi2dvprimary_min=ipchi2_muon_min,
            pid=pid)
        muons = [muon, muon]

    DecayDescriptor = f'{parent_id} -> mu+ mu-'
    if same_sign: DecayDescriptor = f'[{parent_id} -> mu+ mu+]cc'

    pvs = make_pvs()

    return make_inclusive_cut_dilepton(
        *muons,
        pvs,
        DecayDescriptor=DecayDescriptor,
        name=name,
        two_body=two_body)
