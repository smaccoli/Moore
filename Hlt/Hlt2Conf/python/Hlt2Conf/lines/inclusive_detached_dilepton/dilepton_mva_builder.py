###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Functors as F
import Functors.math as fmath
from PyConf import configurable
from GaudiKernel.SystemOfUnits import GeV, MeV, ps
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dielectron, make_rd_detached_pions, make_rd_detached_muons
from Hlt2Conf.standard_particles import make_detached_mue_with_brem
from RecoConf.reconstruction_objects import make_pvs
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter


@configurable
def make_neutral_lls(name="rd_neutral_mesons_lls_{hash}",
                     pi_p_min=2. * GeV,
                     pi_pt_min=0. * GeV,
                     pi_ipchi2_min=9.,
                     adocachi2cut=30.,
                     bpvvdchi2_min=4.,
                     bpvltime_min=1. * ps,
                     vchi2pdof_max=25.):

    pions = make_rd_detached_pions(
        name="rd_detached_pions_{hash}",
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        mipchi2dvprimary_min=pi_ipchi2_min,
        pid=None)
    descriptor = 'KS0 -> pi+ pi-'
    combination_code = F.require_all(F.MAXDOCACHI2CUT(adocachi2cut))
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    return ParticleCombiner([pions, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_inclusive_dilepton_detached_mue(
        name="inclusive_dilepton_detached_mue_{hash}",
        min_dilepton_mass=0. * MeV,
        max_dilepton_mass=6000. * MeV,
        parent_id='J/psi(1S)',
        min_probnn_mu=0.,
        min_PIDmu=0.,
        IsMuon=False,
        min_PIDe=2.,
        min_pt_e=0.25 * GeV,
        min_pt_mu=0.25 * GeV,
        min_bpvvdchi2=30.,
        max_vchi2ndof=10.,
        same_sign=False):
    """
    Make the detached muon-electron pair, opposite-sign or same-sign.
    """
    dileptons = make_detached_mue_with_brem(
        dilepton_ID=parent_id,
        min_probnn_mu=min_probnn_mu,
        min_PIDmu=min_PIDmu,
        IsMuon=IsMuon,
        min_PIDe=min_PIDe,
        opposite_sign=not same_sign,
        min_pt_e=min_pt_e,
        min_pt_mu=min_pt_mu,
        min_bpvvdchi2=min_bpvvdchi2,
        max_vchi2ndof=max_vchi2ndof)
    code = F.require_all(
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass))
    return ParticleFilter(dileptons, F.FILTER(code), name=name)


@configurable
def make_inclusive_dilepton_detached_dimuon(
        name="inclusive_dilepton_detached_dimuon_{hash}",
        parent_id='J/psi(1S)',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=300. * MeV,
        p_muon_min=3000. * MeV,
        ipchi2_muon_min=9.,
        pidmu_muon_min=3.,
        IsMuon=False,
        adocachi2cut_max=30.,
        bpvvdchi2_min=30.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6000. * MeV,
        same_sign=False):
    """
    Make the detached dimuon, opposite-sign or same-sign.
    """
    if (IsMuon):
        pid = F.require_all(F.ISMUON, F.PID_MU > pidmu_muon_min)
    else:
        pid = (F.PID_MU > pidmu_muon_min)

    muons = make_rd_detached_muons(
        name="rd_detached_muons_{hash}",
        pt_min=pt_muon_min,
        p_min=p_muon_min,
        mipchi2dvprimary_min=ipchi2_muon_min,
        pid=pid)

    DecayDescriptor = f'{parent_id} -> mu+ mu-'
    if same_sign: DecayDescriptor = f'[{parent_id} -> mu+ mu+]cc'

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_dimuon_min,
        F.MAXDOCACHI2CUT(adocachi2cut_max))
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    return ParticleCombiner([muons, muons],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_dielectron_plus_track(
        name="rd_detached_dielectron_plus_track_{hash}",
        pid_e_min=2.,
        pt_e_min=0.25 * GeV,
        ipchi2_e_min=9.,
        parent_id="B+",
        opposite_sign=True,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=30.,
        vfaspfchi2ndof_max=10.,
        am_min=0. * MeV,
        am_max=6000. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=2000 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=16.,
        min_b_vtx_chi2=10.,
        p_pion_min=2. * GeV,
        pt_pion_min=250. * MeV,
        ipchi2_pion_min=9.):
    """
    Make the detached e+e- pair with the proper bremsstrahlung correction handling.
    """
    dielectrons = make_rd_detached_dielectron(
        name="rd_detached_dielectron_{hash}",
        pid_e_min=pid_e_min,
        pt_e_min=pt_e_min,
        ipchi2_e_min=ipchi2_e_min,
        parent_id="J/psi(1S)",
        opposite_sign=opposite_sign,
        pt_diE_min=pt_diE_min,
        adocachi2cut_min=adocachi2cut_min,
        bpvvdchi2_min=bpvvdchi2_min,
        vfaspfchi2ndof_max=vfaspfchi2ndof_max,
        am_min=am_min,
        am_max=am_max)

    pions = make_rd_detached_pions(
        name="rd_detached_pions_{hash}",
        p_min=p_pion_min,
        pt_min=pt_pion_min,
        mipchi2dvprimary_min=ipchi2_pion_min,
        pid=None)

    DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi+]cc'
    if not opposite_sign: DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi-]cc'

    combination_code = F.require_all(F.PT > pt_b_min,
                                     in_range(m_b_min, F.MASS, m_b_max))

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < min_b_vtx_chi2,
                                F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner([dielectrons, pions],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_mue_plus_track(name="rd_detached_mue_plus_track_{hash}",
                                    max_dilepton_mass=6000. * MeV,
                                    parent_id='B+',
                                    min_probnn_mu=None,
                                    min_PIDmu=None,
                                    IsMuon=False,
                                    min_PIDe=2.,
                                    min_pt_e=0.25 * GeV,
                                    min_pt_mu=0.25 * GeV,
                                    min_bpvvdchi2=30.,
                                    max_vchi2ndof=10.,
                                    same_sign=False,
                                    pt_b_min=0 * GeV,
                                    m_b_min=2000 * MeV,
                                    m_b_max=10000 * MeV,
                                    fdchi2_min=16.,
                                    min_b_vtx_chi2=10.,
                                    p_pion_min=2. * GeV,
                                    pt_pion_min=250. * MeV,
                                    ipchi2_pion_min=9.):
    """
    Make the detached muon-electron pair, opposite-sign or same-sign.
    """
    dileptons = make_inclusive_dilepton_detached_mue(
        name="rd_detached_mue_{hash}",
        max_dilepton_mass=max_dilepton_mass,
        parent_id='J/psi(1S)',
        min_probnn_mu=min_probnn_mu,
        min_PIDmu=min_PIDmu,
        IsMuon=IsMuon,
        min_PIDe=min_PIDe,
        min_pt_e=min_pt_e,
        min_pt_mu=min_pt_mu,
        min_bpvvdchi2=min_bpvvdchi2,
        max_vchi2ndof=max_vchi2ndof,
        same_sign=same_sign)

    pions = make_rd_detached_pions(
        name="rd_detached_pions_{hash}",
        p_min=p_pion_min,
        pt_min=pt_pion_min,
        mipchi2dvprimary_min=ipchi2_pion_min,
        pid=None)

    DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi+]cc'
    if same_sign: DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi-]cc'

    combination_code = F.require_all(F.PT > pt_b_min,
                                     in_range(m_b_min, F.MASS, m_b_max))

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < min_b_vtx_chi2,
                                F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner([dileptons, pions],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_dielectron_plus_2track(
        name="rd_detached_dielectron_plus_2track_{hash}",
        pid_e_min=2.,
        pt_e_min=0.25 * GeV,
        ipchi2_e_min=9.,
        parent_id="B0",
        opposite_sign_leptons=True,
        same_sign_pions=False,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=30.,
        vfaspfchi2ndof_max=10.,
        am_min=0. * MeV,
        am_max=6000. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=2000 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=16.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=250. * MeV,
        ipchi2_pion_min=9.):
    """
    Make the detached e+e- pair with the proper bremsstrahlung correction handling.
    """
    dielectrons = make_rd_detached_dielectron(
        name="rd_detached_dielectron_{hash}",
        pid_e_min=pid_e_min,
        pt_e_min=pt_e_min,
        ipchi2_e_min=ipchi2_e_min,
        parent_id="J/psi(1S)",
        opposite_sign=opposite_sign_leptons,
        pt_diE_min=pt_diE_min,
        adocachi2cut_min=adocachi2cut_min,
        bpvvdchi2_min=bpvvdchi2_min,
        vfaspfchi2ndof_max=vfaspfchi2ndof_max,
        am_min=am_min,
        am_max=am_max)

    pions = make_rd_detached_pions(
        name="rd_detached_pions_{hash}",
        p_min=p_pion_min,
        pt_min=pt_pion_min,
        mipchi2dvprimary_min=ipchi2_pion_min,
        pid=None)

    DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi+ pi-]cc'
    if same_sign_pions:
        DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi- pi-]cc'

    combination_code = F.require_all(F.PT > pt_b_min,
                                     in_range(m_b_min, F.MASS, m_b_max))

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < min_b_vtx_chi2,
                                F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner([dielectrons, pions, pions],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_mue_plus_2track(name="rd_detached_mue_plus_2track_{hash}",
                                     max_dilepton_mass=6000. * MeV,
                                     parent_id='B0',
                                     min_probnn_mu=None,
                                     min_PIDmu=None,
                                     IsMuon=False,
                                     min_PIDe=2.,
                                     min_pt_e=0.25 * GeV,
                                     min_pt_mu=0.25 * GeV,
                                     min_bpvvdchi2=30.,
                                     max_vchi2ndof=10.,
                                     same_sign_leptons=False,
                                     same_sign_pions=False,
                                     pt_b_min=0 * GeV,
                                     m_b_min=2000 * MeV,
                                     m_b_max=10000 * MeV,
                                     fdchi2_min=16.,
                                     min_b_vtx_chi2=30.,
                                     p_pion_min=2. * GeV,
                                     pt_pion_min=250. * MeV,
                                     ipchi2_pion_min=9.):
    """
    Make the detached muon-electron pair, opposite-sign or same-sign.
    """
    dileptons = make_inclusive_dilepton_detached_mue(
        name="rd_detached_mue_{hash}",
        max_dilepton_mass=max_dilepton_mass,
        parent_id='J/psi(1S)',
        min_probnn_mu=min_probnn_mu,
        min_PIDmu=min_PIDmu,
        IsMuon=IsMuon,
        min_PIDe=min_PIDe,
        min_pt_e=min_pt_e,
        min_pt_mu=min_pt_mu,
        min_bpvvdchi2=min_bpvvdchi2,
        max_vchi2ndof=max_vchi2ndof,
        same_sign=same_sign_leptons)

    pions = make_rd_detached_pions(
        name="rd_detached_pions_{hash}",
        p_min=p_pion_min,
        pt_min=pt_pion_min,
        mipchi2dvprimary_min=ipchi2_pion_min,
        pid=None)

    DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi+ pi-]cc'
    if same_sign_pions:
        DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi- pi-]cc'

    combination_code = F.require_all(F.PT > pt_b_min,
                                     in_range(m_b_min, F.MASS, m_b_max))

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < min_b_vtx_chi2,
                                F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner([dileptons, pions, pions],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_dimuon_plus_track(
        name="rd_detached_dimuon_plus_track_{hash}",
        parent_id='B+',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=300. * MeV,
        p_muon_min=3000. * MeV,
        ipchi2_muon_min=9.,
        pidmu_muon_min=0.,
        IsMuon=False,
        adocachi2cut_max=30.,
        bpvvdchi2_min=30.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=16000. * MeV,
        same_sign=False,
        pt_b_min=0 * GeV,
        m_b_min=2500 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=16.,
        min_b_vtx_chi2=10.,
        p_pion_min=2. * GeV,
        pt_pion_min=250. * MeV,
        ipchi2_pion_min=9.):
    """
    Make the detached dimuon, opposite-sign or same-sign.
    """

    dimuon = make_inclusive_dilepton_detached_dimuon(
        name="inclusive_dilepton_detached_dimuon_{hash}",
        parent_id='J/psi(1S)',
        pt_dimuon_min=pt_dimuon_min,
        pt_muon_min=pt_muon_min,
        p_muon_min=p_muon_min,
        ipchi2_muon_min=ipchi2_muon_min,
        pidmu_muon_min=pidmu_muon_min,
        IsMuon=IsMuon,
        adocachi2cut_max=adocachi2cut_max,
        bpvvdchi2_min=bpvvdchi2_min,
        vchi2pdof_max=vchi2pdof_max,
        am_min=am_min,
        am_max=am_max,
        same_sign=same_sign)

    pions = make_rd_detached_pions(
        name="rd_detached_pions_{hash}",
        p_min=p_pion_min,
        pt_min=pt_pion_min,
        mipchi2dvprimary_min=ipchi2_pion_min,
        pid=None)

    DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi+]cc'
    if same_sign: DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi-]cc'

    combination_code = F.require_all(F.PT > pt_b_min,
                                     in_range(m_b_min, F.MASS, m_b_max))
    pvs = make_pvs()

    vertex_code = F.require_all(F.CHI2DOF < min_b_vtx_chi2,
                                F.BPVFDCHI2(pvs) > fdchi2_min)
    return ParticleCombiner([dimuon, pions],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_dimuon_plus_2track(
        name="rd_detached_dimuon_plus_2track_{hash}",
        parent_id='B0',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=300. * MeV,
        p_muon_min=3000. * MeV,
        ipchi2_muon_min=9.,
        pidmu_muon_min=0.,
        IsMuon=False,
        adocachi2cut_max=30.,
        bpvvdchi2_min=30.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=16000. * MeV,
        same_sign_leptons=False,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=2500 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=16.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=250. * MeV,
        ipchi2_pion_min=9.):
    """
    Make the detached dimuon, opposite-sign or same-sign.
    """

    dimuon = make_inclusive_dilepton_detached_dimuon(
        name="inclusive_dilepton_detached_dimuon_{hash}",
        parent_id='J/psi(1S)',
        pt_dimuon_min=pt_dimuon_min,
        pt_muon_min=pt_muon_min,
        p_muon_min=p_muon_min,
        ipchi2_muon_min=ipchi2_muon_min,
        pidmu_muon_min=pidmu_muon_min,
        IsMuon=IsMuon,
        adocachi2cut_max=adocachi2cut_max,
        bpvvdchi2_min=bpvvdchi2_min,
        vchi2pdof_max=vchi2pdof_max,
        am_min=am_min,
        am_max=am_max,
        same_sign=same_sign_leptons)

    pions = make_rd_detached_pions(
        name="rd_detached_pions_{hash}",
        p_min=p_pion_min,
        pt_min=pt_pion_min,
        mipchi2dvprimary_min=ipchi2_pion_min,
        pid=None)

    DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi+ pi-]cc'
    if same_sign_pions:
        DecayDescriptor = f'[{parent_id} -> J/psi(1S) pi- pi-]cc'

    combination_code = F.require_all(F.PT > pt_b_min,
                                     in_range(m_b_min, F.MASS, m_b_max))

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < min_b_vtx_chi2,
                                F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner(
        Inputs=[dimuon, pions, pions],
        name=name,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_rd_detached_dielectron_plus_neutral(
        name="rd_detached_dielectron_plus_neutral_{hash}",
        pid_e_min=2.,
        pt_e_min=0.25 * GeV,
        ipchi2_e_min=9.,
        parent_id="B0",
        opposite_sign_leptons=True,
        same_sign_pions=False,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=30.,
        vfaspfchi2ndof_max=10.,
        am_min=0. * MeV,
        am_max=6000. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=2000 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=16.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=250. * MeV,
        ipchi2_pion_min=9.,
        neutral_bpvvdchi2_min=4.0,
        neutral_bpvltime_min=1. * ps,
        neutral_vchi2pdof_max=25.):
    """
    Make the detached e+e- pair with the proper bremsstrahlung correction handling.
    """
    dielectrons = make_rd_detached_dielectron(
        name="rd_detached_dielectron_{hash}",
        pid_e_min=pid_e_min,
        pt_e_min=pt_e_min,
        ipchi2_e_min=ipchi2_e_min,
        parent_id="J/psi(1S)",
        opposite_sign=opposite_sign_leptons,
        pt_diE_min=pt_diE_min,
        adocachi2cut_min=adocachi2cut_min,
        bpvvdchi2_min=bpvvdchi2_min,
        vfaspfchi2ndof_max=vfaspfchi2ndof_max,
        am_min=am_min,
        am_max=am_max)

    neutrals = make_neutral_lls(
        name="rd_neutral_mesons_lls_{hash}",
        pi_p_min=p_pion_min,
        pi_pt_min=pt_pion_min,  # has to be 0 !!!
        pi_ipchi2_min=ipchi2_pion_min,
        adocachi2cut=adocachi2cut_min,
        bpvvdchi2_min=neutral_bpvvdchi2_min,
        bpvltime_min=neutral_bpvltime_min,
        vchi2pdof_max=neutral_vchi2pdof_max)

    DecayDescriptor = f'[{parent_id} -> J/psi(1S) KS0]cc'

    combination_code = F.require_all(F.PT > pt_b_min,
                                     in_range(m_b_min, F.MASS, m_b_max))

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < min_b_vtx_chi2,
                                F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner([dielectrons, neutrals],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_mue_plus_neutral(
        name="rd_detached_mue_plus_neutral_{hash}",
        max_dilepton_mass=6000. * MeV,
        parent_id='B0',
        min_probnn_mu=0.,
        min_PIDmu=0.,
        IsMuon=False,
        min_PIDe=2.,
        min_pt_e=0.25 * GeV,
        min_pt_mu=0.25 * GeV,
        min_bpvvdchi2=30.,
        max_vchi2ndof=10.,
        adocachi2cut_min=30.,
        same_sign_leptons=False,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=2000 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=16.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=250. * MeV,
        ipchi2_pion_min=9.,
        neutral_bpvvdchi2_min=4.0,
        neutral_bpvltime_min=1. * ps,
        neutral_vchi2pdof_max=25.):
    """
    Make the detached muon-electron pair, opposite-sign or same-sign.
    """
    dileptons = make_inclusive_dilepton_detached_mue(
        name="rd_detached_mue_{hash}",
        max_dilepton_mass=max_dilepton_mass,
        parent_id='J/psi(1S)',
        min_probnn_mu=min_probnn_mu,
        min_PIDmu=min_PIDmu,
        IsMuon=IsMuon,
        min_PIDe=min_PIDe,
        min_pt_e=min_pt_e,
        min_pt_mu=min_pt_mu,
        min_bpvvdchi2=min_bpvvdchi2,
        max_vchi2ndof=max_vchi2ndof,
        same_sign=same_sign_leptons)

    neutrals = make_neutral_lls(
        name="rd_neutral_mesons_lls_{hash}",
        pi_p_min=p_pion_min,
        pi_pt_min=pt_pion_min,  # has to be 0 !!!
        pi_ipchi2_min=ipchi2_pion_min,
        adocachi2cut=adocachi2cut_min,
        bpvvdchi2_min=neutral_bpvvdchi2_min,
        bpvltime_min=neutral_bpvltime_min,
        vchi2pdof_max=neutral_vchi2pdof_max)

    DecayDescriptor = f'[{parent_id} -> J/psi(1S) KS0]cc'

    combination_code = F.require_all(F.PT > pt_b_min,
                                     in_range(m_b_min, F.MASS, m_b_max))

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < min_b_vtx_chi2,
                                F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner([dileptons, neutrals],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rd_detached_dimuon_plus_neutral(
        name="rd_detached_dimuon_plus_neutral_{hash}",
        parent_id='B0',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=300. * MeV,
        p_muon_min=3000. * MeV,
        ipchi2_muon_min=9.,
        pidmu_muon_min=0.,
        IsMuon=False,
        adocachi2cut_max=30.,
        bpvvdchi2_min=30.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=16000. * MeV,
        same_sign_leptons=False,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=2500 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=16.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=250. * MeV,
        ipchi2_pion_min=9.,
        neutral_bpvvdchi2_min=4.0,
        neutral_bpvltime_min=1. * ps,
        neutral_vchi2pdof_max=25.):
    """
    Make the detached dimuon, opposite-sign or same-sign.
    """
    dimuon = make_inclusive_dilepton_detached_dimuon(
        name="inclusive_dilepton_detached_dimuon_{hash}",
        parent_id='J/psi(1S)',
        pt_dimuon_min=pt_dimuon_min,
        pt_muon_min=pt_muon_min,
        p_muon_min=p_muon_min,
        ipchi2_muon_min=ipchi2_muon_min,
        pidmu_muon_min=pidmu_muon_min,
        IsMuon=IsMuon,
        adocachi2cut_max=adocachi2cut_max,
        bpvvdchi2_min=bpvvdchi2_min,
        vchi2pdof_max=vchi2pdof_max,
        am_min=am_min,
        am_max=am_max,
        same_sign=same_sign_leptons)

    neutrals = make_neutral_lls(
        name="rd_neutral_mesons_lls_{hash}",
        pi_p_min=p_pion_min,
        pi_pt_min=pt_pion_min,  # has to be 0 !!!
        pi_ipchi2_min=ipchi2_pion_min,
        adocachi2cut=adocachi2cut_max,
        bpvvdchi2_min=neutral_bpvvdchi2_min,
        bpvltime_min=neutral_bpvltime_min,
        vchi2pdof_max=neutral_vchi2pdof_max)

    DecayDescriptor = f'[{parent_id} -> J/psi(1S) KS0]cc'

    combination_code = F.require_all(F.PT > pt_b_min,
                                     in_range(m_b_min, F.MASS, m_b_max))

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < min_b_vtx_chi2,
                                F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner(
        Inputs=[dimuon, neutrals],
        name=name,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def filter_dimuon_MVA(pvs=None, dimuons=None, mvacut=0.7):

    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetDimuon_weights_202304.json",
            'Name':
            "SigmaNet_mumu",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "10",
            'Monotone_Constraints':
            '[0,0,1,0,0,1,0,1,0,1]',
            'Variables':
            'TwoBody_LoKi_DIRA_OWNPV,log(TwoBody_LoKi_ENDVERTEX_CHI2),log(TwoBody_LoKi_FDCHI2_OWNPV),log(TwoBody_LoKi_MINIPCHI2),TwoBody_LoKi_Mcorr,TwoBody_LoKi_PT,log(Track1_LoKi_MINIPCHI2),Track1_LoKi_PT,log(Track2_LoKi_MINIPCHI2),Track2_LoKi_PT',
        },
        Inputs={
            "TwoBody_LoKi_DIRA_OWNPV": F.BPVDIRA(pvs),
            "log(TwoBody_LoKi_ENDVERTEX_CHI2)": fmath.log(F.CHI2DOF),
            "log(TwoBody_LoKi_FDCHI2_OWNPV)": fmath.log(F.BPVFDCHI2(pvs)),
            "log(TwoBody_LoKi_MINIPCHI2)": fmath.log(F.BPVIPCHI2(pvs)),
            "TwoBody_LoKi_Mcorr": F.BPVCORRM(pvs) / GeV,
            "TwoBody_LoKi_PT": F.PT / GeV,
            "log(Track1_LoKi_MINIPCHI2)": fmath.log(
                F.CHILD(1, F.BPVIPCHI2(pvs))),
            "Track1_LoKi_PT": F.CHILD(1, F.PT) / GeV,
            "log(Track2_LoKi_MINIPCHI2)": fmath.log(
                F.CHILD(2, F.BPVIPCHI2(pvs))),
            "Track2_LoKi_PT": F.CHILD(2, F.PT) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dimuons, F.FILTER(code))


@configurable
def filter_dielectron_MVA(pvs=None, dielectrons=None, mvacut=0.7):
    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetDielectron_weights_202304.json",
            'Name':
            "SigmaNet_ee",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "10",
            'Monotone_Constraints':
            '[0,0,1,0,0,1,0,1,0,1]',
            'Variables':
            'TwoBody_LoKi_DIRA_OWNPV,log(TwoBody_LoKi_ENDVERTEX_CHI2),log(TwoBody_LoKi_FDCHI2_OWNPV),log(TwoBody_LoKi_MINIPCHI2),TwoBody_LoKi_Mcorr,TwoBody_LoKi_PT,log(Track1_LoKi_MINIPCHI2),Track1_LoKi_PT,log(Track2_LoKi_MINIPCHI2),Track2_LoKi_PT',
        },
        Inputs={
            "TwoBody_LoKi_DIRA_OWNPV": F.BPVDIRA(pvs),
            "log(TwoBody_LoKi_ENDVERTEX_CHI2)": fmath.log(F.CHI2DOF),
            "log(TwoBody_LoKi_FDCHI2_OWNPV)": fmath.log(F.BPVFDCHI2(pvs)),
            "log(TwoBody_LoKi_MINIPCHI2)": fmath.log(F.BPVIPCHI2(pvs)),
            "TwoBody_LoKi_Mcorr": F.BPVCORRM(pvs) / GeV,
            "TwoBody_LoKi_PT": F.PT / GeV,
            "log(Track1_LoKi_MINIPCHI2)": fmath.log(
                F.CHILD(1, F.BPVIPCHI2(pvs))),
            "Track1_LoKi_PT": F.CHILD(1, F.PT) / GeV,
            "log(Track2_LoKi_MINIPCHI2)": fmath.log(
                F.CHILD(2, F.BPVIPCHI2(pvs))),
            "Track2_LoKi_PT": F.CHILD(2, F.PT) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dielectrons, F.FILTER(code))


@configurable
def filter_mue_MVA(pvs=None, dileptons=None, mvacut=0.7):

    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetElectronMuon_weights_202304.json",
            'Name':
            "SigmaNet_mue",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "10",
            'Monotone_Constraints':
            '[0,0,1,0,0,1,0,1,0,1]',
            'Variables':
            'TwoBody_LoKi_DIRA_OWNPV,log(TwoBody_LoKi_ENDVERTEX_CHI2),log(TwoBody_LoKi_FDCHI2_OWNPV),log(TwoBody_LoKi_MINIPCHI2),TwoBody_LoKi_Mcorr,TwoBody_LoKi_PT,log(Track1_LoKi_MINIPCHI2),Track1_LoKi_PT,log(Track2_LoKi_MINIPCHI2),Track2_LoKi_PT',
        },
        Inputs={
            "TwoBody_LoKi_DIRA_OWNPV": F.BPVDIRA(pvs),
            "log(TwoBody_LoKi_ENDVERTEX_CHI2)": fmath.log(F.CHI2DOF),
            "log(TwoBody_LoKi_FDCHI2_OWNPV)": fmath.log(F.BPVFDCHI2(pvs)),
            "log(TwoBody_LoKi_MINIPCHI2)": fmath.log(F.BPVIPCHI2(pvs)),
            "TwoBody_LoKi_Mcorr": F.BPVCORRM(pvs) / GeV,
            "TwoBody_LoKi_PT": F.PT / GeV,
            "log(Track1_LoKi_MINIPCHI2)": fmath.log(
                F.CHILD(1, F.BPVIPCHI2(pvs))),
            "Track1_LoKi_PT": F.CHILD(1, F.PT) / GeV,
            "log(Track2_LoKi_MINIPCHI2)": fmath.log(
                F.CHILD(2, F.BPVIPCHI2(pvs))),
            "Track2_LoKi_PT": F.CHILD(2, F.PT) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dileptons, F.FILTER(code))


@configurable
def filter_dimuon_plus_track_MVA(pvs=None, dimuons=None, mvacut=0.7):

    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetDimuon_3B_weights_202304.json",
            'Name':
            "SigmaNet_mumu_plus_track",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "17",
            'Monotone_Constraints':
            '[0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0]',
            'Variables':
            'ThreeBody_DIRA_OWNPV,log(ThreeBody_ENDVERTEX_CHI2),log(ThreeBody_FDCHI2_OWNPV),log(ThreeBody_MINIPCHI2),ThreeBody_PT,TwoBody_DIRA_OWNPV,log(TwoBody_ENDVERTEX_CHI2),log(TwoBody_FDCHI2_OWNPV),log(TwoBody_MINIPCHI2),TwoBody_Mcorr,TwoBody_PT,log(Track1_MINIPCHI2),Track1_PT,log(Track2_MINIPCHI2),Track2_PT,log(Track3_MINIPCHI2),Track3_PT',
        },
        Inputs={
            "ThreeBody_DIRA_OWNPV":
            F.BPVDIRA(pvs),
            "log(ThreeBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHI2DOF),
            "log(ThreeBody_FDCHI2_OWNPV)":
            fmath.log(F.BPVFDCHI2(pvs)),
            "log(ThreeBody_MINIPCHI2)":
            fmath.log(F.BPVIPCHI2(pvs)),
            "ThreeBody_PT":
            F.PT / GeV,
            "TwoBody_DIRA_OWNPV":
            F.CHILD(1, F.BPVDIRA(pvs)),
            "log(TwoBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(1, F.CHI2DOF)),
            "log(TwoBody_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
            "log(TwoBody_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.BPVIPCHI2(pvs))),
            "TwoBody_Mcorr":
            F.CHILD(1, F.BPVCORRM(pvs)) / GeV,
            "TwoBody_PT":
            F.CHILD(1, F.PT) / GeV,
            "log(Track1_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track1_PT":
            F.CHILD(1, F.CHILD(1, F.PT)) / GeV,
            "log(Track2_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track2_PT":
            F.CHILD(1, F.CHILD(2, F.PT)) / GeV,
            "log(Track3_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.BPVIPCHI2(pvs))),
            "Track3_PT":
            F.CHILD(2, F.PT) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dimuons, F.FILTER(code))


@configurable
def filter_dielectron_plus_track_MVA(pvs=None, dielectrons=None, mvacut=0.7):
    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetDielectron_3B_weights_202304.json",
            'Name':
            "SigmaNet_ee_plus_track",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "17",
            'Monotone_Constraints':
            '[0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0]',
            'Variables':
            'ThreeBody_DIRA_OWNPV,log(ThreeBody_ENDVERTEX_CHI2),log(ThreeBody_FDCHI2_OWNPV),log(ThreeBody_MINIPCHI2),ThreeBody_PT,TwoBody_DIRA_OWNPV,log(TwoBody_ENDVERTEX_CHI2),log(TwoBody_FDCHI2_OWNPV),log(TwoBody_MINIPCHI2),TwoBody_Mcorr,TwoBody_PT,log(Track1_MINIPCHI2),Track1_PT,log(Track2_MINIPCHI2),Track2_PT,log(Track3_MINIPCHI2),Track3_PT',
        },
        Inputs={
            "ThreeBody_DIRA_OWNPV":
            F.BPVDIRA(pvs),
            "log(ThreeBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHI2DOF),
            "log(ThreeBody_FDCHI2_OWNPV)":
            fmath.log(F.BPVFDCHI2(pvs)),
            "log(ThreeBody_MINIPCHI2)":
            fmath.log(F.BPVIPCHI2(pvs)),
            "ThreeBody_PT":
            F.PT / GeV,
            "TwoBody_DIRA_OWNPV":
            F.CHILD(1, F.BPVDIRA(pvs)),
            "log(TwoBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(1, F.CHI2DOF)),
            "log(TwoBody_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
            "log(TwoBody_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.BPVIPCHI2(pvs))),
            "TwoBody_Mcorr":
            F.CHILD(1, F.BPVCORRM(pvs)) / GeV,
            "TwoBody_PT":
            F.CHILD(1, F.PT) / GeV,
            "log(Track1_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track1_PT":
            F.CHILD(1, F.CHILD(1, F.PT)) / GeV,
            "log(Track2_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track2_PT":
            F.CHILD(1, F.CHILD(2, F.PT)) / GeV,
            "log(Track3_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.BPVIPCHI2(pvs))),
            "Track3_PT":
            F.CHILD(2, F.PT) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dielectrons, F.FILTER(code))


@configurable
def filter_mue_plus_track_MVA(pvs=None, dileptons=None, mvacut=0.7):

    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetElectronMuon_3B_weights_202304.json",
            'Name':
            "SigmaNet_mue_plus_track",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "17",
            'Monotone_Constraints':
            '[0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0]',
            'Variables':
            'ThreeBody_DIRA_OWNPV,log(ThreeBody_ENDVERTEX_CHI2),log(ThreeBody_FDCHI2_OWNPV),log(ThreeBody_MINIPCHI2),ThreeBody_PT,TwoBody_DIRA_OWNPV,log(TwoBody_ENDVERTEX_CHI2),log(TwoBody_FDCHI2_OWNPV),log(TwoBody_MINIPCHI2),TwoBody_Mcorr,TwoBody_PT,log(Track1_MINIPCHI2),Track1_PT,log(Track2_MINIPCHI2),Track2_PT,log(Track3_MINIPCHI2),Track3_PT',
        },
        Inputs={
            "ThreeBody_DIRA_OWNPV":
            F.BPVDIRA(pvs),
            "log(ThreeBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHI2DOF),
            "log(ThreeBody_FDCHI2_OWNPV)":
            fmath.log(F.BPVFDCHI2(pvs)),
            "log(ThreeBody_MINIPCHI2)":
            fmath.log(F.BPVIPCHI2(pvs)),
            "ThreeBody_PT":
            F.PT / GeV,
            "TwoBody_DIRA_OWNPV":
            F.CHILD(1, F.BPVDIRA(pvs)),
            "log(TwoBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(1, F.CHI2DOF)),
            "log(TwoBody_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
            "log(TwoBody_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.BPVIPCHI2(pvs))),
            "TwoBody_Mcorr":
            F.CHILD(1, F.BPVCORRM(pvs)) / GeV,
            "TwoBody_PT":
            F.CHILD(1, F.PT) / GeV,
            "log(Track1_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track1_PT":
            F.CHILD(1, F.CHILD(1, F.PT)) / GeV,
            "log(Track2_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track2_PT":
            F.CHILD(1, F.CHILD(2, F.PT)) / GeV,
            "log(Track3_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.BPVIPCHI2(pvs))),
            "Track3_PT":
            F.CHILD(2, F.PT) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dileptons, F.FILTER(code))


@configurable
def filter_dimuon_plus_2track_MVA(pvs=None, dimuons=None, mvacut=0.7):

    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetDimuon_4B_weights_202304.json",
            'Name':
            "SigmaNet_mumu_plus_2track",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "19",
            'Monotone_Constraints':
            '[0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,0]',
            'Variables':
            'FourBody_DIRA_OWNPV,log(FourBody_ENDVERTEX_CHI2),log(FourBody_FDCHI2_OWNPV),log(FourBody_MINIPCHI2),FourBody_PT,TwoBody_DIRA_OWNPV,log(TwoBody_ENDVERTEX_CHI2),log(TwoBody_FDCHI2_OWNPV),log(TwoBody_MINIPCHI2),TwoBody_Mcorr,TwoBody_PT,log(Track1_MINIPCHI2),Track1_PT,log(Track2_MINIPCHI2),Track2_PT,log(Track3_MINIPCHI2),Track3_PT,log(Track4_MINIPCHI2),Track4_PT',
        },
        Inputs={
            "FourBody_DIRA_OWNPV":
            F.BPVDIRA(pvs),
            "log(FourBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHI2DOF),
            "log(FourBody_FDCHI2_OWNPV)":
            fmath.log(F.BPVFDCHI2(pvs)),
            "log(FourBody_MINIPCHI2)":
            fmath.log(F.BPVIPCHI2(pvs)),
            "FourBody_PT":
            F.PT / GeV,
            "TwoBody_DIRA_OWNPV":
            F.CHILD(1, F.BPVDIRA(pvs)),
            "log(TwoBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(1, F.CHI2DOF)),
            "log(TwoBody_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
            "log(TwoBody_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.BPVIPCHI2(pvs))),
            "TwoBody_Mcorr":
            F.CHILD(1, F.BPVCORRM(pvs)) / GeV,
            "TwoBody_PT":
            F.CHILD(1, F.PT) / GeV,
            "log(Track1_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track1_PT":
            F.CHILD(1, F.CHILD(1, F.PT)) / GeV,
            "log(Track2_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track2_PT":
            F.CHILD(1, F.CHILD(2, F.PT)) / GeV,
            "log(Track3_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.BPVIPCHI2(pvs))),
            "Track3_PT":
            F.CHILD(2, F.PT) / GeV,
            "log(Track4_MINIPCHI2)":
            fmath.log(F.CHILD(3, F.BPVIPCHI2(pvs))),
            "Track4_PT":
            F.CHILD(3, F.PT) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dimuons, F.FILTER(code))


@configurable
def filter_dielectron_plus_2track_MVA(pvs=None, dielectrons=None, mvacut=0.7):
    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetDielectron_4B_weights_202304.json",
            'Name':
            "SigmaNet_ee_plus_2track",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "19",
            'Monotone_Constraints':
            '[0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,0]',
            'Variables':
            'FourBody_DIRA_OWNPV,log(FourBody_ENDVERTEX_CHI2),log(FourBody_FDCHI2_OWNPV),log(FourBody_MINIPCHI2),FourBody_PT,TwoBody_DIRA_OWNPV,log(TwoBody_ENDVERTEX_CHI2),log(TwoBody_FDCHI2_OWNPV),log(TwoBody_MINIPCHI2),TwoBody_Mcorr,TwoBody_PT,log(Track1_MINIPCHI2),Track1_PT,log(Track2_MINIPCHI2),Track2_PT,log(Track3_MINIPCHI2),Track3_PT,log(Track4_MINIPCHI2),Track4_PT',
        },
        Inputs={
            "FourBody_DIRA_OWNPV":
            F.BPVDIRA(pvs),
            "log(FourBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHI2DOF),
            "log(FourBody_FDCHI2_OWNPV)":
            fmath.log(F.BPVFDCHI2(pvs)),
            "log(FourBody_MINIPCHI2)":
            fmath.log(F.BPVIPCHI2(pvs)),
            "FourBody_PT":
            F.PT / GeV,
            "TwoBody_DIRA_OWNPV":
            F.CHILD(1, F.BPVDIRA(pvs)),
            "log(TwoBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(1, F.CHI2DOF)),
            "log(TwoBody_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
            "log(TwoBody_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.BPVIPCHI2(pvs))),
            "TwoBody_Mcorr":
            F.CHILD(1, F.BPVCORRM(pvs)) / GeV,
            "TwoBody_PT":
            F.CHILD(1, F.PT) / GeV,
            "log(Track1_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track1_PT":
            F.CHILD(1, F.CHILD(1, F.PT)) / GeV,
            "log(Track2_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track2_PT":
            F.CHILD(1, F.CHILD(2, F.PT)) / GeV,
            "log(Track3_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.BPVIPCHI2(pvs))),
            "Track3_PT":
            F.CHILD(2, F.PT) / GeV,
            "log(Track4_MINIPCHI2)":
            fmath.log(F.CHILD(3, F.BPVIPCHI2(pvs))),
            "Track4_PT":
            F.CHILD(3, F.PT) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dielectrons, F.FILTER(code))


@configurable
def filter_mue_plus_2track_MVA(pvs=None, dileptons=None, mvacut=0.7):

    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetElectronMuon_4B_weights_202304.json",
            'Name':
            "SigmaNet_mue_plus_2track",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "19",
            'Monotone_Constraints':
            '[0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,0]',
            'Variables':
            'FourBody_DIRA_OWNPV,log(FourBody_ENDVERTEX_CHI2),log(FourBody_FDCHI2_OWNPV),log(FourBody_MINIPCHI2),FourBody_PT,TwoBody_DIRA_OWNPV,log(TwoBody_ENDVERTEX_CHI2),log(TwoBody_FDCHI2_OWNPV),log(TwoBody_MINIPCHI2),TwoBody_Mcorr,TwoBody_PT,log(Track1_MINIPCHI2),Track1_PT,log(Track2_MINIPCHI2),Track2_PT,log(Track3_MINIPCHI2),Track3_PT,log(Track4_MINIPCHI2),Track4_PT',
        },
        Inputs={
            "FourBody_DIRA_OWNPV":
            F.BPVDIRA(pvs),
            "log(FourBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHI2DOF),
            "log(FourBody_FDCHI2_OWNPV)":
            fmath.log(F.BPVFDCHI2(pvs)),
            "log(FourBody_MINIPCHI2)":
            fmath.log(F.BPVIPCHI2(pvs)),
            "FourBody_PT":
            F.PT / GeV,
            "TwoBody_DIRA_OWNPV":
            F.CHILD(1, F.BPVDIRA(pvs)),
            "log(TwoBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(1, F.CHI2DOF)),
            "log(TwoBody_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
            "log(TwoBody_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.BPVIPCHI2(pvs))),
            "TwoBody_Mcorr":
            F.CHILD(1, F.BPVCORRM(pvs)) / GeV,
            "TwoBody_PT":
            F.CHILD(1, F.PT) / GeV,
            "log(Track1_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track1_PT":
            F.CHILD(1, F.CHILD(1, F.PT)) / GeV,
            "log(Track2_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track2_PT":
            F.CHILD(1, F.CHILD(2, F.PT)) / GeV,
            "log(Track3_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.BPVIPCHI2(pvs))),
            "Track3_PT":
            F.CHILD(2, F.PT) / GeV,
            "log(Track4_MINIPCHI2)":
            fmath.log(F.CHILD(3, F.BPVIPCHI2(pvs))),
            "Track4_PT":
            F.CHILD(3, F.PT) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dileptons, F.FILTER(code))


@configurable
def filter_dimuon_plus_neutral_MVA(pvs=None, dimuons=None, mvacut=0.7):

    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetDimuon_neutral_weights_202304.json",
            'Name':
            "SigmaNet_mumu_plus_neutral",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "23",
            'Monotone_Constraints':
            '[0,0,1,0,1,0,0,0,0,0,0,0,0,1,1,0,1,0,1,0,0,0,0]',
            'Variables':
            'FourBody_DIRA_OWNPV,log(FourBody_ENDVERTEX_CHI2),log(FourBody_FDCHI2_OWNPV),log(FourBody_MINIPCHI2),FourBody_PT,FourBody_CHILD_DIRA_OWNPV,log(TwoBody_ENDVERTEX_CHI2),log(TwoBody_pion_ENDVERTEX_CHI2),log(FourBody_CHILD_FDCHI2_OWNPV),log(FourBody_CHILD_MINIPCHI2),FourBody_CHILD_Mcorr,log(FourBody_CHILD2_FDCHI2_OWNPV),log(FourBody_CHILD2_MINIPCHI2),TwoBody_PT,TwoBody_pion_PT,log(Track1_MINIPCHI2),Track1_PT,log(Track2_MINIPCHI2),Track2_PT,log(Track3_MINIPCHI2),Track3_PT,log(Track4_MINIPCHI2),Track4_PT',
        },
        Inputs={
            "FourBody_DIRA_OWNPV":
            F.BPVDIRA(pvs),
            "log(FourBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHI2DOF),
            "log(FourBody_FDCHI2_OWNPV)":
            fmath.log(F.BPVFDCHI2(pvs)),
            "log(FourBody_MINIPCHI2)":
            fmath.log(F.BPVIPCHI2(pvs)),
            "FourBody_PT":
            F.PT / GeV,
            "FourBody_CHILD_DIRA_OWNPV":
            F.CHILD(1, F.BPVDIRA(pvs)),
            "log(TwoBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(1, F.CHI2DOF)),
            "log(TwoBody_pion_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(2, F.CHI2DOF)),
            "log(FourBody_CHILD_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
            "log(FourBody_CHILD_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.BPVIPCHI2(pvs))),
            "FourBody_CHILD_Mcorr":
            F.CHILD(1, F.BPVCORRM(pvs)) / GeV,
            "log(FourBody_CHILD2_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(2, F.BPVFDCHI2(pvs))),
            "log(FourBody_CHILD2_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.BPVIPCHI2(pvs))),
            "TwoBody_PT":
            F.CHILD(1, F.PT) / GeV,
            "TwoBody_pion_PT":
            F.CHILD(2, F.PT) / GeV,
            "log(Track1_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track1_PT":
            F.CHILD(1, F.CHILD(1, F.PT)) / GeV,
            "log(Track2_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track2_PT":
            F.CHILD(1, F.CHILD(2, F.PT)) / GeV,
            "log(Track3_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track3_PT":
            F.CHILD(2, F.CHILD(1, F.PT)) / GeV,
            "log(Track4_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track4_PT":
            F.CHILD(2, F.CHILD(2, F.PT)) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dimuons, F.FILTER(code))


@configurable
def filter_dielectron_plus_neutral_MVA(pvs=None, dielectrons=None, mvacut=0.7):
    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetDielectron_neutral_weights_202304.json",
            'Name':
            "SigmaNet_ee_plus_neutral",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "23",
            'Monotone_Constraints':
            '[0,0,1,0,1,0,0,0,0,0,0,0,0,1,1,0,1,0,1,0,0,0,0]',
            'Variables':
            'FourBody_DIRA_OWNPV,log(FourBody_ENDVERTEX_CHI2),log(FourBody_FDCHI2_OWNPV),log(FourBody_MINIPCHI2),FourBody_PT,FourBody_CHILD_DIRA_OWNPV,log(TwoBody_ENDVERTEX_CHI2),log(TwoBody_pion_ENDVERTEX_CHI2),log(FourBody_CHILD_FDCHI2_OWNPV),log(FourBody_CHILD_MINIPCHI2),FourBody_CHILD_Mcorr,log(FourBody_CHILD2_FDCHI2_OWNPV),log(FourBody_CHILD2_MINIPCHI2),TwoBody_PT,TwoBody_pion_PT,log(Track1_MINIPCHI2),Track1_PT,log(Track2_MINIPCHI2),Track2_PT,log(Track3_MINIPCHI2),Track3_PT,log(Track4_MINIPCHI2),Track4_PT',
        },
        Inputs={
            "FourBody_DIRA_OWNPV":
            F.BPVDIRA(pvs),
            "log(FourBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHI2DOF),
            "log(FourBody_FDCHI2_OWNPV)":
            fmath.log(F.BPVFDCHI2(pvs)),
            "log(FourBody_MINIPCHI2)":
            fmath.log(F.BPVIPCHI2(pvs)),
            "FourBody_PT":
            F.PT / GeV,
            "FourBody_CHILD_DIRA_OWNPV":
            F.CHILD(1, F.BPVDIRA(pvs)),
            "log(TwoBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(1, F.CHI2DOF)),
            "log(TwoBody_pion_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(2, F.CHI2DOF)),
            "log(FourBody_CHILD_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
            "log(FourBody_CHILD_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.BPVIPCHI2(pvs))),
            "FourBody_CHILD_Mcorr":
            F.CHILD(1, F.BPVCORRM(pvs)) / GeV,
            "log(FourBody_CHILD2_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(2, F.BPVFDCHI2(pvs))),
            "log(FourBody_CHILD2_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.BPVIPCHI2(pvs))),
            "TwoBody_PT":
            F.CHILD(1, F.PT) / GeV,
            "TwoBody_pion_PT":
            F.CHILD(2, F.PT) / GeV,
            "log(Track1_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track1_PT":
            F.CHILD(1, F.CHILD(1, F.PT)) / GeV,
            "log(Track2_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track2_PT":
            F.CHILD(1, F.CHILD(2, F.PT)) / GeV,
            "log(Track3_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track3_PT":
            F.CHILD(2, F.CHILD(1, F.PT)) / GeV,
            "log(Track4_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track4_PT":
            F.CHILD(2, F.CHILD(2, F.PT)) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dielectrons, F.FILTER(code))


@configurable
def filter_mue_plus_neutral_MVA(pvs=None, dileptons=None, mvacut=0.7):

    mva = (F.MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "paramfile://data/Upgrade_InclDetElectronMuon_neutral_weights_202304.json",
            'Name':
            "SigmaNet_mue_plus_neutral",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "23",
            'Monotone_Constraints':
            '[0,0,1,0,1,0,0,0,0,0,0,0,0,1,1,0,1,0,1,0,0,0,0]',
            'Variables':
            'FourBody_DIRA_OWNPV,log(FourBody_ENDVERTEX_CHI2),log(FourBody_FDCHI2_OWNPV),log(FourBody_MINIPCHI2),FourBody_PT,FourBody_CHILD_DIRA_OWNPV,log(TwoBody_ENDVERTEX_CHI2),log(TwoBody_pion_ENDVERTEX_CHI2),log(FourBody_CHILD_FDCHI2_OWNPV),log(FourBody_CHILD_MINIPCHI2),FourBody_CHILD_Mcorr,log(FourBody_CHILD2_FDCHI2_OWNPV),log(FourBody_CHILD2_MINIPCHI2),TwoBody_PT,TwoBody_pion_PT,log(Track1_MINIPCHI2),Track1_PT,log(Track2_MINIPCHI2),Track2_PT,log(Track3_MINIPCHI2),Track3_PT,log(Track4_MINIPCHI2),Track4_PT',
        },
        Inputs={
            "FourBody_DIRA_OWNPV":
            F.BPVDIRA(pvs),
            "log(FourBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHI2DOF),
            "log(FourBody_FDCHI2_OWNPV)":
            fmath.log(F.BPVFDCHI2(pvs)),
            "log(FourBody_MINIPCHI2)":
            fmath.log(F.BPVIPCHI2(pvs)),
            "FourBody_PT":
            F.PT / GeV,
            "FourBody_CHILD_DIRA_OWNPV":
            F.CHILD(1, F.BPVDIRA(pvs)),
            "log(TwoBody_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(1, F.CHI2DOF)),
            "log(TwoBody_pion_ENDVERTEX_CHI2)":
            fmath.log(F.CHILD(2, F.CHI2DOF)),
            "log(FourBody_CHILD_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(1, F.BPVFDCHI2(pvs))),
            "log(FourBody_CHILD_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.BPVIPCHI2(pvs))),
            "FourBody_CHILD_Mcorr":
            F.CHILD(1, F.BPVCORRM(pvs)) / GeV,
            "log(FourBody_CHILD2_FDCHI2_OWNPV)":
            fmath.log(F.CHILD(2, F.BPVFDCHI2(pvs))),
            "log(FourBody_CHILD2_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.BPVIPCHI2(pvs))),
            "TwoBody_PT":
            F.CHILD(1, F.PT) / GeV,
            "TwoBody_pion_PT":
            F.CHILD(2, F.PT) / GeV,
            "log(Track1_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track1_PT":
            F.CHILD(1, F.CHILD(1, F.PT)) / GeV,
            "log(Track2_MINIPCHI2)":
            fmath.log(F.CHILD(1, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track2_PT":
            F.CHILD(1, F.CHILD(2, F.PT)) / GeV,
            "log(Track3_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.CHILD(1, F.BPVIPCHI2(pvs)))),
            "Track3_PT":
            F.CHILD(2, F.CHILD(1, F.PT)) / GeV,
            "log(Track4_MINIPCHI2)":
            fmath.log(F.CHILD(2, F.CHILD(2, F.BPVIPCHI2(pvs)))),
            "Track4_PT":
            F.CHILD(2, F.CHILD(2, F.PT)) / GeV,
        }) > mvacut)

    code = F.require_all(mva)

    return ParticleFilter(dileptons, F.FILTER(code))
