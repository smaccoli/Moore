###############################################################################
# (c) Copyright 20.0 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of inclusive detached dilepton HLT2 lines.
"""
from Moore.config import HltLine, register_line_builder
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dielectron, make_rd_detached_mue
from Hlt2Conf.lines.inclusive_detached_dilepton.dilepton_mva_builder import make_inclusive_dilepton_detached_dimuon
from Hlt2Conf.lines.inclusive_detached_dilepton.dilepton_mva_builder import make_rd_detached_dimuon_plus_track, make_rd_detached_dielectron_plus_track, make_rd_detached_mue_plus_track
from Hlt2Conf.lines.inclusive_detached_dilepton.dilepton_mva_builder import make_rd_detached_dimuon_plus_2track, make_rd_detached_dielectron_plus_2track, make_rd_detached_mue_plus_2track
from Hlt2Conf.lines.inclusive_detached_dilepton.dilepton_mva_builder import make_rd_detached_dimuon_plus_neutral, make_rd_detached_dielectron_plus_neutral, make_rd_detached_mue_plus_neutral
from Hlt2Conf.lines.inclusive_detached_dilepton.dilepton_mva_builder import filter_dimuon_MVA, filter_dielectron_MVA, filter_mue_MVA
from Hlt2Conf.lines.inclusive_detached_dilepton.dilepton_mva_builder import filter_dimuon_plus_track_MVA, filter_dielectron_plus_track_MVA, filter_mue_plus_track_MVA
from Hlt2Conf.lines.inclusive_detached_dilepton.dilepton_mva_builder import filter_dimuon_plus_2track_MVA, filter_dielectron_plus_2track_MVA, filter_mue_plus_2track_MVA
from Hlt2Conf.lines.inclusive_detached_dilepton.dilepton_mva_builder import filter_dimuon_plus_neutral_MVA, filter_dielectron_plus_neutral_MVA, filter_mue_plus_neutral_MVA

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter

from GaudiKernel.SystemOfUnits import GeV, MeV, ps

all_lines = {}


@register_line_builder(all_lines)
def incldetdimuon_line(name="Hlt2_InclDetDiMuon", prescale=1):

    pvs = make_pvs()

    dimuons = make_inclusive_dilepton_detached_dimuon(
        name="inclusive_dilepton_detached_dimuon_{hash}",
        parent_id='J/psi(1S)',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=100. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=3.,
        pidmu_muon_min=-3.0,
        IsMuon=True,
        adocachi2cut_max=30.,
        bpvvdchi2_min=10.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        same_sign=False)

    dimuon_filter = filter_dimuon_MVA(pvs=pvs, dimuons=dimuons, mvacut=0.96)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, dimuon_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdielectron_line(name="Hlt2_InclDetDiElectron", prescale=1):

    pvs = make_pvs()

    dielectrons = make_rd_detached_dielectron(
        pid_e_min=1.0,
        pt_e_min=0.1 * GeV,
        ipchi2_e_min=3.,
        parent_id="J/psi(1S)",
        opposite_sign=True,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=10.,
        vfaspfchi2ndof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV)

    dielectron_filter = filter_dielectron_MVA(
        pvs=pvs, dielectrons=dielectrons, mvacut=0.95)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, dielectron_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdiemu_line(name="Hlt2_InclDetDiMuE", prescale=1):

    pvs = make_pvs()

    dileptons = make_rd_detached_mue(
        max_dilepton_mass=6500. * MeV,
        parent_id='J/psi(1S)',
        min_probnn_mu=None,
        min_PIDe=-3.0,
        min_PIDmu=-3.0,
        IsMuon=True,
        min_pt_e=0.1 * GeV,
        min_pt_mu=0.0 * GeV,
        min_bpvvdchi2=10.,
        max_vchi2ndof=30.,
        same_sign=False)

    dielectronmuons = filter_mue_MVA(pvs=pvs, dileptons=dileptons, mvacut=0.96)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, dielectronmuons],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdimuon_line_SS(name="Hlt2_InclDetDiMuon_SS", prescale=1):

    pvs = make_pvs()

    dimuons = make_inclusive_dilepton_detached_dimuon(
        name="inclusive_dilepton_detached_dimuon_SS_{hash}",
        parent_id='J/psi(1S)',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=100. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=3.,
        pidmu_muon_min=-3.0,
        IsMuon=True,
        adocachi2cut_max=30.,
        bpvvdchi2_min=10.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        same_sign=True)

    dimuon_filter = filter_dimuon_MVA(pvs=pvs, dimuons=dimuons, mvacut=0.96)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, dimuon_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdielectron_line_SS(name="Hlt2_InclDetDiElectron_SS", prescale=1):

    pvs = make_pvs()

    dielectrons = make_rd_detached_dielectron(
        pid_e_min=1.0,
        pt_e_min=0.1 * GeV,
        ipchi2_e_min=3.,
        parent_id="J/psi(1S)",
        opposite_sign=False,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=10.,
        vfaspfchi2ndof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV)

    dielectron_filter = filter_dielectron_MVA(
        pvs=pvs, dielectrons=dielectrons, mvacut=0.95)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, dielectron_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdiemu_line_SS(name="Hlt2_InclDetDiMuE_SS", prescale=1):

    pvs = make_pvs()

    dileptons = make_rd_detached_mue(
        max_dilepton_mass=6500. * MeV,
        parent_id='J/psi(1S)',
        min_probnn_mu=None,
        min_PIDe=-3.0,
        min_PIDmu=-3.0,
        IsMuon=True,
        min_pt_e=0.1 * GeV,
        min_pt_mu=0.0 * GeV,
        min_bpvvdchi2=10.,
        max_vchi2ndof=30.,
        same_sign=True)

    dielectronmuons = filter_mue_MVA(pvs=pvs, dileptons=dileptons, mvacut=0.96)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, dielectronmuons],
        prescale=prescale,
        persistreco=True)


################## Dilepton plus track


@register_line_builder(all_lines)
def incldetdimuon_plus_track_line(name="Hlt2_InclDetDiMuon_3Body", prescale=1):

    pvs = make_pvs()

    dimuons = make_rd_detached_dimuon_plus_track(
        name="rd_detached_dimuon_plus_track_{hash}",
        parent_id='B+',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=100. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=6.,
        pidmu_muon_min=-3.0,
        IsMuon=True,
        adocachi2cut_max=30.,
        bpvvdchi2_min=0.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        same_sign=False,
        pt_b_min=0 * GeV,
        m_b_min=0.00 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dimuon_filter = filter_dimuon_plus_track_MVA(
        pvs=pvs, dimuons=dimuons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, dimuon_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdielectron_plus_track_line(name="Hlt2_InclDetDiElectron_3Body",
                                      prescale=1):

    pvs = make_pvs()

    dielectrons = make_rd_detached_dielectron_plus_track(
        pid_e_min=1.0,
        pt_e_min=0.1 * GeV,
        ipchi2_e_min=6.,
        parent_id='B+',
        opposite_sign=True,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=0.,
        vfaspfchi2ndof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=0.000 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectron_filter = filter_dielectron_plus_track_MVA(
        pvs=pvs, dielectrons=dielectrons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, dielectron_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdiemu_plus_track_line(name="Hlt2_InclDetDiMuE_3Body", prescale=1):

    pvs = make_pvs()

    dileptons = make_rd_detached_mue_plus_track(
        max_dilepton_mass=6500. * MeV,
        parent_id='B+',
        min_probnn_mu=None,
        min_PIDe=-1.0,
        min_PIDmu=-3.0,
        IsMuon=True,
        min_pt_e=0.1 * GeV,
        min_pt_mu=0.1 * GeV,
        min_bpvvdchi2=0.,
        max_vchi2ndof=30.,
        same_sign=False,
        pt_b_min=0 * GeV,
        m_b_min=0.0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectronmuons = filter_mue_plus_track_MVA(
        pvs=pvs, dileptons=dileptons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, dielectronmuons],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdimuon_plus_track_line_SS(name="Hlt2_InclDetDiMuon_3Body_SS",
                                     prescale=1):

    pvs = make_pvs()

    dimuons = make_rd_detached_dimuon_plus_track(
        name="rd_detached_dimuon_plus_track_SS_{hash}",
        parent_id='B+',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=100. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=6.,
        pidmu_muon_min=-3.0,
        IsMuon=True,
        adocachi2cut_max=30.,
        bpvvdchi2_min=0.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        same_sign=True,
        pt_b_min=0 * GeV,
        m_b_min=0.00 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dimuon_filter = filter_dimuon_plus_track_MVA(
        pvs=pvs, dimuons=dimuons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, dimuon_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdielectron_plus_track_line_SS(
        name="Hlt2_InclDetDiElectron_3Body_SS", prescale=1):

    pvs = make_pvs()

    dielectrons = make_rd_detached_dielectron_plus_track(
        pid_e_min=1.0,
        pt_e_min=0.1 * GeV,
        ipchi2_e_min=6.,
        parent_id='B+',
        opposite_sign=False,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=0.,
        vfaspfchi2ndof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=0.000 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectron_filter = filter_dielectron_plus_track_MVA(
        pvs=pvs, dielectrons=dielectrons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, dielectron_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdiemu_line_plus_track_SS(name="Hlt2_InclDetDiMuE_3Body_SS",
                                    prescale=1):

    pvs = make_pvs()

    dileptons = make_rd_detached_mue_plus_track(
        max_dilepton_mass=6500. * MeV,
        parent_id='B+',
        min_probnn_mu=None,
        min_PIDe=-1.0,
        min_PIDmu=-3.0,
        IsMuon=True,
        min_pt_e=0.1 * GeV,
        min_pt_mu=0.1 * GeV,
        min_bpvvdchi2=0.,
        max_vchi2ndof=30.,
        same_sign=True,
        pt_b_min=0 * GeV,
        m_b_min=0.0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectronmuons = filter_mue_plus_track_MVA(
        pvs=pvs, dileptons=dileptons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, dielectronmuons],
        prescale=prescale,
        persistreco=True)


################## Dilepton plus 2 track


@register_line_builder(all_lines)
def incldetdimuon_plus_2track_line(name="Hlt2_InclDetDiMuon_4Body",
                                   prescale=1):

    pvs = make_pvs()

    dimuons = make_rd_detached_dimuon_plus_2track(
        name="rd_detached_dimuon_plus_2track_{hash}",
        parent_id='B0',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=100. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=6.,
        pidmu_muon_min=-3.0,
        IsMuon=True,
        adocachi2cut_max=30.,
        bpvvdchi2_min=0.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        same_sign_leptons=False,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dimuon_filter = filter_dimuon_plus_2track_MVA(
        pvs=pvs, dimuons=dimuons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, dimuon_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdielectron_plus_2track_line(name="Hlt2_InclDetDiElectron_4Body",
                                       prescale=1):

    pvs = make_pvs()

    dielectrons = make_rd_detached_dielectron_plus_2track(
        pid_e_min=1.0,
        pt_e_min=0.1 * GeV,
        ipchi2_e_min=6.,
        parent_id='B0',
        opposite_sign_leptons=True,
        same_sign_pions=False,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=0.,
        vfaspfchi2ndof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=0.0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectron_filter = filter_dielectron_plus_2track_MVA(
        pvs=pvs, dielectrons=dielectrons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, dielectron_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdiemu_plus_2track_line(name="Hlt2_InclDetDiMuE_4Body", prescale=1):

    pvs = make_pvs()

    dileptons = make_rd_detached_mue_plus_2track(
        max_dilepton_mass=6500. * MeV,
        parent_id='B0',
        min_probnn_mu=None,
        min_PIDe=-1.0,
        min_PIDmu=-3.0,
        IsMuon=True,
        min_pt_e=0.1 * GeV,
        min_pt_mu=0.1 * GeV,
        min_bpvvdchi2=0.0,
        max_vchi2ndof=30.,
        same_sign_leptons=False,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=0.0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectronmuons = filter_mue_plus_2track_MVA(
        pvs=pvs, dileptons=dileptons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, dielectronmuons],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdimuon_plus_2track_line_SS(name="Hlt2_InclDetDiMuon_4Body_SS",
                                      prescale=1):

    pvs = make_pvs()

    dimuons = make_rd_detached_dimuon_plus_2track(
        name="rd_detached_dimuon_plus_2track_SS_{hash}",
        parent_id='B0',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=100. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=6.,
        pidmu_muon_min=-3.0,
        IsMuon=True,
        adocachi2cut_max=30.,
        bpvvdchi2_min=0.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        same_sign_leptons=True,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dimuon_filter = filter_dimuon_plus_2track_MVA(
        pvs=pvs, dimuons=dimuons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, dimuon_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdielectron_plus_2track_line_SS(
        name="Hlt2_InclDetDiElectron_4Body_SS", prescale=1):

    pvs = make_pvs()

    dielectrons = make_rd_detached_dielectron_plus_2track(
        pid_e_min=1.0,
        pt_e_min=0.1 * GeV,
        ipchi2_e_min=6.0,
        parent_id='B0',
        opposite_sign_leptons=False,
        same_sign_pions=False,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=0.,
        vfaspfchi2ndof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=0.0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectron_filter = filter_dielectron_plus_2track_MVA(
        pvs=pvs, dielectrons=dielectrons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, dielectron_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdiemu_line_plus_2track_SS(name="Hlt2_InclDetDiMuE_4Body_SS",
                                     prescale=1):

    pvs = make_pvs()

    dileptons = make_rd_detached_mue_plus_2track(
        max_dilepton_mass=6500. * MeV,
        parent_id='B0',
        min_probnn_mu=None,
        min_PIDe=-1.0,
        min_PIDmu=-3.0,
        IsMuon=True,
        min_pt_e=0.1 * GeV,
        min_pt_mu=0.1 * GeV,
        min_bpvvdchi2=0.0,
        max_vchi2ndof=30.,
        same_sign_leptons=True,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=0.0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectronmuons = filter_mue_plus_2track_MVA(
        pvs=pvs, dileptons=dileptons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, dielectronmuons],
        prescale=prescale,
        persistreco=True)


################## Dilepton plus 2 track same sign pions


@register_line_builder(all_lines)
def incldetdimuon_plus_2track_PionSS_line(
        name="Hlt2_InclDetDiMuon_4Body_PionSS", prescale=1):

    pvs = make_pvs()

    dimuons = make_rd_detached_dimuon_plus_2track(
        name="rd_detached_dimuon_plus_2track_PionSS_{hash}",
        parent_id='B+',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=100. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=6.,
        pidmu_muon_min=-3.0,
        IsMuon=True,
        adocachi2cut_max=30.,
        bpvvdchi2_min=0.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        same_sign_leptons=True,
        same_sign_pions=True,
        pt_b_min=0 * GeV,
        m_b_min=0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dimuon_filter = filter_dimuon_plus_2track_MVA(
        pvs=pvs, dimuons=dimuons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, dimuon_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdielectron_plus_2track_PionSS_line(
        name="Hlt2_InclDetDiElectron_4Body_PionSS", prescale=1):

    pvs = make_pvs()

    dielectrons = make_rd_detached_dielectron_plus_2track(
        pid_e_min=1.0,
        pt_e_min=0.1 * GeV,
        ipchi2_e_min=6.,
        parent_id='B0',
        opposite_sign_leptons=False,
        same_sign_pions=True,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=0.,
        vfaspfchi2ndof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=0.0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectron_filter = filter_dielectron_plus_2track_MVA(
        pvs=pvs, dielectrons=dielectrons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, dielectron_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdiemu_plus_2track_PionSS_line(name="Hlt2_InclDetDiMuE_4Body_PionSS",
                                         prescale=1):

    pvs = make_pvs()

    dileptons = make_rd_detached_mue_plus_2track(
        max_dilepton_mass=6500. * MeV,
        parent_id='B0',
        min_probnn_mu=None,
        min_PIDe=-1.0,
        min_PIDmu=-3.0,
        IsMuon=True,
        min_pt_e=0.1 * GeV,
        min_pt_mu=0.1 * GeV,
        min_bpvvdchi2=0.0,
        max_vchi2ndof=30.,
        same_sign_leptons=True,
        same_sign_pions=True,
        pt_b_min=0 * GeV,
        m_b_min=0.0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.)

    dielectronmuons = filter_mue_plus_2track_MVA(
        pvs=pvs, dileptons=dileptons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, dielectronmuons],
        prescale=prescale,
        persistreco=True)


################## Dilepton plus Neutral


@register_line_builder(all_lines)
def incldetdimuon_plus_neutral_line(name="Hlt2_InclDetDiMuon_neutral",
                                    prescale=1):

    pvs = make_pvs()
    dimuons = make_rd_detached_dimuon_plus_neutral(
        name="rd_detached_dimuon_{hash}",
        parent_id='B0',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=100. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=6.,
        pidmu_muon_min=-3.0,
        IsMuon=True,
        adocachi2cut_max=30.,
        bpvvdchi2_min=0.,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        same_sign_leptons=False,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.,
        neutral_bpvvdchi2_min=2.0,
        neutral_bpvltime_min=0.1 * ps,
        neutral_vchi2pdof_max=100)

    dimuon_filter = filter_dimuon_plus_neutral_MVA(
        pvs=pvs, dimuons=dimuons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, dimuon_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdielectron_plus_neutral_line(name="Hlt2_InclDetDiElectron_neutral",
                                        prescale=1):

    pvs = make_pvs()

    dielectrons = make_rd_detached_dielectron_plus_neutral(
        pid_e_min=1.0,
        pt_e_min=0.1 * GeV,
        ipchi2_e_min=6.0,
        parent_id='B0',
        opposite_sign_leptons=True,
        same_sign_pions=False,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=0.,
        vfaspfchi2ndof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.,
        neutral_bpvvdchi2_min=2.0,
        neutral_bpvltime_min=0.1 * ps,
        neutral_vchi2pdof_max=100.)

    dielectron_filter = filter_dielectron_plus_neutral_MVA(
        pvs=pvs, dielectrons=dielectrons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, dielectron_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdiemu_plus_neutral_line(name="Hlt2_InclDetDiMuE_neutral",
                                   prescale=1):

    pvs = make_pvs()

    dileptons = make_rd_detached_mue_plus_neutral(
        max_dilepton_mass=6500. * MeV,
        parent_id='B0',
        min_probnn_mu=None,
        min_PIDe=-1.0,
        min_PIDmu=-3.0,
        IsMuon=True,
        min_pt_e=0.1 * GeV,
        min_pt_mu=0.1 * GeV,
        min_bpvvdchi2=0.,
        max_vchi2ndof=30.,
        same_sign_leptons=False,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.,
        neutral_bpvvdchi2_min=2.0,
        neutral_bpvltime_min=0.1 * ps,
        neutral_vchi2pdof_max=100.)

    dielectronmuons = filter_mue_plus_neutral_MVA(
        pvs=pvs, dileptons=dileptons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, dielectronmuons],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdimuon_plus_neutral_line_SS(name="Hlt2_InclDetDiMuon_neutral_SS",
                                       prescale=1):

    pvs = make_pvs()

    dimuons = make_rd_detached_dimuon_plus_neutral(
        name="rd_detached_dimuon_{hash}",
        parent_id='B0',
        pt_dimuon_min=0. * MeV,
        pt_muon_min=100. * MeV,
        p_muon_min=0. * MeV,
        ipchi2_muon_min=6.,
        pidmu_muon_min=-3.0,
        IsMuon=True,
        adocachi2cut_max=30.,
        bpvvdchi2_min=0.0,
        vchi2pdof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        same_sign_leptons=True,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.,
        neutral_bpvvdchi2_min=2.0,
        neutral_bpvltime_min=0.1 * ps,
        neutral_vchi2pdof_max=100.)

    dimuon_filter = filter_dimuon_plus_neutral_MVA(
        pvs=pvs, dimuons=dimuons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, dimuon_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdielectron_plus_neutral_line_SS(
        name="Hlt2_InclDetDiElectron_neutral_SS", prescale=1):

    pvs = make_pvs()

    dielectrons = make_rd_detached_dielectron_plus_neutral(
        pid_e_min=1.0,
        pt_e_min=0.1 * GeV,
        ipchi2_e_min=6.0,
        parent_id='B0',
        opposite_sign_leptons=False,
        same_sign_pions=False,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=0.,
        vfaspfchi2ndof_max=30.,
        am_min=0. * MeV,
        am_max=6500. * MeV,
        pt_b_min=0 * GeV,
        m_b_min=0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.,
        neutral_bpvvdchi2_min=2.0,
        neutral_bpvltime_min=0.1 * ps,
        neutral_vchi2pdof_max=100.)

    dielectron_filter = filter_dielectron_plus_neutral_MVA(
        pvs=pvs, dielectrons=dielectrons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, dielectron_filter],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def incldetdiemu_line_plus_neutral_SS(name="Hlt2_InclDetDiMuE_neutral_SS",
                                      prescale=1):

    pvs = make_pvs()

    dileptons = make_rd_detached_mue_plus_neutral(
        max_dilepton_mass=6500. * MeV,
        parent_id='B0',
        min_probnn_mu=None,
        min_PIDe=-1.0,
        min_PIDmu=-3.0,
        IsMuon=True,
        min_pt_e=0.1 * GeV,
        min_pt_mu=0.1 * GeV,
        min_bpvvdchi2=0.,
        max_vchi2ndof=30.,
        same_sign_leptons=True,
        same_sign_pions=False,
        pt_b_min=0 * GeV,
        m_b_min=0 * MeV,
        m_b_max=10000 * MeV,
        fdchi2_min=10.,
        min_b_vtx_chi2=30.,
        p_pion_min=2. * GeV,
        pt_pion_min=100. * MeV,
        ipchi2_pion_min=6.,
        neutral_bpvvdchi2_min=2.0,
        neutral_bpvltime_min=0.1 * ps,
        neutral_vchi2pdof_max=100.)

    dielectronmuons = filter_mue_plus_neutral_MVA(
        pvs=pvs, dileptons=dileptons, mvacut=0.94)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, dielectronmuons],
        prescale=prescale,
        persistreco=True)
