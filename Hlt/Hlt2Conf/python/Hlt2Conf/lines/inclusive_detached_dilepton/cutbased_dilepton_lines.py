###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the cut-based inclusive dilepton lines

Includes:
    B_s0 -> e+ e- (cc)
    B_s0 -> e+ mu- (cc)
    B_s0 -> mu+ mu- (cc)

    B+ -> J/psi(1S)  (-> e+ e- (cc)) pi+ (cc)
    B+ -> J/psi(1S)  (-> e+ mu- (cc)) pi+ (cc)
    B+ -> J/psi(1S)  (-> mu+ mu- (cc)) pi+ (cc)

    B+ -> J/psi(1S)  (-> e+ e- (cc)) pi+ pi- (cc)
    B+ -> J/psi(1S)  (-> e+ mu- (cc)) pi+ pi- (cc)
    B+ -> J/psi(1S)  (-> mu+ mu- (cc)) pi+ pi- (cc)

    Same-sign variants (e.g. B(s)0 -> e+ e+ (cc))
    Fake-lepton variants (i.e. PID selection cuts reversed)

Author: Jamie Gooding (jamie.gooding@cern.ch)
Date: 03.02.2023
"""

from Moore.config import HltLine, register_line_builder

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES

from Hlt2Conf.lines.inclusive_detached_dilepton.cutbased_dilepton_builders import make_inclusive_cut_dilepton_plus_tracks, make_inclusive_cut_dielectron, make_inclusive_cut_electronmuon, make_inclusive_cut_dimuon

from RecoConf.reconstruction_objects import make_pvs

single_fake_prescale = 0.025  # applied relative to default
double_fake_prescale = 0.0005  # applied relative to default
all_lines = {}

################################################
# 2-body inclusive cut-based dilepton lines    #
################################################

####################################
# 2-body lines (opposite-sign)     #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectron(name="Hlt2CutBasedInclDielectron",
                               prescale=1,
                               persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body dielectron events with opposite-sign electrons, e.g. B_s0 -> e+ e- 
    Line categories:   - 2-body   - dielectron
    """

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='B_s0', two_body=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuon(name="Hlt2CutBasedInclElectronMuon",
                                 prescale=1,
                                 persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with opposite-sign leptons, e.g. B_s0 -> e+ mu- 
    Line categories:   - 2-body   - electron-muon
    """

    electronmuons = make_inclusive_cut_electronmuon(
        parent_id='B_s0', two_body=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuon(name="Hlt2CutBasedInclDimuon",
                           prescale=1,
                           persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with opposite-sign muon, e.g. B_s0 -> mu+ mu- 
    Line categories:   - 2-body   - dimuon
    """

    dimuons = make_inclusive_cut_dimuon(parent_id='B_s0', two_body=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


####################################
# 2-body lines (same-sign)         #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronSS(name="Hlt2CutBasedInclDielectronSS",
                                 prescale=1,
                                 persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body dielectron events with same-sign electrons, e.g. B_s0 -> e+ e+ 
    Line categories:   - 2-body   - dielectron   - same-sign
    """

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='B_s0', two_body=True, same_sign=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonSS(name="Hlt2CutBasedInclElectronMuonSS",
                                   prescale=1,
                                   persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with same-sign leptons, e.g. B_s0 -> e+ mu+
    Line categories:   - 2-body   - electron-muon   - same-sign
    """

    electronmuons = make_inclusive_cut_electronmuon(
        parent_id='B_s0', two_body=True, same_sign=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonSS(name="Hlt2CutBasedInclDimuonSS",
                             prescale=1,
                             persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with same-sign muons, e.g. B_s0 -> mu+ mu+ 
    Line categories:   - 2-body   - dimuon   - same-sign
    """

    dimuons = make_inclusive_cut_dimuon(
        parent_id='B_s0', two_body=True, same_sign=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


####################################
# Fake 2-body lines (opposite-sign)#
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDielectron(
        name="Hlt2CutBasedInclSingleFakeDielectron",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake dielectron events with opposite-sign electrons, e.g. B_s0 -> e+ e- (PID cuts reversed) 
    Line categories:   - 2-body   - dielectron   - single fake line
    """

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='B_s0', two_body=True, fake_electrons=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDielectron(
        name="Hlt2CutBasedInclDoubleFakeDielectron",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake dielectron events with opposite-sign electrons, e.g. B_s0 -> e+ e- (PID cuts reversed) 
    Line categories:   - 2-body   - dielectron   - double fake line
    """

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='B_s0', two_body=True, fake_electrons=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleEFakeElectronMuon(
        name="Hlt2CutBasedInclSingleEFakeElectronMuon",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake electron-muon events with opposite-sign leptons, e.g. B_s0 -> e+ mu- (PID cuts reversed)
    Line categories:   - 2-body   - electron-muon   - single fake electron line
    """

    electronmuons = make_inclusive_cut_electronmuon(
        parent_id='B_s0', two_body=True, fake_electron=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleMuFakeElectronMuon(
        name="Hlt2CutBasedInclSingleMuFakeElectronMuon",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake electron-muon events with opposite-sign leptons, e.g. B_s0 -> e+ mu- (PID cuts reversed)
    Line categories:   - 2-body   - electron-muon   - single fake muon line
    """

    electronmuons = make_inclusive_cut_electronmuon(
        parent_id='B_s0', two_body=True, fake_muon=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeElectronMuon(
        name="Hlt2CutBasedInclDoubleFakeElectronMuon",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake electron-muon events with opposite-sign leptons, e.g. B_s0 -> e+ mu- (PID cuts reversed)
    Line categories:   - 2-body   - electron-muon   - double fake line
    """

    electronmuons = make_inclusive_cut_electronmuon(
        parent_id='B_s0', two_body=True, fake_electron=True, fake_muon=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDimuon(name="Hlt2CutBasedInclSingleFakeDimuon",
                                     prescale=single_fake_prescale,
                                     persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake dimuon events with opposite-sign muon, e.g. B_s0 -> mu+ mu- (PID cuts reversed)
    Line categories:   - 2-body   - dimuon   - single fake line
    """

    dimuons = make_inclusive_cut_dimuon(
        parent_id='B_s0', two_body=True, fake_muons=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDimuon(name="Hlt2CutBasedInclDoubleFakeDimuon",
                                     prescale=double_fake_prescale,
                                     persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake dimuon events with opposite-sign muon, e.g. B_s0 -> mu+ mu- (PID cuts reversed)
    Line categories:   - 2-body   - dimuon   - double fake line
    """

    dimuons = make_inclusive_cut_dimuon(
        parent_id='B_s0', two_body=True, fake_muons=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


####################################
# Fake 2-body lines (same-sign)    #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDielectronSS(
        name="Hlt2CutBasedInclSingleFakeDielectronSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake dielectron events with same-sign electrons, e.g. B_s0 -> e+ e+ (PID cuts reversed)
    Line categories:   - 2-body   - dielectron   - same-sign   - single fake line
    """

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='B_s0', two_body=True, same_sign=True, fake_electrons=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDielectronSS(
        name="Hlt2CutBasedInclDoubleFakeDielectronSS",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake dielectron events with same-sign electrons, e.g. B_s0 -> e+ e+ (PID cuts reversed)
    Line categories:   - 2-body   - dielectron   - same-sign   - double fake line
    """

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='B_s0', two_body=True, same_sign=True, fake_electrons=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleEFakeElectronMuonSS(
        name="Hlt2CutBasedInclSingleEFakeElectronMuonSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake electron-muon events with same-sign leptons, e.g. B_s0 -> e+ mu+ (PID cuts revsersed)
    Line categories:   - 2-body   - electron-muon    - same-sign   - single fake electron line
    """

    electronmuons = make_inclusive_cut_electronmuon(
        parent_id='B_s0', two_body=True, same_sign=True, fake_electron=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleMuFakeElectronMuonSS(
        name="Hlt2CutBasedInclSingleMuFakeElectronMuonSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake electron-muon events with same-sign leptons, e.g. B_s0 -> e+ mu+ (PID cuts revsersed)
    Line categories:   - 2-body   - electron-muon    - same-sign   - single fake muon line
    """

    electronmuons = make_inclusive_cut_electronmuon(
        parent_id='B_s0', two_body=True, same_sign=True, fake_muon=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeElectronMuonSS(
        name="Hlt2CutBasedInclDoubleFakeElectronMuonSS",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body fake electron-muon events with same-sign leptons, e.g. B_s0 -> e+ mu+ (PID cuts revsersed)
    Line categories:   - 2-body   - electron-muon    - same-sign   - double fake line
    """

    electronmuons = make_inclusive_cut_electronmuon(
        parent_id='B_s0',
        two_body=True,
        same_sign=True,
        fake_electron=True,
        fake_muon=True)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDimuonSS(
        name="Hlt2CutBasedInclSingleFakeDimuonSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with same-sign muons, e.g. B_s0 -> mu+ mu+ 
    Line categories:   - 2-body   - dimuon   - same-sign   - single fake line
    """

    dimuons = make_inclusive_cut_dimuon(
        parent_id='B_s0', two_body=True, same_sign=True, fake_muons=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDimuonSS(
        name="Hlt2CutBasedInclDoubleFakeDimuonSS",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with same-sign muons, e.g. B_s0 -> mu+ mu+ 
    Line categories:   - 2-body   - dimuon   - same-sign   - double fake line
    """

    dimuons = make_inclusive_cut_dimuon(
        parent_id='B_s0', two_body=True, same_sign=True, fake_muons=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


################################################
# 3-body inclusive cut-based dilepton lines    #
################################################

####################################
# 3-body lines (opposite-sign)     #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronPlusTrack(
        name="Hlt2CutBasedInclDielectronPlusTrack", prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 3-body   - dielectron
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(parent_id='J/psi(1S)')
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='[B+ -> J/psi(1S) pi+]cc',
        n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonPlusTrack(
        name="Hlt2CutBasedInclElectronMuonPlusTrack", prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 3-body   - electron-muon
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(parent_id='J/psi(1S)')
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonPlusTrack(name="Hlt2CutBasedInclDimuonPlusTrack",
                                    prescale=1,
                                    persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body dimuon events with opposite-sign muon, e.g. B+ -> J/psi(1S)  (-> mu+ mu-) pi+
    Line categories:   - 3-body   - dimuon
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id='J/psi(1S)')
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


####################################
# 3-body lines (same-sign)         #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronPlusTrackSS(
        name="Hlt2CutBasedInclDielectronPlusTrackSS", prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 3-body   - dielectron   - same-sign
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', same_sign=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='[B+ -> J/psi(1S) pi+]cc',
        n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonPlusTrackSS(
        name="Hlt2CutBasedInclElectronMuonPlusTrackSS",
        prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 3-body   - electron-muon   - same-sign
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', same_sign=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonPlusTrackSS(name="Hlt2CutBasedInclDimuonPlusTrackSS",
                                      prescale=1,
                                      persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body dimuon events with opposite-sign muon, e.g. B+ -> J/psi(1S)  (-> mu+ mu-) pi+
    Line categories:   - 3-body   - dimuon   - same-sign
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id='J/psi(1S)', same_sign=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


####################################
# Fake 3-body lines (opposite-sign)#
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDielectronPlusTrack(
        name="Hlt2CutBasedInclSingleFakeDielectronPlusTrack",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - dielectron   - single fake line
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='[B+ -> J/psi(1S) pi+]cc',
        n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDielectronPlusTrack(
        name="Hlt2CutBasedInclDoubleFakeDielectronPlusTrack",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - dielectron   - double fake line
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', fake_electrons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='[B+ -> J/psi(1S) pi+]cc',
        n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleEFakeElectronMuonPlusTrack(
        name="Hlt2CutBasedInclSingleEFakeElectronMuonPlusTrack",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - electron-muon   - single fake electron line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', fake_electron=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleMuFakeElectronMuonPlusTrack(
        name="Hlt2CutBasedInclSingleMuFakeElectronMuonPlusTrack",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - electron-muon   - single fake muon line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', fake_muon=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeElectronMuonPlusTrack(
        name="Hlt2CutBasedInclDoubleFakeElectronMuonPlusTrack",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - electron-muon   - double fake line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', fake_electron=True, fake_muon=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDimuonPlusTrack(
        name="Hlt2CutBasedInclSingleFakeDimuonPlusTrack",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake dimuon events with opposite-sign muons, e.g. B+ -> J/psi(1S)  (-> mu+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - dimuon   - single fake line
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id='J/psi(1S)', fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDimuonPlusTrack(
        name="Hlt2CutBasedInclDoubleFakeDimuonPlusTrack",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake dimuon events with opposite-sign muons, e.g. B+ -> J/psi(1S)  (-> mu+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - dimuon   - double fake line
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id='J/psi(1S)', fake_muons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


####################################
# Fake 3-body lines (same-sign)    #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDielectronPlusTrackSS(
        name="Hlt2CutBasedInclSingleFakeDielectronPlusTrackSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake dielectron events with same-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e+) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - dielectron   - same-sign   - single fake line
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', same_sign=True, fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='[B+ -> J/psi(1S) pi+]cc',
        n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDielectronPlusTrackSS(
        name="Hlt2CutBasedInclDoubleFakeDielectronPlusTrackSS",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake dielectron events with same-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e+) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - dielectron   - same-sign   - double fake line
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', same_sign=True, fake_electrons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='[B+ -> J/psi(1S) pi+]cc',
        n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleEFakeElectronMuonPlusTrackSS(
        name="Hlt2CutBasedInclSingleEFakeElectronMuonPlusTrackSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake electron-muon events with same-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - electron-muon   - same-sign   - single fake electron line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', same_sign=True, fake_electron=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleMuFakeElectronMuonPlusTrackSS(
        name="Hlt2CutBasedInclSingleMuFakeElectronMuonPlusTrackSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake electron-muon events with same-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - electron-muon   - same-sign   - single fake muon line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', same_sign=True, fake_muon=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeElectronMuonPlusTrackSS(
        name="Hlt2CutBasedInclDoubleFakeElectronMuonPlusTrackSS",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake electron-muon events with same-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - electron-muon   - same-sign   - double fake line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)',
        same_sign=True,
        fake_electron=True,
        fake_muon=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDimuonPlusTrackSS(
        name="Hlt2CutBasedInclSingleFakeDimuonPlusTrackSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake dimuon events with same-sign muons, e.g. B+ -> J/psi(1S)  (-> mu+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - dimuon   - same-sign   - single fake line
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(
        parent_id='J/psi(1S)', same_sign=True, fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDimuonPlusTrackSS(
        name="Hlt2CutBasedInclDoubleFakeDimuonPlusTrackSS",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 3-body fake dimuon events with same-sign muons, e.g. B+ -> J/psi(1S)  (-> mu+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 3-body   - dimuon   - same-sign   - double fake line
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(
        parent_id='J/psi(1S)', same_sign=True, fake_muons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='[B+ -> J/psi(1S) pi+]cc', n_tracks=1)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


################################################
# 4-body inclusive cut-based dilepton lines    #
################################################

####################################
# 4-body lines (opposite-sign)     #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronPlusTwoTrack(
        name="Hlt2CutBasedInclDielectronPlusTwoTrack",
        prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 4-body   - dielectron
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(parent_id='J/psi(1S)')
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonPlusTwoTrack(
        name="Hlt2CutBasedInclElectronMuonPlusTwoTrack",
        prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 4-body   - electron-muon
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(parent_id='J/psi(1S)')
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonPlusTwoTrack(
        name="Hlt2CutBasedInclDimuonPlusTwoTrack", prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body dimuon events with opposite-sign muon, e.g. B+ -> J/psi(1S)  (-> mu+ mu-) pi+
    Line categories:   - 4-body   - dimuon
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id='J/psi(1S)')
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-', n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


####################################
# 4-body lines (same-sign)         #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronPlusTwoTrackSS(
        name="Hlt2CutBasedInclDielectronPlusTwoTrackSS",
        prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 4-body   - dielectron   - same-sign
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', same_sign=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonPlusTwoTrackSS(
        name="Hlt2CutBasedInclElectronMuonPlusTwoTrackSS",
        prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 4-body   - electron-muon   - same-sign
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', same_sign=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonPlusTwoTrackSS(
        name="Hlt2CutBasedInclDimuonPlusTwoTrackSS", prescale=1,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body dimuon events with opposite-sign muon, e.g. B+ -> J/psi(1S)  (-> mu+ mu-) pi+
    Line categories:   - 4-body   - dimuon   - same-sign
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id='J/psi(1S)', same_sign=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-', n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


####################################
# Fake 4-body lines (opposite-sign)#
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDielectronPlusTwoTrack(
        name="Hlt2CutBasedInclSingleFakeDielectronPlusTwoTrack",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - dielectron   - single fake line
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDielectronPlusTwoTrack(
        name="Hlt2CutBasedInclDoubleFakeDielectronPlusTwoTrack",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - dielectron   - double fake line
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', fake_electrons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleEFakeElectronMuonPlusTwoTrack(
        name="Hlt2CutBasedInclSingleEFakeElectronMuonPlusTwoTrack",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - electron-muon   - single fake electron line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', fake_electron=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleMuFakeElectronMuonPlusTwoTrack(
        name="Hlt2CutBasedInclSingleMuFakeElectronMuonPlusTwoTrack",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - electron-muon   - single fake muon line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', fake_muon=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeElectronMuonPlusTwoTrack(
        name="Hlt2CutBasedInclDoubleFakeElectronMuonPlusTwoTrack",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - electron-muon   - double fake line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', fake_electron=True, fake_muon=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDimuonPlusTwoTrack(
        name="Hlt2CutBasedInclSingleFakeDimuonPlusTwoTrack",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake dimuon events with opposite-sign muons, e.g. B+ -> J/psi(1S)  (-> mu+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - dimuon   - single fake line
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id='J/psi(1S)', fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-', n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDimuonPlusTwoTrack(
        name="Hlt2CutBasedInclDoubleFakeDimuonPlusTwoTrack",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake dimuon events with opposite-sign muons, e.g. B+ -> J/psi(1S)  (-> mu+ mu-) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - dimuon   - double fake line
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id='J/psi(1S)', fake_muons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-', n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


####################################
# Fake 4-body lines (same-sign)    #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDielectronPlusTwoTrackSS(
        name="Hlt2CutBasedInclSingleFakeDielectronPlusTwoTrackSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake dielectron events with same-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e+) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - dielectron   - same-sign   - single fake line
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', same_sign=True, fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDielectronPlusTwoTrackSS(
        name="Hlt2CutBasedInclDoubleFakeDielectronPlusTwoTrackSS",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake dielectron events with same-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e+) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - dielectron   - same-sign   - double fake line
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(
        parent_id='J/psi(1S)', same_sign=True, fake_electrons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleEFakeElectronMuonPlusTwoTrackSS(
        name="Hlt2CutBasedInclSingleEFakeElectronMuonPlusTwoTrackSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake electron-muon events with same-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - electron-muon   - same-sign   - single fake electron line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', same_sign=True, fake_electron=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleMuFakeElectronMuonPlusTwoTrackSS(
        name="Hlt2CutBasedInclSingleMuFakeElectronMuonPlusTwoTrackSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake electron-muon events with same-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - electron-muon   - same-sign   - single fake muon line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)', same_sign=True, fake_muon=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeElectronMuonPlusTwoTrackSS(
        name="Hlt2CutBasedInclDoubleFakeElectronMuonPlusTwoTrackSS",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake electron-muon events with same-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - electron-muon   - same-sign   - double fake line
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(
        parent_id='J/psi(1S)',
        same_sign=True,
        fake_electron=True,
        fake_muon=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons,
        pvs,
        DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-',
        n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclSingleFakeDimuonPlusTwoTrackSS(
        name="Hlt2CutBasedInclSingleFakeDimuonPlusTwoTrackSS",
        prescale=single_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake dimuon events with same-sign muons, e.g. B+ -> J/psi(1S)  (-> mu+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - dimuon   - same-sign   - single fake line
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(
        parent_id='J/psi(1S)', same_sign=True, fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-', n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)


@register_line_builder(all_lines)
def Hlt2CutBasedInclDoubleFakeDimuonPlusTwoTrackSS(
        name="Hlt2CutBasedInclDoubleFakeDimuonPlusTwoTrackSS",
        prescale=double_fake_prescale,
        persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 4-body fake dimuon events with same-sign muons, e.g. B+ -> J/psi(1S)  (-> mu+ mu+) pi+ (PID cuts reversed) 
    Line categories:   - 4-body   - dimuon   - same-sign   - double fake line
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(
        parent_id='J/psi(1S)', same_sign=True, fake_muons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor='B_s0 -> J/psi(1S) pi+ pi-', n_tracks=2)

    return HltLine(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco)
