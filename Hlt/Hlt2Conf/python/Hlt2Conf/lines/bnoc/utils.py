###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Utilities used in BnoC selection lines

check_process: make sure the `process` argument is 'hlt2' or 'spruce'

"""

from PyConf.utilities import ConfigurationError


def check_process(func):
    def wrapper(*args, process, **kwargs):
        if process not in ['hlt2', 'spruce']:
            raise ConfigurationError(
                '`process` should be either "hlt2" or "spruce".')
        return func(*args, process=process, **kwargs)

    return wrapper
