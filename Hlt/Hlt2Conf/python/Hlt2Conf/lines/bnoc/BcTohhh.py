###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC BcTohhh lines
"""
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.bnoc.builders import basic_builder
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders import b_builder

##############################################
# BcTohhh lines
##############################################


@check_process
def make_BcToppbarpi(process):
    if process == 'spruce':
        protons = basic_builder.make_protons()
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        protons = basic_builder.make_soft_protons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
    line_alg = b_builder._make_b2hhh(  #change the name of the Bc builder???
        particles=[protons, protons, pions],
        descriptor='[B_c+ -> p+ p~- pi+]cc',
        am_min=5998 * MeV,
        am_max=6502 * MeV,
        am_min_vtx=5998 * MeV,
        am_max_vtx=6502 * MeV,
        CORRMmin=0.0 * MeV,
        CORRMmax=10000 * MeV,
        Psum=22000 * MeV,
        vtx_chi2pdof_max=40,
        TRKCHIDOFmin=1.5)
    return line_alg


@check_process
def make_BcToppbarK(process):
    if process == 'spruce':
        protons = basic_builder.make_protons(pi_pidk_max=None)
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        protons = basic_builder.make_soft_protons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)

    line_alg = b_builder._make_b2hhh(
        particles=[protons, protons, kaons],
        descriptor='[B_c+ -> p+ p~- K+]cc',
        am_min=5998 * MeV,
        am_max=6502 * MeV,
        am_min_vtx=5998 * MeV,
        am_max_vtx=6502 * MeV,
        CORRMmin=0.0 * MeV,
        CORRMmax=10000 * MeV,
        Psum=22000 * MeV,
        vtx_chi2pdof_max=40,
        TRKCHIDOFmin=1.5)
    return line_alg


@check_process
def make_BcToKKK(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
    line_alg = b_builder._make_b2hhh(
        particles=[kaons, kaons, kaons],
        descriptor=
        '[B_c+ -> K+ K+ K-]cc',  #Exception throw: Cannot find ParticleProperty for Bc+ StatusCode=FAILURE
        am_min=5998 * MeV,
        am_max=6502 * MeV,
        am_min_vtx=5998 * MeV,
        am_max_vtx=6502 * MeV,
        CORRMmin=0.0 * MeV,
        CORRMmax=10000 * MeV,
        Psum=22000 * MeV,
        vtx_chi2pdof_max=40,
        TRKCHIDOFmin=1.5)
    return line_alg


@check_process
def make_BcToKpiK(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
    line_alg = b_builder._make_b2hhh(
        particles=[kaons, pions, kaons],
        descriptor='[B_c+ -> K+ pi+ K-]cc',
        am_min=5998 * MeV,
        am_max=6502 * MeV,
        am_min_vtx=5998 * MeV,
        am_max_vtx=6502 * MeV,
        CORRMmin=0.0 * MeV,
        CORRMmax=10000 * MeV,
        Psum=22000,
        vtx_chi2pdof_max=40,
        TRKCHIDOFmin=1.5)
    return line_alg


@check_process
def make_BcToKpipi(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
    line_alg = b_builder._make_b2hhh(
        particles=[kaons, pions, pions],
        descriptor='[B_c+ -> K+ pi+ pi-]cc',
        am_min=5998 * MeV,
        am_max=6502 * MeV,
        am_min_vtx=5998 * MeV,
        am_max_vtx=6502 * MeV,
        CORRMmin=0.0 * MeV,
        CORRMmax=10000 * MeV,
        Psum=22000 * MeV,
        vtx_chi2pdof_max=40,
        TRKCHIDOFmin=1.5)
    return line_alg


@check_process
def make_BcTopipipi(process):
    if process == 'spruce':
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
    line_alg = b_builder._make_b2hhh(
        particles=[pions, pions, pions],
        descriptor='[B_c+ -> pi+ pi+ pi-]cc',
        am_min=5998 * MeV,
        am_max=6502 * MeV,
        am_min_vtx=5998 * MeV,
        am_max_vtx=6502 * MeV,
        CORRMmin=0.0 * MeV,
        CORRMmax=10000 * MeV,
        Psum=22000 * MeV,
        vtx_chi2pdof_max=40,
        TRKCHIDOFmin=1.5)
    return line_alg


@check_process
def make_BcToKpKppim(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
    line_alg = b_builder._make_b2hhh(
        particles=[kaons, kaons, pions],
        descriptor='[B_c+ -> K+ K+ pi-]cc',
        am_min=5998 * MeV,
        am_max=6502 * MeV,
        am_min_vtx=5998 * MeV,
        am_max_vtx=6502 * MeV,
        CORRMmin=0.0 * MeV,
        CORRMmax=10000 * MeV,
        Psum=22000 * MeV,
        vtx_chi2pdof_max=40,
        TRKCHIDOFmin=1.5)
    return line_alg


@check_process
def make_BcTopippipKm(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)

    line_alg = b_builder._make_b2hhh(
        particles=[pions, pions, kaons],
        descriptor='[B_c+ -> pi+ pi+ K-]cc',
        am_min=5998 * MeV,
        am_max=6502 * MeV,
        am_min_vtx=5998 * MeV,
        am_max_vtx=6502 * MeV,
        CORRMmin=0.0 * MeV,
        CORRMmax=10000 * MeV,
        Psum=22000 * MeV,
        vtx_chi2pdof_max=40,
        TRKCHIDOFmin=1.5)
    return line_alg
