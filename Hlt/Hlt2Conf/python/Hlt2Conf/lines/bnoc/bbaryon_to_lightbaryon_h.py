###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC b-baryon -> hyperon h (h) lines
"""
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders import basic_builder
from Hlt2Conf.lines.bnoc.builders import combiners
all_lines = {}


###########################################################
# Form the Lambda_b0 -> Xi- K+, Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-
# Xi- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_LbToXimKp_XimToLambdaLLPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        kaons = basic_builder.make_kaons(k_pidk_min=None)
    if process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions())
        kaons = basic_builder.make_kaons()
    line_alg = combiners.make_twobody(
        particles=[hyperons, kaons],
        am_min=5510 * MeV,
        am_max=5730 * MeV,
        vtx_am_min=5540 * MeV,
        vtx_am_max=5700 * MeV,
        name="LbToXimKp_LLL_Commbiner",
        descriptor='[Lambda_b0 -> Xi- K+]cc')
    return line_alg


@check_process
def make_LbToXimKp_XimToLambdaDDPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        kaons = basic_builder.make_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions())
        kaons = basic_builder.make_kaons()
    line_alg = combiners.make_twobody(
        particles=[hyperons, kaons],
        am_min=5510 * MeV,
        am_max=5730 * MeV,
        vtx_am_min=5540 * MeV,
        vtx_am_max=5700 * MeV,
        name="LbToXimKp_DDL_Commbiner",
        descriptor='[Lambda_b0 -> Xi- D0]cc')
    return line_alg


###########################################################
# Form the Xi_b0 -> Xi- pi+, Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-
# Xi- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_Xib0ToXimPip_XimToLambdaLLPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        pions = basic_builder.make_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions())
        pions = basic_builder.make_pions()
    line_alg = combiners.make_twobody(
        particles=[hyperons, pions],
        am_min=5680 * MeV,
        am_max=5900 * MeV,
        vtx_am_min=5710 * MeV,
        vtx_am_max=5870 * MeV,
        name="Xib0ToXimPip_LLL_Commbiner",
        descriptor='[Xi_b0 -> Xi- pi+]cc')
    return line_alg


@check_process
def make_Xib0ToXimPip_XimToLambdaDDPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        pions = basic_builder.make_pions(pi_pidk_max=None)
    if process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions())
        pions = basic_builder.make_pions()
    line_alg = combiners.make_twobody(
        particles=[hyperons, pions],
        am_min=5680 * MeV,
        am_max=5900 * MeV,
        vtx_am_min=5710 * MeV,
        vtx_am_max=5870 * MeV,
        name="Xib0ToXimPip_DDL_Commbiner",
        descriptor='[Xi_b0 -> Xi- pi+]cc')
    return line_alg


###########################################################
# Form the Xi_b- -> Xi- pi+ pi-, Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-
# Xi- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_XibmToXimPipPim_XimToLambdaLLPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        pions = basic_builder.make_pions(pi_pidk_max=None)
    if process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions())
        pions = basic_builder.make_pions()
    line_alg = combiners.make_threebody(
        particles=[hyperons, pions, pions],
        am_min=5680 * MeV,
        am_max=5900 * MeV,
        vtx_am_min=5710 * MeV,
        vtx_am_max=5870 * MeV,
        name="XibmToXimPipPim_LLL_Commbiner",
        descriptor='[Xi_b- -> Xi- pi+ pi-]cc')
    return line_alg


@check_process
def make_XibmToXimPipPim_XimToLambdaDDPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        pions = basic_builder.make_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions())
        pions = basic_builder.make_pions()
    line_alg = combiners.make_threebody(
        particles=[hyperons, pions, pions],
        am_min=5680 * MeV,
        am_max=5900 * MeV,
        vtx_am_min=5710 * MeV,
        vtx_am_max=5870 * MeV,
        name="XibmToXimPipPim_DDL_Commbiner",
        descriptor='[Xi_b- -> Xi- pi+ pi-]cc')
    return line_alg


###########################################################
# Form the Xi_b0 -> Omega- K+, Omega- -> Lambda0 K-, Lambda0 -> p+ pi-
# Omega- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_Xib0ToOmmKp_OmmToLambdaLLK(process):
    if process == 'spruce':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_LL(),
            kaons=basic_builder.make_kaons(k_pidk_min=None))
        kaons = basic_builder.make_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_LL(),
            kaons=basic_builder.make_kaons())
        kaons = basic_builder.make_kaons()
    line_alg = combiners.make_twobody(
        particles=[hyperons, kaons],
        am_min=5680 * MeV,
        am_max=5900 * MeV,
        vtx_am_min=5710 * MeV,
        vtx_am_max=5870 * MeV,
        name="Xib0ToOmmKp_LLL_Commbiner",
        descriptor='[Xi_b0 -> Omega- K+]cc')
    return line_alg


@check_process
def make_Xib0ToOmmKp_OmmToLambdaDDK(process):
    if process == 'spruce':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_DD(),
            kaons=basic_builder.make_kaons(k_pidk_min=None))
        kaons = basic_builder.make_kaons(k_pidk_min=None)
    if process == 'hlt2':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_DD(),
            kaons=basic_builder.make_kaons())
        kaons = basic_builder.make_kaons()
    line_alg = combiners.make_twobody(
        particles=[hyperons, kaons],
        am_min=5680 * MeV,
        am_max=5900 * MeV,
        vtx_am_min=5710 * MeV,
        vtx_am_max=5870 * MeV,
        name="Xib0ToOmmKp_DDL_Commbiner",
        descriptor='[Xi_b0 -> Omega- K+]cc')
    return line_alg


###########################################################
# Form the Xi_b- -> Omega- K+ pi-, Omega- -> Lambda0 K-, Lambda0 -> p+ pi-
# Omega- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_XibmToOmmKpPim_OmmToLambdaLLK(process):
    if process == 'spruce':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_LL(),
            kaons=basic_builder.make_kaons(k_pidk_min=None))
        kaons = basic_builder.make_kaons(k_pidk_min=None)
        pions = basic_builder.make_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_LL(),
            kaons=basic_builder.make_kaons())
        kaons = basic_builder.make_kaons()
        pions = basic_builder.make_pions()
    line_alg = combiners.make_threebody(
        particles=[hyperons, kaons, pions],
        am_min=5680 * MeV,
        am_max=5900 * MeV,
        vtx_am_min=5710 * MeV,
        vtx_am_max=5870 * MeV,
        name="XibmToOmmKpPim_LLL_Commbiner",
        descriptor='[Xi_b- -> Omega- K+ pi-]cc')
    return line_alg


@check_process
def make_XibmToOmmKpPim_OmmToLambdaDDK(process):
    if process == 'spruce':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_DD(),
            kaons=basic_builder.make_kaons(k_pidk_min=None))
        kaons = basic_builder.make_kaons(k_pidk_min=None)
        pions = basic_builder.make_pions(pi_pidk_max=None)
    if process == 'hlt2':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_DD(),
            kaons=basic_builder.make_kaons())
        kaons = basic_builder.make_kaons()
        pions = basic_builder.make_pions()
    line_alg = combiners.make_threebody(
        particles=[hyperons, kaons, pions],
        am_min=5680 * MeV,
        am_max=5900 * MeV,
        vtx_am_min=5710 * MeV,
        vtx_am_max=5870 * MeV,
        name="XibmToOmmKpPim_DDL_Commbiner",
        descriptor='[Xi_b- -> Omega- K+ pi-]cc')
    return line_alg


###########################################################
# Form the Omega_b- -> Omega- pi+ pi-, Omega- -> Lambda0 K-, Lambda0 -> p+ pi-
# Omega- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_OmbmToOmmPipPim_OmmToLambdaLLK(process):
    if process == 'spruce':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_LL(),
            kaons=basic_builder.make_kaons(k_pidk_min=None))
        pions = basic_builder.make_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_LL(),
            kaons=basic_builder.make_kaons())
        pions = basic_builder.make_pions()
    line_alg = combiners.make_threebody(
        particles=[hyperons, pions, pions],
        am_min=5970 * MeV,
        am_max=6130 * MeV,
        vtx_am_min=6000 * MeV,
        vtx_am_max=6100 * MeV,
        name="OmbmToOmmPipPim_LLL_Commbiner",
        descriptor='[Xi_b- -> Omega- pi+ pi-]cc')
    return line_alg


@check_process
def make_OmbmToOmmPipPim_OmmToLambdaDDK(process):
    if process == 'spruce':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_DD(),
            kaons=basic_builder.make_kaons(k_pidk_min=None))
        pions = basic_builder.make_pions(pi_pidk_max=None)
    if process == 'hlt2':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_DD(),
            kaons=basic_builder.make_kaons())
        pions = basic_builder.make_pions()
    line_alg = combiners.make_threebody(
        particles=[hyperons, pions, pions],
        am_min=5970 * MeV,
        am_max=6130 * MeV,
        vtx_am_min=6000 * MeV,
        vtx_am_max=6100 * MeV,
        name="OmbmToOmmPipPim_DDL_Commbiner",
        descriptor='[Xi_b- -> Omega- pi+ pi-]cc')
    return line_alg
