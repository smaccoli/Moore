###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC BTohhh lines
"""
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.bnoc.builders import basic_builder
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders import b_builder

##############################################
# BuTohhh lines
##############################################


@check_process
def make_BuToKKK(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
    line_alg = b_builder._make_b2hhh(
        particles=[kaons, kaons, kaons], descriptor='[B+ -> K+ K+ K-]cc')
    return line_alg


@check_process
def make_BuToKpiK(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
    line_alg = b_builder._make_b2hhh(
        particles=[kaons, pions, kaons], descriptor='[B+ -> K+ pi+ K-]cc')
    return line_alg


@check_process
def make_BuToKpipi(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)

    line_alg = b_builder._make_b2hhh(
        particles=[kaons, pions, pions], descriptor='[B+ -> K+ pi+ pi-]cc')
    return line_alg


@check_process
def make_BuTopipipi(process):
    if process == 'spruce':
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
    line_alg = b_builder._make_b2hhh(
        particles=[pions, pions, pions], descriptor='[B+ -> pi+ pi+ pi-]cc')
    return line_alg


@check_process
def make_BuToKpKppim(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)

    line_alg = b_builder._make_b2hhh(
        particles=[kaons, kaons, pions], descriptor='[B+ -> K+ K+ pi-]cc')
    return line_alg


@check_process
def make_BuTopippipKm(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)

    line_alg = b_builder._make_b2hhh(
        particles=[pions, pions, kaons], descriptor='[B+ -> pi+ pi+ K-]cc')
    return line_alg


@check_process
def make_BuToppbarK(process):
    if process == 'spruce':
        protons = basic_builder.make_soft_protons(pi_pidk_max=None)
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        protons = basic_builder.make_soft_protons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)

    line_alg = b_builder._make_b2hhh(
        particles=[protons, protons, kaons],
        descriptor='[B+ -> p+ p~- K+]cc',
        am_min=5079.15 * MeV,
        am_max=5679.15 * MeV)
    return line_alg


@check_process
def make_BuToppbarpi(process):
    if process == 'spruce':
        protons = basic_builder.make_soft_protons(pi_pidk_max=None)
        pions = basic_builder.make_soft_pions(k_pidk_min=None)
    elif process == 'hlt2':
        protons = basic_builder.make_soft_protons(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)
        pions = basic_builder.make_soft_pions(
            tr_ghost_prob_max=0.5, trchi2todof_max=4.0, mipchi2_min=1.0)

        pions = basic_builder.make_soft_pions()
    line_alg = b_builder._make_b2hhh(
        particles=[protons, protons, pions],
        descriptor='[B+ -> p+ p~- pi+]cc',
        am_min=5079.15 * MeV,
        am_max=5679.15 * MeV)
    return line_alg
