###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of BnoC hlt2 lines. This is where Hlt2 lines are actually booked

    FT is currently a bit unclear but suggest that if you want FT you also save
    the "extra_output" of additional tracks
"""
from Moore.config import HltLine, register_line_builder

from PyConf import configurable

from Hlt2Conf.lines.bnoc.prefilters import bnoc_prefilters

from Hlt2Conf.lines.bnoc.builders.basic_builder import ft_extra_outputs, Topo_prefilter_extra_outputs
from Hlt2Conf.lines.bnoc import BdsTohhhh
from Hlt2Conf.lines.bnoc import BdsToVV
from Hlt2Conf.lines.bnoc import BuToKShhh
from Hlt2Conf.lines.bnoc import BuTohhh
from Hlt2Conf.lines.bnoc import BcTohhh
from Hlt2Conf.lines.bnoc import bbaryon_to_lightbaryon_h
from Hlt2Conf.lines.bnoc import bTohh
from Hlt2Conf.lines.bnoc import BuToKSh

PROCESS = 'hlt2'
hlt2_lines = {}


@register_line_builder(hlt2_lines)
@configurable
def Bds_PhiPhi_line(name='Hlt2BnoC_BdsToPhiPhi', prescale=1):
    line_alg = BdsToVV.make_BdsToPhiPhi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters() + [line_alg],
        persistreco=True,
        extra_outputs=ft_extra_outputs())


@register_line_builder(hlt2_lines)
@configurable
def Bds_KstzKstzb_line(name='Hlt2BnoC_BdsToKstzKstzb', prescale=1):
    line_alg = BdsToVV.make_BdsToKstzKstzb(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters() + [line_alg],
        extra_outputs=ft_extra_outputs())


@register_line_builder(hlt2_lines)
@configurable
def Bds_KstzPhi_line(name='Hlt2BnoC_BdsToKstzPhi', prescale=1):
    line_alg = BdsToVV.make_BdsToKstzPhi(process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
@configurable
def Bds_KstzRho_line(name='Hlt2BnoC_BdsToKstzRho', prescale=1):
    line_alg = BdsToVV.make_BdsToKstzRho(process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
@configurable
def Bds_pipipipi_line(name='Hlt2BnoC_BdsToPipPimPipPim',
                      prescale=1,
                      min_twobody_mva=0.13,
                      min_threebody_mva=0.13):
    line_alg = BdsTohhhh.make_BdsToPiPiPiPi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=ft_extra_outputs() + Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bds_Kpipipi_line(name='Hlt2BnoC_BdsToKpPimPipPim',
                     prescale=1,
                     min_twobody_mva=0.15,
                     min_threebody_mva=0.15):
    line_alg = BdsTohhhh.make_BdsToKPiPiPi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=ft_extra_outputs() + Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bds_KKpipi_line(name='Hlt2BnoC_BdsToKpKmPipPim',
                    prescale=1,
                    min_twobody_mva=0.15,
                    min_threebody_mva=0.15):
    line_alg = BdsTohhhh.make_BdsToKKPiPi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=ft_extra_outputs() + Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bds_KKKpi_line(name='Hlt2BnoC_BdsToKpKmKpPim',
                   prescale=1,
                   min_twobody_mva=0.13,
                   min_threebody_mva=0.13):
    line_alg = BdsTohhhh.make_BdsToKKKPi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=ft_extra_outputs() + Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bds_KKKK_line(name='Hlt2BnoC_BdsToKpKmKpKm',
                  prescale=1,
                  min_twobody_mva=0.1,
                  min_threebody_mva=0.1):
    line_alg = BdsTohhhh.make_BdsToKKKK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=ft_extra_outputs() + Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSPiPiPi_LL_line(name='Hlt2BnoC_BuToKSPiPiPi_LL',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1,
                        persistreco=False):
    line_alg = BuToKShhh.make_BuToKSPiPiPi_LL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=
        persistreco,  # persistreco if this want to be the input for the sprucing
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BuKPi_line(name="Hlt2BnoC_BuToKpPi0", prescale=1):
    line_alg = bTohh.make_BuToKpPi0(process=PROCESS)
    return HltLine(
        name=name, algs=bnoc_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSPiPiPi_DD_line(name='Hlt2BnoC_BuToKSPiPiPi_DD',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSPiPiPi_DD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKpPiPi_LL_line(name='Hlt2BnoC_BuToKSKpPiPi_LL',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKpPiPi_LL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKpPiPi_DD_line(name='Hlt2BnoC_BuToKSKpPiPi_DD',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKpPiPi_DD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKmPiPi_LL_line(name='Hlt2BnoC_BuToKSKmPiPi_LL',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKmPiPi_LL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKmPiPi_DD_line(name='Hlt2BnoC_BuToKSKmPiPi_DD',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKmPiPi_DD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKKPip_LL_line(name='Hlt2BnoC_BuToKSKKPip_LL',
                       prescale=1,
                       min_twobody_mva=0.1,
                       min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKKPip_LL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKKPip_DD_line(name='Hlt2BnoC_BuToKSKKPip_DD',
                       prescale=1,
                       min_twobody_mva=0.1,
                       min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKKPip_DD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKKPim_LL_line(name='Hlt2BnoC_BuToKSKKPim_LL',
                       prescale=1,
                       min_twobody_mva=0.1,
                       min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKKPim_LL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKKPim_DD_line(name='Hlt2BnoC_BuToKSKKPim_DD',
                       prescale=1,
                       min_twobody_mva=0.1,
                       min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKKPim_DD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKKK_LL_line(name='Hlt2BnoC_BuToKSKKK_LL',
                     prescale=1,
                     min_twobody_mva=0.1,
                     min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKKK_LL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def Bu_KSKKK_DD_line(name='Hlt2BnoC_BuToKSKKK_DD',
                     prescale=1,
                     min_twobody_mva=0.1,
                     min_threebody_mva=0.1):
    line_alg = BuToKShhh.make_BuToKSKKK_DD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


##############################################
# From the bbaryon_to_lightbaryon_h builder
##############################################


@register_line_builder(hlt2_lines)
def LbToXimKp_XimToLambdaLLPi_line(name='Hlt2BnoC_LbToXimKp_XimToLambdaLLPi',
                                   prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_LbToXimKp_XimToLambdaLLPi(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def LbToXimKp_XimToLambdaDDPi_line(name='Hlt2BnoC_LbToXimKp_XimToLambdaDDPi',
                                   prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_LbToXimKp_XimToLambdaDDPi(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def Xib0ToXimPip_XimToLambdaLLPi_line(
        name='Hlt2BnoC_Xib0ToXimPip_XimToLambdaLLPi', prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_Xib0ToXimPip_XimToLambdaLLPi(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def Xib0ToXimPip_XimToLambdaDDPi_line(
        name='Hlt2BnoC_Xib0ToXimPip_XimToLambdaDDPi', prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_Xib0ToXimPip_XimToLambdaDDPi(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def XibmToXimPipPim_XimToLambdaLLPi_line(
        name='Hlt2BnoC_XibmToXimPipPim_XimToLambdaLLPi', prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_XibmToXimPipPim_XimToLambdaLLPi(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def XibmToXimPipPim_XimToLambdaDDPi_line(
        name='Hlt2BnoC_XibmToXimPipPim_XimToLambdaDDPi', prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_XibmToXimPipPim_XimToLambdaDDPi(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def Xib0ToOmmKp_OmmToLambdaLLK_line(name='Hlt2BnoC_Xib0ToOmmKp_OmmToLambdaLLK',
                                    prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_Xib0ToOmmKp_OmmToLambdaLLK(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def Xib0ToOmmKp_OmmToLambdaDDK_line(name='Hlt2BnoC_Xib0ToOmmKp_OmmToLambdaDDK',
                                    prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_Xib0ToOmmKp_OmmToLambdaDDK(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def XibmToOmmKpPim_OmmToLambdaLLK_line(
        name='Hlt2BnoC_XibmToOmmKpPim_OmmToLambdaLLK', prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_XibmToOmmKpPim_OmmToLambdaLLK(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def XibmToOmmKpPim_OmmToLambdaDDK_line(
        name='Hlt2BnoC_XibmToOmmKpPim_OmmToLambdaDDK', prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_XibmToOmmKpPim_OmmToLambdaDDK(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def OmbmToOmmPipPim_OmmToLambdaLLK_line(
        name='Hlt2BnoC_OmbmToOmmPipPim_OmmToLambdaLLK', prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_OmbmToOmmPipPim_OmmToLambdaLLK(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


@register_line_builder(hlt2_lines)
def OmbmToOmmPipPim_OmmToLambdaDDK_line(
        name='Hlt2BnoC_OmbmToOmmPipPim_OmmToLambdaDDK', prescale=1):
    line_alg = bbaryon_to_lightbaryon_h.make_OmbmToOmmPipPim_OmmToLambdaDDK(
        process=PROCESS)
    return HltLine(
        name=name, prescale=prescale, algs=bnoc_prefilters() + [line_alg])


#bTohh
@register_line_builder(hlt2_lines)
def BuToLambdapLL_hlt2_line(name='Hlt2BnoC_BuToLambdapLL',
                            prescale=1,
                            min_twobody_mva=0.1,
                            min_threebody_mva=0.1):
    line_alg = bTohh.make_BuToLambdapLL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BuToLambdapDD_hlt2_line(name='Hlt2BnoC_BuToLambdapDD',
                            prescale=1,
                            min_twobody_mva=0.1,
                            min_threebody_mva=0.1):
    line_alg = bTohh.make_BuToLambdapDD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BcToLambdapLL_hlt2_line(name='Hlt2BnoC_BcToLambdapLL',
                            prescale=1,
                            min_twobody_mva=0.1,
                            min_threebody_mva=0.1):
    line_alg = bTohh.make_BcToLambdapLL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BcToLambdapDD_hlt2_line(name='Hlt2BnoC_BcToLambdapDD',
                            prescale=1,
                            min_twobody_mva=0.1,
                            min_threebody_mva=0.1):
    line_alg = bTohh.make_BcToLambdapDD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def XibToLambdapiLL_hlt2_line(name='Hlt2BnoC_XibToLambdapiLL',
                              prescale=1,
                              min_twobody_mva=0.1,
                              min_threebody_mva=0.1):
    line_alg = bTohh.make_XibToLambdapiLL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def XibToLambdapiDD_hlt2_line(name='Hlt2BnoC_XibToLambdapiDD',
                              prescale=1,
                              min_twobody_mva=0.1,
                              min_threebody_mva=0.1):
    line_alg = bTohh.make_XibToLambdapiDD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def XibToLambdaKLL_hlt2_line(name='Hlt2BnoC_XibToLambdaKLL',
                             prescale=1,
                             min_twobody_mva=0.1,
                             min_threebody_mva=0.1):
    line_alg = bTohh.make_XibToLambdaKLL(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def XibToLambdaKDD_hlt2_line(name='Hlt2BnoC_XibToLambdaKDD',
                             prescale=1,
                             min_twobody_mva=0.1,
                             min_threebody_mva=0.1):
    line_alg = bTohh.make_XibToLambdaKDD(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


#Ksh
@register_line_builder(hlt2_lines)
def BuToKsLLPi_hlt2_line(name='Hlt2BnoC_BuToKsLLPi',
                         prescale=1,
                         min_twobody_mva=0.1,
                         min_threebody_mva=0.1):
    line_alg = BuToKSh.make_BuToKSLLPi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BuToKsDDPi_hlt2_line(name='Hlt2BnoC_BuToKsDDPi',
                         prescale=1,
                         min_twobody_mva=0.1,
                         min_threebody_mva=0.1):
    line_alg = BuToKSh.make_BuToKSDDPi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BcToKsLLPi_hlt2_line(name='Hlt2BnoC_BcToKsLLPi',
                         prescale=1,
                         min_twobody_mva=0.1,
                         min_threebody_mva=0.1):
    line_alg = BuToKSh.make_BcToKSLLPi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BcToKsDDPi_hlt2_line(name='Hlt2BnoC_BcToKsDDPi',
                         prescale=1,
                         min_twobody_mva=0.1,
                         min_threebody_mva=0.1):
    line_alg = BuToKSh.make_BcToKSDDPi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BuToKsLLK_hlt2_line(name='Hlt2BnoC_BuToKsLLK',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuToKSh.make_BuToKSLLK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BuToKsDDK_hlt2_line(name='Hlt2BnoC_BuToKsDDK',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuToKSh.make_BuToKSDDK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BcToKsLLK_hlt2_line(name='Hlt2BnoC_BcToKsLLK',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuToKSh.make_BuToKSLLK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
def BcToKsDDK_hlt2_line(name='Hlt2BnoC_BcToKsDDK',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuToKSh.make_BuToKSDDK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BuToKKK_hlt2_line(name='Hlt2BnoC_BuToKKK',
                      prescale=1,
                      min_twobody_mva=0.13,
                      min_threebody_mva=0.13):
    line_alg = BuTohhh.make_BuToKKK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BuToKpiK_hlt2_line(name='Hlt2BnoC_BuToKpiK',
                       prescale=1,
                       min_twobody_mva=0.1,
                       min_threebody_mva=0.1):
    line_alg = BuTohhh.make_BuToKpiK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BuToKpipi_hlt2_line(name='Hlt2BnoC_BuToKpipi',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BuTohhh.make_BuToKpipi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BuTopipipi_hlt2_line(name='Hlt2BnoC_BuTopipipi',
                         prescale=1,
                         min_twobody_mva=0.1,
                         min_threebody_mva=0.1):
    line_alg = BuTohhh.make_BuTopipipi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BuToKpKppim_hlt2_line(name='Hlt2BnoC_BuToKpKppim',
                          prescale=1,
                          min_twobody_mva=0.1,
                          min_threebody_mva=0.1):
    line_alg = BuTohhh.make_BuToKpKppim(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BuTopippipKm_hlt2_line(name='Hlt2BnoC_BuTopippipKm',
                           prescale=1,
                           min_twobody_mva=0.1,
                           min_threebody_mva=0.1):
    line_alg = BuTohhh.make_BuTopippipKm(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BuToppbarK_hlt2_line(name='Hlt2BnoC_BuToppbarK',
                         prescale=1,
                         min_twobody_mva=0.1,
                         min_threebody_mva=0.1):
    line_alg = BuTohhh.make_BuToppbarK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BuToppbarpi_hlt2_line(name='Hlt2BnoC_BuToppbarpi',
                          prescale=1,
                          min_twobody_mva=0.1,
                          min_threebody_mva=0.1):
    line_alg = BuTohhh.make_BuToppbarpi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


#Bc->hhh
@register_line_builder(hlt2_lines)
@configurable
def BcToKKK_hlt2_line(name='Hlt2BnoC_BcToKKK',
                      prescale=1,
                      min_twobody_mva=0.1,
                      min_threebody_mva=0.1):
    line_alg = BcTohhh.make_BcToKKK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BcToKpiK_hlt2_line(name='Hlt2BnoC_BcToKpiK',
                       prescale=1,
                       min_twobody_mva=0.1,
                       min_threebody_mva=0.1):
    line_alg = BcTohhh.make_BcToKpiK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BcToKpipi_hlt2_line(name='Hlt2BnoC_BcToKpipi',
                        prescale=1,
                        min_twobody_mva=0.1,
                        min_threebody_mva=0.1):
    line_alg = BcTohhh.make_BcToKpipi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BcTopipipi_hlt2_line(name='Hlt2BnoC_BcTopipipi',
                         prescale=1,
                         min_twobody_mva=0.1,
                         min_threebody_mva=0.1):
    line_alg = BcTohhh.make_BcTopipipi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BcToKpKppim_hlt2_line(name='Hlt2BnoC_BcToKpKppim',
                          prescale=1,
                          min_twobody_mva=0.1,
                          min_threebody_mva=0.1):
    line_alg = BcTohhh.make_BcToKpKppim(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BcTopippipKm_hlt2_line(name='Hlt2BnoC_BcTopippipKm',
                           prescale=1,
                           min_twobody_mva=0.1,
                           min_threebody_mva=0.1):
    line_alg = BcTohhh.make_BcTopippipKm(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BcToppbarK_hlt2_line(name='Hlt2BnoC_BcToppbarK',
                         prescale=1,
                         min_twobody_mva=0.1,
                         min_threebody_mva=0.1):
    line_alg = BcTohhh.make_BcToppbarK(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))


@register_line_builder(hlt2_lines)
@configurable
def BcToppbarpi_hlt2_line(name='Hlt2BnoC_BcToppbarpi',
                          prescale=1,
                          min_twobody_mva=0.1,
                          min_threebody_mva=0.1):
    line_alg = BcTohhh.make_BcToppbarpi(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=bnoc_prefilters(
            require_topo=True,
            require_GEC=True,
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva) + [line_alg],
        extra_outputs=Topo_prefilter_extra_outputs(
            min_twobody_mva=min_twobody_mva,
            min_threebody_mva=min_threebody_mva))
