###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builds for BNOC BdToppbarhh line
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Hlt2Conf.algorithms import ParticleContainersMerger

from RecoConf.reconstruction_objects import make_pvs
from PyConf import configurable

from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons,
                                         make_has_rich_long_protons)

import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner


@configurable
def filter_PionsforB2ppbarhh(make_particles,
                             make_pvs=make_pvs,
                             trchi2todof_max=3,
                             mipchi2_min=4,
                             tr_ghost_prob_max=0.4,
                             pt_min=300 * MeV,
                             p_min=1.5 * GeV):
    pvs = make_pvs()
    code = F.require_all(F.PT > pt_min, F.P > p_min,
                         F.CHI2DOF < trchi2todof_max,
                         F.GHOSTPROB < tr_ghost_prob_max,
                         F.MINIPCHI2(pvs) > mipchi2_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_PionsforB2ppbarhh(pi_pidk_max=5., p_min=1.5 * GeV, pt_min=300 * MeV):
    """Return pions filtered by thresholds set for this selection."""
    pions = filter_PionsforB2ppbarhh(
        make_particles=make_has_rich_long_pions, p_min=p_min, pt_min=pt_min)
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def filter_KaonsforB2ppbarhh(make_particles,
                             make_pvs=make_pvs,
                             trchi2todof_max=3,
                             mipchi2_min=6.0,
                             tr_ghost_prob_max=0.4,
                             pt_min=300 * MeV,
                             p_min=1.5 * GeV):
    pvs = make_pvs()
    code = F.require_all(F.PT > pt_min, F.P > p_min,
                         F.CHI2DOF < trchi2todof_max,
                         F.GHOSTPROB < tr_ghost_prob_max,
                         F.MINIPCHI2(pvs) > mipchi2_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_KaonsforB2ppbarhh(k_pidk_min=-5., p_min=1.5 * GeV, pt_min=300 * MeV):
    """Return kaons filtered by thresholds set for this selection."""
    kaons = filter_KaonsforB2ppbarhh(
        make_particles=make_has_rich_long_kaons, p_min=p_min, pt_min=pt_min)
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def filter_ProtonsforB2ppbarhh(make_particles,
                               make_pvs=make_pvs,
                               trchi2todof_max=3,
                               mipchi2_min=9,
                               tr_ghost_prob_max=0.4,
                               pt_min=300 * MeV,
                               p_min=1.5 * GeV):
    pvs = make_pvs()
    code = F.require_all(F.PT > pt_min, F.P > p_min,
                         F.CHI2DOF < trchi2todof_max,
                         F.GHOSTPROB < tr_ghost_prob_max,
                         F.MINIPCHI2(pvs) > mipchi2_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_ProtonsforB2ppbarhh(p_pidp_min=-5., p_min=1.5 * GeV,
                             pt_min=300 * MeV):
    """Return protons filtered by thresholds set for this selection."""
    protons = filter_ProtonsforB2ppbarhh(
        make_particles=make_has_rich_long_protons, p_min=p_min, pt_min=pt_min)
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def _make_B2ppbarhh(particles,
                    DecayDescriptor,
                    am_max_ppbar=5000.0 * MeV,
                    adoca_chi2_ppbar=20.0,
                    asum_PT_ppbar=750.0 * MeV,
                    asum_P_ppbar=7000.0 * MeV,
                    am_max_ppbarK=5600.0 * MeV,
                    adoca_chi2_ppbarK=20.0,
                    am_min_ppbarKpi=5050 * MeV,
                    am_max_ppbarKpi=5550 * MeV,
                    adoca_chi2_ppbarKpi=20.0,
                    amaxdoca4h=0.25 * mm,
                    comb_PTSUM_min=3000.0 * MeV,
                    B_dira_min=0.9999,
                    B_vtx_CHI2_max=25.0,
                    B_PT_min=1000.0 * MeV,
                    B_minip=0.2 * mm):

    combination12_code = F.require_all(
        F.MASS < am_max_ppbar,
        F.SUM(F.PT) > asum_PT_ppbar,
        F.SUM(F.P) > asum_P_ppbar,
        F.MAXDOCACHI2CUT(adoca_chi2_ppbar))  #cuts on the ppbar combination

    combination123_code = F.require_all(
        F.MASS < am_max_ppbarK,
        F.MAXDOCACHI2CUT(adoca_chi2_ppbarK))  #cuts on the ppbarK combination

    combination_code = F.require_all(
        in_range(am_min_ppbarKpi, F.MASS, am_max_ppbarKpi),
        F.MAXDOCACHI2CUT(
            adoca_chi2_ppbarKpi))  #cuts on the ppbarKpi combination

    pvs = make_pvs()

    vertex_code = F.require_all(
        in_range(am_min_ppbarKpi, F.MASS, am_max_ppbarKpi),
        F.BPVDIRA(pvs) > B_dira_min, F.CHI2DOF < B_vtx_CHI2_max,
        F.SUM(F.PT) > comb_PTSUM_min, F.PT > B_PT_min,
        F.MINIPCHI2(pvs) < B_minip)  #cuts on the B0

    return ParticleCombiner(
        particles,
        DecayDescriptor=DecayDescriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


''' for iteration on descriptors elements '''


@configurable
def make_B2ppbarhh(particles,
                   DecayDescriptor,
                   name='BNOCB2ppbarhhMerger',
                   **decay_arguments):
    assert len(DecayDescriptor) > 0
    b_hadrons = []
    for descriptor in DecayDescriptor:
        b_hadrons.append(
            _make_B2ppbarhh(
                particles=particles,
                DecayDescriptor=descriptor,
                **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)
