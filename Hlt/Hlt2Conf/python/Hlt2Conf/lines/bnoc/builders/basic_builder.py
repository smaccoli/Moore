###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of BnoC basic objects: pions, kaons, ...
"""
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ps

from RecoConf.reconstruction_objects import make_pvs

from PyConf import configurable
from Hlt2Conf.standard_particles import (
    make_long_pions,
    make_up_pions,  # used for FT
    make_has_rich_long_pions,
    make_has_rich_down_pions,
    make_has_rich_long_kaons,
    make_has_rich_long_protons,
    make_photons,
    make_resolved_pi0s,
    make_merged_pi0s,
    make_LambdaLL,
    make_LambdaDD,
    make_long_protons_for_V0,
    make_down_protons_for_V0,
    make_long_pions_for_V0,
    make_down_pions_for_V0)

import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Functors import require_all
from Hlt2Conf.lines.topological_b import make_filtered_topo_twobody, make_filtered_topo_threebody

####################################
# Track selections                 #
####################################


@configurable
def filter_particles(make_particles,
                     make_pvs=make_pvs,
                     trchi2todof_max=3,
                     mipchi2_min=4,
                     tr_ghost_prob_max=0.8,
                     pt_min=250 * MeV,
                     p_min=1 * GeV):
    pvs = make_pvs()
    code = F.require_all(F.PT > pt_min, F.P > p_min,
                         F.CHI2DOF < trchi2todof_max,
                         F.GHOSTPROB < tr_ghost_prob_max,
                         F.MINIPCHI2(pvs) > mipchi2_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_pions(pi_pidk_max=5., p_min=1 * GeV, pt_min=250 * MeV):
    """Return pions filtered by thresholds common to BnoC decay product selections."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions, p_min=p_min, pt_min=pt_min)
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


'''
for make_KS_DD
'''


@configurable
def make_down_pions(pi_pidk_max=5., p_min=1 * GeV, pt_min=250 * MeV):
    """Return pions filtered by thresholds common to BnoC decay product selections."""
    pions = filter_particles(
        make_particles=make_has_rich_down_pions, p_min=p_min, pt_min=pt_min)
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_tight_pions(pi_pidk_max=0.,
                     p_min=1.5 * GeV,
                     pt_min=500 * MeV,
                     mipchi2_min=9):
    """Return pions filtered by thresholds common to BnoC decay product selections."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min)
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_soft_pions(pi_pidk_max=0,
                    p_min=1.5 * GeV,
                    pt_min=200 * MeV,
                    tr_ghost_prob_max=0.8,
                    trchi2todof_max=3,
                    mipchi2_min=4):
    """Return accompanying pions filtered by thresholds common to BnoC very soft selections."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions,
        trchi2todof_max=trchi2todof_max,
        mipchi2_min=mipchi2_min,
        tr_ghost_prob_max=tr_ghost_prob_max,
        p_min=p_min,
        pt_min=pt_min)
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_kaons(k_pidk_min=-5., p_min=1 * GeV, pt_min=250 * MeV):
    """Return kaons filtered by thresholds common to BnoC decay product selections."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons, p_min=p_min, pt_min=pt_min)
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_tight_kaons(k_pidk_min=2.,
                     p_min=1.5 * GeV,
                     pt_min=500 * MeV,
                     mipchi2_min=9):
    """Return pions filtered by thresholds common to BnoC decay product selections."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min)
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_soft_kaons(k_pidk_min=0,
                    p_min=1.5 * GeV,
                    pt_min=200 * MeV,
                    tr_ghost_prob_max=0.8,
                    trchi2todof_max=3.,
                    mipchi2_min=4):
    """Return accompanying kaons filtered by thresholds common to BnoC very soft selections."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons,
        trchi2todof_max=trchi2todof_max,
        mipchi2_min=mipchi2_min,
        tr_ghost_prob_max=tr_ghost_prob_max,
        p_min=p_min,
        pt_min=pt_min)
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_protons(p_pidp_min=-5, p_min=1 * GeV, pt_min=250 * MeV):
    """Return protons filtered by thresholds common to BnoC decay product selections."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons, p_min=p_min, pt_min=pt_min)
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_tight_protons(p_pidp_min=0,
                       p_min=1.5 * GeV,
                       pt_min=500 * MeV,
                       mipchi2_min=9):
    """Return protons filtered by thresholds common to BnoC decay product selections."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min)
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_soft_protons(p_pidp_min=-10,
                      p_min=1 * GeV,
                      pt_min=100 * MeV,
                      tr_ghost_prob_max=0.8,
                      trchi2todof_max=3.,
                      mipchi2_min=4):
    """Return accompanying protons filtered by thresholds common to BnoC very soft selections."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons,
        trchi2todof_max=trchi2todof_max,
        mipchi2_min=mipchi2_min,
        tr_ghost_prob_max=tr_ghost_prob_max,
        p_min=p_min,
        pt_min=pt_min)
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


####################################
# Neutral objects selections       #
####################################


@configurable
def make_photons(make_particles=make_photons, CL_min=0.25, et_min=150 * MeV):
    """For the time being just a dummy selection"""

    # F.require_all(F.CL > CL_min=0.25, F.PT > et_min) once F.CL will be available
    code = F.PT > et_min
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_resolved_pi0s(make_particles=make_resolved_pi0s, pt_min=0 * MeV):
    """For the time being just a dummy selection"""

    code = F.PT > pt_min
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_merged_pi0s(make_particles=make_merged_pi0s,
                     p_min=0 * MeV,
                     pt_min=0 * MeV):
    """For the time being just a dummy selection"""

    code = require_all(F.PT > pt_min, F.P > p_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_detached_protons(p_min=2. * GeV, mipchi2_min=9.,
                          p_pidp_min=-2.):  #(F.PID_P > -2.)):
    """ 
    Return detached pions.
    """

    protons = filter_particles(
        make_particles=make_long_protons_for_V0,
        p_min=p_min,
        mipchi2_min=mipchi2_min)
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_detached_down_protons(p_min=2. * GeV, mipchi2_min=4.,
                               p_pidp_min=None):
    """ 
    Return downstream hadrons with proton mass hypothesis.
    """
    protons = filter_particles(
        make_particles=make_down_protons_for_V0,
        p_min=p_min,
        mipchi2_min=mipchi2_min)

    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_detached_pions(p_min=2. * GeV, mipchi2_min=9.,
                        pi_pidk_max=2.):  #(F.PID_K <= 2.)):
    """  
    Return detached pions.
    """
    pions = filter_particles(
        make_particles=make_long_pions_for_V0,
        p_min=p_min,
        mipchi2_min=mipchi2_min)
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_detached_down_pions(p_min=2. * GeV, mipchi2_min=4., pi_pidk_max=None):
    """  
    Return downstream hadrons with pion mass hypothesis.
    """
    pions = filter_particles(
        make_particles=make_down_pions_for_V0,
        p_min=p_min,
        mipchi2_min=mipchi2_min)
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


####################################
# ks, kstar0, ... 2-body decays    #
####################################
""" KS LL and DD already defined the same in B2OC module
so just import them from there unless we want to change something.
Moore will not compile if you have a repeat of the same algorithm
"""


#for ButoKSh
@configurable
def make_KS_LL(name='BNOCKSLLFilter_{hash}',
               make_pions=make_pions,
               make_pvs=make_pvs,
               pi_p_min=2 * GeV,
               p_min=8000.0 * MeV,
               pt_min=1000.0 * MeV,
               am_min=482.0 * MeV,
               am_max=512.0 * MeV,
               vchi2_max=10.0,
               bpvfdchi2_min=100.0):
    ''' 
    Builds LL Kshorts, currently corresponding to the Run2
    StdVeryLooseKSLL.
    '''
    pions = make_pions(p_min=pi_p_min)
    descriptor = '[KS0 -> pi+ pi-]cc'

    combination_code = in_range(am_min - 35, F.MASS, am_max + 35)

    pvs = make_pvs()
    vertex_code = F.require_all(F.P > p_min, F.PT > pt_min, F.CHI2 < vchi2_max,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    return ParticleCombiner([pions, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_KS_DD(name='BNOCKSDDFilter_{hash}',
               make_pions=make_down_pions,
               make_pvs=make_pvs,
               pi_p_min=2 * GeV,
               p_min=8000.0 * MeV,
               am_max=527.0 * MeV,
               am_min=467.0 * MeV,
               vchi2_max=10.0,
               bpvfdchi2_min=50.0,
               pt_min=1000.0 * MeV):
    '''
    Builds DD Kshorts, currently corresponding to the Run2
    StdLooseKSDD.
    '''
    pions = make_down_pions(p_min=pi_p_min)
    descriptor = '[KS0 -> pi+ pi-]cc'

    combination_code = in_range(am_min - 50, F.MASS, am_max + 50)

    pvs = make_pvs()

    vertex_code = F.require_all(F.P > p_min, F.PT > pt_min, F.CHI2 < vchi2_max,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    return ParticleCombiner([pions, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_kstar0(name='BnoCKstarCombiner_{hash}',
                make_pions=make_pions,
                make_kaons=make_kaons,
                make_pvs=make_pvs,
                am_min=742 * MeV,
                am_max=1042 * MeV,
                pi_p_min=2 * GeV,
                pi_pt_min=100 * MeV,
                k_p_min=2 * GeV,
                k_pt_min=100 * MeV,
                adoca12_max=0.5 * mm,
                asumpt_min=1000 * MeV,
                bpvvdchi2_min=16,
                vchi2pdof_max=16):
    '''
    Build Kstar0 candidates.
    '''

    pions = make_pions(p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = make_kaons(p_min=k_p_min, pt_min=k_pt_min)
    descriptor = '[K*(892)0 -> K+ pi-]cc'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min, F.MAXDOCACUT(adoca12_max))
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    return ParticleCombiner([kaons, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_wide_kstar0(name='BnoCWideKstarCombiner_{hash}',
                     make_pions=make_pions,
                     make_kaons=make_kaons,
                     make_pvs=make_pvs,
                     am_min=630 * MeV,
                     am_max=1600 * MeV,
                     pi_pidk=5,
                     pi_p_min=2 * GeV,
                     pi_pt_min=500 * MeV,
                     k_pidk_min=-5.,
                     k_p_min=2 * GeV,
                     k_pt_min=500 * MeV,
                     adoca12_max=0.5 * mm,
                     asumpt_min=1000 * MeV,
                     bpvvdchi2_min=16,
                     vchi2pdof_max=16,
                     motherpt_min=None):
    '''
    Build wide Kstar0 candidates.
    '''

    pions = make_pions(pi_pidk_max=pi_pidk, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = make_kaons(k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    descriptor = '[K*(892)0 -> K+ pi-]cc'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min, F.MAXDOCACUT(adoca12_max))
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    if motherpt_min is not None:
        vertex_code &= F.PT > motherpt_min
    return ParticleCombiner([kaons, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_phi(name='BnoCPhiCombiner_{hash}',
             make_kaons=make_kaons,
             make_pvs=make_pvs,
             am_min=990 * MeV,
             am_max=1050 * MeV,
             k_pidk_min=-5.,
             k_p_min=1 * GeV,
             k_pt_min=400 * MeV,
             asumpt_min=0 * MeV,
             bpvvdchi2_min=0,
             vchi2pdof_max=15,
             motherpt_min=None):
    '''
    Build phi candidates.
    '''

    kaons = make_kaons(k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    descriptor = '[phi(1020) -> K+ K-]cc'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min)
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    if motherpt_min is not None:
        vertex_code &= F.PT > motherpt_min
    return ParticleCombiner([kaons, kaons],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rho0(name='BnoCRhoCombiner_{hash}',
              make_pions=make_pions,
              make_pvs=make_pvs,
              am_min=400 * MeV,
              am_max=1500 * MeV,
              pi_p_min=2 * GeV,
              pi_pt_min=500 * MeV,
              adoca12_max=0.5 * mm,
              asump_min=1 * GeV,
              asumpt_min=900 * MeV,
              bpvvdchi2_min=25,
              vchi2pdof_max=9):
    '''
    Build Rho0 candidates.
    '''

    pions = make_pions(pi_pidk_max=None, p_min=pi_p_min, pt_min=pi_pt_min)
    descriptor = '[rho(770)0 -> pi+ pi-]cc'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.SUM(F.P) > asump_min, F.MAXDOCACUT(adoca12_max))
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    return ParticleCombiner([pions, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


######################
# select tracks for FT
######################


@configurable
def get_particles_WithPVs(make_particles=make_has_rich_long_pions,
                          make_pvs=make_pvs,
                          mipchi2_min=6):
    pvs = make_pvs()
    code = F.MINIPCHI2(pvs) > mipchi2_min
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def ft_longt_trks(mipchi2_min=0):
    return get_particles_WithPVs(make_long_pions, mipchi2_min=mipchi2_min)


@configurable
def ft_upstreamt_trks(mipchi2_min=0):
    return get_particles_WithPVs(make_up_pions, mipchi2_min=mipchi2_min)


@configurable
def ft_longall_trks():
    return get_particles_WithPVs(make_long_pions)


@configurable
def ft_upstreamall_trks():
    return get_particles_WithPVs(make_up_pions)


@configurable
def ft_extra_outputs(t_trk_mipchi2_min=0):
    LongT = ft_longt_trks(t_trk_mipchi2_min)
    UpstreamT = ft_upstreamt_trks(t_trk_mipchi2_min)
    LongALL = ft_longall_trks()
    UpstreamALL = ft_upstreamall_trks()
    return [("LongTracks", LongT), ("UpstreamTracks", UpstreamT),
            ("LongTracksOS", LongALL), ("UpstreamTracksOS", UpstreamALL)]


####################################
# lambda0  2-body decays           #
####################################
def make_lambda_LL(make_lambda=make_LambdaLL,
                   m_max=1135 * MeV,
                   m_min=1095 * MeV,
                   pt_min=550 * MeV,
                   p_min=1.0 * GeV,
                   vchi2dof_max=9.,
                   endvz_min=-100.0 * mm,
                   endvz_max=500.0 * mm):
    code = require_all(
        in_range(m_min, F.MASS, m_max), F.PT > pt_min, F.P > p_min,
        F.CHI2DOF < vchi2dof_max, in_range(endvz_min, F.END_VZ, endvz_max))
    return ParticleFilter(
        make_lambda(), F.FILTER(code), name="LambdaLLCombiner")


def make_lambda_DD(make_lambda=make_LambdaDD,
                   m_max=1140 * MeV,
                   m_min=1095 * MeV,
                   pt_min=500.0 * MeV,
                   p_min=1.0 * GeV,
                   vchi2dof_max=9.,
                   endvz_min=300.0 * mm,
                   endvz_max=2275.0 * mm):
    code = require_all(
        in_range(m_min, F.MASS, m_max), F.PT > pt_min, F.P > p_min,
        F.CHI2DOF < vchi2dof_max, in_range(endvz_min, F.END_VZ, endvz_max))
    return ParticleFilter(
        make_lambda(), F.FILTER(code), name="LambdaDDCombiner")


@configurable
def make_veryloose_lambda_LL(
        name="veryloose_lambda_LL",
        make_protons=make_detached_protons,
        make_pions=make_detached_pions,
        make_pvs=make_pvs,
        mass_window_comb_min=1065. * MeV,
        mass_window_comb_max=1165. * MeV,
        mass_window_min=1095. * MeV,
        mass_window_max=1135. * MeV,
        pi_p_min=2. * GeV,
        p_p_min=2. * GeV,
        p_pt_min=0.,  # recommended to be small
        pi_pt_min=0.,  # has to be 0 !!!
        pi_ipchi2_min=9.,
        p_ipchi2_min=9.,
        adocachi2cut=30.,
        bpvvdchi2_min=4.,
        bpvltime_min=1. * ps,
        vchi2pdof_max=15.,
        childcut_trchi2dof1=3.0,
        childcut_trchi2dof2=3.0,
        childcut_trghostprob1=0.5,
        childcut_trghostprob2=0.5,
):

    protons = make_protons(
        p_min=p_p_min, mipchi2_min=p_ipchi2_min,
        p_pidp_min=None)  # important to get rid of any PID cuts!
    pions = make_pions(
        p_min=pi_p_min, mipchi2_min=pi_ipchi2_min, pi_pidk_max=None)
    descriptor = '[Lambda0 -> p+ pi-]cc'
    combination_code = require_all(
        in_range(mass_window_comb_min, F.MASS, mass_window_comb_max),
        F.MAXDOCACHI2CUT(adocachi2cut))
    pvs = make_pvs()
    vertex_code = require_all(
        in_range(mass_window_min, F.MASS, mass_window_max),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.CHILD(1, F.CHI2DOF) < childcut_trchi2dof1,
        F.CHILD(2, F.CHI2DOF) < childcut_trchi2dof2,
        F.CHILD(1, F.GHOSTPROB) < childcut_trghostprob1,
        F.CHILD(2, F.GHOSTPROB) < childcut_trghostprob2)
    return ParticleCombiner([protons, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_loose_lambda_DD(
        name="loose_lambda_DD",
        make_protons=make_detached_down_protons,
        make_pions=make_detached_down_pions,
        make_pvs=make_pvs,
        mass_window_comb_min=1035. * MeV,
        mass_window_comb_max=1195. * MeV,
        mass_window_min=1090. * MeV,
        mass_window_max=1140. * MeV,
        pi_p_min=2. * GeV,
        p_p_min=2. * GeV,
        p_pt_min=0.,  # recommended to be small
        pi_pt_min=0.,  # has to be 0 !!!
        pi_ipchi2_min=4.,
        p_ipchi2_min=4.,
        adocachi2cut=25.,
        bpvltime_min=1. * ps,
        vchi2pdof_max=15.,
        vt_p_min=5000. * MeV,
        vt_bpvvdchi2_min=50.,
):

    protons = make_protons(
        p_min=p_p_min,
        #mipchi2_min=p_ipchi2_min,
        p_pidp_min=None)  # important to get rid of any PID cuts!
    pions = make_pions(
        p_min=pi_p_min,
        #mipchi2_min=pi_ipchi2_min,
        pi_pidk_max=None)
    descriptor = '[Lambda0 -> p+ pi-]cc'
    combination_code = require_all(
        in_range(mass_window_comb_min, F.MASS, mass_window_comb_max),
        F.MAXDOCACHI2CUT(adocachi2cut))
    pvs = make_pvs()
    vertex_code = require_all(
        in_range(mass_window_min, F.MASS, mass_window_max),
        F.P > vt_p_min,
        F.CHI2DOF < vchi2pdof_max,  #for run2, it's VFASPF(VCHI2)<15.0
        #F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVFDCHI2(pvs) > vt_bpvvdchi2_min)

    return ParticleCombiner([protons, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


###############################################################################
# Specific hyperon decay builders, overrides default cuts where needed        #
###############################################################################
@configurable
def make_xim_to_lambda0pim(lambdas,
                           pions,
                           comb_m_min=1248 * MeV,
                           comb_m_max=1396 * MeV,
                           comb_pt_min=500 * MeV,
                           m_min=1258 * MeV,
                           m_max=1386 * MeV,
                           pt_min=300 * MeV,
                           vchi2pdof_max=10.,
                           bpvfdchi2_min=10.):
    """ Make Xi- -> Lambda pi- using long pi-.  """
    # Todo, tune cuts for Xi- from b, default is from c
    pvs = make_pvs()
    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > comb_pt_min)
    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max), F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    return ParticleCombiner([lambdas, pions],
                            name="XimToLambdaPiCombiner_{hash}",
                            DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_omegam_to_lambda0km(lambdas,
                             kaons,
                             comb_m_min=1630 * MeV,
                             comb_m_max=1715 * MeV,
                             comb_pt_min=500 * MeV,
                             m_min=1640 * MeV,
                             m_max=1705 * MeV,
                             pt_min=100 * MeV,
                             vchi2pdof_max=5.,
                             bpvfdchi2_min=10.):
    """ Make Omega- -> Lambda K- from long tracks.  """
    # Todo, tune cuts for Omega- from b, default is from c
    pvs = make_pvs()
    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > comb_pt_min)
    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max), F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    return ParticleCombiner([lambdas, kaons],
                            name="OmmToLambdaKCombiner_{hash}",
                            DecayDescriptor="[Omega- -> Lambda0 K-]cc",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


################################
# select topo-prefiltered tracks
################################


@configurable
def Topo_prefilter_extra_outputs(min_twobody_mva=0.1, min_threebody_mva=0.1):
    return [("Topo_2Body", make_filtered_topo_twobody(MVACut=min_twobody_mva)),
            ("Topo_3Body",
             make_filtered_topo_threebody(MVACut=min_threebody_mva))]
