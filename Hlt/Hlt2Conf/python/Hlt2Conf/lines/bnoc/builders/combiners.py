###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of BnoC combiners
"""
from GaudiKernel.SystemOfUnits import MeV, mm, GeV, picosecond

from RecoConf.reconstruction_objects import make_pvs

from PyConf import configurable

import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleCombiner

######################################################
# Generic twobody/threebody/fourbody decay builders, #
# defines default combination cuts                   #
######################################################


@configurable
def make_twobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name='BnoCGenericTwoBodyCombiner_{hash}',
        make_pvs=make_pvs,  # if you want you can pass your own callable pv maker
        asumpt_min=1800 * MeV,
        adoca12_max=0.5 * mm,
        vchi2pdof_max=10,
        mipchi2_max=None,
        bpvvdchi2_min=36,
        bpvdira_min=0):
    """
    A generic 2body decay maker.

    2Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptor : string
        Decay descriptor to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.DOCA(1, 2) < adoca12_max)

    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                in_range(vtx_am_min, F.MASS, vtx_am_max))
    if mipchi2_max is not None:
        vertex_code &= F.MINIPCHI2(pvs) < mipchi2_max

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_phixtwobody(
        particles,
        descriptor,
        am_min,
        am_max,
        name='BnoCPhiXTwoBodyCombiner_{hash}',
        make_pvs=make_pvs,  # if you want you can pass your own callable pv maker
        ptproduct=1.2 * GeV * GeV,
        ltime_min=0.1 * picosecond,
        vchi2pdof_max=15):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        (F.CHILD(1, F.PT) * F.CHILD(2, F.PT)) > ptproduct)
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                in_range(am_min, F.MASS, am_max),
                                F.BPVLTIME(pvs) > ltime_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_threebody(particles,
                   descriptor,
                   am_min,
                   am_max,
                   vtx_am_min,
                   vtx_am_max,
                   name='BnoCGenericThreeBodyCombiner_{hash}',
                   make_pvs=make_pvs,
                   asumpt_min=1800 * MeV,
                   adoca12_max=0.5 * mm,
                   adoca13_max=0.5 * mm,
                   adoca23_max=0.5 * mm,
                   vchi2pdof_max=10,
                   bpvvdchi2_min=36,
                   bpvdira_min=0):
    """
    A generic 3body decay maker. Makes use of ThreeBodyCombiner
    to be more efficient, first making a DOCAcut on the *2 first particles in the
    decay descriptor*.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptor : string
        Decay descriptor to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    if adoca12_max is not None:
        combination12_code = F.DOCA(1, 2) < adoca12_max
    else:
        combination12_code = F.ALL
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min)
    if adoca13_max is not None:
        combination_code &= F.DOCA(1, 3) < adoca13_max
    if adoca23_max is not None:
        combination_code &= F.DOCA(2, 3) < adoca23_max
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                in_range(vtx_am_min, F.MASS, vtx_am_max))
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_fourbody(particles,
                  descriptor,
                  am_min,
                  am_max,
                  vtx_am_min,
                  vtx_am_max,
                  name='BnoCGenericFourBodyCombiner_{hash}',
                  make_pvs=make_pvs,
                  asumpt_min=1800 * MeV,
                  adoca12_max=0.5 * mm,
                  adoca13_max=0.5 * mm,
                  adoca14_max=0.5 * mm,
                  adoca23_max=0.5 * mm,
                  adoca24_max=0.5 * mm,
                  adoca34_max=0.5 * mm,
                  vchi2pdof_max=10,
                  bpvvdchi2_min=36,
                  bpvdira_min=0):
    """
    A generic 4body decay maker. Makes use of N4BodyCombinerWithPVs
    to be more efficient, first making a DOCAcut on the *2 first particles in the
    decay descriptor* and then on the *3 first particles in the decay descriptor*.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptor : string
        Decay descriptor to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination12_code = F.DOCA(1, 2) < adoca12_max
    combination123_code = F.require_all(
        F.DOCA(1, 3) < adoca13_max,
        F.DOCA(2, 3) < adoca23_max)
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > asumpt_min)
    if adoca14_max is not None:
        combination_code &= F.DOCA(1, 4) < adoca14_max
    if adoca24_max is not None:
        combination_code &= F.DOCA(2, 4) < adoca24_max
    if adoca34_max is not None:
        combination_code &= F.DOCA(3, 4) < adoca34_max
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                in_range(vtx_am_min, F.MASS, vtx_am_max))
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
