###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builds BNOC B decays, and defines the common default cuts applied to the B2X combinations
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond

from Hlt2Conf.algorithms import ParticleContainersMerger
from PyConf import configurable

from RecoConf.reconstruction_objects import make_pvs

import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all


@configurable
def _make_b2x(
        particles,
        descriptor,
        am_min=4500 * MeV,  #Beware TightCut!
        am_max=6300 * MeV,
        MassWindow=False,  #VV-AS inclusive mass window
        am123_max=1900. * MeV,
        am12_max=1900. * MeV,  # include D0
        am123_min=0. * MeV,  #should be at production threshold
        am12_min=0. * MeV,
        mothermass_min=4800 * MeV,
        mothermass_max=6100 * MeV,
        asumpt_min=1.5 * GeV,  #5 GeV???
        motherpt_min=250. * MeV,  # 1500 * MeV Run12
        achi2doca_max=30.,
        bpvipchi2_max=25.,
        dira_min=0.999,
        ltime_min=0.2 * picosecond,
        mipchi2_max=None,
        adoca_max=None,
        vtxchi2pdof_max=20.):

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min, F.MAXDOCACHI2CUT(achi2doca_max))
    combination123_code = F.require_all(F.MAXDOCACHI2CUT(achi2doca_max))
    combination12_code = F.require_all(F.MAXDOCACHI2CUT(achi2doca_max))
    if MassWindow:
        combination_code &= (F.SUBCOMB(Functor=in_range(am12_min, F.MASS, am12_max), Indices=[1,2]) &
                             F.SUBCOMB(Functor=in_range(am12_min, F.MASS, am12_max), Indices=[3,4])) | \
                             (F.SUBCOMB(Functor=in_range(am12_min, F.MASS, am12_max), Indices=[1,4]) &
                              F.SUBCOMB(Functor=in_range(am12_min, F.MASS, am12_max), Indices=[2,3])) | \
                             (F.SUBCOMB(Functor=in_range(am123_min, F.MASS, am123_max), Indices=[1,2,3])) | \
                             (F.SUBCOMB(Functor=in_range(am123_min, F.MASS, am123_max), Indices=[1,2,4])) | \
                             (F.SUBCOMB(Functor=in_range(am123_min, F.MASS, am123_max), Indices=[1,3,4])) | \
                             (F.SUBCOMB(Functor=in_range(am123_min, F.MASS, am123_max), Indices=[2,3,4]))
    else:
        combination123_code &= in_range(am123_min, F.MASS, am123_max)
        combination12_code &= in_range(am12_min, F.MASS, am12_max)

    pvs = make_pvs()
    vertex_code = F.require_all(
        in_range(mothermass_min, F.MASS, mothermass_max), F.PT > motherpt_min,
        F.CHI2DOF < vtxchi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > ltime_min,
        F.BPVDIRA(pvs) > dira_min)
    if mipchi2_max is not None:
        vertex_code &= F.MINIPCHI2(pvs) < mipchi2_max
    if adoca_max is not None:
        combination12_code &= F.DOCA(1, 2) < adoca_max
        combination123_code &= F.require_all(
            F.DOCA(1, 3) < adoca_max,
            F.DOCA(2, 3) < adoca_max)
        combination_code &= F.require_all(
            F.DOCA(1, 4) < adoca_max,
            F.DOCA(2, 4) < adoca_max,
            F.DOCA(3, 4) < adoca_max)

    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        Combination123Cut=combination123_code,
        Combination12Cut=combination12_code)


''' for iteration on descriptors elements '''


@configurable
def make_b2x(particles,
             descriptors,
             name='BNOCB2XMerger_{hash}',
             **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2x(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def _make_lifetime_unbiased_b2x(particles,
                                descriptor,
                                name='BNOCB2XLTUCombiner_{hash}',
                                am_min=5050 * MeV,
                                am_max=5650 * MeV,
                                am_min_vtx=5050 * MeV,
                                am_max_vtx=5650 * MeV,
                                sum_pt_min=5 * GeV,
                                vtx_chi2pdof_max=20.):  # was 10 in Run1+2
    '''
    LifeTime Unbiased B decay maker: defines default cuts and B mass range.
    '''
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)

    vertex_code = F.require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


''' for iteration on descriptors elements '''


@configurable
def make_lifetime_unbiased_b2x(particles,
                               descriptors,
                               name='BNOCB2XLTUMerger_{hash}',
                               **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_lifetime_unbiased_b2x(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def _make_b2hhh(
        particles,
        descriptor,
        name='BNOCB2HHHCombiner_{hash}',
        am_min=5050 * MeV,
        am_max=6300 * MeV,
        am_min_vtx=5050 * MeV,
        am_max_vtx=6300 * MeV,
        sum_pt_min=3 * GeV,
        FDCHI2=500,
        PVDOCAmin=3.0,
        IPCHI2_min=10,
        PT_min=1000.0 * MeV,
        PTsum=4500 * MeV,
        Psum=20000 * MeV,
        PVIPCHI2sum=500,
        TRKCHIDOFmin=3.0,
        CORRMmin=4000 * MeV,
        CORRMmax=7000 * MeV,
        vtx_chi2pdof_max=12,  # was 10 in Run1+2
        bpvipchi2_max=25,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.99998,
        adoca_max=0.2 * mm,
        probnnpi=0.15,  #PROBNNpi for pion tracks. WARNING: DO NOT APPLY THIS CUT FOR PIPIPI OR KPIPI LINES
        probnnk=0.20,
        probnnp=0.05):
    '''
    Specialised 3-body Hb --> (h h h)  decay maker
    '''

    combination12_code = F.require_all(F.DOCA(1, 2) < adoca_max)

    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min,
        F.SUM(F.MINIPCHI2(pvs)) > PVIPCHI2sum,
        F.MIN(F.CHI2DOF) < TRKCHIDOFmin,
        F.MIN(F.PT) > PT_min)

    vertex_code = F.require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx),
        in_range(CORRMmin, F.BPVCORRM(pvs), CORRMmax),
        F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVFDCHI2(pvs) > FDCHI2, F.MIN_ELEMENT @ F.ALLPV_FD(pvs) > PVDOCAmin,
        F.BPVFDCHI2(pvs) > FDCHI2,
        F.MINIPCHI2(pvs) < IPCHI2_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


''' for iteration on descriptors elements '''


@configurable
def make_b2hhh(particles,
               descriptors,
               name='BNOCB2HHHMerger_{hash}',
               **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2Lambdah(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def _make_b2Lambdah(particles,
                    descriptor,
                    name='BNOCb2LambdahCombiner_{hash}',
                    apt_min=1000.0 * MeV,
                    apt1_min=500.0 * MeV,
                    sumpt_min=800.0 * MeV,
                    sumpt_num_min=2,
                    am_min=4800. * MeV,
                    am_max=7000. * MeV,
                    acutdocachi2=5.0,
                    vt_pt=800.0 * MeV,
                    vt_vchi2pdof_max=12.,
                    vt_bpvdira_min=0.995,
                    mipchidv_max=15.,
                    bpvvdchi2=30.0):
    ''' 
    For B+->Lambda~0 p+, Bc+->Lambda~0 p+
    '''

    combination_code = require_all(F.PT > apt_min,
                                   F.CHILD(1, F.PT) > apt1_min,
                                   F.SUM(F.PT > sumpt_min) >= sumpt_num_min,
                                   in_range(am_min, F.MASS, am_max),
                                   F.MAXDOCACHI2CUT(acutdocachi2))

    pvs = make_pvs()

    vertex_code = require_all(F.PT > vt_pt,
                              F.BPVDIRA(pvs) > vt_bpvdira_min,
                              F.CHI2DOF < vt_vchi2pdof_max,
                              F.MINIPCHI2(pvs) < mipchidv_max,
                              F.BPVFDCHI2(pvs) > bpvvdchi2)
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_b2Lambdah(particles,
                   descriptors,
                   name='BNOCb2LambdahMerger_{hash}',
                   **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2Lambdah(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def _make_b2ksh(particles,
                descriptor,
                name='BNOCBC2KSHCombiner_{hash}',
                am_min=4800.0 * MeV,
                am_max=8000.0 * MeV,
                amed_pt_min=4000.0 * MeV,
                p_min=25000.0 * MeV,
                vchi2pdof_max=9.0,
                bpvdira_min=0.99,
                mipchi2_max=20.0,
                bpvfdchi2_min=20.0):

    med_pt_cut = ((F.CHILD(1, F.PT) + F.CHILD(2, F.PT)) > amed_pt_min)

    combination_code = F.require_all(
        in_range(am_min - 50 * MeV, F.MASS, am_max + 50 * MeV), med_pt_cut)
    pvs = make_pvs()

    vertex_code = F.require_all(F.P > p_min, in_range(am_min, F.MASS, am_max),
                                F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                F.MINIPCHI2(pvs) < mipchi2_max,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_b2ksh(particles,
               descriptors,
               name='BNOCBC2KSHMerger_{hash}',
               **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2ksh(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


#''' b hadrons builders using the make_b2x and make_b2hhh defined above '''


@configurable
def make_b2Kpi(particles,
               descriptor,
               name='BNOCb2KpiMerger_{hash}',
               comb_m_min=4000 * MeV,
               comb_m_max=6200 * MeV,
               comb_pt_min=5000 * MeV,
               mtdocachi2_max=10.0,
               pt_min=4000 * MeV):
    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > comb_pt_min,
    )

    pvs = make_pvs()
    composite_code = require_all(
        F.MTDOCACHI2(1, pvs) < mtdocachi2_max,
        F.PT > pt_min,
    )
    return ParticleCombiner(
        particles,
        ParticleCombiner="ParticleAdder",
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@configurable
def _make_b2hh(particles,
               descriptor,
               am_min=4700 * MeV,
               am_max=6200 * MeV,
               sum_pt=4500 * MeV,
               docachi2=9.0,
               pt_min=1200 * MeV,
               dira_min=0.99,
               ipchi2_max=9,
               fdchi2_min=100):
    pvs = make_pvs()
    combination_cut = require_all(
        F.SUM(F.PT) > sum_pt,
        in_range(am_min, F.MASS, am_max),
        F.MAXDOCACHI2CUT(docachi2),
    )
    composite_cut = require_all(
        F.PT > pt_min,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > fdchi2_min,
    )
    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut)
