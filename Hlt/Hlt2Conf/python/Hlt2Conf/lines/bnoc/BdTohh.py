###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of the B0 -> hh inclusive HLT2 line
"""
from GaudiKernel.SystemOfUnits import MeV
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.standard_particles import make_has_rich_long_pions
from Hlt2Conf.lines.bnoc.builders.basic_builder import filter_particles
from Hlt2Conf.lines.bnoc.builders import b_builder
from RecoConf.reconstruction_objects import make_pvs


@check_process
def make_BdTohh(process):
    pions = filter_particles(
        make_particles=make_has_rich_long_pions,
        make_pvs=make_pvs,
        mipchi2_min=16,
        pt_min=1000 * MeV,
        # unused cuts
        p_min=-999 * MeV,
        trchi2todof_max=999,
        tr_ghost_prob_max=999,
    )
    line_alg = b_builder._make_b2hh(
        particles=[pions, pions], descriptor='[B0 -> pi+ pi-]cc')
    return line_alg
