###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B0(s) -> hhhh inclusive HLT2 lines.
"""

from GaudiKernel.SystemOfUnits import MeV, GeV, mm

from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.basic_builder import make_soft_pions, make_soft_kaons
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2x

#aprox masses of particles: lower than true value because of detector resolution
m_K = 480. * MeV
m_pi = 120. * MeV

all_lines = {}

Bds_kwargs = {
    #"am_min": 4500 * MeV,
    #"am_max": 6800 * MeV,
    # "am123_max": 1869.65 * MeV,
    # "am12_max": 1864.83 * MeV,
    "MassWindow": True,
    #"mothermass_min": 4700 * MeV,
    #"mothermass_max": 6200 * MeV,
    #"asumpt_min": 1000. * MeV,
    "adoca_max": .5 * mm,
    "dira_min": 0.99995,
    #"ltime_min": 0.1 * picosecond
}


@check_process
@configurable
def make_BdsToPiPiPiPi(process):
    pions = make_soft_pions(pt_min=500 * MeV)
    Bds = make_b2x(
        particles=[pions, pions, pions, pions],
        descriptors=['B0 -> pi+ pi+ pi- pi-'],
        **Bds_kwargs,
        am12_min=2 * m_pi,
        am123_min=3 * m_pi)
    return Bds


@check_process
@configurable
def make_BdsToKPiPiPi(process):
    pions = make_soft_pions(pt_min=500 * MeV)
    kaons = make_soft_kaons(pt_min=500 * MeV)
    Bds = make_b2x(
        particles=[kaons, pions, pions, pions],
        descriptors=['[B0 -> K+ pi+ pi- pi-]cc'],
        **Bds_kwargs,
        am12_min=2 * m_pi,
        am123_min=3 * m_pi)
    return Bds


@check_process
@configurable
def make_BdsToKKPiPi(process):
    pions = make_soft_pions(pt_min=500 * MeV)
    kaons = make_soft_kaons(pt_min=500 * MeV)
    Bds = make_b2x(
        particles=[kaons, kaons, pions, pions],
        descriptors=['B0 -> K+ K- pi+ pi-'],
        **Bds_kwargs,
        am12_min=2 * m_pi,
        am123_min=2 * m_pi + m_K)
    return Bds


@check_process
@configurable
def make_BdsToKKKPi(process):
    pions = make_soft_pions(pt_min=500 * MeV)
    kaons = make_soft_kaons(pt_min=500 * MeV)
    Bds = make_b2x(
        particles=[kaons, kaons, kaons, pions],
        descriptors=['[B0 -> K+ K+ K- pi-]cc'],
        **Bds_kwargs,
        am12_min=m_pi + m_K,
        am123_min=m_pi + 2 * m_K)
    return Bds


@check_process
@configurable
def make_BdsToKKKK(process):
    kaons = make_soft_kaons(p_min=1. * GeV, pt_min=150 * MeV)
    Bds = make_b2x(
        particles=[kaons, kaons, kaons, kaons],
        descriptors=['B0 -> K+ K+ K- K-'],
        **Bds_kwargs,
        am12_min=2 * m_K,
        am123_min=3 * m_K)
    return Bds
