# BnoC Run 3 Migration Branch

Please push all BnoC HLT2 lines to this subdirectory and keep our [TWiki](https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/BnoCRealTimeAnalysis) updated on your progress.

## Structure of the package

The package has a simple structure inspired by work in the B2OC group.
There are a series of common objects built in `builders/basic_builders.py` (note these have slightly different cuts to the equivalent objects in the b2oc package and match what is most often used in BnoC).
There are also some wrappers to perform 2, 3 and 4-body combinations in `builders/combiners.py`.
There is a generic `builders/b_builder.py` but none of these methods are currently used anywhere so this may be retired.

## When you want to add a line

 - Call the actual function which will makes your candidates `make_<DecayName>()`
 - You can put this in a new file if you like but if it would fit better into one of the files that already exists then please use that (e.g. `BdsTohhhh.py` or `BdsToVV.py`)
 - The actual hlt2 lines are booked in `hlt2_bnoc.py`. This doesn't really do much but book the line with `@register_line` using the algorithm returned by your `make_<DecayName>()` function above.
 - If you want to run this as a sprucing line then instead you should book it in `spruce_bnoc.py`.

 - Rather than asserting in the `make_<DecayName>()` that the function must have the process `hlt2` or `spruce`, you can now use the `@check_process` decorator found in utils.py modelled after the B2OC group (e.g. see BdsToVV.py).
 - This decorator will probably be made centrally available some time soon, at which point the import statements should be redirected and utils.py can (probably) be removed.

## To run Moore and get some rate estimates or yield estimates

There are a variety of ways to do this and it will likely depend on what exactly you want to do but there are a lot moore (excuse the pun) details on the different ways to run and get rates / efficiencies here: https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/index.html and in particular https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hltefficiencychecker.html.

Some basic BnoC style implementation and checks are described below. The options files for running these live in `Moore/Hlt/Hlt2Conf/options/examples/bnoc`.

 1. **Get the LHCb software stack and compile it.**
 ```bash
 curl https://gitlab.cern.ch/rmatev/lb-stack-setup/raw/master/setup.py | python3 - stack
 cd stack
 make MooreAnalysis # you could just use make Moore here
 cd Moore
 git checkout bnoc_run3
 cd ..
 make MooreAnalysis # you could just use make Moore here
 ```
 I'm afraid you will need to recompile every time you make a change.
 The first compile takes a while (40mins - 1hr). Subsequent compiles should be fast.

 2. **WIP: Check HLT1 and HLT2 efficiencies / rates in one step**

 This is still a bit of a work-in-progress but soon will be the recommended way of runnnig BnoC checks. It should allow you to bypass steps 3. and 4. below and do everything in one step. Due to some recent additions to `MooreAnalysis` we can now run Hlt1 and Hlt2 together in one step, and produce an `MCDecayTree` tuple which contains the decisions of both. This is very useful and allows us to not only assess the efficiency of a given Hlt2 line with respect to what children can be reconstructed but also the efficiency with respect to what would already have been selected by Hlt1.

 **Please be aware that some samples are already Hlt1 Filtered so will give very different looking Hlt1 efficiencies than if they hadn't**

 In BnoC, we typically trigger at Hlt1 using the `TrackMVA` and `TwoTrackMVA` lines and so using this method we can compute the line efficiency with respect to `Hlt1TrackMVADecision || Hlt1TwoTrackMVADecision` or perhaps even better the equivalent TOS decision of our B candidate, e.g. `Bs_Hlt1TrackMVADecisionTOS || Bs_Hlt1TwoTrackMVADecisionTOS`.

 Some helper scripts have been provided to run this, which have now been merged with the `master` branch of `MooreAnalysis` (you may need to update your local `MooreAnalysis` repository).


 There is an example of how to run the chained Hlt1 and Hlt2 in `Moore/Hlt/Hlt2Conf/options/examples/bnoc/hlt1_and_hlt2_lines_bnoc.py`. As with the steps below you will need to modify this script for you own needs by changing the `DecayDescriptor` and the input files it runs on (you may also need to change `options.input_raw_format` to match your file type).

```bash
MooreAnalysis/run gaudirun.py Moore/Hlt/Hlt2Conf/options/examples/bnoc/hlt1_and_hlt2_lines_bnoc.py
```
This will produce an outputfile called `eff_ntuple_bnoc_hlt1_and_hlt2.root` which will contain the `MCDecayTree`, all Hlt1 line decisions and all of the Hlt2 line decisions you have specified to run in the `make_lines()` function (by default this runs all bnoc lines).

You can make use of a centralised script for the checking the efficiency from the `HltEfficiencyChecker` package. This now has the option for custom denominators if you want to assess the efficiency with respect to certain Hlt1 lines. An example of how this runs for the KstKst case:

```bash
MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py eff_ntuple_bnoc_hlt1_and_hlt2.root \
    --parent Bs --reconstructible-children Kp,Km,pip,pim \
    --lines Hlt2BnoC_BdsToKstzKstzbDecision --TOS-match-to Bs \
    --custom-denoms "Hlt1TrackMVAs:Hlt1TrackMVADecision || Hlt1TwoTrackMVADecision,Bs_Hlt1TrackMVAsTOS:Bs_Hlt1TrackMVADecisionTOS || Bs_Hlt1TwoTrackMVADecisionTOS" \
    --make-plots
```

Note, the `--custom-denoms` argument needs to be surrouned by quotes (as shown above). The structure is `"nickname1:conditions1,nickname2:conditions2..."`.

The Moore docs have a new section on using Hlt1 and Hlt2 together [here](https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hltefficiencychecker.html#running-a-combined-hlt1-and-hlt2). `hlt_line_efficiencies.py` also has many other useful arguments such as `--vars`, `--plot-prefix` and `--TOS-match-to` which you may wish to use.


You can now use the `HltEfficiencyChecker` package to chain Hlt1 and Hlt2 for the rates too.
To do this, you can use the options file in `Moore/Hlt/Hlt2Conf/options/examples/bnoc/hlt1_and_hlt2_rate_bnoc.py` to produce a rate tuple (run over MinBias MC) with Hlt1 and Hlt2 line decisions.

You can check the rates of your lines for Hlt1 and Hlt2 together like this:
 ```bash
MooreAnalysis/run gaudirun.py Moore/Hlt/Hlt2Conf/options/examples/bnoc/hlt1_and_hlt2_rate_bnoc.py
```
Again this will produce an ntuple, this time called `rate_ntuple_bnoc_hlt1_and_hlt2.root` and you can check the rate using
```bash
MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_calculate_rates.py --json Hlt1_and_Hlt2_rates.json rate_ntuple_bnoc_hlt1_and_hlt2.root
```

You should be aiming for this to have a rate of a few Hertz.

When running `hlt_calculate_rates.py`, there is now the helpful argument `--filter-lines` (again, see the new [docs](https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hltefficiencychecker.html#running-a-combined-hlt1-and-hlt2)):
> `hlt_calculate_rates.py` has the `--filter-lines` argument, which has a very similar effect to the `--custom-denoms` explained above. This one takes a comma-separated list of line names (no nicknames this time) which we calculate the rate with respect to (w.r.t.) i.e. at least one of these lines must pass in every considered event. This emulates the effect of e.g. HLT1 filtering out events before they reach your HLT2 lines.

So, in our case, we probably want to use `--filter-lines Hlt1TrackMVADecision,Hlt1TwoTrackMVADecision`.

 3. **Check your HLT1 efficiency.**
 I think this is a useful starting point as it gives you an idea of how efficient HLT1 is going to be for your decay. Note for BnoC we usually have fully hadronic final states and so the HLT1 lines of interested are the `TrackMVA` and `TwoTrackMVA`.

 To run HLT1 and check its efficiency on your signal MC then from the top level stack directory above you can run (clearly you will need to change the file such that the **input files** are the ones which are appropriate to your analysis / signal decay and also change the **Decay Descriptor** to match the signal decay in those files, you may also need to change `options.input_raw_format` to match your file type). Please also note that your input files may already be HLT1 filtered in which case this step is pointless and you can move on to step 3.
 ```bash
 MooreAnalysis/run gaudirun.py Moore/Hlt/Hlt2Conf/options/examples/bnoc/hlt1_lines_bnoc.py
 ```
 This will produce an ntuple called `eff_ntuple_bnoc_hlt1.root` which will contain the `MCDecayTree` and HLT1 line decisions which you can peruse to your  hearts content. There is a nice centralised script for checking on the efficiency from the `HltEfficiencyChecker` package:
 ```bash
 MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py eff_ntuple_bnoc_hlt1.root --level Hlt1 --parent <your_parent_branch_name> --reconstructible-children <comma_sep_list_of_children_branch_names> --make-plots --vars PT,TAU,ETA --denoms CanRecoChildren,CanRecoChildrenParentCut
 ```
 There are even more options for this and for the KstKstb checks I ran it exactly like this:
 ```bash
MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py Tuples/eff_ntuple_kstkst_hlt1.root --level Hlt1 --parent Bs --reconstructible-children Kp,Km,pip,pim --plot-prefix Plots/ --lines Hlt1TrackMVADecision,Hlt1TwoTrackMVADecision,Bs_Hlt1TrackMVADecisionTOS,Bs_Hlt1TwoTrackMVADecisionTOS --make-plots --vars PT,TAU,ETA --legend-header "B_{s}^{0} #rightarrow K^{*0}#bar{K}^{*0}" --denoms CanRecoChildren,CanRecoChildrenParentCut,CanRecoChildrenAndChildPt
```
This should then provide you with some text output and some plots which provide you with the TOS efficiency of HLT1. The denominator is important here and if the denominator is `CanRecoChildren` you may not get as high efficiency as you might naively hope for. For an approximate comparison with what is in the old TDRs for the `L0Hadron` efficiency you should use the efficiency with `CanRecoChildrenParentCut` in the denominator.

 4. **Check the HLT2 rate and efficiencies**
 The procedure here is almost identical to the one above. This time we just run the hlt2 lines. Notice that in this case you may want to speed thing up by only running a subset of the bnoc lines (by default this will run all booked bnoc lines).
 ```bash
 MooreAnalysis/run gaudirun.py Moore/Hlt/Hlt2Conf/options/examples/bnoc/hlt2_lines_bnoc.py
 ```
This will produce an ntuple called `eff_ntuple_bnoc_hlt2.root` which will contain the `MCDecayTree` and HLT2 line decisions which you can peruse at your leisure. Note that this code will save all events that find a candidate for MCDecayTree so if you run on the same number of events in this step as in Step 2 you will be able to match events and figure out which ones passed HLT1 and thus can get the HLT2 efficiency given HLT1 (if you have a script for this please share it around). Ideally this functionality would be included in the centralised code but it is not yet available.

As before you can now get the efficiency using:
```bash
 MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py eff_ntuple_bnoc_hlt2.root --level Hlt2 --parent <your_parent_branch_name> --reconstructible-children <comma_sep_list_of_children_branch_names> --make-plots --vars PT,TAU,ETA --denoms CanRecoChildren,CanRecoChildrenParentCut
 ```

 To check the rate of your line then it needs to run on MinBias MC. That can be done like this:
 ```bash
MooreAnalysis/run gaudirun.py Moore/Hlt/Hlt2Conf/options/examples/bnoc/hlt2_rate_bnoc.py
```
Again this will produce an ntuple, this time called `rate_ntuple_bnoc_hlt2.root` and you can check the rate using
```bash
MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_calculate_rates.py --using-hlt1-filtered-MC --json Hlt2_rates.json rate_ntuple_bnoc_hlt2.root
```

You should be aiming for this to have a rate of a few Hertz.

## Other considerations

It maybe that what is most beneficial for the group is to run a series of inclusive BdsTohhhh lines which are then spruced by your specific code. You can check what the outcome of this would be by running both lines together and then in the output tuple seeing how your specific line fairs relative to what has already been triggered by BdsTohhhh.

