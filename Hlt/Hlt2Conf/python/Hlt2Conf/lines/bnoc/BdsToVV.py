###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B0(s) -> hhhh HLT2 lines.
"""

from GaudiKernel.SystemOfUnits import MeV, mm, GeV, picosecond

from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.bnoc.builders.basic_builder import make_wide_kstar0, make_phi, make_rho0, make_tight_kaons, make_tight_pions
from Hlt2Conf.lines.bnoc.builders.combiners import make_twobody, make_phixtwobody
from Hlt2Conf.lines.bnoc.utils import check_process


@configurable
def _make_BdsToPhiPhi(
        particles,
        descriptor,
        am_min=5000 * MeV,
        am_max=6000 * MeV,
        make_pvs=make_pvs,
        ptproduct=1.5 * GeV * GeV,
        ltime_min=0.2 * picosecond,
        vchi2pdof_max=15,
):

    return make_phixtwobody(
        particles,
        descriptor,
        am_min,
        am_max,
        name="BdsToPhiPhiCombiner",
        make_pvs=make_pvs,
        ptproduct=ptproduct,
        vchi2pdof_max=vchi2pdof_max,
    )


@check_process
@configurable
def make_BdsToPhiPhi(process):
    phi = make_phi(am_min=995 * MeV, am_max=1045 * MeV)
    line_alg = _make_BdsToPhiPhi(
        particles=[phi, phi],
        make_pvs=make_pvs,
        descriptor="[B_s0 -> phi(1020) phi(1020) ]cc")
    return line_alg


@configurable
def _make_BdsToKstzKstzb(particles,
                         descriptor,
                         am_min=4600 * MeV,
                         am_max=6000 * MeV,
                         vtx_am_min=4650 * MeV,
                         vtx_am_max=5950 * MeV,
                         make_pvs=make_pvs,
                         asumpt_min=5000. * MeV,
                         adoca12_max=0.3 * mm,
                         vchi2pdof_max=15,
                         mipchi2_max=25,
                         bpvvdchi2_min=81,
                         bpvdira_min=0.99):

    return make_twobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name="BdsToKstzKstzbCombiner",
        make_pvs=make_pvs,
        asumpt_min=asumpt_min,
        adoca12_max=adoca12_max,
        vchi2pdof_max=vchi2pdof_max,
        mipchi2_max=mipchi2_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min)


@check_process
@configurable
def make_BdsToKstzKstzb(process):
    kstar = make_wide_kstar0(
        make_kaons=make_tight_kaons, motherpt_min=900 * MeV)
    line_alg = _make_BdsToKstzKstzb(
        particles=[kstar, kstar],
        make_pvs=make_pvs,
        descriptor='B0 -> K*(892)0 K*(892)~0')
    return line_alg


@configurable
def _make_BdsToKstzPhi(particles,
                       descriptor,
                       am_min=5000 * MeV,
                       am_max=6000 * MeV,
                       vtx_am_min=4550 * MeV,
                       vtx_am_max=6750 * MeV,
                       make_pvs=make_pvs,
                       asumpt_min=1500 * MeV,
                       adoca12_max=0.3 * mm,
                       vchi2pdof_max=15,
                       mipchi2_max=25,
                       bpvdira_min=0.99):

    return make_twobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name="BdsToKstzPhiCombiner",
        make_pvs=make_pvs,
        adoca12_max=adoca12_max,
        vchi2pdof_max=vchi2pdof_max,
        mipchi2_max=mipchi2_max,
        bpvdira_min=bpvdira_min)


@check_process
@configurable
def make_BdsToKstzPhi(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV)
    phi = make_phi(
        make_kaons=make_tight_kaons,
        k_pidk_min=-5.,
        k_p_min=1.5 * GeV,
        k_pt_min=500 * MeV,
        asumpt_min=500 * MeV,
        vchi2pdof_max=9)
    line_alg = _make_BdsToKstzPhi(
        particles=[phi, kstar],
        make_pvs=make_pvs,
        descriptor='[B0 -> phi(1020) K*(892)0]cc')
    return line_alg


@configurable
def _make_BdsToKstzRho(
        particles,
        descriptor,
        am_min=4800 * MeV,
        am_max=5600 * MeV,
        vtx_am_min=4850 * MeV,
        vtx_am_max=5550 * MeV,
        make_pvs=make_pvs,
        asumpt_min=5000. * MeV,
        adoca12_max=0.3 * mm,
        vchi2pdof_max=9,
        mipchi2_max=20,  #Maybe two-body should include mipchi2 cuts?
        bpvdira_min=0.9999):

    return make_twobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name="BdsToKstzRhoCombiner",
        make_pvs=make_pvs,
        asumpt_min=asumpt_min,
        adoca12_max=adoca12_max,
        vchi2pdof_max=vchi2pdof_max,
        mipchi2_max=mipchi2_max,
        bpvdira_min=bpvdira_min)


@check_process
@configurable
def make_BdsToKstzRho(process):
    kstar = make_wide_kstar0(
        pi_pt_min=500 * MeV,
        k_pidk_min=0.,
        k_pt_min=500 * MeV,
        vchi2pdof_max=9,
        motherpt_min=900 * MeV)
    rho = make_rho0()
    line_alg = _make_BdsToKstzRho(
        particles=[kstar, rho],
        make_pvs=make_pvs,
        descriptor='[B0 -> K*(892)~0 rho(770)0]cc')
    return line_alg
