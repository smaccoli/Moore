###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of BnoC sprucing lines. This is where sprucing lines are actually booked
"""
#from Moore.config import SpruceLine, register_line_builder

#from PyConf import configurable

#from Hlt2Conf.lines.bnoc.prefilters import bnoc_prefilters

PROCESS = 'spruce'
sprucing_lines = {}
