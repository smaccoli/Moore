###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC BdToppbarhh lines
"""

from GaudiKernel.SystemOfUnits import MeV, mm

from PyConf import configurable
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.B_ppbarhh_builder import make_B2ppbarhh, make_PionsforB2ppbarhh, make_KaonsforB2ppbarhh, make_ProtonsforB2ppbarhh

B0_kwargs = {
    "am_max_ppbar": 5000 * MeV,
    "adoca_chi2_ppbar": 20.0,
    "asum_PT_ppbar": 750 * MeV,
    "asum_P_ppbar": 7000 * MeV,
    "am_max_ppbarK": 5600 * MeV,
    "adoca_chi2_ppbarK": 20.,
    "am_min_ppbarKpi": 5050. * MeV,
    "am_max_ppbarKpi": 5550. * MeV,
    "adoca_chi2_ppbarKpi": 20.0,
    "amaxdoca4h": 0.25 * mm,
    "comb_PTSUM_min": 3000 * MeV,
    "B_dira_min": 0.9999,
    "B_vtx_CHI2_max": 25.0,
    "B_PT_min": 1000 * MeV,
    "B_minip": 0.2 * mm
}


@check_process
@configurable
def make_Bd0ToPPbarKPi(process):
    pions = make_PionsforB2ppbarhh()
    kaons = make_KaonsforB2ppbarhh()
    protons = make_ProtonsforB2ppbarhh()
    Bd0 = make_B2ppbarhh(
        particles=[protons, protons, kaons, pions],
        DecayDescriptor=["[B0 -> p+ p~- K+ pi-]cc"],
        **B0_kwargs)
    return Bd0


@check_process
@configurable
def make_Bd0ToPPbarPiPi(process):
    pions = make_PionsforB2ppbarhh()
    protons = make_ProtonsforB2ppbarhh()
    Bd0 = make_B2ppbarhh(
        particles=[protons, protons, pions, pions],
        DecayDescriptor=["[B0 -> p+ p~- pi+ pi-]cc"],
        **B0_kwargs)
    return Bd0


@check_process
@configurable
def make_Bd0ToPPbarKK(process):

    kaons = make_KaonsforB2ppbarhh()
    protons = make_ProtonsforB2ppbarhh()
    Bd0 = make_B2ppbarhh(
        particles=[protons, protons, kaons, kaons],
        DecayDescriptor=["[B0 -> p+ p~- K+ K-]cc"],
        **B0_kwargs)
    return Bd0
