###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC BTohhh lines
"""
from Hlt2Conf.lines.bnoc.builders import basic_builder
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders import b_builder

##############################################
# BuTohhh lines
##############################################


@check_process
def make_BuToKKK(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons()
    line_alg = b_builder.make_b2hhh(
        particles=[kaons, kaons, kaons], descriptors=['[B+ -> K+ K+ K-]cc'])
    return line_alg


@check_process
def make_BuToKpiK(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons()
        pions = basic_builder.make_soft_pions()
    line_alg = b_builder.make_b2hhh(
        particles=[kaons, pions, kaons], descriptors=['[B+ -> K+ pi+ K-]cc'])
    return line_alg


@check_process
def make_BuToKpipi(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=2.)
        pions = basic_builder.make_soft_pions(pi_pidk_max=0.)
    line_alg = b_builder.make_b2hhh(
        particles=[kaons, pions, pions], descriptors=['[B+ -> K+ pi+ pi-]cc'])
    return line_alg


@check_process
def make_BuTopipipi(process):
    if process == 'spruce':
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = basic_builder.make_soft_pions()
    line_alg = b_builder.make_b2hhh(
        particles=[pions, pions, pions], descriptors=['[B+ -> pi+ pi+ pi-]cc'])
    return line_alg


@check_process
def make_BuToKpKppim(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons()
        pions = basic_builder.make_soft_pions()
    line_alg = b_builder.make_b2hhh(
        particles=[kaons, kaons, pions], descriptors=['[B+ -> K+ K+ pi-]cc'])
    return line_alg


@check_process
def make_BuTopippipKm(process):
    if process == 'spruce':
        kaons = basic_builder.make_soft_kaons(k_pidk_min=None)
        pions = basic_builder.make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_soft_kaons()
        pions = basic_builder.make_soft_pions()
    line_alg = b_builder.make_b2hhh(
        particles=[pions, pions, kaons], descriptors=['[B+ -> pi+ pi+ K-]cc'])
    return line_alg
