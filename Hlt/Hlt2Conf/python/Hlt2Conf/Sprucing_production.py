###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import Options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import upfront_reconstruction
from Hlt2Conf.lines import sprucing_lines, all_lines
from Hlt2Conf.lines.test.spruce_test import Test_sprucing_line, Test_extraoutputs_sprucing_line, Test_persistreco_sprucing_line
from Hlt2Conf.lines.b_to_open_charm import b_to_dh
from Moore.lines import PassLine, SpruceLine
from PyConf.application import metainfo_repos
from pprint import pprint

from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as b2oc_lines
from Hlt2Conf.lines.bandq import sprucing_lines as bandq_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as sl_lines
from Hlt2Conf.lines.bnoc import sprucing_lines as bnoc_lines
from Hlt2Conf.lines.rd import sprucing_lines as rd_lines
from Hlt2Conf.lines.qee import sprucing_lines as qee_lines

from Hlt2Conf.lines.b_to_charmonia import all_lines as hlt2_b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import all_lines as hlt2_b2oc_lines
from Hlt2Conf.lines.bandq import all_lines as hlt2_bandq_lines
from Hlt2Conf.lines.charm import all_lines as hlt2_charm_lines
from Hlt2Conf.lines.qee import all_lines as hlt2_qee_lines
from Hlt2Conf.lines.rd import all_lines as hlt2_rd_lines
from Hlt2Conf.lines.semileptonic import all_lines as hlt2_sl_lines
from Hlt2Conf.lines.bnoc import all_lines as hlt2_bnoc_lines

from Moore import streams_spruce
public_tools = [stateProvider_with_simplified_geom()]


def spruce_streaming(options: Options):
    """
    Test streaming of sprucing lines. Produces two spruce_streaming.dst files prepended by stream name
    """

    def make_streams():
        linedict = dict(
            test_stream_A=[
                Test_sprucing_line(name="Spruce_test_stream_A_line")
            ],
            test_stream_B=[
                Test_extraoutputs_sprucing_line(
                    name="Spruce_test_stream_B_line")
            ])
        pprint(linedict)
        return linedict

    ###Custom rawbanks not implemented for sprucing yet
    # Modify stream dictionaries for testing purposes
    streams_spruce.stream_banks = {
        "test_stream_A": [
            'ODIN', 'VP', 'UT', 'FTCluster', 'Rich', 'EcalPacked',
            'HcalPacked', 'Muon'
        ],
        "test_stream_B":
        ['ODIN', 'VP', 'UT', 'FTCluster', 'EcalPacked', 'HcalPacked',
         'Muon']  # Remove RICH raw bank
    }

    print("streams_spruce.stream_banks")
    pprint(streams_spruce.stream_banks)

    config = run_moore(options, make_streams, public_tools)
    return config


def spruce_all_lines_realtime(options: Options):
    """Test running all Sprucing lines on topo{2,3} persistreco hlt2 output (use real time reco). Produces spruce_all_lines_realtimereco_newPacking.dst

    Run over HLT1 filtered Min bias sample that has been processed by TOPO{2, 3} HLT2 lines.
    To produce this see `Hlt/Hlt2Conf/options/Sprucing/hlt2_2or3bodytopo_realtime.py`
    and then in the readback application  configure the GitANNSvc to use the branch key-43a25419
    """

    metainfo_repos.global_bind(extra_central_tags=['key-5b3d0522'])

    def make_lines():
        return [builder() for builder in sprucing_lines.values()]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_example_realtime(options: Options):
    """
    Test running Sprucing line on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_example_realtimereco.dst
    """

    def make_lines():
        return [
            Test_sprucing_line(name="Spruce_Test_line"),
        ]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_passthrough(options: Options):
    """Test pass-through Sprucing line. Produces spruce_passthrough.dst
    Run like any other options file:
    ./Moore/run gaudirun.py spruce_passthrough.py
    """

    def pass_through_line(name="PassThrough"):
        """Return a Sprucing line that performs no selection
        """
        return PassLine(name=name)

    def make_lines():
        return [pass_through_line()]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_hlt2filter(options: Options):
    """Test HLT2 filters on Sprucing lines. Runs on `hlt2_2or3bodytopo_realtime.mdf` from `hlt2_2or3bodytopo_realtime.py`

    Run like any other options file:

    ./Moore/run gaudirun.py spruce_hlt2filter.py

    """

    def Sprucing_line_1(name='Spruce_filteronTopo2'):
        line_alg = b_to_dh.make_BdToDsmK_DsmToKpKmPim(process='spruce')
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [line_alg],
            #hlt1_filter_code="HLT_PASS_RE('Hlt1GECPassthroughDecision')",
            hlt2_filter_code="Hlt2Topo2BodyDecision")

    def Sprucing_line_2(name='Spruce_filteronTopo3'):
        line_alg = b_to_dh.make_BdToDsmK_DsmToHHH_FEST(process='spruce')
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [line_alg],
            #hlt1_filter_code="HLT_PASS_RE('Hlt1PassthroughDecision')",
            hlt2_filter_code="Hlt2Topo3BodyDecision")

    def make_lines():
        return [Sprucing_line_1(), Sprucing_line_2()]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_example_realtime_extraoutputs(options: Options):
    """
    Test Sprucing line with `extra_outputs` on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_realtimereco_extraoutputs.dst
    """

    def make_lines():
        return [
            Test_extraoutputs_sprucing_line(
                name="Spruce_Test_line_extraoutputs"),
        ]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_example_realtime_persistreco(options: Options):
    """Test Sprucing line with `persistreco` on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_realtimereco_persistreco.dst
    """

    def make_lines():
        return [
            Test_persistreco_sprucing_line(name="Spruce_Test_line_persistreco")
        ]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_all_lines_realtime_test_old_json(options: Options):
    """Test running all Sprucing lines on topo{2,3} persistreco hlt2 output (use real time reco). Produces spruce_all_lines_realtimereco_newPacking.dst

    Use this for rate tests

    Run like any other options file:

    ./Moore/run gaudirun.py spruce_all_lines_realtime.py
    """

    def make_lines():
        return [builder() for builder in sprucing_lines.values()]

    config = run_moore(options, make_lines, public_tools)
    return config


###################Options for 2022 data###################


def lines_running(linedict):
    """ Return the full list of lines to be run"""

    return [
        item for sublist in [list(linedict[wg].keys()) for wg in linedict]
        for item in sublist
    ]


def lines_not_running(all_lines, lines_to_run):
    """Return the list of lines that are declared in the framework but that are not set to run"""
    return [item for item in list(all_lines) if item not in lines_to_run]


def excl_spruce_production(options: Options):

    ##Should only need to change the following dictionary.
    # Note that wg_lines are themselves dict types
    streams_spruce.streams = [
        "default",
        "test",
        "b2oc",
        "bandq",
        "sl",
        "b2cc",
        "charm",
        "qee",
        "rd",
        "bnoc",
    ]
    ##Deliberately leave out Rich bank
    streams_spruce.stream_banks = {
        key: [
            "ODIN",
            "VP",
            "UT",
            "FTCluster",
            "EcalPacked",
            "HcalPacked",
            "Muon",
        ]
        for key in streams_spruce.streams
    }

    linedict = {
        "b2oc": b2oc_lines,
        "bandq": bandq_lines,
        "b2cc": b2cc_lines,
        "rd": rd_lines,
        "sl": sl_lines,
        "qee": qee_lines,
        "bnoc": bnoc_lines,
    }

    linedict["qee"].pop("SpruceQEE_WGammaToEPhoton")

    lines_to_run = lines_running(linedict)
    missing_lines = lines_not_running(sprucing_lines, lines_to_run)

    def make_streams():
        streamdict = {
            wg: [builder() for builder in linedict[wg].values()]
            for wg in linedict
        }
        return streamdict

    print(f"Running {len(lines_to_run)} lines")
    for wg in linedict.keys():
        lines = list(linedict[wg].keys())
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )

    config = run_moore(options, make_streams, public_tools=[])
    return config


def pass_spruce_production(options: Options):

    ##Should only need to change the following dictionary.
    # Note that wg_lines are themselves dict types

    streams_spruce.streams = [
        "default",
        "test",
        "b2oc",
        "bandq",
        "sl",
        "b2cc",
        "charm",
        "qee",
        "rd",
        "bnoc",
    ]
    ##Deliberately leave out Rich bank
    streams_spruce.stream_banks = {
        key: [
            "ODIN",
            "VP",
            "UT",
            "FTCluster",
            "EcalPacked",
            "HcalPacked",
            "Muon",
        ]
        for key in streams_spruce.streams
    }

    linedict = {
        "b2oc": hlt2_b2oc_lines,
        "bandq": hlt2_bandq_lines,
        "b2cc": hlt2_b2cc_lines,
        "sl": hlt2_sl_lines,
        "charm": hlt2_charm_lines,
        "qee": hlt2_qee_lines,
        "rd": hlt2_rd_lines,
        "bnoc": hlt2_bnoc_lines,
    }

    lineregex = {
        wg: [line + "Decision" for line in list(linedict[wg].keys())]
        for wg in linedict
    }

    lines_to_run = lines_running(linedict)
    missing_lines = lines_not_running(all_lines, lines_to_run)

    def make_streams():

        streamdict = {
            wg: [PassLine(
                name="Pass" + wg,
                hlt2_filter_code=lineregex[wg],
            )]
            for wg in linedict
        }

        return streamdict

    for wg in linedict.keys():
        lines = lineregex[wg]
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )

    config = run_moore(options, make_streams, public_tools=[])
    return config
