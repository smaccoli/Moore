###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Maker functions for Particle definitions common across HLT2.

The Run 2 code makes the sensible choice of creating Particle objects first,
and then filtering these with FilterDesktop instances. Because the
FunctionalParticleMaker can apply  ThOr predicates directly to Track and
ProtoParticle objects, we just do the one step.

Implemented filters and builders:
- get_{all, long, down, upstream, ttrack}_track_selector : selectors of specific track type containers
- standard_protoparticle_filter : basic protoparticle filter
- _make_particles : basic particle maker
- _make_{ , all, long}_ChargedBasics : basic maker using Proto2ChargedBasic
- make_long_cb_{electrons, muons, pions, kaons, protons} : maker of long ChargedBasic particles per type
- make_has_rich_long_cb_{pions, kaons} : maker of long ChargedBasic particles per type requiring RICH
- make_all_cb_{electrons, muons, pions, kaons, protons} :
- make_photons : basic builder for photons
- make_long_electrons_{no, with}_brem : basic builders for long electrons with or without brem corrections
- make_long_{muons, pions, kaons, protons} : basic builder for long muons, pions, kaons, protons
- make_ismuon_long_muon : basic builder for long muons with ISMUON condition
- make_{up, down}_{electrons_no_brem, muons, pions, kaons, protons} : basic builders for upstream or downstream electrons (no brem correction), muons, pions, kaons, protons
- make_ttrack_{pions, protons} : basic builders for ttrack protons or pions
- make_has_rich_{long, down, up}_{pions, kaons, protons} : basic builders for longstream, downstream and upstream pions, kaons, protons
- make_has_rich_ttrack_{pions, protons} : basic builders for ttrack pions, protons
- make_{resolved, merged}_pi0s : basic builder to make resolved and merged pi0
- filter_leptons_loose : basic filter for preselection of leptons
- _make_dielectron_with_brem :
- make_detached_{dielectron, mue, mumu} : detached dilepton builded
- make_detached_{dielectron, mue}_with_brem : detached dilepton builder with included brem correction
- make_dimuon_base : basic maker for dimuon combination
- make_mass_constrained_jpsi2mumu : JpsiToMuMu builder using dimuon_base
- make_phi2kk : basic builder for PhiToKK
- _make_{long, down, up}_for_V0 : basic builder base for V0 combination basic particles
- make_{long, down, up}_{pions, kaons, protons}_for_V0 : builders of particles for V0
- _make_V0{LL, DD, LD_UL} : basic builder for V0 particles
- make_Ks{LL, DD, LD, UL} : builder of KS for LL, DD, LD, UL combinations
- make_Ks{LL, DD}_fromSV : builder for KS originating from SV
- make_Lambda{LL, DD} : builder for Lambda0, LL and DD combination


TO DO:
- port LoKi-based track selectors to a proper ThOr-based track selectors

"""
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond

from PyConf.Algorithms import (
    FunctionalParticleMaker, LHCb__Phys__ParticleMakers__PhotonMaker as
    PhotonMaker, LHCb__Phys__ParticleMakers__MergedPi0Maker as MergedPi0Maker,
    Proto2ChargedBasic, ParticleWithBremMaker, FunctionalDiElectronMaker)
from PyConf import configurable
from RecoConf.reconstruction_objects import (
    make_pvs, make_charged_protoparticles as _make_charged_protoparticles,
    make_neutral_protoparticles as _make_neutral_protoparticles,
    make_upstream_charged_protoparticles as
    _make_upstream_charged_protoparticles)

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
import Functors as F
from Functors.math import in_range

from RecoConf.core_algorithms import make_unique_id_generator
from RecoConf.ttrack_selections_reco import make_ttrack_protoparticles

masses = {
    'pi0': 134.9768 *
    MeV,  # +/- 0.0005 PDG, P.A. Zyla et al. (Particle Data Group), Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
    'KS0': 497.611 * MeV,  # +/- 0.013, PDG, PR D98, 030001 and 2019 update
    'Lambda0':
    1115.683 * MeV,  # +/- 0.006, PDG, PR D98, 030001 and 2019 update
    'J/psi(1S)': 3096.900 *
    MeV,  # +/- 0.006 PDG, P.A. Zyla et al. (Particle Data Group), Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update.
}


# Basic tracks selectors
@configurable
def get_all_track_selector(Code=F.ALL):
    return Code


@configurable
def get_long_track_selector(Code=None):
    if Code: return F.require_all(F.TRACKISLONG, Code)
    return F.TRACKISLONG


@configurable
def get_down_track_selector(Code=None):
    if Code: return F.require_all(F.TRACKISDOWNSTREAM, Code)
    return F.TRACKISDOWNSTREAM


@configurable
def get_upstream_track_selector(Code=None):
    if Code: return F.require_all(F.TRACKISUPSTREAM, Code)
    return F.TRACKISUPSTREAM


@configurable
def get_ttrack_track_selector(Code=None):
    if Code: return F.require_all(F.TRACKISTTRACK, Code)
    return F.TRACKISTTRACK


@configurable
def standard_protoparticle_filter(Code=F.ALL):
    return Code


@configurable
def _make_particles(species,
                    CheckPID=True,
                    make_protoparticles=_make_charged_protoparticles,
                    get_track_selector=get_long_track_selector,
                    make_protoparticle_filter=standard_protoparticle_filter):
    """ creates LHCb::Particles from LHCb::ProtoParticles """
    tp = get_track_selector()
    pp = make_protoparticle_filter()
    particles = FunctionalParticleMaker(
        InputProtoParticles=make_protoparticles(),
        ParticleID=species,
        CheckPID=CheckPID,
        TrackPredicate=tp,
        ProtoParticlePredicate=pp).Particles
    return particles


# ChargedBasics
@configurable
def _make_ChargedBasics(
        species,
        make_protoparticles=_make_charged_protoparticles,
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter):
    """ creates LHCb::v2::ChargedBasics from LHCb::ProtoParticles """
    particles = Proto2ChargedBasic(
        InputUniqueIDGenerator=make_unique_id_generator(),
        InputProtoParticles=make_protoparticles(),
        ParticleID=species,
        TrackPredicate=get_track_selector(),
        ProtoParticlePredicate=make_protoparticle_filter(),
    ).Particles
    return particles


@configurable
def _make_all_ChargedBasics(species):
    return _make_ChargedBasics(
        species=species,
        get_track_selector=get_all_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


@configurable
def _make_long_ChargedBasics(species):
    return _make_ChargedBasics(
        species=species,
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


# Basic particles (e/mu/pi/K/p) per track type (long/down/up)
def make_long_cb_electrons():
    return _make_long_ChargedBasics('electron')


def make_long_cb_muons():
    return _make_long_ChargedBasics('muon')


def make_long_cb_pions():
    return _make_long_ChargedBasics('pion')


def make_long_cb_kaons():
    return _make_long_ChargedBasics('kaon')


def make_long_cb_protons():
    return _make_long_ChargedBasics('proton')


def make_has_rich_long_cb_kaons():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_long_cb_kaons()


def make_has_rich_long_cb_pions():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_long_cb_pions()


def make_all_cb_electrons():
    return _make_all_ChargedBasics('electron')


def make_all_cb_muons():
    return _make_all_ChargedBasics('muon')


def make_all_cb_pions():
    return _make_all_ChargedBasics('pion')


def make_all_cb_kaons():
    return _make_all_ChargedBasics('kaon')


def make_all_cb_protons():
    return _make_all_ChargedBasics('proton')


@configurable
def make_photons(make_neutral_protoparticles=_make_neutral_protoparticles,
                 pv_maker=make_pvs,
                 **kwargs):
    """ creates photon LHCb::Particles from LHCb::ProtoParticles (PVs are optional) """
    pvs = pv_maker()
    particles = PhotonMaker(
        InputProtoParticles=make_neutral_protoparticles(),
        InputPrimaryVertices=pvs,
        **kwargs).Particles
    return particles


#Longstream particles
def make_long_electrons_no_brem():
    return _make_particles(
        species="electron",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


@configurable
def make_long_electrons_with_brem(bremadder='SelectiveBremAdder'):
    """Make long electrons adding bremsstrahlung correction to the track momentum.
    No extra cuts are applied.
    """
    return ParticleWithBremMaker(
        InputParticles=make_long_electrons_no_brem(),
        BremAdder=bremadder,
    ).Particles


def make_long_pions():
    return _make_particles(
        species="pion",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_kaons():
    return _make_particles(
        species="kaon",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_protons():
    return _make_particles(
        species="proton",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_muons():
    return _make_particles(
        species="muon",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_sigmaps():
    return _make_particles(
        species="sigmap",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_sigmams():
    return _make_particles(
        species="sigmam",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_xis():
    return _make_particles(
        species="xim",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_omegas():
    return _make_particles(
        species="omegam",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


# Make muons with ismuon
@configurable
def make_ismuon_long_muon():
    with standard_protoparticle_filter.bind(Code=F.ISMUON):
        return make_long_muons()


#Upstream particles
def make_up_electrons_no_brem():
    return _make_particles(
        species="electron",
        make_protoparticles=_make_upstream_charged_protoparticles,
        get_track_selector=get_upstream_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_up_pions():
    return _make_particles(
        species="pion",
        make_protoparticles=_make_upstream_charged_protoparticles,
        get_track_selector=get_upstream_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_up_kaons():
    return _make_particles(
        species="kaon",
        make_protoparticles=_make_upstream_charged_protoparticles,
        get_track_selector=get_upstream_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_up_protons():
    return _make_particles(
        species="proton",
        make_protoparticles=_make_upstream_charged_protoparticles,
        get_track_selector=get_upstream_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_up_muons():
    return _make_particles(
        species="muon",
        make_protoparticles=_make_upstream_charged_protoparticles,
        get_track_selector=get_upstream_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


#Downstream particles
def make_down_pions():
    return _make_particles(
        species="pion",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_kaons():
    return _make_particles(
        species="kaon",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_protons():
    return _make_particles(
        species="proton",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_electrons():
    return _make_particles(
        species="electron",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_electrons_no_brem():
    return _make_particles(
        species="electron",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_muons():
    return _make_particles(
        species="muon",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


# Ttrack particles
def make_ttrack_pions():
    return _make_particles(
        species="pion",
        get_track_selector=get_ttrack_track_selector,
        make_protoparticles=make_ttrack_protoparticles,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_ttrack_protons():
    return _make_particles(
        species="proton",
        get_track_selector=get_ttrack_track_selector,
        make_protoparticles=make_ttrack_protoparticles,
        make_protoparticle_filter=standard_protoparticle_filter)


# HASRICH definitions of pions/K: long, down, up


# Make long, down, up pions
@configurable
def make_has_rich_long_pions():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_long_pions()


@configurable
def make_has_rich_down_pions():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_down_pions()


@configurable
def make_has_rich_up_pions():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_up_pions()


# Make long, down, up kaons
@configurable
def make_has_rich_long_kaons():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_long_kaons()


@configurable
def make_has_rich_down_kaons():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_down_kaons()


@configurable
def make_has_rich_up_kaons():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_up_kaons()


# Make long, down, up protons
@configurable
def make_has_rich_long_protons():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_long_protons()


@configurable
def make_has_rich_down_protons():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_down_protons()


@configurable
def make_has_rich_up_protons():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_up_protons()


# Ttrack particles
def make_has_rich_ttrack_pions():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_ttrack_pions()


def make_has_rich_ttrack_protons():
    with standard_protoparticle_filter.bind(Code=F.PPHASRICH):
        return make_ttrack_protons()


# Definitions of pi0
@configurable
def make_resolved_pi0s(particles=make_photons,
                       nominal_mass=masses['pi0'],
                       mass_window=30. * MeV,
                       PtCut=0. * MeV,
                       **kwargs):

    combination_code = in_range(nominal_mass - mass_window, F.MASS,
                                nominal_mass + mass_window)
    vertex_code = F.require_all(F.PT > PtCut)
    return ParticleCombiner(
        Inputs=[particles(**kwargs), particles(**kwargs)],
        DecayDescriptor="pi0 -> gamma gamma",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        ParticleCombiner="ParticleAdder")


@configurable
def make_merged_pi0s(mass_window=60. * MeV,
                     PtCut=2000. * MeV,
                     make_neutral_protoparticles=_make_neutral_protoparticles,
                     pvs=make_pvs,
                     **kwargs):
    particles = MergedPi0Maker(
        InputProtoParticles=make_neutral_protoparticles(),
        InputPrimaryVertices=pvs(),
        MassWindow=mass_window,
        PtCut=PtCut,
        **kwargs).Particles
    return particles


@configurable
def filter_leptons_loose(particles,
                         lepton,
                         pv_maker=make_pvs,
                         pt_min=500 * MeV,
                         p_min=0. * MeV,
                         probnn_e=None,
                         probnn_mu=None,
                         pid_e=-999,
                         pid_mu=None,
                         ismuon=False,
                         minipchi2=-999,
                         trghostprob=999):
    """Returns loosely preselected leptons """
    pvs = pv_maker()
    code = F.require_all(F.PT > pt_min, F.P > p_min,
                         F.MINIPCHI2(pvs) > minipchi2,
                         F.GHOSTPROB < trghostprob)
    if lepton == 'muon':
        if probnn_mu is not None:
            code = (F.PROBNN_MU > probnn_mu) & code
        if pid_mu is not None:
            code = (F.PID_MU > pid_mu) & code
        if ismuon:
            code = code & (F.ISMUON)
    elif lepton == 'electron':
        if pid_e is not None:
            code = (F.PID_E > pid_e) & code
        if probnn_e is not None:
            code = (F.PROBNN_E > probnn_e) & code
    else:
        raise Exception(
            'in HLT2Conf.standard_particles.filter_leptons_loose need specify leptons (str) as either "muon" or "electron"'
        )

    return ParticleFilter(particles, F.FILTER(code))


#Basic dilepton builders
@configurable
def make_detached_dielectron(name='make_detached_dielectron_{hash}',
                             pv_maker=make_pvs,
                             opposite_sign=True,
                             dilepton_ID='J/psi(1S)',
                             electron_maker=make_long_electrons_no_brem,
                             pid_e=2,
                             probnn_e=None,
                             pt_e=0.25 * GeV,
                             minipchi2=9.,
                             trghostprob=0.25,
                             adocachi2cut=30,
                             bpvvdchi2=30,
                             vfaspfchi2ndof=10):
    if opposite_sign:
        DecayDescriptor = f'{dilepton_ID} -> e+ e-'
        name = name + '_rs_{hash}'
    else:
        DecayDescriptor = f'[{dilepton_ID} -> e+ e+]cc'
        name = name + '_ws_{hash}'

    pvs = pv_maker()

    electrons = filter_leptons_loose(
        particles=electron_maker(),
        lepton='electron',
        pt_min=pt_e,
        pid_e=pid_e,
        probnn_e=probnn_e,
        minipchi2=minipchi2,
        trghostprob=trghostprob)
    combination_code = F.require_all(F.MAXDOCACHI2CUT(float(adocachi2cut)))
    vertex_code = F.require_all(F.CHI2DOF < vfaspfchi2ndof,
                                F.BPVFDCHI2(pvs) > bpvvdchi2)
    return ParticleCombiner(
        Inputs=[electrons, electrons],
        name=name,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def _make_dielectron_with_brem(electrons,
                               pt_diE=0 * MeV,
                               m_diE_min=0 * MeV,
                               m_diE_max=6000 * MeV,
                               m_diE_ID="J/psi(1S)",
                               opposite_sign=True,
                               bremadder='SelectiveBremAdder'):
    """Make Jpsi -> e+ e- or [Jpsi -> e+ e+ ]cc candidates adding bremsstrahlung correction to the
    electrons while ensuring the same photon is not used to correct both electrons.
    No PT and a very loose mass cut is applied to the dielectron initially.
    No cuts can be applied to the input electrons, but it is encouraged to filter
    them first, to reduce the combinatorics. See make_detached_dielectron_with_brem
    for a usage example.
    """
    return FunctionalDiElectronMaker(
        InputParticles=electrons,
        MinDiElecPT=pt_diE,
        MinDiElecMass=m_diE_min,
        MaxDiElecMass=m_diE_max,
        DiElecID=m_diE_ID,
        OppositeSign=opposite_sign,
        BremAdder=bremadder).Particles


@configurable
def make_detached_dielectron_with_brem(
        electron_maker=make_long_electrons_no_brem,
        opposite_sign=True,
        pv_maker=make_pvs,
        PIDe_min=2.,
        probnn_e=None,
        pt_e=0.25 * GeV,
        p_e=0. * GeV,
        minipchi2=9.,
        trghostprob=0.25,
        dielectron_ID="J/psi(1S)",
        pt_diE=0 * MeV,
        m_diE_min=0 * MeV,
        m_diE_max=6000 * MeV,
        adocachi2cut=30,
        bpvvdchi2=30,
        vfaspfchi2ndof=10):
    """Make detached Jpsi -> e+ e- or [Jpsi -> e+ e+ ]cc candidates adding bremsstrahlung correction to the
    electrons. The selection follows make_detached_dielectron but the dielectron
    combination is built through _make_dielectron_with_brem, providing an example
    of the usage of this private method.
    """
    pvs = pv_maker()
    particles = electron_maker()
    code_e = F.require_all(F.PT > pt_e, F.P > p_e,
                           F.MINIPCHI2(pvs) > minipchi2,
                           F.GHOSTPROB < trghostprob)
    if PIDe_min is not None:
        code_e = code_e & (F.PID_E > PIDe_min)
    if probnn_e is not None:
        code_e = code_e & (F.PROBNN_E > probnn_e)

    detached_e = ParticleFilter(particles, F.FILTER(code_e))
    detached_dielectron_with_brem = _make_dielectron_with_brem(
        detached_e,
        pt_diE=pt_diE,
        m_diE_min=m_diE_min,
        m_diE_max=m_diE_max,
        m_diE_ID=dielectron_ID,
        opposite_sign=opposite_sign)
    code_dielectron = F.require_all(
        F.MAXDOCACHI2CUT(float(adocachi2cut)), F.CHI2DOF < vfaspfchi2ndof,
        F.BPVFDCHI2(pvs) > bpvvdchi2)
    return ParticleFilter(detached_dielectron_with_brem,
                          F.FILTER(code_dielectron))


@configurable
def make_detached_mue(name='make_detached_mue_{hash}',
                      opposite_sign=True,
                      dilepton_ID='J/psi(1S)',
                      pv_maker=make_pvs,
                      pid_mu=0.2,
                      probnn_mu=None,
                      pt_mu=0. * GeV,
                      pid_e=2.,
                      probnn_e=None,
                      pt_e=0.25 * GeV,
                      minipchi2=9.,
                      trghostprob=0.25,
                      adocachi2cut=30,
                      bpvvdchi2=30,
                      vfaspfchi2ndof=10):
    if opposite_sign:
        DecayDescriptor = f'[{dilepton_ID} -> mu+ e-]cc'
        name = name + '_rs'
    else:
        DecayDescriptor = f'[{dilepton_ID} -> mu+ e+]cc'
        name = name + '_ws'

    pvs = pv_maker()

    muons = filter_leptons_loose(
        particles=make_long_muons(),
        lepton='muon',
        pt_min=pt_mu,
        pid_mu=pid_mu,
        probnn_mu=probnn_mu,
        minipchi2=minipchi2,
        trghostprob=trghostprob)
    electrons = filter_leptons_loose(
        particles=make_long_electrons_no_brem(),
        lepton='electron',
        pt_min=pt_e,
        pid_e=pid_e,
        probnn_e=probnn_e,
        minipchi2=minipchi2,
        trghostprob=trghostprob)
    combination_code = F.require_all(F.MAXDOCACHI2CUT(float(adocachi2cut)))
    vertex_code = F.require_all(F.CHI2DOF < vfaspfchi2ndof,
                                F.BPVFDCHI2(pvs) > bpvvdchi2)
    return ParticleCombiner(
        Inputs=[muons, electrons],
        name=name,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_detached_mue_with_brem(name='make_detached_mue_with_brem_{hash}',
                                opposite_sign=True,
                                dilepton_ID='J/psi(1S)',
                                pv_maker=make_pvs,
                                min_probnn_mu=None,
                                min_PIDmu=0.,
                                IsMuon=False,
                                min_pt_mu=0. * GeV,
                                min_PIDe=2,
                                probnn_e=None,
                                min_pt_e=0.25 * GeV,
                                minipchi2_track=9.,
                                max_trghostprob=0.25,
                                max_adocachi2=30,
                                min_bpvvdchi2=30,
                                max_vchi2ndof=10):
    """Make detached J/psi(1S) -> mu e candidates with ismuon muons and adding bremsstrahlung correction to the
    electrons. The opposite_sign flag is a boolean that can be activated.
    """
    if opposite_sign:
        DecayDescriptor = f'[{dilepton_ID} -> mu+ e-]cc'
        name = name + '_rs_{hash}'
    else:
        DecayDescriptor = f'[{dilepton_ID} -> mu+ e+]cc'
        name = name + '_ws_{hash}'

    pvs = pv_maker()

    muons = filter_leptons_loose(
        particles=make_ismuon_long_muon(),
        lepton='muon',
        pt_min=min_pt_mu,
        probnn_mu=min_probnn_mu,
        pid_mu=min_PIDmu,
        ismuon=IsMuon,
        minipchi2=minipchi2_track,
        trghostprob=max_trghostprob)
    electrons = filter_leptons_loose(
        particles=make_long_electrons_no_brem(),
        lepton='electron',
        pt_min=min_pt_e,
        pid_e=min_PIDe,
        probnn_e=probnn_e,
        minipchi2=minipchi2_track,
        trghostprob=max_trghostprob)
    combination_code = F.require_all(F.MAXDOCACHI2CUT(float(max_adocachi2)))
    vertex_code = F.require_all(F.CHI2DOF < max_vchi2ndof,
                                F.BPVFDCHI2(pvs) > min_bpvvdchi2)
    return ParticleCombiner(
        Inputs=[muons, electrons],
        name=name,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_detached_mumu(name='make_detached_mumu_{hash}',
                       opposite_sign=True,
                       dilepton_ID='J/psi(1S)',
                       pv_maker=make_pvs,
                       pid_mu=2,
                       probnn_mu=None,
                       pt_mu=0. * GeV,
                       minipchi2=9.,
                       trghostprob=0.25,
                       adocachi2cut=30,
                       bpvvdchi2=30,
                       vfaspfchi2ndof=10):
    #def make_detached_mumu(probnn_mu=-0.2, pt_mu=0.*GeV, minipchi2=0., trghostprob=0.925, adocachi2cut=30, bpvvdchi2=30, vfaspfchi2ndof=10):
    if opposite_sign:
        DecayDescriptor = f'{dilepton_ID} -> mu+ mu-'
        name = name + '_rs'
    else:
        DecayDescriptor = f'[{dilepton_ID} -> mu+ mu+]cc'
        name = name + '_ws'

    pvs = pv_maker()

    muons = filter_leptons_loose(
        particles=make_long_muons(),
        lepton='muon',
        pt_min=pt_mu,
        pid_mu=pid_mu,
        probnn_mu=probnn_mu,
        minipchi2=minipchi2,
        trghostprob=trghostprob)
    combination_code = F.require_all(F.MAXDOCACHI2CUT(float(adocachi2cut)))
    vertex_code = F.require_all(F.CHI2DOF < vfaspfchi2ndof,
                                F.BPVFDCHI2(pvs) > bpvvdchi2)
    return ParticleCombiner(
        Inputs=[muons, muons],
        name=name,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_dimuon_base(name='DiMuonBaseCombiner_{hash}',
                     maxVCHI2PDOF=25,
                     pt=None,
                     pid_mu=None,
                     probnn_mu=None,
                     adoca_chi2=None):
    """Basic dimuon without any requirements but common vertex
    Please DO NOT add pt requirements here:
    a dedicated (tighter) dimuon filter is implemented in the dimuon module.
    """
    # get the long muons
    muons = filter_leptons_loose(
        particles=make_ismuon_long_muon(),
        lepton='muon',
        pt_min=pt if pt else 0 * MeV,
        pid_mu=pid_mu,
        probnn_mu=probnn_mu)

    # require that the muons come from the same vertex
    combination_code = F.ALL if adoca_chi2 is None else F.MAXDOCACHI2CUT(
        float(adoca_chi2))
    vertex_code = F.require_all(F.CHI2DOF < maxVCHI2PDOF)
    return ParticleCombiner(
        Inputs=[muons, muons],
        name=name,
        DecayDescriptor='J/psi(1S) -> mu+ mu-',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_mass_constrained_jpsi2mumu(name='MassConstrJpsi2MuMuMaker_{hash}',
                                    jpsi_maker=make_dimuon_base,
                                    nominal_mass=masses['J/psi(1S)'],
                                    pid_mu=0,
                                    probnn_mu=None,
                                    pt_mu=0.5 * GeV,
                                    admass=250. * MeV,
                                    adoca_chi2=20,
                                    vchi2=16):
    """Make the Jpsi, starting from dimuons"""

    # get the dimuons with basic cuts (only vertexing)
    # note that the make_dimuon_base combiner uses vertexChi2/ndof < 25,
    # which is looser than the vertexChi2 < 16 required here
    dimuons = jpsi_maker(
        pt=pt_mu, pid_mu=pid_mu, probnn_mu=probnn_mu, adoca_chi2=adoca_chi2)

    code = F.require_all(
        in_range(nominal_mass - admass, F.MASS, nominal_mass + admass),
        F.CHI2 < vchi2)
    return ParticleFilter(dimuons, F.FILTER(code))


@configurable
def make_phi2kk(am_max=1100. * MeV,
                adoca_chi2=30,
                vchi2=25.0,
                name='make_phi2kk_{hash}'):
    kaons = make_long_kaons()
    descriptors = 'phi(1020) -> K+ K-'
    combination_code = F.require_all(F.MASS < am_max,
                                     F.MAXDOCACHI2CUT(float(adoca_chi2)))
    vertex_code = F.require_all(F.CHI2 < vchi2)
    return ParticleCombiner(
        Inputs=[kaons, kaons],
        name=name,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


# Set of builders for V0 (KS0/L0) particles including filtering of daughters
# Make V0s -- All lines using V0 particles must filter on PVs !
def _make_long_for_V0(particles, pvs):
    code = F.require_all(F.MINIPCHI2(pvs) > 36)
    return ParticleFilter(particles, F.FILTER(code))


def _make_down_for_V0(particles):
    code = F.require_all(F.P > 3000 * MeV, F.PT > 175 * MeV)
    return ParticleFilter(particles, F.FILTER(code))


def _make_up_for_V0(particles):
    code = F.require_all(F.P > 1000 * MeV, F.PT > 175 * MeV)
    return ParticleFilter(particles, F.FILTER(code))


def make_long_pions_for_V0():
    return _make_long_for_V0(make_long_pions(), make_pvs())


def make_long_protons_for_V0():
    return _make_long_for_V0(make_long_protons(), make_pvs())


def make_down_pions_for_V0():
    return _make_down_for_V0(make_down_pions())


def make_down_kaons_for_V0():
    return _make_down_for_V0(make_down_kaons())


def make_down_protons_for_V0():
    return _make_down_for_V0(make_down_protons())


def make_up_pions_for_V0():
    return _make_up_for_V0(make_up_pions())


def make_up_kaons_for_V0():
    return _make_up_for_V0(make_up_kaons())


def make_up_protons_for_V0():
    return _make_up_for_V0(make_up_protons())


# All lines using V0 particles must filter on PVs !
@configurable
def _make_V0LL(particles,
               descriptors,
               pv_maker=make_pvs,
               name='make_V0LL_{hash}',
               nominal_mass=masses['KS0'],
               am_dmass=50 * MeV,
               m_dmass=35 * MeV,
               vchi2pdof_max=30,
               bpvltime_min=2.0 * picosecond):
    """Make long-long V0 -> h+ h'- candidates
    Initial implementation a replication of the old Hlt2SharedParticles
    """
    pvs = pv_maker()
    combination_code = F.require_all(
        in_range(nominal_mass - am_dmass, F.MASS, nominal_mass + am_dmass))
    if bpvltime_min is not None:
        vertex_code = F.require_all(
            in_range(nominal_mass - am_dmass, F.MASS, nominal_mass + am_dmass),
            F.CHI2DOF < vchi2pdof_max,
            F.BPVLTIME(pvs) > bpvltime_min)
    else:
        vertex_code = F.require_all(
            in_range(nominal_mass - m_dmass, F.MASS, nominal_mass + m_dmass),
            F.CHI2DOF < vchi2pdof_max)

    return ParticleCombiner(
        Inputs=particles,
        DecayDescriptor=descriptors,
        name=name,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def _make_V0DD(particles,
               descriptors,
               pv_maker=make_pvs,
               name='make_V0DD_{hash}',
               nominal_mass=masses['KS0'],
               am_dmass=80 * MeV,
               m_dmass=64 * MeV,
               vchi2pdof_max=30,
               bpvvdz_min=400 * mm):
    """Make down-down V0 -> h+ h'- candidates
    Initial implementation a replication of the old Hlt2SharedParticles
    """
    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(nominal_mass - am_dmass, F.MASS, nominal_mass + am_dmass))
    vertex_code = F.require_all(
        in_range(nominal_mass - m_dmass, F.MASS, nominal_mass + m_dmass),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVVDZ(pvs) > bpvvdz_min)
    return ParticleCombiner(
        Inputs=particles,
        DecayDescriptor=descriptors,
        name=name,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def _make_V0LD_UL(particles,
                  descriptors,
                  pv_maker=make_pvs,
                  name='make_V0LD_{hash}',
                  nominal_mass=masses['KS0'],
                  am_dmass=80 * MeV,
                  m_dmass=64 * MeV,
                  vchi2pdof_max=30):
    """Make long-down or long-up V0 -> h+ h'- candidates
    Initial implementation a replication of the _make_V0DD function without
    the BPVVDZ() requirement
    """
    combination_code = in_range(nominal_mass - am_dmass, F.MASS,
                                nominal_mass + am_dmass)
    vertex_code = F.require_all(
        in_range(nominal_mass - m_dmass, F.MASS, nominal_mass + m_dmass),
        F.CHI2DOF < vchi2pdof_max)
    return ParticleCombiner(
        Inputs=particles,
        DecayDescriptor=descriptors,
        name=name,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_KsLL(pions_long=None):
    if pions_long is None:
        pions_long = make_long_pions_for_V0()
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0LL(
        particles=[pions_long, pions_long],
        descriptors=descriptors,
        pv_maker=make_pvs)


def make_KsDD(pions_down=None):
    if pions_down is None:
        pions_down = make_down_pions_for_V0()
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0DD(
        particles=[pions_down, pions_down],
        descriptors=descriptors,
        pv_maker=make_pvs)


def make_KsLD(pions_down=None, pions_long=None):
    if pions_down is None:
        pions_down = make_down_pions_for_V0()
    if pions_long is None:
        pions_long = make_long_pions_for_V0()
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0LD_UL(
        particles=[pions_long, pions_down],
        descriptors=descriptors,
        pv_maker=make_pvs)


def make_KsUL(pions_long=None, pions_up=None):
    if pions_up is None:
        pions_up = make_up_pions_for_V0()
    if pions_long is None:
        pions_long = make_long_pions_for_V0()
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0LD_UL(
        particles=[pions_long, pions_up],
        descriptors=descriptors,
        pv_maker=make_pvs)


def make_KsLL_fromSV():
    pions_long = make_long_pions_for_V0()
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0LL(
        particles=[pions_long, pions_long],
        descriptors=descriptors,
        pv_maker=make_pvs,
        vchi2pdof_max=25.0,
        bpvltime_min=None)


def make_KsDD_fromSV():
    pions_down = make_down_pions_for_V0()
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0DD(
        particles=[pions_down, pions_down],
        descriptors=descriptors,
        pv_maker=make_pvs,
        vchi2pdof_max=25.0)


def make_LambdaLL():
    pions = make_long_pions_for_V0()
    protons = make_long_protons_for_V0()
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0LL(
        particles=[protons, pions],
        descriptors=descriptors,
        pv_maker=make_pvs,
        nominal_mass=masses['Lambda0'],
        am_dmass=50 * MeV,
        m_dmass=20 * MeV,
        vchi2pdof_max=30,
        bpvltime_min=2.0 * picosecond)


@configurable
def make_LambdaDD():
    pions = make_down_pions_for_V0()
    protons = make_down_protons_for_V0()
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0DD(
        particles=[protons, pions],
        descriptors=descriptors,
        pv_maker=make_pvs,
        nominal_mass=masses['Lambda0'],
        am_dmass=80 * MeV,
        m_dmass=24 * MeV,
        vchi2pdof_max=30,
        bpvvdz_min=400 * mm)
