###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# needed to check if we didn't find something in TES
import cppyy
#cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True
not_found = cppyy.bind_object(0, cppyy.gbl.DataObject)


def error(msg):
    print("Check ERROR", msg)


# These locations do not exist in old brunel outputs or any file produced
# before LHCb!3622, for the moment ignore the errors in unpacking checks.
# until we have some versioning of the reco locations
UnexpectedReco = ["MuonTracks"]
# The following list is some-what tailor made for the hadronic decay MC sample we are testing on.
# Its the reco objects that we expect to find 0 of in this particular decay.
UnexpectedLocs = [
    'SplitPhotons', 'Neutrals', 'MergedPi0s', 'Electrons', 'Photons'
]


def check_persistreco(TES, locations, N=3, unexpected_locs=UnexpectedLocs):

    for loc in locations:
        unpacked = TES[loc]

        if any(x in loc for x in UnexpectedReco):
            continue

        if any(x in loc for x in unexpected_locs):
            continue
        if unpacked == not_found:
            error("Unpacked location not found: " + loc)

        if "Rec/Summary" in loc:
            if unpacked.size() < 1:
                error("RecSummary not persisted.")
        print(loc, " has size ", len(unpacked))

        if len(unpacked) < N:
            if "Vertex" not in loc:  ## Do not expect N_TURBO+ vertices
                error(loc + " has only " + str(len(unpacked)) +
                      f" entries, expected >={N}.")


def check_MCoutput(TES, RECO_ROOT, fs=4, mcpart=100):
    """
    Check MC TES locations are present for dst workflows

    Note that if at any point an MDF has been used in the workflow these locations will not exist
    (MDFs only support RawBanks)
    """

    MC_rel = TES[RECO_ROOT + '/Relations/ChargedPP2MCP']
    if not MC_rel:
        error("MC relations table not propagated")
    if not MC_rel.relations().size() >= fs:
        error("MC relations table not correctly propagated")
    print("MC relations table size is ", MC_rel.relations().size())

    MC_part = TES[RECO_ROOT + '/MC/Particles']
    if not MC_part:
        error("MC particles not propagated")
    if not MC_part.size() > mcpart:
        error("MC particles not correctly propagated")
    print(
        "MC particles " + RECO_ROOT + '/MC/Particles' + " container has size ",
        MC_part.size())

    MC_part_packed = TES[RECO_ROOT + '/pSim/MCParticles'].mcParts().size()
    print("Packed MC particles container has size ", MC_part_packed)
    if not MC_part.size() == MC_part_packed:
        error("MC object packing not working: " + str(MC_part.size()) +
              " unpacked, vs. :" + str(MC_part_packed) + " packed")

    MC_vert = TES[RECO_ROOT + '/MC/Vertices']
    if not MC_vert:
        error("MC vertices not propagated")
    if not MC_vert.size() > mcpart:
        error("MC vertices not correctly propagated")
    print("MC vertices " + RECO_ROOT + '/MC/Vertices' + " container has size ",
          MC_vert.size())

    header = TES['/Event/Gen/Header']
    if not header:
        error("MC Headers not being propagated")
    print("MC eventtype is ", header.evType())
    MCTrackInfo = TES['/Event/MC/TrackInfo']
    if not MCTrackInfo:
        error("MC TrackInfo not being propagated")


def check_banks(TES, stream, banks=[9]):
    """
    Check a RawBank is populated
    RawBank numbers can be found here gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h#L62
    """
    #
    for bank in banks:
        bank_loc = '/Event/' + stream
        RawBank = TES[bank_loc].banks(bank)
        if not RawBank:
            error("Expected raw bank" + str(bank))
        print("RawBank ", bank, " has size ", RawBank.size())


def check_particlesandrelations(TES, prefix, N=3):
    """
    Check particles exist and the relations between them

    Assumes decay like X->YZ where Z is a final state particle
    """

    container = TES[prefix + '/Particles']
    if not container:
        error("No particles propagated contrary to DecReports")
    if not container.size() > 0:
        error("Zero particles propagated contrary to DecReports")
    print(prefix + "/Particles has size ", container.size())
    # The CopyParticles algorithm should ensure P2PV relations are saved for the
    # top-level object (the decay head)
    relations = TES[prefix + '/Particle2VertexRelations']
    if not relations:
        error("relations ERROR no P2PV relations")
    else:
        print(prefix + "/Particle2VertexRelations  has size ",
              relations.size())
    # Special selection algorithm configuration is required to ensure
    # P2PV relations for the descendents are propagated (they are only
    # created if an object uses PVs in its selection). Here we check
    # that at least some lines have child relations, but don't require
    # it for every line (it's valid for a line not to require PVs to
    # perform its selection).
    if not relations.size() > container.size():
        error("P2PV relations error")

    # Check a protoparticle
    proto = container[0].daughtersVector()[1].proto()
    if not proto:
        error("Proto particles are not saved")
    print("proto: ", proto)

    mothers = container.size()
    firstdaughters = container[0].daughtersVector().size()
    print("mothers: ", mothers, " firstdaughters: ", firstdaughters)
    if mothers + firstdaughters < N:
        error("Decay tree of particles not being saved correctly.")


def check_decreports(TES, decs=[], stage="Hlt2"):

    reportloc = TES['/Event/' + stage + '/DecReports']
    lines_available = reportloc.decReports().keys()
    decs = [dec + "Decision" for dec in decs]
    decs = set(decs).intersection(lines_available)
    decisions = {dec: reportloc.decReport(dec).decision() for dec in decs}

    print(stage, " reports are ", decisions)
    if not any(decisions):
        error(
            "HLT2 DecReports not working or something is wrong with the streaming."
        )
    return decisions
