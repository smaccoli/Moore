###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for testing the first data with HLT2.
"""
from Moore.lines import Hlt2Line
from Hlt2Conf.lines.topological_b import all_lines as topological_b_lines
from Hlt2Conf.lines.inclusive_detached_dilepton.inclusive_detached_dilepton_trigger import all_lines as inclusive_detached_dilepton_trigger_lines
from Hlt2Conf.lines.pid import all_lines as pid_lines
from Hlt2Conf.lines.trackeff import all_lines as trackeff_lines
from Hlt2Conf.lines.monitoring import all_lines as monitoring_lines
from Hlt2Conf.lines.b_to_open_charm import all_lines as b_to_open_charm_lines
from Hlt2Conf.lines.rd import all_lines as rd_lines
from Hlt2Conf.lines.bandq import all_lines as bandq_lines
from Hlt2Conf.lines.qee import hlt2_full_lines as qee_full_lines
from Hlt2Conf.lines.qee import hlt2_turbo_lines as qee_turbo_lines
from Hlt2Conf.lines.charm import all_lines as charm_lines
from Hlt2Conf.lines.b_to_charmonia import all_lines as b_to_charmonia_lines
from Hlt2Conf.lines.semileptonic import all_lines as semileptonic_lines
from Hlt2Conf.lines.charmonium_to_dimuon import all_lines as charmonium_to_dimuon_lines
from Hlt2Conf.lines.charmonium_to_dimuon_detached import all_lines as charmonium_to_dimuon_detached_lines
from Hlt2Conf.lines.bnoc import all_lines as bnoc_lines
from Hlt2Conf.lines.DiMuonNoIP import all_lines as DiMuonNoIP_lines

from GaudiKernel.SystemOfUnits import (GeV, MeV, mm, micrometer as um)
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_particles import (
    make_long_pions, make_has_rich_long_pions, make_has_rich_long_kaons,
    make_long_protons, make_photons)
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
import Functors as F
import re


def _hlt2_lumi_line(name='Hlt2Lumi', prescale=1):
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale)


def _hlt2_no_bias_line(name="Hlt2NoBias", prescale=0.1):
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1ODINNoBiasDecision",
        prescale=prescale)


def _hlt2_hlt1passthrough_line(name="Hlt2Hlt1PassThrough", prescale=1):
    '''
      Anything else than the nobias line
    '''
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code=r"^Hlt1(?!ODINNoBias|ODINLumi).*Decision",
        prescale=prescale)


def _MIP_MIN(cut, pvs=make_pvs):
    return F.MINIPCUT(IPCut=cut, Vertices=pvs())


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


def _filter_long_pions_for_strange():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 80 * MeV, _MIP_MIN(1.2 * mm),
                          _MIPCHI2_MIN(12))))


# take the charm pi/K filter from d0_to_hh
def _filter_long_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K < 5.,
                _MIPCHI2_MIN(4),
            ), ),
    )


def _filter_long_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K > 5.,
                _MIPCHI2_MIN(4),
            ), ),
    )


def _filter_long_protons_for_strange():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.PID_P > -5., _MIP_MIN(80. * um),
                          _MIPCHI2_MIN(9))))


def _filter_long_protons_for_charm():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 600 * MeV, F.PID_P > 5., _MIP_MIN(20. * um),
                          _MIPCHI2_MIN(4))))


def _filter_photons():
    return ParticleFilter(
        make_photons(),
        F.FILTER(
            F.require_all(F.PT > 300 * MeV,
                          F.CALO_NEUTRAL_1TO9_ENERGY_RATIO > 0.9,
                          F.IS_PHOTON > 0.7, F.IS_NOT_H > 0.5)))


def _make_ll_kshorts():
    return ParticleCombiner(
        [_filter_long_pions_for_strange(),
         _filter_long_pions_for_strange()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name='Comissioning_KS0_LL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(420 * MeV, F.MASS, 570 * MeV),
            F.PT > 300 * MeV,
            F.P > 3.5 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(450 * MeV, F.MASS, 550 * MeV),
            F.PT > 350 * MeV,
            F.P > 4 * GeV,
            F.CHI2DOF < 12.,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.BPVVDRHO(make_pvs()) > 1.5 * mm,
            F.BPVFDCHI2(make_pvs()) > 80.,
            F.BPVDIRA(make_pvs()) < 0.9995,
        ),
    )


def _make_ll_lambdas():
    return ParticleCombiner(
        [_filter_long_protons_for_strange(),
         _filter_long_pions_for_strange()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Commissioning_Lambda_LL",
        CombinationCut=F.require_all(
            F.MASS < 1180 * MeV,
            F.PT > 450 * MeV,
            F.MAXDOCACUT(300 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1080 * MeV, F.MASS, 1140 * MeV),
            F.PT > 500 * MeV,
            F.CHI2DOF < 16.,
            F.BPVVDZ(make_pvs()) > 12 * mm,
            F.math.in_range(-100 * mm, F.END_VZ, 700 * mm),
            F.BPVVDRHO(make_pvs()) > 1.5 * mm,
            F.BPVDIRA(make_pvs()) > 0.999,
            F.BPVFDCHI2(make_pvs()) > 100,
        ),
    )


def _make_dp_kpipi():
    return ParticleCombiner(
        [
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm(),
            _filter_long_pions_for_charm()
        ],
        DecayDescriptor="[D+ -> K- pi+ pi+]cc",
        name="Commissioning_DpToKmPipPip",
        CombinationCut=F.require_all(
            F.math.in_range(1750 * MeV, F.MASS, 1990 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 10 * GeV,
            F.SUM(F.PT) > 1.2 * GeV,
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1770 * MeV, F.MASS, 1970 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 12 * GeV,
            F.CHI2DOF < 12.,
            F.BPVVDZ(make_pvs()) > 1.5 * mm,
            F.BPVDIRA(make_pvs()) > 0.999,
            F.BPVFDCHI2(make_pvs()) > 32.,
        ),
    )


def _diphoton_line(name='Hlt2Commissioning_DiPhoton', prescale=1):
    diphoton = ParticleCombiner(
        name="Commissioning_DiPhoton_Combiner",
        Inputs=[_filter_photons(), _filter_photons()],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="eta -> gamma gamma",
        CombinationCut=F.require_all(
            F.math.in_range(0 * MeV, F.MASS, 1200 * MeV), F.PT > 1.6 * GeV),
        CompositeCut=F.ALL,
    )
    return Hlt2Line(
        name=name, algs=[diphoton], persistreco=True, prescale=prescale)


def _kshort_ll_tos_line(name="Hlt2Commissioning_KsToPimPip_LL_Hlt1KsTOS"):
    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1.*Ks.*Decision",
        algs=[require_pvs(make_pvs()),
              _make_ll_kshorts()])


def _kshort_ll_line(name="Hlt2Commissioning_KsToPimPip_LL"):
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs()),
              _make_ll_kshorts()],
        persistreco=True)


def _phi_to_kk_detached_line(name="Hlt2Commissioning_PhiToKmKp"):
    """
      Detached inclusive phi -> KK
    """
    phis = ParticleCombiner(
        [_filter_long_kaons_for_charm(),
         _filter_long_kaons_for_charm()],
        DecayDescriptor="phi(1020) -> K- K+",
        name="Commissioning_Phi",
        CombinationCut=F.require_all(
            F.MASS < 1080 * MeV,
            F.MAXSDOCACUT(100 * um),
            F.PT > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(985 * MeV, F.MASS, 1065 * MeV),
            F.PT > 600 * MeV,
            F.CHI2DOF < 10.,
            F.BPVVDZ(make_pvs()) > 0.8 * mm,
            F.BPVVDRHO(make_pvs()) < 2 * mm,
        ),
    )
    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=[require_pvs(make_pvs()), phis],
        persistreco=True)


def _lambda_ll_tos_line(name="Hlt2Commissioning_L0ToPpPim_LL_Hlt1L02PPiTOS"):
    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1L02PPiDecision",
        algs=[require_pvs(make_pvs()),
              _make_ll_lambdas()])


def _lambda_ll_line(name="Hlt2Commissioning_L0ToPpPim_LL"):
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs()),
              _make_ll_lambdas()],
        persistreco=True)


# the non-TOS version lives in charm/d0_to_hh
def _d0_to_kpi_line(name="Hlt2Commissioning_D0ToKmPip_Hlt1D2KPiTOS"):
    d0 = ParticleCombiner(
        [_filter_long_kaons_for_charm(),
         _filter_long_pions_for_charm()],
        DecayDescriptor="[D0 -> K- pi+]cc",
        name="Commissioning_D0ToKmPip",
        CombinationCut=F.require_all(
            F.math.in_range(1685 * MeV, F.MASS, 2045 * MeV),
            F.PT > 2 * GeV,
            F.MAX(F.PT) > 1200 * MeV,
            F.MAXDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1715 * MeV, F.MASS, 2015 * MeV),
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(make_pvs()) > 25.,
            F.BPVDIRA(make_pvs()) > 0.99985,
        ),
    )
    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1D2KPiDecision",
        algs=[require_pvs(make_pvs()), d0],
        persistreco=True)


def _dp_to_kpipi_line(name="Hlt2Commissioning_DpToKmPipPip"):
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs()),
              _make_dp_kpipi()],
        persistreco=True)


def _dp_to_kpipi_tos_line(
        name="Hlt2Commissioning_DpToKmPipPip_Hlt1TrackMVATOS"):
    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=[require_pvs(make_pvs()),
              _make_dp_kpipi()])


def _lc_to_pkpi_line(name="Hlt2Commissioning_LcpToPpKmPip"):
    pvs = make_pvs()
    lcs = ParticleCombiner(
        [
            _filter_long_protons_for_charm(),
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm()
        ],
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        name="Commissioning_LcpToPpKmPip",
        CombinationCut=F.require_all(
            F.math.in_range(2200 * MeV, F.MASS, 2560 * MeV),
            F.PT > 1.7 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2230 * MeV, F.MASS, 2550 * MeV),
            F.PT > 2 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 12.,
            F.BPVVDZ(pvs) > 0.5 * mm,
            F.BPVDIRA(pvs) > 0.999,
            F.BPVFDCHI2(pvs) > 12.,
        ),
    )
    return Hlt2Line(
        name=name, algs=[require_pvs(make_pvs()), lcs], persistreco=True)


def _remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.search(pattern_to_remove, name) is None
    }
    return filtered


## for removing lines that are incompatible with DD4HEP
to_remove = [
    "Hlt2TrackEff_DiMuon_Downstream", "Hlt2TrackEff_DiMuon_MuonUT",
    "Hlt2TrackEff_ZToMuMu_Downstream", "Hlt2TrackEff_ZToMuMu_UTMuon"
]


def _make_lines(lines):
    trunc_lines = lines
    for remove in to_remove:
        trunc_lines = _remove_lines(trunc_lines, remove)
    if len(lines) != len(trunc_lines):
        print("Manually removed lines due to DD4HEP: ",
              lines.keys() - trunc_lines.keys())
    return [builder() for builder in trunc_lines.values()]


def make_streams():
    return dict(
        full=_make_lines(topological_b_lines) +
        _make_lines(inclusive_detached_dilepton_trigger_lines) +
        _make_lines(charmonium_to_dimuon_detached_lines) +
        _make_lines(qee_full_lines) + [_hlt2_lumi_line()],
        turbo=_make_lines(b_to_open_charm_lines) + _make_lines(rd_lines) +
        _make_lines(bandq_lines) + _make_lines(qee_turbo_lines) +
        _make_lines(charm_lines) + _make_lines(b_to_charmonia_lines) +
        _make_lines(semileptonic_lines) +
        _make_lines(charmonium_to_dimuon_lines) + _make_lines(bnoc_lines) +
        _make_lines(DiMuonNoIP_lines) + [_hlt2_lumi_line()],
        turcal=_make_lines(pid_lines) + _make_lines(trackeff_lines) +
        _make_lines(monitoring_lines) + [
            _diphoton_line(),
            _kshort_ll_line(),
            _kshort_ll_tos_line(),
            _phi_to_kk_detached_line(),
            _lambda_ll_line(),
            _lambda_ll_tos_line(),
            _d0_to_kpi_line(),
            _dp_to_kpipi_line(),
            _dp_to_kpipi_tos_line(),
            _lc_to_pkpi_line(),
            _hlt2_lumi_line()
        ],
        no_bias=[_hlt2_no_bias_line(), _hlt2_lumi_line()],
        passthrough=[_hlt2_hlt1passthrough_line(),
                     _hlt2_lumi_line()])
