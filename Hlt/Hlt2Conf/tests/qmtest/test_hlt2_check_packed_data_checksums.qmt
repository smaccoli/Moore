<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Runs hlt2_check_packed_data.py with DEBUG output and checksum enabled on HltPackedDataWriter
and checks the output against a reference.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/default_input_and_conds_hlt2.py</text>
  <text>$HLT2CONFROOT/tests/options/hlt2_checksum_packed_data.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="reference"><text>../refs/hlt2_check_packed_data_checksums.ref</text></argument>
<argument name="error_reference"><text>../refs/empty.ref</text></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))

from Moore.qmtest.exclusions import skip_initialize, skip_scheduler, counter_preprocessor
from RecConf.QMTest.exclusions import preprocessor as RecPreprocessor

# FIXME understand the reason for these fluctuations, see LHCb#151
preprocessor = skip_initialize + RecPreprocessor + skip_scheduler + LineSkipper(regexps=[
    r"HLT2_Packer\s+INFO Packed data checksum for .*/Relations/NeutralPP2MCPPacked.*",
    r"HLT2_Packer\s+INFO Packed data checksum for .*/OutputClustersPacked.*",
    r"HLT2_Packer\s+INFO Packed data checksum for .*/ProtoP/ChargedPacked.*",
    # the 'global' check sum is spoiled by the above fluctations...
    r"HLT2_Packer\s+INFO Packed data checksum for '_global_'"
])

validateWithReference(preproc = preprocessor,
                      counter_preproc = counter_preprocessor)

</text></argument>

</extension>
