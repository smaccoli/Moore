<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Test sprucing on output of {2, 3}body topo lines run using real time reco.

Runs over hlt2_2or3bodytopo_realtime.mdf with the tck stored in eos/ created with `Hlt/Hlt2Conf/options/Sprucing/hlt2_2or3bodytopo_realtime.py`.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>lbexec</text></argument>
<argument name="timeout"><integer>3000</integer></argument>
<argument name="args"><set>
  <text>Hlt2Conf.Sprucing_production:spruce_all_lines_realtime_test_old_json</text>
  <text>$HLT2CONFROOT/options/sprucing/lbexec_yamls/spruce_all_lines_realtime_test_old_json.yaml</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"FATAL": 0, "WARNING":0, "ERROR": 20},
                stdout=remove_known_warnings(stdout))

import re
matches = re.findall('LAZY_AND: (Spruce.*) .*Sum=(\d+)', stdout)
if not matches:
    causes.append('no line decisions found')

# In this test we are running over approx. 5000 HLT1-filtered events that are additionaly
# filtered with the topo. 2 or 3 body topological trigger requirement (this is per-prepared and stored on RTA eos/).
# The topo. trigger requirement reduces the sample by about 1/10 so about 500 events
# are passed to the sprucing.
# We consider a failure if any of the sprucing selections exceeds 50 positive decisions
# (cut value to be tuned), this corresponds to a retention above 10 %.
max_expected_decisions = 50
for line, positive_decisions in matches:
    if int(positive_decisions) &gt; max_expected_decisions and "FEST" not in line:
        causes.append('{} positive decisions found for {}'.format(positive_decisions, line))

</text></argument>
</extension>

