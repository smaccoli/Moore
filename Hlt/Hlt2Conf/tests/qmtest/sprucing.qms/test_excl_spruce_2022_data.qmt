<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Test excl sprucing on 2022 data processed by HLT2.

-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>lbexec</text></argument>
<argument name="timeout"><integer>3000</integer></argument>
<argument name="args"><set>
  <text>Hlt2Conf.Sprucing_production:excl_spruce_production</text>
  <text>$HLT2CONFROOT/options/sprucing/lbexec_yamls/excl_spruce_2022_data.yaml</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"WARNING": 0, "FATAL": 0, "ERROR": 0},
                stdout=remove_known_warnings(stdout))

import os, ROOT, json, unittest

FILENAME=["spruce_all_lines_production.bandq.dst", "spruce_all_lines_production.b2cc.dst", "spruce_all_lines_production.b2oc.dst", "spruce_all_lines_production.qee.dst", "spruce_all_lines_production.rd.dst", "spruce_all_lines_production.sl.dst"]

expected = {"counts": {
                '255620': 12381
            },
            "empty": False,
            "type": "LumiEventCounter"
        }

missing_files = [
        name for name in FILENAME
        if not os.path.exists(name)
    ]
if missing_files:
        causes.append("missing output file(s)")

for file in FILENAME:
  try:
      f=ROOT.TFile.Open(file)
      fsr_root=json.loads(str(f.FileSummaryRecord))
  except Exception as err:
      causes.append(f"failure in file {file}")

  try:
    tester=unittest.TestCase()
    tester.assertEqual(expected, fsr_root['LumiCounter.eventsByRun'])
  except AssertionError as err:
    causes.append(f"FSR content wrong for {file}")

</text></argument>
</extension>

