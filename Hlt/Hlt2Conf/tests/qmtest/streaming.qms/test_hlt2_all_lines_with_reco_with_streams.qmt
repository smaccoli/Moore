<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Make sure HLT2 configures and runs all lines without errors when running reconstruction
and saving the output to two streams, one using persistreco.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="timeout"><integer>3600</integer></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/mdf_input_and_conds_hlt2.py</text>
  <text>$HLT2CONFROOT/options/fest/hlt2_all_lines_with_reco_with_streams.py</text>
  <text>--output=hlt2_all_lines_with_reco_with_streams.opts.py</text>
  <text>--all-opt</text>
</set></argument>
<argument name="options"><text>
from Configurables import HiveDataBrokerSvc
HiveDataBrokerSvc().OutputLevel = 5
</text></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings
from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))

import re
matches = re.findall('LAZY_AND: (Hlt2[A-Za-z0-9_]+) .*Sum=(\d+)', stdout)
if not matches:
    causes.append('no line decisions found')

# We're running over 100 HLT1-filtered events in this test.
# If we expect an HLT2 accept rate of of 100 kHz with an HLT1 accept rate of 1 MHz,
# then observing more than 20 positive decisions in 100 events is highly unlikely (consider
# the cdf of the binomial distribution with N=100 and mu=100/1000).
# As HLT2 selections should be rather exclusive, we consider it a failure if a selection
# exceeds 20 positive decisions.
# (Of course most lines should have an accept rate *well* below 20; this is a sanity check.)
max_expected_decisions = 20
for line, positive_decisions in matches:
    if int(positive_decisions) &gt; max_expected_decisions:
        causes.append('{} positive decisions found for {}'.format(positive_decisions, line))
</text></argument>
</extension>
