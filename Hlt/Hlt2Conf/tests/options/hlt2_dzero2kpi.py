###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.charm.d0_to_hh import dzero2kpi_line

from RecoConf.hlt1_tracking import default_ft_decoding_version

options.output_file = "hlt2_D0_Kpi_100evts_newPacking_newDst.dst"
options.output_type = "ROOT"
options.output_manifest_file = "hlt2_D0_Kpi_100evts_newPacking_newDst.tck.json"
options.evt_max = 100
default_ft_decoding_version.global_bind(value=6)
options.set_input_and_conds_from_testfiledb(
    'upgrade-magdown-sim10-up08-27163003-digi')


def make_lines():
    return [dzero2kpi_line()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
