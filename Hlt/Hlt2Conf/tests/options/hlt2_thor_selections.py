###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Example configuration of ThOr-based LHCb::Particle selection algorithms.

The thresholds in each selection algorithm are designed to accept and reject at
least some events to verify that the functors are not simply accepting or
rejecting all input.
"""
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import mm, GeV, MeV
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter,
                                      PVFilter)
from Hlt2Conf.standard_particles import make_long_pions
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import (
    make_pvs,
    reconstruction,
)


def filter_pv_by_z_coordinate(pvs, min_z, max_z):
    return PVFilter(
        name="filter_pv_by_z",
        Input=pvs,
        Cut=F.FILTER(in_range(min_z, F.Z_COORDINATE @ F.POSITION, max_z)))


def make_lines():
    pions = make_long_pions()
    pvs = make_pvs()
    # We can filter on LHCb::Event::PV
    filtered_pvs = filter_pv_by_z_coordinate(pvs, -200 * mm, 0 * mm)
    # We can filter KeyedContainer of LHCb::Particle
    filtered = ParticleFilter(Input=pions, Cut=F.FILTER(F.PT > 250 * MeV))
    # We can filter the output of ParticleFilter
    filtered = ParticleFilter(
        Input=filtered,
        Cut=F.FILTER(F.require_all(
            F.PT > 750 * MeV,
            F.MINIPCHI2(pvs) > 4,
        )),
    )
    # We can create composites from KeyedContainer of LHCb::Particle
    combination = ParticleCombiner(
        Inputs=[filtered, filtered],
        DecayDescriptor="KS0 -> pi+ pi-",
        CombinationCut=F.require_all(
            F.CHILD(1, F.PT) > 500 * MeV,
            F.CHILD(2, F.PT > 500 * MeV),
            F.SUM(F.PT) > 2 * GeV,
            in_range(450 * MeV, F.MASS, 550 * MeV),
        ),
        CompositeCut=F.BPVFDCHI2(pvs) > 2,
    )
    # We can create composites from the output of the combiner
    dzero = ParticleCombiner(
        Inputs=[combination, pions, pions],
        DecayDescriptor="D0 -> KS0 pi+ pi-",
        Combination12Cut=F.MAXDOCACHI2CUT(10.),
        CombinationCut=F.require_all(
            F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(10.), Indices=(2, 3)),
            F.SUBCOMB(Functor=F.MAXDOCACHI2CUT(10.), Indices=(1, 3)),
            in_range(1500 * MeV, F.MASS, 2500 * MeV),
            F.MAXDOCACHI2CUT(16.),
        ),
        CompositeCut=F.CHI2DOF < 12,
    )
    # We can filter composites from the combiner
    dzero_filtered = ParticleFilter(
        Input=dzero,
        # Cut=F.FILTER(F.PT > 2.5 * GeV),
        Cut=F.FILTER(F.BPVFDCHI2(pvs) > 1),
    )
    return [
        Hlt2Line(
            name="Hlt2FilteredPVsFilteredPions",
            algs=[require_pvs(pvs), filtered_pvs, filtered]),
        Hlt2Line(name="Hlt2FilteredPions", algs=[require_pvs(pvs), filtered]),
        Hlt2Line(
            name="Hlt2CombinedPions", algs=[require_pvs(pvs), combination]),
        Hlt2Line(name="Hlt2CombinedKS0Pions", algs=[require_pvs(pvs), dzero]),
        Hlt2Line(
            name="Hlt2FilteredCombinedKS0Pions",
            algs=[require_pvs(pvs), dzero_filtered]),
    ]


options.evt_max = 10
options.output_file = "hlt2_thor_selections.dst"
options.output_type = "ROOT"
options.output_manifest_file = "hlt2_thor_selections.tck.json"
public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
