###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test if streaming functionality for Hlt2 or Sprucing works as expected.

"""
import argparse

import GaudiPython as GP
LHCb = GP.gbl.LHCb
from GaudiConf import IOExtension
from Configurables import (
    ApplicationMgr,
    CondDB,
    LHCbApp,
    IODataManager,
    HistogramPersistencySvc,
)

from GaudiConf.reading import do_unpacking, load_manifest
from PyConf.application import configured_ann_svc

from Hlt2Conf.check_output import check_particlesandrelations, check_decreports


def error(msg):
    print("CheckOutput ERROR", msg)


##Helper functions for returning routing bits
def routing_bits():
    """Return a list with the 96 routing bit values."""
    rawevent = TES['/Event/DAQ/RawEvent']
    rbbanks = rawevent.banks(LHCb.RawBank.HltRoutingBits)
    assert rbbanks.size() == 1  # stop if we have multiple rb banks
    on_bits = []
    for bank in range(0, len(rbbanks)):
        d = rbbanks[bank].data()
        bits = "{:032b}{:032b}{:032b}".format(d[2], d[1], d[0])
        ordered = list(map(int, reversed(bits)))
        for i in range(0, len(ordered)):
            if ordered[i] == 1:
                on_bits.append(str(i))

    return on_bits


#Argument parser
parser = argparse.ArgumentParser()
parser.add_argument('input', help='Input filename')
parser.add_argument('manifest', help='JSON manifest dump')
parser.add_argument('input_process', help='Hlt2 or Spruce')
parser.add_argument('stream', help='Stream to test as defined in options')

args = parser.parse_args()

#Check job configuration
if not (args.input_process == "Hlt2" or args.input_process == "Spruce"):
    error("input_process not supported")

cfg = load_manifest(args.manifest)

##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
CondDB(Upgrade=True)

algs = do_unpacking(cfg, input_process=args.input_process, stream=args.stream)

mgr = ApplicationMgr(TopAlg=algs)
mgr.ExtSvc += [configured_ann_svc()]

IOExtension().inputFiles([args.input], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

#Raw event location
if args.input_process == "Hlt2":
    raweventloc = '/Event/DAQ/RawEvent'
else:
    raweventloc = '/Event/' + args.stream

TES.dump()

while True:
    if not TES['/Event']:
        break

    print("Looking in RawEvent ", raweventloc)
    # Check that RawBank configuration per stream is working
    Rich_banks = TES[raweventloc].banks(LHCb.RawBank.Rich)
    print("Rich_banks ", Rich_banks.size())
    if "A" in args.stream and Rich_banks.size() == 0:
        error("Expected RICH rawbanks in this stream")
    if "B" in args.stream and Rich_banks.size() != 0:
        error("Expected no RICH rawbanks in this stream")

    # Check that routing bits per stream are working
    if args.input_process == "Hlt2":
        on_bits = routing_bits()
        print(on_bits)
        if "A" in args.stream:
            if '85' not in on_bits:
                error("Expected bit 85 in stream A")
            if '90' in on_bits:
                error("Did NOT expect bit 90 in stream A")
        elif "B" in args.stream:
            if '90' not in on_bits:
                error("Expected bit 90 in stream B")
            if '85' in on_bits:
                error("Did NOT expect bit 85 in stream B")
        else:
            error("Only streams A and B supported")

    # Check dec reports
    dec_to_check = args.input_process + '_' + args.stream + '_line'

    if args.input_process == 'Hlt2':
        hlt2decisions = check_decreports(TES, decs=[dec_to_check])
        for k, v in hlt2decisions.items():
            if v:
                prefix = '/Event/HLT2/' + k
                check_particlesandrelations(TES,
                                            prefix.removesuffix("Decision"))
    if args.input_process == "Spruce":
        hlt2decisions = check_decreports(
            TES, decs=['Hlt2Topo2Body', 'Hlt2Topo3Body'])
        sprucedecisions = check_decreports(
            TES, decs=[dec_to_check], stage='Spruce')
        for k, v in sprucedecisions.items():
            if v:
                prefix = '/Event/Spruce/' + k
                check_particlesandrelations(TES,
                                            prefix.removesuffix("Decision"))

    # Check rec summary
    if args.input_process == "Hlt2":
        rec_summary = TES["/Event/HLT2/Rec/Summary"]
        print(" getting TES['/Event/HLT2/Rec/Summary']")
    elif args.input_process == "Spruce":
        rec_summary = TES["/Event/Spruce/HLT2/Rec/Summary"]
        print(" getting TES['/Event/Spruce/HLT2/Rec/Summary']")
    else:
        rec_summary = None
        print("its none")
    if not rec_summary:
        error("Could not find RecSummary for this stream")
    if (rec_summary.info(rec_summary.nTracks, -1) < 1) or (rec_summary.info(rec_summary.nPVs, -1) < 1)\
        or (rec_summary.info(rec_summary.nFTClusters, -1) < 1):
        error("Wrong values in RecSummary for this stream")
        print(rec_summary)

    appMgr.run(1)
