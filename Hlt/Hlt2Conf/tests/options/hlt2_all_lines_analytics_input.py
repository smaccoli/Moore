###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Setup input file for the `hlt2_all_lines_analytics.py` option."""

from Moore import options

options.set_input_and_conds_from_testfiledb('UpgradeHLT1FilteredWithGEC')
options.input_type = 'MDF'
options.evt_max = 100
