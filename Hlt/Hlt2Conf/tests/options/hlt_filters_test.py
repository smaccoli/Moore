###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test HLT1 and HLT2 filters. Produces hlt_filterstest_realtime.mdf

Run like any other options file:

    ./run gaudirun.py hlt_filters_test.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
##Make dummy Hlt2Line
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import upfront_reconstruction
from Hlt2Conf.standard_particles import make_long_pions_for_V0, make_KsLL

from RecoConf.hlt1_tracking import (
    make_SPmixed_raw_banks,
    make_RetinaCluster_raw_bank,
    make_RetinaClusters,
    get_global_measurement_provider,
    make_velo_full_clusters,
    make_VeloClusterTrackingSIMD,
)
from RecoConf.hlt2_tracking import get_UpgradeGhostId_tool
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClustering
from RecoConf.hlt1_tracking import default_ft_decoding_version
ft_decoding_version = 4
default_ft_decoding_version.global_bind(value=ft_decoding_version)

from Configurables import HiveDataBrokerSvc
HiveDataBrokerSvc().OutputLevel = 1

# Input must be specified by a supplemental options file.
# Intended input is the output of allen_mdf_write.qmt, as configured by
#       $HLT1CONFROOT/tests/options/allen_hlt1_mdf_input.py
# To reproduce do
# ./Moore/run gaudirun.py Moore/Hlt/Hlt1Conf/tests/qmtest/persistency.qms/allen_mdf_write.qmt
options.evt_max = 1000
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20180815'
options.conddb_tag = 'sim-20180530-vc-md100'

options.output_file = 'hlt_filterstest_realtime.mdf'
options.output_type = 'MDF'
options.output_manifest_file = "hlt_filterstest_realtime.tck.json"


## Make 2 identical lines except for a Hlt1 filter in `Hlt2Test1`
def make_line1():
    pions = make_long_pions_for_V0()
    return Hlt2Line(
        name="Hlt2Test1",
        algs=upfront_reconstruction() + [make_KsLL(pions)],
        persistreco=True,
        hlt1_filter_code=[
            "Hlt1GECPassthroughDecision", "Hlt1.*fakeDecisions",
            "Hlt1DummyDecision"
        ])


def make_line2():
    pions = make_long_pions_for_V0()
    return Hlt2Line(
        name="Hlt2Test2",
        algs=upfront_reconstruction() + [make_KsLL(pions)],
        persistreco=True)


def make_lines():
    return [make_line1(), make_line2()]


## For retina clusters, see Moore issue 409
make_RetinaCluster_raw_bank.global_bind(make_raw=make_SPmixed_raw_banks)
make_velo_full_clusters.global_bind(
    make_raw=make_SPmixed_raw_banks, make_full_cluster=VPRetinaFullClustering)
make_RetinaClusters.global_bind(make_raw=make_RetinaCluster_raw_bank)
make_VeloClusterTrackingSIMD.global_bind(
    make_raw=make_RetinaCluster_raw_bank,
    algorithm=VeloRetinaClusterTrackingSIMD)
get_UpgradeGhostId_tool.global_bind(velo_hits=make_RetinaClusters)
get_global_measurement_provider.global_bind(velo_hits=make_RetinaClusters)

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
