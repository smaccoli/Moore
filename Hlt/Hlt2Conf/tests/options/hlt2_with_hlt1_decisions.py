###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.options import options
from Moore.config import (moore_control_flow, allen_control_flow,
                          stream_writer)
from RecoConf.hlt1_allen import (
    allen_gaudi_node as allen_sequence,
    call_allen_decision_logger,
)
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from Hlt2Conf.lines.bnoc.hlt2_bnoc import Bds_PhiPhi_line

options.evt_max = 20


def make_lines():
    return [Bds_PhiPhi_line()]


##Run Allen for MC without Retina clusters ("hlt1_pp_veloSP") and log HLT1 decisions
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind(
        from_file=False), stream_writer.bind(write_all_input_leaves=False):

    config = configure_input(options)

    allen_node = allen_control_flow(options, write_all_input_leaves=False)
    hlt1_node = CompositeNode(
        'MooreAllenWithLogger',
        combine_logic=NodeLogic.NONLAZY_AND,
        children=[allen_node, call_allen_decision_logger()],
        force_order=True)

    hlt2_streams = make_lines()
    # Create streams definition if make_streams returned a list
    # For possible HLT2 stream names see  https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/streams_hlt2.py
    # Setting here to full for the test
    if not isinstance(hlt2_streams, dict):
        hlt2_streams = dict(full=hlt2_streams)

    #Note the output file name should include the stream name
    options.output_type = 'ROOT'
    options.output_file = 'HLt1andHlt2_filtered.full.dst'
    options.output_manifest_file = 'HLt1andHlt2_filtered.full.json'

    # Combine all lines and output in a global control flow.
    hlt2_node = moore_control_flow(options, hlt2_streams, "hlt2")

    top_cf_node = CompositeNode(
        'HLT_1and2',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[hlt1_node, hlt2_node],
        force_order=False)

    config.update(
        configure(
            options,
            top_cf_node,
            public_tools=[stateProvider_with_simplified_geom()]))
