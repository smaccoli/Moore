###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test exclusive and passthrough (Turbo) Sprucing output.
"""
import argparse

import GaudiPython as GP
from GaudiConf import IOExtension
from Configurables import (
    ApplicationMgr,
    CondDB,
    LHCbApp,
    IODataManager,
    HistogramPersistencySvc,
)

from PyConf.packing import default_persisted_locations
from GaudiConf.reading import do_unpacking, load_manifest
from PyConf.application import configured_ann_svc
from Hlt2Conf.check_output import (
    check_persistreco,
    check_MCoutput,
    check_banks,
    check_particlesandrelations,
    check_decreports,
)

parser = argparse.ArgumentParser()
parser.add_argument('i', type=str, help='Input MDF or DST')
parser.add_argument('t', type=str, help='.tck.json file from the job')
parser.add_argument('p', type=str, help='input_process can be Spruce or Turbo')
parser.add_argument('s', type=str, help='Stream to test')

args = parser.parse_args()

assert args.p == "Spruce" or args.p == "Turbo", 'input_process is Turbo (passthrough sprucing) or Spruce'

##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
CondDB(Upgrade=True)

input_process = args.p
stream = args.s

if input_process == "Spruce":
    RECO_ROOT = "/Event/Spruce/HLT2"
    dec_to_check = "Spruce_Test_line"

elif input_process == "Turbo":
    RECO_ROOT = "/Event/HLT2"
    dec_to_check = "PassThrough"

algs = do_unpacking(
    load_manifest(args.t),
    input_process=input_process,
    stream=stream,
    simulation=True,
    raw_event_format=0.3)

app = ApplicationMgr(TopAlg=algs)
app.ExtSvc += [configured_ann_svc()]

IOExtension().inputFiles([args.i], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

nevents = 1
for ii in range(nevents):
    print('Checking next event.')
    appMgr.run(1)
    if not TES['/Event']:
        break
    if ii == 0:
        TES.dump()

    # Check dec reports
    hlt2decisions = check_decreports(
        TES, decs=['Hlt2Topo2Body', 'Hlt2Topo3Body'])
    sprucedecisions = check_decreports(
        TES, decs=[dec_to_check], stage='Spruce')

    # Check particles and relations between them
    if input_process == 'Turbo':
        for k, v in hlt2decisions.items():
            if v:
                prefix = '/Event/HLT2/' + k
                check_particlesandrelations(TES,
                                            prefix.removesuffix("Decision"))
    else:
        for k, v in sprucedecisions.items():
            if v:
                prefix = '/Event/Spruce/' + k
                check_particlesandrelations(TES,
                                            prefix.removesuffix("Decision"))

    # Check MC locations if simulation like
    if "_dstinput" in args.i:
        check_MCoutput(TES, RECO_ROOT)

    # Check persistency of packed containers
    locations = default_persisted_locations(stream=RECO_ROOT)
    check_persistreco(TES, locations.values())

    # Check the Rich (=9) RawBank is present
    check_banks(TES, stream, banks=[9])
