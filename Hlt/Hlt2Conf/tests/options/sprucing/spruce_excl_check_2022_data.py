###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test excl sprucing over 2022 data.

"""
import argparse

import cppyy

import GaudiPython as GP
LHCb = GP.gbl.LHCb
from GaudiConf import IOExtension
from Configurables import (
    ApplicationMgr,
    CondDB,
    LHCbApp,
    IODataManager,
    HistogramPersistencySvc,
)

from GaudiConf.reading import unpack_rawevent, hlt_decisions
from PyConf.application import configured_ann_svc

from Hlt2Conf.check_output import check_decreports
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as b2oc_lines
from Hlt2Conf.lines.rd import sprucing_lines as rd_lines
from Hlt2Conf.lines.b_to_open_charm import all_lines as hlt2_b2oc_lines
from Hlt2Conf.lines.rd import all_lines as hlt2_rd_lines


def error(msg):
    print("CheckOutput ERROR", msg)


def spruce_lines_running(wg):
    linedict = {
        "b2oc": b2oc_lines,
        "rd": rd_lines,
    }
    return [item for item in list(linedict[wg].keys())]


def hlt2_lines_running(wg):
    linedict = {
        "b2oc": hlt2_b2oc_lines,
        "rd": hlt2_rd_lines,
    }
    return [item for item in list(linedict[wg].keys())]


#Argument parser
parser = argparse.ArgumentParser()
parser.add_argument('input', type=str, help='Input filename')
parser.add_argument('manifest', type=str, help='JSON manifest dump')
parser.add_argument('job_type', type=str, help='excl or pass')
parser.add_argument(
    'stream', type=str, help='Stream to test as defined in options')

args = parser.parse_args()

print("input ", args.input)
print("manifest ", args.manifest)
print("job_type ", args.job_type)
print("stream ", args.stream)

##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
CondDB(Upgrade=True)

process = "Turbo" if args.job_type == "pass" else "Spruce"
#algs = do_unpacking(cfg, input_process=input_process, stream=args.stream)

unpack = [
    unpack_rawevent(
        bank_types=['ODIN', 'HltDecReports', 'DstData'],
        stream=args.stream,
        input_process=process,
        configurables=True,
    )
]

hlt2 = [
    hlt_decisions(
        stream=args.stream,
        input_process=process,
        source="Hlt2",
        output_loc="/Event/Hlt2/DecReports")
]

spruce = [
    hlt_decisions(
        input_process=process,
        stream=args.stream,
        source="Spruce",
        output_loc="/Event/Spruce/DecReports")
]

algs = unpack + hlt2 + spruce

mgr = ApplicationMgr(TopAlg=algs)
mgr.ExtSvc += [configured_ann_svc(json_file=args.manifest)]

IOExtension().inputFiles([args.input], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

raweventloc = '/Event/' + args.stream

nevents = 5
for ii in range(nevents):
    print('Checking next event.')
    appMgr.run(1)
    if not TES['/Event']:
        break
    if ii == 0:
        TES.dump()

    #print("Looking in RawEvent ", raweventloc)
    # Check that detector RawBanks are propagated
    Rich_banks = TES[raweventloc].banks(LHCb.RawBank.Rich)
    print("Rich_banks ", Rich_banks.size())
    if Rich_banks.size() != 0:
        error("Expected no RICH rawbanks in streams")
    Muon_banks = TES[raweventloc].banks(LHCb.RawBank.Muon)
    print("Muon_banks ", Muon_banks.size())
    if Muon_banks.size() == 0:
        error("Expected Muon rawbanks in all streams")

    # Check dec reports
    if "b2oc" in args.stream:
        if args.job_type == "excl":
            dec_to_check = spruce_lines_running('b2oc')
        else:
            dec_to_check = hlt2_lines_running('b2oc')
        pass_dec_check = ["Passb2oc"]
    elif "rd" in args.stream:
        if args.job_type == "excl":
            dec_to_check = spruce_lines_running('rd')
        else:
            dec_to_check = hlt2_lines_running('rd')
        pass_dec_check = ["Passrd"]

    if args.job_type == 'pass':
        hlt2decisions = check_decreports(TES, decs=dec_to_check)
        hlt2_fired = [k for k, v in hlt2decisions.items() if v]
        print("Fired ", hlt2_fired)
        ## ToDo : reimplement the particles checks
        #for k, v in hlt2decisions.items():
        #    if v:
        #        prefix = '/Event/HLT2/' + k
        #        check_particlesandrelations(TES, prefix)
        sprucedecisions = check_decreports(
            TES, decs=pass_dec_check, stage='Spruce')
        passfired = [k for k, v in sprucedecisions.items() if v]
        print("Fired ", passfired)
    elif args.job_type == "excl":
        sprucedecisions = check_decreports(
            TES, decs=dec_to_check, stage='Spruce')
        sprucefired = [k for k, v in sprucedecisions.items() if v]
        print("Fired ", sprucefired)
        #for k, v in sprucedecisions.items():
        #    if v:
        #        prefix = '/Event/Spruce/' + k
        #        check_particlesandrelations(TES, prefix)
