###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file to test the monitoring
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import (
    make_pvs,
    upfront_reconstruction,
)
from Hlt2Conf.standard_particles import make_long_pions
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

from PyConf import configurable

from PyConf.Algorithms import Monitor__ParticleRange

from Hlt2Conf.algorithms_thor import thor_backend, PARTICLE_V1


def monitor(name, particles, functor, *, histogram_name, range, bins=100):

    assert (len(range) == 2)
    config = dict(
        name=name,
        Input=particles,
        Variable=functor,
        HistogramName=histogram_name,
        Bins=bins,
        Range=range)
    bk = thor_backend()
    if bk == PARTICLE_V1:
        return Monitor__ParticleRange(**config)
    else:
        raise NotImplementedError(
            f'Monitoring not implemented for ThOr backend "{bk}"')


@configurable
def filter_decay_products(particles,
                          pvs,
                          *,
                          max_track_chi2dof=9.,
                          max_ghostprob=0.4,
                          ip_min=0.5 * mm,
                          mipchi2_min=25.):
    cut = F.require_all(
        F.CHI2DOF < max_track_chi2dof,
        F.GHOSTPROB < max_ghostprob,
        F.MINIP(pvs) > ip_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def make_kaons(decay_descriptor,
               inputs,
               pvs,
               *,
               mass_min=400. * MeV,
               mass_max=600. * MeV,
               two_body_comb_maxdocachi2=9.,
               comb_maxdoca=0.4 * mm,
               vchi2pdof_max=9.):

    combination_code = F.require_all(
        in_range(mass_min, F.MASS, mass_max),
        F.MAXDOCACUT(comb_maxdoca),
        F.MAXDOCACHI2CUT(two_body_comb_maxdocachi2),
    )
    vertex_code = (F.CHI2DOF < vchi2pdof_max)
    return ParticleCombiner(
        inputs,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


all_lines = {}


@register_line_builder(all_lines)
@configurable
def ks02pipi_line(name="Hlt2KS02pipi", prescale=1):

    pvs = make_pvs()
    pions = filter_decay_products(make_long_pions(), pvs)
    kaons = make_kaons('KS0 -> pi+ pi-', [pions, pions], pvs)

    mass_min, mass_max = 400. * MeV, 600. * MeV

    # this will monitor all the pions satisfying the basic requirements
    # from events triggered by this line
    precomb_pions_pt_monitor = monitor(
        f'{name}PionPtMonitor',
        pions,
        F.PT,
        histogram_name='pion_pt',
        range=(0., 1e3 * MeV))
    mass_monitor = monitor(
        f'{name}MassMonitor',
        kaons,
        F.MASS,
        histogram_name='mass',
        range=(mass_min, mass_max))
    momenta_sum_monitor = monitor(
        f'{name}SquaredFunctorSumMonitor',
        kaons,
        F.MASS**2 + F.PT**2,
        histogram_name='mass',
        range=(mass_min, mass_max))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [kaons, precomb_pions_pt_monitor, mass_monitor, momenta_sum_monitor],
        prescale=prescale,
    )


# Moore configuration
from Moore import options, run_moore

# Temporary workaround for TrackStateProvider
from RecoConf.global_tools import stateProvider_with_simplified_geom

options.set_input_and_conds_from_testfiledb('Upgrade_MinBias_LDST')
options.input_raw_format = 4.3
options.evt_max = 1000
options.histo_file = 'hlt2_histos.root'

run_moore(options, lambda: [ks02pipi_line()],
          [stateProvider_with_simplified_geom()])
