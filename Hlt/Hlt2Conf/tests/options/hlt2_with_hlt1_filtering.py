###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Demonstrate filtering on HLT1 decisions written by Allen.

This is not intended as an example of how to do HLT1 filtering in HLT2, as
that is not yet supported natively in Moore. This test configures the
necessary algorithms by hand.
"""
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from PyConf.components import unique_name_ext_re

from RecoConf.global_tools import stateProvider_with_simplified_geom


def filter_hlt1_line(name="Hlt2FilterHlt1", prescale=1):
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=[],
        hlt1_filter_code="Hlt1LowPtMuon" + unique_name_ext_re() + "Decision")


def make_lines():
    return [filter_hlt1_line()]


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
