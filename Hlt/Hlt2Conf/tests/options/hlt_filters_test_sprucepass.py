###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test HLT1 and HLT2 filters. Produces spruce_filterstest.dst

To check whether a SpruceLine with Hlt1 and Hlt2 filters gives same results
as a SpruceLine with a Hlt2 filter where the Hlt2 line has a Hlt1 filter

Run like any other options file:

    ./Moore/run gaudirun.py spruce_filters_test.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction, upfront_reconstruction
##Make dummy SpruceLine
from Moore.lines import SpruceLine
from Hlt2Conf.standard_particles import make_long_pions_for_V0, make_KsLL

# Run over output of
# ./run gaudirun.py Hlt/Hlt2Conf/tests/options/hlt_filters_test.py
input_files = ['hlt_filterstest_realtime.mdf']

options.input_raw_format = 0.3
options.input_files = input_files
options.input_manifest_file = "hlt_filterstest_realtime.tck.json"
options.input_type = 'MDF'

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'spruce_filterstest.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_filterstest.tck.json"


## Make 2 identical lines except for filters
def make_line1(name='SpruceTest1'):
    pions = make_long_pions_for_V0()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [make_KsLL(pions)],
        hlt2_filter_code='Hlt2Test1Decision')


def make_line2(name='SpruceTest2'):
    pions = make_long_pions_for_V0()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [make_KsLL(pions)],
        hlt1_filter_code='Hlt1GECPassthroughDecision',
        hlt2_filter_code='Hlt2Test2Decision')


def make_lines():
    return [make_line1(), make_line2()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(options, make_lines, public_tools)
