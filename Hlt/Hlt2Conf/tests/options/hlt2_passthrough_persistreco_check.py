###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test if packed reco objects are persisted and persistreco flag works
as expected when pass through line runs with `persistreco`.
Also tests the unpacking of the tracks from pRec/ to Rec/.

Runs over the output file passed as the last argument to this script.

A cleverer version of this script would check the number of packed objects
against the number of objects created upstream in Moore. But we cannot yet
run a GaudiPython job in Moore to get the container counts (see LBCOMP-101),
so instead we use heuristics to estimate the number of objects we should
expect.

"""
import argparse

import GaudiPython as GP
from GaudiConf import IOExtension
from Configurables import (ApplicationMgr, LHCbApp, IODataManager,
                           HistogramPersistencySvc)
from GaudiConf.reading import load_manifest
from GaudiConf.reading import do_unpacking
from PyConf.application import configured_ann_svc

from Hlt2Conf.check_output import check_persistreco, check_decreports
from PyConf.packing import default_persisted_locations

parser = argparse.ArgumentParser()
parser.add_argument('input', help='Input filename')
parser.add_argument('hlt2_tck', help='HLT2 JSON TCK dump')
args = parser.parse_args()

algs = do_unpacking(load_manifest(args.hlt2_tck), input_process='Hlt2')

##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)

appmgr = ApplicationMgr(TopAlg=algs)
appmgr.ExtSvc += [configured_ann_svc()]

IOExtension().inputFiles([args.input], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)


def error(msg):
    print("Check ERROR", msg)


unexpected_locs = [
    'SplitPhotons', 'Neutrals', 'MergedPi0s', 'Electrons', 'Photons',
    'Rec/Summary'
]

#As this is a pass through line with persistreco
N_TURBO = 90

for ii in range(2):

    locations = default_persisted_locations(stream="/Event/HLT2")
    check_persistreco(TES, locations.values(), N_TURBO, unexpected_locs)

    check_decreports(TES, decs=['Hlt2Passthrough'])

    if TES['/Event/HLT2/Rec/Summary'].size() < 1:
        error("RecSummary not persisted.")
    print("/Event/HLT2/Rec/Summary has size ",
          TES['/Event/HLT2/Rec/Summary'].size())

    appMgr.run(1)
