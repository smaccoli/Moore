###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from collections import defaultdict
import sys

from GaudiConf.QMTest.LHCbTest import extract_counters

with open(sys.argv[-1]) as f:
    stdout = f.readlines()
counters = extract_counters(stdout)
filter_counters = defaultdict(dict)
combiner_counters = defaultdict(dict)
vertex_counters = defaultdict(dict)


def checkAlgName(name, start, end):
    name = name[:name.rfind("_")]  # drop the unique hex part of the name
    if name.startswith(start) and name.endswith(end):
        name[len(start):-len(end)]
    return None


for alg_name in counters:
    for algoType in ("Filter", "Combination", "Vertex"):
        for functorType in ("LoKi", "ThOr"):
            functor_name = checkAlgName(alg_name, functorType + "Compare",
                                        algoType)
            if functor_name:
                filter_counters[functor_name][functorType] = counters[alg_name]

fail = False
print("Checking filter counters")
for functor_name in sorted(filter_counters):
    loki_eff = filter_counters[functor_name]["LoKi"]["efficiency"]
    thor_eff = filter_counters[functor_name]["ThOr"][
        "Cut selection efficiency"]
    if loki_eff != thor_eff:
        print("Test ERROR {}".format(functor_name))
        print("           Reason: Counter differences")
        print("           LoKi: {}".format(loki_eff))
        print("           ThOr: {}".format(thor_eff))
        fail = True
    else:
        # To check the functors aren't just accepting or rejecting all inputs
        # we check that only the ALL/NONE functors do this; all other cuts
        # should be tuned such that *some* objects are rejected but not *all*
        # (Don't need to also check `thor_eff` as this block is only entered
        # when `loki_eff` == `thor_eff`)
        rejected_all = loki_eff[1] == "0"
        accepted_all = loki_eff[0] == loki_eff[1]
        if rejected_all and functor_name != "NONE":
            print("Test ERROR {}".format(functor_name))
            print("           Reason: Rejected all inputs")
        elif accepted_all and functor_name != "ALL":
            print("Test ERROR {}".format(functor_name))
            print("           Reason: Accepted all inputs")
        else:
            print("Test SUCCESS {}".format(functor_name))

print("Checking combiner counters")
for functor_name in sorted(combiner_counters):
    # Strip off the efficiency percentage as they are formatted differently
    # between the two algorithms
    loki_eff = combiner_counters[functor_name]["LoKi"]["#pass combcut"][:-1]
    thor_eff = combiner_counters[functor_name]["ThOr"][
        "# passed CombinationCut"][:-1]
    if loki_eff != thor_eff:
        print("Test ERROR {}".format(functor_name))
        print("           Reason: Counter differences")
        print("           LoKi: {}".format(loki_eff))
        print("           ThOr: {}".format(thor_eff))
        fail = True
    else:
        rejected_all = loki_eff[1] == "0"
        accepted_all = loki_eff[0] == loki_eff[1]
        if rejected_all and functor_name != "NONE":
            print("Test ERROR {}".format(functor_name))
            print("           Reason: Rejected all inputs")
        elif accepted_all and functor_name != "ALL":
            print("Test ERROR {}".format(functor_name))
            print("           Reason: Accepted all inputs")
        else:
            print("Test SUCCESS {}".format(functor_name))

print("Checking vertex counters")
for functor_name in sorted(vertex_counters):
    if functor_name[:
                    8] == "BPVLTIME":  #known differences in lifetime functor, return to testing later
        continue
    # Strip off the efficiency percentage as they are formatted differently
    # between the two algorithms
    loki_eff = vertex_counters[functor_name]["LoKi"]["#pass mother cut"][:-1]
    thor_eff = vertex_counters[functor_name]["ThOr"][
        "# passed CompositeCut"][:-1]
    if loki_eff != thor_eff:
        print("Test ERROR {}".format(functor_name))
        print("           Reason: Counter differences")
        print("           LoKi: {}".format(loki_eff))
        print("           ThOr: {}".format(thor_eff))
        fail = True
    else:
        rejected_all = loki_eff[1] == "0"
        accepted_all = loki_eff[0] == loki_eff[1]
        if rejected_all and functor_name != "NONE":
            print("Test ERROR {}".format(functor_name))
            print("           Reason: Rejected all inputs")
        elif accepted_all and functor_name != "ALL":
            print("Test ERROR {}".format(functor_name))
            print("           Reason: Accepted all inputs")
        else:
            print("Test SUCCESS {}".format(functor_name))

sys.exit(int(fail))
