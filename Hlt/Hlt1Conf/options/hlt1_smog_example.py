###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from Hlt1Conf.settings import smog_lines

test_files = {
    'pgas':
    "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_290-Extended.digi",
    'pp':
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/DIGI/00091069/0000/00091069_00000010_1.digi",
    'pp_pgas':
    "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGppMB/digi/SMOGppMB_490-Extended.digi"
}

which_sample = "pgas"

options.input_files = [test_files[which_sample]]
options.input_type = "ROOT"
options.dddb_tag = "dddb-20190223"
options.conddb_tag = "sim-20180530-vc-mu100"
options.evt_max = 1000

run_moore(options, smog_lines)
