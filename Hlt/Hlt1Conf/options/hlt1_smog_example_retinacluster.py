###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from Hlt1Conf.settings import smog_lines

test_sample = {
    'pgas': "SMOG2_pHe_retinacluster",
    'pp': "upgrade_DC19_01_MinBiasMU_retinacluster",
    'pp_pgas': "SMOGHepp8MB_retinacluster",
}

which_sample = "pgas"

options.set_input_and_conds_from_testfiledb(test_sample[which_sample])
options.evt_max = 1000

run_moore(options, smog_lines)
