<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Run Allen HLT1 and check secondary vertexes against SelReports with TOSFilters.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/default_input_and_conds_hlt1_retinacluster.py</text>
  <text>$HLT1CONFROOT/tests/options/allen_vtxs_tos.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>
from Moore.qmtest.exclusions import preprocessor
countErrorLines({
    "FATAL": 0,
    "ERROR": 0,
    "WARNING": 0
},
                stdout=preprocessor(stdout))

# Check that:
#   1. For multi-track Hlt1 lines, the number of events passing the
#      respective TOS filter is equal to the number of events selected by
#      the line.
#   2. Vertexes are never TOS on Beam, ODIN, and PassThrough lines

import re
from GaudiConf.QMTest.LHCbTest import extract_counters
from PyConf.components import findCounters

logctrs = extract_counters(stdout)
decctrs = findCounters(logctrs, 'GaudiAllenCountAndDumpLineDecisions')

# Search patterns
lines_to_check = r'(Hlt1(?:TwoTrack|.*DiMuon|D2|KsToPiPi)\w*Decision)'
dec_pattern = re.compile('Selected by ' + lines_to_check)
tos_pattern = re.compile(lines_to_check + 'TOS')
tec_pattern = re.compile(
    r'(Hlt1(?:ODIN|.*Beam|(?:.*Passthrough)).*Decision)TOS')

# Check that the number of TOS events is as expected
something = False
tos_ctr_key = 'Event selection efficiency'
for alg, ctrs in logctrs.items():
    mt = tos_pattern.match(alg)
    if mt:
        something = True
        dec_key = 'Selected by ' + mt.group(1)
        decs = decctrs.get(dec_key)
        if decs:
            trigs = int(decs[1])
            filts = int(ctrs[tos_ctr_key][1])
            if trigs != filts:
                causes.append(
                    'expected same number of line decisions and TOS decisions for '
                    + alg)
        else:
            causes.append('expected line counter for ' + alg)
    mc = tec_pattern.match(alg)
    if mc and int(ctrs[tos_ctr_key][1]) > 0:
        causes.append('expected no TOS decisions for ' + alg)

if not something:
    causes.append('complete failure to find a TOS filter matching pattern')

something = False
# Check that each expected line had TOS check
for line in decctrs.keys():
    md = dec_pattern.match(line)
    if md:
        something = True
        filts = md.group(1) + 'TOS'
        found = False
        for k in logctrs:
            if k.startswith(filts):
                found = True
                break
        if not found:
            causes.append('expected TOS check for trigger ' + line)

if not something:
    causes.append(
        'complete failure to find a decision report matching pattern')
</text></argument>
</extension>
