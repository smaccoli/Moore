###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from Moore.config import get_allen_hlt1_decision_ids
from RecoConf.hlt1_allen import (
    make_allen_sel_reports, make_allen_forward_tracks,
    call_allen_decision_logger, make_allen_decision, allen_gaudi_node)
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from Allen.config import setup_allen_non_event_data_service


def make_tos_filter(line_name):
    from PyConf.Algorithms import TOSFilter__v1__Track as TOSFilter
    from PyConf.Tools import ParticleTisTos

    return TOSFilter(
        name=line_name + 'TOS',
        InputContainer=make_allen_forward_tracks()['v1'],
        SelReports=make_allen_sel_reports(),
        DecisionPattern=line_name,
        ParticleTisTosTool=ParticleTisTos(
            TOSFracVP=1.0, TOSFracFT=1.0, TOSFracMuon=1.0))


def setup_tos_node():
    hlt1_line_ids = get_allen_hlt1_decision_ids()
    filt_algs = [
        make_tos_filter(line_name) for line_name in hlt1_line_ids.keys()
    ]

    return CompositeNode(
        'tosfilters', combine_logic=NodeLogic.NONLAZY_OR, children=filt_algs)


def run_test():
    """
    Final configuration for the test application
    """
    non_event_data_node = setup_allen_non_event_data_service()
    config = configure_input(options)

    # Get Allen configuration
    allen_node = allen_gaudi_node()

    # Get the configured TOS filters
    tos_node = setup_tos_node()

    # Make a complete control flow
    test_node = CompositeNode(
        'TestAllenFwdTracksTOS',
        combine_logic=NodeLogic.LAZY_AND,
        children=[
            non_event_data_node, allen_node,
            call_allen_decision_logger(),
            make_allen_decision(), tos_node
        ],
        force_order=True)

    # Finalize configuration
    config.update(configure(options, test_node))

    return config


# Script main
from RecoConf.hlt1_tracking import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)

# Expand algorithm name field
options.msg_svc_format = '% F%70W%S %7W%R%T %0W%M'

run_test()
