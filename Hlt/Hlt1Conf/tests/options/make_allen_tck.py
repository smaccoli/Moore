###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import get_allen_hlt1_decision_ids
from AllenConf.persistency import register_decision_ids

# raw_event_format must be configured to successfully configure Allen
# Configure with the same input as the default for Allen tests.
from hlt1_filtered_mdf_input import options
from PyConf.application import configure_input
config = configure_input(options)
key = register_decision_ids(get_allen_hlt1_decision_ids())
print(key)
if key: print('PASSED')
