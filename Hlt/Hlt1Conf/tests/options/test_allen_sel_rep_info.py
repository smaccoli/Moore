###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Check the contents of SelReport

Takes two outputs from previously-run jobs:
  1. The MDF
  2. The TCK in TCKData/config.cdb
For the following classes of Hlt1 lines, the script checks that the type,
heirarchy, and stdinfo of the object summaries stored in SelReports are
consistent with expectations:
  1. Lines not triggered by reconstructed objects (Passthrough, ODIN, etc.)
  2. Lines triggered on single Track objects
  3. Lines triggered on simple RecVertex objects
"""
import argparse
import re
from collections import defaultdict
from Configurables import (ApplicationMgr, HistogramPersistencySvc,
                           IODataManager, LHCbApp)
from GaudiConf import IOHelper
import GaudiPython
from PyConf.application import configured_ann_svc

# CLIDs of summarized objects
# QUESTION:  Are these available through GaudiPython?
clid_Track = 10010  # defined in LHCb/TrackEvent/Track_v1.h
clid_RecVertex = 10030  # defined in LHCb/Event/RecEvent//RecVertex.h

# Set of info keys expected for v4 summary of LHCb::Track as defined in
# LHCb/Hlt/HltDAQ/ReportConvertTool.cpp
# QUESTION: Is this list available through GaudiPython?
s_track_unordered_map2_4 = set([
    "0#Track.firstState.z", "1#Track.firstState.x", "2#Track.firstState.y",
    "3#Track.firstState.tx", "4#Track.firstState.ty",
    "5#Track.firstState.qOverP", "6#Track.chi2PerDoF", "7#Track.nDoF"
])
# Set of info keys expected for v4 summary of LHCb::RecVertex as defined in
# LHCb/Hlt/HltDAQ/ReportConvertTool.cpp
s_recvertex_unordered_map2_4 = set([
    "0#RecVertex.position.x", "1#RecVertex.position.y",
    "2#RecVertex.position.z", "3#RecVertex.chi2"
])

parser = argparse.ArgumentParser()
parser.add_argument("--input-mdf", help="Input MDF file")
parser.add_argument(
    "--track-lines-regex",
    default='(Hlt1(?:Track|Single|LowPtMuon).*Decision)',
    help=
    "Regex specifying names of lines to be tested for single track contents.")
parser.add_argument(
    "--recvertex-lines-regex",
    default='(Hlt1(?:TwoTrack|.*DiMuon|D2|KsToPiPi).*Decision)',
    help="Regex specifying names of lines to be tested for RecVertex contents."
)
parser.add_argument(
    "--objectless-lines-regex",
    default='(Hlt1(?:ODIN|.*Beam|(?:.*Passthrough)).*Decision)',
    help="Regex specifying names of lines to be tested for absence of contents."
)
args = parser.parse_args()

## Dictionary for logging issues.  Reported at end of script
issues = defaultdict(int)


def check_track_line_summary(objSummary, prefix=''):
    """
    Check consistency of object summary with expected contents for a SelReport
    for a line based on single track objects.
    """
    check = True
    subsr = objSummary.substructure()
    if subsr.size() <= 0:
        check = False
        problem = "{} no summarized objects".format(prefix)
        issues[problem] += 1

    check = check and all([check_track_info(a.data(), prefix) for a in subsr])
    return check


def check_track_info(objSummary, prefix=''):
    """
    Check consistency of object summary with expected contents for LHCb::Track
    """
    return check_track_clid(objSummary, prefix) and check_track_info_keys(
        objSummary, prefix)


def check_track_clid(objSummary, prefix=''):
    """
    Check that the summarized object has the expected CLID for a track
    """
    clid_obj = objSummary.summarizedObjectCLID()
    if not clid_obj == clid_Track:
        problem = "{} unexpected track object CLID {}".format(prefix, clid_obj)
        issues[problem] += 1
        return False

    return True


def check_track_info_keys(objSummary, prefix=''):
    """
    Check that the summarized object has the expected set of info keys for a
    track.
    """
    mismatch_keys = s_track_unordered_map2_4.symmetric_difference(
        set(objSummary.numericalInfoKeys()))
    if len(mismatch_keys) > 0:
        problem = "{} missing/unexpected track info keys {}".format(
            prefix, mismatch_keys)
        issues[problem] += 1
        return False

    return True


def check_recvertex_line_summary(objSummary, prefix=''):
    """
    Check consistency of object summary with expected contents for a SelReport
    for a line based on a single vertex composed of two or more tracks.
    """
    check = True
    subsr = objSummary.substructure()
    if subsr.size() <= 0:
        check = False
        problem = "{} no summarized objects".format(prefix)
        issues[problem] += 1

    check = check and all(
        [check_recvertex_info(a.data(), prefix) for a in subsr])
    return check


def check_recvertex_info(objSummary, prefix=''):
    """
    Check consistency of object summary with expected contents for
    LHCb::RecVertex
    """
    subsr = objSummary.substructure()
    check = (check_recvertex_clid(objSummary, prefix)
             and check_recvertex_info_keys(objSummary, prefix)
             and check_recvertex_nsubs(objSummary, prefix)
             and all([check_track_info(a.data(), prefix) for a in subsr]))
    return check


def check_recvertex_clid(objSummary, prefix=''):
    """
    Check that the summarized object has the expected CLID for a track
    """
    clid_obj = objSummary.summarizedObjectCLID()
    if not clid_obj == clid_RecVertex:
        problem = "{} unexpected RecVertex object CLID {}".format(
            prefix, clid_obj)
        issues[problem] += 1
        return False

    return True


def check_recvertex_info_keys(objSummary, prefix=''):
    """
    Check that the summarized object has the expected set of info keys for a
    RecVertex.
    """
    mismatch_keys = s_recvertex_unordered_map2_4.symmetric_difference(
        set(objSummary.numericalInfoKeys()))
    if len(mismatch_keys) > 0:
        problem = "{} missing/unexpected RecVertex info keys {}".format(
            prefix, mismatch_keys)
        issues[problem] += 1
        return False

    return True


def check_recvertex_nsubs(objSummary, prefix=''):
    """
    Check that a summary object has the expected number of subtructure summary
    objects (Tracks) for a RecVertex.
    """
    ntrks = objSummary.substructure().size()
    if ntrks < 2:
        problem = "{} missing/unexpected number of subobjects ({}) for RecVertex".format(
            prefix, ntrks)
        issues[problem] += 1
        return False

    return True


def check_no_object_line_summary(objSummary, prefix=''):
    """
    Check consistency of object summary with expected contents for a SelReport
    for a line that is not based on reconstructed objects.
    """
    subsr = objSummary.substructure()
    if subsr.size() > 0:
        problem = "{} unexpected summarized objects".format(prefix)
        issues[problem] += 1
        return False

    return True


# Configure basic application with inputs
LHCbApp(DataType="Upgrade", Simulation=True)
IOHelper("MDF").inputFiles([args.input_mdf])
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)
# Decode Hlt DecReports
from Configurables import LHCb__UnpackRawEvent, HltDecReportsDecoder, HltSelReportsDecoder
unpacker = LHCb__UnpackRawEvent(
    "UnpackRawEvent",
    RawBankLocations=[
        "DAQ/RawBanks/HltDecReports", "DAQ/RawBanks/HltSelReports"
    ],
    BankTypes=["HltDecReports", "HltSelReports"])

decDec = HltDecReportsDecoder(
    "HltDecReportsDecoder/Hlt1DecReportsDecoder",
    SourceID="Hlt1",
    DecoderMapping="TCKANNSvc",
    RawBanks=unpacker.RawBankLocations[0])

selDec = HltSelReportsDecoder(
    "HltDecReportsDecoder/Hlt1SelReportsDecoder",
    SourceID="Hlt1",
    DecoderMapping="TCKANNSvc",
    RawBanks=unpacker.RawBankLocations[1])

app = ApplicationMgr(TopAlg=[unpacker, decDec, selDec])

# Configure TCKANNSvc
app.ExtSvc += [configured_ann_svc(name='TCKANNSvc')]

gaudi = GaudiPython.AppMgr()
TES = gaudi.evtSvc()
gaudi.run(1)

print('Applying single-track tests to lines matching ', args.track_lines_regex)
single_track_pattern = re.compile(args.track_lines_regex)

print('Applying RecVertex line tests to lines matching ',
      args.recvertex_lines_regex)
recvertex_pattern = re.compile(args.recvertex_lines_regex)

print('Applying objectless line tests to lines matching ',
      args.objectless_lines_regex)
objectless_pattern = re.compile(args.objectless_lines_regex)

while TES["/Event"]:
    sels = TES[str(selDec.OutputHltSelReportsLocation)]
    if not sels:
        issues["SelReports TES location not found"] += 1
        continue

    for key in sels.selectionNames():
        sr = sels.selReport(key)
        if single_track_pattern.match(str(key)):
            check_track_line_summary(sr, key)
        if recvertex_pattern.match(str(key)):
            check_recvertex_line_summary(sr, key)
        if objectless_pattern.match(str(key)):
            check_no_object_line_summary(sr, key)

    gaudi.run(1)

print('Issues found: ', issues)

if len(issues) > 0:
    exit("Test failed")  # exit with a non-zero code
