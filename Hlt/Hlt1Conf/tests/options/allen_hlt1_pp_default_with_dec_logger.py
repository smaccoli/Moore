###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.options import options
from Moore.config import allen_control_flow
from RecoConf.hlt1_allen import call_allen_decision_logger
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic

config = configure_input(options)
allen_node = allen_control_flow(options)

top_cf_node = CompositeNode(
    'MooreAllenWithLogger',
    combine_logic=NodeLogic.NONLAZY_AND,
    children=[allen_node, call_allen_decision_logger()],
    force_order=True)

config.update(configure(options, top_cf_node))
