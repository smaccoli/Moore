###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt1Conf.lines.track_mva import (
    one_track_mva_line,
    two_track_mva_line,
)
from Hlt1Conf.lines.track_muon import one_track_muon_mva_line, low_mass_dimuon_line, high_mass_dimuon_line
from Hlt1Conf.lines.low_pt_muon import detached_low_pt_dimuon_line
from Hlt1Conf.lines.high_pt_muon import one_track_muon_highpt_line
# from Hlt1Conf.lines.gec_passthrough import gec_passthrough_line
from Hlt1Conf.lines.smog_lines import smog_minimumbias_line, smog_dimuon_line
from Hlt1Conf.lines.minimum_bias import (
    low_multiplicity_velo_line,
    no_bias_line,
)


def track_mva_lines():
    return [
        one_track_mva_line(),
        two_track_mva_line(),
    ]


def muon_lines():
    return [
        one_track_muon_mva_line(),
        low_mass_dimuon_line(),
        high_mass_dimuon_line(),
        detached_low_pt_dimuon_line(),
    ]


def smog_lines():
    return [smog_minimumbias_line(), smog_dimuon_line()]


def minimum_bias_lines():
    return [
        low_multiplicity_velo_line(),
        no_bias_line(),
    ]


def all_lines():
    return track_mva_lines() + muon_lines() + minimum_bias_lines()


def comparison_lines():
    """Settings for Allen-x86 comparison.

    This is a kind of "common denominator" configuration which aims to
    align x86 and Allen in terms of reconstruction and selections, and
    will be used for throughput and physics performance measurements for
    the technology decision document.

    """
    return [
        one_track_mva_line(),
        two_track_mva_line(),
        one_track_muon_mva_line(),
        low_mass_dimuon_line(),
        high_mass_dimuon_line(),
        low_multiplicity_velo_line(),
        no_bias_line(),
        one_track_muon_highpt_line(),
        #gec_passthrough_line()
    ]
