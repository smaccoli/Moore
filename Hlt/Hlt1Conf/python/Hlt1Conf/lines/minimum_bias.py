###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of minimum bias lines.

Each line has a very minimal set of restrictions on what must be present in the
event, such that the selected events are biased in a very limited way,
typically meant with respect to 'heavy flavour events'.
"""

# Workaround for ROOT-10769
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import cppyy

from PyConf.Algorithms import VoidFilter
from PyConf.application import make_odin
from Moore.config import HltLine

from RecoConf.hlt1_tracking import make_hlt1_tracks

from Functors import EVENTTYPE, SIZE
from Functors import math as fmath


def odin_nobias_filter(make_odin=make_odin):
    nobias_bit = cppyy.gbl.LHCb.ODIN.EventTypes.NoBias
    odin = make_odin()
    return VoidFilter(Cut=fmath.test_bit(EVENTTYPE(odin), nobias_bit))


def low_multiplicity_velo_line(name="Hlt1MicroBiasLowMultVelo",
                               prescale=1,
                               nvelo_tracks_min=5):
    """Accepts NoBias events with at least some number of VELO tracks."""
    nobias = odin_nobias_filter()
    velo_tracks = make_hlt1_tracks()["Velo"]["Pr"]
    ntracks_filter = VoidFilter(Cut=SIZE(velo_tracks) > nvelo_tracks_min)

    return HltLine(name=name, algs=[nobias, ntracks_filter], prescale=prescale)


def no_bias_line(name="Hlt1NoBias", prescale=1):
    """Accepts all crossings marked with the NoBias ODIN bit."""
    nobias = odin_nobias_filter()

    return HltLine(
        name=name,
        algs=[nobias],
        prescale=prescale,
    )
