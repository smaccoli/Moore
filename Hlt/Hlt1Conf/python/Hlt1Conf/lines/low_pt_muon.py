###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import MeV, mm, GeV
from PyConf import configurable
from PyConf.Algorithms import VoidFilter
from Moore.config import HltLine
from RecoConf.event_filters import (
    require_gec,
    require_pvs,
)
from RecoConf.hlt1_tracking import (
    make_pvs,
    make_hlt1_fitted_tracks,
)

from RecoConf.hlt1_muonid import (make_muon_id, make_tracks_with_muon_id)

from RecoConf.hlt1_muonmatch import make_tracks_with_muonmatch_ipcut
from ..algorithms import (CombineTracks, Filter)

from Functors import (ISMUON, MASS, P, PT, CHI2DOF, MAXSDOCACHI2, MINIPCUT,
                      MINIPCHI2CUT, MAXSDOCA, SIZE)


def make_fitted_tracks_with_muon_id(velo_track_min_ip, tracking_min_pt):
    all_tracks = make_tracks_with_muonmatch_ipcut(velo_track_min_ip,
                                                  tracking_min_pt)
    # fit the reconstructed tracks to make fitted tracks
    fitted_forward_tracks = make_hlt1_fitted_tracks(all_tracks)
    # get muonIDs, this needs forward tracks (not fitted forward tracks)
    muon_ids = make_muon_id(all_tracks["Forward"])
    # bind the muonIDs and the fitted forward tracks together
    tracks_with_muon_id = make_tracks_with_muon_id(fitted_forward_tracks,
                                                   muon_ids)
    # return:
    # 1. pr upstream tracks: needed to filter for events with at least 2 upstream tracks (for building dimuon candidates)
    # 2. zipped object of fitted forward tracks and muonIDs: needed by the muon filter
    return {
        'PrUpstream': all_tracks["Upstream"]["Pr"],
        'PrFittedForwardWithMuonID': tracks_with_muon_id
    }


@configurable
def prefilters(make_pvs=make_pvs):
    return [require_gec(), require_pvs(make_pvs())]


@configurable
def detached_low_pt_muon_line(
        name='Hlt1LowPtMuon',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        make_pvs=make_pvs,
        velo_track_min_ip=4. * mm,
        tracking_min_pt=80. * MeV,
        max_chi2dof=100.0,
        min_pt=80.0 * MeV,
        min_ipchi2=7.4):

    pvs = make_pvs()
    sel = (ISMUON) & (PT > min_pt) & MINIPCHI2CUT(
        IPChi2Cut=min_ipchi2, Vertices=pvs) & (CHI2DOF < max_chi2dof)
    tracks_with_muon_id = make_input_tracks(velo_track_min_ip, tracking_min_pt)
    # make selection algorithm object
    trackmuon_filter = Filter(tracks_with_muon_id,
                              sel)['PrFittedForwardWithMuonID']
    return HltLine(
        name=name,
        algs=prefilters() + [trackmuon_filter],
        prescale=prescale,
    )


@configurable
def detached_low_pt_dimuon_line(
        name='Hlt1LowPtDiMuon',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        make_pvs=make_pvs,
        velo_track_min_ip=0.1 * mm,
        tracking_min_pt=80. * MeV,
        min_p=3.0 * GeV,
        min_pt=80.0 * MeV,
        max_track_chi2dof=100.,
        min_track_ipchi2=1.,
        max_doca=0.2 * mm,
        max_vertex_chi2=25.,
        min_mass=220. * MeV):

    pvs = make_pvs()

    tracks_with_muon_id = make_input_tracks(velo_track_min_ip, tracking_min_pt)

    #require that the upstream track container has at least two tracks
    twoupstreamtracks_rec = VoidFilter(
        name='require_twotracks',
        Cut=SIZE(tracks_with_muon_id["PrUpstream"]) > 1)

    # functor for the selection on the individual forward fitted tracks with muonID
    sel = (ISMUON) & (P > min_p) & (PT > min_pt) & MINIPCUT(
        IPCut=velo_track_min_ip, Vertices=pvs) & MINIPCHI2CUT(
            IPChi2Cut=min_track_ipchi2,
            Vertices=pvs) & (CHI2DOF < max_track_chi2dof)
    #get the output of the selection algorithm run over the fitted forward tracks, that will be the input tracks for the combiner
    children = Filter(tracks_with_muon_id, sel)
    # functor for the selection on the combination of 2 forward fitted tracks with muonID
    CombinationCut = (MAXSDOCA < max_doca) & (MAXSDOCACHI2 < max_vertex_chi2)
    # functor for the selection on the vertex of 2 forward fitted tracks with muonID
    VertexCut = (CHI2DOF < max_vertex_chi2) & (MASS > min_mass)
    combination_filter = CombineTracks(
        AssumedMass='mu+',
        NBodies=2,
        VertexCut=VertexCut,
        InputTracks=children['PrFittedForwardWithMuonID'],
        CombinationCut=CombinationCut)
    return HltLine(
        name=name,
        algs=prefilters() + [twoupstreamtracks_rec, combination_filter],
        prescale=prescale,
    )
