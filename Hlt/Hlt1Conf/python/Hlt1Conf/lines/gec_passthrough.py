###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from Moore.config import HltLine
from RecoConf.event_filters import require_gec


@configurable
def gec_prefilters():
    return [require_gec()]


@configurable
def gec_passthrough_line(name='Hlt1GECPassThrough', prescale=1):
    """A line that returns a positive decision if the global event cut passes.

    Can be useful for determining the GEC efficiency of a given physics
    process when run over simulated data.
    """
    return HltLine(name=name, algs=gec_prefilters(), prescale=prescale)
