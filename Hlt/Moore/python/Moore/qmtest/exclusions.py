###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting.BaseTest import LineSkipper
from GaudiConf.QMTest.LHCbTest import GroupMessages, BlockSkipper
from RecConf.QMTest.exclusions import preprocessor as RecPreprocessor

remove_known_warnings = LineSkipper(
    strings=[
        # DD4hep geometry. XXX should be fixed there
        r"The sub volume lvUX851InUT is NOT constructed",
    ],
    regexps=[
        # expected WARNINGs from the data broker
        r"HiveDataBrokerSvc +WARNING non-reentrant algorithm: .*",
        # expected WARNINGs from MuonIDHlt1Alg due to lhcb/Rec#79
        r"MuonID.* +WARNING CondDB {X,Y}FOIParameters member "
        r"size is 20, geometry expects 16",
        r"ToolSvc.CommonMuonTool.* +WARNING CondDB {X,Y}FOIParameters member "
        r"size is 20, geometry expects 16",
        # hard to remove WARNINGs due to TrackResChecker.FullDetail = True
        # https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/783#note_4406625
        (r"ToolSvc.IdealStateCreator +WARNING Extrapolation of True State from"
         r" z = 9[2-4][0-9.]+ to z = 9[2-4][0-9.]+ failed!"),
        # also due to TrackResChecker see
        # https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/2788#note_5399928
        (r"ToolSvc.TrackMasterExtrapolator +WARNING Suppressing message: "
         r"'Protect against absurd tracks. See debug for details'"),
        # expected WARNINGs from MuonUTTracking when extrapolation is failed
        r"ToolSvc.LoKi::VertexFitter +WARNING LoKi::VertexFitter:: Error from Kalman-step, skip *",
        r"MuonUTTracking +WARNING Could not propagate state to VELO!",
        # backwards compatibility -- key is from manifest, not git
        r"key 0x[0-9a-f]+ has an explicitly configured overrule -- using that",
        # backwards compatibility -- old data
        r"DstData raw bank  has a zero encoding key, and it is not explicitly specified for decoding",
        r"HltDecReports has a zero TCK, and it is not explicitly specified for decoding",
        # Known warnings with dd4hep
        r".* WARNING TransportSvc is currently incompatible with DD4HEP.*",
        r".* WARNING See https://gitlab.cern.ch/lhcb/Rec/-/issues/326 for more details",
        # known issues with 2022 data
        r"FTRawBankDecoder(_[0987654321abcdef]*)?\s+ERROR .*Odd bank",
    ])

preprocessor = remove_known_warnings + LineSkipper(regexps=[
    # output from the scheduler that is expected to differ run-by-run
    r"HLTControlFlowMgr                      INFO Timing started at: [0-9]+:[0-9]+:[0-9]+",
    r"HLTControlFlowMgr                      INFO Timing stopped at: [0-9]+:[0-9]+:[0-9]+",
    r"HLTControlFlowMgr\s+INFO ---> End of Initialization. This took [0-9\.]+ ms",
    r"HLTControlFlowMgr\s+INFO ---> Loop over \d+ Events Finished -  WSS [0-9\.]+, timed \d+ Events: [0-9\.]+ ms, Evts/s = [0-9\.]+",
    r"HLTControlFlowMgr\s+INFO  o Number of events slots: .*",
    r"HLTControlFlowMgr\s+INFO  o TBB thread pool size:  'ThreadPoolSize':.*"
])

remove_known_fluctuating_counters = LineSkipper(regexps=[
    # Functor cache hit/miss counters; can vary in uninteresting ways in
    # reconstruction tests based on HLT2 line content
    r' \| "# loaded from (?:CACHE|PYTHON)" .*',
    # The HltPackedDataWriter uses two data buffers: one that holds uncompressed
    # data and another that holds this data after compression.
    # For tracking efficiency lines, these counters fluctuate for some reason.
    # Consequently, the DstData bank size and the total event size fluctuate.
    # FIXME understand the reason for these fluctuations, see LHCb#151
    r' \| "Size of compressed data" .*',
    r' \| "DstData bank size \(bytes\)" .*',
    r' \| "Event size \(bytes\)" .*',
    r' \| "PP2MCPRelations buffer size" .*',
])

# Some counters are known to not be reproducible within the default
# tolerance of 1e-4.
# For Calo counters see https://gitlab.cern.ch/lhcb/Rec/-/issues/118
sensitivities = {
    'CaloTrackBasedElectronShowerAlg_...': {
        'average DLL': 0.0004,
        'average E/p': 0.0002,
    },
    'CaloFutureMergedPi0.CaloFutureEC...': {
        '&lt;alpha&gt; Outer': 0.1,
    },
    'FutureNeutralProtoPAlg': {
        'IsNotH for Photon': 0.0005,
        'IsNotE for Photon': 0.0002,
        'IsNotH for PhotonFromMergedPi0': 0.005,
        'IsNotE for Pi0Merged': 0.002,
        'IsNotH for Pi0Merged': 0.002,
    },
}

# In most cases everything interesting happens at finalize: counters,
# PRChecker tables, etc., so pick everything starting with finalize
# until the end of the file.
# NB: Always put before RecPreprocessor as it filters the '# ...' lines
only_finalize = BlockSkipper("# --> Including file",
                             " INFO Application Manager Stopped successfully")

# In case you do not want to check what is printed before the first event.
# NB: Always put before RecPreprocessor as it filters the '# ...' lines
skip_initialize = BlockSkipper(
    "# --> Including file",
    " Reading Event record 1. Record number within stream 1: 1")

# In case you do not want to check counters.
remove_finalize = BlockSkipper(
    " INFO Application Manager Stopped successfully",
    " INFO Application Manager Terminated successfully")

# Skip everything from the HLTControlFlowMgr (incl. the table with
# number of executions/passes).
skip_scheduler = GroupMessages() + LineSkipper(strings=["HLTControlFlowMgr"])

ref_preprocessor = only_finalize + RecPreprocessor + skip_scheduler

counter_preprocessor = remove_known_fluctuating_counters
