###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import Optional
from contextlib import contextmanager

from GaudiConf.LbExec import Options as DefaultOptions, InputProcessTypes
from PyConf.application import ROOT_KEY

from RecoConf.reconstruction_objects import reconstruction
from RecoConf.reco_objects_for_spruce import reconstruction as reconstruction_for_spruce, upfront_reconstruction as upfront_reconstruction_for_spruce


class Options(DefaultOptions):
    tck: Optional[int] = None

    @contextmanager
    def apply_binds(self):
        """
        This function configures the following functions, where their keyword
        arguments are globally bound to the user-specified values.
        This way users do not have to manually configure these functions themselves.
        - reconstruction() from reconstruction_objects
        - reconstruction() from reco_objects_for_spruce
        - upfront_reconstruction() from reco_objects_for_spruce

        """

        reconstruction.global_bind(
            spruce=self.input_process == InputProcessTypes.Hlt2,
            from_file=self.input_process == InputProcessTypes.Hlt2)
        reconstruction_for_spruce.global_bind(
            simulation=self.simulation and self.input_type == ROOT_KEY)
        upfront_reconstruction_for_spruce.global_bind(
            simulation=self.simulation and self.input_type == ROOT_KEY)

        with super().apply_binds():
            yield
