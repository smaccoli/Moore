###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the construction of DecisionLine objects."""
from PyConf.Algorithms import Gaudi__Examples__IntDataProducer as IntDataProducer
from PyConf.application import default_raw_event

from Moore.lines import Hlt2Line
from Moore.persistence.particle_moving import particle_output
# A little gross to reach into Hlt2Conf from Moore, but it's useful to be able
# to grab a preconfigured LHCb::Particle producer for the tests here
from Hlt2Conf.standard_particles import make_long_pions, make_dimuon_base


def test_unknown_producer(caplog):
    """Hlt2Line construction with an unknown output type should produce a warning.

    We explicitly whitelist what is supported for persistence in Hlt2Line.
    This is so that line authors do not request algorithm outputs which are
    silently ignored by the cloners/packers.
    """
    with default_raw_event.bind(raw_event_format=4.3):
        Hlt2Line("Hlt2IntDataProducer", algs=[IntDataProducer()])

    # A warning should be emitted about the unknown output type
    assert len(caplog.records) == 1
    record, = caplog.records
    assert record.levelname == "WARNING", record
    assert record.msg.startswith("Unsupported type"), record


def test_particle_producer(caplog):
    """Hlt2Line construction with an known output type should not produce a warning.

    This is currently on LHCb::Particle producers. As more producers are
    supported, additional tests should be added.
    """
    with default_raw_event.bind(raw_event_format=4.3):
        Hlt2Line("Hlt2ParticleProducer", algs=[make_dimuon_base()])

    # Should see no warnings as the output type of the producer is supported
    assert len(caplog.records) == 0


def test_particle_output():
    """Particle outputs should be deducible."""
    # We happen to know that this algorithm is functional, so its Particle
    # output should be detected cleanly
    particles = make_long_pions()
    assert particle_output(particles.producer) == particles
    # This algorithm inherits from DVCommonBase, so its Particle output should
    # be detected via a heuristic
    particles = make_dimuon_base()
    assert particle_output(particles.producer) == particles
