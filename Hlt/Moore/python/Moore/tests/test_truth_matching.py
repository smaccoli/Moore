###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test internal methods in the truth-matching configuration."""
from Hlt2Conf.algorithms import ParticleCombiner
from Hlt2Conf.standard_particles import make_long_pions, make_photons
from Moore.persistence.truth_matching import (
    TRACK_CONTAINER_T,
    _collect_dependencies,
    _find_protoparticles,
)
from PyConf.application import default_raw_event
from RecoConf.reconstruction_objects import reconstruction


def _combiner(charged, neutral):
    return ParticleCombiner(
        particles=[charged, neutral],
        DecayDescriptors=["D+ -> pi+ gamma"],
        CombinationCut="AALL",
        MotherCut="ALL",
    )


def test_find_protoparticles():
    """The method should correctly find the ProtoParticles containers referenced by Particle objects.

    This exercises some of the assumptions made by the method, such as the
    assumed type of the ProtoParticle container, and how one can distinguish
    between ProtoParticle objects the refer to neutral reconstruction objects
    or charged ones.
    """
    # Construct lines using the reconstruction from the raw event and from the
    # file, creating a line with only charged objects, with only neutral
    # objects, and with composite objects that reference both charged and
    # neutral objects
    with default_raw_event.bind(raw_event_format=4.3):
        with reconstruction.bind(from_file=False):
            pions = make_long_pions()
            photons = make_photons()
            composites = _combiner(pions, photons)

        with reconstruction.bind(from_file=True):
            pions_brunel = make_long_pions()
            photons_brunel = make_photons()
            composites_brunel = _combiner(pions_brunel, photons_brunel)

    (c, n), (cb, nb) = _find_protoparticles([pions])
    assert len(c) == 1 and not n and not cb and not nb

    (c, n), (cb, nb) = _find_protoparticles([photons])
    assert not c and len(n) == 1 and not cb and not nb

    (c, n), (cb, nb) = _find_protoparticles([composites])
    assert len(c) == 1 and len(n) == 1 and not cb and not nb

    (c, n), (cb, nb) = _find_protoparticles([pions_brunel])
    assert not c and not n and len(cb) == 1 and not nb

    (c, n), (cb, nb) = _find_protoparticles([photons_brunel])
    assert not c and not n and not cb and len(nb) == 1

    (c, n), (cb, nb) = _find_protoparticles([composites_brunel])
    assert not c and not n and len(cb) == 1 and len(nb) == 1

    # The method should return the superset of all containers
    (c, n), (cb, nb) = _find_protoparticles([composites, composites_brunel])
    assert len(c) == 1 and len(n) == 1 and len(cb) == 1 and len(nb) == 1


def test_event_model_assumptions():
    """Assert that assumptions made in the truth matching are correct.

    These are:

    1. Charged ProtoParticle objects created from the raw event reference a
       single container of v1::Track objects.
    """
    with default_raw_event.bind(raw_event_format=4.3), \
         reconstruction.bind(from_file=False):
        pions = make_long_pions()

    tracks = _collect_dependencies(pions.producer, TRACK_CONTAINER_T)
    assert len(tracks) == 1
