###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
import os
import re
from functools import lru_cache

from PyConf.Algorithms import CopyParticlesWithPVRelations
from PyConf.Algorithms import CopyFlavourTagsWithParticlePointers
from PyConf.components import force_location
from PyConf.dataflow import UntypedDataHandle

from PyConf.location_prefix import prefix

#: Final component of a TES location holding Particle objects
PARTICLES_LOCATION_SUFFIX = "Particles"
# Container type we match against to define 'a container of Particle objects'
PARTICLE_CONTAINER_TYPES = {
    "KeyedContainer<LHCb::Particle,Containers::KeyedObjectManager<Containers::hashmap> >",
    "SharedObjectsContainer<LHCb::Particle>",
}
FLAVOURTAG_CONTAINER_TYPE = "KeyedContainer<LHCb::FlavourTag,Containers::KeyedObjectManager<Containers::hashmap> >"

# Set of locations we've forced to specific values; must keep track of these to
# ensure we never force the same location twice
_FORCED_LOCATIONS = set()

log = logging.getLogger(__name__)


@lru_cache(maxsize=None)
def dvalgorithm_locations(producer):
    """Return a list of all DVAlgorithm output DataHandles.

    Assumes `producer` represents a DVAlgorithm.
    """
    # Check that the outputs match the output API defined by
    # Hlt2Conf.algorithms
    output_keys = set(producer.outputs)
    if output_keys not in [{"Particles"},
                           {"Particles", "Particle2VertexRelations"}]:
        raise TypeError("producer is not a DVAlgorithm")
    particle_loc = producer.outputs["Particles"].location
    particles_suffix = '/{}'.format(PARTICLES_LOCATION_SUFFIX)
    assert particle_loc.endswith(particles_suffix), particle_loc

    # Algorithms that create '/Particles' locations can also create some
    # additional locations under the same parent path; we want those too
    sneaky_suffixes = [
        'decayVertices', '_RefitPVs', 'Particle2VertexRelations'
    ]

    locations = []
    for suffix in [PARTICLES_LOCATION_SUFFIX] + sneaky_suffixes:
        loc = re.sub(particles_suffix + '$', '/' + suffix, particle_loc)
        # The 'key' of the data handle is taken as `suffix`, but the
        # configurable does not expose these outputs as configurable
        # properties. Because of this, PyConf will be unable to infer the C++
        # type of these output objects, so we explicitly instantiate an untyped
        # data handle
        dh = UntypedDataHandle(producer, suffix, force_location(prefix(loc)))
        locations.append(dh)

    return locations


def CopyParticles(particles, pvs, path_components):
    """Return an algorithm that copies Particle objects.

    Args:
        particles (DataHandle): LHCb::Particle container to be copied.
        pvs (DataHandle): Primary vertices used to generate P2PV relations
        tables for the copied Particle objects.
        path_components (list of str): Definition of TES path, relative to
        '/Event', under which to store copied Particle objects.
    """
    # Path components should not contain slashes
    assert all('/' not in pc for pc in path_components)
    # We will add the suffix in this method to ensure consistency
    assert path_components[-1] != PARTICLES_LOCATION_SUFFIX

    base = os.path.join("/Event", *path_components)
    output_loc_particles = os.path.join(base, PARTICLES_LOCATION_SUFFIX)
    output_loc_vertices = os.path.join(base, "decayVertices")
    output_loc_p2pv = os.path.join(base, "Particle2VertexRelations")
    # Keep track of forced output locations to make sure we don't write to the
    # same place twice
    assert output_loc_particles not in _FORCED_LOCATIONS, "Duplicate CopyParticles instantiation"
    _FORCED_LOCATIONS.add(output_loc_particles)

    return CopyParticlesWithPVRelations(
        InputParticles=particles,
        InputPrimaryVertices=pvs,
        outputs=dict(
            # Force output locations to match the pattern of those created by
            # DVCommonBase algorithms
            OutputParticles=force_location(output_loc_particles),
            OutputParticleVertices=force_location(output_loc_vertices),
            OutputP2PVRelations=force_location(output_loc_p2pv),
        ),
    )


def CopyFlavourTags(flavourtags, path_components, particles):
    """Return an algorithm that copies Particle objects.

    Args:
        flavourtags (DataHandle): LHCb::FlavourTags container to be copied.
        path_components (list of str): Definition of TES path, relative to
        '/Event', under which to store copied FlavourTags objects.
        particles (DataHandle): LHCb::Particle container with the (B) candidates
        that the new FlavourTags objects should point to
    """
    # Path components should not contain slashes
    assert all('/' not in pc for pc in path_components)

    output_loc_flavourtags = os.path.join("/Event", *path_components)
    # Keep track of forced output locations to make sure we don't write to the
    # same place twice
    assert output_loc_flavourtags not in _FORCED_LOCATIONS, "Duplicate CopyFlavourTags instantiation"
    _FORCED_LOCATIONS.add(output_loc_flavourtags)

    return CopyFlavourTagsWithParticlePointers(
        InputFlavourTags=flavourtags,
        NewBCandidates=particles,
        outputs=dict(OutputFlavourTags=force_location(output_loc_flavourtags)),
    )


@lru_cache(maxsize=None)
def is_particle_producer(producer):
    """Return True if `producer` outputs an `LHCb::Particle` container.

    If an output cannot be introspected to have a type in
    `PARTICLE_CONTAINER_TYPES` a fallback is used to detect a common type of
    particle producer: descendents of `DVCommonBase`. In this case the output
    property is called `Particles` and output type cannot be deduced by the
    Python configuration framework, so is marked as "unknown_t".

    Note:
        The `Particles` property name is a convention used with Moore and
        Hlt2Conf. It is the argument name in output transforms used to
        decorate various `DVCommonBase` subclasses; see `Hlt2Conf.algorithms`
        for examples.

        If a `DVCommonBase` descendent does not follow these conventions, it
        will not be recognised by this method as a `LHCb::Particle` producer.
    """
    # First, check if the producer has outputs of the correct C++ type
    output_types = {dh.type for dh in producer.outputs.values()}
    if PARTICLE_CONTAINER_TYPES & output_types:
        return True
    # Otherwise, use a heuristic to determine if producer derives from
    # DVCommonBase, in which case it is a Particle producer
    try:
        particles = producer.Particles
        return particles.type == "unknown_t"
    except AttributeError:
        return False


def particle_output(producer):
    """Return the LHCb::Particle output of the producer algorithm.

    Raises:
        ValueError: If `producer` does not output a LHCb::Particle container.
    """
    # First assume we have a functional algorithm and can introspect the
    # output types, such that we just pick the output with the right type
    particles = [
        dh for dh in producer.outputs.values()
        if dh.type in PARTICLE_CONTAINER_TYPES
    ]
    if not particles:
        try:
            # The output transform for DVCommonBase algorithms in
            # Hlt2Conf.algorithms assigns the `Particles` name to the particle
            # output property, so try that next
            particles = [producer.Particles]
        except AttributeError:
            particles = []


#    Check was first introduced by apearce here:
#    https://gitlab.cern.ch/lhcb/Moore/-/commit/6f50581315efac1188f38fdbd4eda3b2a82d6c81
#    But it's unclear why, so the check was removed in Moore!967 to make that MR work.
#    Moore#320 is to follow up and investigate if check is needed.
#    if len(particles) != 1:
#        raise ValueError("Could not find unique LHCb::Particle outputs of {}".
#                         format(producer))
    return particles.pop()


def is_flavourtag_producer(producer):
    """Return True if `producer` outputs an `LHCb::FlavourTag` container.
    """
    # Check if the producer has outputs of the correct C++ type
    if FLAVOURTAG_CONTAINER_TYPE in {
            dh.type
            for dh in producer.outputs.values()
    }:
        return True

    try:
        flavourtags = producer.FlavourTags
        return flavourtags.type == "unknown_t"
    except AttributeError:
        return False


def flavourtag_output(producer):
    """Return the LHCb::FlavourTags output of the producer algorithm.

    Raises:
        ValueError: If `producer` does not output a LHCb::FlavourTags container.
    """
    # First assume we have a functional algorithm and can introspect the
    # output types, such that we just pick the output with the right type
    flavourtags = [
        dh for dh in producer.outputs.values()
        if dh.type in FLAVOURTAG_CONTAINER_TYPE
    ]
    if not flavourtags:
        try:
            flavourtags = [producer.FlavourTags]
        except AttributeError:
            flavourtags = []
    return flavourtags.pop()
