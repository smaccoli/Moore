###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Logic for cloning HLT2 line outputs under ``/Event/HLT2(Spruce/)``.

This is largely a port of the Run 2 logic, up until the serialisation of the
packed containers into raw banks. 

The Run 2 logic is defined in the Hlt project. Reading the `_persistRecoSeq`
method in the `HltAfterburner.py` file is a good place to start if you want to
understand more.
"""
import logging

from PyConf.Algorithms import CopySignalMCParticles, CopyProtoParticle2MCRelations

from PyConf.location_prefix import prefix

log = logging.getLogger(__name__)


def mc_cloners(stream, protoparticle_relations):
    """Copy signal and associated MC particles to microDST-like locations.

    In the Stripping, the microDST machinery only saves a subset of the
    input MC particles and vertices. This subset contains two possibly
    overlapping contributions:

    1. The MCParticle objects in the signal decay tree; and
    2. The MCParticle objects which can be associated to the reconstructed
    objects selected by at least one stripping line.

    To mimic this behaviour in Tesla, we run almost the same set of
    microDST algorithms, using the relations tables Tesla makes to find the
    MC particles that have been associated to the reconstruction.

    Args:
        stream (str): TES root under which objects will be cloned.
        protoparticle_relations (list of DataHandle): ProtoParticle-to-MCParticle relations.
    """
    # Algorithm to clone the existing relations tables and the MC particles
    # and vertices these tables point to from their original location to
    # the same location prefixed by /Event/Turbo
    copy_pp2mcp = CopyProtoParticle2MCRelations(
        InputLocations=protoparticle_relations,
        OutputPrefix=stream,
        # Don't use the assumed copy of the input ProtoParticle objects, as
        # Tesla doesn't need to copy them (they're already under /Event/Turbo)
        # UseOriginalFrom=True,
        OutputLevel=4)

    # Algorithm to clone all MC particles and vertices that are associated
    # to the simulated signal process (using LHCb::MCParticle::fromSignal)
    if "Spruce" in stream:
        MCParticlesLocation = "/Event/HLT2/MC/Particles"
        ExtraOutputs = [
            prefix(l, stream)
            for l in ['HLT2/MC/Particles', 'HLT2/MC/Vertices']
        ]
    else:
        MCParticlesLocation = "/Event/MC/Particles"
        ExtraOutputs = [
            prefix(l, stream) for l in ['MC/Particles', 'MC/Vertices']
        ]
    copy_signal_mcp = CopySignalMCParticles(
        MCParticlesLocation=MCParticlesLocation,
        OutputPrefix=stream,
        ExtraOutputs=ExtraOutputs,

        # Don't try to copy the associated reconstruction, as other cloners
        # will have already taken care of it
        SaveAssociatedRecoInfo=False,
    )

    return [copy_pp2mcp, copy_signal_mcp]
