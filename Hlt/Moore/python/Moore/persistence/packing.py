###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration       #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
import os

from PyConf.components import force_location
from PyConf.control_flow import CompositeNode, NodeLogic

from PyConf.Algorithms import PackMCParticle, PackMCVertex

log = logging.getLogger(__name__)


def pack_stream_mc_locations(stream):
    return [
        os.path.join(stream,
                     str(PackMCParticle.getDefaultProperties()["InputName"])),
        os.path.join(stream,
                     str(PackMCVertex.getDefaultProperties()["InputName"])),
        os.path.join(stream,
                     str(PackMCParticle.getDefaultProperties()["OutputName"])),
        os.path.join(stream,
                     str(PackMCVertex.getDefaultProperties()["OutputName"]))
    ]


def pack_stream_mc(stream):
    """Return a list of packers that will produce mc packed output.

    Args:
        stream (str): TES root containing objects to be packed.

    Returns:
        algs (list): Algorithms to run the packing.
        outputs (list of str): Locations that should be persisted, in the
        specification used by ROOT output writers (e.g. OutputStream).
    """
    mc_packers = []
    mc_packers += [
        PackMCParticle(
            OutputLevel=4,
            InputName=force_location(
                os.path.join(
                    stream,
                    str(PackMCParticle.getDefaultProperties()["InputName"]))),
            outputs={
                'OutputName':
                force_location(
                    os.path.join(
                        stream,
                        str(PackMCParticle.getDefaultProperties()
                            ["OutputName"])))
            }),
        PackMCVertex(
            OutputLevel=4,
            InputName=force_location(
                os.path.join(
                    stream,
                    str(PackMCVertex.getDefaultProperties()["InputName"]))),
            outputs={
                'OutputName':
                force_location(
                    os.path.join(
                        stream,
                        str(PackMCVertex.getDefaultProperties()
                            ["OutputName"])))
            },
        ),
    ]

    mc_packers_cf = CompositeNode(
        "mc_packers",
        combine_logic=NodeLogic.NONLAZY_OR,
        children=mc_packers,
    )
    packers_mc_locations = [p.OutputName for p in mc_packers]

    return mc_packers_cf, packers_mc_locations
