###############################################################################
# (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for creating the SelReports object.

SelReports summarise the set of objects that fired a line. These objects are
assumed to be produced by the last algorithm in the line's control flow.

The SelReports maker algorithm accepts v1 Track and v1 Vertex objects, and so
this module contains some logic for traversing the dataflow tree of the
decision algorithm and instantiating converters required to go from whatever
object the algorithm produces to the v1 objects.

To support additional algorithms, add a converter function and the supported
algorithm type to the `CONVERTERS` dict.

The logic of the converter functions is somewhat brittle, as it depends on the
structure of the dataflow that enters into each line's decision algorithm.
"""
import logging
from functools import partial

from PyConf.Algorithms import (
    fromV3TrackWithMuonIDV1TrackVector,
    fromV3TrackWithPVsV1TrackVector,
    LHCb__Converters__Composites__TracksWithPVsToVectorOfRecVertex,
    LHCb__Converters__Composites__TracksWithMuonIDToVectorOfRecVertex,
    SelReportsMaker,
)
from PyConf.filecontent_metadata import retrieve_encoding_dictionary

__all__ = ["make_selreports"]
log = logging.getLogger(__name__)


class UnconvertableAlgorithmError(Exception):
    pass


class AlgorithmDidNotMatchError(Exception):
    pass


class AmbiguousVeloHitsError(Exception):
    pass


def convert_pr_fitted_tracks(alg, converter_alg_type):
    v1_tracks = converter_alg_type(InputTracks=alg.outputs["Output"])
    return v1_tracks


def convert_composites(alg, converter_alg_type):
    """Configure a converter from the output of `alg` to v1 types.

    Args:
        alg (Algorithm): Composite producer whose output we should convert
        converter_alg_type (type): Some LHCb::Converters::Composites algorithm
        type that matches the input/output types of `alg`
    """
    # Get input/output DataHandles
    tracks = alg.inputs['InputTracks']
    composites = alg.outputs['Output']
    # Get a converter function that acts on the type of 'tracks'
    track_converter = get_converter_from_types(Tracks=tracks)
    input_tracks_producer = tracks.producer  # PrFilter?
    v1_tracks = track_converter(input_tracks_producer)
    return converter_alg_type(
        TracksInVertices=tracks,
        ConvertedTracks=v1_tracks,
        InputComposites=composites)


def get_type_key(alg_type, property_names):
    """Flatten a {category: type_string} dictionary into a hashable key."""
    return tuple(
        sorted((prop_category,
                alg_type.getDefaultProperties()[prop_name].type())
               for prop_category, prop_name in property_names.items()))


# Map algorithm types to converter functions. The dictionary keys are
# essentially {category: C++ type string, ...} where category is a descriptive
# name (Composites, Children, Tracks, ...). The C++ type strings are extracted
# from the type of the converter algorithm, and that type is passed to the
# converter function in the keyword argument `converter_alg_type`.
# The converter functions accept an algorithm instance (whose outputs are to be
# converted) and returns an algorithm whose output(s) are the converted (v1)
# objects.
CONVERTERS_BY_TYPES = {
    get_type_key(alg_type, property_names): partial(
        function, converter_alg_type=alg_type)
    for alg_type, property_names, function in
    [(fromV3TrackWithPVsV1TrackVector, {
        'Tracks': 'InputTracks'
    }, convert_pr_fitted_tracks),
     (fromV3TrackWithMuonIDV1TrackVector, {
         'Tracks': 'InputTracks'
     }, convert_pr_fitted_tracks),
     (LHCb__Converters__Composites__TracksWithPVsToVectorOfRecVertex, {
         'Composites': 'InputComposites',
         'Children': 'TracksInVertices'
     }, convert_composites),
     (LHCb__Converters__Composites__TracksWithMuonIDToVectorOfRecVertex, {
         'Composites': 'InputComposites',
         'Children': 'TracksInVertices'
     }, convert_composites)]
}

# `CONVERTERS_BY_UNIQUE_TYPE` contains a subset of `CONVERTERS_BY_TYPES` where
# the converter function can be uniquely resolved from a single input type. In
# other words, if `CONVERTERS_BY_TYPES` has `key = {k1: v1, k2: v2, ...}`,
# `CONVERTERS_BY_UNIQUE_TYPE` has key `v1` iff `len(key) == 1` and `v1` alone
# would be unique.
CONVERTERS_BY_UNIQUE_TYPE = {}
for key, converter in CONVERTERS_BY_TYPES.items():
    if len(key) != 1: continue
    category_name, type_name = key[0]
    CONVERTERS_BY_UNIQUE_TYPE[
        type_name] = None if type_name in CONVERTERS_BY_UNIQUE_TYPE else converter
CONVERTERS_BY_UNIQUE_TYPE = {
    k: v
    for k, v in CONVERTERS_BY_UNIQUE_TYPE.items() if v is not None
}


def match_converter_from_outputs(handles):
    """See if the C++ types of the given handles uniquely map to a converter
    function.

    Raises:
        AlgorithmDidNotMatchError: If there is no unique match.
    """
    converters = []
    for handle in handles:
        converter = CONVERTERS_BY_UNIQUE_TYPE.get(handle.type, None)
        if converter is not None: converters.append(converter)
    if len(converters) == 1: return converters[0]
    raise AlgorithmDidNotMatchError()


def get_converter_from_types(**kwargs):
    """See if there is a converter function matching the given C++ types and
    categories.

    Raises:
        KeyError: If there is no match.
    """
    return CONVERTERS_BY_TYPES[tuple(
        sorted((name, handle.type) for name, handle in kwargs.items()))]


def convert_combinetracks_simd(alg):
    """See if `alg` is an instantiation of CombineTracksSIMD and, if so, return
    a converter for it.

    Args:
        alg: Composite producer whose output we should convert
    Raises:
        AlgorithmDidNotMatchError: If this is not an instantiation of CombineTracksSIMD.
    """
    # Apply some heuristics
    if not alg.type.__name__.startswith('CombineTracksSIMD'):
        raise AlgorithmDidNotMatchError()
    # looks good -- alg is a CombineTracksSIMD instantiation
    # Look up a converter function based on the child + composite type (though
    # the composite type is type-erased, so that part is a bit pointless)
    return get_converter_from_types(
        Composites=alg.outputs['Output'],
        Children=alg.inputs['InputTracks'])(alg)


# TODO this doesn't scale very efficiently :-(
CONVERTER_FUNCTIONS = [convert_combinetracks_simd]


def convert_output(alg):
    """Return an algorithm that produces the output(s) of `alg` in v1 format.

    Raises:
        UnconvertableAlgorithmError: If the type of `alg` is not listed in
        `CONVERTERS`.
    """
    # See if there is an unambiguous match in `CONVERTERS_BY_UNIQUE_TYPE` based
    # on the output type(s) of `alg`.
    try:
        return match_converter_from_outputs(alg.outputs.values())(alg)
    except AlgorithmDidNotMatchError:
        pass
    # Finally, see if any heuristic functions match
    for converter_function in CONVERTER_FUNCTIONS:
        try:
            return converter_function(alg)
        except AlgorithmDidNotMatchError:
            pass
    raise UnconvertableAlgorithmError("Unsupported algorithm type {!r}".format(
        alg.type))


def make_selreports(process, lines, decreports, **kwargs):
    """Return a SelReportsMaker instance configured for the given lines.

    Args:
        lines: The list of lines for which to create SelReport objects
        decreports: DecReports data used as input to the SelReports maker.
        kwargs: Passed to the SelReportsMaker.
    """
    # Gather all outputs along with their type and decision names
    info = [(line.decision_name, output, output.type) for line in lines
            for output in line.objects_to_persist]
    if info:
        # Configurable properties expect list types, not tuples
        names, outputs, types = list(map(list, zip(*info)))
    else:
        # No lines we can handle, so do nothing
        names, outputs, types = [], [], []

    dec_props = decreports.properties
    # TODO: perhaps ExecutionReportsWriter should get an explicit encoding key..
    key = dec_props.get('EncodingKey', dec_props.get('TCK', None))
    assert key is not None
    # get the content for this key, and verify it contains SelectionIDs
    table = retrieve_encoding_dictionary('{:08x}'.format(key))
    sid = table["{}SelectionID".format(process.capitalize())]
    assert all(n in sid.values() for n in names)

    # Each line output should be unique; we expect no two lines to do exactly
    # the same work
    assert len(outputs) == len(
        set(outputs)), "multiple lines with identical output"

    # The SelReports maker must be a barrier as its inputs are conditional
    # on line decisions (if a line does not fire, its outputs will not be
    # available to make SelReports with)
    return SelReportsMaker(
        is_barrier=True,
        DecReports=decreports,
        SelectionNames=names,
        CandidateTypes=types,
        Inputs=outputs,
        EncodingKey=key,
        **kwargs)
