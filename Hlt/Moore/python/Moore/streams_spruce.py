###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define Sprucing streams

"""

from .streams_hlt2 import DETECTOR_RAW_BANK_TYPES
from PyConf.utilities import ConfigurationError

streams = ['default', 'test', 'b2oc', 'bandq', 'sl', 'b2cc']

stream_banks = {key: list(DETECTOR_RAW_BANK_TYPES) for key in streams}

stream_banks['test'].remove('Rich')


def stream_bank_types(stream):
    """Return list of detector raw banks to save for given stream"""
    if stream not in stream_banks:
        raise ConfigurationError(
            "stream %r is not configured in streams.py" % stream)
    return stream_banks[stream]
