###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import re
from Gaudi.Configuration import ConfigurableUser, appendPostConfigAction
from Configurables import (
    LHCbApp,
    DDDBConf,
    HistogramPersistencySvc,
    EventSelector,
)


def undressInputFileName(selector):
    m = re.match(r"DATAFILE='([^']+)'", selector)
    return m.group(1)


def remove_duplicates():
    """Remove duplicates introduced by using both PyConf and LHCbApp"""
    from Gaudi.Configuration import allConfigurables
    for name, prop in (("ApplicationMgr", "ExtSvc"), ("EventPersistencySvc",
                                                      "CnvServices"),
                       ("FileRecordPersistencySvc", "CnvServices")):
        try:
            c = allConfigurables[name]
            # we have a mixture of configurables and strings,
            # so we count as duplicates the entries with the same name
            new_items = {}
            for element in getattr(c, prop):
                el_name = element.name() if hasattr(
                    element, "name") else element.rsplit("/", 1)[-1]
                new_items.setdefault(el_name, element)
            setattr(c, prop, list(new_items.values()))
        except (AttributeError, KeyError):
            pass


class Moore(ConfigurableUser):
    __used_configurables__ = [
        LHCbApp,
        DDDBConf,
    ]

    __slots__ = {
        "DDDBtag": "",
        "CondDBtag": "",
        "outputFile": "",
        "from_file": False,
        "spruce": False,
    }

    def __apply_configuration__(self):
        from Configurables import LHCbApp

        from Moore import options, run_moore
        from RecoConf.global_tools import stateProvider_with_simplified_geom
        from RecoConf.reconstruction_objects import reconstruction

        from RecoConf.decoders import default_ft_decoding_version

        default_ft_decoding_version.global_bind(value=6)

        options.evt_max = LHCbApp().getProp("EvtMax")
        options.output_file = self.getProp("outputFile")
        ext = os.path.splitext(options.output_file)[1].lower()
        options.output_type = "MDF" if ext in [".mdf", ".raw"] else "ROOT"

        options.dddb_tag = self.getProp("DDDBtag")
        options.conddb_tag = self.getProp("CondDBtag")

        options.use_iosvc = False
        options.event_store = 'HiveWhiteBoard'

        options.input_type = getattr(options, "input_type", "ROOT")
        options.input_files = [
            undressInputFileName(s) for s in EventSelector().Input
        ]

        if HistogramPersistencySvc().isPropertySet("OutputFile"):
            options.histo_file = HistogramPersistencySvc().OutputFile
            # TODO this will also produce a *_new.root file with new histograms
            # that won't be known to dirac.

        from_file = self.getProp("from_file")
        spruce = self.getProp("spruce")

        public_tools = [stateProvider_with_simplified_geom()]
        with reconstruction.bind(from_file=from_file, spruce=spruce):
            run_moore(options, public_tools=public_tools)

        appendPostConfigAction(remove_duplicates)
