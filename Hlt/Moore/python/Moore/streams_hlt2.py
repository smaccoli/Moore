###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define hlt2 streams and routing bits

Routing bits are used to decide where to send events, to Hlt2 only, or as well to calibration and monitoring tasks.
Each bit is set according to trigger line decisions.

The routing bits are defined in https://gitlab.cern.ch/lhcb/runII-routingbits.
The meaning of some routing bits (eg. 40, 94, 95 ) has been agreed on and should not be changed.

"""
from PyConf.utilities import ConfigurationError
import re

PHYSICS = r"^Hlt2(?!Lumi).*Decision$"
LUMI = r"^Hlt2LumiDecision$"

DETECTOR_RAW_BANK_TYPES = {
    'ODIN', 'VP', 'UT', 'FTCluster', 'Rich', 'EcalPacked', 'HcalPacked',
    'Muon', 'VPRetinaCluster', 'Calo', 'Plume'
}
HLT1_REPORT_RAW_BANK_TYPES = {
    'HltDecReports', 'HltSelReports', 'HltLumiSummary'
}
HLT2_REPORT_RAW_BANK_TYPES = {
    'HltDecReports',
    'HltLumiSummary',
}

# For now passing all RawBanks to all streams, to be modified based on the streaming development
# list ODIN explicitly just here
DETECTOR_RAW_BANK_TYPES_PER_STREAM = {
    "full": DETECTOR_RAW_BANK_TYPES,
    "turbo": DETECTOR_RAW_BANK_TYPES,
    "turcal": DETECTOR_RAW_BANK_TYPES,
    "no_bias": DETECTOR_RAW_BANK_TYPES,
    "passthrough": DETECTOR_RAW_BANK_TYPES,
    "default": DETECTOR_RAW_BANK_TYPES
}

stream_bits = dict(
    full=87, turbo=88, turcal=90, no_bias=91, passthrough=92, default=85)


def stream_bank_types(stream):
    """Return list of detector raw banks to save for given stream"""
    if stream not in DETECTOR_RAW_BANK_TYPES_PER_STREAM:
        raise ConfigurationError(
            "stream %r is not configured in streams.py" % stream)
    return list(DETECTOR_RAW_BANK_TYPES_PER_STREAM[stream])


def get_default_routing_bits(streams):
    """ Define default routing bits via regular expressions. This is currently a placeholder to test the functionality."""
    # Temporarily hard-code a default mapping of streams to bits, see #268
    routingBits = {}
    for stream, lines_in_stream in streams.items():
        # Bit for stream
        if (stream_bits[stream] == 94 or stream_bits[stream] == 95):
            raise ConfigurationError(
                f"Bits 94 and 95 are reserved. Do not use for stream {stream}."
            )

        routingBits[stream_bits[stream]] = [
            l.decision_name for l in lines_in_stream
        ]
        # Lumi bit, always 94
        routingBits[94] = [
            l.decision_name for l in lines_in_stream
            if re.search(LUMI, l.decision_name)
        ]
        # Physics bit, always 95
        routingBits[95] = [
            l.decision_name for l in lines_in_stream
            if re.search(PHYSICS, l.decision_name)
        ]

    return routingBits
