###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import re

from Moore import Options

MC_PASSTHROUGH_LINE_NAME = "Hlt2MCPassThroughLine"


def allen_hlt1_production(options: Options, sequence: str = "hlt1_pp_default"):
    from Moore import run_allen
    from RecoConf.hlt1_allen import allen_gaudi_node

    with allen_gaudi_node.bind(sequence=sequence):
        return run_allen(options)


def hlt2(options: Options, *raw_args):
    args = _parse_args(raw_args)
    return _hlt2(options, _make_all_lines, args.lines_regex,
                 args.require_deployed_trigger_key)


def hlt2_pp_commissioning(options: Options, *raw_args):
    args = _parse_args(raw_args)
    return _hlt2(options, _make_pp_commissioning_lines, args.lines_regex,
                 args.require_deployed_trigger_key)


def _parse_args(raw_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--lines-regex",
        type=re.compile,
        help="Regex pattern to select which lines to run. "
        f"For simulation, if {MC_PASSTHROUGH_LINE_NAME} is not selected filtering will be applied.",
    )
    parser.add_argument(
        "--require-deployed-trigger-key",
        action='store_true',
        help=
        "When enabled, it is checked that the trigger configuration has been deployed on cvmfs. "
        "In case of failure, the job will crash.",
    )
    return parser.parse_args(raw_args)


def _hlt2(options, make_lines, lines_regex,
          require_deployed_trigger_key=False):
    from DDDB.CheckDD4Hep import UseDD4Hep
    from Moore import run_moore
    from RecoConf.reconstruction_objects import reconstruction
    from RecoConf.hlt2_tracking import (
        make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
        make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
        get_UpgradeGhostId_tool_no_UT)
    from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
    from RecoConf import mc_checking
    from RecoConf.hlt1_tracking import (
        make_VeloClusterTrackingSIMD, make_RetinaClusters, make_reco_pvs,
        make_PatPV3DFuture_pvs, get_global_measurement_provider)
    from RecoConf.hlt1_muonid import make_muon_hits
    from RecoConf.calorimeter_reconstruction import make_digits
    from Moore.lines import Hlt2Line
    from PyConf.application import metainfo_repos, retrieve_encoding_dictionary
    if require_deployed_trigger_key:
        metainfo_repos.global_bind(repos=[])  # only use repos on cvmfs
        retrieve_encoding_dictionary.global_bind(
            require_key_present=True)  # require key is in repo
    if options.simulation:
        mc_checking.make_links_lhcbids_mcparticles_tracking_and_muon_system.global_bind(
            with_ut=False)

    if UseDD4Hep:
        from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

        print("Removing UT from DD4hep geometry")
        dd4hepSvc = DD4hepSvc()
        dd4hepSvc.DetectorList = [
            '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal',
            'Muon'
        ]

    # Do not configure simplified geometry as it does not exist for DD4HEP
    public_tools = []
    # Workaround to enable running of Tracking efficiency lines using special muon reconstruction
    from PyConf.Tools import TrackMasterFitter

    def _line_maker():
        lines = make_lines()
        if options.simulation:
            print(f"Adding {MC_PASSTHROUGH_LINE_NAME} for simulation")
            lines.append(
                Hlt2Line(
                    name=MC_PASSTHROUGH_LINE_NAME,
                    algs=[],
                    prescale=1,
                    persistreco=True))
        if lines_regex:
            n_before = len(lines)
            lines = [
                line for line in lines if lines_regex.fullmatch(line.name)
            ]
            print(
                f"Reduced {n_before} lines to {len(lines)} using {lines_regex}"
            )
        return lines

    with reconstruction.bind(from_file=False),\
        make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.2),\
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=4.2),\
        make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.2),\
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=4.2),\
        make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD, SkipForward=4),\
        get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters),\
        make_muon_hits.bind(geometry_version=3),\
        make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
        get_global_measurement_provider.bind(velo_hits=make_RetinaClusters),\
        make_digits.bind(calo_raw_bank=True),\
        TrackMasterFitter.bind(FastMaterialApproximation=True):
        config = run_moore(options, _line_maker, public_tools)

    return config


def _make_all_lines():
    from Hlt2Conf.lines import all_lines
    lines = [builder() for builder in all_lines.values()]
    return lines


def _make_pp_commissioning_lines():
    from Hlt2Conf.settings.hlt2_pp_commissioning import make_streams
    streams = make_streams()
    # Use a set comprehension to deduplicate lines
    return list({line for lines in streams.values() for line in lines})
