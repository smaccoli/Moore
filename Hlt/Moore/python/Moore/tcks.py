###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Functions for manipulating trigger configuration key (TCK) configuration.

'The `TCK`_' can refer to two things:

1. A static, persisted form of the configuration which was used to run a
   trigger application.
2. A label, or 'key', which identifies a unique version of the persisted
   configuration.

In Runs 1 and 2, the key for HLT configuration was a four-byte integer usually
represented in hexadecimal, e.g 0x201b17a0.

The configuration has two primary use-cases:

1. It records the HLT application's configuration in a way which can be *run
   from*. That is, one can instantiate the HLT from the TCK rather than having
   to re-run the Python configuration files.
2. It stores information which is common to all data processed with the
   configuration it represents.

The last point permits bandwidth-saving techniques. For example, rather than
saving the list of all trigger line names in the decision reports in every
event, one can persist a number representing each name. The mapping from number
to name is stored in the TCK. Reading the decision reports then requires
loading the TCK to replace the numbers with the names, but results in a much
smaller decision report object on disk (because the numbers take up fewer bytes
on disk than the names do). The configurable object that holds the
name-to-number mapping is ``HltANNSvc``, where ``ANN`` stands for 'Assigned
Names and Numbers'.

In Run 3 Moore, we do not yet support persisting the full trigger configuration
in a way that we can run the application from.

The functions in this module currently support persisting the configuration of
`HltANNSvc` such that data written from Moore can be read by subsequent
('downstream') applications.

.. _TCK: https://twiki.cern.ch/twiki/bin/view/LHCb/TCK
"""
import GaudiConf


def load_manifest(fname):
    return GaudiConf.reading.load_manifest(fname)
