###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#####
## part I
#####
from Moore import options
from DDDB.CheckDD4Hep import UseDD4Hep
from PyConf.application import metainfo_repos

options.input_files = [
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/data/nov_2022/Run_0000253597_HLT24640_20221114-023831-878.mdf"
]

# Set the tags depending on DetDesc or DD4HEP.
# Note DetDesc requires simulation to be false.
options.dddb_tag = "upgrade/dddb-20221004"
options.conddb_tag = 'md_VP_SciFi_macromicrosurvey_from20220923'
options.geometry_version = "trunk"
options.conditions_version = 'master'
options.simulation = not UseDD4Hep
metainfo_repos.global_bind(extra_central_tags=['commissioning'])

options.evt_max = 500
options.input_type = "MDF"

if UseDD4Hep:
    # The data have no UT so exclude it here.
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hepSvc = DD4hepSvc()
    dd4hepSvc.DetectorList = [
        '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal',
        'Muon'
    ]
