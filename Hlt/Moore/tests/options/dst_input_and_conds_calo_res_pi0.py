###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from Moore import options

options.evt_max = 100
# for meaningfull resolution plot larger statistics is needed: comment out the line above (the test will process ~110K events and take a few hours)

options.set_input_and_conds_from_testfiledb('Upgrade_Bd_pi+pi-pi0_LDST')

options.input_type = 'ROOT'
options.input_raw_format = 4.3

# use old calo raw bank names
from RecoConf.calorimeter_reconstruction import make_digits
make_digits.global_bind(calo_raw_bank=False)

options.histo_file = os.path.expandvars('histos_${QMTTEST_NAME}.root')
options.ntuple_file = os.path.expandvars('ntuple_${QMTTEST_NAME}.root')
