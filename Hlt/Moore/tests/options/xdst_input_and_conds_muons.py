###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.decoders import default_ft_decoding_version

default_ft_decoding_version.global_bind(value=6)

options.input_type = 'ROOT'
options.input_files = [
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000010_2.xdst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000011_2.xdst'
]
options.simulation = True
options.conddb_tag = 'sim-20180530-vc-mu100'
options.dddb_tag = 'dddb-20190223'
options.input_raw_format = 4.3
options.evt_max = 100
