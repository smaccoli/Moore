###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.packing import persisted_location
from PyConf.application import default_raw_banks

from GaudiKernel.SystemOfUnits import GeV

from Configurables import Rich__Future__ParticleProperties as PartProps
from PyConf.Algorithms import (
    Rich__Future__RawBankDecoder as RichDecoder,
    Rich__Future__SmartIDClustering as RichClustering,
    Rich__Future__Rec__SIMDSummaryPixels as RichSIMDPixels,
    Rich__Future__Rec__DetailedTrSegMakerFromTracks as SegmentCreator,
    Rich__Future__Rec__RayTraceTrackGlobalPoints as TkPanelGlobalPoints,
    Rich__Future__Rec__RayTraceTrackLocalPoints as TkPanelLocalPoints,
    Rich__Future__Rec__EmittedPhotonYields as EmittedYields,
    Rich__Future__Rec__TrackEmittedCherenkovAngles as EmittedCKAngles,
    Rich__Future__Rec__RayTraceCherenkovCones as EmittedMassCones,
    Rich__Future__Rec__GeomEffCKMassRings as GeomEff,
    Rich__Future__Rec__SelectTrackSegments as SelectTrackSegments,
    Rich__Future__Rec__SignalPhotonYields as SignalYields,
    Rich__Future__Rec__TrackSignalCherenkovAngles as SignalCherenkovAngles,
    Rich__Future__Rec__TrackFunctionalCherenkovResolutions as FunctTrackCKRes,
    Rich__Future__Rec__TrackParameterisedCherenkovResolutions as
    ParamTrackCKRes,
    Rich__Future__Rec__SIMDQuarticPhotonReco as PhotonReco,
    Rich__Future__Rec__SIMDPhotonPredictedPixelSignal as PhotonPredSignal,
    Rich__Future__Rec__SIMDRecoSummary as RecSummary,
    Rich__Future__Rec__GlobalPID__InitialisePIDInfo as GPIDInit,
    Rich__Future__Rec__GlobalPID__WriteRichPIDs as WriteRichPIDs,
    Rich__Future__Rec__SIMDPixelBackgroundsEstiAvHPD as PixelBackgrounds,
    Rich__Future__Rec__GlobalPID__SIMDLikelihoodMinimiser as LkMinimiser,
    Rich__Future__Rec__MergeRichPIDs as MergePIDs,
    Rich__Future__Rec__TrackFilter as TrackFilter,
)
from PyConf.Tools import TrackSTEPExtrapolator
from RichFutureRecSys.ConfiguredRichReco import (
    defaultNSigmaCuts, defaultMinMaxCKThetaCuts, defaultMaxCKThetaResolutions,
    defaultMinMaxTrackROICuts, defaultMinPhotonProbabilityCuts)


@configurable
def default_rich_reco_options(init_override_opts={}):
    """
    Returns a dict of the default RICH reconstruction options

    Args:
        init_override_opts (dict): override of the initial opts.
    """

    # General options. Those most likely users will want to tweak..
    opts = {
        # The mass hypotheses to consider
        "Particles": [
            "electron",
            "muon",
            "pion",
            "kaon",
            "proton",
            "deuteron",
            "belowThreshold",
        ],
        # The RICH radiators to use
        "RichGases": ["Rich1Gas", "Rich2Gas"],
        # Maximum number of clusters GEC cut
        "MaxPixelClusters":
        200000,
        # Maximum number of tracks GEC cut
        "MaxTracks":
        10000,
        # PID Version
        "PIDVersion":
        2,
    }

    # More technical options
    opts.update({

        #===========================================================
        # Pixel treatment options
        #===========================================================

        # Should pixel clustering be run, for either ( RICH1, RICH2 )
        "ApplyPixelClustering": (False, False),

        # Activate pixels in specific panels in each RICH only.
        "ActivatePanel": (True, True),

        #===========================================================
        # Track treatment options
        #===========================================================

        # Track Extrapolator type
        "TrackExtrapolator":
        TrackSTEPExtrapolator,

        # Tay tracing ring points (Max)
        "NRayTracingRingPointsMax": (96, 96, 96),

        # Tay tracing ring points (Min)
        "NRayTracingRingPointsMin": (16, 16, 16),

        # Tolerence for creating new ray traced CK rings (as fraction of sat. CK theta)
        "RayTracingNewCKRingTol": (0.0, 0.05, 0.1),

        # Detectable Yields Treatment
        "DetectableYieldsPrecision":
        "Average",

        # Treatment of track CK resolutions
        "TkCKResTreatment":
        "Parameterised",

        # Minimum momentum cuts
        "MinP": (0 * GeV, 0 * GeV, 0 * GeV),

        # Maximum momentum cuts
        "MaxP": (9e90 * GeV, 9e90 * GeV, 9e90 * GeV),

        #===========================================================
        # Photon treatment options
        #===========================================================

        # Photon selection cut setting
        #   Nominal - Normal tuned settings
        #   Online  - Loosen settings for Online running (wider side-bands)
        #   None    - Effectively no (pre)selection cuts.
        "PhotonSelection":
        "Nominal",

        # Truncate Cherenkov angles to a fixed precision
        "TruncateCKAngles": (True, True, True),

        # N Sigma cuts by selection, CK resolution model and track type.
        "nSigmaCuts":
        defaultNSigmaCuts(),

        # Min/Max photon CK theta angle cuts
        "MinMaxCKThetaCuts":
        defaultMinMaxCKThetaCuts(),

        # Min/Max Tack ROI pre-sel cuts
        "MinMaxTrackROICuts":
        defaultMinMaxTrackROICuts(),

        # Min Photon Probability cuts
        "MinPhotonProbabilityCuts":
        defaultMinPhotonProbabilityCuts(),

        #===========================================================
        # Settings for the global PID minimisation
        #===========================================================

        # Number of iterations of the global PID background and
        # likelihood minimisation
        "nLikelihoodIterations":
        2,

        # The following are technical settings per iteration.
        # Do not change unless you know what you are doing ;)
        # Array size must be at least as big as the number of iterations

        # Pixel background options

        # Ignore the expected signals based on the track information
        "PDBackIgnoreExpSignals": [True, False, False, False],

        # Minimum allowed pixel background value (RICH1,RICH2)
        "PDBackMinPixBackground": [(0, 0), (0, 0), (0, 0), (0, 0)],

        # Maximum allowed pixel background value (RICH1,RICH2)
        "PDBackMaxPixBackground": [(999, 999), (999, 999), (999, 999),
                                   (999, 999)],

        # Group Size for PDs  RICH1 RICH2
        "PDGroupSize": (4, 4),

        # Likelihood minimizer options

        # Freeze out DLL values
        "TrackFreezeOutDLL": [2, 4, 5, 6],

        # Run a final check on the DLL values
        "FinalDLLCheck": [False, True, True, True],

        # Force change DLL values
        "TrackForceChangeDLL": [-1, -2, -3, -4],

        # likelihood threshold
        "LikelihoodThreshold": [-1e-2, -1e-3, -1e-4, -1e-5],

        # Maximum tracks that can change per iteration
        "MaxTrackChangesPerIt": [5, 5, 4, 3],

        # The minimum DLL signal value
        "MinSignalForNoLLCalc": [1e-3, 1e-3, 1e-3, 1e-3],

        #===========================================================
        # Additional flags will be turned on from elsewhere
        # in case of RICH alignment monitoring.
        #===========================================================

        # Reject ambiguous photons.
        "RejectAmbiguousPhotons": (False, False, False),

        # Save optional mirror data.
        "SaveMirrorData":
        False,
    })

    # Update default options with those defined in steering script
    opts.update(init_override_opts)

    return opts


def get_radiator_bool_opts(options, tktype=""):
    isUp = tktype == "Upstream" or tktype == "Up"
    isSeed = tktype == "Seed"
    # Radiators
    return (
        False,  # Aerogel not supported at all
        "Rich1Gas" in options["RichGases"] and not isSeed,
        "Rich2Gas" in options["RichGases"] and not isUp)


def get_detector_bool_opts(options, tktype=""):
    isUp = tktype == "Upstream" or tktype == "Up"
    isSeed = tktype == "Seed"
    # Detectors
    return ("Rich1Gas" in options["RichGases"] and not isSeed,
            "Rich2Gas" in options["RichGases"] and not isUp)


def configure_rich_particle_properties(options, GroupName=""):
    # Old style from Run II conf
    ppToolName = "RichPartProp" + GroupName
    tool = PartProps(
        name="ToolSvc." + ppToolName, ParticleTypes=options["Particles"])
    return tool
    # Need to work on getting this working ...
    #return PartProps(public=True, ParticleTypes=options["Particles"])


def shortTrackType(track_name):
    tktype = track_name
    if track_name == "Downstream": tktype = "Down"
    if track_name == "Upstream": tktype = "Up"
    return tktype


@configurable
def make_rich_pixels(options, make_raw=default_raw_banks):
    """
    Return pixel specific RICH data.

    Args:
        options (dict): The processing options to use
        make_raw      : The LHCb RawBanks data object

    Returns:
        dict of useful data locations.
    """

    # Configure the particle properties
    configure_rich_particle_properties(options)

    # The conf dict to return
    results = {}

    # Get the raw banks
    rawBanks = make_raw("Rich")

    # Get the detector options
    det_opts = get_detector_bool_opts(options)

    # document raw event and Odin used
    results["RawBanks"] = rawBanks

    # Decode the Raw event to RichSmartIDs
    richDecode = RichDecoder(
        name='RichRawDecoder_{hash}',
        RawBanks=rawBanks,
        Detectors=det_opts,
        Panels=options["ActivatePanel"])
    results["RichDecodedData"] = richDecode.DecodedDataLocation

    # Run clustering
    doClus = options["ApplyPixelClustering"]
    richClus = RichClustering(
        name='RichPixelClustering_{hash}',
        ApplyPixelClustering=doClus,
        MaxClusters=options["MaxPixelClusters"],
        DecodedDataLocation=richDecode.DecodedDataLocation,
        Detectors=det_opts)
    results["RichClusters"] = richClus.RichPixelClustersLocation

    # Make the SIMD pixel objects
    simdPixels = RichSIMDPixels(
        name='RichSIMDPixels_{hash}',
        NoClustering=(not doClus[0] and not doClus[1]),
        RichPixelClustersLocation=richClus.RichPixelClustersLocation)
    results["RichSIMDPixels"] = simdPixels.RichSIMDPixelSummariesLocation

    return results


@configurable
def make_rich_tracks(track_name, input_tracks, options):
    """
    Return tracking specific RICH data from input tracks.

    Args:
        track_name    (str):  The track name to assign to this configuration
        input_tracks (dict): Input track objects to process.
        n_ring_point (3-tuple of int): Number points for mass hypothesis ring ray tracing (Aero,R1Gas,R2Gas).
        options       (dict): The processing options to use

    Returns:
        dict of useful data locations.
    """

    # Configure the particle properties
    configure_rich_particle_properties(options)

    # The conf map to return
    results = {}

    # save the name for this set of results
    results["TrackName"] = track_name

    # Keep tabs on the input tracks used for this conf.
    results["InputTracks"] = input_tracks

    # The detector and radiator options
    rad_opts = get_radiator_bool_opts(options, track_name)
    det_opts = get_detector_bool_opts(options, track_name)

    # Create radiator segments from input tracks
    segments = SegmentCreator(
        name="RichTrackSegments" + track_name + "_{hash}",
        TracksLocation=input_tracks,
        MaxTracks=options["MaxTracks"],
        MinP=options["MinP"],
        MaxP=options["MaxP"],
        TrackExtrapolator=options["TrackExtrapolator"](),
        Detectors=det_opts,
        Radiators=rad_opts)
    results["TrackSegments"] = segments.TrackSegmentsLocation
    results["InitialTrackToSegments"] = segments.TrackToSegmentsLocation
    results["SegmentToTracks"] = segments.SegmentToTrackLocation

    # Intersections of segments with PD panels
    tkGloPnts = TkPanelGlobalPoints(
        name="RichTrackGloPoints" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        TrackSegmentsLocation=segments.TrackSegmentsLocation)
    tkLocPnts = TkPanelLocalPoints(
        name="RichTrackLocPoints" + track_name + "_{hash}",
        TrackGlobalPointsLocation=tkGloPnts.TrackGlobalPointsLocation,
        TrackSegmentsLocation=segments.TrackSegmentsLocation)
    results["TrackGlobalPoints"] = tkGloPnts.TrackGlobalPointsLocation
    results["TrackLocalPoints"] = tkLocPnts.TrackLocalPointsLocation

    # Emitted photon yields
    emitY = EmittedYields(
        name="RichEmittedYields" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        TrackSegmentsLocation=segments.TrackSegmentsLocation)
    results["EmittedYields"] = emitY.EmittedPhotonYieldLocation
    results["EmittedSpectra"] = emitY.EmittedPhotonSpectraLocation

    # Expected CK angles using emitted photon spectra
    emitChAngles = EmittedCKAngles(
        name="RichEmittedCKAngles" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        EmittedPhotonYieldLocation=emitY.EmittedPhotonYieldLocation,
        EmittedPhotonSpectraLocation=emitY.EmittedPhotonSpectraLocation)
    results["EmittedCKAngles"] = emitChAngles.EmittedCherenkovAnglesLocation

    # Cherenkov mass cones using emitted spectra
    emitMassCones = EmittedMassCones(
        name="RichMassCones" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        NRingPointsMin=options["NRayTracingRingPointsMin"],
        NRingPointsMax=options["NRayTracingRingPointsMax"],
        NewCKRingTol=options["RayTracingNewCKRingTol"],
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        CherenkovAnglesLocation=emitChAngles.EmittedCherenkovAnglesLocation)
    results["EmittedCKRings"] = emitMassCones.MassHypothesisRingsLocation

    # Detectable photon yields
    if options["DetectableYieldsPrecision"] == "Full":
        from PyConf.Algorithms import Rich__Future__Rec__DetectablePhotonYields as DetectableYields
    elif options["DetectableYieldsPrecision"] == "Average":
        from PyConf.Algorithms import Rich__Future__Rec__AverageDetectablePhotonYields as DetectableYields
    else:
        raise ValueError("Unknown detectable yield mode '" +
                         options["DetectableYieldsPrecision"] + "'")
    detY = DetectableYields(
        name="RichDetectableYields" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        EmittedSpectraLocation=emitY.EmittedPhotonSpectraLocation,
        MassHypothesisRingsLocation=emitMassCones.MassHypothesisRingsLocation)
    results["DetectableYields"] = detY.DetectablePhotonYieldLocation
    results["DetectableSpectra"] = detY.DetectablePhotonSpectraLocation

    # Geometrical Eff.
    geomEff = GeomEff(
        name="RichGeomEff" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        CherenkovAnglesLocation=emitChAngles.EmittedCherenkovAnglesLocation,
        MassHypothesisRingsLocation=emitMassCones.MassHypothesisRingsLocation)
    results["GeomEffs"] = geomEff.GeomEffsLocation
    results["GeomEffsPerPD"] = geomEff.GeomEffsPerPDLocation
    results["SegmentPhotonFlags"] = geomEff.SegmentPhotonFlagsLocation

    # Select final track segments
    tkSel = SelectTrackSegments(
        name="RichTkSegmentSel" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        InTrackToSegmentsLocation=segments.TrackToSegmentsLocation,
        GeomEffsLocation=geomEff.GeomEffsLocation)
    results["SelectedTrackToSegments"] = tkSel.OutTrackToSegmentsLocation

    # Signal Photon Yields
    sigYields = SignalYields(
        name="RichSignalYields" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        DetectablePhotonYieldLocation=detY.DetectablePhotonYieldLocation,
        DetectablePhotonSpectraLocation=detY.DetectablePhotonSpectraLocation,
        GeomEffsLocation=geomEff.GeomEffsLocation)
    results["SignalYields"] = sigYields.SignalPhotonYieldLocation
    results["SignalSpectra"] = sigYields.SignalPhotonSpectraLocation

    # Signal Cherenkov angles
    sigChAngles = SignalCherenkovAngles(
        name="RichSignalCKAngles" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        SignalPhotonSpectraLocation=sigYields.SignalPhotonSpectraLocation,
        SignalPhotonYieldLocation=sigYields.SignalPhotonYieldLocation)
    results["SignalCKAngles"] = sigChAngles.SignalCherenkovAnglesLocation

    # Track Resolutions
    tktype = shortTrackType(track_name)
    if options["TkCKResTreatment"] == "Functional":
        tkRes = FunctTrackCKRes(
            name="RichCKResolutions" + track_name + "_{hash}",
            Detectors=det_opts,
            Radiators=rad_opts,
            MaxCKThetaRes=defaultMaxCKThetaResolutions()[tktype],
            TrackSegmentsLocation=segments.TrackSegmentsLocation,
            SignalCherenkovAnglesLocation=sigChAngles.
            SignalCherenkovAnglesLocation,
            MassHypothesisRingsLocation=emitMassCones.
            MassHypothesisRingsLocation)
    elif options["TkCKResTreatment"] == "Parameterised":
        tkRes = ParamTrackCKRes(
            name="RichCKResolutions" + track_name + "_{hash}",
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackType=track_name,
            MaxCKThetaRes=defaultMaxCKThetaResolutions()[tktype],
            TrackSegmentsLocation=segments.TrackSegmentsLocation,
            SignalCherenkovAnglesLocation=sigChAngles.
            SignalCherenkovAnglesLocation)
    else:
        raise ValueError("Unknown track CK Theta resolution mode '" +
                         options["TkCKResTreatment"] + "'")
    results["CherenkovResolutions"] = tkRes.CherenkovResolutionsLocation

    return results


@configurable
def make_rich_photons(track_name,
                      input_tracks,
                      options,
                      make_raw=default_raw_banks):
    """
    Return reconstructed photon specific RICH data.

    Args:
        track_name    (str):  The name to assign to this configuration
        input_tracks  (dict): The input tracks to process
        options       (dict): The processing options to use
        make_raw            : The entity that provides the RawBanks to use (??)

    Returns:
        dict of useful data locations.
    """

    # Configure the particle properties
    configure_rich_particle_properties(options)

    # The conf dict to return
    results = {}

    # save the name for this set of results
    results["TrackName"] = track_name

    # pixel and track reco
    pixel_conf = make_rich_pixels(make_raw=make_raw, options=options)
    track_conf = make_rich_tracks(
        track_name=track_name, input_tracks=input_tracks, options=options)

    # include in returned configuration
    results.update(pixel_conf)
    results.update(track_conf)

    # The detector and radiator options
    rad_opts = get_radiator_bool_opts(options, track_name)
    det_opts = get_detector_bool_opts(options, track_name)

    # Photon Reco.
    photSel = options["PhotonSelection"]
    tkCKRes = options["TkCKResTreatment"]
    tktype = shortTrackType(track_name)
    nSigmaC = options["nSigmaCuts"][photSel][tkCKRes][tktype]
    minMaxCKTheta = options["MinMaxCKThetaCuts"][photSel]
    minMaxTkROI = options["MinMaxTrackROICuts"][photSel]
    photReco = PhotonReco(
        name="RichPhotonReco" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        PreSelNSigma=nSigmaC[0],
        NSigma=nSigmaC[1],
        PreSelMinTrackROI=minMaxTkROI["Min"],
        PreSelMaxTrackROI=minMaxTkROI["Max"],
        MinAllowedCherenkovTheta=minMaxCKTheta["Min"],
        MaxAllowedCherenkovTheta=minMaxCKTheta["Max"],
        TruncateCKAngles=options["TruncateCKAngles"],
        TrackSegmentsLocation=track_conf["TrackSegments"],
        CherenkovAnglesLocation=track_conf["SignalCKAngles"],
        CherenkovResolutionsLocation=track_conf["CherenkovResolutions"],
        TrackLocalPointsLocation=track_conf["TrackLocalPoints"],
        TrackToSegmentsLocation=track_conf["SelectedTrackToSegments"],
        SegmentPhotonFlagsLocation=track_conf["SegmentPhotonFlags"],
        RichSIMDPixelSummariesLocation=pixel_conf["RichSIMDPixels"],
        RejectAmbiguousPhotons=options["RejectAmbiguousPhotons"],
        SaveMirrorData=options["SaveMirrorData"])
    results["CherenkovPhotons"] = photReco.CherenkovPhotonLocation
    results["PhotonToParents"] = photReco.PhotonToParentsLocation
    results["PhotonMirrorData"] = photReco.PhotonMirrorDataLocation

    # Predicted pixel signals
    photPredSig = PhotonPredSignal(
        name="RichPredPixelSignal" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        MinPhotonProbability=options["MinPhotonProbabilityCuts"][photSel],
        RichSIMDPixelSummariesLocation=pixel_conf["RichSIMDPixels"],
        TrackSegmentsLocation=track_conf["TrackSegments"],
        CherenkovPhotonLocation=photReco.CherenkovPhotonLocation,
        PhotonToParentsLocation=photReco.PhotonToParentsLocation,
        CherenkovAnglesLocation=track_conf["SignalCKAngles"],
        CherenkovResolutionsLocation=track_conf["CherenkovResolutions"],
        PhotonYieldLocation=track_conf["DetectableYields"])
    results["PhotonSignals"] = photPredSig.PhotonSignalsLocation

    return results


@configurable
def make_rich_pids(track_name,
                   input_tracks,
                   options,
                   make_raw=default_raw_banks):
    """
    Return RICH PID data.

    Args:
        track_name    (str):  The name to assign to this configuration
        input_tracks  (dict): The input tracks to process
        options       (dict): The processing options to use
        make_raw            : The entity that provides the RawBanks to use (??)

    Returns:
        dict of useful data locations.
    """

    # Configure the particle properties
    configure_rich_particle_properties(options)

    # conf dict to return
    results = {}

    # save the name for this set of results
    results["TrackName"] = track_name

    # pixel and track reco
    pixel_conf = make_rich_pixels(make_raw=make_raw, options=options)
    track_conf = make_rich_tracks(
        track_name=track_name, input_tracks=input_tracks, options=options)
    photon_conf = make_rich_photons(
        track_name=track_name,
        input_tracks=input_tracks,
        make_raw=make_raw,
        options=options)

    # include in returned configuration
    results.update(pixel_conf)
    results.update(track_conf)
    results.update(photon_conf)

    # The detector and radiator options
    rad_opts = get_radiator_bool_opts(options, track_name)
    det_opts = get_detector_bool_opts(options, track_name)

    # Create working event summary of reco info
    recSum = RecSummary(
        name="RichRecSummary" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        TrackSegmentsLocation=track_conf["TrackSegments"],
        TrackToSegmentsLocation=track_conf["SelectedTrackToSegments"],
        PhotonToParentsLocation=photon_conf["PhotonToParents"],
        DetectablePhotonYieldLocation=track_conf["DetectableYields"],
        SignalPhotonYieldLocation=track_conf["SignalYields"],
        PhotonSignalsLocation=photon_conf["PhotonSignals"],
        RichSIMDPixelSummariesLocation=pixel_conf["RichSIMDPixels"])
    results["SummaryTracks"] = recSum.SummaryTracksLocation
    results["Summarypixels"] = recSum.SummaryPixelsLocation

    # ==================================================================
    # RICH Global PID
    # ==================================================================

    # Initalise some default locations
    gpidInit = GPIDInit(
        name="RichGPIDInit" + track_name + "_{hash}",
        Detectors=det_opts,
        Radiators=rad_opts,
        SummaryTracksLocation=recSum.SummaryTracksLocation)

    # Cache PID and DLL locations by iteration
    PIDs = {}
    DLLs = {}
    PIDs[0] = gpidInit.TrackPIDHyposLocation
    DLLs[0] = gpidInit.TrackDLLsLocation

    # PID iterations
    for it in range(0, options["nLikelihoodIterations"]):

        itN = "It" + repr(it)

        # Pixel backgrounds
        pixBkgs = PixelBackgrounds(
            name="RichPixBackgrounds" + itN + track_name + "_{hash}",
            # Settings
            Detectors=det_opts,
            Radiators=rad_opts,
            PDGroupSize=options["PDGroupSize"],
            IgnoreExpectedSignals=options["PDBackIgnoreExpSignals"][it],
            MinPixelBackground=options["PDBackMinPixBackground"][it],
            MaxPixelBackground=options["PDBackMaxPixBackground"][it],
            # Input data
            TrackPIDHyposLocation=PIDs[it],
            TrackToSegmentsLocation=track_conf["SelectedTrackToSegments"],
            TrackSegmentsLocation=track_conf["TrackSegments"],
            GeomEffsPerPDLocation=track_conf["GeomEffsPerPD"],
            DetectablePhotonYieldLocation=track_conf["DetectableYields"],
            RichSIMDPixelSummariesLocation=pixel_conf["RichSIMDPixels"])

        # Likelihood minimiser
        like = LkMinimiser(
            name="RichGPIDLikelihood" + itN + track_name + "_{hash}",
            # Settings
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackFreezeOutDLL=options["TrackFreezeOutDLL"][it],
            FinalDLLCheck=options["FinalDLLCheck"][it],
            TrackForceChangeDLL=options["TrackForceChangeDLL"][it],
            LikelihoodThreshold=options["LikelihoodThreshold"][it],
            MaxTrackChangesPerIt=options["MaxTrackChangesPerIt"][it],
            MinSignalForNoLLCalc=options["MinSignalForNoLLCalc"][it],
            # Input data
            SummaryTracksLocation=recSum.SummaryTracksLocation,
            SummaryPixelsLocation=recSum.SummaryPixelsLocation,
            PixelBackgroundsLocation=pixBkgs.PixelBackgroundsLocation,
            TrackDLLsInputLocation=DLLs[it],
            TrackPIDHyposInputLocation=PIDs[it],
            PhotonToParentsLocation=photon_conf["PhotonToParents"],
            PhotonSignalsLocation=photon_conf["PhotonSignals"])

        # Save the outputs for the next it
        PIDs[it + 1] = like.TrackPIDHyposOutputLocation
        DLLs[it + 1] = like.TrackDLLsOutputLocation

        # save the last pixel backgrounds to the conf (for monitoring)
        results["RichPixelBackgrounds"] = pixBkgs.PixelBackgroundsLocation

    # Write the file RichPID objects
    writePIDs = WriteRichPIDs(
        name="RichPIDsWriter" + track_name + "_{hash}",
        # Settings
        Detectors=det_opts,
        Radiators=rad_opts,
        PIDVersion=options["PIDVersion"],
        # Inputs
        TracksLocation=track_conf["InputTracks"],
        SummaryTracksLocation=recSum.SummaryTracksLocation,
        TrackPIDHyposInputLocation=PIDs[options["nLikelihoodIterations"]],
        TrackDLLsInputLocation=DLLs[options["nLikelihoodIterations"]])
    results["RichPIDs"] = writePIDs.RichPIDsLocation

    # return the final dict
    return results


@configurable
def make_all_rich_pids(best_tracks,
                       reco_opts,
                       make_pids=make_rich_pids,
                       track_types=['Long', 'Downstream', 'Upstream']):
    # First, extract seperate selections for the Long, Downstream, Upstream and Seed tracks
    # Eventually this should not be done, as the best container should go and
    # the tracks should be provided from source in seperate containers.
    richTkFilt = TrackFilter(InTracksLocation=best_tracks["v1"])
    track_type_locations = {
        'Long': richTkFilt.OutLongTracksLocation,
        'Downstream': richTkFilt.OutDownTracksLocation,
        'Upstream': richTkFilt.OutUpTracksLocation,
        'Seed': richTkFilt.OutSeedTracksLocation,
    }
    richRecConfs = dict()
    # Create the confs for each RICH reco for the different track types (Long, Downstream, Upstream, Seed)
    for track_type in track_types:
        richRecConfs[track_type] = make_pids(
            track_name=track_type,
            input_tracks=track_type_locations[track_type],
            options=reco_opts)
    return richRecConfs


def make_merged_rich_pids(richRecConfs):

    # Merge them for now to be compatible with Brunel.
    merged_pids = MergePIDs(
        InputRichPIDLocations=[
            conf["RichPIDs"] for conf in richRecConfs.values()
        ],
        PIDVersion=2,
        outputs={'OutputRichPIDLocation': persisted_location('RichPIDs')},
    )
    return merged_pids.OutputRichPIDLocation
