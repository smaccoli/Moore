###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Functions to create charged and neutral protoparticles from reconstruction output."""
from PyConf import configurable
from PyConf.Algorithms import (FunctionalChargedProtoParticleMaker,
                               FutureNeutralProtoPAlg,
                               ProtoParticlesEmptyProducer)
from PyConf.Tools import (
    CaloFutureElectron,
    CaloFutureHypoEstimator,
    CaloFutureHypo2CaloFuture,
    FutureGammaPi0XGBoostTool,
    FutureGammaPi0SeparationTool,
    FutureNeutralIDTool,
    ChargedProtoParticleAddEcalInfo,
    ChargedProtoParticleAddHcalInfo,
    ChargedProtoParticleAddBremInfo,
    ChargedProtoParticleAddRichInfo,
    ChargedProtoParticleAddMuonInfo,
    ChargedProtoParticleAddCombineDLLs,
    ANNGlobalPID__ChargedProtoParticleAddANNPIDInfo,
)

import Functors as F


def hypo_estimator(calo_pids):
    return CaloFutureHypoEstimator(
        DigitLocation=calo_pids["digitsEcal"],
        Hypo2Calo=CaloFutureHypo2CaloFuture(
            EcalDigitsLocation=calo_pids["digitsEcal"],
            HcalDigitsLocation=calo_pids["digitsHcal"]),
        Pi0Separation=FutureGammaPi0SeparationTool(),
        Pi0SeparationXGB=FutureGammaPi0XGBoostTool(
            DigitLocation=calo_pids["digitsEcal"]),
        NeutralID=FutureNeutralIDTool(),
        Electron=CaloFutureElectron(),
    )


@configurable
def make_neutral_protoparticles(calo_pids, location=None):
    """Create neutral ProtoParticles from Calorimeter reconstruction output.

    Args:
        calo_pids: dictionary containing all necessary inputs to create neutral ProtoParticles.

    Returns:
        DataHandle to the container of neutral ProtoParticles

    """
    if calo_pids is None:
        neutral_protos_locations = {
            'ProtoParticleLocation': ProtoParticlesEmptyProducer().Output,
            'ClusterMass': None,
            'CaloClusterCode': None,
            'IsNotH': None,
            'IsPhoton': None
        }  # neutral protos have to be written out, even if the Calo did not run
        # The full data flow is functional, we only keep this for compatibility with reco from file.
    else:
        neutral_protos = FutureNeutralProtoPAlg(
            CaloHypoEstimator=hypo_estimator(calo_pids),
            MergedPi0s=calo_pids["v1_mergedPi0s"],
            Photons=calo_pids["v1_photons"],
            SplitPhotons=calo_pids["v1_splitPhotons"],
            TrackMatchTable=calo_pids["v2_clustertrackmatches"],
            outputs={
                'ProtoParticleLocation': location,
                'ClusterMass': None,
                'CaloClusterCode': None,
                'IsNotH': None,
                'IsPhoton': None
            },
        )
        neutral_protos_locations = {
            'ProtoParticleLocation': neutral_protos.ProtoParticleLocation,
            'ClusterMass': neutral_protos.ClusterMass,
            'CaloClusterCode': neutral_protos.CaloClusterCode,
            'IsNotH': neutral_protos.IsNotH,
            'IsPhoton': neutral_protos.IsPhoton
        }

    return neutral_protos_locations


@configurable
def make_charged_protoparticles(tracks,
                                rich_pids,
                                calo_pids,
                                muon_pids,
                                track_types=["Long", "Downstream", "Upstream"],
                                location=None):
    """Create charged ProtoParticles from tracking and particle identification reconstruction outputs.

    Args:
        tracks: input tracks to create charged protos
        rich_pids: input rich_pids created from tracks
        calo_pids: input calo_pids from Calorimeter reconstruction
        muon_pids: input muon_pids created from tracks

    Returns:
        dict with DataHandle to the container of charged ProtoParticles and the sequence of algorithms to create them.

    """
    addInfo = []

    accepted_types = {
        "calo": ["Long", "Downstream", "Ttrack"],
        "brem": ["Long", "Downstream", "Upstream", "Velo"],
        "ANN": ["Long", "Downstream", "Upstream"]
    }

    track_predicates = {
        "Long": F.TRACKISLONG,
        "Downstream": F.TRACKISDOWNSTREAM,
        "Upstream": F.TRACKISUPSTREAM,
        "Ttrack": F.TRACKISTTRACK,
        "Velo": F.TRACKISVELO
    }

    # add CALO info
    if calo_pids is not None:
        addInfo += [
            ChargedProtoParticleAddEcalInfo(
                name='addEcalInfo',
                InputPIDs=[
                    calo_pids["v1_chargedpids"][tt]
                    for tt in calo_pids["v1_chargedpids"].keys()
                    if tt in accepted_types["calo"]
                ],
            ),
            ChargedProtoParticleAddHcalInfo(
                name='addHcalInfo',
                InputPIDs=[
                    calo_pids["v1_chargedpids"][tt]
                    for tt in calo_pids["v1_chargedpids"].keys()
                    if tt in accepted_types["calo"]
                ],
            ),
            ChargedProtoParticleAddBremInfo(
                name='addBremInfo',
                InputPIDs=[
                    calo_pids["v1_breminfos"][tt]
                    for tt in calo_pids["v1_chargedpids"].keys()
                    if tt in accepted_types["brem"]
                ],
            ),
        ]

    # add RICH info
    if rich_pids is not None:
        addInfo += [
            ChargedProtoParticleAddRichInfo(
                name='addRichInfo', InputRichPIDLocation=rich_pids)
        ]

    # add MUON info
    if muon_pids is not None:
        addInfo += [
            ChargedProtoParticleAddMuonInfo(
                name='addMuonInfo', InputMuonPIDLocation=muon_pids)
        ]

    if (calo_pids is not None) or (rich_pids is not None) or (muon_pids is
                                                              not None):
        addInfo.append(ChargedProtoParticleAddCombineDLLs())

    if ("Upstream" in track_types and (rich_pids is not None)) or (
        ("Downstream" in track_types or "Long" in track_types) and
        (calo_pids is not None) and (rich_pids is not None) and
        (muon_pids is not None)):
        # Set up ann pids
        addInfo += [
            ANNGlobalPID__ChargedProtoParticleAddANNPIDInfo(
                name="AddANNPID" + TrackType + PIDType,
                NetworkVersion="MCUpTuneV1",
                PIDType=PIDType,
                TrackType=TrackType,
            ) for TrackType in track_types
            if TrackType in accepted_types["ANN"] for PIDType in
            ["Electron", "Muon", "Pion", "Kaon", "Proton", "Ghost"]
        ]

    # this should always be run
    is_list = type(tracks) is list
    charged_protos = FunctionalChargedProtoParticleMaker(
        Inputs=[track["v1"]
                for track in tracks] if is_list else [tracks["v1"]],
        Code=F.require_any(*[track_predicates[i] for i in track_types]),
        AddInfo=addInfo,
        outputs={'Output': location})

    return charged_protos.Output
