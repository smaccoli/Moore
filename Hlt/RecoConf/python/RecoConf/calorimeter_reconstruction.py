###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.packing import persisted_location
from PyConf.Algorithms import (
    AcceptanceBremAlg, AcceptanceEcalAlg, AcceptanceHcalAlg,
    TrackToEcalEnergyAlg, TrackToHcalEnergyAlg, CaloChargedPIDsAlg,
    CaloChargedBremAlg, CaloFutureRawToDigits, FutureCellularAutomatonAlg,
    TrackBasedElectronShowerAlg, MergeTrackMatchTables,
    ChargedPIDsConverter_v1, BremInfosConverter_v1,
    ChargedPIDsConverter_v1Shared, BremInfosConverter_v1Shared,
    SelectiveTrackMatchAlg, ClassifyPhotonElectronAlg, CaloFutureMergedPi0,
    GraphClustering, SelectiveElectronMatchAlg, SelectiveBremMatchAlg,
    LHCb__Converters__Calo__Cluster__v1__fromV2 as ClusterConverter,
    LHCb__Converters__Calo__Hypo__v1__fromV2 as HypoConverter,
    LHCb__Converters__Calo__Hypo__v1__MergedPi0__fromV2 as MergedPi0Converter)

from PyConf.Tools import (CaloFutureECorrection, CaloFutureSCorrection,
                          CaloFutureLCorrection)
from PyConf.application import default_raw_banks
from DDDB.CheckDD4Hep import UseDD4Hep


def make_ecal_digits(raw, rawerror):
    ecalpath = '/world/DownstreamRegion/Ecal:DetElement-Info-IOV' if UseDD4Hep else '/dd/Structure/LHCb/DownstreamRegion/Ecal'
    adc_alg = CaloFutureRawToDigits(
        name='FutureEcalZSup',
        RawBanks=raw,
        ErrorRawBanks=rawerror,
        #need a different property wrt make_hcal_digits and output cannot be set
        #so DetectorLocation has to be set here
        DetectorLocation=ecalpath)
    return adc_alg.OutputDigitData


def make_hcal_digits(raw, rawerror):
    hcalpath = '/world/DownstreamRegion/Hcal:DetElement-Info-IOV' if UseDD4Hep else '/dd/Structure/LHCb/DownstreamRegion/Hcal'
    adc_alg_Hcal = CaloFutureRawToDigits(
        name='FutureHcalZSup',
        RawBanks=raw,
        ErrorRawBanks=rawerror,
        #need a different property wrt make_ecal_digits and output cannot be set
        #so DetectorLocation has to be set here
        DetectorLocation=hcalpath)
    return adc_alg_Hcal.OutputDigitData


@configurable
def make_digits(calo_raw_bank=False):
    if calo_raw_bank:  # run3 bank types
        rawEventCalo = default_raw_banks("Calo")
        rawEventCaloError = default_raw_banks("CaloError")
        digitsEcal = make_ecal_digits(rawEventCalo, rawEventCaloError)
        digitsHcal = make_hcal_digits(rawEventCalo, rawEventCaloError)
    else:  # run2 bank types - used in old run 3 MC samples
        rawEventEcal = default_raw_banks("EcalPacked")
        rawEventHcal = default_raw_banks("HcalPacked")
        rawEventEcalError = default_raw_banks("EcalPackedError")
        rawEventHcalError = default_raw_banks("HcalPackedError")
        digitsEcal = make_ecal_digits(rawEventEcal, rawEventEcalError)
        digitsHcal = make_hcal_digits(rawEventHcal, rawEventHcalError)

    return {"digitsEcal": digitsEcal, "digitsHcal": digitsHcal}


def make_correction_base_tool(condPath):
    from PyConf.Tools import CaloFutureCorrectionBase
    return CaloFutureCorrectionBase(ConditionName=condPath)


def make_covariance_matrix_tool(
        condPath='/world/DownstreamRegion/Ecal:EcalCovariance'
        if UseDD4Hep else 'Conditions/Reco/Calo/EcalCovariance',
        make_correction_base=make_correction_base_tool):
    from PyConf.Tools import FutureClusterCovarianceMatrixTool
    return FutureClusterCovarianceMatrixTool(
        name='EcalCovariance',
        ConditionName=condPath,
        CorrectionBase=make_correction_base(condPath))


def make_subCluster_selector_tool(
        condPath='/world/DownstreamRegion/Ecal:EcalClusterMasks'
        if UseDD4Hep else 'Conditions/Reco/Calo/EcalClusterMasks',
        make_correction_base=make_correction_base_tool):
    from PyConf.Tools import FutureSubClusterSelectorTool
    return FutureSubClusterSelectorTool(
        name='EcalClusterTag',
        ConditionName=condPath,
        CaloFutureCorrectionBase=make_correction_base(condPath))


def make_shower_overlap_tool(
        condPath='/world/DownstreamRegion/Ecal:PhotonShowerProfile'
        if UseDD4Hep else 'Conditions/Reco/Calo/PhotonShowerProfile',
        make_correction_base=make_correction_base_tool):
    from PyConf.Tools import CaloFutureShowerOverlapTool
    return CaloFutureShowerOverlapTool(
        name='SplitPhotonShowerOverlap',
        Profile=condPath,
        CaloFutureCorrectionBase=make_correction_base(condPath))


def make_shower_overlap(inputData):
    from PyConf.Algorithms import CaloFutureShowerOverlap
    return CaloFutureShowerOverlap(
        InputData=inputData,
        SubClusterSelector=make_subCluster_selector_tool(),
        OverlapTool=make_shower_overlap_tool()).OutputData


def make_cluster_covariance(inputData, energyTags=[], positionTags=[]):
    from PyConf.Algorithms import CaloFutureClusterCovarianceAlg
    return CaloFutureClusterCovarianceAlg(
        InputData=inputData,
        EnergyTags=energyTags,
        PositionTags=positionTags,
        CovarianceTool=make_covariance_matrix_tool(),
        TaggerTool=make_subCluster_selector_tool()).OutputData


def make_clusters(digits):
    ecalClustersRaw = GraphClustering(InputData=digits).OutputData
    return make_cluster_covariance(ecalClustersRaw)


def make_clusters_various(digits):
    """Make various types of clusters for resolution tests.

    See Also:
        `make_calo_resolution_gamma`

    """
    # Graph Clustering with covar
    ecalClustersGCRaw = GraphClustering(InputData=digits).OutputData
    ecalClustersGCwithCovar = make_cluster_covariance(ecalClustersGCRaw)

    # Cellular Automaton no overlap
    ecalClustersRaw = FutureCellularAutomatonAlg(InputData=digits).OutputData

    # Cellular Automaton overlap default with covar
    ecalClustersOverlap = make_shower_overlap(ecalClustersRaw)
    ecalClustersOverlapWithCovar = make_cluster_covariance(ecalClustersOverlap)

    return {
        "ecalClustersNoOverlap": ecalClustersRaw,
        "ecalClustersOverlapWithCovar": ecalClustersOverlapWithCovar,
        "ecalClustersGCWithCovar": ecalClustersGCwithCovar
    }


def make_clusters_shapes(digits):

    cluster_shapes = {
        "3x3": {
            "energy": "3x3",
            "position": "3x3"
        },
        "2x2": {
            "energy": "2x2",
            "position": "2x2"
        },
        "SwissCross": {
            "energy": "SwissCross",
            "position": "SwissCross"
        },
        "Mixed": {
            "energy": "2x2",
            "position": "SwissCross"
        },
    }

    results = {}
    for title, shapes in cluster_shapes.items():
        ecalClustersRaw = GraphClustering(
            InputData=digits,
            CellSelectorForEnergy=shapes["energy"],
            CellSelectorForPosition=shapes["position"]).OutputData
        ecalClustersOverlapWithCovar = make_cluster_covariance(
            ecalClustersRaw,
            energyTags=[shapes["energy"] for _ in range(3)],
            positionTags=[shapes["position"] for _ in range(3)])
        results[title] = {
            "ecalClustersRaw": ecalClustersRaw,
            "ecalClustersOverlapWithCovar": ecalClustersOverlapWithCovar,
        }
    return results


def make_photons_and_electrons(clusters, matchtable, pvs):
    photonElectronAlg = ClassifyPhotonElectronAlg(
        InputTable=matchtable,
        InputClusters=clusters,
        ElectrMaxChi2=50.,
        ElectrMinEt=50.0,
        MinDigits=2,
        PhotonMinChi2=10.,
        PhotonMinEt=50.0,
        PrimaryVertices=pvs,
        PhotonCorrection=[
            CaloFutureECorrection(),
            CaloFutureSCorrection(),
            CaloFutureLCorrection()
        ],
        ElectronCorrection=[
            CaloFutureECorrection(),
            CaloFutureSCorrection(),
            CaloFutureLCorrection()
        ])
    return {
        "photons": photonElectronAlg.OutputPhotons,
        "electrons": photonElectronAlg.OutputElectrons,
    }


def make_acceptance(
        tracks,
        accepted_types={
            "calo": ["Long", "Downstream", "Ttrack"],
            "brem": ["Long", "Downstream", "Upstream", "Velo"]
        },
        base=r"CaloAcceptance{0}Alg_{1}_{{hash}}",
):
    accs = dict()
    for tt in tracks.keys():
        accs[tt] = {}
        if tt in accepted_types["calo"]:
            accs[tt]["inEcal"] = AcceptanceEcalAlg(
                name=base.format("Ecal", tt), Tracks=tracks[tt]).Output
            accs[tt]["inHcal"] = AcceptanceHcalAlg(
                name=base.format("Hcal", tt), Tracks=tracks[tt]).Output
        if tt in accepted_types["brem"]:
            accs[tt]["inBrem"] = AcceptanceBremAlg(
                name=base.format("Brem", tt), Tracks=tracks[tt]).Output
    return accs


def make_track_energy_in_calo(
        tracks_incalo,
        digits_ecal,
        digits_hcal,
        accepted_types=["Long", "Downstream", "Ttrack"],
        base=r"CaloTrackTo{0}EnergyAlg_{1}_{{hash}}",
):
    energies = dict()
    for tt in tracks_incalo.keys():
        if tt in accepted_types:
            energies[tt] = {}
            energies[tt]["EcalE"] = TrackToEcalEnergyAlg(
                name=base.format("Ecal", tt),
                TracksInCalo=tracks_incalo[tt]["inEcal"],
                Digits=digits_ecal).Output
            energies[tt]["HcalE"] = TrackToHcalEnergyAlg(
                name=base.format("Hcal", tt),
                TracksInCalo=tracks_incalo[tt]["inHcal"],
                Digits=digits_hcal).Output
    return energies


def make_trackbased_eshower(
        tracks_incalo,
        ecaldigits,
        accepted_types=["Long", "Downstream", "Ttrack"],
        base=r"CaloTrackBasedElectronShowerAlg_{0}_{{hash}}",
):
    eshower = dict()
    for tt in tracks_incalo.keys():
        if tt in accepted_types:
            eshower[tt] = TrackBasedElectronShowerAlg(
                name=base.format(tt),
                TracksInCalo=tracks_incalo[tt]["inEcal"],
                Digits=ecaldigits).Output
    return eshower


def make_track_cluster_matching(
        clusters,
        tracks_incalo,
        accepted_types=["Long", "Downstream", "Ttrack"],
        base_trackmatch=r"CaloSelectiveTrackMatchAlg_{0}_{{hash}}",
        base_merge=r"CaloMergeTrackMatchTables_{hash}",
):
    trackmatch = dict()
    for tt in tracks_incalo.keys():
        if tt in accepted_types:
            trackmatch[tt] = SelectiveTrackMatchAlg(
                name=base_trackmatch.format(tt),
                InputTracksInCalo=tracks_incalo[tt]["inEcal"],
                InputClusters=clusters).Output
    trackmatch["combined"] = MergeTrackMatchTables(
        name=base_merge,
        Inputs=[trackmatch[tt] for tt in trackmatch.keys()]).Output
    return trackmatch


def make_track_electron_and_brem_matching(
        tracks_incalo,
        trackmatches,
        ecaldigits,
        electrons,
        photons,
        accepted_types={
            "brem": ["Long", "Downstream", "Upstream", "Velo"],
            "calo": ["Long", "Downstream", "Ttrack"]
        },
        base=r"CaloSelective{0}MatchAlg_{1}_{{hash}}",
):
    matches = dict()
    for tt in tracks_incalo.keys():
        matches[tt] = {}
        if tt in accepted_types["brem"]:
            bremalg = SelectiveBremMatchAlg(
                name=base.format("Brem", tt),
                InputHypos=photons,
                InputDigits=ecaldigits,
                TracksInCalo=tracks_incalo[tt]["inBrem"])
            matches[tt] = {
                "BremMatch": bremalg.OutputMatchTable,
                "BremEnergy": bremalg.OutputEnergyTable,
            }
        if tt in accepted_types["calo"]:
            matches[tt]["ElectronMatch"] = SelectiveElectronMatchAlg(
                name=base.format("Electron", tt),
                InputHypos=electrons,
                InputTracks2Clusters=trackmatches[tt]).Output
    return matches


@configurable
def make_calo_chargedpids(
        tracks_incalo,
        calo_energies,
        hypomatches,
        clustermatches,
        eshower,
        bremmethod="Mixed",
        accepted_types={
            "brem": ["Long", "Downstream", "Upstream", "Velo"],
            "calo": ["Long", "Downstream", "Ttrack"]
        },
        base=r"CaloCharged{0}Alg_{1}_{{hash}}",
):
    pids = {"chargedpids": {}, "breminfos": {}}
    for tt in tracks_incalo.keys():
        if tt in accepted_types["calo"]:
            pids["chargedpids"][tt] = CaloChargedPIDsAlg(
                name=base.format("PIDs", tt),
                InEcal=tracks_incalo[tt]["inEcal"],
                InHcal=tracks_incalo[tt]["inHcal"],
                HcalEnergy=calo_energies[tt]["HcalE"],
                Tracks2Clusters=clustermatches[tt],
                Tracks2Electrons=hypomatches[tt]["ElectronMatch"],
                ElectronShower=eshower[tt])
        if tt in accepted_types["brem"]:
            pids["breminfos"][tt] = CaloChargedBremAlg(
                name=base.format("Brem", tt),
                InBrem=tracks_incalo[tt]["inBrem"],
                BremEnergy=hypomatches[tt]["BremEnergy"],
                Brems=hypomatches[tt]["BremMatch"],
                BremMethod=bremmethod).Output
    return pids


@configurable
def make_convert_calo_chargedpids(
        calopids_v3,
        trackrels,
        shared_container=False,
        base=r"{0}Converter_{1}_{{hash}}",
):
    pids = {"chargedpids": {}, "breminfos": {}}
    if not trackrels: return pids
    if not len(trackrels): return pids
    for tt in calopids_v3["chargedpids"]:
        pids["chargedpids"][tt] = ChargedPIDsConverter_v1(
            name=base.format("ChargedCaloPIDs", tt),
            ChargedPIDs=calopids_v3["chargedpids"][tt],
            Relations=trackrels[tt]
        ).Output if not shared_container else ChargedPIDsConverter_v1Shared(
            name=base.format("ChargedCaloPIDs", tt),
            ChargedPIDs=calopids_v3["chargedpids"][tt],
            Relations=trackrels[tt]).Output
    for tt in calopids_v3["breminfos"]:
        pids["breminfos"][tt] = BremInfosConverter_v1(
            name=base.format("BremInfos", tt),
            ChargedPIDs=calopids_v3["breminfos"][tt],
            Relations=trackrels[tt]
        ).Output if not shared_container else BremInfosConverter_v1Shared(
            name=base.format("BremInfos", tt),
            ChargedPIDs=calopids_v3["breminfos"][tt],
            Relations=trackrels[tt]).Output
    return pids


def make_merged_pi0(ecalClusters, pvs, maxIterations=25, applyLSCorr=True):
    mergedPi0 = CaloFutureMergedPi0(
        InputData=ecalClusters,
        EtCut=1500.0,
        PrimaryVertices=pvs,
        PhotonTools=[
            CaloFutureECorrection(),
            CaloFutureSCorrection(),
            CaloFutureLCorrection()
        ],
        MaxIterations=maxIterations,
        ApplyLSCorr=applyLSCorr,
        EcalCovariance=make_covariance_matrix_tool(),
        EcalClusterTag=make_subCluster_selector_tool(),
        SplitPhotonShowerOverlap=make_shower_overlap_tool())
    return {
        "ecalSplitClusters": mergedPi0.SplitClusters,
        "mergedPi0s": mergedPi0.MergedPi0s,
    }


def make_merged_pi0_various(ecalClusters, pvs):
    """Make various types of pi0 for resolution tests.

    See Also:
        `make_calo_resolution_pi0`

    """
    # default
    mergedPi0 = make_merged_pi0(ecalClusters, pvs)
    # no overlap
    mergedPi0NoOverlap = make_merged_pi0(ecalClusters, pvs, maxIterations=0)
    # overlap fast (with stopping criteria)
    mergedPi0OverlapFast = make_merged_pi0(
        ecalClusters, pvs, maxIterations=-25)
    # overlap without S, L corrections (and also with stopping criteria)
    mergedPi0OverlapNoCor = make_merged_pi0(
        ecalClusters, pvs, applyLSCorr=False, maxIterations=-25)
    return {
        #"ecalSplitClusters": mergedPi0.SplitClusters,
        "mergedPi0s": mergedPi0.MergedPi0s,
        "mergedPi0sNoOverlap": mergedPi0NoOverlap.MergedPi0s,
        "mergedPi0sOverlapFast": mergedPi0OverlapFast.MergedPi0s,
        "mergedPi0sOverlapNoCor": mergedPi0OverlapNoCor.MergedPi0s,
        #"splitPhotons": mergedPi0.SplitPhotons
    }


@configurable
def make_calo(tracks,
              pvs,
              make_raw=default_raw_banks,
              chargedpid_types={
                  "calo": ["Long", "Downstream"],
                  "brem": ["Long", "Downstream", "Upstream"]
              },
              trackrels=None):
    # digits
    digitsCalo = make_digits()
    digitsEcal = digitsCalo["digitsEcal"]
    digitsHcal = digitsCalo["digitsHcal"]

    # clusters
    ecalClusters = make_clusters(digitsEcal)

    # track acceptances
    tracks_incalo = make_acceptance(tracks)

    # track-cluster matching
    tcmatches = make_track_cluster_matching(ecalClusters, tracks_incalo)

    # calo hypos
    PhElOutput = make_photons_and_electrons(ecalClusters,
                                            tcmatches["combined"], pvs)
    photons = PhElOutput["photons"]
    electrons = PhElOutput["electrons"]

    # pi0s
    mergePi0Out = make_merged_pi0(ecalClusters, pvs)
    ecalSplitClusters = mergePi0Out["ecalSplitClusters"]
    mergedPi0s = mergePi0Out["mergedPi0s"]

    # track-hypo matching
    thmatches = make_track_electron_and_brem_matching(
        tracks_incalo, tcmatches, digitsEcal, electrons, photons)

    # track-calo energies
    track2caloe = make_track_energy_in_calo(tracks_incalo, digitsEcal,
                                            digitsHcal)

    # electron shower variables
    eshower = make_trackbased_eshower(tracks_incalo, digitsEcal)

    # combined charged pid and convert to v1
    calopids_v3 = make_calo_chargedpids(
        tracks_incalo,
        track2caloe,
        thmatches,
        tcmatches,
        eshower,
        accepted_types=chargedpid_types)
    calopids_v1 = make_convert_calo_chargedpids(calopids_v3, trackrels)

    # conversions to v1 versions of clusters/hypos
    old_ecal = ClusterConverter(InputClusters=ecalClusters)
    old_ecalClusters = old_ecal.OutputClusters
    old_ecalDigits = old_ecal.OutputDigits

    old_photons = HypoConverter(
        InputHypos=photons,
        InputClusters=old_ecalClusters,
        ExtraInputs=[old_ecalDigits],
        outputs={
            'OutputHypos': persisted_location('CaloPhotons'),
            'OutputTable': None,
        },
    ).OutputHypos

    old_electrons = HypoConverter(
        InputHypos=electrons,
        InputClusters=old_ecalClusters,
        ExtraInputs=[old_ecalDigits],
        outputs={
            'OutputHypos': persisted_location('CaloElectrons'),
            'OutputTable': None,
        },
    ).OutputHypos
    old_ecalSplit = ClusterConverter(InputClusters=ecalSplitClusters)
    old_ecalSplitClusters = old_ecalSplit.OutputClusters
    old_ecalSplitDigits = old_ecalSplit.OutputDigits

    convert_mergedpi0 = MergedPi0Converter(
        InputClusters=old_ecalClusters,
        InputSplitClusters=old_ecalSplitClusters,
        ExtraInputs=[old_ecalDigits, old_ecalSplitDigits],
        InputHypos=mergedPi0s,
        outputs={
            'OutputHypos': persisted_location('CaloMergedPi0s'),
            'OutputSplitPhotons': persisted_location('CaloSplitPhotons'),
            'OutputTable': None,
        },
    )
    old_mergedPi0s = convert_mergedpi0.OutputHypos
    old_splitPhotons = convert_mergedpi0.OutputSplitPhotons

    # return relevant containers
    return {
        "digitsEcal": digitsEcal,
        "digitsHcal": digitsHcal,
        "ecalClusters": ecalClusters,
        "ecalSplitClusters": ecalSplitClusters,
        "v1_chargedpids": calopids_v1["chargedpids"],
        "v1_breminfos": calopids_v1["breminfos"],
        "v1_photons": old_photons,
        "v1_electrons": old_electrons,
        "v1_mergedPi0s": old_mergedPi0s,
        "v1_splitPhotons": old_splitPhotons,
        "v2_chargedpids": calopids_v3["chargedpids"],
        "v2_breminfos": calopids_v3["breminfos"],
        "v2_photons": photons,
        "v2_electrons": electrons,
        "v2_mergedPi0s": mergedPi0s,
        "v2_clustertrackmatches": tcmatches["combined"],
    }


def make_calo_reduced():
    digitsEcal = make_digits()["digitsEcal"]
    clusters = make_clusters(digitsEcal)

    return {
        "digitsEcal": digitsEcal,
        "ecalClusters": clusters,
    }


def make_calo_resolution_gamma(tracks, pvs):
    digitsEcal = make_digits()["digitsEcal"]

    clusters = make_clusters_various(digitsEcal)

    # track-cluster matching input
    tracks_incalo = make_acceptance(tracks)

    def getPhotonsElectrons(clusters, tracks_incalo=tracks_incalo, pvs=pvs):
        tcmatches = make_track_cluster_matching(clusters, tracks_incalo)
        pheloutput = make_photons_and_electrons(clusters,
                                                tcmatches["combined"], pvs)
        return pheloutput

    # default Graph Clustering
    PhElOutputGC = getPhotonsElectrons(clusters["ecalClustersGCWithCovar"])
    photonsGC = PhElOutputGC["photons"]
    electronsGC = PhElOutputGC["electrons"]

    # default Cellular Automaton
    PhElOutput = getPhotonsElectrons(clusters["ecalClustersOverlapWithCovar"])
    photons = PhElOutput["photons"]
    electrons = PhElOutput["electrons"]

    return {
        "clusters": clusters,
        "digitsEcal": digitsEcal,
        "photons": photons,
        "photonsGC": photonsGC,
        "electrons": electrons,
        "electronsGC": electronsGC,
    }


def make_calo_cluster_shapes(tracks, pvs):
    digitsEcal = make_digits()["digitsEcal"]
    clusters = make_clusters_shapes(digitsEcal)
    tracks_incalo = make_acceptance(tracks)

    photons_and_electrons = {}
    for shape, clus_shape in clusters.items():
        tcmatches = make_track_cluster_matching(
            clus_shape["ecalClustersOverlapWithCovar"], tracks_incalo)
        photons_and_electrons[shape] = make_photons_and_electrons(
            clus_shape["ecalClustersOverlapWithCovar"], tcmatches["combined"],
            pvs)

    return {
        "photons_and_electrons": photons_and_electrons,
        "clusters": clusters,
        "digitsEcal": digitsEcal
    }


def make_calo_resolution_pi0(pvs, make_raw=default_raw_banks):
    digitsEcal = make_digits()["digitsEcal"]
    clusters = make_clusters_various(digitsEcal)

    # default Cellular Automaton
    pi0 = make_merged_pi0(clusters["ecalClustersOverlapWithCovar"],
                          pvs)["mergedPi0s"]

    # default Graph Clustering
    pi0GC = make_merged_pi0(clusters["ecalClustersGCWithCovar"],
                            pvs)["mergedPi0s"]

    return {
        "clusters": clusters,
        "digitsEcal": digitsEcal,
        "clusDef-pi0Def": pi0,
        "clusDefGC-pi0Def": pi0GC,
    }
