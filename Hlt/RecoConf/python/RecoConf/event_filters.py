###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
from PyConf import configurable
from PyConf.application import default_raw_banks

from .decoders import default_ft_decoding_version

from PyConf.Algorithms import (PrGECFilter, VoidFilter)

from Functors import SIZE

log = logging.getLogger(__name__)


@configurable
def require_gec(make_raw=default_raw_banks, cut=-1, **kwargs):
    # Fall back to an FT-decoding-dependent value if none was explicitly given
    if cut == -1:

        if default_ft_decoding_version() > 6:
            log.warning(
                "GEC cut of 9750 applied. This cut is not yet verified for FT decoding version > 6!"
            )

        cut = 9750 if default_ft_decoding_version() >= 4 else 11000
    props = dict(NumberFTUTClusters=cut, **kwargs)  # kwargs take precedence
    # PrGECFilter needs a raw event with both FT and UT raw banks
    # fix this after
    return PrGECFilter(
        FTRawBanks=make_raw('FTCluster'), UTRawBanks=make_raw('UT'), **props)


def require_pvs(pvs):
    """Requires that at least 1 PV was reconstructed in this event.

       Args:
           pvs (DataHandle): PV maker.

       Returns:
           VoidFilter acting on ``pvs``
    """
    return VoidFilter(name='require_pvs', Cut=SIZE(pvs) > 0)
