###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Methods defining ProtoParticle makers, used as input to Particle selections.

The 'global' reconstruction is that which produces the final output of the
'full' HLT2 reconstruction: charged and neutral ProtoParticle containers.
"""
from .hlt1_tracking import make_reco_pvs, make_FTRawBankDecoder_clusters
from .hlt2_tracking import make_hlt2_tracks, make_hlt2_tracks_without_UT, convert_tracks_to_v3_from_v1
from .rich_reconstruction import make_all_rich_pids, default_rich_reco_options, make_merged_rich_pids
from .calorimeter_reconstruction import make_calo
from .muon_reconstruction import make_all_muon_pids, make_merged_muon_pids, make_conv_muon_pids

from .protoparticles import (
    make_charged_protoparticles as make_charged_protoparticles_from,
    make_neutral_protoparticles as make_neutral_protoparticles_from,
)

from PyConf import configurable
from PyConf.packing import persisted_location
from PyConf.Algorithms import (TrackContainerCopy, RecSummaryMaker,
                               TrackContainersMerger, TracksEmptyProducer,
                               PVsEmptyProducer, RecVertexEmptyProducer)


# TODO make things configurable, if needed
@configurable
def make_legacy_reconstruction():
    """Return reconstruction objects of the default reconstruction.
    """
    # Tracks
    hlt2_tracks = make_hlt2_tracks()
    best_tracks = hlt2_tracks["Best"]
    velo_tracks = hlt2_tracks["Velo"]

    rich_and_charged_proto_track_types = ["Long", "Downstream"]

    # PVs
    pvs = make_reco_pvs(velo_tracks, persisted_location('PVs'))

    # RICH
    richRecConfs = make_all_rich_pids(best_tracks, default_rich_reco_options())
    # Merge them for now to be compatible with Brunel.
    rich_pids = make_merged_rich_pids(richRecConfs)

    # Calo, enable v3 track versions
    tracks_v3, trackrels = convert_tracks_to_v3_from_v1(
        best_tracks["v1"],
        track_types=rich_and_charged_proto_track_types + ["Ttrack"])
    calo_pids = make_calo(tracks_v3, pvs["v3"], trackrels=trackrels)

    # Muons
    muonRecConfs = make_all_muon_pids(
        tracks=tracks_v3, track_types=rich_and_charged_proto_track_types)
    # Convert them to KeyedContainer for ProtoParticle storage
    muon_conv, muon_tracks = make_conv_muon_pids(
        muonRecConfs,
        best_tracks,
        track_types=rich_and_charged_proto_track_types)
    # Merge them for now to be compatible with Brunel.
    muon_pids = make_merged_muon_pids(muon_conv)
    muonpid_tracklists = [muon_tracks['Long'], muon_tracks['Downstream']]
    merge_muontracks = TrackContainersMerger(
        name="CreateMuonPIDTrackContainer_{hash}",
        InputLocations=muonpid_tracklists,
        outputs={
            'OutputLocation': persisted_location('MuonPIDTracks')
        },
    ).OutputLocation

    charged_protos = make_charged_protoparticles_from(
        tracks=best_tracks,
        rich_pids=rich_pids,
        calo_pids=calo_pids,
        muon_pids=muon_pids,
        track_types=rich_and_charged_proto_track_types,
        location=persisted_location('ChargedProtos'))

    neutral_protos = make_neutral_protoparticles_from(
        calo_pids=calo_pids, location=persisted_location('NeutralProtos'))

    return {
        "ChargedProtos": charged_protos,
        "NeutralProtos": neutral_protos['ProtoParticleLocation'],
        "IsPhoton": neutral_protos['IsPhoton'],
        "IsNotH": neutral_protos['IsNotH'],
        "AllTrackHandles": hlt2_tracks,
        "Tracks": best_tracks["v1"],
        "VeloTracks": velo_tracks["v1"],
        "MuonPIDTracks": merge_muontracks,
        "PVs": pvs["v3"],
        "PVs_v1": pvs["v1"],
        # The full data flow is functional, we only keep this for compatibility with reco from file.
        "UpfrontReconstruction": [],
        "CaloElectrons": calo_pids["v1_electrons"],
        "CaloPhotons": calo_pids["v1_photons"],
        "CaloMergedPi0s": calo_pids["v1_mergedPi0s"],
        "CaloSplitPhotons": calo_pids["v1_splitPhotons"],
        "MuonPIDs": muon_pids,
        "RichPIDs": rich_pids,
    }


@configurable
def make_light_reconstruction(usePatPVFuture=False,
                              use_pr_kf=True,
                              skipUT=False,
                              skipRich=False,
                              skipCalo=False,
                              skipMuon=False,
                              fastReco=False):
    """Return reconstruction objects of the fastest or the light reconstruction, with possibility to skip the UT

    Currently no charged protoparticles for upstream tracks are created.

    """
    # Tracks
    merger_list = []
    rich_and_charged_proto_track_types = []
    if skipUT:
        hlt2_tracks = make_hlt2_tracks_without_UT(
            light_reco=True, fast_reco=fastReco, use_pr_kf=use_pr_kf)
        merger_list = [
            hlt2_tracks["BestLong"]["v1"], hlt2_tracks["SeedDecloned"]["v1"]
        ]
        rich_and_charged_proto_track_types = ["Long"]
    else:
        hlt2_tracks = make_hlt2_tracks(
            light_reco=True, fast_reco=fastReco, use_pr_kf=use_pr_kf)
        merger_list = [
            hlt2_tracks["BestLong"]["v1"], hlt2_tracks["BestDownstream"]["v1"],
            hlt2_tracks["SeedDecloned"]["v1"]
        ]
        rich_and_charged_proto_track_types = ["Long", "Downstream"]

    velo_tracks = hlt2_tracks["Velo"]

    merge_tracks = TrackContainerCopy(
        name="CreateBestTrackContainer_{hash}",
        inputLocations=merger_list,
        outputs={'outputLocation': persisted_location('Tracks')})

    best_tracks = {"v1": merge_tracks.outputLocation}
    # PVs
    pvs = make_reco_pvs(velo_tracks, persisted_location('PVs'))

    # RICH
    if skipRich:
        rich_pids = None
    else:
        richRecConfs = make_all_rich_pids(
            best_tracks,
            default_rich_reco_options(),
            track_types=rich_and_charged_proto_track_types)
        # Merge them for now to be backwards compatible.
        rich_pids = make_merged_rich_pids(richRecConfs)

    # Calo, enable v3 track versions
    tracks_v3, trackrels = convert_tracks_to_v3_from_v1(
        best_tracks["v1"],
        track_types=rich_and_charged_proto_track_types + ["Ttrack"])

    # Muons
    if skipMuon:
        muon_pids = None
    else:
        muonRecConfs = make_all_muon_pids(
            tracks=tracks_v3, track_types=rich_and_charged_proto_track_types)
        muon_conv, muon_tracks = make_conv_muon_pids(
            muonRecConfs,
            best_tracks,
            track_types=rich_and_charged_proto_track_types,
            output_locations={'Long': persisted_location('MuonPIDTracks')}
            if skipUT else {})
        muon_pids = make_merged_muon_pids(muon_conv)
        if skipUT:
            merge_muontracks = muon_tracks['Long']
        else:
            muonpid_tracklists = [
                muon_tracks['Long'], muon_tracks['Downstream']
            ]
            merge_muontracks = TrackContainersMerger(
                name="CreateMuonPIDTrackContainer_{hash}",
                InputLocations=muonpid_tracklists,
                outputs={
                    'OutputLocation': persisted_location('MuonPIDTracks')
                }).OutputLocation

    if skipCalo:
        calo_pids = None
    else:
        calo_pids = make_calo(tracks_v3, pvs["v3"], trackrels=trackrels)

    # charged protoparticles
    charged_protos = make_charged_protoparticles_from(
        tracks=best_tracks,
        rich_pids=rich_pids,
        calo_pids=calo_pids,
        muon_pids=muon_pids,
        track_types=rich_and_charged_proto_track_types,
        location=persisted_location('ChargedProtos'))

    # neutral protoparticles
    neutral_protos = make_neutral_protoparticles_from(
        calo_pids=calo_pids, location=persisted_location('NeutralProtos'))

    ####
    # Define the output
    ####
    output = {
        "AllTrackHandles": hlt2_tracks,
        "Tracks": best_tracks["v1"],
        "VeloTracks": velo_tracks["v1"],
        "PVs": pvs["v3"],
        "PVs_v1": pvs["v1"],
        "ChargedProtos": charged_protos,
        "NeutralProtos": neutral_protos['ProtoParticleLocation'],
        "IsPhoton": neutral_protos['IsPhoton'],
        "IsNotH": neutral_protos['IsNotH'],
        "UpfrontReconstruction": []
    }

    if skipUT:
        extra_tracks_dict = {
            "BestLong": hlt2_tracks["BestLong"],
            "BestSeed": hlt2_tracks["BestSeed"]
        }

        output.update(extra_tracks_dict)

    if not skipCalo:
        caloDict = {
            "CaloElectrons": calo_pids["v1_electrons"],
            "CaloPhotons": calo_pids["v1_photons"],
            "CaloMergedPi0s": calo_pids["v1_mergedPi0s"],
            "CaloSplitPhotons": calo_pids["v1_splitPhotons"]
        }

        output.update(caloDict)

    if not skipRich:
        richDict = {"RichPIDs": rich_pids}
        output.update(richDict)

    if not skipMuon:
        muonDict = {"MuonPIDTracks": merge_muontracks, "MuonPIDs": muon_pids}
        output.update(muonDict)

    return output


@configurable
def make_calo_only_reconstruction():
    """Return reconstruction objects of the calo only reconstruction

    """
    # make fake container of tracks and PVs
    tracks = TracksEmptyProducer()
    pvs = PVsEmptyProducer()
    pvs_v1 = RecVertexEmptyProducer()

    # Add Calo
    tracks_for_calo, trackrels = convert_tracks_to_v3_from_v1(
        tracks, track_types=['Long'])
    calo = make_calo(tracks_for_calo, pvs, trackrels=trackrels)

    neutral_protos = make_neutral_protoparticles_from(calo_pids=calo)

    return {
        "NeutralProtos": neutral_protos['ProtoParticleLocation'],
        "Tracks": tracks,
        "PVs": pvs,
        "PVs_v1": pvs_v1,
        # The full data flow is functional, we only keep this for compatibility with reco from file.
        "UpfrontReconstruction": [],
        "CaloElectrons": calo["v1_electrons"],
        "CaloPhotons": calo["v1_photons"],
        "CaloMergedPi0s": calo["v1_mergedPi0s"],
        "CaloSplitPhotons": calo["v1_splitPhotons"],
    }


@configurable
def make_light_reco_pr_kf_without_UT(skipRich=False,
                                     skipCalo=False,
                                     skipMuon=False):
    """Shortcut for running the light reconstruction, with PrKalmanFilter, without UT
    """
    return make_light_reconstruction(
        skipUT=True,
        skipRich=skipRich,
        skipCalo=skipCalo,
        skipMuon=skipMuon,
        fastReco=False)


@configurable
def make_light_reco_pr_kf(skipRich=False, skipCalo=False, skipMuon=False):
    """Shortcut for running the light reconstruction, with PrKalmanFilter, with UT
    """
    return make_light_reconstruction(
        skipRich=skipRich,
        skipCalo=skipCalo,
        skipMuon=skipMuon,
        fastReco=False)


@configurable
def make_fastest_reconstruction(skipUT=False):
    """Shortcut for running the fastest reconstruction
    """
    return make_light_reconstruction(skipUT=skipUT, fastReco=True)


@configurable
def make_rec_summary(reco_output):
    summary = RecSummaryMaker(
        Tracks=reco_output["Tracks"],
        PVs=reco_output["PVs_v1"],
        SciFiClusters=make_FTRawBankDecoder_clusters(),
        outputs={"Output": persisted_location('RecSummary')})
    return summary.Output


@configurable
def reconstruction(make_reconstruction=make_light_reco_pr_kf_without_UT):
    """Hook to allow switching between reconstructions.

    make_reconstruction (function): which reconstruction to run.

    Note it is advised to use this function if more than one object is needed,
    rather than the accessors below  as it makes the configuration slower.
    """
    reco_output = make_reconstruction()
    # Create the summary of the reconstruction
    reco_output["RecSummary"] = make_rec_summary(reco_output)
    return reco_output


def make_charged_protoparticles():
    """Return a DataHandle to the container of charged ProtoParticles.

    The charged ProtoParticle making is not functional, and so the sequence of algorithms
    which produces the full information at this location needs to be scheduled explicitly
    by including the `upfront_reconstruction` method in the file in the control flow.
    """
    return reconstruction()["ChargedProtos"]


def upfront_reconstruction():
    """Return sequence to create charged ProtoParticles.

    Be aware that the node needs to be configures with the mode `force_order=True`.

    The charged ProtoParticle making is not functional, and so the sequence of algorithms
    which produces the full information at this location needs to be scheduled explicitly
    by including the `upfront_reconstruction` method in the file in the control flow.
    """

    return reconstruction()["UpfrontReconstruction"]


def make_neutral_protoparticles():
    """Return a DataHandle to the container of neutral ProtoParticles.

    The neutral ProtoParticle making is functional, there is no need to
    schedule the upfront reconstruction explicitly.
    """

    return reconstruction()["NeutralProtos"]


def make_pvs():
    """Return a DataHandle to the container of PVs
    """

    return reconstruction()["PVs"]


def make_tracks():
    """Return a DataHandle to the container of all tracks
    """
    return reconstruction()["Tracks"]


@configurable
def make_upstream_charged_protoparticles(make_reconstruction=reconstruction):
    hlt2_default_reco = make_reconstruction()
    hlt2_tracks = hlt2_default_reco["AllTrackHandles"]

    if "BestUpstream" in hlt2_tracks.keys():
        upstream_tracks = hlt2_tracks["BestUpstream"]
    else:
        upstream_tracks = {
            "v1": TracksEmptyProducer(name="FakeUpstreamTracks")
        }

    pvs = hlt2_default_reco["PVs"]

    upstream_track_types = ["Upstream"]

    # RICH
    richRecConfs = make_all_rich_pids(
        upstream_tracks,
        default_rich_reco_options(),
        track_types=upstream_track_types)
    rich_pids = richRecConfs["Upstream"]["RichPIDs"]

    # Calo, enable v3 track versions
    tracks_v3, trackrels = convert_tracks_to_v3_from_v1(
        upstream_tracks["v1"], track_types=upstream_track_types + ["Ttrack"])

    # calo_pids = make_calo(tracks_v3, pvs["v3"], trackrels=trackrels)
    calo_pids = make_calo(tracks_v3, pvs, trackrels=trackrels)

    # charged protoparticles
    upstream_protos = make_charged_protoparticles_from(
        tracks=upstream_tracks,
        rich_pids=rich_pids,
        calo_pids=calo_pids,
        muon_pids=None,
        track_types=upstream_track_types)

    return upstream_protos
