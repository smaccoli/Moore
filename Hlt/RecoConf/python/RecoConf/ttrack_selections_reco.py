###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define the necessary reconstruction chain to go from Ttracks to particles
"""
from RecoConf.protoparticles import make_charged_protoparticles
from RecoConf.rich_reconstruction import make_all_rich_pids, default_rich_reco_options, make_merged_rich_pids
from RecoConf.calorimeter_reconstruction import make_calo
from RecoConf.hlt2_global_reco import convert_tracks_to_v3_from_v1
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_tracking import kill_clones, make_TrackBestTrackCreator_tracks

from PyConf.Algorithms import (TrackListRefiner, TrackSelectionToContainer)

from PyConf.Tools import (ParticleVertexFitter, TrackRungeKuttaExtrapolator,
                          TrackStateProvider, TrackInterpolator)
from PyConf import configurable
from PyConf.packing import persisted_location
import Functors as F


### Define particle reconstruction functions
# Define a basic track filter
def make_filtered_tracks(inputTracks, code):
    """
    Basic track filter for v1 tracks

    Filters input tracks using the provided fuctors code.
    This is done using TrackListRefiner and TrackSelectionToContainer

    Args: inputTracks: the tracks to filter
          code:        the cuts you want to use to filter
    """
    tracks_selection = TrackListRefiner(
        name="TtrackSel_TrackListRefiner",
        inputLocation=inputTracks,
        Code=code).outputLocation
    filtered_tracks = TrackSelectionToContainer(
        name="TtrackSel_TrackSelectionToContainer",
        InputLocation=tracks_selection).OutputLocation
    return {"v1": filtered_tracks}


# Define a good SciFi tracks maker
@configurable
def make_good_ttracks(global_reco=reconstruction,
                      filter_code=F.require_all(F.TRACKISTTRACK, F.P > 2000,
                                                F.P < 500000),
                      refit_tracks=True):
    if "BestLong" in global_reco() and "BestSeed" in global_reco():
        # this is for the default reconstruction in Hlt2. Sprucing not currently supported
        best_long = global_reco()["BestLong"]["v1"]
        fitted_seed = global_reco()["BestSeed"]["v1"]

        best_seed = kill_clones(
            inputTracks=fitted_seed, referenceTracks=[best_long])

        filtered_and_reduced_tracks = make_filtered_tracks(
            best_seed, filter_code)
        return filtered_and_reduced_tracks

    else:
        # this is for compatibility with reco from file and tests
        # get all the tracks and filter. In the case there are no T-tracks in the container, there will be no output.
        # one may also need to bind global_reco to hlt2_global_reco.reconstruction for test compatibility.
        all_tracks = global_reco(
        )["Tracks"]  # TODO: can a check be added for fitted tracks? Check that there are Ttracks?
        filtered_and_reduced_tracks = make_filtered_tracks(
            all_tracks, filter_code)

        if not refit_tracks:
            return filtered_and_reduced_tracks

        # Run TBCT to fit and add necessary states
        fitted_filtered_track_containers = make_TrackBestTrackCreator_tracks(
            tracks={"Seed": filtered_and_reduced_tracks},
            track_version="v1",
            get_tracklist=lambda skip_UT: ["Seed"])

        # Make the track dictionary
        fitted_filtered_tracks = dict()
        for trType in fitted_filtered_track_containers.keys():
            fitted_filtered_tracks[trType] = {
                "v1": fitted_filtered_track_containers[trType]
            }

        return fitted_filtered_tracks["Best"]


# Calo PIDs from T tracks
def make_ttrack_calo_pids(ttracks, pvs):
    tracks_v3, trackrels = convert_tracks_to_v3_from_v1(
        ttracks["v1"], track_types=["Ttrack"])
    calo_pids = make_calo(
        tracks_v3,
        pvs,
        trackrels=trackrels,
        chargedpid_types={
            "calo": ["Ttrack"],
            "brem": [None]
        })
    return calo_pids


# RICH PIDs from T tracks
def make_ttrack_rich_pids(ttracks):
    richRecConfs = make_all_rich_pids(
        ttracks, default_rich_reco_options(), track_types=["Seed"])
    rich_pids = make_merged_rich_pids(richRecConfs)
    return rich_pids


# All the T track reco
@configurable
def make_ttrack_reco(global_reco, make_ttracks):
    good_ttracks = make_ttracks()
    pvs = global_reco["PVs"]

    rich_pids = make_ttrack_rich_pids(good_ttracks)
    calo_pids = make_ttrack_calo_pids(good_ttracks, pvs)
    # TODO: add muon PIDs when compatible with T tracks

    return {
        "good_ttracks": good_ttracks,
        "rich_pids": rich_pids,
        "calo_pids": calo_pids,
        # TODO: add muon PIDs when compatible with T tracks
    }


# define protoparticle maker for T tracks that uses the above reconstruction
@configurable
def make_ttrack_protoparticles(name="TTrackSel_ProtoParticleMaker",
                               make_ttrack_reco=make_ttrack_reco,
                               make_ttracks=make_good_ttracks,
                               make_global_reco=reconstruction):
    """ ttrack protoparticle maker
    This is used to make ttrack protoparticles
    """

    global_reco = make_global_reco()
    with persisted_location.bind(force=False):
        ttrack_reco = make_ttrack_reco(global_reco, make_ttracks)

    return make_charged_protoparticles(
        tracks=ttrack_reco["good_ttracks"],
        rich_pids=ttrack_reco["rich_pids"],
        calo_pids=ttrack_reco["calo_pids"],
        muon_pids=None,  # TODO: add muon PIDs when compatible with T tracks
        track_types=["Ttrack"])


def TrackStateProvider_with_TrackRungeKuttaExtrapolator():
    RK_extrapolator = TrackRungeKuttaExtrapolator(
        name="ttracks_RKextrapolator")
    RK_interpolator = TrackInterpolator(
        name="ttracks_RKinterpolator", Extrapolator=RK_extrapolator)
    return TrackStateProvider(
        name="ttracks_TrackStateProvider",
        Extrapolator=RK_extrapolator,
        Interpolator=RK_interpolator,
        public=True)


def PVF_with_single_extrapolation():
    return ParticleVertexFitter(
        name="RKParticleVertexFitter",
        StateProvider=TrackStateProvider_with_TrackRungeKuttaExtrapolator(),
        extrapolateTracksWithoutVelo=True,
    )
