###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
from PyConf import configurable
from PyConf.application import default_raw_event, default_raw_banks
from PyConf.utilities import DISABLE_TOOL
from PyConf.packing import persisted_location

from PyConf.Algorithms import (
    fromPrUpstreamTracksV1Tracks,
    fromPrForwardTracksFromVeloUTV1Tracks,
    fromV3TrackV1Track,
    fromPrVeloTracksV1TracksMerger,
    FTRawBankDecoder,
    VPClus,
    VPClusFull,
    VPRetinaSPmixer,
    VPRetinaClusterCreator,
    VPRetinaClusterDecoder,
    VeloClusterTrackingSIMD,
    TrackBeamLineVertexFinderSoA,
    PrVeloUT,
    SciFiTrackForwardingStoreHit,
    SciFiTrackForwarding,
    PrStoreFTHit,
    PrStoreSciFiHits,
    VeloKalman,
    TrackEventFitter,
    MakePVRelations__PrFittedForwardTracks,
    MakeZip__PrFittedForwardTracks__BestVertexRelations,
    PrStoreUTHit,
    PrStorePrUTHits,
    PatPV3DFuture,
    PrStorePrUTHitsEmptyProducer,
    PrStoreUTHitEmptyProducer,
    LHCb__UnpackRawEvent,
)

from PyConf.Tools import (
    TrackMasterExtrapolator,
    SimplifiedMaterialLocator,
    TrackMasterFitter,
    MeasurementProvider,
    VPMeasurementProvider,
    UTMeasurementProvider,
    FTMeasurementProvider,
)

from Functors import ALL

log = logging.getLogger(__name__)

from RecoConf.core_algorithms import make_unique_id_generator


def _rawevent_to_rawbank(dh, tp):
    if dh.type == "LHCb::RawEvent":
        print("FIXME: fix input to return RawBanks instead of RawEvent")

        def output_transform(Output):
            return {"RawBankLocations": [Output]}

        dh = LHCb__UnpackRawEvent(
            RawEventLocation=dh,
            output_transform=output_transform,
            BankTypes=[tp]).Output
    return dh


@configurable
def default_ft_decoding_version(value=4):
    raise DeprecationWarning(
        "Function default_ft_decoding_version has to be imported from the RecoConf.decoders module."
    )


@configurable
def require_gec(make_raw=default_raw_banks, cut=-1, **kwargs):
    raise DeprecationWarning(
        "Function require_gec has to be imported from the RecoConf.event_filters module."
    )


@configurable
def make_VeloClusterTrackingSIMD(algorithm=VeloClusterTrackingSIMD,
                                 make_raw=default_raw_banks,
                                 masked_sensors=[],
                                 MaxScatterSeeding=0.1,
                                 MaxScatterForwarding=0.1,
                                 MaxScatter3hits=0.02,
                                 SkipForward=1):
    """Simple helper to make sure both, make_VeloClusterTrackingSIMD_tracks and make_VeloClusterTrackingSIMD_hits,
    access the identically configured version of VeloClusterTrackingSIMD

    Args:
    make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`;
    algorithm: The Velo tracking algorithm to run;
    masked_sensors: List of unique sensor IDs that will *not* be considered in the pattern reco's *tracks*, but are included in the *clusters* returned.

    Returns:
    The Velo tracking algorithm.

    """
    if "Retina" in str(algorithm):
        # For VeloRetinaClusterTrackingSIMD use VPRetinaClusters rawbank in the file
        bank_type = "VPRetinaCluster"
    else:
        # In case raw_event is made from VP or algorithm is VeloClusterTrackingSIMD, use VP rawbank in the event
        bank_type = "VP"
    my_SensorMasks = [j in masked_sensors for j in range(208)
                      ]  # 208 = LHCb::Pr::Velo::VPInfos::NSensors
    return algorithm(
        RawBanks=_rawevent_to_rawbank(make_raw(bank_type), bank_type),
        SensorMasks=tuple(my_SensorMasks),
        MaxScatterSeeding=MaxScatterSeeding,
        MaxScatterForwarding=MaxScatterForwarding,
        MaxScatter3hits=MaxScatter3hits,
        SkipForward=SkipForward)


@configurable
def make_VeloClusterTrackingSIMD_hits(
        make_tracks=make_VeloClusterTrackingSIMD):
    """Makes velo hits with VeloClusterTrackingSIMD

       Args:
           make_tracks (DatHandle): The function that returns the Velo tracking algorithm.

       Returns:
           DataHandle: VeloClusterTrackingSIMD's HitsLocation.
    """
    return make_tracks().HitsLocation


def make_VPClus_location_and_offsets(make_raw=default_raw_event):
    """Makes velo clusters with VPClus

       Args:
           make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
           A dict mapping VPClus' ClusterLocation and ClusterOffsets DataHandles to ``'Location'`` and ``'Offsets'`` respectively.
    """
    clustering = VPClus(RawEventLocation=make_raw(["VP"]))
    return {
        "Location": clustering.ClusterLocation,
        "Offsets": clustering.ClusterOffsets
    }


def make_VPClus_hits():
    return make_VPClus_location_and_offsets()["Location"]


@configurable
def make_velo_full_clusters(make_raw=default_raw_event,
                            make_full_cluster=VPClusFull,
                            detector=["VP"]):
    return make_full_cluster(
        RawEventLocation=make_raw(detector)).ClusterLocation


@configurable
def make_SPmixed_raw_banks(detector=None, make_raw=default_raw_banks):
    """Mix SP.

       Args:
           make_raw (DataHandle): RawEventLocation for VPRetinaSPmixer, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
    """
    # Can detector ever be something else
    assert not detector or detector == "VP" or detector == ['VP'], detector
    return VPRetinaSPmixer(RawBanks=make_raw("VP")).RawEventLocationMixed


@configurable
def make_RetinaCluster_raw_event(detector=None, make_raw=default_raw_banks):
    """Makes velo clusters with VPRetinaClusterCreator.

       Args:
           make_raw (DataHandle): RawEventLocation for VPRetinaClusterCreator, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
    """
    assert not detector or detector == [
        "VPRetinaCluster"
    ] or detector == "VPRetinaCluster", detector
    clustering = VPRetinaClusterCreator(
        RawBanks=_rawevent_to_rawbank(make_raw("VP"), "VP"))
    return clustering.RetinaRawBanks  # this is a RawBank::View


@configurable
def make_RetinaCluster_raw_bank(detector=None, make_raw=default_raw_banks):
    """Makes velo clusters with VPRetinaClusterCreator.

       Args:
           make_raw (DataHandle): RawEventLocation for VPRetinaClusterCreator, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
    """
    assert not detector or detector == [
        "VPRetinaCluster"
    ] or detector == "VPRetinaCluster", detector
    clustering = VPRetinaClusterCreator(
        RawBanks=_rawevent_to_rawbank(make_raw("VP"), "VP"))
    return clustering.RetinaRawBanks  # this is a RawBank::View


@configurable
def make_RetinaClusters(detector=None, make_raw=default_raw_banks):
    """Decodes RetinaClusters with VPRetinaClusterDecoder.

       Args:
           make_raw (DataHandle): RawBankLocation for VPRetinaClusterCreator, defaults to `default_raw_banks <PyConf.application.default_raw_banks>`.

       Returns:
           vector<LHCb::VPLightCluster>
    """
    assert not detector or detector == 'VPRetinaCluster', detector
    decoding = VPRetinaClusterDecoder(RawBanks=make_raw('VPRetinaCluster'))
    return decoding.ClusterLocation


@configurable
def make_VeloClusterTrackingSIMD_tracks(
        make_tracks=make_VeloClusterTrackingSIMD):
    """Makes velo tracks with VeloClusterTrackingSIMD.

       Args:
       make_tracks (DataHandle): The function that returns the Velo tracking algorithm.

       Returns:
           A dict mapping forward- and backward-going velo tracks to ``'Pr'`` and ``'Pr::backward'`` respectively.
    """

    tracking = make_tracks()
    return {
        "Pr": tracking.TracksLocation,
        "Pr::backward": tracking.TracksBackwardLocation,
    }


@configurable
def filter_velo_tracks(make_velo_tracks=make_VeloClusterTrackingSIMD_tracks,
                       fwdTracksFunctor=ALL,
                       bwdTracksFunctor=ALL):
    """Helper function to filter the Velo tracks

       Args:
           make_velo_tracks (DataHandle): velo tracking algorithm, defaults to `make_VeloClusterTrackingSIMD_tracks`.
           fwdTracksFunctor: Functor to filter the forward Velo tracks
           bwdTracksFunctor: Functor to filter the backward Velo tracks

       Returns:
           A dict mapping forward- and backward-going tracks to ``'Pr'`` and ``'Pr::backward'``, respectively.
    """
    from Hlt1Conf.algorithms import Filter

    velo_tracks = make_velo_tracks()

    fiddlyDict = {
        "PrVeloTracks": velo_tracks['Pr'],
        "PrVeloBackwardTracks": velo_tracks['Pr::backward'],
    }

    forward_going_tracks = Filter(fiddlyDict, fwdTracksFunctor)['PrVeloTracks']
    backward_going_tracks = Filter(fiddlyDict,
                                   bwdTracksFunctor)['PrVeloBackwardTracks']

    return {
        "Pr": forward_going_tracks,
        "Pr::backward": backward_going_tracks,
    }


@configurable
def all_velo_track_types(make_velo_tracks=make_VeloClusterTrackingSIMD_tracks):
    """Helper function to get all types of velo tracks.

       Args:
           make_velo_tracks (DataHandle): velo tracking algorithm, defaults to `make_VeloClusterTrackingSIMD_tracks`.

       Returns:
           A dict mapping forward-, backward-going, v1 and v2 velo tracks to ``'Pr'``, ``'Pr::backward'``, ``'v1'`` and ``'v2'`` respectively.
    """
    velo_tracks = make_velo_tracks()
    velo_tracks_v1 = fromPrVeloTracksV1TracksMerger(
        InputTracksLocation1=velo_tracks["Pr"],
        InputTracksLocation2=velo_tracks["Pr::backward"]).OutputTracksLocation

    return {
        "Pr": velo_tracks["Pr"],
        "Pr::backward": velo_tracks["Pr::backward"],
        "v1": velo_tracks_v1
    }


@configurable
def make_TrackBeamLineVertexFinderSoA_pvs(velo_tracks, location, minz=-300):
    """Makes primary vertices from velo tracks using TrackBeamLineVertexFinderSoA.

       Args:
           velo_tracks (dict): velo tracks, needs ``'Pr'`` and ``'Pr::backward'`` tracks, e.g. from `all_velo_track_types`.

       Returns:
           DataHandle: TrackBeamLineVertexFinderSoA's OutputVertices.
    """
    pvs = {
        "v3":
        TrackBeamLineVertexFinderSoA(
            TracksLocation=velo_tracks["Pr"],
            TracksBackwardLocation=velo_tracks["Pr::backward"],
            MinZ=minz,
            outputs={
                'OutputVertices': location if "v1" not in velo_tracks else None
            },
        ).OutputVertices
    }

    if "v1" in velo_tracks:
        from PyConf.Algorithms import PVToRecConverterV1
        pvs["v1"] = PVToRecConverterV1(
            InputVertices=pvs["v3"],
            InputTracks=velo_tracks["v1"],
            outputs={
                'OutputVertices': location
            },
        ).OutputVertices
    return pvs


@configurable
def make_PatPV3DFuture_pvs(velo_tracks, location, use_beam_spot_cut=True):
    """Makes primary vertices from velo tracks using PatPV3DFuture.

       Args:
           velo_tracks (dict): velo tracks, needs ``'v2'`` tracks, e.g. from `all_velo_track_types`.

       Returns:
           DataHandle: PatPV3DFuture's OutputVerticesName

       Note:
           PatPV3DFuture's defaults have been overridden in this maker with ``BeamSpotRCut=0.6, UseBeamSpotRCut=True, minClusterMult=4``.
    """
    pvs = {
        "v1":
        PatPV3DFuture(
            InputTracks=velo_tracks["v1"],
            BeamSpotRCut=0.6,
            UseBeamSpotRCut=use_beam_spot_cut,
            minClusterMult=4,
            outputs={
                'OutputVerticesName': location
            },
        ).OutputVerticesName
    }
    from PyConf.Algorithms import RecV1ToPVConverter
    pvs["v3"] = RecV1ToPVConverter(
        InputVertices=pvs["v1"],
        outputs={
            'OutputVertices': location if "v1" not in velo_tracks else None
        },
    ).OutputVertices
    return pvs


@configurable
def make_reco_pvs(
        velo_tracks,
        location,
        make_pvs_from_velo_tracks=make_TrackBeamLineVertexFinderSoA_pvs):
    """Makes PVs from velo tracks given a PV algorithm.

       Args:
           velo_tracks (dict): velo tracks, e.g. from `all_velo_track_types`.
           make_pvs_from_velo_tracks (DataHandle): PV maker consuming ``velo_tracks``, defaults to `make_TrackBeamLineVertexFinderSoA_pvs`.

       Returns:
           DataHandle: Output of ``make_pvs_from_velo_tracks``
    """
    return make_pvs_from_velo_tracks(velo_tracks, location)


def make_all_pvs():
    """Makes PVs from HLT1 inputs, i.e. from `all_velo_track_types` using the PV maker passed to `make_reco_pvs`

       Returns:
          dictionary of v1 and v3 PVs
    """
    return make_reco_pvs(all_velo_track_types(), persisted_location('PVs'))


def make_pvs():
    """Makes PVs from HLT1 inputs, i.e. from `all_velo_track_types` using the PV maker passed to `make_reco_pvs`

       Returns:
           DataHandle: primary vertices
    """
    return make_all_pvs()["v3"]


def require_pvs(pvs):
    raise DeprecationWarning(
        "Function require_pvs has to be imported from the RecoConf.event_filters module."
    )


@configurable
def make_PrStoreUTHit_hits(make_raw=default_raw_banks,
                           isCluster=True,
                           isAdcMax=False):
    """Decodes UT hits from raw data.

       Args:
            make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
            DataHandle: PrStoreUTHit's UTHitsLocation.

    """
    return PrStoreUTHit(
        RawBanks=make_raw("UT"), isCluster=isCluster,
        isAdcMax=isAdcMax).UTHitsLocation


@configurable
def make_PrStoreUTHit_empty_hits():
    """Creates an empty container of UT hits, used for the no UT scenario.

    Returns:
            DataHandle: PrStoreUTHitEmptyProducer' Output.

    """
    return PrStoreUTHitEmptyProducer().Output


@configurable
def make_PrStorePrUTHits_hits(make_raw=default_raw_banks,
                              isCluster=True,
                              isAdcMax=False):
    """Decodes UT hits from raw data, fills an SOA container.

       Args:
            make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
            DataHandle: PrStorePrUTHits' UTHitsLocation.

    """
    return PrStorePrUTHits(
        RawBanks=make_raw("UT"), isCluster=isCluster,
        isAdcMax=isAdcMax).UTHitsLocation


@configurable
def make_PrStorePrUTHits_empty_hits():
    """Creates an empty container of UT hits, used for the no UT scenario.

    Returns:
            DataHandle: PrStorePrUTHitsEmptyProducer' Output.

    """
    return PrStorePrUTHitsEmptyProducer().Output


@configurable
def make_PrVeloUT_tracks(velo_tracks, make_ut_hits=make_PrStorePrUTHits_hits):
    """Makes upstream tracks from velo tracks and UT hits.

       Args:
           velo_tracks (dict): velo tracks, needs ``'Pr'`` tracks, e.g. from `all_velo_track_types`.
           make_ut_hits (DataHandle): UT hit maker, defaults to `make_PrStoreUTHit_hits`.

       Returns:
           DataHandle: PrVeloUT's OutputTracksName.
    """
    return PrVeloUT(
        InputTracksName=velo_tracks["Pr"],
        UTHits=make_ut_hits()).OutputTracksName


@configurable
def all_upstream_track_types(velo_tracks,
                             make_velo_ut_tracks=make_PrVeloUT_tracks):
    """Helper function to get all types of upstream tracks.

       Args:
           velo_tracks (dict): velo tracks, needs ``'v2'`` tracks, e.g. from `all_velo_track_types`.
           make_velo_ut_tracks (DataHandle): upstream track maker, defaults to `make_PrVeloUT_tracks`.

       Returns:
           A dict mapping Pr, v1 and v2 upstream tracks to ``'Pr'``, ``'v1'`` and ``'v2'`` respectively.
    """
    upstream_tracks_pr = make_velo_ut_tracks(velo_tracks=velo_tracks)

    upstream_tracks_v1 = fromPrUpstreamTracksV1Tracks(
        InputTracksLocation=upstream_tracks_pr,
        VeloTracksLocation=velo_tracks["v1"]).OutputTracksLocation

    return {"Pr": upstream_tracks_pr, "v1": upstream_tracks_v1}


def make_FTRawBankDecoder_clusters(make_raw=default_raw_banks):
    """Decodes the FT raw bank into FTLiteClusters. DecodingVersion set by `default_ft_decoding_version`.

       Args:
           make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
           DataHandle: FTRawBankDecoder's OutputLocation.
    """
    return FTRawBankDecoder(
        name="FTRawBankDecoder", RawBanks=make_raw("FTCluster")).OutputLocation


@configurable
def make_SciFiTrackForwardingStoreHit_hits(
        make_ft_clusters=make_FTRawBankDecoder_clusters):
    """Transforms FTLiteClusters into the input format needed by the SciFiTrackForwarding.

       Args:
            make_ft_clusters (DataHandle): maker of FT clusters, defaults to `make_FTRawBankDecoder_clusters`.

       Returns:
           DataHandle: SciFiTrackForwardingStoreHit's Output.
    """
    return SciFiTrackForwardingStoreHit(HitsLocation=make_ft_clusters()).Output


@configurable
def make_SciFiTrackForwarding_tracks(
        input_tracks, make_ft_hits=make_SciFiTrackForwardingStoreHit_hits):
    """Makes forward tracks with SciFiTrackForwarding (long tracks from upstream seeds).

       Args:
           input_tracks (dict): upstream tracks, needs ``'Pr'`` tracks, e.g. from  `all_upstream_track_types`.
           make_ft_hits (DataHandle): maker of FT hits, defaults to `make_SciFiTrackForwardingStoreHit_hits`.

       Returns:
           DataHandle: SciFiTrackForwarding's Output.
    """
    return SciFiTrackForwarding(
        HitsLocation=make_ft_hits(), InputTracks=input_tracks["Pr"]).Output


@configurable
def make_PrStoreFTHit_hits(make_ft_clusters=make_FTRawBankDecoder_clusters):
    """Makes FT hits taking the detector geometry into account.

       Args:
           make_ft_clusters (DataHandle): maker of FT clusters, defaults to `make_FTRawBankDecoder_clusters`.

       Returns:
           DataHandle: PrStoreFTHit's FTHitsLocation.
    """
    return PrStoreFTHit(InputLocation=make_ft_clusters()).FTHitsLocation


@configurable
def make_PrStoreSciFiHits_hits(make_ft_clusters=make_FTRawBankDecoder_clusters,
                               disabled_layers=[]):
    """Makes SciFi hits from FTLiteClusters taking the detector geometry into accout. This is the SoA
       replacement for `make_PrStoreFTHit_hits`.

       Args:
           make_ft_clusters (DataHandle): maker of FT clusters, defaults to `make_FTRawBankDecoder_clusters`.

       Returns:
              DataHandle: PrStoreSciFiHits's Output.
    """

    my_disabled_layers = [j in disabled_layers
                          for j in range(12)]  # 12 = n Layers in SciFi

    return PrStoreSciFiHits(
        HitsLocation=make_ft_clusters(),
        LayerMasks=tuple(my_disabled_layers)).Output


@configurable
def all_hlt1_forward_track_types(
        input_tracks, make_forward_tracks=make_SciFiTrackForwarding_tracks):
    """Helper function to get all types of HLT1 forward tracks.

       Args:
           input_tracks (dict): upstream tracks, needs ``'v2'`` tracks, e.g. from `all_upstream_track_types`.
           make_forward_tracks (DataHandle): maker of forward tracks, defaults to `make_SciFiTrackForwarding_tracks`.

       Returns:
           A dict mapping Pr, v1, v2 and v2Zip HLT1 forward tracks to ``'Pr'``, ``'v1'``, ``'v2'`` and ``'v2Zip'`` respectively.
    """
    forward_tracks_pr = make_forward_tracks(input_tracks=input_tracks)

    forward_tracks_v1 = fromPrForwardTracksFromVeloUTV1Tracks(
        InputTracksLocation=forward_tracks_pr,
        UpstreamTracksLocation=input_tracks["v1"]).OutputTracksLocation

    return {
        "Pr": forward_tracks_pr,
        "v1": forward_tracks_v1,
    }


def make_hlt1_tracks():
    """Function to get all types of tracks reconstructed in HLT1

       Returns:
           A dict mapping all types of velo, upstream and HLT1 forward tracks to ``'Velo'``, ``'Upstream'`` and ``'Forward'`` respectively.
    """
    velo_tracks = all_velo_track_types()
    velo_ut_tracks = all_upstream_track_types(velo_tracks)
    forward_tracks = all_hlt1_forward_track_types(velo_ut_tracks)
    return {
        "Velo": velo_tracks,
        "Upstream": velo_ut_tracks,
        "Forward": forward_tracks,
    }


@configurable
def make_VeloKalman_fitted_tracks(
        tracks,
        make_hits=make_VeloClusterTrackingSIMD_hits,
        make_unique_id_generator=make_unique_id_generator):
    """Fits tracks with VeloKalman.

       Args:
           tracks (dict of dicts): input tracks to VeloKalman. It will consume ``["Velo"]["Pr"]`` and ``["Forward"]["Pr"]`` tracks.
           make_hits (DataHandle): maker of velo hits, defaults to `make_VeloClusterTrackingSIMD_hits`.
           make_unique_id_generator (DataHandle): maker of the ID generator object, defaults to `make_unique_id_generator`.
       Returns:
           A dict mapping Pr, v1, v2, v1Sel, v2Sel, v2Zip and v2ZipSel fitted forward tracks to ``'Pr'``, ``'v1'``, ``'v2'``, ``'v1Sel'``, ``'v2Sel'``, ``'v2Zip'`` and ``'v2ZipSel'`` respectively.
    """
    fitted_tracks = VeloKalman(
        HitsLocation=make_hits(),
        TracksVPLocation=tracks["Velo"]["Pr"],
        TracksFTLocation=tracks["Forward"]["Pr"],
        UniqueIDGenerator=make_unique_id_generator()).OutputTracksLocation

    fitted_tracks_v1 = fromV3TrackV1Track(
        InputTracks=fitted_tracks).OutputTracks

    return {
        "Pr": fitted_tracks,
        "v1": fitted_tracks_v1,
    }


@configurable
def make_fitted_forward_tracks_with_pv_relations(tracks, make_pvs=make_pvs):
    """Zips fitted forward tracks with their best PV.

       Args:
           tracks (dict): fitted forward tracks as input to MakeZip__PrFittedForwardTracks__BestVertexRelations, needs ``["Pr"]`` tracks, e.g. from make_VeloKalman_fitted_tracks.
           make_pvs (DataHandle): maker of PVs, defaults to `make_pvs`.

       Returns:
           A dict mapping tracks zipped with PV relations to ``'PrFittedForwardWithPVs'``.
    """
    pv_container = make_pvs()
    pr_in_tracks = tracks["Pr"]  # PrFittedForwardTracks
    pv_relations = MakePVRelations__PrFittedForwardTracks(
        Input=pr_in_tracks, InputVertices=pv_container).Output
    trackrel_zip = MakeZip__PrFittedForwardTracks__BestVertexRelations(
        Input1=pr_in_tracks, Input2=pv_relations)
    return {
        "PrFittedForwardWithPVs": trackrel_zip.Output,
    }


@configurable
def get_global_materiallocator(materiallocator=SimplifiedMaterialLocator):
    """Returns instance of (Simplified/Detailed)MaterialLocator.

       Args:
           materiallocator: MaterialLocator tool, defaults to SimplifiedMaterialLocator.

       Returns:
           Instance of input tool.
    """
    return materiallocator()


@configurable
def get_global_measurement_provider(velo_hits=make_VPClus_hits,
                                    ut_hits=make_PrStoreUTHit_hits,
                                    ft_clusters=make_FTRawBankDecoder_clusters,
                                    vp_provider=VPMeasurementProvider,
                                    ut_provider=UTMeasurementProvider,
                                    ft_provider=FTMeasurementProvider,
                                    muon_provider=DISABLE_TOOL,
                                    ignoreVP=False,
                                    ignoreUT=False,
                                    ignoreFT=False,
                                    ignoreMuon=True):
    """Returns instance of MeasurementProvider given VP, UT and FT provider and cluster/hits.

       Args:
           velo_clusters (DataHandle): maker of velo clusters, defaults to `make_VPClus_location_and_offsets`.
           ut_clusters (DataHandle): maker of UT hits, defaults to `make_PrStoreUTHit_hits`.
           ft_clusters (DataHandle): maker of FT clusters, defaults to `make_FTRawBankDecoder_clusters`.
           vp_provider: MeasurementProvider tool for VeloPix, defaults to VPMeasurementProvider.
           ut_provider: MeasurementProvider tool for UT, defaults to UTMeasurementProvider.
           ft_provider: MeasurementProvider tool for SciFi, defaults to FTMeasurementProvider.
           muon_provider: MeasurementProvider tool for Muon detector, disabled by default
           IngoreMuon: defaults to true, ignores muon measurement provider tool

       Returns:
           Instance of a global MeasurementProvider.
    """
    return MeasurementProvider(
        VPProvider=vp_provider(ClusterLocation=velo_hits()),
        UTProvider=ut_provider(ClusterLocation=ut_hits()),
        FTProvider=ft_provider(ClusterLocation=ft_clusters()),
        MuonProvider=muon_provider(),
        IgnoreVP=ignoreVP,
        IgnoreUT=ignoreUT,
        IgnoreFT=ignoreFT,
        IgnoreMuon=ignoreMuon)


@configurable
def get_track_master_fitter(
        get_materiallocator=get_global_materiallocator,
        get_measurement_provider=get_global_measurement_provider,
        MaxUpdateTransports=10):
    """Returns instance of TrackMasterFitter given MaterialLocator and MeasurementProvider.

       Args:
           get_materiallocator: MaterialLocator tool, defaults to `get_global_materiallocator`.
           get_measurement_provider: MeasurementProvider tool, defaults to `get_global_measurement_provider`.

       Returns:
           Instance of TrackMasterFitter.
    """
    materialLocator = get_materiallocator()
    return TrackMasterFitter(
        MeasProvider=get_measurement_provider(),
        MaterialLocator=materialLocator,
        Extrapolator=TrackMasterExtrapolator(MaterialLocator=materialLocator),
        MaxUpdateTransports=MaxUpdateTransports)


@configurable
def make_TrackEventFitter_fitted_tracks(tracks,
                                        fitter_tool=get_track_master_fitter):
    """Fits tracks with TrackEventFitter.

       Args:
           tracks (dict of dicts): input tracks to TrackEventFitter, needs``["Forward"]["v1"]`` tracks, e.g. from make_hlt1_tracks.
           fitter_tool: instance of track fitting tool, defaults to `get_track_master_fitter`.

       Returns:
           A dict mapping fitted HLT1 forward v1 tracks to ``'v1'``.
    """
    return {
        "v1":
        TrackEventFitter(
            TracksInContainer=tracks["Forward"]["v1"],
            Fitter=fitter_tool()).TracksOutContainer
    }


@configurable
def make_hlt1_fitted_tracks(
        tracks, make_forward_fitted_tracks=make_VeloKalman_fitted_tracks):
    """Helper function to bind to for passing forward fitted tracks.

       Args:
           tracks (dict of dicts): input tracks to the track fitter, i.e. to ``make_forward_fitted_tracks``, e.g. from make_hlt1_tracks.
           make_forward_fitted_tracks (DataHandle): track fitter, defaults to `make_VeloKalman_fitted_tracks`.

       Returns:
           A dict of fitted tracks. The content depends on the input maker
    """
    return make_forward_fitted_tracks(tracks)
