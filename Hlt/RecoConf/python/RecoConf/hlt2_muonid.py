###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.Algorithms import MuonIDHlt2Alg
from RecoConf.hlt1_muonid import make_muon_hits


@configurable
def make_muon_ids(track_type, tracks, make_muon_hits=make_muon_hits):
    return MuonIDHlt2Alg(
        name="MuonIDHlt2Alg" + track_type + "_{hash}",
        InputTracks=tracks,
        InputMuonHits=make_muon_hits()).OutputMuonPID
