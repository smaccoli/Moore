###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import importlib
from PyConf import configurable
from PyConf.components import Algorithm, matchAlgoName
from PyConf.control_flow import CompositeNode
from PyConf.Algorithms import (
    LHCb__Converters__Track__v1__fromV2TrackV1TrackVector as
    FromV2TrackV1Track, fromV3TrackV1Track,
    LHCb__Converters__Track__v2__fromV1TrackV2Track as fromV1TrackV2Track)
from RecoConf.core_algorithms import make_unique_id_generator

from AllenConf.hlt1_reconstruction import hlt1_reconstruction


@configurable
def allen_gaudi_node(sequence='hlt1_pp_default', node_name='hlt1_node'):
    """
    Provide the Allen top-level node 
    """
    seq = importlib.import_module('AllenSequences.{}'.format(sequence))
    node = getattr(seq, node_name)
    return node


def get_allen_line_names(suffix='Decision'):
    """
    Read Allen HLT1 line names from the Allen control node
    Inputs:
      suffix:  a string that is appended to every Allen line node name
    """
    from PyConf.application import all_nodes_and_algs

    allen_node = allen_gaudi_node()
    allen_nodes, allen_algs = all_nodes_and_algs(allen_node)
    try:
        lines_node = next(
            node for node in allen_nodes if node.name == 'SetupAllLines')
    except StopIteration:
        print("Node 'SetupAllLines' not found")

    return [
        child.name.replace('_node', '') + suffix
        for child in lines_node.children
    ]


def call_allen_raw_reports():
    """
    Configures GaudiAllenReportsToRawEvent transformer to Convert output of
    Allen dec_reporter and sel_report_writer to LHCb::RawBank::View objects.
    """
    from PyConf.Algorithms import GaudiAllenReportsToRawEvent

    allen_cf_node = allen_gaudi_node()

    # Find the algorithms for the input required
    gather_selections = find_by_name(allen_cf_node, 'gather_selections')
    dec_reporter = find_by_name(allen_cf_node, 'dec_reporter')
    make_selreps = find_by_name(allen_cf_node, 'make_selreps')
    prefix_sum_selrep_size = find_by_name(allen_cf_node,
                                          'prefix_sum_selrep_size')

    return GaudiAllenReportsToRawEvent(
        allen_number_of_active_lines=gather_selections.
        host_number_of_active_lines_t,
        allen_dec_reports=dec_reporter.dev_dec_reports_t,
        allen_selrep_offsets=prefix_sum_selrep_size.dev_output_buffer_t,
        allen_sel_reports=make_selreps.dev_sel_reports_t)


def make_allen_dec_reports(raw_reports=call_allen_raw_reports):
    """
    Make DecReports objects by decoding LHCb::RawEvent
    """
    decoder = decode_allen_dec_reports(raw_reports)
    return decoder.OutputHltDecReportsLocation


def make_allen_sel_reports(raw_reports=call_allen_raw_reports):
    """
    Make SelReports objects by decoding LHCb::RawEvent
    """
    decoder = decode_allen_sel_reports(raw_reports)
    return decoder.OutputHltSelReportsLocation


def make_allen_decision():
    from PyConf.Algorithms import GaudiAllenFilterEventsLineDecisions

    allen_cf_node = allen_gaudi_node()

    # Find the algorithms for the input required
    global_decision = find_by_name(allen_cf_node, 'global_decision')

    return GaudiAllenFilterEventsLineDecisions(
        allen_global_decision=global_decision.host_global_decision_t)


def call_allen_decision_logger():
    """
    Configure GaudiAllenCountAndDumpLineDecisions to count and report
    Allen line decisions.
    """
    from PyConf.Algorithms import GaudiAllenCountAndDumpLineDecisions

    allen_cf_node = allen_gaudi_node()

    # Find the algorithms for the input required
    gather_selections = find_by_name(allen_cf_node, 'gather_selections')

    return GaudiAllenCountAndDumpLineDecisions(
        allen_number_of_active_lines=gather_selections.
        host_number_of_active_lines_t,
        allen_names_of_active_lines=gather_selections.
        host_names_of_active_lines_t,
        allen_selections=gather_selections.dev_selections_t,
        allen_selections_offsets=gather_selections.dev_selections_offsets_t,
        Hlt1LineNames=get_allen_line_names())


def decode_allen_dec_reports(raw_reports=call_allen_raw_reports):
    from PyConf.Algorithms import HltDecReportsDecoder

    decoder = HltDecReportsDecoder(
        RawBanks=raw_reports().OutputDecView, SourceID='Hlt1')
    return decoder


def decode_allen_sel_reports(raw_reports=call_allen_raw_reports):
    from PyConf.Algorithms import HltSelReportsDecoder

    decoder = HltSelReportsDecoder(
        RawBanks=raw_reports().OutputSelView,
        DecReports=raw_reports().OutputDecView,
        SourceID='Hlt1')
    return decoder


def make_allen_calo_clusters():
    """
    Configures the adaptor between the calo clusters reconstructed in
    Allen and Gaudi-LHCb CaloClusters.
    """
    from PyConf.Algorithms import GaudiAllenCaloToCaloClusters

    allen_gaudi_node()  # Configure Allen sequence and apply bindings

    allen_ecal_clusters = hlt1_reconstruction()['ecal_clusters']

    ecal_clusters = GaudiAllenCaloToCaloClusters(
        allen_ecal_cluster_offsets=allen_ecal_clusters[
            'dev_ecal_cluster_offsets'],
        allen_ecal_clusters=allen_ecal_clusters['dev_ecal_clusters']
    ).AllenEcalClusters

    return {"ecal_clusters": ecal_clusters}


def make_allen_velo_tracks():
    """
    Configures the adaptor between the velo tracks reconstructed in
    Allen and Gaudi-LHCb tracks.
    """
    from PyConf.Algorithms import (GaudiAllenVeloToV3Tracks,
                                   TrackContainersMerger)

    allen_gaudi_node()  # Configure Allen sequence and apply bindings

    allen_reco = hlt1_reconstruction()

    # Velo reconstruction
    velo_tracks = allen_reco['velo_tracks']
    velo_states = allen_reco['velo_states']

    velo_tracks_v3_alg = GaudiAllenVeloToV3Tracks(
        allen_tracks_mec=velo_tracks["dev_velo_multi_event_tracks_view"],
        allen_beamline_states_view=velo_states[
            "dev_velo_kalman_beamline_states_view"],
        InputUniqueIDGenerator=make_unique_id_generator())

    velo_tracks_fwd_v3 = velo_tracks_v3_alg.OutputTracksForward
    velo_tracks_bwd_v3 = velo_tracks_v3_alg.OutputTracksBackward

    velo_tracks_fwd_v1_keyed = fromV3TrackV1Track(
        InputTracks=velo_tracks_fwd_v3).OutputTracks

    velo_tracks_bwd_v1_keyed = fromV3TrackV1Track(
        InputTracks=velo_tracks_bwd_v3).OutputTracks

    velo_tracks_v1_keyed = TrackContainersMerger(
        InputLocations=[velo_tracks_fwd_v1_keyed, velo_tracks_bwd_v1_keyed
                        ]).OutputLocation

    velo_tracks_v2 = fromV1TrackV2Track(
        InputTracksName=velo_tracks_v1_keyed).OutputTracksName

    velo_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=velo_tracks_v2).OutputTracksName

    return {
        "v3fwd": velo_tracks_fwd_v3,
        "v3bwd": velo_tracks_bwd_v3,
        "v2": velo_tracks_v2,
        "v1": velo_tracks_v1,
        "v1keyed": velo_tracks_v1_keyed
    }


def make_allen_velo_ut_tracks():
    """
    Configures the adaptor between the velo tracks reconstructed in
    Allen and Gaudi-LHCb tracks.
    """
    from PyConf.Algorithms import GaudiAllenUTToV3Tracks

    allen_gaudi_node()  # Configure Allen sequence and apply bindings

    allen_reco = hlt1_reconstruction()

    # Velo reconstruction
    velo_states = allen_reco["velo_states"]
    # UT reconstruction
    ut_tracks = allen_reco["ut_tracks"]

    ut_tracks_v3 = GaudiAllenUTToV3Tracks(
        allen_tracks_mec=ut_tracks["dev_ut_multi_event_tracks_view"],
        allen_beamline_states_view=velo_states[
            "dev_velo_kalman_beamline_states_view"],
        allen_endvelo_states_view=velo_states[
            "dev_velo_kalman_endvelo_states_view"],
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    ut_tracks_v1_keyed = fromV3TrackV1Track(
        InputTracks=ut_tracks_v3).OutputTracks

    ut_tracks_v2 = fromV1TrackV2Track(
        InputTracksName=ut_tracks_v1_keyed).OutputTracksName

    ut_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=ut_tracks_v2).OutputTracksName

    return {
        "v3": ut_tracks_v3,
        "v2": ut_tracks_v2,
        "v1": ut_tracks_v1,
        "v1keyed": ut_tracks_v1_keyed
    }


def make_allen_forward_tracks():
    """
    Configures the adaptor between the forward tracks reconstructed in
    Allen and Gaudi-LHCb tracks.
    """
    from PyConf.Algorithms import GaudiAllenMEBasicParticlesToV3Tracks

    allen_gaudi_node()  # Configure Allen sequence and apply bindings

    allen_reco = hlt1_reconstruction()

    forward_tracks_v3 = GaudiAllenMEBasicParticlesToV3Tracks(
        allen_tracks_mec=allen_reco['long_track_particles']
        ['dev_multi_event_basic_particles'],
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    forward_tracks_v1_keyed = fromV3TrackV1Track(
        InputTracks=forward_tracks_v3).OutputTracks

    forward_tracks_v2 = fromV1TrackV2Track(
        InputTracksName=forward_tracks_v1_keyed).OutputTracksName

    forward_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=forward_tracks_v2).OutputTracksName

    return {
        "v3": forward_tracks_v3,
        "v2": forward_tracks_v2,
        "v1": forward_tracks_v1,
        "v1keyed": forward_tracks_v1_keyed
    }


def make_allen_seed_and_match_tracks_no_ut():
    """
    Configures the adaptor between the seed and match tracks reconstructed in
    Allen and Gaudi-LHCb tracks.
    """
    from PyConf.Algorithms import GaudiAllenMEBasicParticlesToV3Tracks

    allen_gaudi_node()  # Configure Allen sequence and apply bindings

    from AllenConf.enum_types import TrackingType
    allen_reco = hlt1_reconstruction(
        tracking_type=TrackingType.MATCHING, with_ut=False)

    seed_and_match_tracks_v3 = GaudiAllenMEBasicParticlesToV3Tracks(
        allen_tracks_mec=allen_reco['long_track_particles']
        ['dev_multi_event_basic_particles'],
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    seed_and_match_tracks_v1_keyed = fromV3TrackV1Track(
        InputTracks=seed_and_match_tracks_v3).OutputTracks

    seed_and_match_tracks_v2 = fromV1TrackV2Track(
        InputTracksName=seed_and_match_tracks_v1_keyed).OutputTracksName

    seed_and_match_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=seed_and_match_tracks_v2).OutputTracksName

    return {
        "v3": seed_and_match_tracks_v3,
        "v2": seed_and_match_tracks_v2,
        "v1": seed_and_match_tracks_v1,
        "v1keyed": seed_and_match_tracks_v1_keyed
    }


def make_allen_secondary_vertices():
    """
    Configures the adaptor between the secondary vertices reconstructed in
    Allen and Gaudi-LHCb vertices.
    """
    from PyConf.Algorithms import (
        GaudiAllenSVsToRecVertexV2,
        LHCb__Converters__RecVertex__v1__fromRecVertexv2RecVertexv1 as
        FromRecVertexv2RecVertexv1)

    allen_gaudi_node()  # Configure Allen sequence and apply bindings

    allen_reco = hlt1_reconstruction()

    tracks = make_allen_forward_tracks()
    forward_tracks = allen_reco['long_tracks']
    secondary_vertices = allen_reco['secondary_vertices']

    allen_v2_svs = GaudiAllenSVsToRecVertexV2(
        allen_atomics_scifi=forward_tracks["dev_offsets_long_tracks"],
        allen_sv_offsets=secondary_vertices['dev_sv_offsets'],
        allen_secondary_vertices=secondary_vertices['dev_consolidated_svs'],
        InputTracks=tracks['v2']).OutputSVs

    svs_v1 = FromRecVertexv2RecVertexv1(
        InputVerticesName=allen_v2_svs,
        InputTracksName=tracks['v1']).OutputVerticesName

    return {'v2': allen_v2_svs, 'v1': svs_v1}


def gather_leafs(node):
    """
    gathers algs from a tree that do decision making
    """

    def impl(node):
        if isinstance(node, Algorithm):
            yield node
        if isinstance(node, CompositeNode):
            for child in node.children:
                yield from impl(child)

    return frozenset(impl(node))


def gather_algs(node):
    return frozenset([
        alg for leaf in gather_leafs(node) for alg in leaf.all_producers(False)
    ])


def find_by_name(node, name):
    try:
        named_alg = next(
            alg for alg in gather_algs(node) if matchAlgoName(alg.name, name))
    except StopIteration:
        # deal with postfixed `#N` and see if that gives a unique algorithm
        import re
        prog = re.compile('^{}#[0-9]+$'.format(name))
        algs = [alg for alg in gather_algs(node) if prog.match(alg.name)]
        if len(algs) == 1:
            named_alg = algs[0]
        else:
            print('Algorithm {} not found in node'.format(name))

    return named_alg


@configurable
def combine_raw_banks_with_MC_data_for_standalone_Allen_checkers(output_file):
    """Combine raw banks and MC info required for Allen standalone running into one RawEvent

    Combining:
    - The Velo, UT, SciFi, Muon, ODIN banks
    - MC info from the TrackerDumper and the PVDumper required for the Allen standalone checker
    Written to binaries (inside the "geometry" directory):
    """

    from Moore.config import DETECTOR_RAW_BANK_TYPES
    from RecoConf.mc_checking import tracker_dumper, pv_dumper
    from PyConf.application import default_raw_event, mdf_writer
    from PyConf.Algorithms import RawEventCombiner

    algs = []
    trackerDumper = tracker_dumper(dump_to_root=False)
    algs.append(trackerDumper)
    pvDumper = pv_dumper()
    algs.append(pvDumper)

    detector_parts = [
        default_raw_event([bt]) for bt in DETECTOR_RAW_BANK_TYPES
    ]
    # get only unique elements while preserving order
    detector_parts = list(dict.fromkeys(detector_parts).keys())
    combiner = RawEventCombiner(RawEventLocations=detector_parts + [
        trackerDumper.OutputRawEventLocation,
        pvDumper.OutputRawEventLocation,
    ])

    algs.append(combiner)

    algs.append(
        mdf_writer(output_file, location=combiner.RawEvent, compression=0))

    return algs
