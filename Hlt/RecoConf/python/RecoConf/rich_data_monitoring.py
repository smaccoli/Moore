###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable
from PyConf.utilities import ConfigurationError

from PyConf.Algorithms import (
    Rich__Future__Rec__Moni__DetectorHits as DetectorHits,
    Rich__Future__SmartIDClustering as RichClustering,
    Rich__Future__Rec__Moni__PixelClusters as ClusterMoni,
    Rich__Future__Rec__Moni__SIMDAlignment as MirrorAlign,
    Rich__Future__Rec__Moni__SIMDRecoStats as RecoStats,
    Rich__Future__Rec__Moni__TrackSelEff as TrackSelEff,
    Rich__Future__Rec__Moni__TrackGeometry as TrackGeometry,
    Rich__Future__Rec__Moni__SIMDPhotonCherenkovAngles as PhotAngles,
    Rich__Future__Rec__Moni__TrackRadiatorMaterial as TkMaterial,
    Rich__Future__Rec__Moni__DLLs as DLLs,
    Rich__Future__Rec__Moni__MassHypoRings as MassRings,
    Rich__Future__Rec__Moni__PhotonYield as PhotonYield,
    Rich__Future__Rec__Moni__GeometricalEfficiencies as GeomEffs,
    Rich__Future__Rec__Moni__PixelBackgrounds as PixBackgrds)

from PyConf.Tools import TrackSelector

from .rich_reconstruction import (get_radiator_bool_opts,
                                  get_detector_bool_opts)

###############################################################################


@configurable
def default_rich_monitoring_options(init_override_opts={}):
    """
    Returns a dict of the default RICH data monitoring options

    Args:
        init_override_opts (dict): override of the initial opts.
    """

    opts = {
        "TightTrackSelection": {
            "MinP": 10.0 * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2.0,
            "MaxGhostProb": 0.1,
        },
        "UseUT": True,
        "CKResHistoRange": (0.025, 0.0026, 0.002),
    }

    opts.update(init_override_opts)

    return opts


###############################################################################


@configurable
def alignment_rich_monitoring_options(radiator="Rich2Gas",
                                      init_override_opts={}):
    """
    Returns a dict of the RICH alignment histogram filling options.

    Args:
       radiator (string): RICH to align.
       init_override_opts (dict): override of the initial opts.

    Returns:
        A dict of additional monitoring options.
    """

    opts = {}
    if radiator == "Rich1Gas":
        # yapf: disable
        opts = {
            "PrebookHistos": [
                ('p00', 's00'), ('p00', 's01'), ('p01', 's04'), ('p01', 's05'),
                ('p00', 's02'), ('p00', 's03'), ('p01', 's06'), ('p01', 's07'),
                ('p03', 's08'), ('p03', 's09'), ('p02', 's12'), ('p02', 's13'),
                ('p03', 's10'), ('p03', 's11'), ('p02', 's14'), ('p02', 's15'),
            ],
            "MinP4Align": 40. * GeV,
            "DeltaThetaRange": 0.004,
            "NPhiBins": 60,
            "NThetaBins": 50,
            "PoorestPopulation": 100.,
            "MinUsefulTracks": 1,
            "Variant": '',
        }
        # yapf: enable
    elif radiator == "Rich2Gas":
        # yapf: disable
        opts = {
            "PrebookHistos": [
                ('p27', 's19'),  ('p55', 's39'),
                ('p26', 's18'),  ('p54', 's38'),
                ('p25', 's17'),  ('p53', 's37'),
                ('p24', 's16'),  ('p52', 's36'),
                ('p23', 's19'),  ('p51', 's39'),
                ('p23', 's15'),  ('p51', 's38'),
                ('p22', 's14'),  ('p51', 's35'),
                ('p21', 's18'),  ('p50', 's37'),
                ('p21', 's13'),  ('p50', 's34'),
                ('p20', 's17'),  ('p49', 's36'),
                ('p20', 's16'),  ('p49', 's33'),
                ('p20', 's12'),  ('p48', 's32'),
                ('p19', 's15'),  ('p47', 's35'),
                ('p19', 's11'),  ('p47', 's31'),
                ('p18', 's14'),  ('p46', 's34'),
                ('p18', 's10'),  ('p46', 's30'),
                ('p17', 's13'),  ('p45', 's33'),
                ('p17', 's09'),  ('p45', 's29'),
                ('p16', 's12'),  ('p44', 's32'),
                ('p16', 's08'),  ('p44', 's28'),
                ('p15', 's11'),  ('p43', 's31'),
                ('p14', 's11'),  ('p43', 's30'),
                ('p14', 's10'),  ('p42', 's30'),
                ('p13', 's10'),  ('p42', 's29'),
                ('p13', 's09'),  ('p41', 's29'),
                ('p12', 's09'),  ('p41', 's28'),
                ('p12', 's08'),  ('p40', 's28'),
                ('p11', 's11'),  ('p39', 's31'),
                ('p11', 's07'),  ('p39', 's27'),
                ('p10', 's10'),  ('p38', 's30'),
                ('p10', 's06'),  ('p38', 's26'),
                ('p09', 's09'),  ('p37', 's29'),
                ('p09', 's05'),  ('p37', 's25'),
                ('p08', 's08'),  ('p36', 's28'),
                ('p08', 's04'),  ('p36', 's24'),
                ('p07', 's07'),  ('p35', 's27'),
                ('p06', 's06'),  ('p35', 's23'),
                ('p06', 's03'),  ('p35', 's22'),
                ('p05', 's05'),  ('p34', 's26'),
                ('p05', 's02'),  ('p34', 's21'),
                ('p04', 's04'),  ('p33', 's25'),
                ('p04', 's01'),  ('p32', 's24'),
                ('p04', 's00'),  ('p32', 's20'),
                ('p03', 's03'),  ('p31', 's23'),
                ('p02', 's02'),  ('p30', 's22'),
                ('p01', 's01'),  ('p29', 's21'),
                ('p00', 's00'),  ('p28', 's20'),
            ],
            "MinP4Align": 60. * GeV,
            "DeltaThetaRange": 0.004,
            "NPhiBins": 60,
            "NThetaBins": 50,
            "PoorestPopulation": 100.,
            "MinUsefulTracks": 1,
            "Variant": '',
        }
        # yapf: enable
    else:
        raise ConfigurationError("Unknown radiator tag " + radiator)

    opts.update({
        "HistoOutputLevel": 3,
    })

    opts.update(init_override_opts)

    return opts


###############################################################################


@configurable
def alignment_rich_reco_options(radiator="Rich2Gas"):
    """
    Returns a dict of additional Cherenkov photon reconstruction options.

    In case of the RICH alignment histograms filling, additional Cherenkov
    photon reconstruction options are required. NB! alignment is done strictly
    one radiator at a time, because in each case entirely different sets of
    selected in HLT1 events are used.

    Args:
        radiator (string): RICH to align.

    Returns:
        A dict of additional Cherenkov photon reconstruction options.
    """

    opts = {}
    if radiator == "Rich1Gas":
        opts = {
            "RichGases": ["Rich1Gas"],
            "RejectAmbiguousPhotons": (False, True, False),
        }
    elif radiator == "Rich2Gas":
        opts = {
            "RichGases": ["Rich2Gas"],
            "RejectAmbiguousPhotons": (False, False, True),
        }
    else:
        raise ConfigurationError("Unknown radiator tag " + radiator)

    opts.update({
        "SaveMirrorData": True,
        "Particles": ["pion"],
    })

    return opts


###############################################################################


@configurable
def default_rich_monitors(moni_set="Standard"):
    """
    Returns the set of monitors to activate

    Args:
        moni_set (string): Monitor set to activate

    Returns:
        dict of activated monitoring algorithms
    """

    monitors = {
        # Activates all monitors, except "MirrorAlign".
        "Expert": [
            "RecoStats",
            "RichHits",
            "PixelClusters",
            "RichMaterial",
            "PixelBackgrounds",
            "TrackGeometry",
            "TrackSelEff",
            "PhotonCherenkovAngles",
            "DLLs",
            "PhotonYields",
            "GeomEffs",
            "MassRings",
        ],
        # The default set of monitors.
        "Standard": [
            "RecoStats",
            "RichHits",
            "PixelClusters",
            "RichMaterial",
            "PixelBackgrounds",
            "TrackGeometry",
            "TrackSelEff",
            "PhotonCherenkovAngles",
            "DLLs",
            "PhotonYields",
            "GeomEffs",
            "MassRings",
        ],
        # For monitoring at the pit.
        "OnlineMonitoring": [
            "RecoStats",
            "RichHits",
            "PixelClusters",
            "TrackSelEff",
            "PhotonCherenkovAngles",
            "DLLs",
        ],
        # For refractive index task monitoring
        "RefractiveIndex": [
            "RecoStats",
            "RichHits",
            "PhotonCherenkovAngles",
        ],
        # Activate only PhotonCherenkovAngles for testing purposes.
        "PhotonCherenkovAngles": ["PhotonCherenkovAngles"],

        # Producer of histograms for the RICH alignment procedure. It is not a
        # monitor as such, but rather a filler of histograms for the subsequent
        # processing of them, aimed at working out the misalignment
        # compensations and taking decision, whether they are significant
        # enough to be applied.
        "MirrorAlign": ["MirrorAlign"],
        #
        "None": [],
    }

    if moni_set not in monitors:
        raise ConfigurationError("Unknown histogram set " + moni_set)

    return monitors[moni_set]


###############################################################################


@configurable
def make_rich_pixel_monitors(conf, reco_opts, moni_opts, moni_set="Standard"):
    """
    Returns a set of RICH pixel only level monitors (i.e. tracking free).

    Args:
        conf       (dict): Reconstruction configuration (data) to run monitoring on
        reco_opts  (dict): Reconstruction options
        moni_opts  (dict): Data monitoring options
        moni_set (string): Monitor set to activate

    Returns:
        dict of activated monitoring algorithms
    """

    # get the list of monitors to activate
    monitors = default_rich_monitors(moni_set)

    # The dict of configured monitors to return
    results = {}

    # RICH hits
    key = "RichHits"
    if key in monitors:
        results[key] = DetectorHits(
            name="RichRecPixelQC", DecodedDataLocation=conf["RichDecodedData"])

    # Rich Clustering checks
    key = "PixelClusters"
    if key in monitors:
        # Run custom monitoring specific clustering here, to be decoupled
        # from whatever the reco uses (likely no clustering).
        clustering = RichClustering(
            name="RichClusteringForMoni",
            # Force clustering on for both RICHes
            ApplyPixelClustering=(True, True),
            # input data
            DecodedDataLocation=conf["RichDecodedData"])
        # ... and now the monitor for these clusters
        results[key] = ClusterMoni(
            name="RichRecPixelClusters",
            RichPixelClustersLocation=clustering.RichPixelClustersLocation)

    return results


###############################################################################


@configurable
def make_rich_track_monitors(conf,
                             reco_opts,
                             moni_opts,
                             moni_set="Standard",
                             mirror_align_tasks=['Produce']):
    """
    Returns a set of RICH track-level monitors.

    Args:
        conf               (dict): Reconstruction data configuration to run monitoring
                                   on.
        reco_opts          (dict): Reconstruction options.
        moni_opts          (dict): Data monitoring options.
        moni_set         (string): Monitor set to activate.
        mirror_align_tasks (dict): MirrorAlign tasks to activate in
                                   Rec/Rich/RichFutureRecMonitors/src/
                                   RichSIMDAlignment.cpp.
                                   Implemented tasks are such as:
                                   'Produce'
                                   'Monitor'
                                   ...
                                   Full list of the tasks and explanations
                                   are in the above program.

    Returns:
        A dict of activated monitoring algorithms.
    """

    # get the list of monitors to activate
    monitors = default_rich_monitors(moni_set)

    # The dict of configured monitors to return
    results = {}

    # The track name for this configuration
    track_name = conf["TrackName"]

    # The detector and radiator options
    rad_opts = get_radiator_bool_opts(reco_opts, track_name)
    det_opts = get_detector_bool_opts(reco_opts, track_name)

    # Basic reco statistics
    key = "RecoStats"
    if key in monitors:
        results[key] = RecoStats(
            name="RichRecoStats" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackToSegmentsLocation=conf["SelectedTrackToSegments"],
            TrackSegmentsLocation=conf["TrackSegments"],
            CherenkovPhotonLocation=conf["CherenkovPhotons"])

    # RICH track geometry
    key = "TrackGeometry"
    if key in monitors:
        results[key] = TrackGeometry(
            name="RichTkGeom" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"])

    # RICH track selection efficiencies
    key = "TrackSelEff"
    if key in monitors:
        results[key] = TrackSelEff(
            name="RichTkSelEff" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TracksLocation=conf["InputTracks"],
            RichPIDsLocation=conf["RichPIDs"])

    # RICH cherenkov photon angles
    key = "PhotonCherenkovAngles"
    if key in monitors:
        # Standard monitor
        results[key] = PhotAngles(
            name="RiCKRes" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            CKResHistoRange=moni_opts["CKResHistoRange"],
            TracksLocation=conf["InputTracks"],
            TrackSegmentsLocation=conf["TrackSegments"],
            CherenkovPhotonLocation=conf["CherenkovPhotons"],
            CherenkovAnglesLocation=conf["SignalCKAngles"],
            SummaryTracksLocation=conf["SummaryTracks"],
            PhotonToParentsLocation=conf["PhotonToParents"],
        )
        # Tight monitor
        tight_sel = moni_opts["TightTrackSelection"]
        results[key + "Tight"] = PhotAngles(
            name="RiCKRes" + track_name + "Tight",
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSelector=TrackSelector(
                MinPCut=tight_sel["MinP"],
                MinPtCut=tight_sel["MinPt"],
                MaxChi2Cut=tight_sel["MaxChi2"],
                MaxGhostProbCut=tight_sel["MaxGhostProb"]),
            CKResHistoRange=moni_opts["CKResHistoRange"],
            TracksLocation=conf["InputTracks"],
            TrackSegmentsLocation=conf["TrackSegments"],
            CherenkovPhotonLocation=conf["CherenkovPhotons"],
            CherenkovAnglesLocation=conf["SignalCKAngles"],
            SummaryTracksLocation=conf["SummaryTracks"],
            PhotonToParentsLocation=conf["PhotonToParents"],
        )
    # Photon Yields
    key = "PhotonYields"
    if key in monitors:
        results["Emitted" + key] = PhotonYield(
            name="RiTkEmittedYields" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            MaximumYields=(100, 800, 800),
            TrackSegmentsLocation=conf["TrackSegments"],
            PhotonYieldLocation=conf["EmittedYields"])
        results["Detectable" + key] = PhotonYield(
            name="RiTkDetectableYields" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"],
            PhotonYieldLocation=conf["DetectableYields"])
        results["Signal" + key] = PhotonYield(
            name="RiTkSignalYields" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"],
            PhotonYieldLocation=conf["SignalYields"])

    # Geom. Effs.
    key = "GeomEffs"
    if key in monitors:
        results[key] = GeomEffs(
            name="RiTkGeomEffs" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"],
            GeomEffsLocation=conf["GeomEffs"])

    # Rich material monitor
    key = "RichMaterial"
    if key in monitors:
        results[key] = TkMaterial(
            name="RiTkMaterial" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"])

    # Rich likelihood pixel backgrounds
    key = "PixelBackgrounds"
    if key in monitors:
        results[key] = PixBackgrds(
            name="RichRecPixBkgs" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            RichSIMDPixelSummariesLocation=conf["RichSIMDPixels"],
            PixelBackgroundsLocation=conf["RichPixelBackgrounds"])

    # RICH DLL values
    key = "DLLs"
    if key in monitors:
        results[key] = DLLs(
            name="RichDLLs" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            RichPIDsLocation=conf["RichPIDs"])

    # RICH mass rings
    key = "MassRings"
    if key in monitors:
        results[key] = MassRings(
            name="RichMassRings" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"],
            MassHypothesisRingsLocation=conf["EmittedCKRings"],
            TrackLocalPointsLocation=conf["TrackLocalPoints"],
            CherenkovAnglesLocation=conf["SignalCKAngles"])

    # RICH alignment histogram production and/or optionally, others tasks
    key = "MirrorAlign"
    if key in monitors:
        results[key] = MirrorAlign(
            name="MirrorAlign" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSelector=TrackSelector(MinPCut=moni_opts["MinP4Align"], ),
            TracksLocation=conf["InputTracks"],
            TrackSegmentsLocation=conf["TrackSegments"],
            CherenkovPhotonLocation=conf["CherenkovPhotons"],
            CherenkovAnglesLocation=conf["SignalCKAngles"],
            SummaryTracksLocation=conf["SummaryTracks"],
            PhotonToParentsLocation=conf["PhotonToParents"],
            PhotonMirrorDataLocation=conf["PhotonMirrorData"],
            RichGases=reco_opts["RichGases"],
            PrebookHistos=moni_opts["PrebookHistos"],
            DeltaThetaRange=moni_opts["DeltaThetaRange"],
            NPhiBins=moni_opts["NPhiBins"],
            NThetaBins=moni_opts["NThetaBins"],
            HistoOutputLevel=moni_opts["HistoOutputLevel"],
            MirrorAlignTasks=mirror_align_tasks,
            PoorestPopulation=moni_opts["PoorestPopulation"],
            MinUsefulTracks=moni_opts["MinUsefulTracks"],
            Variant=moni_opts["Variant"],
        )

    return results
