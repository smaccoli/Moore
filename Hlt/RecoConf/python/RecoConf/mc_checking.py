###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
In this file, algorithms needed to run the MC reconstruction checking (PrTrackChecker) are defined.
'''
from PyConf.tonic import configurable
from PyConf.dataflow import DataHandle
from PyConf.application import (
    make_data_with_FetchDataFromFile,
    make_odin,
)
from PyConf.Algorithms import (
    VPFullCluster2MCParticleLinker, VPFullCluster2MCHitLinker,
    PrLHCbID2MCParticle, PrLHCbID2MCParticleVP, PrLHCbID2MCParticleVPUT,
    PrLHCbID2MCParticleVPFT, PrLHCbID2MCParticleVPUTFTMU,
    PrLHCbID2MCParticleVPFTMU, PrTrackAssociator, PrTrackChecker,
    PrUTHitChecker, TrackListRefiner, TrackResChecker, PrMultiplicityChecker,
    TrackIPResolutionCheckerNT, MCParticle2MCHitAlg, PrimaryVertexChecker,
    PrVPHitsChecker, PrUTHitsChecker, PrFTHitsChecker,
    TriggerObjectsCompatibilityProfileChecker,
    GaudiAllenPVsToPrimaryVertexContainer, PrTrackConverter)

from PyConf.Tools import (IdealStateCreator, LoKi__Hybrid__MCTool,
                          VisPrimVertTool, TrackMasterExtrapolator,
                          MCReconstructible, TrackDistanceExtraSelector)
from RecoConf.hlt1_tracking import (
    make_PrStoreUTHit_hits, make_FTRawBankDecoder_clusters,
    make_velo_full_clusters, make_VPClus_hits, make_PrStoreFTHit_hits,
    make_VeloClusterTrackingSIMD_hits, make_VeloClusterTrackingSIMD_tracks,
    make_PrStorePrUTHits_hits)

from RecoConf.mc_checking_categories import get_mc_categories, get_hit_type_mask, categories
from RecoConf.hlt1_muonid import make_muon_hits
from .data_from_file import boole_links_digits_mcparticles, boole_links_digits_mchits, mc_unpackers
import Functors as F


def get_item(x, key):
    """Return `key` from `x` if `x` is a dict, otherwise return `x`.

    TODO: This helper function can be replaces once Moore!63 has been addressed.
    """
    if isinstance(x, DataHandle): return x
    return x[key]


@configurable
def make_links_veloclusters_mcparticles():
    return VPFullCluster2MCParticleLinker(
        ClusterLocation=make_velo_full_clusters(),
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPDigit2MCParticleLinksLocation=boole_links_digits_mcparticles()
        ["VPDigits"],
    ).OutputLocation


@configurable
def make_links_veloclusters_mchits():
    return VPFullCluster2MCHitLinker(
        ClusterLocation=make_velo_full_clusters(),
        MCHitLocation=mc_unpackers()["MCVPHits"],
        VPDigit2MCHitLinksLocation=boole_links_digits_mchits()["VPDigits"],
    ).OutputLocation


@configurable
def make_links_lhcbids_mcparticles_VP():
    return PrLHCbID2MCParticleVP(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPFullClustersLocation=make_velo_full_clusters(),
        VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
    ).TargetName


@configurable
def make_links_lhcbids_mcparticles_VP_UT():
    return PrLHCbID2MCParticleVPUT(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPFullClustersLocation=make_velo_full_clusters(),
        VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
        UTHitsLocation=make_PrStoreUTHit_hits(),
        UTHitsLinkLocation=boole_links_digits_mcparticles()["UTClusters"],
    ).TargetName


@configurable
def make_links_lhcbids_mcparticles_VP_FT():
    return PrLHCbID2MCParticleVPFT(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPFullClustersLocation=make_velo_full_clusters(),
        VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
        FTLiteClustersLocation=make_FTRawBankDecoder_clusters(),
        FTLiteClustersLinkLocation=boole_links_digits_mcparticles()
        ["FTLiteClusters"],
    ).TargetName


@configurable
def make_links_lhcbids_mcparticles_tracking_system():
    return PrLHCbID2MCParticle(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPFullClustersLocation=make_velo_full_clusters(),
        VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
        UTHitsLocation=make_PrStoreUTHit_hits(),
        UTHitsLinkLocation=boole_links_digits_mcparticles()["UTClusters"],
        #UTHitsLinkLocation=boole_links_digits_mcparticles()["UTDigits"],
        FTLiteClustersLocation=make_FTRawBankDecoder_clusters(),
        FTLiteClustersLinkLocation=boole_links_digits_mcparticles()
        ["FTLiteClusters"],
    ).TargetName


@configurable
def make_links_lhcbids_mcparticles_tracking_and_muon_system(
        with_ut: bool = True):
    """Match lhcbIDs of hits in the tracking stations and muon stations to MCParticles.
    """
    if with_ut:
        return PrLHCbID2MCParticleVPUTFTMU(
            MCParticlesLocation=mc_unpackers()["MCParticles"],
            VPFullClustersLocation=make_velo_full_clusters(),
            VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
            UTHitsLocation=make_PrStoreUTHit_hits(),
            UTHitsLinkLocation=boole_links_digits_mcparticles()["UTClusters"],
            FTLiteClustersLocation=make_FTRawBankDecoder_clusters(),
            FTLiteClustersLinkLocation=boole_links_digits_mcparticles()
            ["FTLiteClusters"],
            MuonHitsLocation=make_muon_hits(),
            MuonHitsLinkLocation=boole_links_digits_mcparticles()
            ["MuonDigits"],
        ).TargetName
    else:
        return PrLHCbID2MCParticleVPFTMU(
            MCParticlesLocation=mc_unpackers()["MCParticles"],
            VPFullClustersLocation=make_velo_full_clusters(),
            VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
            FTLiteClustersLocation=make_FTRawBankDecoder_clusters(),
            FTLiteClustersLinkLocation=boole_links_digits_mcparticles()
            ["FTLiteClusters"],
            MuonHitsLocation=make_muon_hits(),
            MuonHitsLinkLocation=boole_links_digits_mcparticles()
            ["MuonDigits"],
        ).TargetName


@configurable
def make_links_tracks_mcparticles(InputTracks, LinksToLHCbIDs):
    return PrTrackAssociator(
        SingleContainer=get_item(InputTracks, "v1"),
        LinkerLocationID=LinksToLHCbIDs,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation


@configurable
def check_tracking_efficiency(
        TrackType,
        InputTracks,
        LinksToTracks,
        LinksToLHCbIDs,
        MCCategories,
        HitTypesToCheck,
        WriteHistos=2,
):
    """ Setup tracking efficiency checker

    Args:
        InputTracks (dict or DataHandle): The InputTracks can be either a dict with the key v1 or a DataHandle to LHCb::Event::v1::Tracks directly.
    """
    props = dict(
        Title=TrackType,
        HitTypesToCheck=HitTypesToCheck,
        MyCuts=MCCategories,
        WriteHistos=WriteHistos,
    )
    return PrTrackChecker(
        name=TrackType + "TrackChecker_{hash}",
        Tracks=get_item(InputTracks, "v1"),
        Links=LinksToTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"],
        LinkTableLocation=LinksToLHCbIDs,
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        LoKiFactory=LoKi__Hybrid__MCTool(Modules=["LoKiMC.decorators"]),
        **props)


@configurable
def check_tracking_efficiency_vs_multiplicity(
        TrackType,
        InputTracks,
        LinksToTracks,
        LinksToLHCbIDs,
        MCCategories,
):
    """ Setup tracking efficiency checker vs multiplicity

    Args:
        InputTracks (dict or DataHandle): The InputTracks can be either a dict with the key v1 or a DataHandle to LHCb::Event::v1::Tracks directly.
    """
    props = dict(Title=TrackType, )
    return PrMultiplicityChecker(
        name=TrackType + "MultiplicityChecker_{hash}",
        Tracks=get_item(InputTracks, "v1"),
        Links=LinksToTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"],
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        Velo_Hits=make_VeloClusterTrackingSIMD_hits(),
        Velo_Tracks=make_VeloClusterTrackingSIMD_tracks()['Pr'],
        UT_Hits=make_PrStoreUTHit_hits(),
        FT_Hits=make_PrStoreFTHit_hits(),
        **props)


@configurable
def monitor_uthit_efficiency(
        TrackType,
        InputTracks,
        LinksToTracks,
        LinksToLHCbIDs,
        MCCategories,
        WriteHistos=1,
):
    """ Setup UT hit efficiency checker

    Args:
        InputTracks (dict or DataHandle): The InputTracks can be either a dict with the key v1 or a DataHandle to LHCb::Event::v1::Tracks directly.
    """
    props = dict(
        Title=TrackType + "UTHits",
        WriteHistos=WriteHistos,
        MyCuts=MCCategories,
    )

    return PrUTHitChecker(
        name=TrackType + "UTHitsChecker_{hash}",
        Tracks=get_item(InputTracks, "v1"),
        Links=LinksToTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"],
        LinkTableLocation=LinksToLHCbIDs,
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        LoKiFactory=LoKi__Hybrid__MCTool(Modules=["LoKiMC.decorators"]),
        **props)


@configurable
def get_track_checkers(
        types_and_locations,
        uthit_efficiency_types=["Forward", "Downstream", "Match"],
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system
):
    """ Setup track and UT hit efficiency checkers

    The track type has to correspond to a key mc_checking_categories.categories. This defines which MC particle categories are tested.

    Args:
        types_and_locations (dict): Types and locations of track containers.
        uthit_efficiency_types (list of str): Types for which dedicated UT hit efficiency checker is setup.
        make_links_lhcbids_mcparticles (function): Maker that returns ???
    """

    assert isinstance(
        types_and_locations,
        dict), "Please provide a dictionary of track type and tracks"

    efficiency_checkers = []
    links_to_lhcbids = make_links_lhcbids_mcparticles()
    # TODO (Python 3 compatibility) remove sort once we drop Python 2 support
    # so that we keep the more meaningful order of items as defined originally.
    for track_type, tracks in sorted(types_and_locations.items()):
        assert track_type in categories, track_type + " unknown. Please chose from " + ", ".join(
            categories)

        links_to_tracks = make_links_tracks_mcparticles(
            InputTracks=tracks, LinksToLHCbIDs=links_to_lhcbids)

        pr_checker = check_tracking_efficiency(
            TrackType=track_type,
            InputTracks=tracks,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(track_type),
            HitTypesToCheck=get_hit_type_mask(track_type),
        )
        efficiency_checkers.append(pr_checker)

        if track_type not in uthit_efficiency_types:
            continue
        uthit_checker = monitor_uthit_efficiency(
            TrackType=track_type,
            InputTracks=tracks,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(track_type + "UTHits"),
        )
        efficiency_checkers.append(uthit_checker)
    return efficiency_checkers


@configurable
def get_track_checkers_multiplicity(
        types_and_locations,
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system
):
    """ Setup track efficiency vs multiplicity checker

    The track type has to correspond to a key mc_checking_categories.categories. This defines which MC particle categories are tested.

    Args:
        types_and_locations (dict): Types and locations of track containers.
        make_links_lhcbids_mcparticles (function): Maker that returns ???
    """

    assert isinstance(
        types_and_locations,
        dict), "Please provide a dictionary of track type and tracks"

    efficiency_checkers = []
    links_to_lhcbids = make_links_lhcbids_mcparticles()
    # TODO (Python 3 compatibility) remove sort once we drop Python 2 support
    # so that we keep the more meaningful order of items as defined originally.
    for track_type, tracks in sorted(types_and_locations.items()):
        assert track_type in categories, track_type + " unknown. Please chose from " + ", ".join(
            categories)

        links_to_tracks = make_links_tracks_mcparticles(
            InputTracks=tracks, LinksToLHCbIDs=links_to_lhcbids)

        pr_multiplicity_checker = check_tracking_efficiency_vs_multiplicity(
            TrackType=track_type,
            InputTracks=tracks,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(track_type),
        )
        efficiency_checkers.append(pr_multiplicity_checker)

    return efficiency_checkers


@configurable
def get_fitted_tracks_checkers(
        FittedTracks,
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system,
        with_mc_hits=True,
        fitted_track_types=["BestLong", "BestDownstream"],
        max_ghost_prob=0.5,
        with_UT=True):

    links_to_lhcbids = make_links_lhcbids_mcparticles()

    links_to_tracks = {}
    for track_type in fitted_track_types:
        links_to_tracks[track_type] = make_links_tracks_mcparticles(
            InputTracks=FittedTracks[track_type],
            LinksToLHCbIDs=links_to_lhcbids)

    categories = []

    if "BestLong" in fitted_track_types:
        good_long_tracks = make_track_filter(
            InputTracks=FittedTracks["BestLong"],
            code=(F.GHOSTPROB < max_ghost_prob))
        categories.append((FittedTracks["BestLong"], "BestLong", "BestLong",
                           "BestLong"))
        categories.append((good_long_tracks, "LongGhostFiltered", "BestLong",
                           "BestLong"))

    if "BestDownstream" in fitted_track_types:
        good_downstream_tracks = make_track_filter(
            InputTracks=FittedTracks["BestDownstream"],
            code=(F.GHOSTPROB < max_ghost_prob))
        categories.append((FittedTracks["BestDownstream"], "BestDownstream",
                           "BestDownstream", "BestDownstream"))
        categories.append((good_downstream_tracks, "DownstreamGhostFiltered",
                           "BestDownstream", "BestDownstream"))

    efficiency_checkers = []

    for tracks, tr_key, mc_key, hit_key in categories:
        checker = check_tracking_efficiency(
            InputTracks=tracks,
            TrackType=tr_key,
            LinksToTracks=links_to_tracks[mc_key],
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(mc_key),
            HitTypesToCheck=get_hit_type_mask(hit_key),
        )
        efficiency_checkers.append(checker)

    if with_mc_hits:
        if with_UT:
            for tr_type in fitted_track_types:
                resolution_checker = check_track_resolution(
                    FittedTracks[tr_type], suffix=tr_type)
                efficiency_checkers.append(resolution_checker)
        else:
            for tr_type in fitted_track_types:
                resolution_checker = check_track_resolution(
                    FittedTracks[tr_type],
                    suffix=tr_type,
                    make_links_lhcbids_mcparticles=
                    make_links_lhcbids_mcparticles)
                efficiency_checkers.append(resolution_checker)

    return efficiency_checkers


@configurable
def get_best_tracks_checkers(BestTracks, with_mc_hits=True):

    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=BestTracks, LinksToLHCbIDs=links_to_lhcbids)

    long_tracks = make_track_filter(
        InputTracks=BestTracks,
        code=F.require_all(~F.TRACKISINVALID, F.TRACKISLONG))
    downstream_tracks = make_track_filter(
        InputTracks=BestTracks,
        code=F.require_all(~F.TRACKISINVALID, F.TRACKISDOWNSTREAM))
    good_long_tracks = make_track_filter(
        InputTracks=BestTracks,
        code=F.require_all(~F.TRACKISINVALID, F.TRACKISLONG,
                           F.GHOSTPROB < 0.5))
    good_downstream_tracks = make_track_filter(
        InputTracks=BestTracks,
        code=F.require_all(~F.TRACKISINVALID, F.TRACKISDOWNSTREAM,
                           F.GHOSTPROB < 0.5))

    efficiency_checkers = []
    for tracks, tr_key, mc_key, hit_key in [
        (BestTracks, "Best", "Best", "Best"),
        (long_tracks, "BestLong", "BestLong", "BestLong"),
        (good_long_tracks, "LongGhostFiltered", "BestLong", "BestLong"),
        (downstream_tracks, "BestDownstream", "BestDownstream",
         "BestDownstream"),
        (good_downstream_tracks, "DownstreamGhostFiltered", "BestDownstream",
         "BestDownstream"),
    ]:
        checker = check_tracking_efficiency(
            InputTracks=tracks,
            TrackType=tr_key,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(mc_key),
            HitTypesToCheck=get_hit_type_mask(hit_key),
        )
        efficiency_checkers.append(checker)

    if with_mc_hits:
        resolution_checker = check_track_resolution(BestTracks)
        efficiency_checkers.append(resolution_checker)

    return efficiency_checkers


@configurable
def get_pv_checkers(pvs, tracks, produce_ntuple=False):

    assert isinstance(
        pvs, DataHandle), "Please provide reconstructed primary verticies"

    pv_checkers = []

    pvchecker = PrimaryVertexChecker(
        produceNtuple=produce_ntuple,
        inputVerticesName=pvs,
        inputTracksName=tracks["v1"],
        MCVertexInput=mc_unpackers()["MCVertices"],
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCHeaderLocation=make_data_with_FetchDataFromFile("/Event/MC/Header"),
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"))

    pv_checkers.append(pvchecker)
    return pv_checkers


def make_track_filter(InputTracks, code):
    return TrackListRefiner(
        inputLocation=InputTracks["v1"], Code=code).outputLocation


def make_default_IdealStateCreator(public=False):
    mcpart = mc_unpackers()["MCParticles"]
    mcvert = mc_unpackers()["MCVertices"]
    vphits = mc_unpackers()["MCVPHits"]
    uthits = mc_unpackers()["MCUTHits"]
    fthits = mc_unpackers()["MCFTHits"]

    link_vp_hits = MCParticle2MCHitAlg(MCParticles=mcpart, MCHitPath=vphits)
    link_ut_hits = MCParticle2MCHitAlg(MCParticles=mcpart, MCHitPath=uthits)
    link_ft_hits = MCParticle2MCHitAlg(MCParticles=mcpart, MCHitPath=fthits)

    return IdealStateCreator(
        Extrapolator=TrackMasterExtrapolator(
            ExtraSelector=TrackDistanceExtraSelector(shortDist=0)),
        MCVertices=mcvert,
        VPMCHits=vphits,
        VPMCHitLinks=link_vp_hits,
        UTMCHits=uthits,
        UTMCHitLinks=link_ut_hits,
        FTMCHits=fthits,
        FTMCHitLinks=link_ft_hits,
        public=public)


@configurable
def check_track_resolution(
        InputTracks,
        per_hit_resolutions=False,
        split_per_type=False,
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system,
        suffix="",
        **kwargs):

    links_to_lhcbids = make_links_lhcbids_mcparticles()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks["v1"], LinksToLHCbIDs=links_to_lhcbids)
    mcpart = mc_unpackers()["MCParticles"]
    mchead = make_data_with_FetchDataFromFile('/Event/MC/Header')
    mcprop = make_data_with_FetchDataFromFile('/Event/MC/TrackInfo')
    res_checker = TrackResChecker(
        name="TrackResChecker" + suffix + "_{hash}",
        TracksInContainer=InputTracks["v1"],
        MCParticleInContainer=mc_unpackers()["MCParticles"],
        LinkerInTable=links_to_tracks,
        VisPrimVertTool=VisPrimVertTool(
            public=True,
            MCHeader=mchead,
            MCParticles=mcpart,
            MCProperties=mcprop),
        StateCreator=make_default_IdealStateCreator(public=True),
        FullDetail=per_hit_resolutions,
        SplitByType=split_per_type,
        **kwargs)

    return res_checker


def vphits_resolution_checker():
    mcparticles = mc_unpackers()["MCParticles"]
    vphits = make_VeloClusterTrackingSIMD_hits()
    vpclusters = make_velo_full_clusters()
    mcvphits = mc_unpackers()["MCVPHits"]
    vplinks = make_links_veloclusters_mchits()

    vpchecker = PrVPHitsChecker(
        MCParticleLocation=mcparticles,
        VPHitsLocation=vphits,
        MCHitsLocation=mcvphits,
        VPFullClusterLocation=vpclusters,
        VPHits2MCHitLinksLocation=vplinks,
        performStudy=True,
        isGlobal=True)
    return vpchecker


def uthits_resolution_checker():
    mcparticles = mc_unpackers()["MCParticles"]
    mcuthits = mc_unpackers()["MCUTHits"]
    uthits = make_PrStorePrUTHits_hits()
    utlinks = make_data_with_FetchDataFromFile(
        "/Event/Link/Raw/UT/Clusters2MCHits")
    #"/Event/Link/Raw/UT/TightDigits2MCHits")
    utchecker = PrUTHitsChecker(
        UTHitsLocation=uthits,
        MCHitsLocation=mcuthits,
        UTHits2MCHitLinksLocation=utlinks,
        MCParticleLocation=mcparticles,
        performStudy=True,
        isGlobal=True)
    return utchecker


def fthits_resolution_checker():
    mcparticles = mc_unpackers()["MCParticles"]
    fthits = make_PrStoreFTHit_hits()
    ftclusters = make_FTRawBankDecoder_clusters()
    mcfthits = mc_unpackers()["MCFTHits"]
    ftlinks = make_data_with_FetchDataFromFile(
        "/Event/Link/Raw/FT/LiteClusters2MCHits")
    ftchecker = PrFTHitsChecker(
        FTHitsLocation=fthits,
        FTLiteClusterLocation=ftclusters,
        FTHits2MCHitLinksLocation=ftlinks,
        MCHitsLocation=mcfthits,
        MCParticleLocation=mcparticles,
        performStudy=True,
        isGlobal=True)
    return ftchecker


@configurable
def hits_resolution_checkers(with_mc_hits=True,
                             with_VP=True,
                             with_UT=True,
                             with_FT=True):

    hits_checkers = []
    if with_mc_hits:
        if with_VP:
            vphits_checker = vphits_resolution_checker()
            hits_checkers.append(vphits_checker)
        if with_UT:
            uthits_checker = uthits_resolution_checker()
            hits_checkers.append(uthits_checker)
        if with_FT:
            fthits_checker = fthits_resolution_checker()
            hits_checkers.append(fthits_checker)

    return hits_checkers


def monitor_IPresolution(InputTracks, InputPVs, VeloTracks):

    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks, LinksToLHCbIDs=links_to_lhcbids)
    IPres_checker = TrackIPResolutionCheckerNT(
        TrackContainer=InputTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCHeaderLocation=make_data_with_FetchDataFromFile("/Event/MC/Header"),
        LinkerLocation=links_to_tracks,
        PVContainer=InputPVs,
        NTupleLUN="FILE1")
    return IPres_checker


def checker_trigger_objects(InputTracksHLT1, InputTracksHLT2, InputPVsHLT1,
                            InputPVsHLT2, VeloTracksHLT1, VeloTracksHLT2):

    allen_pvs = GaudiAllenPVsToPrimaryVertexContainer(
        number_of_multivertex=InputPVsHLT1[
            "dev_number_of_multi_final_vertices"],
        reconstructed_multi_pvs=InputPVsHLT1["dev_multi_final_vertices"]
    ).OutputPVs
    from PyConf.Algorithms import PVToRecConverterV1
    vertexConverterHLT1 = PVToRecConverterV1(
        InputVertices=allen_pvs, InputTracks=VeloTracksHLT1).OutputVertices

    allen_tracks = PrTrackConverter(
        InputTracksLocation=InputTracksHLT1).OutKeyedTrackLocation

    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    mcparticles = mc_unpackers()["MCParticles"]
    links_to_tracksHLT1 = make_links_tracks_mcparticles(
        allen_tracks, LinksToLHCbIDs=links_to_lhcbids)

    links_to_tracksHLT2 = make_links_tracks_mcparticles(
        InputTracksHLT2, LinksToLHCbIDs=links_to_lhcbids)
    mcprop = make_data_with_FetchDataFromFile("/Event/MC/TrackInfo")
    mchead = make_data_with_FetchDataFromFile("/Event/MC/Header")

    IPres_checker = TriggerObjectsCompatibilityProfileChecker(
        TrackContainerHLT1=allen_tracks,
        TrackContainerHLT2=InputTracksHLT2,
        MCParticleInput=mcparticles,
        LinkerLocationHLT1=links_to_tracksHLT1,
        LinkerLocationHLT2=links_to_tracksHLT2,
        PVContainerHLT1=vertexConverterHLT1,
        PVContainerHLT2=InputPVsHLT2,
        Selector=MCReconstructible(
            public=True,
            MCTrackInfo=make_data_with_FetchDataFromFile(
                "/Event/MC/TrackInfo")),
        StateCreator=make_default_IdealStateCreator(public=True),
        VisPrimVertTool=VisPrimVertTool(
            public=True,
            MCHeader=mchead,
            MCParticles=mcparticles,
            MCProperties=mcprop))

    return [IPres_checker]


@configurable
def tracker_dumper(odin_location=make_odin,
                   root_output_dir="dump/TrackerDumper",
                   dump_to_root=True,
                   velo_hits=make_VPClus_hits):
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    from PyConf.Algorithms import PrTrackerDumper
    return PrTrackerDumper(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPLightClusterLocation=velo_hits(),
        FTHitsLocation=make_PrStoreFTHit_hits(),
        UTHitsLocation=make_PrStoreUTHit_hits(),
        ODINLocation=odin_location(),
        LinkerLocation=links_to_lhcbids,
        DumpToROOT=dump_to_root,
        OutputDirectory=root_output_dir)


def pv_dumper(odin_location=make_odin, output_dir="dump/MC_info/PVs"):
    from PyConf.Algorithms import PVDumper
    return PVDumper(
        MCVerticesLocation=mc_unpackers()["MCVertices"],
        MCPropertyLocation=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"))
