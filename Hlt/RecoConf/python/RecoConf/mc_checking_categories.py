###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

categories = {
    "Velo": {
        "01_velo":
        "isVelo",
        "02_long":
        "isLong",
        "03_long_P>5GeV":
        "isLong & over5",
        "04_long_strange":
        "isLong & strange",
        "05_long_strange_P>5GeV":
        "isLong & strange & over5",
        "06_long_fromB":
        "isLong & fromB",
        "06_long_fromD":
        "isLong & fromD",
        "07_long_fromB_P>5GeV":
        "isLong & fromB & over5",
        "07_long_fromD_P>5GeV":
        "isLong & fromD & over5",
        "08_long_electrons":
        "isLong & isElectron",
        "09_long_fromB_electrons":
        "isLong & isElectron & fromB",
        "10_long_fromB_electrons_P>5GeV":
        "isLong & isElectron & over5 & fromB",
        "11_long_fromB_P>3GeV_Pt>0.5GeV":
        "isLong & fromB & trigger",
        "11_long_fromB_electrons_P>3GeV_Pt>0.5GeV":
        "isLong & fromB & trigger & isElectron",
        "11_long_strange_P>3GeV_Pt>0.5GeV":
        "isLong & strange& trigger",
        "11_long_fromD_P>3GeV_Pt>0.5GeV":
        "isLong & fromD & trigger",
        "12_UT_long_fromB_P>3GeV_Pt>0.5GeV":
        "isLong & fromB & trigger & isUT",
    },
    "VeloFull": {
        "01_notElectron_Velo":
        "isNotElectron & isVelo",
        "02_notElectron_Velo_Forward":
        "isNotElectron & isVelo & (MCETA>0)",
        "03_notElectron_Velo_Backward":
        "isNotElectron & isVelo & (MCETA<0)",
        "04_notElectron_Velo_Eta25":
        "isNotElectron & isVelo & (MCETA>2.0) & (MCETA<5.0)",
        "05_notElectron_Long_Eta25":
        "isNotElectron & isLong & (MCETA>2.0) & (MCETA<5.0)",
        "06_notElectron_Long_Eta25 p>5GeV":
        "isNotElectron & isLong & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
        "07_notElectron_Long_Eta25 p<5GeV":
        "isNotElectron & isLong & (MCETA>2.0) & (MCETA<5.0)",
        "08_notElectron_Long_Eta25 p>3GeV pt>400MeV":
        "isNotElectron & isLong & (MCETA>2.0) & (MCETA<5.0)",
        "09_notElectron_Long_FromB_Eta25":
        "isNotElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0)",
        "10_notElectron_Long_FromB_Eta25 p>5GeV":
        "isNotElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
        "11_notElectron_Long_FromB_Eta25 p<5GeV":
        "isNotElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
        "12_notElectron_Long_FromB_Eta25 p>3GeV pt>400MeV":
        "isNotElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
        "13_notElectron_Long_FromD_Eta25":
        "isNotElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0)",
        "14_notElectron_Long_FromD_Eta25 p>5GeV":
        "isNotElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
        "15_notElectron_Long_FromD_Eta25 p<5GeV":
        "isNotElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
        "16_notElectron_Long_FromD_Eta25 p>3GeV pt>400MeV":
        "isNotElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
        "17_notElectron_Long_strange_Eta25":
        "isNotElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0)",
        "18_notElectron_Long_strange_Eta25 p>5GeV":
        "isNotElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
        "19_notElectron_Long_strange_Eta25 p<5GeV":
        "isNotElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
        "20_notElectron_Long_strange_Eta25 p>3GeV pt>400MeV":
        "isNotElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
        "21_Electron_Velo":
        "isElectron & isVelo",
        "22_Electron_Velo_Forward":
        "isElectron & isVelo & (MCETA>0)",
        "23_Electron_Velo_Backward":
        "isElectron & isVelo & (MCETA<0)",
        "24_Electron_Velo_Eta25":
        "isElectron & isVelo & (MCETA>2.0) & (MCETA<5.0)",
        "25_Electron_Long_Eta25":
        "isElectron & isLong & (MCETA>2.0) & (MCETA<5.0)",
        "26_Electron_Long_Eta25 p>5GeV":
        "isElectron & isLong & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
        "27_Electron_Long_Eta25 p<5GeV":
        "isElectron & isLong & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
        "28_Electron_Long_Eta25 p>3GeV pt>400MeV":
        "isElectron & isLong & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
        "29_Electron_Long_FromB_Eta25":
        "isElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0)",
        "30_Electron_Long_FromB_Eta25 p>5GeV":
        "isElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
        "31_Electron_Long_FromB_Eta25 p<5GeV":
        "isElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
        "32_Electron_Long_FromB_Eta25 p>3GeV pt>400MeV":
        "isElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
        "33_Electron_Long_FromD_Eta25":
        "isElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0)",
        "34_Electron_Long_FromD_Eta25 p>5GeV":
        "isElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
        "35_Electron_Long_FromD_Eta25 p<5GeV":
        "isElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
        "36_Electron_Long_FromD_Eta25 p>3GeV pt>400MeV":
        "isElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
        "37_Electron_Long_strange_Eta25":
        "isElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0)",
        "38_Electron_Long_strange_Eta25 p>5GeV":
        "isElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
        "39_Electron_Long_strange_Eta25 p<5GeV":
        "isElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
        "40_Electron_Long_strange_Eta25 p>3GeV pt>400MeV":
        "isElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)"
    },
    "Forward": {
        "01_long":
        "isLong",
        "02_long_P>5GeV":
        "isLong & over5",
        "03_long_strange":
        "isLong & strange",
        "04_long_strange_P>5GeV":
        "isLong & strange & over5",
        "05_long_fromB":
        "isLong & fromB",
        "05_long_fromD":
        "isLong & fromD",
        "06_long_fromB_P>5GeV":
        "isLong & fromB & over5",
        "06_long_fromD_P>5GeV":
        "isLong & fromD & over5",
        "07_long_electrons":
        "isLong & isElectron",
        "08_long_fromB_electrons":
        "isLong & isElectron & fromB",
        "09_long_fromB_electrons_P>5GeV":
        "isLong & isElectron & over5 & fromB",
        "10_long_fromB_P>3GeV_Pt>0.5GeV":
        "isLong & fromB & trigger",
        "10_long_fromB_electrons_P>3GeV_Pt>0.5GeV":
        "isLong & fromB & trigger & isElectron",
        "10_long_strange_P>3GeV_Pt>0.5GeV":
        "isLong & strange & trigger",
        "10_long_fromD_P>3GeV_Pt>0.5GeV":
        "isLong & fromD & trigger",
        "11_UT_long_fromB_P>3GeV_Pt>0.5GeV":
        "isLong & fromB & trigger & isUT",
    },
    "Upstream": {
        "01_velo":
        "isVelo",
        "02_velo+UT":
        "isVelo & isUT",
        "03_velo+UT_P>5GeV":
        "isVelo & isUT & over5",
        "04_velo+notLong":
        "isNotLong & isVelo ",
        "05_velo+UT+notLong":
        "isNotLong & isVelo & isUT",
        "06_velo+UT+notLong_P>5GeV":
        "isNotLong & isVelo & isUT & over5",
        "07_long":
        "isLong",
        "07_long_strange":
        "isLong & strange",
        "08_long_P>5GeV":
        "isLong & over5 ",
        "08_long_strange_P>5GeV":
        "isLong & over5  & strange",
        "09_long_fromB":
        "isLong & fromB",
        "09_long_fromD":
        "isLong & fromD",
        "10_long_fromB_P>5GeV":
        "isLong & fromB & over5",
        "10_long_fromD_P>5GeV":
        "isLong & fromD & over5",
        "11_long_electrons":
        "isLong & isElectron",
        "12_long_fromB_electrons":
        "isLong & isElectron & fromB",
        "13_long_fromB_electrons_P>5GeV":
        "isLong & isElectron & over5 & fromB",
        "14_long_fromB_P>3GeV_Pt>0.5GeV":
        "isLong & fromB & trigger",
        "14_long_fromB_electrons_P>3GeV_Pt>0.5GeV":
        "isLong & fromB & isElectron & trigger",
        "14_long_strange_P>3GeV_Pt>0.5GeV":
        "isLong & strange& trigger",
        "14_long_fromD_P>3GeV_Pt>0.5GeV":
        "isLong & fromD & trigger",
        "15_UT_long_fromB_P>3GeV_Pt>0.5GeV":
        "isLong & fromB & trigger & isUT",
    },
    "UpstreamForMuons": {
        "01_long_muon": "isLong & isMuon",
        "02_long_muon_from_strange": "isLong & strange & isMuon",
        "03_long_pion": "isLong & isPion",
    },
    "MuonMatch": {
        "01_long":
        "isLong",
        "02_long_muon":
        "isLong & isMuon",
        "02_long_muon_P>3GeV_Pt>0.5GeV":
        "isLong & isMuon & (MCPT>500) & (MCP>3000)",
        "03_long_muon_from_strange":
        "isLong & strange & isMuon",
        "03_long_muon_strange_P>3GeV_Pt>0.5GeV":
        "isLong & isMuon & (MCPT>500) & (MCP>3000) & strange",
        "04_long_pion":
        "isLong & isPion",
        "04_long_pion_P>3GeV_Pt>0.5GeV":
        "isLong & isPion & (MCPT>500) & (MCP>3000)",
    },
    "MuonID": {
        "01_long": "isLong",
        "02_long_muon": "isLong & isMuon",
        "03_long_muon_from_strange": "isLong & strange & isMuon",
        "04_long_pion": "isLong & isPion",
        "05_long+muonHits": "isLong & muonHitsInAtLeastTwoStations ",
        "06_long+muonHits_muon":
        "isLong & isMuon & muonHitsInAtLeastTwoStations"
    },
    "Seed": {
        "01_hasT":
        "isSeed ",
        "02_long":
        "isLong",
        "03_long_P>5GeV":
        "isLong & over5",
        "04_long_fromB":
        "isLong & fromB",
        "05_long_fromB_P>5GeV":
        "isLong & fromB & over5",
        "06_UT+T_strange":
        "strange & isDown",
        "07_UT+T_strange_P>5GeV":
        "strange & isDown & over5",
        "08_noVelo+UT+T_strange":
        "strange & isDown & isNotVelo",
        "09_noVelo+UT+T_strange_P>5GeV":
        "strange & isDown & over5 & isNotVelo",
        "10_UT+T_SfromDB":
        "strange & isDown & ( BOrDMother)",
        "11_UT+T_SfromDB_P>5GeV":
        "strange & isDown & over5 & ( BOrDMother)",
        "12_noVelo+UT+T_SfromDB_P>5GeV":
        "strange & isDown & isNotVelo & over5 & ( BOrDMother)",
        "13_hasT_electrons":
        "isElectron & isSeed",
        "14_long_electrons":
        "isElectron & isLong",
        "15_long_fromB_electrons":
        "isElectron & isLong & fromB",
        "16_long_electrons_P>5GeV":
        "isElectron & isLong & over5",
        "17_long_fromB_electrons_P>5GeV":
        "isElectron & isLong & over5 & fromB",
    },
    "Downstream": {
        "01_UT+T":
        "isDown ",
        "02_UT+T_P>5GeV":
        "isDown & over5",
        "03_UT+T_strange":
        " strange & isDown",
        "04_UT+T_strange_P>5GeV":
        " strange & isDown & over5",
        "05_noVelo+UT+T_strange":
        " strange & isDown & isNotVelo",
        "06_noVelo+UT+T_strange_P>5GeV":
        " strange & isDown & over5 & isNotVelo",
        "07_UT+T_fromDB":
        "isDown & (BOrDMother)",
        "08_UT+T_fromBD_P>5GeV":
        "isDown & (BOrDMother) & over5",
        "09_noVelo+UT+T_fromBD":
        "isDown & (BOrDMother) & isNotVelo",
        "10_noVelo+UT+T_fromBD_P>5GeV":
        "isDown &( BOrDMother) & over5 & isNotVelo",
        "11_UT+T_SfromDB":
        " strange & isDown & ( BOrDMother)",
        "12_UT+T_SfromDB_P>5GeV":
        " strange & isDown & over5 & ( BOrDMother)",
        "13_noVelo+UT+T_SfromDB":
        " strange & isDown & isNotVelo & ( BOrDMother)",
        "14_noVelo+UT+T_SfromDB_P>5GeV":
        " strange & isDown & isNotVelo & over5 & ( BOrDMother) ",
    },
    "ForwardUTHits": {
        "01_long": "isLong",
        "02_long_P>5GeV": "isLong & over5",
        "03_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "04_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "DownstreamUTHits": {
        "01_has seed": "isSeed",
        "02_has seed +noVelo, T+UT": "isSeed & isNotVelo & isDown",
        "03_down+strange": "strange & isDown",
        "04_down+strange+P>5GeV": "strange & isDown & over5",
        "05_pi<-Ks<-B": "fromKsFromB",
        "06_pi<-Ks<-B+P>5GeV": "fromKsFromB & over5",
    },
    "CaloElectrons": {
        "01_long": "isLong",
        "02_long_P>5GeV": "isLong & over5",
        "03_long_electron": "isLong & isElectron",
        "04_long_electron_P>5GeV": "isLong & isElectron & over5",
        "05_long_fromB_electrons": "isLong & isElectron & fromB",
        "06_long_fromB_electrons_P>5GeV":
        "isLong & isElectron & over5 & fromB",
        "07_long_fromD_electrons": "isLong & isElectron & fromD",
        "08_long_fromD_electrons_P>5GeV":
        "isLong & isElectron & over5 & fromD",
        "09_long_notElectron": "isLong & isNotElectron",
        "10_long_notElectron_P>5GeV": "isLong & isNotElectron & over5",
    },
}
categories["ForwardHlt1"] = categories["Forward"]
categories["Match"] = categories["Forward"]
categories["MatchUTHits"] = categories["ForwardUTHits"]
categories["Best"] = categories["Forward"]
categories["BestLong"] = categories["Forward"]
categories["BestDownstream"] = categories["Downstream"]


def get_mc_categories(key, etaRange25=True):
    cuts = categories.get(key, {})
    if etaRange25:
        cuts = {
            name: cut + " & (MCETA > 2.0) & (MCETA < 5.0)"
            for name, cut in cuts.items()
        }
    return cuts


def get_hit_type_mask(track_type):
    """  The enum HitType in PrTrackCounter.h is used to set which hit types to check.
    Here the enum is mirrored in the dict hit_type to give the number a name.
    """

    hit_types_to_check = {
        "Velo": ["VP"],
        "Upstream": ["UT"],
        "Forward": ["FT"],
        "MuonMatch": ["UT"],
        "Downstream": ["UT"],
        "Seed": ["FT"],
        "Best": ["VP", "UT", "FT"],
        "BestLong": ["VP", "UT", "FT"],
        "BestDownstream": ["UT", "FT"],
        "CaloElectrons": [],
    }
    hit_types_to_check["ForwardHlt1"] = hit_types_to_check["Forward"]
    hit_types_to_check["Match"] = hit_types_to_check["Forward"]
    hit_types_to_check["VeloFull"] = hit_types_to_check["Velo"]

    hit_type = {"VP": 3, "UT": 4, "FT": 8}
    dets = hit_types_to_check[track_type]
    mask = 0
    for det in dets:
        if det not in hit_type:
            print(
                "WARNING: Hit type to check unknown. Ignoring hit type, counting all."
            )
            return 0
        mask += hit_type[det]

    return mask
