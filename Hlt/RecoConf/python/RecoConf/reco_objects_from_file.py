###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from .data_from_file import reco_unpackers
from PyConf.Algorithms import FakeRecSummaryMaker
from PyConf.packing import persisted_location


def upfront_reconstruction():
    """Return a list DataHandles that define the upfront reconstruction output.

    This differs from `reconstruction` as it should not be used as inputs to
    other algorithms, but only to define the control flow, i.e. the return
    value of this function should be run before all HLT2 lines.

    """
    return list(reco_unpackers().values())


def reconstruction():
    """Return a {name: DataHandle} dict that define the reconstruction output."""

    ### Temporary: as long as we persist v1, we need to insert a converter for the new PVs
    m = {k: v.OutputName for k, v in reco_unpackers().items()}
    from PyConf.Algorithms import RecV1ToPVConverter
    m["PVs_v1"] = m["PVs"]
    m["PVs"] = RecV1ToPVConverter(InputVertices=m["PVs_v1"]).OutputVertices
    ### Temporary: this is to be able to read and process data with an unpacked RecSummary
    if "RecSummary" not in m.keys():
        m["RecSummary"] = FakeRecSummaryMaker(
            outputs={
                "Output": persisted_location('RecSummary')
            }).Output
    return m


def make_charged_protoparticles():
    return reconstruction()['ChargedProtos']


def make_neutral_protoparticles():
    return reconstruction()['NeutralProtos']


def get_IsPhoton_table():
    """
    Return a DataHandle to the Relation table for IsPhoton.
    """

    return reconstruction()["IsPhoton"]


def get_IsNotH_table():
    """
    Return a DataHandle to the Relation table for IsNotH.
    """

    return reconstruction()["IsNotH"]


def get_ClusterMass_table():
    """
    Return a DataHandle to the Relation table for ClusterMass.
    """

    return reconstruction()["ClusterMass"]


def get_CaloClusterCode_table():
    """
    Return a DataHandle to the Relation table for CaloClusterCode.
    """

    return reconstruction()["CaloClusterCode"]


def make_pvs():
    return reconstruction()['PVs']


def make_tracks():
    return reconstruction()['Tracks']
