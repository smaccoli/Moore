###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.application import default_raw_banks
from PyConf.Algorithms import (
    MuonRawToHits,
    MuonRawInUpgradeToHits,
    MuonIDHlt1Alg,
    MakeZip__BestTracks__MuonPIDs__v2,
)
from RecoConf.hlt1_tracking import make_hlt1_fitted_tracks, make_hlt1_tracks


@configurable
def make_muon_hits(geometry_version=2):

    if geometry_version == 2: raw_to_hits = MuonRawToHits
    elif geometry_version == 3: raw_to_hits = MuonRawInUpgradeToHits
    else:
        raise ValueError("Unsupported muon decoding version")

    return raw_to_hits(RawBanks=default_raw_banks("Muon")).HitContainer


@configurable
def make_muon_id(tracks, make_muon_hits=make_muon_hits):
    return MuonIDHlt1Alg(
        runMVA=False, InputTracks=tracks["Pr"],
        InputMuonHits=make_muon_hits()).OutputMuonPID


def make_tracks_with_muon_id(tracks, muon_ids):
    return MakeZip__BestTracks__MuonPIDs__v2(
        Input1=tracks["Pr"], Input2=muon_ids).Output


def make_fitted_tracks_with_muon_id():
    tracks = make_hlt1_fitted_tracks(make_hlt1_tracks())
    muon_ids = make_muon_id(make_hlt1_tracks()["Forward"])
    tracks_with_muon_id = make_tracks_with_muon_id(tracks, muon_ids)
    return {'PrFittedForwardWithMuonID': tracks_with_muon_id}
