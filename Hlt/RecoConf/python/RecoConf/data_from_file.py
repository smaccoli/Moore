###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Load data from files and set up unpackers.

There are two things we have to deal with:

1. Loading the data from the file in to the TES, done by
   Gaudi::Hive::FetchDataFromFile.
2. Unpacking and preparing packed containers, if the 'reconstruction' is
   defined as the objects already present in the file.

In most LHCb applications, step 2 is done for you behind the scenes. The
DataOnDemandSvc is configured in LHCb/GaudiConf/DstConf.py to unpack containers
when they are requested. It also configures adding RICH, MUON, and combined PID
information to ProtoParticles when they're unpacked. Because we don't have the
DataOnDemandSvc here, we have to do this by hand.

The interesting 'user-facing' exports of this module are:

* [reco,mc]_from_file(): Dict of names to locations that can be loaded from a file.
* [reco,mc]_unpackers(): Dict from unpacked object name to Algorithm that produces a
  container of those objects.
"""
import collections

#####
# New unpackers can not unpack old dst files from brunel
# so here we are using old unpackers which unpack from packed objects
# while new unpackers will unpack from packed data buffers
#####
from Gaudi.Configuration import ERROR
from Configurables import (UnpackMuonPIDs, UnpackRichPIDs, UnpackCaloHypo,
                           UnpackProtoParticle, UnpackRecVertex, UnpackTrack,
                           UnpackMCParticle, UnpackMCVertex, MCVPHitUnpacker,
                           MCUTHitUnpacker, MCFTHitUnpacker, MCRichHitUnpacker,
                           MCEcalHitUnpacker, MCHcalHitUnpacker,
                           MCMuonHitUnpacker, MCRichDigitSummaryUnpacker)

from PyConf.components import Algorithm, force_location
from PyConf.application import make_data_with_FetchDataFromFile
from PyConf.Tools import (ChargedProtoParticleAddRichInfo,
                          ChargedProtoParticleAddMuonInfo,
                          ChargedProtoParticleAddCombineDLLs)


def _packed_reco_from_file():
    return {
        'PackedPVs': '/Event/pRec/Vertex/Primary',
        'PackedCaloElectrons': '/Event/pRec/Calo/Electrons',
        'PackedCaloPhotons': '/Event/pRec/Calo/Photons',
        'PackedCaloMergedPi0s': '/Event/pRec/Calo/MergedPi0s',
        'PackedCaloSplitPhotons': '/Event/pRec/Calo/SplitPhotons',
        'PackedMuonPIDs': '/Event/pRec/Muon/MuonPID',
        'PackedRichPIDs': '/Event/pRec/Rich/PIDs',
        'PackedTracks': '/Event/pRec/Track/Best',
        'PackedNeutralProtos': '/Event/pRec/ProtoP/Neutrals',
        'PackedChargedProtos': '/Event/pRec/ProtoP/Charged',
    }


def _packed_mc_from_file():
    return {
        'PackedMCParticles': '/Event/pSim/MCParticles',
        'PackedMCVertices': '/Event/pSim/MCVertices',
        'PackedMCVPHits': '/Event/pSim/VP/Hits',
        'PackedMCUTHits': '/Event/pSim/UT/Hits',
        'PackedMCFTHits': '/Event/pSim/FT/Hits',
        'PackedMCRichHits': '/Event/pSim/Rich/Hits',
        'PackedMCEcalHits': '/Event/pSim/Ecal/Hits',
        'PackedMCHcalHits': '/Event/pSim/Hcal/Hits',
        'PackedMCMuonHits': '/Event/pSim/Muon/Hits',
        'PackedMCRichDigitSummaries': '/Event/pSim/Rich/DigitSummaries',
    }


def unpacked_mc_from_file():
    return {
        'MCParticles': '/Event/Sim/MCParticles',
        'MCVertices': '/Event/Sim/MCVertices',
        'MCVPHits': '/Event/Sim/VP/Hits',
        'MCUTHits': '/Event/Sim/UT/Hits',
        'MCFTHits': '/Event/Sim/FT/Hits',
        'MCRichHits': '/Event/Sim/Rich/Hits',
        'MCEcalHits': '/Event/Sim/Ecal/Hits',
        'MCHcalHits': '/Event/Sim/Hcal/Hits',
        'MCMuonHits': '/Event/Sim/Muon/Hits',
        'CRichDigitSummaries': '/Event/Sim/Rich/DigitSummaries',
    }


def unpacked_reco_locations():
    # If the structure is not like this, pointers point to to the wrong place...
    # The SmartRefs held by the unpacked MC objects only work if we unpack to these specific locations
    locations = {}
    for k, v in _packed_reco_from_file().items():
        if 'pRec' in v:
            locations[k] = v.replace('pRec', 'Rec')
        elif 'pHLT2' in v:  #sprucing picks them from HLT2
            locations[k] = v.replace('pHLT2', 'HLT2')

    return locations


def unpacked_mc_locations():
    # If the structure is not like this, pointers point to to the wrong place...
    # The SmartRefs held by the unpacked MC objects only work if we unpack to these specific locations
    return {
        'PackedMCParticles': '/Event/MC/Particles',
        'PackedMCVertices': '/Event/MC/Vertices',
        'PackedMCVPHits': '/Event/MC/VP/Hits',
        'PackedMCUTHits': '/Event/MC/UT/Hits',
        'PackedMCFTHits': '/Event/MC/FT/Hits',
        'PackedMCRichHits': '/Event/MC/Rich/Hits',
        'PackedMCEcalHits': '/Event/MC/Ecal/Hits',
        'PackedMCHcalHits': '/Event/MC/Hcal/Hits',
        'PackedMCMuonHits': '/Event/MC/Muon/Hits',
        'PackedMCRichDigitSummaries': '/Event/MC/Rich/DigitSummaries',
    }


def reco_from_file():
    # TODO(AP) should only add the packed data if we're running on Upgrade MC
    # where Brunel has already been run
    packed_data = _packed_reco_from_file()
    # raw_event = raw_event_from_file()
    # We don't want any keys accidentally overwriting each other
    # assert set(packed_data.keys()).intersection(set(raw_event.keys())) == set()
    # return dict(list(packed_data.items()) + list(raw_event.items()))
    return packed_data


def mc_from_file():
    # TODO(AP) should only add the packed data if we're running on Upgrade MC
    # where Brunel has already been run
    packed_data = _packed_mc_from_file()
    return packed_data


def reco_unpacker(key, configurable, name, **kwargs):
    """Return unpacker that reads from file and unpacks to a forced output location."""
    packed_loc = reco_from_file()[key]
    unpacked_loc = unpacked_reco_locations()[key]
    alg = Algorithm(
        configurable,
        name=name,
        outputs={'OutputName': force_location(unpacked_loc)},
        InputName=make_data_with_FetchDataFromFile(packed_loc),
        **kwargs)
    return alg


def mc_unpacker(key, configurable, name, **kwargs):
    """Return unpacker that reads from file and unpacks to a forced output location."""
    packed_loc = mc_from_file()[key]
    unpacked_loc = unpacked_mc_locations()[key]
    alg = Algorithm(
        configurable,
        name=name,
        outputs={'OutputName': force_location(unpacked_loc)},
        InputName=make_data_with_FetchDataFromFile(packed_loc),
        **kwargs)
    return alg


def make_mc_track_info():
    mc_track_info = make_data_with_FetchDataFromFile('/Event/MC/TrackInfo')
    mc_track_info.force_type('LHCb::MCProperty')
    return mc_track_info


def reco_unpackers():
    muonPIDs = reco_unpacker('PackedMuonPIDs', UnpackMuonPIDs,
                             'UnpackMuonPIDs')
    richPIDs = reco_unpacker(
        'PackedRichPIDs', UnpackRichPIDs, 'UnpackRichPIDs', OutputLevel=ERROR)
    # The OutputLevel above suppresses the following useless warnings (plus more?)
    # WARNING DataPacking::Unpack<LHCb::RichPIDPacker>:: Incorrect data version 0 for packing version > 3. Correcting data to version 2.

    # Ordered so that dependents are unpacked first
    d = collections.OrderedDict([
        ('PVs', reco_unpacker('PackedPVs', UnpackRecVertex,
                              'UnpackRecVertices')),
        ('CaloElectrons',
         reco_unpacker('PackedCaloElectrons', UnpackCaloHypo,
                       'UnpackCaloElectrons')),
        ('CaloPhotons',
         reco_unpacker('PackedCaloPhotons', UnpackCaloHypo,
                       'UnpackCaloPhotons')),
        ('CaloMergedPi0s',
         reco_unpacker('PackedCaloMergedPi0s', UnpackCaloHypo,
                       'UnpackCaloMergedPi0s')),
        ('CaloSplitPhotons',
         reco_unpacker('PackedCaloSplitPhotons', UnpackCaloHypo,
                       'UnpackCaloSplitPhotons')),
        ('MuonPIDs', muonPIDs),
        ('RichPIDs', richPIDs),
        ('Tracks',
         reco_unpacker('PackedTracks', UnpackTrack, 'UnpackBestTracks')),
        ('NeutralProtos',
         reco_unpacker('PackedNeutralProtos', UnpackProtoParticle,
                       'UnpackNeutralProtos')),
        ('ChargedProtos',
         reco_unpacker(
             'PackedChargedProtos',
             UnpackProtoParticle,
             'UnpackChargedProtos',
             AddInfo=[
                 ChargedProtoParticleAddRichInfo(
                     InputRichPIDLocation=richPIDs.OutputName),
                 ChargedProtoParticleAddMuonInfo(
                     InputMuonPIDLocation=muonPIDs.OutputName),
                 ChargedProtoParticleAddCombineDLLs()
             ])),
    ])

    # Make sure we have consistent names, and that we're unpacking everything
    # we load from the file
    assert set(['Packed' + k for k in d.keys()]) - set(
        _packed_reco_from_file().keys()) == set()

    return d


def mc_unpackers():
    # Ordered so that dependents are unpacked first
    mc_vertices = mc_unpacker('PackedMCVertices', UnpackMCVertex,
                              'MCVerticesUnpacker')
    # Make sure that MC particles and MC vertices are unpacked together,
    # see https://gitlab.cern.ch/lhcb/LHCb/issues/57 for details.
    mc_particles = mc_unpacker(
        'PackedMCParticles',
        UnpackMCParticle,
        'UnpackMCParticles',
        ExtraInputs=[mc_vertices])

    mc_vp_hits = mc_unpacker('PackedMCVPHits', MCVPHitUnpacker,
                             'UnpackMCVPHits')
    mc_ut_hits = mc_unpacker('PackedMCUTHits', MCUTHitUnpacker,
                             'UnpackMCUTHits')
    mc_ft_hits = mc_unpacker('PackedMCFTHits', MCFTHitUnpacker,
                             'UnpackMCFTHits')
    mc_rich_hits = mc_unpacker('PackedMCRichHits', MCRichHitUnpacker,
                               'UnpackMCRichHits')
    mc_ecal_hits = mc_unpacker('PackedMCEcalHits', MCEcalHitUnpacker,
                               'UnpackMCEcalHits')
    mc_hcal_hits = mc_unpacker('PackedMCHcalHits', MCHcalHitUnpacker,
                               'UnpackMCHcalHits')
    mc_muon_hits = mc_unpacker('PackedMCMuonHits', MCMuonHitUnpacker,
                               'UnpackMCMuonHits')

    # RICH Digit summaries
    mc_rich_digit_sums = mc_unpacker('PackedMCRichDigitSummaries',
                                     MCRichDigitSummaryUnpacker,
                                     "RichSumUnPack")

    d = collections.OrderedDict([
        ('MCRichDigitSummaries', mc_rich_digit_sums),
        ('MCParticles', mc_particles),
        ('MCVertices', mc_vertices),
        ('MCVPHits', mc_vp_hits),
        ('MCUTHits', mc_ut_hits),
        ('MCFTHits', mc_ft_hits),
        ('MCRichHits', mc_rich_hits),
        ('MCEcalHits', mc_ecal_hits),
        ('MCHcalHits', mc_hcal_hits),
        ('MCMuonHits', mc_muon_hits),
    ])

    # Make sure we have consistent names, and that we're unpacking everything
    # we load from the file
    assert set(['Packed' + k for k in d.keys()]) - set(
        _packed_mc_from_file().keys()) == set()

    return d


def boole_links_digits_mcparticles():
    """Return a dict of locations for MC linker tables (to mcparticles) created by Boole."""
    locations = {
        "EcalDigitsV1": "/Event/Link/Raw/Ecal/Digits",
        "EcalDigits": "/Event/Link/Raw/Ecal/Digits2MCParticles",
        "FTLiteClusters": "/Event/Link/Raw/FT/LiteClusters",
        "HcalDigitsV1": "/Event/Link/Raw/Hcal/Digits",
        "HcalDigits": "/Event/Link/Raw/Hcal/Digits2MCParticles",
        "MuonDigits": "/Event/Link/Raw/Muon/Digits",
        "UTClusters": "/Event/Link/Raw/UT/Clusters",
        "UTDigits": "/Event/Link/Raw/UT/TightDigits",
        "VPDigits": "/Event/Link/Raw/VP/Digits",
    }
    return {
        key: make_data_with_FetchDataFromFile(loc)
        for key, loc in locations.items()
    }


def boole_links_digits_mchits():
    """Return a dict of locations for MC linker tables (to mchits) created by Boole.

    These locations are only propagated out of Boole for eXtendend DIGI and DST types.
    """
    locations = {
        "FTLiteClusters": "/Event/Link/Raw/FT/LiteClusters2MCHits",
        "UTClusters": "/Event/Link/Raw/UT/Clusters2MCHits",
        "UTDigits": "/Event/Link/Raw/UT/TightDigits2MCHits",
        "VPDigits": "/Event/Link/Raw/VP/Digits2MCHits",
    }
    return {
        key: make_data_with_FetchDataFromFile(loc)
        for key, loc in locations.items()
    }


def brunel_links():
    """Return a dict of locations for MC linker tables created by Brunel."""
    locations = {
        "CaloElectrons": "/Event/Link/Rec/Calo/Electrons",
        "CaloMergedPi0s": "/Event/Link/Rec/Calo/MergedPi0s",
        "CaloPhotons": "/Event/Link/Rec/Calo/Photons",
        "CaloSplitPhotons": "/Event/Link/Rec/Calo/SplitPhotons",
        "Tracks": "/Event/Link/Rec/Track/Best",
    }
    return {
        key: make_data_with_FetchDataFromFile(loc)
        for key, loc in locations.items()
    }
