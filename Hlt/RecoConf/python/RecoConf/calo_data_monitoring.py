###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable
from PyConf.application import make_odin
from PyConf.Algorithms import (
    CaloChargedPIDsMonitor, CaloFutureClusterMonitor, CaloFutureDigitMonitor,
    CaloFutureTimeAlignment, CaloTAEData, CaloFuturePedestal)


@configurable
def monitor_calo_clusters(calo, split_clusters=True):
    nodes = (CaloFutureClusterMonitor(
        name="ClusterMonitorEcalClusters",
        Input=calo["ecalClusters"],
        HistoMultiplicityMax=400,
        HistoEnergyMax=50. * GeV,
        HistoEtMax=5. * GeV,
        HistoSizeMax=30,
    ), )
    if split_clusters:
        nodes += (CaloFutureClusterMonitor(
            name="ClusterMonitorEcalSplitClusters",
            Input=calo["ecalSplitClusters"],
            HistoMultiplicityMax=50,
            HistoEnergyMax=100. * GeV,
            HistoEtMax=10. * GeV,
        ), )
    return nodes


@configurable
def monitor_calo_digits(calo,
                        adcFilterEcal=-100,
                        adcFilterHcal=-100,
                        eFilter=-9999.,
                        etFilter=-9999.,
                        histoAdcMin=-100.5,
                        histoAdcMax=4095.5,
                        histoAdcBin=4196,
                        HistoMultiplicityMaxEcal=6000,
                        HistoMultiplicityMaxHCal=4000,
                        Calib_BXIDsA=[],
                        Calib_BXIDsB=[],
                        perBXIDlist=[],
                        spectrum=False):
    ecal_digit_moni = CaloFutureDigitMonitor(
        name="DigitMonitorEcal",
        Input=calo["digitsEcal"],
        ODINLocation=make_odin(),
        HistoMultiplicityMax=HistoMultiplicityMaxEcal,
        HistoEtMax=5. * GeV,
        HistoSizeMax=30,
        HistoAdcMin=histoAdcMin,
        HistoAdcMax=histoAdcMax,
        HistoAdcBin=histoAdcBin,
        HistoXMin=-3900.8,
        HistoXMax=3900.8,
        HistoXBin=64,
        HistoYMin=-3169.4,
        HistoYMax=3169.4,
        HistoYBin=52,
        Calib_BXIDsA=Calib_BXIDsA,
        Calib_BXIDsB=Calib_BXIDsB,
        perBXIDlist=perBXIDlist,
        ADCFilter=adcFilterEcal,
        EnergyFilter=eFilter,
        EtFilter=etFilter,
        Spectrum=spectrum)
    hcal_digit_moni = CaloFutureDigitMonitor(
        name="DigitMonitorHcal",
        Input=calo["digitsHcal"],
        ODINLocation=make_odin(),
        HistoMultiplicityMax=HistoMultiplicityMaxHCal,
        HistoEtMax=5. * GeV,
        HistoSizeMax=30,
        HistoAdcMin=histoAdcMin,
        HistoAdcMax=histoAdcMax,
        HistoAdcBin=histoAdcBin,
        HistoXMin=-4201.6,
        HistoXMax=4201.6,
        HistoXBin=32,
        HistoYMin=-3413.8,
        HistoYMax=3413.8,
        HistoYBin=26,
        Calib_BXIDsA=Calib_BXIDsA,
        Calib_BXIDsB=Calib_BXIDsB,
        perBXIDlist=perBXIDlist,
        ADCFilter=adcFilterHcal,
        EnergyFilter=eFilter,
        EtFilter=etFilter,
        Spectrum=spectrum)
    return ecal_digit_moni, hcal_digit_moni


@configurable
def monitor_calo_time_alignment(calo,
                                adcFilterEcal=-100,
                                adcFilterHcal=-100,
                                histoEnergyMax=250. * GeV,
                                histoAdcMax=4000,
                                taHistoYBin=100,
                                TAE_BXIDs=[1000, 2000],
                                spectrum=False):
    ecal_ta_moni = CaloFutureTimeAlignment(
        name="CaloFutureECALTimeAlignment",
        Input=calo["digitsEcal"],
        ODINLocation=make_odin(),
        ADCFilter=adcFilterEcal,
        HistoEnergyMax=histoEnergyMax,
        HistoAdcMax=histoAdcMax,
        TAHistoYBin=taHistoYBin,
        TAE_BXIDs=TAE_BXIDs,
        Spectrum=spectrum)
    hcal_ta_moni = CaloFutureTimeAlignment(
        name="CaloFutureHCALTimeAlignment",
        Input=calo["digitsHcal"],
        ODINLocation=make_odin(),
        ADCFilter=adcFilterHcal,
        TAHistoYBin=taHistoYBin,
        TAE_BXIDs=TAE_BXIDs,
        Spectrum=spectrum)
    tae_data = CaloTAEData(
        name="CaloTAEData", Input=calo["digitsEcal"], ODINLocation=make_odin())
    return ecal_ta_moni, hcal_ta_moni, tae_data


@configurable
def monitor_calo_pedestal(calo,
                          ecal_name="CaloFutureECALPedestal",
                          hcal_name="CaloFutureHCALPedestal",
                          adcFilterEcal=-100,
                          adcFilterHcal=-100,
                          histoAdcMin=-100.5,
                          histoAdcMax=100.5,
                          histoAdcBin=201,
                          spectrum=False):
    ecal_pedestal_moni = CaloFuturePedestal(
        name=ecal_name,
        Input=calo["digitsEcal"],
        ODINLocation=make_odin(),
        ADCFilter=adcFilterEcal,
        HistoAdcMin=histoAdcMin,
        HistoAdcMax=histoAdcMax,
        HistoAdcBin=histoAdcBin,
        Spectrum=spectrum,
        Profile=True,
        ProfileError="s"  # only applied to 1D Profiles
    )
    hcal_pedestal_moni = CaloFuturePedestal(
        name=hcal_name,
        Input=calo["digitsHcal"],
        ODINLocation=make_odin(),
        ADCFilter=adcFilterHcal,
        HistoAdcMin=histoAdcMin,
        HistoAdcMax=histoAdcMax,
        HistoAdcBin=histoAdcBin,
        Spectrum=spectrum,
        Profile=True,
        ProfileError="s"  # only applied to 1D Profiles
    )
    return ecal_pedestal_moni, hcal_pedestal_moni


@configurable
def monitor_calo_charged(calo,
                         tracks,
                         name="CaloChargedPIDsMonitor",
                         accepted_track_types=["Long"]):
    cpids = calo["v2_chargedpids"]
    brems = calo["v2_breminfos"]
    track_types = list(
        set(cpids.keys()) & set(brems.keys()) & set(accepted_track_types))
    nodes = ()
    for tt in track_types:
        nodes += (CaloChargedPIDsMonitor(
            name=name + "_" + tt,
            Tracks=tracks[tt],
            ChargedPID=cpids[tt],
            BremInfo=brems[tt],
            Clusters=calo["ecalClusters"],
        ), )
    return nodes


def monitor_calo(calo, tracks=None):
    monitors = (
        monitor_calo_clusters(calo) + monitor_calo_digits(calo) +
        monitor_calo_time_alignment(calo) + monitor_calo_pedestal(calo))
    if tracks: monitors += (monitor_calo_charged(calo, tracks))
    return monitors
