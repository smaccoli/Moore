###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.packing import persisted_location

from PyConf.Algorithms import (LHCb__Converters__Track__SOA__fromV1Track as
                               TrackSOAFromV1, fromV2MuonPIDV1MuonPID,
                               MergeMuonPIDsV1, MuonPIDV2ToMuonTracks)

from .hlt2_muonid import make_muon_ids
from RecoConf.core_algorithms import make_unique_id_generator


@configurable
def make_muon_input_tracks(best_tracks,
                           light_reco=False,
                           track_types=['Long', 'Downstream']):
    tracks = dict()
    for track_type in track_types:
        tracks[track_type] = TrackSOAFromV1(
            InputTracks=best_tracks[track_type]['v1']
            if light_reco else best_tracks['v1'],
            InputUniqueIDGenerator=make_unique_id_generator(),
            RestrictToType=track_type).OutputTracks
    return tracks


@configurable
def make_all_muon_pids(tracks, track_types=['Long', 'Downstream']):
    muonPidsConfs = dict()
    # Create the confs for each Muon reco for the different track types (Long, Downstream)
    for track_type in tracks:
        if track_type in track_types:
            muonPidsConfs[track_type] = make_muon_ids(track_type,
                                                      tracks[track_type])
    return muonPidsConfs


def make_conv_muon_pids(muonPidsConfs,
                        best_tracks,
                        light_reco=False,
                        track_types=['Long', 'Downstream'],
                        output_locations=dict()):
    muon_pids_v1 = dict()
    muon_tracks = dict()
    for track_type in track_types:
        # Add muon hits of MuonPID to "muontracks"
        muon_tracks[track_type] = MuonPIDV2ToMuonTracks(
            name="MuonPIDV2ToMuonTracks_" + track_type + "_{hash}",
            InputMuonPIDs=muonPidsConfs[track_type],
            InputTracks=best_tracks[track_type]["v1"]
            if light_reco else best_tracks['v1'],
            RestrictToType=track_type,
            outputs={
                'OutputMuonTracks': output_locations.get(track_type, None)
            }).OutputMuonTracks

        # Convert to Keyed Container for ProtoParticle
        muon_pids_v1[track_type] = fromV2MuonPIDV1MuonPID(
            name="fromV2MuonPIDV1MuonPID" + track_type + "_{hash}",
            InputMuonPIDs=muonPidsConfs[track_type],
            InputMuonTracks=muon_tracks[track_type],
            InputTracks=best_tracks[track_type]["v1"]
            if light_reco else best_tracks['v1'],
            RestrictToType=track_type).OutputMuonPIDs
    return muon_pids_v1, muon_tracks


def make_merged_muon_pids(muon_pids_v1):
    # Merge them for now to be compatible with Brunel.
    merged_pids = MergeMuonPIDsV1(
        InputMuonPIDLocations=list(muon_pids_v1.values()),
        outputs={'OutputMuonPIDLocation': persisted_location('MuonPIDs')},
    )
    return merged_pids.OutputMuonPIDLocation
