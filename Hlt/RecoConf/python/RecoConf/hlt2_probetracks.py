##############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Functions to create charged and neutral protoparticles from reconstruction output."""
from PyConf import configurable
from PyConf.utilities import DISABLE_TOOL
from functools import partial
from PyConf.Algorithms import (FunctionalChargedProtoParticleMaker,
                               StandaloneMuonRec, VeloMuonBuilder,
                               MuonUTTracking, PrKalmanFilter_Downstream,
                               TrackBestTrackCreator, SeedMuonBuilder)
from RecoConf.core_algorithms import make_unique_id_generator
from PyConf.Tools import (
    TrackMasterExtrapolator,
    PrAddUTHitsTool,
    MuonMeasurementProvider,
    ChargedProtoParticleAddMuonInfo,
)
from .hlt1_muonid import make_muon_hits
from .muon_reconstruction import make_all_muon_pids, make_conv_muon_pids, make_muon_input_tracks
from .hlt1_tracking import (
    make_hlt1_tracks,
    get_track_master_fitter,
    get_global_materiallocator,
    make_PrStorePrUTHits_hits,
    make_pvs,
    get_global_measurement_provider,
    make_PrStoreSciFiHits_hits,
)
from .hlt2_tracking import (
    make_seeding_tracks, make_PrLongLivedTracking_tracks,
    make_PrHybridSeeding_tracks, get_UpgradeGhostId_tool)


@configurable
def make_charged_seed():
    """ Seedmuon tracks make from the Standalone muon tracks for tracking efficiency study """
    withmuon_provider = partial(
        get_global_measurement_provider,
        vp_provider=DISABLE_TOOL,
        ut_provider=DISABLE_TOOL,
        muon_provider=MuonMeasurementProvider,
        ignoreVP=True,
        ignoreUT=True,
        ignoreMuon=False)
    fitter = partial(
        get_track_master_fitter, get_measurement_provider=withmuon_provider)
    seedtracks = SeedMuonBuilder(
        MuonTracksLocation=StandaloneMuonRec(
            MuonHitsLocation=make_muon_hits()),
        SciFiTracksLocation=make_PrHybridSeeding_tracks()["v1"],
        Fitter=fitter())

    charged_protos = FunctionalChargedProtoParticleMaker(Inputs=[seedtracks])
    return charged_protos.Output


@configurable
def make_charged_downstream():
    """ Downstream tracks without clone killer for tracking efficiency study """
    scifi_tracks = make_seeding_tracks()
    down_tracks = make_PrLongLivedTracking_tracks(scifi_tracks)
    kf_down = PrKalmanFilter_Downstream(
        name="PrKalmanFilter_Downstream",
        MaxChi2=2.8,  ## copy from the default PrKalman fit
        MaxChi2PreOutlierRemoval=20,
        Input=down_tracks["Pr"],
        HitsUT=make_PrStorePrUTHits_hits(),
        HitsFT=make_PrStoreSciFiHits_hits(),
        ReferenceExtrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator()),
        InputUniqueIDGenerator=make_unique_id_generator())

    best_tracks = {}
    best_tracks["v1"] = TrackBestTrackCreator(
        name="BestTrackCreator_Downstream",
        TracksInContainers=[kf_down],
        MaxChi2DoF=2.8,
        GhostIdTool=get_UpgradeGhostId_tool(),
        DoNotRefit=True,
        AddGhostProb=True,
        FitTracks=False).TracksOutContainer

    muon_input_tracks = make_muon_input_tracks(
        best_tracks, light_reco=False, track_types=['Downstream'])
    muonRecConfs = make_all_muon_pids(
        tracks=muon_input_tracks, track_types=['Downstream'])
    muon_pids, muon_tracks = make_conv_muon_pids(
        muonRecConfs,
        best_tracks,
        light_reco=False,
        track_types=['Downstream'])
    addInfo = [
        ChargedProtoParticleAddMuonInfo(
            name='addMuonInfo', InputMuonPIDLocation=muon_pids['Downstream'])
    ]

    charged_protos = FunctionalChargedProtoParticleMaker(
        Inputs=[best_tracks["v1"]], AddInfo=addInfo)
    return charged_protos.Output


@configurable
def make_VeloMuon_tracks():
    """ Velomuon tracks make from the Standalone muon tracks for tracking efficiency study """
    withmuon_provider = partial(
        get_global_measurement_provider,
        ut_provider=DISABLE_TOOL,
        ft_provider=DISABLE_TOOL,
        muon_provider=MuonMeasurementProvider,
        ignoreUT=True,
        ignoreFT=True,
        ignoreMuon=False)
    fitter = partial(
        get_track_master_fitter, get_measurement_provider=withmuon_provider)
    velomuontracks = VeloMuonBuilder(
        MuonTracksLocation=StandaloneMuonRec(
            MuonHitsLocation=make_muon_hits()),
        VeloTracksLocation=make_hlt1_tracks()['Velo']['v1'],
        Fitter=fitter())

    charged_protos = FunctionalChargedProtoParticleMaker(
        Inputs=[velomuontracks])
    return charged_protos.Output


@configurable
def make_standalone_muontracks():
    '''
     Standalone muon tracks, be used to link velo tracks / add ut hits for tracking efficiency study
    '''
    # Tracks
    muon_tracks = StandaloneMuonRec(
        MuonHitsLocation=make_muon_hits(), SecondLoop=False)
    return {"v1": muon_tracks}


@configurable
def make_muonut_particles():
    """ MuonUT tracks made from Standalone muon tracks for tracking efficiency study """
    # Tracks
    muon_tracks = make_standalone_muontracks()["v1"]
    addUTtool = PrAddUTHitsTool(
        MaxChi2Tol=7.0,
        YTolSlope=40000.,
        XTolSlope=40000.,
        XTol=25,
        MinAxProj=5.5,
        MajAxProj=25,
        UTHitsLocation=make_PrStorePrUTHits_hits())

    withmuon_provider = partial(
        get_global_measurement_provider,
        vp_provider=DISABLE_TOOL,
        ft_provider=DISABLE_TOOL,
        muon_provider=MuonMeasurementProvider,
        ignoreVP=True,
        ignoreFT=True,
        ignoreMuon=False)
    fitter = partial(
        get_track_master_fitter, get_measurement_provider=withmuon_provider)
    muonut = MuonUTTracking(
        InputMuonTracks=muon_tracks,
        RecVertices=make_pvs(),
        Fitter=fitter(),
        Extrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator()),
        AddUTHitsTool=addUTtool)

    charged_protos = FunctionalChargedProtoParticleMaker(Inputs=[muonut])
    return charged_protos.Output
