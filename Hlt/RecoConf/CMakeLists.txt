###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

gaudi_install(PYTHON)

gaudi_add_tests(QMTest)

if(BINARY_TAG MATCHES "san$")
    # When running in sanitizer platforms, the tests take longer and
    # the test job gets killed at the 10h mark. Here we disable some
    # test to make the runtime more reasonable.

    # Disable mcchecking and resolution, tests and tests of examples, as
    # they are less interesting for production.
    get_property(recoconf_tests DIRECTORY PROPERTY TESTS)
    foreach(test IN ITEMS ${recoconf_tests})
        if(test MATCHES "mcchecking|efficiency|plots|examples")
            set_property(TEST ${test} PROPERTY DISABLED TRUE)
        endif()
    endforeach()

    # Disable some tests that take a long time and are redundant
    set_property(
        TEST
            RecoConf.hlt2_reco_baseline_DC
        PROPERTY DISABLED TRUE
    )
endif()

if(BUILD_TESTING AND USE_DD4HEP)
    # Disable some tests that are not yet dd4hep ready
    set_property(
        TEST
           RecoConf.examples.hlt2_reco_tracking
           RecoConf.hlt1_reco_baseline
           RecoConf.hlt1_reco_baseline_UTTELL40_with_mcchecking
           RecoConf.hlt1_reco_baseline_multi_threaded
           RecoConf.hlt1_reco_baseline_with_mcchecking
           RecoConf.hlt1_reco_baseline_with_mcchecking_FTv6
           RecoConf.hlt1_reco_muonmatching
           RecoConf.hlt1_reco_muonmatching_with_mcchecking
           RecoConf.hlt2_fast_reco
           RecoConf.hlt2_fast_reco_with_mcchecking
           RecoConf.hlt2_fastest_reco_with_mcchecking
           RecoConf.hlt2_light_reco_pr_kf_with_mcchecking
           RecoConf.hlt2_light_reco_tracking
           RecoConf.hlt2_light_reco_with_mcchecking
           RecoConf.hlt2_light_reco_without_UT_with_mcchecking
           RecoConf.hlt2_particles_baseline
           RecoConf.hlt2_protoparticles_baseline
           RecoConf.hlt2_protoparticles_fastest
           RecoConf.hlt2_protoparticles_ttrack_baseline
           RecoConf.hlt2_reco_baseline
           RecoConf.hlt2_reco_baseline_DC
           RecoConf.hlt2_reco_baseline_UTTELL40_with_mcchecking
           RecoConf.hlt2_reco_baseline_multi_threaded
           RecoConf.hlt2_reco_baseline_with_cheatedseeding
           RecoConf.hlt2_reco_baseline_with_mcchecking
           RecoConf.hlt2_reco_baseline_with_mcchecking_MagUp
           RecoConf.hlt2_reco_baseline_with_mcchecking_mpp_with_bfield
           RecoConf.hlt2_reco_baseline_with_mcchecking_no_gec
           RecoConf.hlt2_reco_baseline_with_monitoring
           RecoConf.hlt2_reco_baseline_with_parametrised_scatters
           RecoConf.hlt2_reco_upstream_particles_low_momentum
           RecoConf.hlt2_ut_filtered_forward_track_reco_with_mcchecking
           RecoConf.performance.hlt1_reco_IPresolution
           RecoConf.performance.hlt1_reco_muonIDeff
           RecoConf.performance.hlt1_reco_trackresolution
        PROPERTY
            DISABLED TRUE
    )
endif()
