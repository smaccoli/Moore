###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import Reconstruction, run_allen_reconstruction
from Moore import options

from RecoConf.calorimeter_mc_checking import check_calo_cluster_efficiency
from RecoConf.calorimeter_reconstruction import make_calo_reduced
from RecoConf.hlt1_allen import make_allen_calo_clusters, allen_gaudi_node
from RecoConf.event_filters import require_gec
from AllenConf.velo_reconstruction import decode_velo

from PyConf.Algorithms import CaloFutureClusterMonitor
from GaudiKernel.SystemOfUnits import GeV


def monitor_calo_clusters(clusters):
    ecal_clus_moni = CaloFutureClusterMonitor(
        Input=clusters,
        HistoMultiplicityMax=400,
        HistoEnergyMax=50. * GeV,
        HistoEtMax=5. * GeV,
        HistoSizeMax=30,
    )
    return ecal_clus_moni


def make_reconstruction():
    data_producers = []

    #Get Allen calo clusters
    allen_calo_clusters = make_allen_calo_clusters()
    ecal_clusters_allen = allen_calo_clusters["ecal_clusters"]

    #Get calo digits
    calo = make_calo_reduced()

    # Build CaloFutureClusterMonitor
    data_producers += [monitor_calo_clusters(ecal_clusters_allen)]

    # Build CaloClusterEfficiency
    calo["ecalClusters"] = ecal_clusters_allen  # for mc checking
    calo_clusters_efficiency = check_calo_cluster_efficiency(calo)
    data_producers += calo_clusters_efficiency

    return Reconstruction('calo_cluster_efficiency', data_producers,
                          [require_gec()])


options.evt_max = 1000
options.histo_file = "Hlt1RecoAllenCaloEfficiencyHistos.root"
options.ntuple_file = "Hlt1RecoAllenCaloEfficiencyNTuple.root"

with decode_velo.bind(retina_decoding=False), allen_gaudi_node.bind(
        sequence='hlt1_cosmics'):
    run_allen_reconstruction(options, make_reconstruction)
