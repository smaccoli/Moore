###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt1_reco_retinacluster
from RecoConf.decoders import default_ft_decoding_version

# The suffix is needed to remove a race condition between tests
suffix = '_v6' if default_ft_decoding_version() == 6 else ''
options.histo_file = "MCMatching_retina_MiniBias{}.root".format(suffix)

with standalone_hlt1_reco_retinacluster.bind(do_mc_checking=True):
    run_reconstruction(options, standalone_hlt1_reco_retinacluster)

options.histo_file = "MCMatching_retina_MiniBias.root"
