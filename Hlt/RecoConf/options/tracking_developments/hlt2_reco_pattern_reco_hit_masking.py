###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""This options file provides an example of a reconstruction in which
part of the detector is "masked" for the pattern recognition, to
avoid possible biases. """

from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_reco

# hit unbiasing for the VELO
from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD


def sensors_for_moduleid(module_id):
    return range(module_id * 4, (module_id + 1) * 4)


# wrapped in a list() to go from generator to list
disabled_sensors = list(sensors_for_moduleid( 1 )) + list(sensors_for_moduleid(2)) \
                    + list(sensors_for_moduleid( 4 )) + list(sensors_for_moduleid(7))

with make_VeloClusterTrackingSIMD.bind(masked_sensors=disabled_sensors):
    run_reconstruction(options, standalone_hlt2_reco)
