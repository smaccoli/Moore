###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.mc_checking import make_default_IdealStateCreator
from RecoConf.data_from_file import mc_unpackers, boole_links_digits_mcparticles
from RecoConf.hlt1_tracking import (
    make_PrStoreFTHit_hits,
    get_track_master_fitter,
)
from RecoConf.mc_checking import (
    make_links_lhcbids_mcparticles_tracking_system,
    make_links_tracks_mcparticles,
)
from PyConf.application import make_data_with_FetchDataFromFile
from PyConf.Algorithms import (
    PrCheatedLongTracking,
    TrackBestTrackCreator,
    PrCheatedSciFiTracking,
    fromPrSeedingTracksV1Tracks,
    LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track,
)


def run_cheated():
    links_to_hits = make_links_lhcbids_mcparticles_tracking_system()

    cheated_long_tracking = PrCheatedLongTracking(
        LHCbIdLinkLocation=links_to_hits,
        FTHitsLocation=make_PrStoreFTHit_hits(),
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesLocation=mc_unpackers()["MCVertices"],
        MCPropertyLocation=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        IdealStateCreator=make_default_IdealStateCreator(),
        AddIdealStates=True)

    cheated_long_tracks_v2 = cheated_long_tracking.OutputName
    cheated_long_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=cheated_long_tracks_v2).OutputTracksName
    # Add the seeding

    cheated_seed_tracking_pr = PrCheatedSciFiTracking(
        FTHitsLocation=make_PrStoreFTHit_hits(),  #from hlt1_tracking
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCPropertyLocation=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        LinkLocation=boole_links_digits_mcparticles()
        ["FTLiteClusters"]).OutputName

    cheated_seed_tracking_v1 = fromPrSeedingTracksV1Tracks(
        InputTracksLocation=cheated_seed_tracking_pr).OutputTracksLocation

    best_long_tracks = TrackBestTrackCreator(
        TracksInContainers=[cheated_long_tracks_v1],
        Fitter=get_track_master_fitter(),
        DoNotRefit=False,
        AddGhostProb=False,
        FitTracks=True).TracksOutContainer

    best_seed_tracks = TrackBestTrackCreator(
        TracksInContainers=[cheated_seed_tracking_v1],
        Fitter=get_track_master_fitter(),
        DoNotRefit=False,
        AddGhostProb=False,
        FitTracks=True).TracksOutContainer

    # add MCLinking to the (fitted) V1 tracks
    links_to_long_tracks = make_links_tracks_mcparticles(
        InputTracks=best_long_tracks, LinksToLHCbIDs=links_to_hits)
    links_to_seed_tracks = make_links_tracks_mcparticles(
        InputTracks=best_seed_tracks, LinksToLHCbIDs=links_to_hits)

    # Now it's showtime: resolutions
    from PyConf.Algorithms import TrackResChecker
    from PyConf.Tools import VisPrimVertTool
    mcpart = mc_unpackers()["MCParticles"]
    mchead = make_data_with_FetchDataFromFile('/Event/MC/Header')
    mcprop = make_data_with_FetchDataFromFile('/Event/MC/TrackInfo')
    res_long_checker = TrackResChecker(
        TracksInContainer=best_long_tracks,
        MCParticleInContainer=mc_unpackers()["MCParticles"],
        LinkerInTable=links_to_long_tracks,
        VisPrimVertTool=VisPrimVertTool(
            public=True,
            MCHeader=mchead,
            MCParticles=mcpart,
            MCProperties=mcprop))
    res_seed_checker = TrackResChecker(
        TracksInContainer=best_seed_tracks,
        MCParticleInContainer=mc_unpackers()["MCParticles"],
        LinkerInTable=links_to_seed_tracks,
        VisPrimVertTool=VisPrimVertTool(
            public=True,
            MCHeader=mchead,
            MCParticles=mcpart,
            MCProperties=mcprop))

    # the last producer of data needs to be added to the control flow.
    data = [res_long_checker, res_seed_checker]

    return Reconstruction('hlt2_cheated_pattern_reco', data)


run_reconstruction(options, run_cheated)
