###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import TestMuonTable
from PyConf.application import configure_input, configure
from Allen.config import setup_allen_non_event_data_service
from Moore import options
from PyConf.control_flow import (CompositeNode, NodeLogic)
from RecoConf.hlt1_muonid import make_muon_hits

config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service()
rec_muon_hits = make_muon_hits()

test_tables = TestMuonTable(
    MuonHitsLocation=rec_muon_hits,
    MuonTable=
    f"./dump/geometry_{options.dddb_tag}_{options.conddb_tag}/muon_tables.bin")

cf_node = CompositeNode(
    'Test_MuonTable',
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node, test_tables],
    force_order=True)
config.update(configure(options, cf_node))
