###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.hlt1_tracking import (make_reco_pvs, make_PatPV3DFuture_pvs,
                                    make_pvs)
from Moore.config import Reconstruction
from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hepSvc = DD4hepSvc()
    dd4hepSvc.DetectorList = ['/world', 'VP']


def run_PatPV3DFuture_reco_pv():
    return Reconstruction('hlt1_pvs_PatPV3DFuture', [make_pvs()])


with make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs):
    run_reconstruction(options, run_PatPV3DFuture_reco_pv)
