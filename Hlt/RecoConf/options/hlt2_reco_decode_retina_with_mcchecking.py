###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This set of options is used for reconstruction development purposes,
and assumes that the input contains MCHits (i.e. is of `Exended`
DST/digi type).
"""

from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_full_track_reco

from RecoConf.mc_checking import check_track_resolution
from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD, make_velo_full_clusters, make_RetinaClusters, get_global_measurement_provider
from RecoConf.hlt2_tracking import get_UpgradeGhostId_tool
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClusterDecoder

options.histo_file = 'hlt2_track_monitoring_with_mc_decode_retina_histos.root'
options.ntuple_file = 'hlt2_track_monitoring_with_mc_decode_retina_ntuples.root'

with standalone_hlt2_full_track_reco.bind(do_mc_checking=True), \
     make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD),\
     get_UpgradeGhostId_tool.bind(velo_hits=make_RetinaClusters),\
     get_global_measurement_provider.bind(velo_hits=make_RetinaClusters),\
     make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClusterDecoder),\
     check_track_resolution.bind(per_hit_resolutions=False, split_per_type=True):
    run_reconstruction(options, standalone_hlt2_full_track_reco)
