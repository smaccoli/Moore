###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco_without_UT

from RecoConf.hlt1_tracking import all_velo_track_types, filter_velo_tracks

from Functors import TX
'''
Emulate a Velo detector with a C side only, on samples with a complete Velo, by cutting on the slope of the tracks. This neglects particles that decay inside the active material, but should be correct within a few percent.
'''

with all_velo_track_types.bind(make_velo_tracks = filter_velo_tracks),\
         filter_velo_tracks.bind( fwdTracksFunctor = (TX < 0), bwdTracksFunctor = (TX > 0)),\
         standalone_hlt2_light_reco_without_UT.bind(
    do_mc_checking=True, do_data_monitoring=True):
    run_reconstruction(options, standalone_hlt2_light_reco_without_UT)
