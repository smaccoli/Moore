###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.hlt1_tracking import make_hlt1_tracks, make_pvs
from RecoConf.mc_checking import get_pv_checkers


def hlt1_reco_pvchecker():

    hlt1_tracks = make_hlt1_tracks()
    pvs = make_pvs()

    data = [pvs]
    data += get_pv_checkers(pvs, hlt1_tracks["Velo"], produce_ntuple=True)

    return Reconstruction('PVperformance', data, [require_gec()])


options.ntuple_file = 'Hlt1_PVperformance.root'
run_reconstruction(options, hlt1_reco_pvchecker)
