###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_reco

from RecoConf.mc_checking import get_best_tracks_checkers, hits_resolution_checkers

with standalone_hlt2_reco.bind(do_mc_checking=True, do_data_monitoring=True), \
     get_best_tracks_checkers.bind(with_mc_hits=False), \
     hits_resolution_checkers.bind(with_mc_hits=False):
    run_reconstruction(options, standalone_hlt2_reco)
