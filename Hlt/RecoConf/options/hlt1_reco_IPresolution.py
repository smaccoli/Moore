###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.hlt1_tracking import make_hlt1_tracks, make_VeloKalman_fitted_tracks, make_pvs
from RecoConf.mc_checking import monitor_IPresolution


def hlt1_reco_IPresolution():
    hlt1_tracks = make_hlt1_tracks()
    fitted_tracks = make_VeloKalman_fitted_tracks(hlt1_tracks)
    inputPVs = make_pvs()
    pr_checker = monitor_IPresolution(fitted_tracks["v1"], inputPVs,
                                      hlt1_tracks["Velo"]["v1"])

    return Reconstruction('IPresolution', [pr_checker], [require_gec()])


options.ntuple_file = "Hlt1_IPresolution.root"
run_reconstruction(options, hlt1_reco_IPresolution)
