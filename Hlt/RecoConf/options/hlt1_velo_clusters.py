###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import TestVeloClusters
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from Allen.config import setup_allen_non_event_data_service
from Moore import options
from Gaudi.Configuration import WARNING
from AllenConf.velo_reconstruction import decode_velo

options.histo_file = "test_velo_clusters.root"

config = configure_input(options)

non_event_data_node = setup_allen_non_event_data_service()
allen_velo_decoding = decode_velo(retina_decoding=False)

test_clusters = TestVeloClusters(
    offsets_estimated_input_size=allen_velo_decoding[
        "dev_offsets_estimated_input_size"],
    module_cluster_num=allen_velo_decoding["dev_module_cluster_num"],
    velo_clusters=allen_velo_decoding["dev_velo_clusters"],
    OutputLevel=WARNING,  # prevent a huge amount of INFO messages
)

cf_node = CompositeNode(
    'test_velo_clusters',
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node, test_clusters],
    force_order=True)

config.update(configure(options, cf_node))
