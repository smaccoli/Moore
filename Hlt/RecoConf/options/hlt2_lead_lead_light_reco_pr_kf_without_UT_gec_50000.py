###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco_ion_without_UT
from RecoConf.hlt2_tracking import make_hlt2_tracks_ion_without_UT
from RecoConf.event_filters import require_gec


with require_gec.bind(cut=50000), \
     make_hlt2_tracks_ion_without_UT.bind( fast_reco=True, use_pr_kf=True, post_fit_selection='(TrGHOSTPROB<0.8)'):
    run_reconstruction(options, standalone_hlt2_light_reco_ion_without_UT)
