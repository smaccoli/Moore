###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#####
## part II
#####

from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco_without_UT
from RecoConf.hlt2_tracking import (
    get_UpgradeGhostId_tool_no_UT, make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_Seed_tracks, make_PrKalmanFilter_Velo_tracks,
    make_TrackBestTrackCreator_tracks)
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD, make_RetinaClusters
from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.calorimeter_reconstruction import make_digits

options.histo_file = "histos_hlt2_light_reco_pr_kf_without_UT_on_data_with_monitoring.root"

with standalone_hlt2_light_reco_without_UT.bind(
        do_mc_checking=False,
        do_data_monitoring=True,
        monitor_all_tracks = True,
        skip_Calo=False,
        skip_Muon=False,
        skip_RICH=False),\
make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.2),\
make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=4.2),\
make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.2),\
make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=4.2),\
make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD),\
get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters),\
make_muon_hits.bind(geometry_version=3),\
make_digits.bind(calo_raw_bank=True):
    run_reconstruction(options, standalone_hlt2_light_reco_without_UT)
