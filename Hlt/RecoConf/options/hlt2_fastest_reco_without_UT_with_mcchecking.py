###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco_without_UT

from RecoConf.mc_checking import check_track_resolution, get_fitted_tracks_checkers

options.ntuple_file = 'hlt2_fastest_reco_without_UT_with_mcchecking.root'

with standalone_hlt2_light_reco_without_UT.bind(do_mc_checking=True, do_data_monitoring=True, fast_reco=True), \
         check_track_resolution.bind(per_hit_resolutions=True, split_per_type=True),\
         get_fitted_tracks_checkers.bind(max_ghost_prob = 0.8): # to roughly equal the ghost rate for tracking without UT with the non-retuned ghost prob
    run_reconstruction(options, standalone_hlt2_light_reco_without_UT)
