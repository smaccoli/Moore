###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.hlt2_global_reco import reconstruction
from PyConf.Algorithms import PrForwardTrackingVelo
from PyConf.packing import persisted_location


def make_two_reconstructions():
    charged_protos_1 = reconstruction()["ChargedProtos"]
    neutral_protos_1 = reconstruction()["NeutralProtos"]
    with PrForwardTrackingVelo.bind(MinPT=1000.), persisted_location.bind(
            force=False):
        charged_protos_2 = reconstruction()["ChargedProtos"]
        neutral_protos_2 = reconstruction()["NeutralProtos"]
    data = [
        charged_protos_1, neutral_protos_1, charged_protos_2, neutral_protos_2
    ]
    return Reconstruction('run_similar_recos_twice', data)


run_reconstruction(options, make_two_reconstructions)

from Configurables import HiveDataBrokerSvc
HiveDataBrokerSvc().OutputLevel = 2
