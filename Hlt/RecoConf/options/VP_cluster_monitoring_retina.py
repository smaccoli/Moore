###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.velo_cluster_data_monitoring import monitor_velo_clusters
from Moore.config import Reconstruction
from RecoConf.hlt1_tracking import make_SPmixed_raw_banks, make_velo_full_clusters
from PyConf.Algorithms import VPRetinaFullClustering

options.histo_file = "VP_cluster_MiniBias_retina.root"


def my_monitor():
    with make_velo_full_clusters.bind(
            make_raw=make_SPmixed_raw_banks,
            make_full_cluster=VPRetinaFullClustering):
        return Reconstruction('vp_cluster_monitoring_retina',
                              [monitor_velo_clusters()])


run_reconstruction(options, my_monitor)
