###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (PrForwardTrackingVelo, PrMatchNN)
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco_without_UT
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks,
    make_TrackBestTrackCreator_tracks,
)
from RecoConf.early_data import (
    get_loose_PrForwardTrackingVelo_params,
    get_loose_PrMatchNN_params,
    get_loose_PrKalmanFilter_params,
    get_loose_TrackBestTrackCreator_params,
)
with PrForwardTrackingVelo.bind(**get_loose_PrForwardTrackingVelo_params()), \
     PrMatchNN.bind(**get_loose_PrMatchNN_params()), \
     make_PrKalmanFilter_noUT_tracks.bind(**get_loose_PrKalmanFilter_params()), \
     make_TrackBestTrackCreator_tracks.bind(**get_loose_TrackBestTrackCreator_params()):
    run_reconstruction(options, standalone_hlt2_light_reco_without_UT)
