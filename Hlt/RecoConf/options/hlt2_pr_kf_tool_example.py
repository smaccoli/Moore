###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from functools import partial
from PyConf.Algorithms import (
    TrackContainersMerger, PrCloneKillerLong, PrCloneKillerDown,
    TrackBestTrackCreator, PrKalmanFilterToolExampleAlgo,
    PrKalmanFilterToolExampleAlgo_Downstream,
    PrKalmanFilterToolExampleAlgo_Upstream, PrKalmanFilterToolExampleAlgo_Velo,
    PrKalmanFilterToolExampleAlgo_V1V1, PrKalmanFilterToolExampleAlgo_Seed,
    PrKalmanFilterToolExampleAlgo_V3, PrKalmanFilterToolExampleAlgo_V1V1_noUT,
    PrKalmanFilterToolExampleAlgo_Downstream_V3,
    PrKalmanFilterToolExampleAlgo_Upstream_V3,
    PrKalmanFilterToolExampleAlgo_Velo_V3,
    PrKalmanFilterToolExampleAlgo_Seed_V3, fromV3TrackFullV1Track)
from PyConf.Tools import (TrackMasterExtrapolator, TrackLinearExtrapolator,
                          KalmanFilterTool)
from RecoConf.core_algorithms import make_unique_id_generator
from RecoConf.hlt1_tracking import (
    make_VeloClusterTrackingSIMD_hits, make_PrStorePrUTHits_hits,
    make_PrStoreSciFiHits_hits, get_global_materiallocator)
from RecoConf.hlt2_tracking import (get_fast_hlt2_tracks,
                                    get_UpgradeGhostId_tool)
from RecoConf.standalone import reco_prefilters
from RecoConf.mc_checking import check_track_resolution
from RecoConf.hlt1_muonid import make_muon_hits


def get_pr_kf_tool(track_type="", tracks_v=""):

    max_chi2 = 2.8

    fitter_template = partial(
        KalmanFilterTool,
        MaxChi2PreOutlierRemoval=20,
        MaxChi2=max_chi2,
        UniqueIDGenerator=make_unique_id_generator())

    if (track_type == "Velo"):
        track_fitter = fitter_template(
            ReferenceExtrapolator=TrackLinearExtrapolator())
    else:
        track_fitter = fitter_template(
            ReferenceExtrapolator=TrackMasterExtrapolator(
                MaterialLocator=get_global_materiallocator()))

    postfix = track_type + "_" + tracks_v
    if (track_type == "Forward"):
        if (tracks_v == "v1"):
            fitter_tool = PrKalmanFilterToolExampleAlgo
        elif (tracks_v == "v3"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_V3
        else:
            return
    elif (track_type == "Match"):
        if (tracks_v == "v1"):
            fitter_tool = PrKalmanFilterToolExampleAlgo
        elif (tracks_v == "v3"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_V3
        else:
            return
    elif (track_type == "Downstream"):
        if (tracks_v == "v1"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_Downstream
        elif (tracks_v == "v3"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_Downstream_V3
        else:
            return
    elif (track_type == "Seed"):
        if (tracks_v == "v1"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_Seed
        elif (tracks_v == "v3"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_Seed_V3
        else:
            return
    elif (track_type == "Velo"):
        if (tracks_v == "v1"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_Velo
        elif (tracks_v == "v3"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_Velo_V3
        else:
            return
    elif (track_type == "Upstream"):
        if (tracks_v == "v1"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_Upstream
        elif (tracks_v == "v3"):
            fitter_tool = PrKalmanFilterToolExampleAlgo_Upstream_V3
        else:
            return

    return partial(
        fitter_tool,
        name="PrKalmanFilterTool" + postfix + "_{hash}",
        TrackFitter=track_fitter)


def make_pr_kf_tool_light_reco_best_tracks(
        tracks, get_ghost_tool=get_UpgradeGhostId_tool, tracks_v="v1"):
    vp_hits = make_VeloClusterTrackingSIMD_hits()
    ut_hits = make_PrStorePrUTHits_hits()
    ft_hits = make_PrStoreSciFiHits_hits()
    muon_hits = make_muon_hits()

    first, name_1 = tracks['Forward']['Pr'], "Forward"
    second, name_2 = tracks['Match']['Pr'], "Match"
    first_ut_hits = tracks['UTHitsRes']
    first_ft_hits = tracks['FTHitsRes']
    second_ut_hits = ut_hits
    second_ft_hits = ft_hits

    max_chi2 = 2.8
    tbtc_template = partial(
        TrackBestTrackCreator,
        MaxChi2DoF=max_chi2,
        GhostIdTool=get_ghost_tool(),
        DoNotRefit=True,
        AddGhostProb=True,
        FitTracks=False)

    out_dict = {}
    for tracks_v in ["v1", "v3"]:

        kf_tool_1 = get_pr_kf_tool(
            track_type=name_1, tracks_v=tracks_v)(
                Input=first,
                HitsVP=vp_hits,
                HitsUT=first_ut_hits,
                HitsFT=first_ft_hits,
                HitsMuon=muon_hits)
        fitted_1_raw = kf_tool_1.OutputTracks

        if (tracks_v == "v3"):
            [partial_chi2s_1, fitresults_1] = [
                kf_tool_1.OutputPartialChi2s, kf_tool_1.OutputTrackFitResults
            ]
            fitted_1 = fromV3TrackFullV1Track(
                name="V3V1Converter" + name_1 + "_{hash}",
                InputTracks=fitted_1_raw,
                InputPartialChi2s=partial_chi2s_1,
                InputTrackFitResults=fitresults_1,
                GhostIdTool=get_ghost_tool()).OutputTracks
        elif (tracks_v == "v1"):
            fitted_1 = fitted_1_raw
        else:
            return

        best_1 = tbtc_template(
            name="TBTC_" + name_1 + "_" + tracks_v + "_{hash}",
            TracksInContainers=[fitted_1]).TracksOutContainer

        decloned_2 = PrCloneKillerLong(
            name="CloneKiller" + name_2 + "_" + tracks_v + "_{hash}",
            TracksInContainer=second,
            TracksRefContainer=best_1).TracksOutContainer

        kf_tool_2 = get_pr_kf_tool(
            track_type=name_2, tracks_v=tracks_v)(
                Input=decloned_2,
                HitsVP=vp_hits,
                HitsUT=second_ut_hits,
                HitsFT=second_ft_hits,
                HitsMuon=muon_hits)

        fitted_2_raw = kf_tool_2.OutputTracks
        if (tracks_v == "v3"):
            [partial_chi2s_2, fitresults_2] = [
                kf_tool_2.OutputPartialChi2s, kf_tool_2.OutputTrackFitResults
            ]
            fitted_2 = fromV3TrackFullV1Track(
                name="V3V1Converter" + name_2 + "_{hash}",
                InputTracks=fitted_2_raw,
                InputPartialChi2s=partial_chi2s_2,
                InputTrackFitResults=fitresults_2,
                GhostIdTool=get_ghost_tool()).OutputTracks
        elif (tracks_v == "v1"):
            fitted_2 = fitted_2_raw
        else:
            return

        best_2 = tbtc_template(
            name="TBTC" + name_2 + "_" + tracks_v + "_{hash}",
            TracksInContainers=[fitted_2]).TracksOutContainer

        best_long = TrackContainersMerger(
            InputLocations=[best_1, best_2]).OutputLocation

        decloned_down = PrCloneKillerDown(
            TracksInContainer=tracks['Downstream']['Pr'],
            TracksRefContainer=best_long).TracksOutContainer

        kf_tool_down = get_pr_kf_tool(
            track_type="Downstream", tracks_v=tracks_v)(
                HitsVP=vp_hits,
                HitsUT=tracks['UTHitsRes'],
                HitsFT=ft_hits,
                HitsMuon=muon_hits,
                Input=decloned_down)
        fitted_down_raw = kf_tool_down.OutputTracks

        if (tracks_v == "v3"):
            [partial_chi2s_down, fitresults_down] = [
                kf_tool_down.OutputPartialChi2s,
                kf_tool_down.OutputTrackFitResults
            ]
            fitted_down = fromV3TrackFullV1Track(
                name='V3V1ConverterDownstream_{hash}',
                InputTracks=fitted_down_raw,
                InputPartialChi2s=partial_chi2s_down,
                InputTrackFitResults=fitresults_down,
                GhostIdTool=get_ghost_tool()).OutputTracks
        elif (tracks_v == "v1"):
            fitted_down = fitted_down_raw
        else:
            return

        best_down = tbtc_template(
            name="TBTC_down_" + tracks_v + "_{hash}",
            TracksInContainers=[fitted_down]).TracksOutContainer

        kf_tool_velo = get_pr_kf_tool(
            track_type="Velo", tracks_v=tracks_v)(
                HitsVP=vp_hits,
                HitsUT=ut_hits,
                HitsFT=ft_hits,
                HitsMuon=muon_hits,
                Input=tracks["Velo"]["Pr"])
        fitted_velo_raw = kf_tool_velo.OutputTracks
        if (tracks_v == "v3"):
            [partial_chi2s_velo, fitresults_velo] = [
                kf_tool_velo.OutputPartialChi2s,
                kf_tool_velo.OutputTrackFitResults
            ]
            best_velo = fromV3TrackFullV1Track(
                name='V3V1ConverterVelo_{hash}',
                InputTracks=fitted_velo_raw,
                InputPartialChi2s=partial_chi2s_velo,
                InputTrackFitResults=fitresults_velo,
                GhostIdTool=get_ghost_tool()).OutputTracks
        elif (tracks_v == "v1"):
            best_velo = fitted_velo_raw
        else:
            return

        kf_tool_seed = get_pr_kf_tool(
            track_type="Seed", tracks_v=tracks_v)(
                HitsVP=vp_hits,
                HitsUT=ut_hits,
                HitsFT=ft_hits,
                HitsMuon=muon_hits,
                Input=tracks['Seed']['Pr'])
        fitted_seed_raw = kf_tool_seed.OutputTracks
        if (tracks_v == "v3"):
            [partial_chi2s_seed, fitresults_seed] = [
                kf_tool_seed.OutputPartialChi2s,
                kf_tool_seed.OutputTrackFitResults
            ]
            fitted_seed = fromV3TrackFullV1Track(
                name='V3V1ConverterSeed_{hash}',
                InputTracks=fitted_seed_raw,
                InputPartialChi2s=partial_chi2s_seed,
                InputTrackFitResults=fitresults_seed,
                GhostIdTool=get_ghost_tool()).OutputTracks
        elif (tracks_v == "v1"):
            fitted_seed = fitted_seed_raw
        else:
            return

        best_seed = tbtc_template(
            name="TBTC_seed_" + tracks_v + "_{hash}",
            TracksInContainers=[fitted_seed]).TracksOutContainer

        # fit upstream tracks (no clone killing)
        kf_tool_upstream = get_pr_kf_tool(
            track_type="Upstream", tracks_v=tracks_v)(
                HitsVP=vp_hits,
                HitsUT=ut_hits,
                HitsFT=ft_hits,
                HitsMuon=muon_hits,
                Input=tracks['Upstream']['Pr'])
        fitted_upstream_raw = kf_tool_upstream.OutputTracks
        if (tracks_v == "v3"):
            [partial_chi2s_upstream, fitresults_upstream] = [
                kf_tool_upstream.OutputPartialChi2s,
                kf_tool_upstream.OutputTrackFitResults
            ]
            fitted_upstream = fromV3TrackFullV1Track(
                name='V3V1ConverterSeed_{hash}',
                InputTracks=fitted_upstream_raw,
                InputPartialChi2s=partial_chi2s_upstream,
                InputTrackFitResults=fitresults_upstream,
                GhostIdTool=get_ghost_tool()).OutputTracks
        elif (tracks_v == "v1"):
            fitted_upstream = fitted_upstream_raw
        else:
            return

        best_upstream = tbtc_template(
            name="TBTC_upstream_" + tracks_v + "_{hash}",
            TracksInContainers=[fitted_upstream]).TracksOutContainer

        out_dict[tracks_v] = {
            'BestLong': best_long,
            'BestDownstream': best_down,
            'SeedDecloned': best_seed,
            'BestVelo': best_velo,
            'BestSeed': best_seed,
            'BestUpstream': best_upstream
        }
    return out_dict


def make_refit_kf_tool_light_reco_best_tracks(tracks):
    vp_hits = make_VeloClusterTrackingSIMD_hits()
    ut_hits = make_PrStorePrUTHits_hits()
    ft_hits = make_PrStoreSciFiHits_hits()
    muon_hits = make_muon_hits()

    max_chi2_v1 = 2.5

    best_long_refit = PrKalmanFilterToolExampleAlgo_V1V1(
        HitsVP=vp_hits,
        HitsUT=ut_hits,
        HitsFT=ft_hits,
        HitsMuon=muon_hits,
        TrackFitter=KalmanFilterTool(
            ReferenceExtrapolator=TrackMasterExtrapolator(
                MaterialLocator=get_global_materiallocator()),
            MaxChi2PreOutlierRemoval=20,
            MaxChi2=max_chi2_v1,
            UniqueIDGenerator=make_unique_id_generator()),
        name='PrKalmanFilterToolLongV1_{hash}',
        Input=tracks['BestLong']).OutputTracks

    best_long_refit_noUT = PrKalmanFilterToolExampleAlgo_V1V1_noUT(
        HitsVP=vp_hits,
        HitsFT=ft_hits,
        HitsMuon=muon_hits,
        TrackFitter=KalmanFilterTool(
            ReferenceExtrapolator=TrackMasterExtrapolator(
                MaterialLocator=get_global_materiallocator()),
            MaxChi2PreOutlierRemoval=20,
            MaxChi2=max_chi2_v1,
            UniqueIDGenerator=make_unique_id_generator()),
        name='PrKalmanFilterToolLongV1_noUT_{hash}',
        Input=tracks['BestLong']).OutputTracks

    best_down_refit = PrKalmanFilterToolExampleAlgo_V1V1(
        HitsVP=vp_hits,
        HitsUT=ut_hits,
        HitsFT=ft_hits,
        HitsMuon=muon_hits,
        TrackFitter=KalmanFilterTool(
            ReferenceExtrapolator=TrackMasterExtrapolator(
                MaterialLocator=get_global_materiallocator()),
            MaxChi2PreOutlierRemoval=20,
            MaxChi2=max_chi2_v1,
            UniqueIDGenerator=make_unique_id_generator()),
        name='PrKalmanFilterToolDownV1_{hash}',
        Input=tracks['BestDownstream']).OutputTracks

    best_seed_refit = PrKalmanFilterToolExampleAlgo_V1V1(
        HitsVP=vp_hits,
        HitsUT=ut_hits,
        HitsFT=ft_hits,
        HitsMuon=muon_hits,
        TrackFitter=KalmanFilterTool(
            ReferenceExtrapolator=TrackMasterExtrapolator(
                MaterialLocator=get_global_materiallocator()),
            MaxChi2PreOutlierRemoval=20,
            MaxChi2=max_chi2_v1,
            UniqueIDGenerator=make_unique_id_generator()),
        name='PrKalmanFilterToolSeedV1_{hash}',
        Input=tracks['BestSeed']).OutputTracks

    best_upstream_refit = PrKalmanFilterToolExampleAlgo_V1V1(
        HitsVP=vp_hits,
        HitsUT=ut_hits,
        HitsFT=ft_hits,
        HitsMuon=muon_hits,
        TrackFitter=KalmanFilterTool(
            ReferenceExtrapolator=TrackMasterExtrapolator(
                MaterialLocator=get_global_materiallocator()),
            MaxChi2PreOutlierRemoval=20,
            MaxChi2=max_chi2_v1,
            UniqueIDGenerator=make_unique_id_generator()),
        name='PrKalmanFilterToolUpstreamV1_{hash}',
        Input=tracks['BestUpstream']).OutputTracks

    best_velo_refit = PrKalmanFilterToolExampleAlgo_V1V1(
        HitsVP=vp_hits,
        HitsUT=ut_hits,
        HitsFT=ft_hits,
        HitsMuon=muon_hits,
        TrackFitter=KalmanFilterTool(
            ReferenceExtrapolator=TrackLinearExtrapolator(),
            MaxChi2PreOutlierRemoval=20,
            MaxChi2=max_chi2_v1,
            UniqueIDGenerator=make_unique_id_generator()),
        name='PrKalmanFilterToolVeloV1_{hash}',
        Input=tracks['BestVelo']).OutputTracks

    return {
        'BestLongRefit': best_long_refit,
        'BestLongRefitnoUT': best_long_refit_noUT,
        'BestDownstreamRefit': best_down_refit,
        'BestVeloRefit': best_velo_refit,
        'BestSeedRefit': best_seed_refit,
        'BestUpstreamRefit': best_upstream_refit
    }


def pr_kf_tool_test():

    track_dict = get_fast_hlt2_tracks()

    track_containers = make_pr_kf_tool_light_reco_best_tracks(
        tracks=track_dict)

    track_containers_v1tool = track_containers["v1"]
    track_containers_v3tool = track_containers["v3"]

    refitted_containers = make_refit_kf_tool_light_reco_best_tracks(
        tracks=track_containers_v1tool)

    tracks_version = "v1"
    for trType in track_containers_v1tool.keys():
        track_dict[trType] = {tracks_version: track_containers_v1tool[trType]}
    for trType in track_containers_v3tool.keys():
        track_dict[trType + "V3"] = {
            tracks_version: track_containers_v3tool[trType]
        }
    for trType in refitted_containers.keys():
        track_dict[trType] = {tracks_version: refitted_containers[trType]}

    hlt2_tracks = track_dict
    out_track_types = {
        "Best": [
            "BestVelo", "BestVeloV3", "BestVeloRefit", "BestLong",
            "BestLongV3", "BestLongRefit", "BestLongRefitnoUT",
            "BestDownstream", "BestDownstreamRefit", "BestDownstreamV3",
            "BestSeed", "BestSeedRefit", "BestSeedV3", "BestUpstream",
            "BestUpstreamRefit", "BestUpstreamV3"
        ]
    }

    best_tracks = {
        track_type: hlt2_tracks[track_type]
        for track_type in out_track_types["Best"]
    }
    data = [tracks["v1"] for tracks in best_tracks.values()]

    resolutions = []
    for tr_type in best_tracks.keys():
        reso = check_track_resolution(
            best_tracks[tr_type], per_hit_resolutions=True, suffix=tr_type)
        resolutions.append(reso)

    data += resolutions
    return Reconstruction('hlt2_reco_pr_kf_tool', data, reco_prefilters())


options.histo_file = "tool_reso_histos.root"
options.evt_max = 10
run_reconstruction(options, pr_kf_tool_test)
