###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco
from RecoConf.event_filters import require_gec
from RecoConf.hlt2_tracking import make_hlt2_tracks

# A higher gec cut than 40000 leads to unreasonable processing times for the nightlies.
with require_gec.bind(cut=40000), \
     make_hlt2_tracks.bind(use_pr_kf=True):
    run_reconstruction(options, standalone_hlt2_light_reco)
