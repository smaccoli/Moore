###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import TestUTHits
from PyConf.application import (configure_input, configure)
from PyConf.control_flow import (CompositeNode, NodeLogic)
from Allen.config import setup_allen_non_event_data_service
from RecoConf.hlt1_tracking import make_PrStorePrUTHits_hits
from RecoConf.data_from_file import mc_unpackers
from Moore import options
from AllenConf.ut_reconstruction import decode_ut

config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service()
unpack_ut_hits = mc_unpackers()['MCUTHits']
allen_decoded_ut = decode_ut()

with make_PrStorePrUTHits_hits.bind(isCluster=False):
    test_hits = TestUTHits(
        ut_hit_offsets=allen_decoded_ut["dev_ut_hit_offsets"],
        ut_hits=allen_decoded_ut["dev_ut_hits"],
        UnpackedUTHits=unpack_ut_hits,
        UTHitsLocation=make_PrStorePrUTHits_hits())

    cf_node = CompositeNode(
        'test_ut_hits',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[non_event_data_node, test_hits],
        force_order=True)
    config.update(configure(options, cf_node))
