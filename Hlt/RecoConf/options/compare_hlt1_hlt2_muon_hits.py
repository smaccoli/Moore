###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import TestMuonHits
from PyConf.application import (configure_input, configure)
from PyConf.control_flow import (CompositeNode, NodeLogic)
from Allen.config import setup_allen_non_event_data_service
from AllenConf.muon_reconstruction import decode_muon
from RecoConf.hlt1_muonid import make_muon_hits
from Moore import options

config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service()
allen_muon_hits = decode_muon()
rec_muon_hits = make_muon_hits()

test_hits = TestMuonHits(
    muon_offsets=allen_muon_hits["dev_station_ocurrences_offset"],
    muon_hits=allen_muon_hits["dev_muon_hits"],
    MuonHitsLocation=rec_muon_hits)

cf_node = CompositeNode(
    'compare_muon_hits',
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node, test_hits],
    force_order=True)
config.update(configure(options, cf_node))
