###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.hlt1_tracking import (
    all_velo_track_types, make_PrVeloUT_tracks,
    make_VeloClusterTrackingSIMD_hits, make_PrStoreSciFiHits_hits,
    make_PrStorePrUTHits_hits, get_global_materiallocator)
from RecoConf.hlt2_tracking import make_PrHybridSeeding_tracks
from PyConf.Algorithms import (PrKalmanFilter_Velo, PrKalmanFilter_Seed,
                               PrKalmanFilter_Upstream)
from RecoConf.mc_checking import check_track_resolution
from RecoConf.core_algorithms import make_unique_id_generator
from PyConf.Tools import TrackMasterExtrapolator, TrackLinearExtrapolator


def pr_kf_special_test():

    velo_tracks = all_velo_track_types()
    scifi_tracks = make_PrHybridSeeding_tracks()
    ut_tracks = make_PrVeloUT_tracks(velo_tracks=velo_tracks)

    vp_hits = make_VeloClusterTrackingSIMD_hits()
    ft_hits = make_PrStoreSciFiHits_hits()
    ut_hits = make_PrStorePrUTHits_hits()

    max_chi2 = 2.8
    max_chi2_preoutlier = 20.

    fitted_velo = PrKalmanFilter_Velo(
        MaxChi2=max_chi2,
        MaxChi2PreOutlierRemoval=max_chi2_preoutlier,
        Input=velo_tracks['Pr'],
        HitsVP=vp_hits,
        ReferenceExtrapolator=TrackLinearExtrapolator(),
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    fitted_seed = PrKalmanFilter_Seed(
        MaxChi2=max_chi2,
        MaxChi2PreOutlierRemoval=max_chi2_preoutlier,
        Input=scifi_tracks['Pr'],
        HitsFT=ft_hits,
        ReferenceExtrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator()),
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    fitted_upstream = PrKalmanFilter_Upstream(
        MaxChi2=max_chi2,
        MaxChi2PreOutlierRemoval=max_chi2_preoutlier,
        Input=ut_tracks,
        HitsVP=vp_hits,
        HitsUT=ut_hits,
        ReferenceExtrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator()),
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    fitted_velo_dict = {"v1": fitted_velo}
    fitted_seed_dict = {"v1": fitted_seed}
    fitted_upstream_dict = {"v1": fitted_upstream}
    resolutions_velo = check_track_resolution(
        fitted_velo_dict, per_hit_resolutions=True, suffix="Velo")
    resolutions_seed = check_track_resolution(
        fitted_seed_dict, per_hit_resolutions=True, suffix="Seed")
    resolutions_upstream = check_track_resolution(
        fitted_upstream_dict, per_hit_resolutions=True, suffix="Upstream")

    return Reconstruction(
        'my_reco', [resolutions_velo, resolutions_seed, resolutions_upstream])


run_reconstruction(options, pr_kf_special_test)
