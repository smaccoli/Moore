###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import VPRetinaFullClustering
from Moore import options
from Moore.config import Reconstruction
from Moore.config import run_allen_reconstruction
from RecoConf.hlt1_tracking import make_velo_full_clusters
from RecoConf.hlt1_allen import make_allen_velo_tracks
from RecoConf.event_filters import require_gec
from RecoConf.mc_checking import (
    check_tracking_efficiency, make_links_tracks_mcparticles,
    make_links_lhcbids_mcparticles_tracking_system)
from RecoConf.mc_checking_categories import (get_mc_categories,
                                             get_hit_type_mask)
from AllenCore.gaudi_allen_generator import allen_runtime_options


def make_reconstruction():

    velo_tracks = make_allen_velo_tracks()

    # make links to lhcb id for mc matching
    # ms: this uses the CPU version of the retina decoding/clustering, but we test that Allen and this gives the same clusters/LHCbIDs
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()

    # build the PrChecker algorihm for velo track
    links_to_velo_tracks = make_links_tracks_mcparticles(
        InputTracks=velo_tracks["v1keyed"], LinksToLHCbIDs=links_to_lhcbids)

    pr_checker_for_velo_track = check_tracking_efficiency(
        TrackType="Velo",
        InputTracks=velo_tracks["v1keyed"],
        LinksToTracks=links_to_velo_tracks,
        LinksToLHCbIDs=links_to_lhcbids,
        MCCategories=get_mc_categories("Velo"),
        HitTypesToCheck=get_hit_type_mask("Velo"),
    )

    return Reconstruction('track_efficiency', [pr_checker_for_velo_track],
                          [require_gec()])


options.evt_max = 100
options.histo_file = "Hlt1VeloTrackingResolutionAllen.root"

with (make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClustering),
      allen_runtime_options.bind(filename="allen_gaudi_velo_monitor.root")):
    run_allen_reconstruction(options, make_reconstruction)
