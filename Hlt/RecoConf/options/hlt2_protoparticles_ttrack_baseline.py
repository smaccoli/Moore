###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Simple options running proto particle making and dumping output to text files for reference checking."""
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.reconstruction_objects import reconstruction as reco_objects_reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_legacy_reconstruction
from RecoConf.ttrack_selections_reco import make_ttrack_protoparticles, make_good_ttracks
from PyConf.Algorithms import PrintProtoParticles


def standalone_hlt2_ttrack_protoparticles():
    ttrack_protoparticles = make_ttrack_protoparticles()

    print_ttrack = PrintProtoParticles(
        name="TtrackProtos",
        Input=ttrack_protoparticles,
    )

    data = [print_ttrack]

    prefilters = [require_gec()]
    return Reconstruction('hlt2_ttrack_protoparticles', data, prefilters)


# Run with serial processing to allow muon ID
options.n_event_slots = 1
options.n_threads = 1
with reco_objects_reconstruction.bind(
        from_file=False), hlt2_reconstruction.bind(
            make_reconstruction=make_legacy_reconstruction
        ), make_good_ttracks.bind(
            global_reco=hlt2_reconstruction, refit_tracks=False):
    run_reconstruction(options, standalone_hlt2_ttrack_protoparticles)
