###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_full_track_reco
from PyConf.Tools import TrackMasterFitter

with standalone_hlt2_full_track_reco.bind(
        light_reco=True), TrackMasterFitter.bind(
            FastMaterialApproximation=True):
    run_reconstruction(options, standalone_hlt2_full_track_reco)
