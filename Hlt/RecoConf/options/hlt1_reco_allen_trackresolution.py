###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.hlt1_allen import make_allen_tracks
from RecoConf.mc_checking import check_track_resolution
from PyConf.application import make_data_with_FetchDataFromFile


def hlt1_reco_allen_trackresolution():
    track_type = "Forward"
    tracks = make_allen_tracks()[track_type]
    pr_checker = check_track_resolution(tracks)

    return Reconstruction(
        'track_resolution',
        [make_data_with_FetchDataFromFile("/Event/MC/TrackInfo"), pr_checker],
        [require_gec()])


options.histo_file = "Hlt1ForwardTrackingResolutionAllen.root"
run_allen_reconstruction(options, hlt1_reco_allen_trackresolution)
