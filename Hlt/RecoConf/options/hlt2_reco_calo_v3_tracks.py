###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_calo_tracks_v3
from RecoConf.calorimeter_reconstruction import make_digits

from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)
options.set_input_and_conds_from_testfiledb('upgrade_B2JpsiK_ee_MU')

options.evt_max = 1000
options.ntuple_file = 'output_calo_chargedpids_checker.root'

# use old calo raw bank names
with make_digits.bind(calo_raw_bank=False):
    run_reconstruction(options, standalone_hlt2_calo_tracks_v3)
