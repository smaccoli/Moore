###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from Moore.config import Reconstruction
from Moore.config import run_allen_reconstruction
from RecoConf.event_filters import require_gec
from AllenConf.velo_reconstruction import decode_velo, make_velo_tracks, run_velo_kalman_filter
from AllenConf.primary_vertex_reconstruction import make_pvs
from PyConf.Algorithms import GaudiAllenPVsToPrimaryVertexContainer
from RecoConf.mc_checking import get_pv_checkers
from RecoConf.core_algorithms import make_unique_id_generator
from PyConf.Algorithms import (GaudiAllenVeloToV3Tracks, TrackContainersMerger)
from PyConf.Algorithms import (fromV3TrackV1Track)


def make_reconstruction():

    # reconstruct Allen velo tracks
    decoded_velo = decode_velo()
    velo_tracks = make_velo_tracks(decoded_velo)
    velo_states = run_velo_kalman_filter(velo_tracks)

    # convert Allen velo tracks to various track objects
    velo_tracks_v3_alg = GaudiAllenVeloToV3Tracks(
        allen_tracks_mec=velo_tracks["dev_velo_multi_event_tracks_view"],
        allen_beamline_states_view=velo_states[
            "dev_velo_kalman_beamline_states_view"],
        InputUniqueIDGenerator=make_unique_id_generator())

    velo_tracks_fwd_v3 = velo_tracks_v3_alg.OutputTracksForward
    velo_tracks_bwd_v3 = velo_tracks_v3_alg.OutputTracksBackward

    velo_tracks_fwd_v1_keyed = fromV3TrackV1Track(
        InputTracks=velo_tracks_fwd_v3).OutputTracks

    velo_tracks_bwd_v1_keyed = fromV3TrackV1Track(
        InputTracks=velo_tracks_bwd_v3).OutputTracks

    velo_tracks_v1_keyed = TrackContainersMerger(
        InputLocations=[velo_tracks_fwd_v1_keyed, velo_tracks_bwd_v1_keyed
                        ]).OutputLocation

    allen_tracks = {"v1": velo_tracks_v1_keyed}

    # reconstruct Allen PVs
    pvs = make_pvs(velo_tracks)

    # convert Allen PVs to PrimaryVertexContainer object
    pv_container = GaudiAllenPVsToPrimaryVertexContainer(
        number_of_multivertex=pvs["dev_number_of_multi_final_vertices"],
        reconstructed_multi_pvs=pvs["dev_multi_final_vertices"]).OutputPVs

    # call PV checker
    pv_checker = get_pv_checkers(
        pv_container, allen_tracks, produce_ntuple=True)

    return Reconstruction('pv_checker', pv_checker, [require_gec()])


options.evt_max = 100
options.ntuple_file = 'Hlt1_PVperformance_Allen.root'
run_allen_reconstruction(options, make_reconstruction)
