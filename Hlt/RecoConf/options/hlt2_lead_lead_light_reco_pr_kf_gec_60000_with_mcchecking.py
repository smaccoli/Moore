###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco_ion
from RecoConf.hlt2_tracking import make_hlt2_tracks_ion

from RecoConf.event_filters import require_gec
from RecoConf.mc_checking import check_track_resolution

options.histo_file = 'hlt2_lead_lead_track_monitoring_with_mc_histos.root'
options.ntuple_file = 'hlt2_lead_lead_track_monitoring_with_mc_ntuples.root'
options.evt_max = 20

with require_gec.bind(cut=60000), \
     standalone_hlt2_light_reco_ion.bind(do_mc_checking=True, do_data_monitoring=True, use_pr_kf=True), \
     check_track_resolution.bind(per_hit_resolutions=True, split_per_type=True), \
     make_hlt2_tracks_ion.bind( fast_reco=True, use_pr_kf=True, post_fit_selection='(TrGHOSTPROB<0.9999)'):
    run_reconstruction(options, standalone_hlt2_light_reco_ion)
