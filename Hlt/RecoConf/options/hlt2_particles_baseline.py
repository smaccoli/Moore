###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Simple options file to demonstrate how to create basic particles and their combination from the output of the reconstruction."""
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec, require_pvs
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import (
    reconstruction,
    make_charged_protoparticles,
    make_neutral_protoparticles,
)
from Hlt2Conf.algorithms_thor import (
    ParticleCombiner,
    ParticleFilter,
)
from Hlt2Conf.standard_particles import (
    _make_V0LL,
    _make_particles,
    standard_protoparticle_filter,
)
from PyConf.Algorithms import PrintProtoParticles

import Functors as F

from GaudiKernel.SystemOfUnits import MeV

from RecoConf.reconstruction_objects import (make_pvs)


#make KS0_LL
def make_KsLL(pions, pvs):
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0LL(
        particles=[pions, pions], descriptors=descriptors, pv_maker=pvs)


#make JPsi
def make_JPsi(muons):
    return ParticleCombiner(
        name="jpsi_combiner",
        Inputs=[muons, muons],
        DecayDescriptor='J/psi(1S) -> mu+ mu-',
        CombinationCut=F.ALL,
        CompositeCut=(F.CHI2DOF < 25))


def standalone_hlt2_particles(print_protos=False):
    pvs = make_pvs

    pions = _make_particles(
        species='pion', make_protoparticles=make_charged_protoparticles)
    with standard_protoparticle_filter.bind(Code=F.ISMUON):
        muons = _make_particles(
            species='muon', make_protoparticles=make_charged_protoparticles)

    data = []

    # Make pions for KS0
    long_detached_pions = ParticleFilter(
        pions,
        name="filter_pions",
        Cut=F.FILTER(
            F.require_all(F.PT > 500 * MeV, F.P > 2000. * MeV, F.PID_K < 0,
                          F.BPVIPCHI2(pvs()) > 9)))

    #make muons for JPsi
    long_muons = ParticleFilter(
        Input=muons,
        name="filter_muons",
        Cut=F.FILTER(
            F.require_all(F.PT > 200. * MeV, F.ISMUON, F.PROBNN_MU > 0.5)))

    ksll = make_KsLL(long_detached_pions, pvs)
    jpsi = make_JPsi(long_muons)

    charged_protos = make_charged_protoparticles()
    neutral_protos = make_neutral_protoparticles()
    if (print_protos):
        data += [
            PrintProtoParticles(
                name="PrintChargedProtos", Input=charged_protos)
        ]
        data += [
            PrintProtoParticles(
                name="PrintNeutralProtos", Input=neutral_protos)
        ]

    data += [jpsi, ksll]

    prefilters = [require_gec(), require_pvs(pvs())]
    return Reconstruction('hlt2_particles', data, prefilters)


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False, spruce=False):
    run_reconstruction(options, standalone_hlt2_particles, public_tools)
