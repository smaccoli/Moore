###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#!/usr/bin/python

# The script for plotting PV efficinecy as the function
# of various distributions: nTracks, z, r.
# As input the NTuple created by hlt1_reco_pvchecker.py
# is needed.
#
# The efficency is calculated usig TGraphAsymmErrors
# and Bayesian error bars
#
# author: Agnieszka Dziurda (agnieszka.dziurda@cern.ch)
# date:   02/2020
#
# Example of usage:
# ../../../run python PrimaryVertexCheckerEfficiency.py
# --file file1.root file2.root --label name1 name2 --dist
#

import os, sys
import argparse

from ROOT import gROOT, TLegend

parser = argparse.ArgumentParser()
parser.add_argument(
    '--file', dest='fileName', default="", nargs='+', help='filename to plot')
parser.add_argument(
    '--label', dest='label', default="", nargs='+', help='labels for files')
parser.add_argument(
    '--tree',
    dest='treeName',
    default="",
    nargs='+',
    help='tree name to plot',
)
parser.add_argument(
    '--smog',
    dest='smog',
    default=False,
    action='store_true',
    help='set true for SMOG')
parser.add_argument(
    '--multi',
    dest='multi',
    default=False,
    action='store_true',
    help='add multiplicity plots')
parser.add_argument(
    '--isol',
    dest='isol',
    default=False,
    action='store_true',
    help='add isolated/closed plots')
parser.add_argument(
    '--dist',
    dest='dist',
    default=False,
    action='store_true',
    help='plot distributions in the canvas')
parser.add_argument(
    '--prefix',
    dest='prefix',
    default="pv_eff",
    help='prefix for the plot name',
)
parser.add_argument(
    '--dir',
    dest='directory',
    default=os.getcwd(),
    help='tree name to plot',
)

parser.add_argument(
    '--offset',
    dest='offset',
    default=0,
    help='offset for plot colors',
)


def get_labels(number_of_files):
    label = []
    for i in range(0, number_of_files):
        label.append("PV Checker {number}".format(number=str(i + 1)))
    return label


if __name__ == '__main__':
    args = parser.parse_args()
    path = args.directory + "/utils/"
    sys.path.append(os.path.abspath(path))
    offset = int(args.offset)

    gROOT.SetBatch()

    from pvutils import get_files, get_trees, get_eff, plot

    from pvconfig import get_variable_ranges
    from pvconfig import get_style, get_categories
    from pvconfig import set_legend

    ranges = get_variable_ranges(args.smog)
    style = get_style()

    label = args.label
    if args.label == "":
        label = get_labels(len(args.fileName))

    tr = {}
    tf = {}
    tf = get_files(tf, label, args.fileName)
    tr = get_trees(tf, tr, label, args.treeName, True)

    eff = {}
    eff["tracks"] = {}
    eff["z"] = {}
    eff["r"] = {}

    hist = {}
    hist["tracks"] = {}
    hist["z"] = {}
    hist["r"] = {}

    cat = get_categories(args.multi, args.isol, args.smog)

    eff["tracks"], hist["tracks"] = get_eff(eff["tracks"], hist["tracks"], tr,
                                            "nrectrmc", style, ranges, cat,
                                            label, offset)
    eff["z"], hist["z"] = get_eff(eff["z"], hist["z"], tr, "zMC", style,
                                  ranges, cat, label, offset)
    eff["r"], hist["r"] = get_eff(eff["r"], hist["r"], tr, "rMC", style,
                                  ranges, cat, label, offset)

    if args.dist:
        legend = TLegend(0.15, 0.82, 0.88, 0.98)
    else:
        legend = TLegend(0.15, 0.86, 0.88, 0.98)
    legend = set_legend(legend, label, eff["tracks"], "eff", hist["z"],
                        args.dist)

    plot(eff["tracks"], "eff", args.prefix, "ntracks", cat, label, legend,
         hist["tracks"], args.dist, (0.9, 0.65))
    plot(eff["z"], "eff", args.prefix, "z", cat, label, legend, hist["z"],
         args.dist, (0.9, 0.65))
    plot(eff["r"], "eff", args.prefix, "r", cat, label, legend, hist["r"],
         args.dist, (0.9, 0.3))
