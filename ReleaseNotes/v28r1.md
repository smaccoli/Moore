2018-04-12 Moore v28r1
========================================

Production version for 2018
----------------------------------------
Based on Gaudi v29r3, LHCb v44r1, Lbcom v22r0p1, Rec v23r1, Phys v25r1, Hlt v28r1
This version is released on the 2018-patches branch.

- Allow for HC emulation in the ReplayL0DUOnly mode, !137 (@rmatev)
- Test Physics_pp_Ramp2018 settings in the nightlies, !139 (@rmatev)
- Use 2018 L0 TCK in tests, !136 (@rmatev)
