2023-03-22 Moore v54r6
===

This version uses
Allen [v3r6](../../../../Allen/-/tags/v3r6),
Rec [v35r6](../../../../Rec/-/tags/v35r6),
Lbcom [v34r6](../../../../Lbcom/-/tags/v34r6),
LHCb [v54r6](../../../../LHCb/-/tags/v54r6),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12),
Detector [v1r10](../../../../Detector/-/tags/v1r10) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Moore [v54r5](/../../tags/v54r5), with the following changes:

### New features ~"new feature"

- ~Persistency | Pp2mcp buffer size counter, !2146 (@sesen)
- ~"Flavour tagging" | B&Q: B hadrons for spectroscopy, !2109 (@ipolyako) [#534]
- Add SL combiner names, !2057 (@masmith)
- Hlt2 ttrack combiners, Lb2JpsiLambdaTT and Bd2JpsiKsTT lines, !1873 (@isanders)
- SOACollection particlev2, !1984 (@ahennequ)


### Fixes ~"bug fix" ~workaround

- Avoid duplicate PV & RecSummary entries in control-flow, !2178 (@graven)
- Created v3 ref files for hlt1_hlt2_comp test, !2172 (@sponce)
- Solve test crashes, !2164 (@lohenry)
- Fixing test that depend on algorithm names, !1909 (@sponce)


### Enhancements ~enhancement

- ~selection ~hlt2 | Update according to BnoC 12Jan meeting, !2122 (@yoyang)
- ~hlt2 | Add `extra_outputs` to save the flavour tagging particles, !2093 (@yihou)
- ~Jets ~"MC checking" | Update Moore/Hlt2 options to run MC jets from Rec/Phys/JetAccessories, !2011 (@helder)
- Update reco binding in  hlt2_all_lines.py, !2104 (@msaur)
- Update Bandq Hlt2 and Spruce test file examples, !1997 (@mengzhen)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~selection | More fixes for Hlt2 lines, !2054 (@mstahl)
- Increase D0 mass range and rename ProbeAndLongMatcher, !2139 (@rowina)
- Adding qmtest of hlt1_hlt2_comparison, !2107 (@xueting)
- Missed wg and logging, !2188 (@nskidmor)


### Documentation ~Documentation

- Update doc/tutorials/hltefficiencychecker.rst, !2148 (@mamartin)


### Other

