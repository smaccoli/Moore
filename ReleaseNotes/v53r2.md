2021-10-22 Moore v53r2
===

This version uses
Allen [v1r7](../../../../Allen/-/tags/v1r7),
Phys [v33r3](../../../../Phys/-/tags/v33r3),
Rec [v33r3](../../../../Rec/-/tags/v33r3),
Lbcom [v33r4](../../../../Lbcom/-/tags/v33r4),
LHCb [v53r4](../../../../LHCb/-/tags/v53r4),
Gaudi [v36r2](../../../../Gaudi/-/tags/v36r2) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Moore [v53r1](/../../tags/v53r1), with the following changes:

### New features ~"new feature"

- ~selection | Update HltEfficiencyChecker docs for TOS efficiencies, !1048 (@rjhunter)
- ~selection ~hlt2 | Include draft of lines to study strange decays, !1063 (@mramospe)
- ~selection ~hlt2 | Hyperon from charm lines, !963 (@mstahl)
- ~Configuration ~Calo | Add calo only reconstruction configuration, !1032 (@cmarinbe)
- Upstream project highlights :star:
  - ~Decoding ~Calo | New calorimeter encoding/decoding, LHCb!3287 (@jmarchan)
  - ~Decoding ~"Event model" | New ODIN implementation, LHCb!3088 (@clemenci) [#123]


### Fixes ~"bug fix" ~workaround

- ~selection ~hlt2 | Bring back F.XYZ, !1031 (@mengzhen)
- ~Tracking | Fix: configuration of public tools to use simplified material, !1049 (@chasse)


### Enhancements ~enhancement

- ~hlt2 | Add hyperon lines to hlt2_pp_thor and ensure the functor cache is build for this config, !1056 (@chasse)
- ~hlt2 | Add extra particles and more hadron combinations to radinclusive lines, !1038 (@aalfonso)
- ~hlt2 | Use available B2OC lines and fastest reconstruction in the HLT2 + selection throughput test., !1033 (@lmeyerga) [#333]
- ~Calo | Update of Calo Cluster Efficiency algorithm, !945 (@nuvallsc)
- ~Functors | Child + subcombination functor in test, !1047 (@nnolte)
- ~Functors ~Monitoring | Implement a ThOr functor-based monitoring, !1046 (@mramospe)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Functors | Add BPVCORRM functor in test which compares ThOr and Loki functors, !1072 (@gtuci)
- ~Monitoring | Remove AlignmentOnlineMonitor from configuration, !1061 (@decianm)
- ~Build | Update docs build to LCG 101 and lhcb-master, !1088 (@rmatev)
- ~Build | Use LbEnv for style checks and fix formatting, !1043 (@rmatev)
- Update v3 gcc11 refs, !1086 (@rmatev)
- Fixed Hlt1Conf.persistency.mdf_read_decs_allen.qmt prerequisites, !1042 (@spradlin) [#252]
- Update References for: Moore!1038 based on lhcb-master-mr/3026, !1073 (@lhcbsoft)
- Update References for: Rec!2579 based on lhcb-master-mr/2996, !1053 (@lhcbsoft)
- Update References for: Moore!1049 based on lhcb-master-mr/2995, !1052 (@lhcbsoft)
- Refs update for Moore Lbcom!573, !1045 (@ascarabo)

### Documentation ~Documentation

- Update HltEfficiencyChecker docs for TOS efficiencies, !1048 (@rjhunter)
- Analysing HLT1 decision reports after HLT2, !1051 (@nskidmor)
- Fix and expand Loki -> Thor documentation, !1023 (@dcervenk)
