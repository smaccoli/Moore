2022-11-10 Moore v54r1p1
===

This version uses
Allen [v3r1p1](../../../../Allen/-/tags/v3r1p1),
Rec [v35r1p1](../../../../Rec/-/tags/v35r1p1),
Lbcom [v34r1p1](../../../../Lbcom/-/tags/v34r1p1),
LHCb [v54r1p1](../../../../LHCb/-/tags/v54r1p1),
Gaudi [v36r8](../../../../Gaudi/-/tags/v36r8),
Detector [v1r5p1](../../../../Detector/-/tags/v1r5p1) and
This version uses LCG [101a](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on `master` branch.
Identical to Moore [v54r1](/../../tags/v54r1).
See corresponding Allen release notes for the actual changes.
