2017-08-02 Moore v26r5p1
========================================

Production release for 2017 data taking with changes already used in TCKs
----------------------------------------
Based on Gaudi v28r2, LHCb v42r5, Rec v21r5, Phys v23r5, Hlt v26r5p1

### Line developments


### New features


### Enhancements


### Bug fixes


### Cleanup and testing
- Tests for the VDM session configuration, !93 (@rmatev) [LBHLT-340,LBHLT-533]


### Other

