Running the HLT1 -> HLT2 chain
===============================

This documentation shows how to run HLT1 on DIGI files (with or without Retina clusters) and run HLT2 on the output specifying a HLT1 prefilter.

We assume you have the full stack built, however you can also run with `lb-run Moore/latest <my_options>.py`

Running HLT1
============================

Let's run HLT1 over some data stored in the `TestFileDB <https://gitlab.cern.ch/lhcb-datapkg/PRConfig/-/blob/master/python/PRConfig/TestFileDB.py>`_,
eg. `MiniBrunel_2018_MinBias_FTv4_DIGI`.

This MC sample does not have Retina clusters so we have to specify a special "sequence" called "hlt1_pp_veloSP". If your MC has Retina clusters you can simply remove the line

::

    with allen_sequence.bind(sequence="hlt1_pp_veloSP"):


In your `Moore/` directory place the following in a file called `HLT1_options.py`
::

    from Moore.config import allen_control_flow
    from Moore.options import options
    from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence, call_allen_decision_logger
    from RecoConf.decoders import default_ft_decoding_version
    from PyConf.application import configure_input, configure
    from PyConf.control_flow import CompositeNode, NodeLogic

    ##Set job options
    default_ft_decoding_version.global_bind(value=4)
    options.set_input_and_conds_from_testfiledb("MiniBrunel_2018_MinBias_FTv4_DIGI")
    options.evt_max = 100
    options.output_file = 'HLT1.dst'
    options.output_type = 'ROOT'

    ##Run Allen for MC without Retina clusters ("hlt1_pp_veloSP") and log HLT1 decisions
    with allen_sequence.bind(sequence="hlt1_pp_veloSP"):
        config = configure_input(options)
        allen_node = allen_control_flow(options)
        top_cf_node = CompositeNode(
        'MooreAllenWithLogger',
        combine_logic=NodeLogic.LAZY_AND,
        children=[allen_node, call_allen_decision_logger()],
        force_order=True)
        config.update(
            configure(options, top_cf_node))

and run with `./run gaudirun.py HLT1_options.py`.

In the output you will see
::

  GaudiAllenCountAndDumpLineDecisions    INFO Number of counters : 49
 |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |
 |*"Selected by Hlt1BeamGasDecision"               |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1BeamOneDecision"               |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1BeamTwoDecision"               |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1BothBeamsDecision"             |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1D2KKDecision"                  |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1D2KPiDecision"                 |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1D2PiPiDecision"                |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1DiMuonHighMassDecision"        |       100 |          1 |( 1.000000 +- 0.9949874)% |
 |*"Selected by Hlt1DiMuonLowMassDecision"         |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1DiMuonSoftDecision"            |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1DisplacedDielectronDecision"   |       100 |          0 |(  0.000000 +-  0.000000)% |
 |*"Selected by Hlt1DisplacedLeptonsDecision"      |       100 |          1 |( 1.000000 +- 0.9949874)% |
 |*"Selected by Hlt1GECPassthroughDecision"        |       100 |         99 |( 99.00000 +- 0.9949874)% |
 |*"Selected by Hlt1KsToPiPiDecision"              |       100 |          4 |( 4.000000 +- 1.959592)% |

This HLT1 output table tells you how many events each HLT1 line selected and is included because we called `call_allen_decision_logger` as well as the `allen_node`.

Running HLT2
============================

Now we can run HLT2 over this output.

We make a HLT2 line with a HLT1 decision prefilter, `hlt1_filter_code`, such that we only "look" at events that passed "Hlt1.*MVADecision" at HLT1 level. Note that here we are using Regex so "Hlt1TrackMuonMVADecision", "Hlt1TwoTrackMVADecision", "Hlt1XYZMVADecision" etc. will pass.


Place the following in a file called `HLT2_options.py` and run as before

::

    from Moore import options, run_moore
    from Moore.lines import Hlt2Line
    from RecoConf.global_tools import stateProvider_with_simplified_geom
    from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
    from RecoConf.reconstruction_objects import reconstruction

    def filter_hlt1_line(name="Hlt2FilterHlt1", prescale=1):
        return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=[],
        hlt1_filter_code=["Hlt1.*MVADecision"])


    def make_lines():
    return [filter_hlt1_line()]

    options.input_files = ['HLT1.dst']
    options.input_type = 'ROOT'
    options.simulation =True
    options.data_type = 'Upgrade'
    options.conddb_tag = "sim-20180530-vc-md100"
    options.dddb_tag = "dddb-20180815"
    options.evt_max = 1000
    options.scheduler_legacy_mode = False
    options.output_manifest_file = "my_hlt2.tck.json"


    with reconstruction.bind(from_file=False), allen_sequence.bind(sequence="hlt1_pp_veloSP"):
    config = run_moore(
        options,
        make_lines,
        public_tools=[
            stateProvider_with_simplified_geom()
        ],  # If you have run HLT1 prior set this to True to get HLT1 decisions
    )

Note the HLT2 line we created here ONLY runs the HLT1 pre-filter. Your HLT2 line will have the `algs` attribute of the `Hlt2Line` populated with selection algorithms.

All being well you should see that 7 events passed the HLT1 filter

`VoidFilter/Hlt1_prefilter                                                                                #=100     Sum=7           Eff=|( 7.000000 +- 2.55147 )%|`


which, accounting for overlap, is consistent with the HLT1 output table.
