# FIXME sphinx >=3.4.0 crashes on ConfigurableUser classes (ApplicationOptions)
# since it considers dict __slots__ as name: docstring and for us the value
# means default value... https://github.com/sphinx-doc/sphinx/pull/8546
sphinx==4.4.0
sphinx_rtd_theme==1.0.0
# Temporary pin urllib3 to circumvent compatibility issues, see e.g. https://gitlab.cern.ch/lhcb/Moore/-/issues/567
urllib3<=2.0
