Line authoring guidelines
=========================

.. |inst-lumi| replace:: :math:`2 \times 10^{33} \mathrm{cm}^{-2}\mathrm{s}^{-1}`

This page motivates and explains the various guidelines you should follow
when writing HLT2 lines. If you just want a birds-eye view of the key points,
skip to the :ref:`summary`, otherwise read on!

When writing an HLT2 line to select your physics of interest, there are
several questions worth considering before you start writing any code.

- Are there any similar lines, filters, or combiners already configured in
  Moore that I can re-use?
- Do I want an exclusive selection, capturing a specific physics process and
  final state, or an inclusive selection, capturing a set of many related
  processes?
- What backgrounds should I try to suppress?

And more, all based on matching the line to the analysis you intend to
perform. These questions should be discussed amongst your research group and
in physics working groups. Once answered, you can follow the
:doc:`../tutorials/hlt2_line` tutorial to get a feeling for how to write the
code.

With an HLT2 line written and running, there are further questions to
consider, based on the fact that HLT2 must run within certain operational
limits, and must be maintained by you and others into the future.

- What is an acceptable accept rate for my line?
- What is an acceptable output bandwidth for my line?
- How can I measure these things myself?

This page gives some guidelines to help you answer these questions. In
general, there are no hard-and-fast rules, and we must consider things on a
case-by-case basis. If things still aren't clear after reading this page, or
you think your usage is not addressed, you can ask a question:

- On the `Upgrade HLT2 Mattermost channel`_.
- As an `issue in the Moore repository`_.
- On the `RTA WP3 mailing list`_.

Efficiency
----------

Your primary concern as an analyst should be maximising signal efficiency. A
larger efficiency means more signal for a given production rate and fewer
opporunities for the selection to bias your signal.

To evaluate your signal efficiency you need a simulated sample of events
which contain your signal process. There are already many `simulated
samples`_ available, so there may be a suitable one available, and if not you
should request a sample.

Write a draft selection once you have a sample in hand, following the
:doc:`../tutorials/hlt2_line` tutorial if this is your first line. Check that
the output looks sensible to you using the :doc:`../tutorials/hlt2_analysis`
tutorial, and then use the output ntuples to check the efficiencies you care
about.

If the efficiencies look good, it's time to move on to measuring the rate.

.. _rate:

Rate
----

There is a natural push and pull between the constraints of operating the
trigger and the desire of the analyst to keep as much data as possible. For
the former there is a fairly hard limit on how many events per second can be
managed by the online and offline systems, and so we must ensure we do not
exceed this. For the latter it can be convinient to retain as much data
mining and data exploration capabilites as possible and to be able to study
the effect of selections by imposing them only offline, where they can be
relaxed and tightened at whim to study potential biases. These all lead to a
tendency to want to increase the event rate.

So, for the analyst, the most efficient and most minimally-biasing signal
selection is no selection at all. Not only is this idea prohibited
operationally online, however, it will also result in every analyst having to
process vast amounts of data offline, which is similarly impossible. There
are also practical concerns for the analyst, as manipulating a compact and
clean signal sample is much more managable than wrangling terabytes of
ntuples for each decay mode of interest. There are typically many 'obvious'
selections which remove some backgrounds cleanly at the expense of little
signal, and so it's in everyone's interests to apply these in the trigger.

Clearly, there is a balance to be struck. How do we achieve this?

Your line shares resources with every other. Having a hard cut-off rate for
every line will result in some selecting far more data than is reasonable,
and others being statistically limited. We need another metric.

The guideline for tuning your HLT2 line is to judge the rate against the
**true signal rate**. That is, if the physics process of interest occurs around
once per second, it would be unreasonable for your line to select one
thousand events per second as almost all events will be background, and you
will have to remove most of that offline anyway.

As the trigger is probably not 100% efficient on your signal, using the true
signal rate as a guideline already includes some tolerance for your line to
accept non-signal processes (background).

So, compute the true signal rate and use that as a benchmark against which to
judge whether your line is being far too greedy or whether you can afford to
relax your selection (or, indeed, whether you have a bug that's rejecting all
your signal!).

.. _signal_rate:

True signal rate
^^^^^^^^^^^^^^^^

In the usual proton-proton running scheme of the LHC, the instantaneous
luminosity of the colliding beams at the LHCb interaction point is around
|inst-lumi|. Knowing this along with the production cross-section of the
signal particle :math:`\sigma` and the branching fraction to the final-state
of interest :math:`\mathcal{B}` gives you an estimate of the signal rate

.. math::

    \mathrm{Signal\ rate} = \mathcal{L}_{\mathrm{Inst.}} \times \sigma(pp \to H) \times \mathcal{B}(H \to f).

If the production cross-section or branching fractions are not known, this
usually means they are quite small, and so your signal rate will be
correspondingly small. An existing measurement of a similar, more common
process can be used to compute a reasonable upper bound or estimate for your
signal.

Use the `LHCb publications page`_ to see what measurements exist for production
cross-sections, and consult the `Particle Data Group`_ for branching fractions.
If in doubt, discuss with your research group.

.. note::

    As an example, here we compute an estimate for the the signal rate of the
    process :math:`B^{+} \to J/\psi K^{+}`, with :math:`J/\psi \to \mu^{+} \mu^{-}`.
    We know these factors, uncertainties omitted:

    - The instantaneous luminosity in Run 3 proton-proton running is |inst-lumi|.
    - The :math:`B^{+}` production cross section is 87 microbarn, or :math:`87 \times 10^{-30} \mathrm{cm}^{-2}`.
    - The :math:`B^{+} \to J/\psi K^{+}` branching fraction is around :math:`10^{-3}`.
    - The :math:`J/\psi \to \mu^{+} \mu^{-}` branching fraction is around :math:`6 \times 10^{-2}`.

    Multiplying these factors together gives us an estimated signal rate of
    10 Hz, which is quite high as beauty rates go.

    Remember that this rate assumes 100% reconstruction and selection
    efficiency. In reality the efficiency can be considerably lower, by an
    order of magnitude or more. Observing the true signal rate as our HLT2
    line selection rate will indicate the presence of background.

Computing rate
^^^^^^^^^^^^^^

One typically uses signal Monte Carlo samples to estimate signal efficiencies
and to study the effect of the HLT2 reconstruction and selection on
quantities of interest. Signal MC contains one signal process per event, and
so is not representative of real data.

Minimum bias MC, with `event type`_ 30000000, is more representative of real
data, and is used to estimated the rate of HLT lines. By assuming that each
event in such a sample corresponds to one event out of the 30 million events
per second in real data, you can scale the accepted fraction of minimum bias
MC events for 30 MHz to get an estimate for the rate of your line

.. math::

    \mathrm{HLT2\ line\ rate} = 30\,\mathrm{MHz} \times \frac{N_{\mathrm{Accepted}}}{N_{\mathrm{Processed}}}.

If you process one million minimum bias MC events and your HLT2 line selects
100 of them, your line has an estimated rate of 3 kHz, which is very high for a
typical exclusive line. If no events are selected, one can estimate an upper
limit of the rate based on the number of processed events.

In real running conditions, HLT2 only sees events processed by HLT1, which is
not 100% efficient for many decay modes. There are HLT1-filtered MC samples
available which can partly account for the HLT1 efficiency. As the Real-time
Analysis group finalises the HLT1 menu, you will be able to make more precise
estimates of the HLT1 efficiency.

Follow the :doc:`../tutorials/hltefficiencychecker` tutorial to learn more
about measuring signal efficiencies and minimum bias rates. In the future we hope to
automate the execution of such tools in the centrally-run nightly tools and
monitor the trend of the rates of all lines over time.

Other considerations
^^^^^^^^^^^^^^^^^^^^

The HLT2 output rate in Run 3 is expected to be in the tens of kilohertz, up
to around 100 kHz, and we expect to run one to two thousand HLT2 lines. This
can give some context to your signal rate. If it is around one hertz or
below, this is very small in the context of the total rate, and the rate of
line can reasonably be rounded up to several hertz, if this is useful for
your analysis.

Use your best judgement when comparing the rate of your line to the signal
rate. If your analysis strategy necessitates selecting a very wide sideband
region in some invariant mass spectrum, a reasonable rate may be considerably
higher than the signal rate as the selection captures a lot more background.
Similarly, if your line selects several different processes, each of these
may bring in their own backgrounds, increasing the rate. Conversely, if you
know there are cuts you will apply before analysing the ntuples, apply
these upfront in the trigger

If a process is considered particuarly critical, for example if it is used by
many analyses or is a control or calibration channel, it may be justifiable
for the corresponding HLT2 line to take considerably more rate that the
signal rate to be be able to meet the various needs.

Ultimately, the distribution of the total HLT2 output rate across the lines
is decided amongst the physics working groups, taking the sorts of
considerations above into account. Still, the task of distributing rates is
much easier if each line already has a reasonable rate, so you should try to
do this earlier rather than later.

.. note::

    We have discussed evaluating the trigger performance using simulated
    data, which is an approximation of what we *think* the real data will
    look like. We will revisit the balance of rates once we have a better
    understanding of operating the trigger in real conditions.

Bandwidth
---------

The total HLT2 output bandwidth is the product of the total output rate and
the average event size

.. math::

    \mathrm{Bandwidth}\ [\mathrm{GB/s}] \propto \mathrm{Output\ rate}\ [\mathrm{kHz}] \times \mathrm{Avg.\ event\ size}\ [\mathrm{kB}].

The `Upgrade Computing Model TDR`_ gives the HLT2 output bandwidth in Run 3
will be ten gigabytes per second, with an average event size of around XX
kilobytes. Each time your HLT2 line makes a positive decision, it contributes
to the output bandwidth. As this is a limited resource, it's important to
understand the magnitude of this contribution and to limit it as much as
possible.

Today, we do not have a good way of measuring the HLT2 output bandwidth, and
so you do not need to worry about computing this exact number. However, one
important component of bandwidth is the rate, and so you can already start
getting a handle on your line's output bandwidth by ensuring your output rate
is sensible. See the :ref:`rate` section for advice.

The Turbo and Full streams
^^^^^^^^^^^^^^^^^^^^^^^^^^

As described in the `Upgrade Computing Model TDR`_ (chapter 3), there are two
output streams of HLT2 intended for almost all physics analysis. Most lines
send their selected events to the Turbo stream, whilst a few lines send their
selected events to the Full stream.

In the Turbo stream, analysts have near-immediate access to their data, and
can access every event and every object persisted by their HLT2 lines.
Typically this means only the candidate decay tree that actually fired the
line, with perhaps a few other objects in event as well.

In the Full stream, the entire reconstructed event is persisted. Analysts
must wait for this data to go through an additional filtering step called
Sprucing. This filters both the number of events and the number of objects
within each event in order to reduce the total data size saved to disk. (The
input data to the Sprucing is saved to tape, which analysts do not have
access to.)

The vast majority of HLT2 lines will send their data to the Turbo stream. The
few lines that output an enormous bandwidth, such as the inclusive
'topological beauty' lines, send their data to the Full stream, because we do
not have the resources store their full output on disk. Unless you know your
line is a special case, it will send events to the Turbo stream.

Further documentation on the streaming can be found in a dedicated
:doc:`section <../tutorials/streaming>`.

Timing and performance
----------------------

As HLT2 must process around one megahertz of HLT1-accepted events, it must be
fast. In computing the total HLT2 decision, each HLT2 line is executed as a
series of steps, with each step determining whether the next should run. To
keep HLT2 fast, each step should do as little work as possible, and a line
should abort processing as early as possible.

As almost every HLT2 line requires the full reconstruction, this is factored
out of the processing time taken by an individual line. The most time spent
in computing a line decision is then typically *combinatorics*, where N-body
vertices are created to form candidate decay chains. Here is the log produced
by an example two-body vertex creation algorithm:

.. code-block:: none

    CombineParticles#6                  SUCCESS Number of counters : 9
    |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |
    | "# D0 -> K-  K+ "                               |        73 |          0 |     0.0000 |     0.0000 |      0.0000 |      0.0000 |
    | "# FilterDesktop#6/Particles"                   |        73 |        349 |     4.7808 |     2.7210 |      0.0000 |      12.000 |
    | "# K+"                                          |        73 |        160 |     2.1918 |     1.6019 |      0.0000 |      6.0000 |
    | "# K-"                                          |        73 |        189 |     2.5890 |     1.7108 |      0.0000 |      8.0000 |
    | "# input particles"                             |        73 |        349 |     4.7808 |     2.7210 |      0.0000 |      12.000 |
    | "# selected"                                    |        73 |          0 |     0.0000 |     0.0000 |      0.0000 |      0.0000 |
    |*"#accept"                                       |        73 |          0 |( 0.000000 +- 0.000000)% |             |             |
    |*"#pass combcut"                                 |       484 |          8 |( 1.652893 +- 0.5795360)%|   -------   |   -------   |
    |*"#pass mother cut"                              |         8 |          0 |( 0.000000 +- 0.000000)% |   -------   |   -------   |

This algorithm processed 73 events and created a total of 484 two-body
combinations. All but 8 of these entered the computationally expensive vertex
fit, having been filtered out by the combination cuts.

It is good that very few candidates entered the vertex fit, but this
algorithm is still computing the combination cut against many two-body
combinations. Tighter selections on the input objects, kaons in this case,
would reduce the number of combinations per event, and hence reduce the total
processing time taken by this component. In conclusion, try to put cuts as early as possible.

We do not yet have centralised monitoring of the processing time of
individual lines, and when you run Moore yourself it's unlikely you'll make
realistic estimates of the *absolute* processing time. What is very helpful
is to look at the log of your job and ensure that no algorithms in your lines
are producing lots of candidates in every event.

We will update this section once we have more quantitative guidance.

.. _coding-conventions:

Code
----

It should be possible for any analyst to understand any HLT2 line. This helps
decrease the maintainance burden of HLT2 and helps us all understand how we
are selecting the events that enter our analyses.

To this end, there are a few coding conventions you should follow to keep the
source coherent. The :ref:`Moore coding conventions <coding_conventions>`
give an overview of Python-specific things, and you should try to get a
feeling for these conventions by looking at the files already in Moore. The
following are conventions specific to writing HLT2 lines.

Cut values
^^^^^^^^^^

Individual cut values should be specified as function arguments. These should
then be used in the cut string definitions. Don't do this::

    def make_pions():
        return PionMaker(Cut="(PT > 500 * MeV) & (PIDK < 5)")

Do this::

    @configurable
    def make_pions(pt_min=500 * MeV, pidk_max=5):
        cut = "(PT > {pt_min}) & (PIDK > {pidk_max})".format(
            pt_min=pt_min,
            pidk_max=pidk_max,
        )
        return PionMaker(Cut=cut)

Units are given in the definition of the default value, not in the cut. Don't
do this::

    @configurable
    def make_kaons(pt_min=500):
        return KaonMaker(Cut="PT > {pt_min} * MeV".format(pt_min=pt_min))

Do this::

    @configurable
    def make_kaons(pt_min=500 * MeV):
        return KaonMaker(Cut="PT > {pt_min} * MeV".format(pt_min=pt_min))

Cut values are defined as arguments to allow overriding with the
``@configurable`` decorator. You can read more about that in the :ref:`Tonic documentation <tonic-design>`.

Naming
^^^^^^

Almost of our names, like function names, function arguments, variables within
functions, and file names, are in lowercase `snake case`_ (like
``snake_case``). Exceptions are module-level constants, which are in uppercase
snake case (like ``SNAKE_CASE``), and class names, which are in `camel case`_
(like ``CamelCase``).

Threshold names are of the format ``<variable>_<threshold type>``. Examples are:

* ``pt_min``/``pt_max`` for a lower/upper bound transverse momentum cut ``PT >
  {pt_min}``.
* ``hasrich`` for a predicate (true/false) requirement ``HASRICH ==
  {hasrich}``.

HLT2 line names, the ``name`` argument you give to the `Moore.lines.Hlt2Line` constructor,
also have their own conventions. These are discussed in `Moore#60`_.

Style
^^^^^

Follow the :ref:`Moore coding conventions <coding_conventions>`. Python
module imports should be grouped together at the top of the file. These are
typically grouped by 'source' (first Python standard library, then from
Gaudi/``Configurables``, then ``PyConf`` or Moore, then your own code) and
then alphabetically within groups.

Structure
^^^^^^^^^

Use Python modules to structure your code. For example, if you're defining
many lines that use common maker functions or helpers, you can move these to
other files and import them into your line definition files. The
``b_to_open_charm`` HLT2 line module does a good job in demonstrating these
practices.

.. _summary:

Summary
-------

All nuance aside, below are the steps typically considered when adding an HLT2 line.

1. Discuss the idea with your research group and your physics analysis
   working group.
2. Look at what's available in Moore. If a selection already exists for your
   physics process, contact the author and work together to make sure it suits
   everyone's needs. If a selection does not exist, see if you can re-use parts
   from existing selections for similar processes.
3. See if `simulated samples`_ exist for your signal, and request some if not.
4. Write a draft selection for your signal, following the :ref:`structure and style <coding-conventions>`
   shown in existing HLT2 lines.
5. Compute the signal efficiencies you care about using the signal MC sample.
   For many analyses you will care about more than just the integrated or
   'total' efficiency, but the efficiency as a function of one or more
   variables. Think about this and check.
6. Compute the selection rate using minimum bias MC samples.
7. Using the ':ref:`true signal rate <signal_rate>`' as a guide, tune your
   selection, referring to step 5, until the rate seems acceptable.
8. Check that you apply cuts as early as possible, and that you do not create
   lots of combinations per event.
9. Discuss your findings and iterate with your working group.
10. Present your results at an RTA WP3 meeting.

You do not need to make detailed bandwidth and timing considerations at this
point.

.. _LHCb publications page: http://lhcbproject.web.cern.ch/lhcbproject/Publications/LHCbProjectPublic/Summary_all.html
.. _Particle Data Group: https://pdglive.lbl.gov/
.. _Upgrade HLT2 Mattermost channel: https://mattermost.web.cern.ch/lhcb/channels/upgrade-hlt2
.. _RTA WP3 mailing list: https://groups.cern.ch/group/lhcb-rta-selections/default.aspx
.. _issue in the Moore repository: https://gitlab.cern.ch/lhcb/Moore/-/issues
.. _event type: http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/
.. _Upgrade Computing Model TDR: https://cds.cern.ch/record/2319756
.. _snake case: https://en.wikipedia.org/wiki/Snake_case
.. _camel case: https://en.wikipedia.org/wiki/Camel_case
.. _Moore#60: https://gitlab.cern.ch/lhcb/Moore/-/issues/60
.. _simulated samples: https://its.cern.ch/jira/browse/LHCBGAUSS-1837
.. _Upgrade Computing Model TDR: https://cds.cern.ch/record/2319756
